<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'stitchdb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'F: @.AfA]pK}(1tLFY8VtG)o7W;&;vi+zs[C#j `MEylR8]LW6j)|^28uOdri&`4');
define('SECURE_AUTH_KEY',  ' OMJ gb!V!MWuS{-:zc:qlx4J$ }-*P;O8@zDgz>zBjParc?uAUW Qfg5>4p)/7.');
define('LOGGED_IN_KEY',    'nE{5a{sj7 `s`ma^<Oijv#*_[*E/q099;Rw:He;)`Xu6>m*DzP6k?IOVu4 g5?Bv');
define('NONCE_KEY',        'vfre3.DyXg;?kFoTm3 oUnl)D5SQI:y@(5c6k*;t>c3WyyL!0<D!}{+}YR#f!@|x');
define('AUTH_SALT',        'h}=([.lk>EK7;{/8=VLO&o-~@:*h~4s[#YC|n{L@f>m7l )cc+s2llH9`y&<O !o');
define('SECURE_AUTH_SALT', 'Ph~(-5%9I9IW>FF,N6R4OLZqSKCR+1DLK>eZ7z||/VY!UN1e48S3,Kz0QLMfY^Zz');
define('LOGGED_IN_SALT',   '%IYxS8?G>adjB0/0`&<t+!2^Cd:<h3:N|P_y5|_NLfT.f2)?L/.<?,Ve3^zr`9{*');
define('NONCE_SALT',       'Y1`b>w>OM[z>.f1&J0cXU:8w]:>JD*3 ?o8PS:?U^:4SNgej![x!NmM}L##Zy>o~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
