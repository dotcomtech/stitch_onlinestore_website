<?php
	/**
	 * Gateway class
	 */
	class WC_Gateway_CardStream_Form extends WC_Payment_Gateway {

		private $gateway = 'cardstream';

		/**
	 	 * Get all the options and constants
	 	 */
		public function __construct() {

			$this->id     			= 'cardstream';
			$this->method_title   	= __('CardStream / Charity Clear Hosted', 'woocommerce_cardstream');
			$this->icon     		= $this->get_plugin_url() . '/images/payment-icons.png';
			$this->has_fields    	= false;

			// Load the form fields
			$this->init_form_fields();

			// Load the settings.
			$this->init_settings();

			// Get setting values
			$this->enabled       		= isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'no';
			$this->title        		= isset( $this->settings['title'] ) ? $this->settings['title'] : 'Credit Card via CardStream';
			$this->description       	= isset( $this->settings['description'] ) ? $this->settings['description'] : 'Pay via Credit / Debit Card with CardStream secure card processing.';
			$this->gateway 				= isset( $this->settings['gateway'] ) ? $this->settings['gateway'] : 'cardstream';
			$this->VPMerchantID         = isset( $this->settings['VPMerchantID'] ) ? $this->settings['VPMerchantID'] : '';
			$this->VPSignature			= isset( $this->settings['VPSignature'] ) ? $this->settings['VPSignature'] : '';
			$this->VPCountryCode        = isset( $this->settings['VPCountryCode'] ) ? $this->settings['VPCountryCode'] : '826';
			$this->VPCurrencyCode       = isset( $this->settings['VPCurrencyCode'] ) ? $this->settings['VPCurrencyCode'] : '826';
			$this->VPPaymentURL       	= isset( $this->settings['VPPaymentURL'] ) ? $this->settings['VPPaymentURL'] : 'https://gateway.cardstream.com/hosted/';
			$this->debug				= isset( $this->settings['debug'] ) && $this->settings['debug'] == 'yes' ? true : false;
			
			// Hooks
			/* 1.6.6 */
			add_action( 'woocommerce_update_options_payment_gateways', array( $this, 'process_admin_options' ) );

			/* 2.0.0 */
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

			/**
			 *  API
			 *  
			 *  woocommerce_api_{lower case class name}
			 */
            add_action( 'woocommerce_api_wc_gateway_cardstream_form', array( $this, 'verify_cardstream_response' ) );

            add_action( 'woocommerce_success_cardstream', array( $this, 'check_cardstream_response' ) );
			add_action( 'woocommerce_receipt_cardstream', array( $this, 'receipt_page' ) );

		} // END __construct

		/**
    	 * Initialise Gateway Settings Form Fields
    	 */
		function init_form_fields() {

			$this->form_fields = array(
				'enabled'		=> array(
					'title'   		=> __( 'Enable/Disable', 'woocommerce_cardstream' ),
					'label'   		=> __( 'Enable CardStream / Charity Clear', 'woocommerce_cardstream' ),
					'type'    		=> 'checkbox',
					'description'  	=> '',
					'default'   	=> 'no'
				),

				'title'			=> array(
					'title'   		=> __( 'Title', 'woocommerce_cardstream' ),
					'type'    		=> 'text',
					'description'  	=> __( 'This controls the title which the user sees during checkout.', 'woocommerce_cardstream' ),
					'default'   	=> __( 'Credit Card via ' . ucwords( $this->gateway ), 'woocommerce_cardstream' )
				),

				'description'	=> array(
					'title'   		=> __( 'Description', 'woocommerce_cardstream' ),
					'type'    		=> 'textarea',
					'description'  	=> __( 'This controls the description which the user sees during checkout.', 'woocommerce_cardstream' ),
					'default'   	=> 'Pay via Credit / Debit Card with ' . ucwords( $this->gateway ) . ' secure card processing.'
				),

				'gateway'     	=> array(
				    'title'         => __( 'Payment Processor', 'woocommerce_cardstream' ),
				    'type'          => 'select',
				    'options'       => array('cardstream'=>'CardStream','charityclear'=>'Charity Clear'),
				    'description'   => __( 'Choose your Payment Processor', 'woocommerce_cardstream' ),
				    'default'       => 'cardstream'
				),

				'VPMerchantID'	=> array(
					'title'   		=> __( 'Merchant ID', 'woocommerce_cardstream' ),
					'type'    		=> 'text',
					'description'  	=> __( 'What is your Merchant ID, this will have been supplied to you by CardStream / Charity Clear', 'woocommerce_cardstream' ),
					'default'   	=> 'testing'
				),

				'VPSignature'	=> array(
					'title'   		=> __( 'Hash Signature', 'woocommerce_cardstream' ),
					'type'    		=> 'text',
					'description'  	=> __( 'What is your Hash Signature, this must match the signature in your CardStream / Charity Clear account', 'woocommerce_cardstream' ),
					'default'   	=> ''
				),

				'VPCountryCode'	=> array(
					'title'   		=> __( 'Country Code', 'woocommerce_cardstream' ),
					'type'    		=> 'text',
					'description'  	=> __( 'What is your Country Code? For the UK you should use 826, for other countries please contact CardStream / Charity Clear', 'woocommerce_cardstream' ),
					'default'   	=> '826'
				),

				'VPCurrencyCode'=> array(
					'title'   		=> __( 'Currency Code', 'woocommerce_cardstream' ),
					'type'    		=> 'text',
					'description'  	=> __( 'What is your Currency Code? For the UK you should use 826, for other countries please contact CardStream / Charity Clear', 'woocommerce_cardstream' ),
					'default'   	=> '826'
				),

				'VPPaymentURL'=> array(
					'title'   		=> __( 'Payment URL', 'woocommerce_cardstream' ),
					'type'    		=> 'text',
					'description'  	=> __( 'If you have requested special branding you will need to enter the URL here, otherwise leave this as https://gateway.cardstream.com/hosted/', 'woocommerce_cardstream' ),
					'default'   	=> 'https://gateway.cardstream.com/hosted/'
				),

				'debug'     	=> array(
				    'title'         => __( 'Debug Mode', 'woocommerce_cardstream' ),
				    'type'          => 'checkbox',
				    'options'       => array('no'=>'No','yes'=>'Yes'),
				    'label'     	=> __( 'Enable Debug Mode', 'woocommerce_cardstream' ),
				    'default'       => 'no'
				)
			);

		} // END init_form_fields

		/**
		 * Returns the plugin's url without a trailing slash
		 */
		public function get_plugin_url() {

			return str_replace( '/classes', '/', untrailingslashit( plugins_url( '/', __FILE__ ) ) );
		}

		/**
 		 * Admin Panel Options
		 * - Options for bits like 'title' and availability on a country-by-country basis
		 */
		public function admin_options() {
?>
	    	<h3><?php _e('CardStream / Charity Clear Hosted', 'woocommerce_cardstream'); ?></h3>
			<p><?php _e('This gateway works by sending the user to CardStream or Charity Clear to enter their payment information.', 'woocommerce_cardstream'); ?></p>
			<table class="form-table">
<?php
			// Generate the HTML for the settings form.
			$this->generate_settings_html();
?>
			</table><!--/.form-table-->
			<?php
		} // END admin_options

		/**
		 * Generate the form button
		 */
		public function generate_cardstream_form( $order_id ) {
			global $woocommerce;

			$order 			 = new WC_Order( $order_id );
			$VPAmount 		 = $order->get_total() * 100;

			$successurl      = str_replace( 'https:', 'http:', add_query_arg('wc-api', 'WC_Gateway_CardStream_Form', home_url( '/' ) ) );
			
			/**
			 * This is the form.
			 */
			
            // WooCommerce 2.1
            if ( $this->debug == false ) {
            	if ( function_exists( 'wc_enqueue_js' ) ) {
					wc_enqueue_js('
					jQuery(function(){
						jQuery("body").block({

							message: "<img src=\"'.$woocommerce->plugin_url().'/assets/images/ajax-loader.gif\" alt=\"Redirecting...\" />'.__('Thank you for your order. We are now redirecting you to CardStream to make payment.', 'woocommerce_cardstream').'",
							overlayCSS:
							{
								background: "#fff",
								opacity: 0.6
							},
							css:
								{
						        	padding:        20,
							        textAlign:      "center",
							        color:          "#555",
							        border:         "3px solid #aaa",
							        backgroundColor:"#fff",
							        cursor:         "wait",
							        lineHeight:		"32px"
						    	}

							});
						jQuery("#submit_cardstream_payment_form").click();
					});
				');
				} else {
					$woocommerce->add_inline_js('
					jQuery(function(){
						jQuery("body").block({

							message: "<img src=\"'.$woocommerce->plugin_url().'/assets/images/ajax-loader.gif\" alt=\"Redirecting...\" />'.__('Thank you for your order. We are now redirecting you to CardStream to make payment.', 'woocommerce_cardstream').'",
							overlayCSS:
							{
								background: "#fff",
								opacity: 0.6
							},
							css:
								{
						        	padding:        20,
							        textAlign:      "center",
							        color:          "#555",
							        border:         "3px solid #aaa",
							        backgroundColor:"#fff",
							        cursor:         "wait",
							        lineHeight:		"32px"
						    	}

							});
						jQuery("#submit_cardstream_payment_form").click();
					});
				');
				}
			} // End $this->debug == false

			// Build the address
			$cardstreamaddress  = $order->billing_address_1 . ",\n";
			if ( isset( $order->billing_address_2 ) && $order->billing_address_2 != '' ) {
				$cardstreamaddress .= $order->billing_address_2 . ",\n";
			}
			$cardstreamaddress .= $order->billing_city . ",\n";
			$cardstreamaddress .= $order->billing_state . ",\n";

			if ( strlen( $cardstreamaddress ) > 1 ) {
				$cardstreamaddress = substr( trim( $cardstreamaddress ), 0, -1 );
			}
			
			// Fields for hash
			$fields = array(
						"merchantID" 		=> $this->VPMerchantID,
						"amount" 			=> $VPAmount,
						"countryCode" 		=> $this->VPCountryCode,
						"currencyCode" 		=> $this->VPCurrencyCode,
						"transactionUnique" => $order->order_key . '-' . $order->id . '-' . time(),
						"orderRef" 			=> $order->id,
						"redirectURL" 		=> $successurl,
						"customerName" 		=> $order->billing_first_name.' '.$order->billing_last_name,
						"customerAddress"	=> $cardstreamaddress,
						"customerPostCode"	=> $order->billing_postcode,
						"customerEmail" 	=> $order->billing_email,
						"customerPhone" 	=> $order->billing_phone
					);
			ksort( $fields );

			// Build the signature
			$signature = http_build_query( $fields ) . $this->VPSignature;
			$fields['signature'] = hash( 'SHA512', $signature );

			// Output debugging information
			if ( $this->debug ) {
				echo '<pre>';
					var_dump($fields);
				echo '</pre>';

				echo '<pre>';
					echo $this->VPSignature;
				echo '</pre>';

				echo '<pre>';
					echo $fields['signature'];
				echo '</pre>';
			}

			// Make sure we're using the correct URL for posting the form
			if( $this->gateway == 'charityclear' ) {
				$this->VPPaymentURL = 'https://gateway.charityclear.com/paymentform/';
			}

			// Build the form
			$cardstreamform = '<form action="' . $this->VPPaymentURL . '" method="post" id="cardstream_payment_form">';
			foreach ( $fields as $key => $value ) {

				$cardstreamform .= '<input type="hidden" name="' . $key . '" value="' . $value . '" />';

			}
			$cardstreamform .= '<input type="submit" class="button alt" id="submit_cardstream_payment_form" value="'.__('Pay securly via ' . ucwords( $this->gateway ), 'woocommerce_cardstream').'" /> <a class="button cancel" href="'.$order->get_cancel_order_url().'">'.__('Cancel order &amp; restore cart', 'woocommerce_cardstream').'</a>';
			$cardstreamform .= '</form>';

			return $cardstreamform;

		} // END generate_form

		/**
		 * Process the payment and return the result
		 */
        function process_payment( $order_id ) {
            $order = new WC_Order( $order_id );

            return array(
                'result'    => 'success',
            	'redirect'	=> $order->get_checkout_payment_url( true )
            );
            
        } // END process_payment

		/**
		 * receipt_page
		 */
		function receipt_page( $order ) {
			echo '<p>'.__('Thank you for your order, please click the button below to pay with ' .ucwords( $this->gateway ). '.', 'woocommerce_cardstream').'</p>';
			echo $this->generate_cardstream_form( $order );
		} // END receipt_page

		/**
		 * Check CardStream Verify Response
		 */
		function verify_cardstream_response() {

			if ( isset( $_POST['responseCode'] ) && $_POST['responseCode'] != '' ) {

				do_action( 'woocommerce_success_cardstream', $_POST );
			}
		}

		/**
		 * Check for CardStream Response
 		 */
		function check_cardstream_response( $requested ) {
			global $woocommerce;
			
			if ( isset($_REQUEST["responseCode"]) ) {

				$VPResponseCode   		= $requested["responseCode"];
				$VPMessage              = $requested["responseMessage"];
				$VPAmountReceived       = $requested["amount"];
				$VPTransactionUnique    = $requested["transactionUnique"];

				if ( isset($VPResponseCode) ) {

					$cardstream_return_values = explode('-', $VPTransactionUnique);

					$order       = new WC_Order( (int) $cardstream_return_values[1] );

					if ($order->status == 'completed') {

					} else {

						$orderNotes  =  "\r\n" . 'Response Code : '   . $VPResponseCode . "\r\n";
						$orderNotes .=  'Message : '        . $VPMessage . "\r\n";
						$orderNotes .=  'Amount Received : ' . number_format( $VPAmountReceived/100, 2, '.', ',' ) . "\r\n";
						$orderNotes .=  'Unique Transaction Code : ' . $VPTransactionUnique;

						if ( $VPResponseCode == '00' ) {

							$order->add_order_note( __(ucwords( $this->gateway ).' payment completed.' . $orderNotes, 'woocommerce_cardstream') );
							$order->payment_complete();

							wp_safe_redirect( $this->get_return_url( $order ) );
							exit;

						} else {

							$message = ( sprintf( __( 'Payment error: %s', 'woocommerce_cardstream' ), $VPMessage ) );
							wc_add_notice( $message, $notice_type = 'error' );

							$order->add_order_note( __(ucwords( $this->gateway ).' payment failed.' . $orderNotes, 'woocommerce_cardstream') );
							wp_safe_redirect( $order->get_cancel_order_url( $order ) );
							exit;

						}

					}

				} else {

					$order->add_order_note( __(ucwords( $this->gateway ).' payment failed.' . $orderNotes, 'woocommerce_cardstream') );
					wp_safe_redirect( $order->get_cancel_order_url( $order ) );
					exit;

				}

			}

		} // END check_cardstream_response

	} // END CLASS