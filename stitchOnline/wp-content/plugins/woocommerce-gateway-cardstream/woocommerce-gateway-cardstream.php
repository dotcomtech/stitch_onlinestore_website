<?php
/*
Plugin Name: WooCommerce CardStream Gateway
Plugin URI: http://woothemes.com/woocommerce/
Description: Extends WooCommerce. Provides a CardStream gateway for WooCommerce. http://www.cardstream.com/. Now compatible with WooCommerce 2.0.
Version: 2.2.2
Author: Add On Enterprises
Author URI: http://www.addonenterprises.com
Woo Support: http://support.woothemes.com
Woo Codex: http://wcdocs.woothemes.com/user-guide/cardstream/
Woo Settings: admin.php?page=woocommerce_settings&tab=payment_gateways
*/

/*  Copyright 2013  Add On Enterprises  (email : support@addonenterprises.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) )
	require_once( 'woo-includes/woo-functions.php' );

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), '7a82b3c829d3a466ad50be4ab9225643', '18710' );

/**
 * Init CardStream Gateway after WooCommerce has loaded
 **/
add_action('plugins_loaded', 'init_cardstream_gateway', 0);

function init_cardstream_gateway() {

	if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;

	/**
	 * Add the CardStream Hosted Gateway to WooCommerce
	 */
	include('classes/cardstream-hosted-class.php');

	function add_cardstream_form_gateway($methods) {
		$methods[] = 'WC_Gateway_CardStream_Form';
		return $methods;
	}
	add_filter('woocommerce_payment_gateways', 'add_cardstream_form_gateway' );

} // END init_cardstream_gateway