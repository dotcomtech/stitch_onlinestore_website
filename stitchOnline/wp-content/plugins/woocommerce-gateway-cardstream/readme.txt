Installation :
Unzip and upload the plugin�s folder to your /wp-content/plugins/ directory
Activate the extension through the �Plugins� menu in WordPress
Go to your site admin -> WooCommerce -> Settings . Now click on the Payment Gateways tab and look for the CardStream link just under the tabs -> click it
Complete the form with the details from CardStream then click 'Save Changes'.


Configuration :
In order to get up and running with the CardStream gateway you only need to check  the 'Enable CardStream'  checkbox and add your CardStream Merchant ID then click 'Save Changes'
You can change what the customer sees on your checkout page by editing the title and description areas. 
If you do not use GBP then you need to contact CardStream for currency and country codes.
If you have a branded payment page then you can enter the URL from CardStream into the 'Payment URL' box.
Don't forget to click save changes!