<?php

if (!defined('ABSPATH')) {
    exit;
}

function dig_wc_search_usr($found_customers){

    if ( ! current_user_can( 'edit_shop_orders' ) ) {
        wp_die( -1 );
    }
    $term    = wc_clean( stripslashes( $_GET['term'] ) );

    if(!is_numeric($term)) return $found_customers;




    $ids = getUserIDSfromPhone($term);

    if(empty($ids) || !is_array($ids)){
        return $found_customers;
    }
    
    if(count($ids)==0)return $found_customers;

    if ( ! empty( $_GET['exclude'] ) ) {
        $ids = array_diff( $ids, (array) $_GET['exclude'] );
    }
    foreach ( $ids as $id ) {
        $customer = new WC_Customer( $id );
        /* translators: 1: user display name 2: user ID 3: user email */
        $found_customers[ $id ] = sprintf(
            esc_html__( '%1$s (#%2$s &ndash; %3$s)', 'woocommerce' ),
            $customer->get_first_name() . ' ' . $customer->get_last_name(),
            $customer->get_id(),
            $customer->get_email()
        );
    }

    return $found_customers;


}

add_action('woocommerce_json_search_found_customers','dig_wc_search_usr');