<?php

function dig_bp_core_screen_signup() {
    $bp = buddypress();


    if ( ! bp_is_current_component( 'register' ) || bp_current_action() )
        return;

    // Not a directory.
    bp_update_is_directory( false, 'register' );

    // If the user is logged in, redirect away from here.
    if ( is_user_logged_in() ) {

        $redirect_to = bp_is_component_front_page( 'register' )
            ? bp_get_members_directory_permalink()
            : bp_get_root_domain();

        /**
         * Filters the URL to redirect logged in users to when visiting registration page.
         *
         * @since 1.5.1
         *
         * @param string $redirect_to URL to redirect user to.
         */
        bp_core_redirect( apply_filters( 'bp_loggedin_register_page_redirect_to', $redirect_to ) );

        return;
    }

    $bp->signup->step = 'request-details';





    if ( !bp_get_signup_allowed() ) {
        $bp->signup->step = 'registration-disabled';

        // If the signup page is submitted, validate and save.
    } elseif ( isset( $_POST['signup_submit'] ) && bp_verify_nonce_request( 'bp_new_signup' ) ) {


        /**
         * Fires before the validation of a new signup.
         *
         * @since 2.0.0
         */
        do_action( 'bp_signup_pre_validate' );

        // Check the base account details for problems.


        $name = sanitize_text_field($_POST['field_1']);
        $mail = sanitize_text_field($_POST['mobile/email']);
        $password = sanitize_text_field($_POST['signup_password']);
        $code = sanitize_text_field($_POST['code']);
        $csrf = sanitize_text_field($_POST['csrf']);

        $otp = sanitize_text_field($_POST['digit_ac_otp']);




        if (empty($name) ) {
            $bp->signup->errors['signup_name'] =  __("Invalid Name!","digits");
        }

        if (empty($code) && empty($mail) && empty($otp)) {
            $bp->signup->errors['signup_mail'] =  __("Invalid Email!","digits");

        }


        if (empty($mail)) {
            $bp->signup->errors['signup_mail'] =  __("Invalid Email or Mobile number","digits");
        }

        if(isValidEmail($mail) && empty($password)){
            $bp->signup->errors['signup_password'] =  __("Invalid Password!","digits");
        }



        if(empty($password)) $password = wp_generate_password();







        // Now we've checked account details, we can check profile information.
        if ( bp_is_active( 'xprofile' ) ) {

            // Make sure hidden field is passed and populated.
            if ( isset( $_POST['signup_profile_field_ids'] ) && !empty( $_POST['signup_profile_field_ids'] ) ) {

                // Let's compact any profile field info into an array.
                $profile_field_ids = explode( ',', $_POST['signup_profile_field_ids'] );

                // Loop through the posted fields formatting any datebox values then validate the field.
                foreach ( (array) $profile_field_ids as $field_id ) {
                    bp_xprofile_maybe_format_datebox_post_data( $field_id );

                    // Create errors for required fields without values.
                    if ( xprofile_check_is_required_field( $field_id ) && empty( $_POST[ 'field_' . $field_id ] ) && ! bp_current_user_can( 'bp_moderate' ) )
                        $bp->signup->errors['field_' . $field_id] = __( 'This is a required field', 'buddypress' );
                }

                // This situation doesn't naturally occur so bounce to website root.
            } else {
                bp_core_redirect( bp_get_root_domain() );
            }
        }

        // Finally, let's check the blog details, if the user wants a blog and blog creation is enabled.
        if ( isset( $_POST['signup_with_blog'] ) ) {
            $active_signup = bp_core_get_root_option( 'registration' );

            if ( 'blog' == $active_signup || 'all' == $active_signup ) {
                $blog_details = bp_core_validate_blog_signup( $_POST['signup_blog_url'], $_POST['signup_blog_title'] );

                // If there are errors with blog details, set them for display.
                if ( !empty( $blog_details['errors']->errors['blogname'] ) )
                    $bp->signup->errors['signup_blog_url'] = $blog_details['errors']->errors['blogname'][0];

                if ( !empty( $blog_details['errors']->errors['blog_title'] ) )
                    $bp->signup->errors['signup_blog_title'] = $blog_details['errors']->errors['blog_title'][0];
            }
        }

        /**
         * Fires after the validation of a new signup.
         *
         * @since 1.1.0
         */
        do_action( 'bp_signup_validate' );
        $validation_error = new WP_Error();
        // Add any errors to the action for the field in the template for display.

        if ( !empty( $bp->signup->errors ) ) {

            foreach ( (array) $bp->signup->errors as $fieldname => $error_message ) {
                /*
                 * The addslashes() and stripslashes() used to avoid create_function()
                 * syntax errors when the $error_message contains quotes.
                 */

                /**
                 * Filters the error message in the loop.
                 *
                 * @since 1.5.0
                 *
                 * @param string $value Error message wrapped in html.
                 */
                add_action( 'bp_' . $fieldname . '_errors', create_function( '', 'echo apply_filters(\'bp_members_signup_error_message\', "<div class=\"error\">" . stripslashes( \'' . addslashes( $error_message ) . '\' ) . "</div>" );' ) );
            }
        } else {
            $bp->signup->step = 'save-details';

            // No errors! Let's register those deets.
            $active_signup = bp_core_get_root_option( 'registration' );

            if ( 'none' != $active_signup ) {

                // Make sure the extended profiles module is enabled.
                if ( bp_is_active( 'xprofile' ) ) {
                    // Let's compact any profile field info into usermeta.
                    $profile_field_ids = explode( ',', $_POST['signup_profile_field_ids'] );

                    /*
                     * Loop through the posted fields, formatting any
                     * datebox values, then add to usermeta.
                     */
                    foreach ( (array) $profile_field_ids as $field_id ) {
                        bp_xprofile_maybe_format_datebox_post_data( $field_id );

                        if ( !empty( $_POST['field_' . $field_id] ) )
                            $usermeta['field_' . $field_id] = $_POST['field_' . $field_id];

                        if ( !empty( $_POST['field_' . $field_id . '_visibility'] ) )
                            $usermeta['field_' . $field_id . '_visibility'] = $_POST['field_' . $field_id . '_visibility'];
                    }

                    // Store the profile field ID's in usermeta.
                    $usermeta['profile_field_ids'] = $_POST['signup_profile_field_ids'];
                }


                // If the user decided to create a blog, save those details to usermeta.
                if ( 'blog' == $active_signup || 'all' == $active_signup )
                    $usermeta['public'] = ( isset( $_POST['signup_blog_privacy'] ) && 'public' == $_POST['signup_blog_privacy'] ) ? true : false;


                /**
                 * Filters the user meta used for signup.
                 *
                 * @since 1.1.0
                 *
                 * @param array $usermeta Array of user meta to add to signup.
                 */
                $usermeta = apply_filters( 'bp_signup_usermeta', $usermeta );

                // Finally, sign up the user and/or blog.

                $check = username_exists($name);
                if (!empty($check)) {
                    $suffix = 2;
                    while (!empty($check)) {
                        $alt_ulogin = $name . $suffix;
                        $check = username_exists($alt_ulogin);
                        $suffix++;
                    }
                    $ulogin = $alt_ulogin;
                }else{
                    $ulogin = $name;
                }

                if (!empty($code) || !empty($otp)) {




                    $digit_tapp = get_option('digit_tapp',1);

                    if($digit_tapp==1){

                        $json = getUserPhoneFromAccountkit($code);

                        $phoneJson = json_decode($json, true);

                        if ($json == null) {
                            $validation_error->add("apifail", __("Invalid API credentials!","digits"));
                        }


                        $mob = $phoneJson['phone'];
                        $phone = $phoneJson['nationalNumber'];
                        $countrycode = $phoneJson['countrycode'];
                    }else{


                        $m = $mail;

                            $countrycode = sanitize_text_field($_REQUEST['digt_countrycode']);
                            if(verifyOTP($countrycode,$m,$otp,true)){
                                $mob = $countrycode.$m;
                                $phone = $m;
                            }



                    }


                            $useMobAsUname = get_option('dig_mobilein_uname',0);

                            if($useMobAsUname==1 ) {
                                $mobu = str_replace("+","",$mob);
                                $check = username_exists($mobu);
                                if (!empty($check)) {
                                    $validation_error->add("MobinUse", __("Mobile number already in use!","digits"));
                                } else {
                                    $ulogin = $mobu;
                                }
                            }



                    $mobuser = getUserFromPhone($mob);
                    if ($mobuser != null) {
                        $validation_error->add("MobinUse", __("Mobile number already in use!","digits"));
                    } else if (username_exists($mob)) {
                        $validation_error->add("MobinUse", __("Mobile number already in use!","digits"));
                    }

                    if (!$validation_error->get_error_code()) {


                        if(empty($password)) $password = wp_generate_password();


                        $ulogin = sanitize_user($ulogin,true);
                        $new_customer = wp_create_user($ulogin, $password, $mail);

                        if (! is_wp_error( $new_customer ) ) {
                            update_user_meta($new_customer, 'digt_countrycode', $countrycode);
                            update_user_meta($new_customer, 'digits_phone_no', $phone);
                            update_user_meta($new_customer, 'digits_phone', $countrycode . $phone);
                            $page = 1;
                        }else{
                            $validation_error->add("Error", __("Error","digits"));
                        }

                    } else {



                    }

                } else {


                    if(empty($password)){
                        $validation_error->add("invalidpassword", __("Invalid Password!","digits"));
                    }
                    if (!$validation_error->get_error_code()) {
                        $ulogin = sanitize_user($ulogin,true);
                        $new_customer = wp_create_user($ulogin, $password, $mail);

                        $login_message = "<span class='msggreen'>User registered successfully.</span>";

                        $page = 1;
                    } else {

                    }

                }



                if ($validation_error->get_error_code()) {
                    $bp->signup->step = 'request-details';
                    bp_core_add_message( $validation_error->get_error_message(), 'error' );
                }else{

                    if (! is_wp_error( $new_customer ) ) {
                        $defaultuserrole = get_option('defaultuserrole', "customer");
                        wp_update_user(array(
                            'ID' => $new_customer,
                            'role' => $defaultuserrole,
                            'first_name' => $name,
                            'display_name'=>$name));


                        $bp->signup->step = 'completed-confirmation';


                            $userd = get_user_by( 'ID', $new_customer );

                            dig_bp_core_signup_user($userd->ID,$mail,$usermeta,$userd->user_login,$password);

                            wp_set_current_user($userd->ID, $userd->user_login);

                            if (wp_validate_auth_cookie()==FALSE)
                            {
                                wp_set_auth_cookie($userd->ID, true, false);
                            }







                    }else{
                        $bp->signup->step = 'request-details';
                        bp_core_add_message( $validation_error->get_error_message(), 'error' );
                    }

                }

            }


            /**
             * Fires after the completion of a new signup.
             *
             * @since 1.1.0
             */
            do_action( 'bp_complete_signup' );
        }

    }

    /**
     * Fires right before the loading of the Member registration screen template file.
     *
     * @since 1.5.0
     */
    do_action( 'dig_bp_core_screen_signup' );

    /**
     * Filters the template to load for the Member registration page screen.
     *
     * @since 1.5.0
     *
     * @param string $value Path to the Member registration template to load.
     */
    bp_core_load_template( apply_filters( 'bp_core_template_register', array( 'register', 'registration/register' ) ) );
    unset($_POST);
}


function dig_bp_core_signup_user( $user_id, $user_email,$usermeta,$user_login,$user_password) {
	$bp = buddypress();


	// We need to cast $user_id to pass to the filters.



    if(!empty($user_email)){
		// Format data.
		$user_email     = sanitize_email( $user_email );
		$activation_key = wp_generate_password( 32, false );

		if ( ! defined( 'BP_SIGNUPS_SKIP_USER_CREATION' ) || ! BP_SIGNUPS_SKIP_USER_CREATION ) {
			bp_update_user_meta( $user_id, 'activation_key', $activation_key );
		}
		$args = array(
			'activation_key' => $activation_key,
			'user_login'     => $user_login,
			'meta'           => $usermeta,
		);

		BP_Signup::add( $args );
    }
		/**
		 * Filters if BuddyPress should send an activation key for a new signup.
		 *
		 * @since 1.2.3
		 *
		 * @param bool   $value          Whether or not to send the activation key.
		 * @param int    $user_id        User ID to send activation key to.
		 * @param string $user_email     User email to send activation key to.
		 * @param string $activation_key Activation key to be sent.
		 * @param array  $usermeta       Miscellaneous metadata about the user (blog-specific
		 *                               signup data, xprofile data, etc).
		 */
		if ( apply_filters( 'bp_core_signup_send_activation_key', true, $user_id, $user_email, $activation_key, $usermeta ) ) {
			bp_core_signup_send_validation_email( $user_id, $user_email, $activation_key, $user_login );
		}


	$bp->signup->username = $user_login;

	/**
	 * Fires at the end of the process to sign up a user.
	 *
	 * @since 1.2.2
	 *
	 * @param bool|WP_Error   $user_id       True on success, WP_Error on failure.
	 * @param string          $user_login    Login name requested by the user.
	 * @param string          $user_password Password requested by the user.
	 * @param string          $user_email    Email address requested by the user.
	 * @param array           $usermeta      Miscellaneous metadata about the user (blog-specific
	 *                                       signup data, xprofile data, etc).
	 */
	do_action( 'bp_core_signup_user', $user_id, $user_login, $user_password, $user_email, $usermeta );

	return $user_id;
}



add_action( 'bp_screens', 'dig_bp_core_screen_signup' ,1);

add_action('bp_signup_username_errors','dig_hide_bp_reg',1);
function dig_hide_bp_reg(){?>
    <input type="text" name="signup_username" id="username" value="<?php bp_signup_username_value(); ?>" <?php bp_form_field_attributes( 'username' ); ?> required/>


    <div id="dig_reg_bp_pass">
    <label for="signup_password"><?php _e( 'Choose a Password', 'buddypress' ); ?> <?php _e( '(required)', 'buddypress' ); ?></label>
    <?php

    /**
     * Fires and displays any member registration password errors.
     *
     * @since 1.1.0
     */
    do_action( 'bp_signup_password_errors' ); ?>
    <input type="password" name="signup_password" id="signup_password" value="" class="password-entry" <?php bp_form_field_attributes( 'password' ); ?> />
    <div id="pass-strength-result"></div>
    </div>
    <?php
    $digit_tapp = get_option('digit_tapp',1);
    if($digit_tapp!=1) {
        ?>
        <input type="hidden" name="dig_nounce" class="dig_nounce" value="<?php echo wp_create_nonce('dig_form') ?>">
        <div id="dig_bp_reg_otp">
            <label for="digit_ac_otp"><?php _e("OTP","digits");?> <span class="required">*</span></label>
            <input type="text" class="input-text" name="digit_ac_otp" id="digit_ac_otp"/>
        </div>
        <?php
    }

    echo "<div class=\"dig_bp_enb\" style='display:none;'>";

}


function dig_createUser($name,$mobileormail,$csrf,$code){

}

add_action('bp_account_details_fields','dig_bp_reg_end',1);
function dig_bp_reg_end(){
    echo "</div>";
}

add_action('bp_before_registration_submit_buttons','dig_bp_sub_reg',1);
function dig_bp_sub_reg(){

    ?>



    <input type="hidden" name="code" id="dig_bp_reg_code">
    <input type="hidden" name="csrf" id="dig_bp_reg_csrf">

    <div class="submit">

        <input type="submit" name="signup_submit" id="signup_submit_otp_bp" otp="1" value="<?php esc_attr_e( 'Sign Up With OTP', 'digits' ); ?>" />
        <div  class="dig_resendotp dig_wcbil_bill_resend" id="dig_man_resend_otp_btn" dis="1"><?php _e("Resend OTP","digits");?> <span>(00:<span><?php echo dig_getOtpTime(); ?></span>)</span></div>


        <input type="submit" name="signup_submit" id="signup_submit_pass_bp" value="<?php esc_attr_e( 'Sign Up With Password', 'digits' ); ?>" />
    </div>

    <div class="dig_bp_enb" style='display:none;'>
    <?php
}



add_action('bp_after_registration_submit_buttons','dig_bp_reg_end',1);


add_action('bp_core_general_settings_before_submit','addCurrentmobHidden');
function addCurrentmobHidden(){
    ?>

<label><?php _e("Mobile Number","digits");?></label>
<input type="text" name="bp_edit_user_mobile" id="username" mob="1"  countryCode="<?php echo esc_attr(get_the_author_meta('digt_countrycode', get_current_user_id())); ?>"
               value="<?php echo esc_attr(get_the_author_meta('digits_phone_no', get_current_user_id())); ?>" />


    <input type="hidden" name="dig_nounce" class="dig_nounce" value="<?php echo wp_create_nonce('dig_form') ?>">
    <input type="hidden" name="code" id="dig_bp_ea_code" />
    <input type="hidden" name="csrf" id="dig_bp_ea_csrf" />
    <?php if ( is_super_admin() ) : ?>
    <input type="hidden" id="dig_superadmin">
    <?php endif; ?>
    <input type="hidden" name="current_mob" id="dig_bp_current_mob" value="<?php echo esc_attr( get_the_author_meta( 'digits_phone_no', get_current_user_id() ) ); ?>" />
    <?php
    $digit_tapp = get_option('digit_tapp',1);
        if($digit_tapp!=1) {
        ?>
        <div id="bp_otp_dig_ea" style="display: none;"><label for="digit_ac_otp"><?php _e("OTP","digits");?> <span class="required">*</span></label>
            <input type="text" class="input-text" name="digit_ac_otp" id="digit_ac_otp"/>
            </div><?php
        }
}

add_action('bp_core_general_settings_after_submit','add_dig_otp_bp');
function add_dig_otp_bp(){

    $digit_tapp = get_option('digit_tapp',1);
        if($digit_tapp!=1) {
            echo "<div  class=\"dig_resendotp dig_bp_ac_ea_resend\" id=\"dig_man_resend_otp_btn\" style='text-align: inherit;' dis='1'>".__('Resend OTP','digits')." <span>(00:<span>".dig_getOtpTime()."</span>)</span></div>";
        }
}



add_action( 'bp_actions', 'dig_bp_settings_action_general' );
function dig_bp_settings_action_general(){

    if(isset($_POST['mobile/email']) && isset($_POST['dig_nounce'])){
        $phone = sanitize_text_field($_POST['mobile/email']);
        $countrycode = sanitize_text_field($_POST['digt_countrycode']);
        $digit_tapp = get_option('digit_tapp',1);
        if(empty($phone) || !is_numeric($phone))  return;

        $otp = sanitize_text_field($_POST['digit_ac_otp']);
        $code = sanitize_text_field($_POST['code']);
        $csrf = sanitize_text_field($_POST['csrf']);
        if ( !is_super_admin() ){
        if($digit_tapp==1){
            if(empty($code) || !wp_verify_nonce($csrf, 'crsf-otp')) return;
            $json = getUserPhoneFromAccountkit($code);
            $phoneJson = json_decode($json, true);
            if ($json == null) {$phone = 0;return;}

            $mob = $phoneJson['phone'];
            $phone = $phoneJson['nationalNumber'];
            $countrycode = $phoneJson['countrycode'];

        }else{
            if(empty($otp))return;
            if(verifyOTP($countrycode,$phone,$otp,true)){

                $mob = $countrycode.$phone;
            }else{
            $phone = 0;
            return;
            }
        }
        }

        $user = getUserFromPhone($mob);
        if($phone!=0 && $user==null){
         update_user_meta(get_current_user_id(), 'digt_countrycode', $countrycode);
         update_user_meta(get_current_user_id(), 'digits_phone_no', $phone);
         update_user_meta(get_current_user_id(), 'digits_phone', $countrycode . $phone);
         }

    }
}