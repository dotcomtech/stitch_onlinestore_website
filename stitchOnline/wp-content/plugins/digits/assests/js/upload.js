jQuery(function(){



    var tt;

    function updateimgkeyDown(){
        if(tt){
            clearTimeout(tt);
            tt = setTimeout( updateImgs, 600 );
        }else{
            tt = setTimeout( updateImgs, 600 );
        }
    }

    var dui;
    function updateImgs(){
        dui.trigger('change');
    }


    jQuery(".dig_preset").change(function () {
       if(jQuery(this).is(':checked')) {
           jQuery('.dig_selinp').hide().removeClass('dig_selinp');
           jQuery(this).closest('label').find('span').addClass('dig_selinp').show();

           var id = jQuery(this).attr('id');

           var preset = jQuery("#dig_" + id).val();
           var jsonobj = jQuery.parseJSON(preset);

           jQuery.each(jsonobj, function (i, va) {
               var c = jQuery("input[name="+i+"]");
               c.val(va).trigger('change');
               if(c.hasClass("bg_color")) c.iris('color',va);

           });
       }
    });

    function isUrlValid(url) {
        return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
    }
    jQuery(".dig_url_img").focusin(function(){
        dui = jQuery(this);
    });
    jQuery(".dig_url_img").on("keydown",function(){
        updateimgkeyDown();
    });
    jQuery(".dig_url_img").on("change",function(){
       var d = jQuery(this).parent().find("div");
       var v = jQuery(this).val();
        if(isUrlValid(v)) {
            d.find("img").attr("src", v);
            d.parent().find("input[type='button']").val(dig.removeimage);
        }else{
            d.find("img").removeAttr("src");
            d.parent().find("input[type='button']").val(dig.selectimage);
        }
    });
    var file_frame;
    var wp_media_post_id = wp.media.model.settings.post.id;
    var set_to_post_id = dig.logo;
    var uploadimagebutton = jQuery('#upload_image_button');
    uploadimagebutton.click(function() {

        var v = jQuery(this).parent().find(".dig_url_img").val();
        if(v.length>0){
            if(isUrlValid(v)) {
                jQuery('#image-preview').attr('src', "").trigger('change');
                jQuery('#image_attachment_id').val("").trigger('change');
                uploadimagebutton.val(dig.selectimage).trigger('change');

                return;
            }
        }
        if ( file_frame ) {
            file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
            file_frame.open();
            return;
        } else {
            wp.media.model.settings.post.id = set_to_post_id;
        }
        file_frame = wp.media.frames.file_frame = wp.media({
            title: dig.selectalogo,
            button: {text: dig.usethislogo},
            multiple: false
        });
        file_frame.on( 'select', function() {
            attachment = file_frame.state().get('selection').first().toJSON();

            wp.media.model.settings.post.id = wp_media_post_id;
            jQuery( '#image-preview' ).attr( 'src', attachment.url );
            jQuery( '#image_attachment_id' ).val( attachment.url ).trigger('change');
            uploadimagebutton.val(dig.removeimage).trigger('change');

        });
        file_frame.open();
    });



    var bg_file_frame;
    var bg_wp_media_post_id = wp.media.model.settings.post.id;
    var set_to_post_id = dig.logo;
    var bg_uploadimagebutton = jQuery('#bg_upload_image_button');
    bg_uploadimagebutton.click(function() {

        var v = jQuery(this).parent().find(".dig_url_img").val();

        if(v.length>0){
            if(isUrlValid(v)) {
                jQuery('#bg_image-preview').attr('src', "").trigger('change');
                jQuery('#bg_image_attachment_id').val("").trigger('change');
                bg_uploadimagebutton.val(dig.selectimage).trigger('change');

                return;
            }
        }
        if ( bg_file_frame ) {
            bg_file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
            bg_file_frame.open();
            return;
        } else {
            wp.media.model.settings.post.id = set_to_post_id;
        }
        bg_file_frame = wp.media.frames.bg_file_frame = wp.media({
            title: dig.selectalogo,
            button: {text: dig.usethislogo},
            multiple: false
        });
        bg_file_frame.on( 'select', function() {
            attachment = bg_file_frame.state().get('selection').first().toJSON();
            wp.media.model.settings.post.id = bg_wp_media_post_id;
            jQuery( '#bg_image-preview' ).attr( 'src', attachment.url ).trigger('change');
            jQuery( '#bg_image_attachment_id' ).val( attachment.url ).trigger('change');
            bg_uploadimagebutton.val(dig.removeimage).trigger('change');

        });
        bg_file_frame.open();
    });





    var bg_file_frame_modal;
    var bg_wp_media_post_id_modal = wp.media.model.settings.post.id;
    var set_to_post_id = dig.logo;
    var bg_uploadimagebutton_modal = jQuery('#bg_upload_image_button_modal');
    bg_uploadimagebutton_modal.click(function() {
        var v = jQuery(this).parent().find(".dig_url_img").val();
        if(v.length>0){
            if(isUrlValid(v)) {
                jQuery('#bg_image-preview_modal').attr('src', "").trigger('change');
                jQuery('#bg_image_attachment_id_modal').val("").trigger('change');
                bg_uploadimagebutton_modal.val(dig.selectimage).trigger('change');

                return;
            }
        }
        if ( bg_file_frame_modal ) {
            bg_file_frame_modal.uploader.uploader.param( 'post_id', set_to_post_id );
            bg_file_frame_modal.open();
            return;
        } else {
            wp.media.model.settings.post.id = set_to_post_id;
        }
        bg_file_frame_modal = wp.media.frames.bg_file_frame_modal = wp.media({
            title: dig.selectalogo,
            button: {text: dig.usethislogo},
            multiple: false
        });
        bg_file_frame_modal.on( 'select', function() {
            attachment = bg_file_frame_modal.state().get('selection').first().toJSON();
            wp.media.model.settings.post.id = bg_wp_media_post_id_modal;
            jQuery( '#bg_image-preview_modal' ).attr( 'src', attachment.url );
            jQuery( '#bg_image_attachment_id_modal' ).val( attachment.url ).trigger('change');
            bg_uploadimagebutton_modal.val(dig.removeimage).trigger('change');

        });
        bg_file_frame_modal.open();
    });


});