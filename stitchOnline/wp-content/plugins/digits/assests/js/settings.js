

jQuery(function() {


    var offs = -1;


    var sb_head = jQuery(".dig_sb_head");
    var das = jQuery(".dig_ad_side");
    var btn = jQuery(".dig_op_wdz_btn");

    var dig_fields_options_main = jQuery(".dig_fields_options_main");

    var dpc = jQuery('#dig_purchasecode');

    function showDigMessage(message){

        if(jQuery(".dig_popmessage").length){
            jQuery(".dig_popmessage").find(".dig_lase_message").text(message);
        }else {
            jQuery("body").append("<div class='dig_popmessage'><div class='dig_firele'><img src='"+ digsetobj.face + "'></div><div class='dig_lasele'><div class='dig_lase_snap'>"+digsetobj.ohsnap+"</div><div class='dig_lase_message'>" + message + "</div></div><img class='dig_popdismiss' src='"+ digsetobj.cross + "'></div>");
            jQuery(".dig_popmessage").slideDown('fast');
        }

    }
    function hideDigMessage(){
        jQuery(".dig_popmessage").remove();
    }

    jQuery(document).on("click", ".dig_popmessage", function() {

        jQuery(this).closest('.dig_popmessage').slideUp('fast', function() { jQuery(this).remove(); } );
    })

    jQuery(".updatetabview").click(function(){

        var c = jQuery(this).attr('tab');

        var acr = jQuery(this).attr('acr');

        if (typeof acr !== typeof undefined && acr !== false) {
            var inv = dpc.attr('invalid');
            if (dpc.val().length!=36 || inv==1) {

                showDigMessage(digsetobj.plsActMessage);
                if(jQuery("#dig_activatetab").length){
                    jQuery("#dig_activatetab").click();
                    dpc.focus();
                }
                return false;
            }
        }


        jQuery(".digcurrentactive").removeClass("digcurrentactive").hide();

        jQuery("."+c).show().addClass("digcurrentactive");


        jQuery(".dig-nav-tab-active").removeClass("dig-nav-tab-active");
        jQuery(this).addClass("dig-nav-tab-active");

        updateURL("tab",c.slice(0,-3));
        return false;
    });

    function updateURL(key,val){
        var url = window.location.href;
        var reExp = new RegExp("[\?|\&]"+key + "=[0-9a-zA-Z\_\+\-\|\.\,\;]*");

        if(reExp.test(url)) {
            // update
            var reExp = new RegExp("[\?&]" + key + "=([^&#]*)");
            var delimiter = reExp.exec(url)[0].charAt(0);
            url = url.replace(reExp, delimiter + key + "=" + val);
        } else {
            // add
            var newParam = key + "=" + val;
            if(!url.indexOf('?')){url += '?';}

            if(url.indexOf('#') > -1){
                var urlparts = url.split('#');
                url = urlparts[0] +  "&" + newParam +  (urlparts[1] ?  "#" +urlparts[1] : '');
            } else {
                url += "&" + newParam;
            }
        }
        window.history.pushState(null, document.title, url);
    }


    jQuery('.bg_color').wpColorPicker();


    var chn = false;
    jQuery(".digits_admim_conf textarea,.digits_admim_conf input").on('keyup',function(){
        if(!jQuery(this).attr("readonly"))
        enableSave();
    });
    jQuery(".digits_admim_conf input,.digits_admim_conf select,.dig_activation_form input").on('change',function(){
        enableSave();
    });

    jQuery("#dig_purchasecode").on('keyup',function(){
       if(jQuery(this).val().length==36 || jQuery(this).val().length==0){
           jQuery(".dig_prc_ver").hide();
           jQuery(".dig_prc_nover").hide();
       }else{
           invPC(-1);
       }
    });
    jQuery(".wp-color-picker").wpColorPicker(
        'option',
        'change',
        function(event, ui) {
            enableSave();
        }
    );

    function enableSave(){
        if(!chn){
            chn = true;
            jQuery(".dig_activation_form").find("input[type='submit']").removeAttr("disabled");
        }
    }
    jQuery("select").niceSelect();

    jQuery(".digits_shortcode_tbs").find("input").click(function(){

        copyShortcode(jQuery(this));
    });

    jQuery(".dig_copy_shortcode").click(function(){
       var a = jQuery(this).parent();
        var i = a.find("input");
        copyShortcode(i);
    });

    function copyShortcode(i){
        if(i.attr("nocop")) return;
        i.select();
        document.execCommand("copy");
        var v = i.val();
        i.val(digsetobj.Copiedtoclipboard);
        setTimeout(
            function() {
                i.val(v);
            }, 800);
    }

    jQuery('.dig_drop_doc_check').each(function( index ) {
        jQuery(this).click(function(){
            var a = jQuery(this).closest('li');
            a.find('.dig_conf_doc').toggle();
            var b = a.find('h2').find('.dig_tgb');
            b.text(b.text() == '+' ? '-' : '+');
        });

    });

    jQuery('#dig_purchasecode').mask('AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA');

    var digit_tapp = jQuery("#digit_tapp");

    var sgs = jQuery(".dig_load_overlay_gs");

    var se = sgs.length;




    jQuery(".dig_activation_form").on("submit",function(){



        hideDigMessage();

        var isOpt = false;
        var isPassdisEmailEnab = false;
        var dig_custom_field_login_j = jQuery(".dig_custom_field_login_j");
        if(dig_custom_field_login_j.length) {
            jQuery(".dig_custom_field_login_j").each(function (a, b) {
                var o = jQuery(this).attr('data-opt');
                var v = jQuery(this).val();
                if (o) {

                    if (v == 1) {
                        isOpt = true;
                        return true;
                    }
                }
                if (v == 0) {
                    var c = jQuery(this).attr('data-disable');

                    if (c) {

                        var ch = jQuery("select[name=" + c + "]").val();

                        if (ch == 1) {
                            isPassdisEmailEnab = true;
                        }
                    }
                }
            });

            if (!isOpt || isPassdisEmailEnab) {
                invPC();
                if (isPassdisEmailEnab) showDigMessage((digsetobj.cannotUseEmailWithoutPass));
                else if (!isOpt) showDigMessage(digsetobj.bothPassAndOTPCannotBeDisabled);
                return false;
            }
        }
        var fd = jQuery(this).serialize();

            sgs.find('.circle-loader').removeClass('load-complete');
            sgs.find('.checkmark').hide();
            sgs.fadeIn();

        var code = jQuery('#dig_purchasecode').val();
        if(code.length==0){

            jQuery(".dig_prc_ver").hide();
            jQuery(".dig_prc_nover").hide();

            updateSettings(fd,-1);
            return false;
        }else if(code.length!=36){
            showDigMessage(digsetobj.invalidpurchasecode);

            jQuery(".dig_prc_ver").hide();
            jQuery(".dig_prc_nover").show();
            updateSettings(fd,-1);
            return false;
        }


        jQuery.post('https://digits.unitedover.com/updates/verify.php',
            {
                code: code
            },function(data, status) {

                if (data != 1) {
                    invPC(se);
                    jQuery('#dig_purchasecode').attr('invalid', 1);
                } else {
                    jQuery(".dig_prc_ver").show();
                    jQuery(".dig_prc_nover").hide();
                    jQuery('#dig_purchasecode').attr('invalid', 0);

                }

                if (data == 0) {
                    showDigMessage(digsetobj.invalidpurchasecode);
                    if (!sgs.attr("ajxsu")) {
                        updateSettings(fd, -1);
                    }

                } else if (data == 1) {
                    if (sgs.attr("ajxsu")) {
                        jQuery(".dig_activation_form").unbind("submit").submit();
                    } else {
                        updateSettings(fd, 1);
                    }
                }
                else {
                    showDigMessage(digsetobj.Error);
                    if (!sgs.attr("ajxsu")) {
                        updateSettings(fd, -1);
                    }
                }
            }
        );



       return false;
    });


    function invPC(se){

        jQuery(".dig_prc_ver").hide();
        jQuery(".dig_prc_nover").show();
        if(se>0) sgs.hide();
    }
    function updateSettings(fd,activate){




        jQuery.ajax({
            type:    "POST",
            url:     digsetobj.ajax_url,
            data:    fd + '&action=digits_save_settings&pca='+ activate,
            success: function(data) {

                sgs.find('.circle-loader').addClass('load-complete');
                sgs.find('.checkmark').show();
                setTimeout(
                    function() {
                        sgs.fadeOut();
                        chn = false;
                        jQuery(".dig_activation_form").find("input[type='submit']").attr("disabled","disabled");

                    }, 1500);


            },
            error: function() {
                invPC();
                showDigMessage(digsetobj.Error);
            }
        });

    }
    jQuery("#digits_setting_update input[type='submit']").click(function(e){
        var val = digit_tapp.value;
        var te = digit_tapp.find("option:selected").attr('han');

        if(te=="msg91"){
            if(jQuery("#msg91senderid").val().length<6){
                showDigMessage(digsetobj.Invalidmsg91senderid);
                return false;
            }
        }

        jQuery("."+te+"cred").find("input").each(function(){
            var input = jQuery(this);
            if(input.val().length==0){
                var optional = input.attr('dig-optional');
                if(optional && optional==1) return;

                showDigMessage(digsetobj.PleasecompleteyourAPISettings);
                e.preventDefault();
                return false;

            }
        });
        return true;
    });



    var rtl = jQuery("#is_rtl");


    var select_field_type = jQuery(".dig_sb_select_field");

    var field_options = jQuery(".dig_fields_options");
    jQuery(document).on("click", ".dig_sb_field_types", function() {
        show_field_options(jQuery(this).attr('data-val'),jQuery(this).attr('data-configure_fields'),null);
    });





    var data_type = jQuery("#dig_custom_field_data_type");

    var dig_field_val_list = jQuery("#dig_field_val_list");

    var required_field_box = jQuery("#dig_field_required");
    var meta_key_box = jQuery("#dig_field_meta_key");
    var field_values = jQuery("#dig_field_options");
    var custom_class_box = jQuery("#dig_field_custom_class");


    var dig_field_label = jQuery("#dig_field_label");


    var isUpdate = false;
    var prevLabel;
    function show_field_options(type,options,values){
        isUpdate = false;
        show_create_new_field_panel();
        options = jQuery.parseJSON(options);


        
        sb_head.text(options.name);
        if(options.meta_key==1){
            meta_key_box.show();
        }else {
            meta_key_box.hide();
        }

        if(options.force_required==0){
            required_field_box.show();
        }else{
            required_field_box.hide();
        }

        if(options.options==1){
            field_values.show();
        }else{
            field_values.hide();
        }



        jQuery(".dig_sb_extr_fields").hide();
        if(options.slug!=null) jQuery(".dig_sb_field_"+options.slug).show();


        if(values!=null){
            isUpdate = true;
            prevLabel = values['label'];

            dig_field_label.find('input').val(values['label']);
            required_field_box.find('select').val(values['required']).niceSelect('update');
            meta_key_box.find('input').val(values['meta_key']);
            custom_class_box.find('input').val(values['custom_class']);
            var dropValues = values['options'].toString();

            dropValues = dropValues.split(',');
            dig_field_val_list.empty();


            if(options.slug!=null){
                jQuery(".dig_sb_field_"+options.slug).find('input').each(function(){
                    var name = jQuery(this).attr('name');
                    jQuery(this).val(values[name]);
                })
            }



            for (var i = 0; i < dropValues.length; i++) {
                addValueToValList(dropValues[i]);
            }
        }else{
            var m = options.name+'_'+ jQuery.now();
            dig_field_label.find('input').val('');
            required_field_box.find('select').val(1).change();
            meta_key_box.find('input').val(m.toLowerCase());
            custom_class_box.find('input').val('');
            dig_field_val_list.empty();
        }

        data_type.val(type);
        dig_fields_options_main.show();
        dig_cus_field_done.show();
        select_field_type.slideUp('fast');
        field_options.fadeIn('fast');
    }



    function addValueToValList(value){
        dig_field_val_list.append('<li></li>').find("li:last-child").text(value).append('<div class="dig_delete_opt_custf"></div>').show();
    }

    var dig_field_sidebar = jQuery(".dig_side_bar");
    var dig_custom_foot = jQuery("#dig_cus_field_footer");
    var dig_ad_cancel = jQuery(".dig_ad_cancel");
    dig_ad_cancel.on('click',function(){
       hide_custom_panel()
    });

    jQuery(".dig_sb_field_list").keypress(function(event) {
        if (event.keyCode == 13 ) {
            event.preventDefault();
            jQuery(this).trigger('focusout');
        }
    });


    jQuery(document).keyup(function(e) {
        hideDigMessage();
        if (e.keyCode == 27) {
            hide_custom_panel()
        }
    });


    var dig_sb_field = jQuery(".dig_sb_field");
    var dig_cus_field_done = jQuery(".dig_cus_field_done");

    dig_sb_field.find('input').keydown(function(e) {
        if (e.keyCode == 13 && !jQuery(this).hasClass('dig_sb_field_list')) {
            dig_cus_field_done.click();
            e.preventDefault();
            return false;
        }
    });

    function getFormData($form){
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};

        jQuery.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'].replace(/<(?:.|\n)*?>/gm, '');
        });


        return indexed_array;
    }




    var reg_custom_field_input = jQuery("#dig_reg_custom_field_data");


    var dig_custom_field_data = jQuery.parseJSON(reg_custom_field_input.val());

    var custom_field_table = jQuery("#dig_custom_field_table");
    var is_newfield;
    dig_cus_field_done.on('click',function () {
        var error_msg = false;

        dig_sb_field.each(function(){
            var sb_field = jQuery(this);
            if(sb_field.is(":visible")){
                if(sb_field.attr('data-req')==1){
                    var is_list = sb_field.attr('data-list');
                    if(is_list==1){

                        var sb_list = sb_field.find("ul");
                        if ( sb_list.find('li').length == 0 ){


                            error_msg = digsetobj.PleasecompleteyourCustomFieldSettings;
                            return false;

                        }

                    }else{
                        var sb_input = sb_field.find("input");
                        if(sb_input.length>0){
                            if(jQuery.trim(sb_input.val()).length==0){
                                error_msg = digsetobj.PleasecompleteyourCustomFieldSettings;
                                return false;
                            }
                        }
                    }



                }
            }
        });

        if(error_msg){
            showDigMessage(error_msg);return false;
        }

        var fields = getFormData(dig_field_sidebar.find("input,select"));

        var opt = [];
        dig_field_val_list.find("li").each(function() {
            var t = jQuery(this).text();
            opt.push(t.replace(/<(?:.|\n)*?>/gm, ''));
        });
        fields['options'] = opt;
        fields['type'] = data_type.val();



        if(!isUpdate && dig_custom_field_data.hasOwnProperty(fields['label'])){
            showDigMessage(digsetobj.fieldAlreadyExist);
            return false;
        }

        var dataString;
        if(isUpdate){
            dig_custom_field_data[prevLabel] = fields;
            dataString = JSON.stringify(dig_custom_field_data);
            dataString = dataString.replace('"'+prevLabel+'":{','"'+fields['label']+'":{');
            dig_custom_field_data = JSON.parse(dataString);

        }else {
            dig_custom_field_data[fields['label']] = fields;

            dataString = JSON.stringify(dig_custom_field_data);
        }
        reg_custom_field_input.val(dataString);
        hide_custom_panel();


        var row = '' +
            '<tr id="dig_cs_'+ removeSpacesAndLowerCase(fields['label']) +'" dig-lab="'+ fields['label'] +'">\n' +
            '            <th scope="row"><label>'+fields['label']+': </label></th>\n' +
            '            <td>\n' +
            '                <div class="dig_custom_field_list">\n' +
            requireToString(fields['required']) +
            '                    <div class="dig_icon_customfield">\n' +
            '                        <div class="icon-shape icon-shape-dims dig_cust_field_delete"></div>\n' +
            '                        <div class="icon-gear icon-gear-dims dig_cust_field_setting"></div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </td>\n' +
            '        </tr>' +
            '';


        if(isUpdate){
            jQuery('#dig_cs_'+ removeSpacesAndLowerCase(prevLabel)).replaceWith(row);
        }else custom_field_table.append(row);

        enableSave();

    })


    function removeSpacesAndLowerCase(str){
        str = jQuery.trim(str.replace(/\s/g, ''));
        return str.toLowerCase();
    }

    jQuery(document).on("click", ".dig_cust_field_setting", function() {
        var row = jQuery(this).closest('tr');
        var label = row.attr('dig-lab');
        var ftype = dig_custom_field_data[label]['type'];
        show_field_options(ftype,jQuery("#dig_cust_list_type_"+ftype).attr('data-configure_fields'),dig_custom_field_data[label]);
        enableSave();
    });




    jQuery(document).on("click", ".dig_delete_opt_custf", function() {
        jQuery(this).closest('li').remove();
    });



    jQuery(document).on("click", ".dig_cust_field_delete", function() {
        var row = jQuery(this).closest('tr');
        var label = row.attr('dig-lab');
        row.slideUp().remove();
        delete dig_custom_field_data[label];
        reg_custom_field_input.val(JSON.stringify(dig_custom_field_data));
        enableSave();
    });


    function requireToString(value){
        switch (value){
            case "0":
                return digsetobj.string_optional;
            case "1":
                return digsetobj.string_required;
            default:
                return null;
        }
    }



    jQuery("#dig_add_new_reg_field").click(function () {


        if(dig_field_sidebar.is(':visible') && !isUpdate) {
            dig_ad_cancel.trigger('click');
        }else {
            isUpdate = false;
            show_create_new_field_panel();
        }
    })

    function  show_create_new_field_panel() {
        sb_head.text(digsetobj.selectatype);
        select_field_type.show();
        dig_fields_options_main.hide();
        dig_cus_field_done.hide();
        dig_field_sidebar.show().animate({right: 0}, 'fast',function(){
        field_options.show();
            dig_custom_foot.show();

        });

    }

    function hide_custom_panel(){
        hideDigMessage();
        jQuery(".dig_sb_field_list").val('');
        var w = dig_field_sidebar.outerWidth(true);

        dig_custom_foot.hide();
        dig_field_sidebar.animate({right:-w},function () {
            dig_field_sidebar.hide();

        })
    }



    var el = document.getElementById('dig_field_val_list');
    if(el) {
        var sortable = Sortable.create(el);
    }

    jQuery(".dig_sb_field_list").focusout(function () {
        hideDigMessage();
        var optval = jQuery(this).val();
        var error = false;
        if(optval.length>0){
            dig_field_val_list.find("li").each(function() {
                if(jQuery(this).text()==optval) {
                    error = true;
                    return false;
                }

            });
            if(!error) {
                addValueToValList(optval);

                jQuery(this).val('');
                dig_field_sidebar.scrollTop(dig_field_sidebar[0].scrollHeight);
            }else{
                showDigMessage(digsetobj.duplicateValue);
            }


        }
    })






    jQuery("#digpassaccep").on('change',function(){

       var val = this.value;

        if(val==0) jQuery("#enabledisableforgotpasswordrow").hide();
        else jQuery("#enabledisableforgotpasswordrow").show();

    });
    digit_tapp.on('change', function() {
        var val = this.value;
        var te = digit_tapp.find("option:selected").attr('han');

        te = te.replace(".", "_");

        if(val==1 || val==13){
            jQuery(".disotp").each(function(){
                jQuery(this).hide();
            });
            jQuery(".dig_current_gateway").hide();
        }else{
            jQuery(".disotp").each(function(){
                jQuery(this).show();
            });
            jQuery(".dig_current_gateway").show().find("span").text(digit_tapp.find("option:selected").text());
        }


        digit_tapp.find('option').each(function(index,element){
            var hanc = jQuery(this).attr("han");
            if(hanc!=te) {
                jQuery("." + hanc + "cred").each(function () {
                    jQuery(this).hide().find("input").removeAttr("required");
                });

            }
        });
        jQuery("."+te+"cred").each(function(){
            var input = jQuery(this).show().find("input");
            var optional = input.attr('dig-optional');
            if(optional && optional==1) return;

            input.attr("required","required");
        });

    })




});
