=== WooCommerce Cost of Goods ===
Author: skyverge
Tags: woocommerce
Requires at least: 4.1
Tested up to: 4.8
WC requires at least: 2.5.5
WC tested up to: 3.1.1

A full-featured cost of goods management extension for WooCommerce, with detailed reporting for total cost and profit

See http://docs.woothemes.com/document/cost-of-goods/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-cost-of-goods' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
