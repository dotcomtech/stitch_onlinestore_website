<?php
/**
 * Plugin Name: WooCommerce Free Gift Coupons
 * Plugin URI: http://www.woothemes.com/products/free-gift-coupons/
 * Description: Add a free product to the cart when a coupon is entered
 * Version: 1.2.3
 * Author: Kathy Darling
 * Author URI: http://kathyisawesome.com
 * Requires at least: 3.9
 * Tested up to: 4.7.0
 * Requires at least WooCommerce: 2.4
 * WooCommerce tested up to: 3.1.0 
 *
 * Text Domain: wc_free_gift_coupons
 * Domain Path: /languages/
 *
 * @package WooCommerce Free Gift Coupons
 * @category Core
 * @author Kathy Darling
 *
 * Copyright: © 2012 Kathy Darling.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

if ( ! defined( 'ABSPATH' ) )
	exit; // Exit if accessed directly

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) )
	require_once( 'woo-includes/woo-functions.php' );

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), 'e1c4570bcc412b338635734be0536062', '414577' );

// Quit right now if WooCommerce is not active
if ( ! is_woocommerce_active() )
	return;

/**
 * Boot up the plugin
 *
 * Load a legacy version for WC<2.7
 *
 * @since   1.2.0
 */
function wc_free_gift_coupons_init() {
	if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0.0' ) >= 0 ) {
		require_once( 'includes/class-wc-free-gift-coupons.php' );
	} else {
		require_once( 'includes/class-wc-free-gift-coupons-legacy.php' );
	}
	WC_Free_Gift_Coupons::init();
}
add_action( 'woocommerce_loaded', 'wc_free_gift_coupons_init' );