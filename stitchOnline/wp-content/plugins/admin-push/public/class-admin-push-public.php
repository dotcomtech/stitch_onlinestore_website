<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://mstoreapp.com
 * @since      1.0.0
 *
 * @package    Admin_Push
 * @subpackage Admin_Push/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Admin_Push
 * @subpackage Admin_Push/public
 * @author     mStore App <support@mstoreapp.com>
 */
class Admin_Push_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Admin_Push_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Admin_Push_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/admin-push-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Admin_Push_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Admin_Push_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/admin-push-public.js', array( 'jquery' ), $this->version, false );

	}

	     public static function get_related_products() {

      $arr = $_REQUEST['related_ids'];
      $myArray = explode(',', $arr);


      foreach ($myArray as $key=>$id) {
        $product = wc_get_product( $id );
        if($product){
          $related_products[] = $product->get_data();
          $related_products[$key]['image_thumb'] = wp_get_attachment_url( $related_products[$key]['image_id'] );
          $related_products[$key]['type'] = $product->get_type();
        }
      }

      if(!$related_products){

       // $status = array();

        $status->error = 'empty';

        $status->message = 'No products!';

        wp_send_json( $myArray );

        die();

      }

      wp_send_json( $related_products );

      die();

    }

}
