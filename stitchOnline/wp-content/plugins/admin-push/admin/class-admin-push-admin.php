<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://mstoreapp.com
 * @since      1.0.0
 *
 * @package    Admin_Push
 * @subpackage Admin_Push/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Admin_Push
 * @subpackage Admin_Push/admin
 * @author     mStore App <support@mstoreapp.com>
 */
class Admin_Push_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Admin_Push_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Admin_Push_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/admin-push-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Admin_Push_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Admin_Push_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/admin-push-admin.js', array( 'jquery' ), $this->version, false );

	}

	 /**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function subscribeNotification() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Admin_Push_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Admin_Push_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		if (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])){

		    global $wpdb;

			$sql = "INSERT INTO {$wpdb->prefix}mstoreapp_admin_push (push_token,user_id,platform,new_order,new_customer,low_stock) VALUES (%s,%s,%s,%d,%d,%d) ON DUPLICATE KEY UPDATE platform = %s, new_order = %d, new_customer = %d, low_stock = %d";

			$prepare = $wpdb->prepare($sql,$_POST['push_token'],$_POST['user_id'],$_POST['platform'],$_POST['new_order'],$_POST['new_customer'],$_POST['low_stock'],$_POST['platform'],$_POST['new_order'],$_POST['new_customer'],$_POST['low_stock']);

			$result = $wpdb->query($prepare);

			//wp_send_json($result);

		}

		

	}

    // define the woocommerce_new_order callback
	public function newOrder($order_id) {

        if ( get_option('new_order') == 'true' ) {

	        global $wpdb;
	        $qry = "SELECT * FROM " . $wpdb->prefix . "mstoreapp_admin_push";
	        $result = $wpdb->get_results( $qry );

		        foreach ($result as $key => $value) {
		            if ($value->new_order) {

				$users[] = $value->user_id;

				}
			}

            $this->send_notification('New Order: #' . $order_id, 'You have new order', $users);
            $this->send_notification_android('New Order: #' . $order_id, 'You have new order', $users);


        }

    }

	// define the woocommerce_low_stock callback 
	public function lowStock( $product ) {

		if ( get_option('low_stock') == 'true' ) {

	        global $wpdb;
	        $qry = "SELECT * FROM " . $wpdb->prefix . "mstoreapp_admin_push";
	        $result = $wpdb->get_results( $qry );

	        foreach ($result as $key => $value) {
	            if ($value->low_stock) {

			    	$users[] = $value->user_id;

			    }
			}

		$this->send_notification('Low Stock: #' . $product->id, 'You have low stock item: ' . $product->name, $users);
		$this->send_notification_android('Low Stock: #' . $product->id, 'You have low stock item: ' . $product->name, $users);

	       
	    }    

	}

	// define the woocommerce_created_customer callback 
	public function newCustomer( $customer_id, $new_customer_data, $password_generated ) {

		if ( get_option('new_customer') == 'true' ) {

	        global $wpdb;
	        $qry = "SELECT * FROM " . $wpdb->prefix . "mstoreapp_admin_push";
	        $result = $wpdb->get_results( $qry );

	        foreach ($result as $key => $value) {
	            if ($value->new_customer) {

					$users[] = $value->user_id;

				}
			}


			$this->send_notification('New Customer: #' . $customer_id, 'You have new customer.', $users);
			$this->send_notification_android('New Customer: #' . $customer_id, 'You have new customer.', $users);

	    }    
	}

	// define the woocommerce_created_customer callback 
	public function settings_page() {
		add_options_page('Admin Push Settings', 'Push Settings', 'manage_options', 'admin-push', array(&$this, 'admin_push_options'));
	}

	function admin_push_options() {

	    if (!current_user_can('manage_options'))  {
	      wp_die( __('You do not have sufficient permissions to access this page.') );
	    }

	    ?>
			<div class="wrap">
			<h1>Admin Push Notifications Settings</h1>

			<form method="post" action="options.php">
			    <?php settings_fields( 'admin_push_settings' ); ?>
			    <?php do_settings_sections( 'admin_push_settings' ); ?>
			    <table class="form-table">

			<tr>       
            <th style="width:250px;"><label for="new_order">Send New Order Notifications</label></th>
            <td><select name="new_order" id="new_order">
            <option value="true" <?php if ( get_option('new_order') == 'true' ) echo 'selected="selected"'; ?>>Enable</option>
            <option value="false" <?php if ( get_option('new_order') == 'false' ) echo 'selected="selected"'; ?>>Disable</option>
            </select></td>
            </tr>

            <tr>       
            <th style="width:250px;"><label for="new_customer">Send New Customer Notifications</label></th>
            <td><select name="new_customer" id="new_customer">
            <option value="true" <?php if ( get_option('new_customer') == 'true' ) echo 'selected="selected"'; ?>>Enable</option>
            <option value="false" <?php if ( get_option('new_customer') == 'false' ) echo 'selected="selected"'; ?>>Disable</option>
            </select></td>
            </tr>

            <tr>       
            <th style="width:250px;"><label for="low_stock">Send Low Stock Notifications</label></th>
            <td><select name="low_stock" id="low_stock">
            <option value="true" <?php if ( get_option('low_stock') == 'true' ) echo 'selected="selected"'; ?>>Enable</option>
            <option value="false" <?php if ( get_option('low_stock') == 'false' ) echo 'selected="selected"'; ?>>Disable</option>
            </select></td>
            </tr>



           </table>

                 <?php submit_button(); ?>

			</form>
			</div>
        <?php

    }

    function register_admin_push_options() {
	  //register our settings
	  register_setting( 'admin_push_settings', 'new_order' );
	  register_setting( 'admin_push_settings', 'new_customer' );
	  register_setting( 'admin_push_settings', 'low_stock' );
    }

    function test() {


    }

    public function send_notification($heading, $title, $users) {

			//$fields['included_segments'] = array("All");

			$fields['include_player_ids'] = $users;

            $fields['headings'] = array("en" => $heading);
            $fields['contents'] = array("en" => $title);

            $fields['isAndroid'] = true;
            $fields['isIos'] = true;
            $fields['isAnyWeb'] = false;
            $fields['isWP'] = false;
            $fields['isAdm'] = false;
            $fields['isChrome'] = false;
            //$fields['data'] = array(
              //  "myappurl" => $fields['url']
            //);

            $ch = curl_init();
            $onesignal_post_url = "https://onesignal.com/api/v1/notifications";
            $onesignal_auth_key = 'NmYxM2JmN2EtNThlMS00NjU5LWE1OGQtNDMwN2MxZGYwOTZk';
            $fields['app_id'] = '681c176c-27a7-4101-aa05-2c7a7dfaeb83';// Bundle ID wooadmin01

            curl_setopt($ch, CURLOPT_URL, $onesignal_post_url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: Basic ' . $onesignal_auth_key
            ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $response = curl_exec($ch);
            
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($response, 0, $header_size);
            $body = json_decode(substr($response, $header_size), true);

            if(isset($body['id']))
            $status = 'success';
            if($body['errors'][0])
            $status = 'errors';

            curl_close($ch);
    }

    public function send_notification_android($heading, $title, $users) {

			//$fields['included_segments'] = array("All");

			$fields['include_player_ids'] = $users;

            $fields['headings'] = array("en" => $heading);
            $fields['contents'] = array("en" => $title);

            $fields['isAndroid'] = true;
            $fields['isIos'] = true;
            $fields['isAnyWeb'] = false;
            $fields['isWP'] = false;
            $fields['isAdm'] = false;
            $fields['isChrome'] = false;
            //$fields['data'] = array(
              //  "myappurl" => $fields['url']
            //);

            $ch = curl_init();
            $onesignal_post_url = "https://onesignal.com/api/v1/notifications";
            $onesignal_auth_key = 'NWExOTI3MGMtYTVlNy00ZjlhLWE5MjItMDIzZjZlZTk2MWYx';// Bundle ID wooadmin03
            $fields['app_id'] = '27ba240b-8d10-45ba-90a3-82ba4f8f2c43';// Bundle ID wooadmin03

            curl_setopt($ch, CURLOPT_URL, $onesignal_post_url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: Basic ' . $onesignal_auth_key
            ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $response = curl_exec($ch);
            
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($response, 0, $header_size);
            $body = json_decode(substr($response, $header_size), true);

            if(isset($body['id']))
            $status = 'success';
            if($body['errors'][0])
            $status = 'errors';

            curl_close($ch);
    }

     public function uploadimage() {

	      if ( ! function_exists( 'wp_handle_upload' ) ) {
	          require_once( ABSPATH . 'wp-admin/includes/file.php' );
	      }

	      $uploadedfile = $_FILES['file'];

	      $upload_overrides = array( 'test_form' => false );

	      $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

	      if ( $movefile && ! isset( $movefile['error'] ) ) {
	         // echo "File is valid, and was successfully uploaded.\n";
	          wp_send_json( $movefile );
	      } else {
	          /**
	           * Error generated by _wp_handle_upload()
	           * @see _wp_handle_upload() in wp-admin/includes/file.php
	           */
	          wp_send_json( $movefile );      }

	         // wp_send_json( $data );

	        die();
      }

}
