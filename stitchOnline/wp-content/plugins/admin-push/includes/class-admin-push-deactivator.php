<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://mstoreapp.com
 * @since      1.0.0
 *
 * @package    Admin_Push
 * @subpackage Admin_Push/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Admin_Push
 * @subpackage Admin_Push/includes
 * @author     mStore App <support@mstoreapp.com>
 */
class Admin_Push_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

		global $wpdb;

        // Drop Tables
        $wpdb->query( "DROP TABLE IF EXISTS " . $wpdb->prefix . "mstoreapp_admin_push" );

	}

}
