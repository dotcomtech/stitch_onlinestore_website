<?php

/**
 * Fired during plugin activation
 *
 * @link       http://mstoreapp.com
 * @since      1.0.0
 *
 * @package    Admin_Push
 * @subpackage Admin_Push/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Admin_Push
 * @subpackage Admin_Push/includes
 * @author     mStore App <support@mstoreapp.com>
 */
class Admin_Push_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		//***** Installer *****
		global $wpdb;
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');	

		$charset_collate = '';
		if (!empty($wpdb->charset)){
		    $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
		}else{
		    $charset_collate = "DEFAULT CHARSET=utf8";
		}
		if (!empty($wpdb->collate)){
		    $charset_collate .= " COLLATE $wpdb->collate";
		}

		$table_name = $wpdb->prefix . "mstoreapp_admin_push";
		        
		$lk_tbl_sql = "CREATE TABLE " . $table_name . " (
		        ID bigint(20) unsigned NOT NULL auto_increment,
		        push_token varchar(162) NOT NULL UNIQUE,
		        user_id varchar(162) NOT NULL default '',
		        platform varchar(10) NOT NULL default '',
		        new_order TINYINT NOT NULL default 1,
		        new_customer TINYINT NOT NULL default 1,
		        low_stock TINYINT NOT NULL default 1,
		        new_comment TINYINT NOT NULL default 0,
		        new_note TINYINT NOT NULL default 0,
		        order_cancelled TINYINT NOT NULL default 0,
		        order_failed TINYINT NOT NULL default 0,
		        order_pending TINYINT NOT NULL default 0,
		        payment_failed TINYINT NOT NULL default 0,
		        status varchar(20) NOT NULL default '',
		        PRIMARY KEY (ID)
		      )" . $charset_collate . ";";
		dbDelta($lk_tbl_sql);

	}

}
