<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://mstoreapp.com
 * @since             1.0.0
 * @package           Admin_Push
 *
 * @wordpress-plugin
 * Plugin Name:       Woo Admin Push Notification
 * Plugin URI:        http://mstoreapp.com
 * Description:       WooCommerce Extension to send push notification to iOS and Android devices in real time.
 * Version:           1.0.0
 * Author:            mStore App
 * Author URI:        http://mstoreapp.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       admin-push
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-admin-push-activator.php
 */
function activate_admin_push() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-admin-push-activator.php';
	Admin_Push_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-admin-push-deactivator.php
 */
function deactivate_admin_push() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-admin-push-deactivator.php';
	Admin_Push_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_admin_push' );
register_deactivation_hook( __FILE__, 'deactivate_admin_push' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-admin-push.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_admin_push() {

	$plugin = new Admin_Push();
	$plugin->run();

}
header("Access-Control-Allow-Origin: http://localhost:8080");
header("Access-Control-Allow-Credentials: true");
run_admin_push();
