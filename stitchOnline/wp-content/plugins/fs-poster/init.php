<?php
/*
 * Plugin Name: FS Poster
 * Description: Facebook, Twitter , Instagram, Linkedin, Reddit, Tumblr, VK, Pinterest Auto Poster Plugin. Post WooCommerce products. Schedule your posts i.e
 * Version: 1.9.3
 * Author: FS-Code
 * Author URI: https://www.fs-code.com
 * License: GNU General Public License v3 or later
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: fs-poster
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once "includes/Config.php";
require_once INCLUDES_DIR . 'CronJob.php';

add_action('registerSession', 'registerSession');

if( get_option('fs_poster_plugin_installed' , '0') )
{
	require_once "includes/PostMetaBox.php";
}

if( is_admin() )
{
	require_once "includes/AjaxClass.php";
	require_once "includes/ModalClass.php";
	require_once "includes/AdminMenuClass.php";



	add_action('admin_enqueue_scripts' , function()
	{
		wp_register_script('fs-code.js', plugins_url( 'js/fs-code.js', __FILE__ ) , array( 'jquery' ) , '1.8');
		wp_enqueue_script( 'fs-code.js' );

		wp_enqueue_style( 'fs-code.css', plugins_url('css/fs-code.css', __FILE__) );
		wp_enqueue_style( 'font_aweasome', '//use.fontawesome.com/releases/v5.0.13/css/all.css' );
	});
}
else
{
	require_once "includes/SiteController.php";
}

function my_plugin_load_plugin_textdomain() {
	load_plugin_textdomain( 'fs-poster', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'my_plugin_load_plugin_textdomain' );
