<?php

define('PLUGIN_DB_PREFIX' , 'fs_');
define('LIB_DIR' , __DIR__ . '/../lib/');
define('PLUGIN_URL' , plugins_url('/' , __DIR__));
define('INCLUDES_DIR' , __DIR__ . '/');
define('VIEWS_DIR' , __DIR__ . '/../views/');
define('FS_API_URL' , 'https://poster.fs-code.com/api/');

function calcDays( $date )
{
	$secs = strtotime($date);
	if( $secs < strtotime('01-01-2000') )
		return '~';

	$currentSec = time();

	return floor(($currentSec - $secs) / 60 / 60 / 24 / 30.4);
}

function response($status , $arr = [])
{
	$arr = is_array($arr) ? $arr : ( is_string($arr) ? ['error_msg' => $arr] : [] );

	if( $status )
	{
		$arr['status'] = 'ok';
	}
	else
	{
		$arr['status'] = 'error';
		if( !isset($arr['error_msg']) )
		{
			$arr['error_msg'] = 'Error!';
		}
	}

	print json_encode($arr);
	exit();
}

function modalView( $view , $parameters = [] )
{
	$mn = isset($_POST['_mn']) && is_numeric($_POST['_mn']) && $_POST['_mn'] > 0 ? (int)$_POST['_mn'] : 0;

	ob_start();
	require( plugin_dir_path( __FILE__ ) . '../views/modals/' . $view . '.php' );
	$viewOutput = ob_get_clean();

	response(true , [
		'html' => htmlspecialchars($viewOutput)
	]);
}

function wpDB()
{
	global $wpdb;

	return $wpdb;
}

function wpTable( $tbName )
{
	return wpDB()->base_prefix . PLUGIN_DB_PREFIX . $tbName;
}

function wpFetch( $table , $where = null )
{
	$whereQuery = '';
	$argss = [];
	$where = is_numeric($where) && $where > 0 ? [$where] : $where;
	if( !empty($where) && is_array($where) )
	{
		$whereQuery =  '';

		foreach($where AS $filed => $value)
		{
			$filed = $filed === 0 ? 'id' : $filed;
			$whereQuery .= ($whereQuery == '' ? '' : ' AND ') . $filed.'=%s';
			$argss[] = (string)$value;
		}

		$whereQuery =  ' WHERE ' . $whereQuery;
	}

	if( empty($argss) )
	{
		return wpDB()->get_row("SELECT * FROM " . wpTable($table) . $whereQuery ,ARRAY_A );
	}

	return wpDB()->get_row(
		wpDB()->prepare("SELECT * FROM " . wpTable($table) . $whereQuery , $argss)
		,ARRAY_A
	);

}

function wpFetchAll( $table , $where = null )
{
	$whereQuery = '';
	$argss = [];
	$where = is_numeric($where) && $where > 0 ? [$where] : $where;
	if( !empty($where) && is_array($where) )
	{
		$whereQuery =  '';

		foreach($where AS $filed => $value)
		{
			$filed = $filed === 0 ? 'id' : $filed;
			$whereQuery .= ($whereQuery == '' ? '' : ' AND ') . $filed.'=%s';
			$argss[] = (string)$value;
		}

		$whereQuery =  ' WHERE ' . $whereQuery;
	}

	if( empty($argss) )
	{
		return wpDB()->get_results("SELECT * FROM " . wpTable($table) . $whereQuery ,ARRAY_A );
	}

	return wpDB()->get_results(
		wpDB()->prepare("SELECT * FROM " . wpTable($table) . $whereQuery , $argss)
		,ARRAY_A
	);

}

function spintax( $text )
{
	$text = is_string($text) ? (string)$text : '';
	return preg_replace_callback(
		'/\{(((?>[^\{\}]+)|(?R))*)\}/x',
		function ($text)
		{
			$text = spintax( $text[1] );
			$parts = explode('|', $text);

			return $parts[ array_rand($parts) ];
		},
		$text
	);
}

function customizePostLink( $link , $feedId )
{
	if( strpos($link , '?') !== false )
	{
		$link .= '&feed_id=' . $feedId;
	}
	else
	{
		$link .= '?feed_id=' . $feedId;
	}

	return $link;
}

function getAccessToken( $nodeType , $nodeId )
{
	if( $nodeType == 'account' )
	{
		$nodeInf = wpFetch('accounts' , $nodeId);
		$nodeProfileId = $nodeInf['profile_id'];
		$accessTokenGet = wpFetch('account_access_tokens', ['account_id' => $nodeId]);
		$accessToken = $accessTokenGet['access_token'];
		$accessTokenSecret = $accessTokenGet['access_token_secret'];
		$appId = $accessTokenGet['app_id'];
		$driver = $nodeInf['driver'];

		if( $driver == 'reddit' && (time()+30) > strtotime($accessTokenGet['expires_on']) )
		{
			require_once LIB_DIR . 'reddit/Reddit.php';
			$accessToken = Reddit::refreshToken($accessTokenGet);
		}
	}
	else
	{
		$nodeInf = wpFetch('account_nodes' , $nodeId);
		$nodeProfileId = $nodeInf['node_id'];
		$driver = $nodeInf['driver'];
		$appId = 0;
		$accessTokenSecret = '';

		if( $driver == 'fb' && $nodeInf['node_type'] == 'ownpage' )
		{
			$accessToken = $nodeInf['access_token'];
		}
		else
		{
			$accessTokenGet = wpFetch('account_access_tokens', ['account_id' => $nodeInf['account_id']]);
			$accessToken = $accessTokenGet['access_token'];
			$accessTokenSecret = $accessTokenGet['access_token_secret'];
			$appId = $accessTokenGet['app_id'];
		}

		if( $driver == 'vk' )
		{
			$nodeProfileId = '-' . $nodeProfileId;
		}
	}

	return [
		'node_id'               =>  $nodeProfileId,
		'access_token'          =>  $accessToken,
		'access_token_secret'   =>  $accessTokenSecret,
		'app_id'                =>  $appId,
		'driver'                =>  $driver,
		'info'                  =>  $nodeInf
	];
}

function _post( $key , $default = null , $check_type = null , $whiteList = [] )
{
	$res = isset($_POST[$key]) ? $_POST[$key] : $default;

	if( !is_null( $check_type ) )
	{
		if( $check_type == 'num' || $check_type == 'int' || $check_type == 'integer' )
		{
			$res = is_numeric( $res ) ? (int)$res : $default;
		}
		else if($check_type == 'str' || $check_type == 'string')
		{
			$res = is_string( $res ) ? stripslashes_deep((string)$res) : $default;
		}
		else if($check_type == 'arr' || $check_type == 'array')
		{
			$res = is_array( $res ) ? stripslashes_deep((array)$res) : $default;
		}
		else if($check_type == 'float')
		{
			$res = is_numeric( $res ) ? (float)$res : $default;
		}
	}

	if( !empty( $whiteList ) && !in_array( $res , $whiteList ) )
	{
		$res = $default;
	}

	return $res;
}

function _get( $key , $default = null , $check_type = null , $whiteList = [] )
{
	$res = isset($_GET[$key]) ? $_GET[$key] : $default;

	if( !is_null( $check_type ) )
	{
		if( $check_type == 'num' || $check_type == 'int' || $check_type == 'integer' )
		{
			$res = is_numeric( $res ) ? (int)$res : $default;
		}
		else if($check_type == 'str' || $check_type == 'string')
		{
			$res = is_string( $res ) ? (string)$res : $default;
		}
		else if($check_type == 'arr' || $check_type == 'array')
		{
			$res = is_array( $res ) ? (array)$res : $default;
		}
		else if($check_type == 'float')
		{
			$res = is_numeric( $res ) ? (float)$res : $default;
		}
	}

	if( !empty( $whiteList ) && !in_array( $res , $whiteList ) )
	{
		$res = $default;
	}

	return $res;
}

function registerSession()
{
	if( !session_id() )
	{
		session_start();
	}
}

function end_session()
{
	session_destroy();
}

function checkPermission( $p )
{
	$permissions = ['public_profile' , 'publish_actions' , 'manage_pages' , 'publish_pages' , 'user_managed_groups' , 'pages_show_list'];

	$p2 = [];
	foreach($p['data'] AS $pName)
	{
		$p2[] = $pName['permission'];
	}

	$not = [];
	foreach($permissions AS $pName)
	{
		if( !in_array($pName , $p2) )
		{
			$not[] = esc_html($pName);
		}
	}

	if( !empty( $not ) )
	{
		return esc_html__('This app does not include certain permissions!' , 'fs-poster') . ' ( '.implode(' , ' , $not).' )';
	}

	return true;
}

function profilePic($info , $w = 40 , $h = 40)
{
	if( is_array($info) && key_exists('cover' , $info) ) // nodes
	{
		if( !empty($info['cover']) )
		{
			return $info['cover'];
		}
		else if( $info['driver'] == 'fb' )
		{
			return "https://graph.facebook.com/".esc_html($info['node_id'])."/picture?redirect=1&height={$h}&width={$w}&type=normal";
		}
		else if( $info['driver'] == 'tumblr' )
		{
			return "https://api.tumblr.com/v2/blog/".esc_html($info['node_id'])."/avatar/" . ($w > $h ? $w : $h);
		}
	}
	else if( $info['driver'] == 'fb' )
	{
		return "https://graph.facebook.com/".esc_html($info['profile_id'])."/picture?redirect=1&height={$h}&width={$w}&type=normal";
	}
	else if( $info['driver'] == 'twitter' )
	{
		require_once __DIR__ . '/../lib/twitter/autoload.php';
		static $twitterAppInfo;

		if( is_null($twitterAppInfo) )
		{
			$twitterAppInfo = wpFetch('apps' , ['driver' => 'twitter']);
		}

		$connection = new Abraham\TwitterOAuth\TwitterOAuth($twitterAppInfo['app_key'], $twitterAppInfo['app_secret']);
		$user = $connection->get("users/show", ['screen_name' => $info['username']]);
		return $user->profile_image_url;
	}
	else if( $info['driver'] == 'instagram' )
	{
		return $info['profile_pic'];
	}
	else if( $info['driver'] == 'linkedin' )
	{
		return $info['profile_pic'];
	}
	else if( $info['driver'] == 'vk' )
	{
		return $info['profile_pic'];
	}
	else if( $info['driver'] == 'pinterest' )
	{
		return $info['profile_pic'];
	}
	else if( $info['driver'] == 'google' )
	{
		return $info['profile_pic'];
	}
	else if( $info['driver'] == 'reddit' )
	{
		return $info['profile_pic'];
	}
	else if( $info['driver'] == 'tumblr' )
	{
		return "https://api.tumblr.com/v2/blog/".esc_html($info['username'])."/avatar/" . ($w > $h ? $w : $h);
	}
	else
	{

	}
}

function profileLink($info)
{
	// IF NODE
	if( is_array($info) && key_exists('cover' , $info) ) // nodes
	{
		if( $info['driver'] == 'fb' )
		{
			return "https://fb.com/".esc_html($info['node_id']);
		}
		else if( $info['driver'] == 'vk' )
		{
			return "https://vk.com/".esc_html($info['screen_name']);
		}
		else if( $info['driver'] == 'tumblr' )
		{
			return "https://" . esc_html($info['screen_name']) . ".tumblr.com";
		}
		else if( $info['driver'] == 'linkedin' )
		{
			return "https://www.linkedin.com/company/" . esc_html($info['node_id']);
		}

		return '';
	}

	if( $info['driver'] == 'fb' )
	{
		return "https://fb.com/".esc_html($info['profile_id']);
	}
	else if( $info['driver'] == 'twitter' )
	{
		return "https://twitter.com/".esc_html($info['username']);
	}
	else if( $info['driver'] == 'instagram' )
	{
		return "https://instagram.com/".esc_html($info['username']);
	}
	else if( $info['driver'] == 'linkedin' )
	{
		return "https://www.linkedin.com/in/".esc_html($info['username']);
	}
	else if( $info['driver'] == 'vk' )
	{
		return "https://vk.com/id" . esc_html($info['profile_id']);
	}
	else if( $info['driver'] == 'pinterest' )
	{
		return "https://www.pinterest.com/" . esc_html($info['username']);
	}
	else if( $info['driver'] == 'reddit' )
	{
		return "https://www.reddit.com/u/" . esc_html($info['username']);
	}
	else if( $info['driver'] == 'tumblr' )
	{
		return "https://" . esc_html($info['username']) . ".tumblr.com";
	}
	else if( $info['driver'] == 'google' )
	{
		return '';
	}
	else
	{

	}
}

function postLink( $postId , $driver , $username = '' )
{
	if( $driver == 'fb' )
	{
		return 'https://fb.com/' . $postId;
	}
	else if( $driver == 'twitter' )
	{
		return 'https://twitter.com/statuses/' . $postId;
	}
	else if( $driver == 'instagram' )
	{
		return 'https://www.instagram.com/p/' . $postId;
	}
	else if( $driver == 'linkedin' )
	{
		return 'https://www.linkedin.com/updates?topic=' . $postId;
	}
	else if( $driver == 'vk' )
	{
		return 'https://vk.com/wall' . $postId;
	}
	else if( $driver == 'pinterest' )
	{
		return 'https://www.pinterest.com/pin/' . $postId;
	}
	else if( $driver == 'reddit' )
	{
		return 'https://www.reddit.com/' . $postId;
	}
	else if( $driver == 'tumblr' )
	{
		return 'https://'.$username.'.tumblr.com/post/' . $postId;
	}
	else if( $driver == 'google' )
	{
		return 'https://www.linkedin.com/updates?topic=' . $postId;
	}
}

function cutText( $text , $n = 35 )
{
	return mb_strlen($text , 'UTF-8') > $n ? mb_substr($text , 0 , $n , 'UTF-8') . '...' : $text;
}

function appIcon( $appInfo )
{
	if( $appInfo['driver'] == 'fb' )
	{
		return "https://graph.facebook.com/".esc_html($appInfo['app_id'])."/picture?redirect=1&height=40&width=40&type=small";
	}
	else
	{
		return PLUGIN_URL . 'images/app_icon.svg';
	}
}

function replaceTags($message , $postInf , $link)
{
	return str_replace([
		'{title}' ,
		'{content_short}' ,
		'{content_full}' ,
		'{link}' ,
		'{product_regular_price}',
		'{product_sale_price}',
		'{uniq_id}',
		'{tags}',
		'{categories}'
	] , [
		strip_tags( $postInf['post_title'] ) ,
		cutText(strip_tags( $postInf['post_content'] ) , 40 ) ,
		strip_tags( $postInf['post_content'] ) ,
		$link ,
		get_post_meta( $postInf['ID'] , '_regular_price' , true ) ,
		get_post_meta( $postInf['ID'] , '_sale_price' , true ) ,
		uniqid(),
		getPostTags( $postInf['ID'] ),
		getPostCats( $postInf['ID'] )
	] , $message);
}

function standartFSAppRedirectURL($sn)
{
	return FS_API_URL . '?sn=' . $sn . '&r_url=' .urlencode(site_url() . '/?fs_app_redirect=1&sn=' . $sn);
}

function getPostTags( $postId )
{
	$tags = wp_get_post_tags( $postId );

	$tagsString = [];
	foreach( $tags AS $tagInf )
	{
		$tagsString[] = '#' . $tagInf->name;
	}
	$tagsString = implode(' ' , $tagsString);

	return $tagsString;
}


function getPostCats( $postId )
{
	$cats = get_the_category( $postId );

	$catsString = [];
	foreach( $cats AS $catInf )
	{
		$catsString[] = '#' . $catInf->name;
	}
	$catsString = implode(' ' , $catsString);

	return $catsString;
}

function shortenerURL( $url )
{
	if( !get_option('url_shortener', '0') )
	{
		return $url;
	}

	if( get_option('shortener_service') == 'tinyurl' )
	{
		return shortURLtinyurl( $url );
	}
	else if( get_option('shortener_service') == 'bitly' )
	{
		return shortURLbitly( $url );
	}

	return $url;
}

function shortURLtinyurl( $url )
{
	if( empty( $url ) )
	{
		return $url;
	}

	require_once LIB_DIR . 'Curl.php';

	$data = Curl::getURL('http://tinyurl.com/api-create.php?url=' . $url);

	return $data;
}

function shortURLbitly( $url )
{
	$params = array();

	$params['access_token'] = get_option('url_short_access_token_bitly');

	if( empty($params['access_token']) )
	{
		return $url;
	}

	$params['longUrl'] = $url;
	require_once LIB_DIR . 'bitly.php';

	$results = bitly_get('shorten', $params);

	return isset($results['data']['url']) && !empty($results['data']['url']) ? $results['data']['url'] : $url;
}

function checkRequirments()
{
	if( !ini_get('allow_url_fopen') )
	{
		response(false , esc_html__('"allow_url_fopen" disabled in your php.ini settings! Please actiavte id and try again!' , 'fs-poster'));
	}
}