<?php
defined('ABSPATH') or exit();

include __DIR__ . "/AjaxClass/Accounts.php";
include __DIR__ . "/AjaxClass/App.php";
include __DIR__ . "/AjaxClass/Nodes.php";
include __DIR__ . "/AjaxClass/Reports.php";
include __DIR__ . "/AjaxClass/Share.php";
include __DIR__ . "/AjaxClass/Schedule.php";
include __DIR__ . "/AjaxClass/Settings.php";

class AjaxClass
{
	use Accounts , App , Nodes , Reports , Share , Schedule , Settings;

	public function __construct()
	{
		$methods = get_class_methods($this);
		foreach ($methods AS $method)
		{
			if( $method == '__construct' )
			{
				continue;
			}

			add_action( 'wp_ajax_' . $method, function() use($method)
			{
				$this->$method();
				exit();
			});
		}
	}

	public function activate_app()
	{
		$code = _post('code' , '' , 'string');

		if( empty($code) )
		{
			response(false, 'Please type purchase key!');
		}

		if( get_option('fs_poster_plugin_installed' , '0') )
		{
			response(false , 'Your plugin also installed!');
		}
		
		/*
		require_once LIB_DIR . 'Curl.php';

		$checkPurchaseCodeURL = FS_API_URL . "api.php?purchase_code=" . $code . "&domain=" . site_url();
		$result2 = file_get_contents($checkPurchaseCodeURL);

		$result = json_decode($result2 , true);
		if( !is_array( $result ) )
		{
			// check requirments
			checkRequirments();

			if( empty( $result2 ) )
			{
				response( false , 'Your server can not access our license server via CURL! Our license server is "' . htmlspecialchars(FS_API_URL) . '". Please contact your hosting provider/server administrator and ask them to solve the problem. If you are sure that problem is not your server/hosting side then contact FS Poster administrators.' );
			}

			response(false , 'Installation error! Rosponse error! Response: ' . htmlspecialchars($result2));
		}

		if( !($result['status'] == 'ok' && isset($result['sql'])) )
		{
			response(false , isset($result['error_msg']) ? $result['error_msg'] : 'Error! Response: ' . htmlspecialchars($result2));
		}

		$sql = str_replace( '{tableprefix}' , (wpDB()->base_prefix . PLUGIN_DB_PREFIX) , base64_decode($result['sql']) );

		if( empty($sql) )
		{
			response(false , 'Error! Please contact the plugin administration! (info@fs-code.com)');
		}

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		foreach( explode(';' , $sql) AS $sqlQueryOne )
		{
			if( !empty( $sqlQueryOne ) )
			{
				wpDB()->query( $sqlQueryOne );
			}
		}*/

		update_option( 'fs_poster_plugin_installed', '1' );

		response(true);
	}

	public function fs_account_login()
	{
		$email = _post('email' , '' , 'string');
		$password = _post('password' , '' , 'string');
		if( !empty($email) && !empty($password) )
		{
			require_once LIB_DIR . "fb/FacebookLib.php";

			$getAppDetails = wpFetch('apps' , ['driver' => 'fb', 'is_standart' => '2']);

			if( !$getAppDetails )
			{
				response(false , ['error_msg' => esc_html__('No FB App found!' , 'fs-poster')]);
			}

			$url = FacebookLib::getLoginUrlWithAuth($email , $password , $getAppDetails['app_key'] , $getAppDetails['app_secret']);
			header('Location: ' . $url);
			exit();
		}

		print 'Error! Email or password is empty!';
	}

}

new AjaxClass();
