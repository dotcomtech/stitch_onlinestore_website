<?php


trait Schedule
{

	public function schedule_save()
	{
		$title = _post('title' , '' , 'string');
		$start_date = _post('start_date' , '' , 'string');
		$end_date = _post('end_date' , '' , 'string');
		$interval = _post('interval' , '0' , 'num');
		$share_time = _post('share_time' , '' , 'string');

		if( empty($title) || empty($start_date) || empty($end_date) || !in_array($interval , [1,2,3,4,5,6,7,8,9,10,1*24,2*24,3*24,4*24,5*24,6*24,7*24,8*24,9*24,10*24]) )
		{
			response(false , ['error_msg' => esc_html__('Validation error' , 'fs-poster')]);
		}

		$start_date = date('Y-m-d' , strtotime($start_date));
		$end_date = date('Y-m-d' , strtotime($end_date));
		$share_time = date('H:i' , strtotime($share_time));

		if( strtotime($start_date) > strtotime($end_date) )
		{
			response(false , ['error_msg' => esc_html__('Start date is wrong!' , 'fs-poster')]);
		}

		wpDB()->insert(wpTable('schedules') , [
			'title'         =>  $title,
			'start_date'    =>  $start_date,
			'end_date'      =>  $end_date,
			'interval'      =>  $interval,
			'status'        =>  'active',
			'insert_date'   =>  date('Y-m-d H:i:s'),
			'user_id'       =>  get_current_user_id(),
			'share_time'    =>  $share_time
		]);

		if( strtotime( $start_date ) < strtotime(date('Y-m-d')) )
		{
			$cronStartTime = date('Y-m-d');
		}
		else
		{
			$cronStartTime = $start_date;
		}

		if( $interval % 24 == 0 )
		{
			$cronStartTime .= ' ' . date('H:i' , strtotime($share_time));
		}
		else
		{
			$cronStartTime .= ' ' . date('H:i' );
		}

		CronJob::setScheduleTask( wpDB()->insert_id , $interval , $cronStartTime );

		response(true);
	}

	public function delete_schedule()
	{
		$id = _post('id' , 0 , 'num');
		if( $id <= 0 )
		{
			response(false);
		}

		$checkSchedule = wpFetch('schedules' , $id);
		if( !$checkSchedule )
		{
			response(false , esc_html__('Schedule not found!' , 'fs-poster'));
		}
		else if( $checkSchedule['user_id'] != get_current_user_id() )
		{
			response(false , esc_html__('You do not have a permission to delete this schedule!' , 'fs-poster'));
		}

		wpDB()->delete(wpTable('schedules') , ['id' => $id]);

		CronJob::clearSchedule($id);

		response(true);
	}

	public function schedule_change_status()
	{
		$id = _post('id' , 0 , 'num');

		if( $id <= 0 )
		{
			response(false);
		}

		$checkSchedule = wpFetch('schedules' , $id);
		if( !$checkSchedule )
		{
			response(false , esc_html__('Schedule not found!' , 'fs-poster'));
		}
		else if( $checkSchedule['user_id'] != get_current_user_id() )
		{
			response(false , esc_html__('You do not have a permission to Pause/Play this schedule!' , 'fs-poster'));
		}

		if( $checkSchedule['status'] != 'paused' && $checkSchedule['status'] != 'active' )
		{
			response(false , esc_html__('This schedule has finished!' , 'fs-poster'));
		}

		$newStatus = $checkSchedule['status'] == 'active' ? 'paused' : 'active';

		wpDB()->update(wpTable('schedules') , ['status' => $newStatus] , ['id' => $id]);

		response(true , ['a'=>$newStatus]);
	}

}