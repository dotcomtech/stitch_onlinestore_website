<?php

trait Nodes
{
	public function settings_node_activity_change()
	{
		if(!( isset($_POST['checked']) && ( $_POST['checked'] == '1' || $_POST['checked'] == '0')
			&& isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0))
		{
			response(false);
		}

		$id = (int)$_POST['id'];
		$checked = (int)$_POST['checked'];

		wpDB()->update(wpTable('account_nodes') , [
			'is_active' => $checked
		] , [
			'id' => $id,
			'user_id' => get_current_user_id()
		]);

		response(true);
	}

	public function settings_node_activity_change_all()
	{
		if(!( isset($_POST['checked']) && ( $_POST['checked'] == '1' || $_POST['checked'] == '0')
			&& isset($_POST['id']) && is_array($_POST['id']) && count($_POST['id']) > 0 && count($_POST['id']) < 1000))
		{
			response(false);
		}

		$id = (int)$_POST['id'];
		$checked = (int)$_POST['checked'];

		foreach ($_POST['id'] AS $id)
		{
			if( !is_numeric($id) || $id <= 0 )
			{
				continue;
			}

			wpDB()->update(wpTable('account_nodes') , [
				'is_active' => $checked
			] , [
				'id' => $id,
				'user_id' => get_current_user_id()
			]);
		}

		response(true);
	}

	public function settings_node_delete()
	{
		if(!( isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0 ))
		{
			response(false);
		}

		$id = (int)$_POST['id'];

		wpDB()->delete(wpTable('account_nodes') , [
			'id' => $id,
			'user_id' => get_current_user_id()
		]);

		response(true);
	}
}