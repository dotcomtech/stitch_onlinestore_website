<?php

trait Share
{
	public function share_post()
	{
		if( !(isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0) )
		{
			exit();
		}

		$feedId = (int)$_POST['id'];

		require_once LIB_DIR . 'SocialNetworkPost.php';

		$res = SocialNetworkPost::post($feedId);

		response(true, ['result' => $res]);
	}

	public function share_saved_post()
	{
		$postId = _post('post_id' , '0' , 'num');
		$postType = _post('post_type' , '' , 'string');
		$nodes = _post('nodes' , [] , 'array');
		$background = !(_post('background' , '0' , 'string')) ? 0 : 1;

		if( empty($postId) || empty($postType) || empty($nodes) || $postId <= 0 )
		{
			response(false);
		}

		$postInterval = (int)get_option('post_interval' , '3');

		foreach( $nodes AS $nodeId )
		{
			if( is_string($nodeId) && strpos( $nodeId , ':' ) !== false )
			{
				$parse = explode(':' , $nodeId);
				$driver = $parse[0];
				$nodeType = $parse[1];
				$nodeId = $parse[2];

				if( $driver == 'tumblr' && $nodeType == 'account' )
				{
					continue;
				}

				if( !( in_array( $nodeType , ['account' , 'ownpage' , 'page' , 'group' , 'event' , 'blog' , 'company'] ) && is_numeric($nodeId) && $nodeId > 0 ) )
				{
					continue;
				}

				if( !($driver == 'instagram' && get_option('instagram_post_in_type', '1') == '2') )
				{
					wpDB()->insert( wpTable('feeds'), [
						'driver'    =>  $driver,
						'post_id'   =>  $postId,
						'post_type' =>  $postType,
						'node_type' =>  $nodeType,
						'node_id'   =>  (int)$nodeId,
						'interval'  =>  $postInterval
					]);
				}

				if( $driver == 'instagram' && (get_option('instagram_post_in_type', '1') == '2' || get_option('instagram_post_in_type', '1') == '3') )
				{
					wpDB()->insert( wpTable('feeds'), [
						'driver'    =>  $driver,
						'post_id'   =>  $postId,
						'post_type' =>  $postType,
						'node_type' =>  $nodeType,
						'node_id'   =>  (int)$nodeId,
						'interval'  =>  $postInterval,
						'feed_type' =>  'story'
					]);
				}
			}
		}

		if( $background )
		{
			CronJob::setbackgroundTask( $postId );
		}

		response(true);
	}
}