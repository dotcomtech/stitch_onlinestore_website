<?php
defined('ABSPATH') or exit();

class PostMetaBox
{

	public function __construct()
	{
		// when post status changed ( saved, updated, scheduled and etc... )
		add_action( 'transition_post_status', [$this , 'onSave'], 10, 3 );

		// if is wp admin panel
		if( is_admin() )
		{
			// add meta boxes , columns, buttons ...

			add_action( 'add_meta_boxes', function()
			{
				add_meta_box( 'share_on_fb', 'FS Poster', [$this , 'publishMetaBox'], ['post','product','page'] , 'side' , 'high'  );
			});

			add_action( 'manage_posts_custom_column', function ( $column_name, $post_id )
			{
				if ( $column_name == 'share_btn' && get_post_status($post_id) == 'publish' )
				{
					printf( '<button type="button" class="button" data-load-modal="share_saved_post" data-parameter-post_id="%d" data-parameter-post_type="post">'.esc_html__('Share' , 'fs-poster').'</button>' , $post_id );
				}
			}, 10, 2 );
			add_filter('manage_posts_columns', function ( $columns )
			{
				if( is_array( $columns ) && ! isset( $columns['share_btn'] ) )
				{
					$columns['share_btn'] = esc_html__('Share post' , 'fs-poster');
				}

				return $columns;
			} );

			add_action( 'manage_pages_custom_column', function ( $column_name, $post_id )
			{
				if ( $column_name == 'share_btn' && get_post_status($post_id) == 'publish')
				{
					printf( '<button type="button" class="button" data-load-modal="share_saved_post" data-parameter-post_id="%d" data-parameter-post_type="post">'.esc_html__('Share' , 'fs-poster').'</button>' , $post_id );
				}
			}, 10, 2 );
			add_filter('manage_pages_columns', function ( $columns )
			{
				if( is_array( $columns ) && ! isset( $columns['share_btn'] ) )
				{
					$columns['share_btn'] = esc_html__('Share post' , 'fs-poster');
				}

				return $columns;
			} );

			add_action( 'manage_media_custom_column', function ( $column_name, $post_id )
			{
				if ( $column_name == 'share_btn' && get_post_status($post_id) == 'publish')
				{
					printf( '<button type="button" class="button" data-load-modal="share_saved_post" data-parameter-post_id="%d" data-parameter-post_type="media">'.esc_html__('Share' , 'fs-poster').'</button>' , $post_id );
				}
			}, 10, 2 );
			add_filter('manage_media_columns', function ( $columns )
			{
				if( is_array( $columns ) && ! isset( $columns['share_btn'] ) )
				{
					$columns['share_btn'] = esc_html__('Share post' , 'fs-poster');
				}

				return $columns;
			} );
		}
	}

	function publishMetaBox( $post )
	{
		// post creating panel
		if( in_array( $post->post_status , ['new' , 'auto-draft' , 'draft' , 'pending'] ) )
		{
			$postId = $post->ID;
			$postType = 'post';
			require_once VIEWS_DIR . "post_meta_box.php";
		}
		else // post edit panel
		{
			require_once VIEWS_DIR . "post_meta_box_edit.php";
		}
	}

	function onSave( $new_status, $old_status, $post )
	{
		if( !( ($new_status == 'publish' || $new_status == 'future' || $new_status == 'draft') && $old_status != 'publish' ) )
		{
			return;
		}

		$post_id = $post->ID;

		// if scheduled post, publish it using cron and exit the function
		if( $new_status == 'publish' && $old_status == 'future' )
		{
			$checkFeedsExist = wpFetch('feeds' , [
				'post_id'       =>  $post_id,
				'is_sended'     =>  '0'
			]);

			if( $checkFeedsExist )
			{
				CronJob::setbackgroundTask( $post_id );
			}

			return;
		}

		// if not checked the 'Shear' checkbox exit the function
		$share_checked = _post('share_checked' , 'on' , 'string' , ['on' , 'off']) === 'on' ? 1 : 0;
		if( !$share_checked )
		{
			return;
		}

		// interval for each publication
		$postInterval = (int)get_option('post_interval' , '1');

		// run share process on background
		$backgroundShare = (int)get_option('fs_share_on_background' , '1');

		// social networks lists
		$nodesList = _post('share_on_nodes' , false , 'array' );

		// if false, may be from xmlrpc, external application or etc... then load ol active nodes
		if( $nodesList === false )
		{
			$nodesList = [];

			$accounts = wpDB()->get_results(
				wpDB()->prepare("SELECT id,driver,'account' AS node_type FROM " . wpTable('accounts') . " WHERE is_active='1' AND user_id=%d ORDER BY name" , [get_current_user_id()])
				, ARRAY_A
			);

			$activeNodes = wpDB()->get_results(
				wpDB()->prepare("SELECT id,driver,node_type FROM " . wpTable('account_nodes') . " WHERE is_active='1' AND user_id=%d ORDER BY (CASE node_type WHEN 'ownpage' THEN 1 WHEN 'group' THEN 2 WHEN 'page' THEN 3 END), name" , [get_current_user_id()])
				, ARRAY_A
			);
			$activeNodes = array_merge($accounts , $activeNodes);

			foreach ($activeNodes AS $nodeInf)
			{
				$nodesList[] = $nodeInf['driver'].':'.$nodeInf['node_type'].':'.$nodeInf['id'];
			}
		}

		// if edited scheduled post then delete old feeds, and insert new selected
		if( $new_status != 'publish' )
		{
			wpDB()->delete(wpTable('feeds') , [
				'post_id'       =>  $post_id,
				'is_sended'     =>  '0'
			]);
		}

		foreach( $nodesList AS $nodeId )
		{
			if( is_string($nodeId) && strpos( $nodeId , ':' ) !== false )
			{
				$parse = explode(':' , $nodeId);
				$driver = $parse[0];
				$nodeType = $parse[1];
				$nodeId = $parse[2];

				if( $driver == 'tumblr' && $nodeType == 'account' )
				{
					continue;
				}

				if( !( in_array( $nodeType , ['account' , 'ownpage' , 'page' , 'group' , 'event' , 'blog' , 'company'] ) && is_numeric($nodeId) && $nodeId > 0 ) )
				{
					continue;
				}

				if( !($driver == 'instagram' && get_option('instagram_post_in_type', '1') == '2') )
				{
					wpDB()->insert( wpTable('feeds'), [
						'driver'            =>  $driver,
						'post_id'           =>  $post_id,
						'post_type'         =>  'post',
						'node_type'         =>  $nodeType,
						'node_id'           =>  (int)$nodeId,
						'interval'          =>  $postInterval
					]);
				}

				if( $driver == 'instagram' && (get_option('instagram_post_in_type', '1') == '2' || get_option('instagram_post_in_type', '1') == '3') )
				{
					wpDB()->insert( wpTable('feeds'), [
						'driver'            =>  $driver,
						'post_id'           =>  $post_id,
						'post_type'         =>  'post',
						'node_type'         =>  $nodeType,
						'node_id'           =>  (int)$nodeId,
						'interval'          =>  $postInterval,
						'feed_type'         =>  'story'
					]);
				}
			}
		}

		// if backround process activated then create a new cron job
		if( $backgroundShare && $new_status == 'publish' )
		{
			CronJob::setbackgroundTask( $post_id );
		}

		// if not scheduled post then add arguments end of url
		if( $new_status == 'publish' )
		{
			add_filter('redirect_post_location', function($location) use( $backgroundShare )
			{
				return $location . '&share=1&background=' . $backgroundShare;
			});
		}

	}

}

// trigger class
new PostMetaBox();
