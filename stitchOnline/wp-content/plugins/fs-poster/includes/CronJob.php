<?php

add_action( 'check_scheduled_posts' , ['CronJob' , 'scheduledPost'] );
add_action( 'check_background_shared_posts' , ['CronJob' , 'sendPostBackground'] );

CronJob::createScheduleTime();

class CronJob
{

	public static function setScheduleTask( $scheduleId , $interval , $startTime )
	{
		wp_schedule_event( strtotime($startTime), $interval.'hour', 'check_scheduled_posts' , [ $scheduleId ] );
	}

	public static function setbackgroundTask( $postId )
	{
		wp_schedule_single_event( time(),  'check_background_shared_posts' , [ $postId ] );
	}

	public static function clearSchedule($scheduleId)
	{
		wp_clear_scheduled_hook( 'check_scheduled_posts' , [ $scheduleId ] );
	}

	public static function createScheduleTime( )
	{
		add_filter('cron_schedules', function($schedules)
		{
			for($h = 1; $h <= 10; $h++)
			{
				$schedules[$h . "hour"] = array(
					'interval'  => 3600 * $h,
					'display'   => 'Once every ' . $h . ' hour'
				);

				$schedules[($h * 24) . "hour"] = array(
					'interval'  => 3600 * $h * 24,
					'display'   => 'Once every ' . ($h*24) . ' hour'
				);
			}

			return $schedules;
		});
	}

	public static function scheduledPost( $scheduleId )
	{
		$scheduleInf = wpFetch('schedules' , $scheduleId);
		$userId = $scheduleInf['user_id'];

		$interval = $scheduleInf['intarvel'];
		$endDate = strtotime($scheduleInf['end_date']);

		if( strtotime(date('Y-m-d' , ( time() + $interval * 3600 ))) > $endDate )
		{
			wp_clear_scheduled_hook( 'check_scheduled_posts' , [$scheduleId] );
			wpDB()->update(wpTable('schedules') , ['status' => 'finished'] , ['id' => $scheduleId]);
		}

		if( $scheduleInf['status'] != 'active' )
		{
			return;
		}

		$getRandomPost = wpDB()->get_row("SELECT * FROM ".wpDB()->base_prefix."posts WHERE post_status='publish' AND post_type IN ('post' , 'page') ORDER BY RAND() LIMIT 1" , ARRAY_A);
		$postId = $getRandomPost['ID'];

		$getActiveAccounts = wpFetchAll('accounts' , ['user_id' => $userId , 'is_active' => '1']);
		$getActiveNodes = wpFetchAll('account_nodes' , ['user_id' => $userId , 'is_active' => '1']);

		$feedsArr = [];

		$postInterval = 1;

		foreach( $getActiveAccounts AS $accountInf )
		{
			if( !($accountInf['driver'] == 'instagram' && get_option('instagram_post_in_type', '1') == '2') )
			{
				wpDB()->insert( wpTable('feeds'), [
					'driver'        =>  $accountInf['driver'],
					'post_id'       =>  $postId,
					'post_type'     =>  'post',
					'node_type'     =>  'account',
					'node_id'       =>  (int)$accountInf['id'],
					'interval'      =>  $postInterval,
					'schedule_id'   =>  $scheduleId
				]);
			}

			$feedsArr[] = wpDB()->insert_id;

			if( $accountInf['driver'] == 'instagram' && (get_option('instagram_post_in_type', '1') == '2' || get_option('instagram_post_in_type', '1') == '3') )
			{
				wpDB()->insert( wpTable('feeds'), [
					'driver'        =>  $accountInf['driver'],
					'post_id'       =>  $postId,
					'post_type'     =>  'post',
					'node_type'     =>  'account',
					'node_id'       =>  (int)$accountInf['id'],
					'interval'      =>  $postInterval,
					'schedule_id'   =>  $scheduleId,
					'feed_type' =>  'story'
				]);
			}

			$feedsArr[] = wpDB()->insert_id;
		}

		foreach( $getActiveNodes AS $nodeInf )
		{
			wpDB()->insert( wpTable('feeds'), [
				'driver'        =>  $nodeInf['driver'],
				'post_id'       =>  $postId,
				'post_type'     =>  'post',
				'node_type'     =>  $nodeInf['node_type'],
				'node_id'       =>  (int)$nodeInf['id'],
				'interval'      =>  $postInterval,
				'schedule_id'   =>  $scheduleId
			]);

			$feedsArr[] = wpDB()->insert_id;
		}

		set_time_limit(0);
		require_once LIB_DIR . 'SocialNetworkPost.php';

		foreach ($feedsArr AS $feedId)
		{
			SocialNetworkPost::post( $feedId );
			sleep($postInterval);
		}
	}

	public static function sendPostBackground( $postId )
	{
		$getFeeds = wpFetchAll('feeds' , ['post_id' => $postId , 'is_sended' => '0']);
		set_time_limit(0);
		require_once LIB_DIR . 'SocialNetworkPost.php';

		foreach ($getFeeds AS $feedInf)
		{
			SocialNetworkPost::post( $feedInf['id'] );

			if( is_numeric($feedInf['interval']) && $feedInf['interval'] > 0 )
			{
				sleep($feedInf['interval']);
			}
		}
	}

}


