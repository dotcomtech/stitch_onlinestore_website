<?php

class SocialNetworkPost
{

	public static function post($feedId)
	{
		$feedInf = wpFetch("feeds" , $feedId);

		$postId = $feedInf['post_id'];
		$feedType = $feedInf['post_type'];

		$nInf = getAccessToken($feedInf['node_type'] , $feedInf['node_id']);
		$nodeProfileId = $nInf['node_id'];
		$appId = $nInf['app_id'];
		$driver = $nInf['driver'];
		$accessToken = $nInf['access_token'];
		$accessTokenSecret = $nInf['access_token_secret'];

		$link = '';
		$message = '';
		$sendType = 'status';
		$images = null;
		$imagesLocale = null;
		$videoURL = null;
		$postTitle = '';
		$videoURLLocale = null;

		switch( $feedType )
		{
			case 'post':
			case 'media':
				$postInf = get_post($postId , ARRAY_A);
				$postType = $postInf['post_type'];

				if( $driver == 'reddit' )
				{
					$postTitle = $postInf['post_title'];
				}

				if( $postType == 'attachment' && strpos($postInf['post_mime_type'] , 'image') !== false )
				{
					$sendType = 'image';
					$images[] = $postInf['guid'];
					$imagesLocale[] = get_attached_file( $postId );

				}
				else if( $postType == 'attachment' && strpos($postInf['post_mime_type'] , 'video') !== false )
				{
					$sendType = 'video';
					$videoURL = $postInf['guid'];
					$videoURLLocale = get_attached_file( $postId );
				}
				else
				{
					$sendType = 'link';
				}

				$link = get_permalink($postInf['ID']);
				break;
		}

		$link = customizePostLink($link , $feedId);

		if( get_option('unique_link', '1') == 1 && !empty($link) )
		{
			$link .= ( strpos($link , '?') === false ? '?' : '&' ) . '_unique_id=' . uniqid();
		}

		$message = replaceTags( get_option('post_text_message_' . $driver, "{title}") , $postInf , $link);

		$link = shortenerURL( $link );

		if( $driver == 'fb' )
		{
			require_once LIB_DIR . "fb/FacebookLib.php";
			$res = FacebookLib::sendPost($nodeProfileId , $sendType , $message , 0 , $link , $images , $videoURL , $accessToken);
		}
		else if( $driver == 'instagram' )
		{
			require_once LIB_DIR . "instagram/Instagram.php";

			if( $sendType != 'image' && $sendType != 'video' )
			{
				$mediaId = get_post_thumbnail_id($postId);

				if( empty($mediaId) )
				{
					$media = get_attached_media( 'image' , $postId);
					$first = reset($media);
					$mediaId = isset($first->ID) ? $first->ID : 0;
				}

				$url = $mediaId > 0 ? get_attached_file($mediaId) : '';

				if( !empty($url) )
				{
					$sendType = 'image';
					$imagesLocale = [$url];
				}
			}

			if( $feedInf['feed_type'] == 'story' )
			{
				$res = Instagram::sendStory($nInf['info'] , $sendType , $message , $link , $imagesLocale , $videoURLLocale);
			}
			else
			{
				$res = Instagram::sendPost($nInf['info'] , $sendType , $message , $link , $imagesLocale , $videoURLLocale);
			}
		}
		else if( $driver == 'linkedin' )
		{
			require_once LIB_DIR . "linkedin/Linkedin.php";
			$res = Linkedin::sendPost($nInf['info'] , $sendType , $message , $link , $images , $videoURL , $accessToken);
		}
		else if( $driver == 'vk' )
		{
			require_once LIB_DIR . "vk/Vk.php";
			$res = Vk::sendPost($nodeProfileId , $sendType , $message , $link , $imagesLocale , $videoURLLocale , $accessToken);
		}
		else if( $driver == 'pinterest' )
		{
			if( $sendType != 'image' && $sendType != 'video' )
			{
				$mediaId = get_post_thumbnail_id($postId);

				if( empty($mediaId) )
				{
					$media = get_attached_media( 'image' , $postId);
					$first = reset($media);
					$mediaId = isset($first->ID) ? $first->ID : 0;
				}

				$url = $mediaId > 0 ? wp_get_attachment_url($mediaId) : '';

				if( !empty($url) )
				{
					$sendType = 'image';
					$images = [$url];
				}
			}

			require_once LIB_DIR . "pinterest/Pinterest.php";
			$res = Pinterest::sendPost($nInf['info'] , $sendType , $message , $link , $images , $videoURL , $accessToken);
		}
		else if( $driver == 'reddit' )
		{
			require_once LIB_DIR . "reddit/Reddit.php";
			$res = Reddit::sendPost($nInf['info'] , $sendType , $postTitle , $message , $link , $images , $videoURL , $accessToken);
		}
		else if( $driver == 'tumblr' )
		{
			require_once LIB_DIR . "tumblr/Tumblr.php";
			$res = Tumblr::sendPost($nInf['info'] , $sendType , $postTitle , $message , $link , $imagesLocale , $videoURLLocale , $accessToken , $accessTokenSecret , $appId);
		}
		else if( $driver == 'twitter' )
		{
			if( $sendType != 'image' && $sendType != 'video' )
			{
				$mediaId = get_post_thumbnail_id($postId);

				if( empty($mediaId) )
				{
					$media = get_attached_media( 'image' , $postId);
					$first = reset($media);
					$mediaId = isset($first->ID) ? $first->ID : 0;
				}

				$url = $mediaId > 0 ? get_attached_file($mediaId) : '';

				if( !empty($url) )
				{
					$imagesLocale = [$url];
				}
			}

			require_once LIB_DIR . "twitter/TwitterLib.php";
			$res = TwitterLib::sendPost($appId , $sendType , $message , $link , $imagesLocale , $videoURLLocale , $accessToken , $accessTokenSecret);
		}
		else
		{
			$res = ['status' => 'error' , 'error_msg' => 'Driver error! Driver type: ' . htmlspecialchars($driver)];
		}

		$udpateDate = [
			'is_sended'         => 1,
			'send_time'         => date('Y-m-d H:i:s'),
			'status'            => $res['status'],
			'error_msg'         => isset($res['error_msg']) ? $res['error_msg'] : '',
			'driver_post_id'    => isset($res['id']) ? $res['id'] : null,
			'driver_post_id2'   => isset($res['id2']) ? $res['id2'] : null
		];

		wpDB()->update(wpTable('feeds') , $udpateDate , ['id' => $feedId]);

		if( isset($res['id']) )
		{
			$username = isset($nInf['info']['screen_name']) ? $nInf['info']['screen_name'] : $nInf['info']['username'];
			$res['post_link'] = postLink($res['id'] , $driver , $username);
		}

		return $res;
	}

}