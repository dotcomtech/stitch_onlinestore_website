<?php

require_once __DIR__ . '/../Curl.php';

class Vk
{

	public static function callbackURL()
	{
		return site_url() . '?vk_callback=1';
	}

	public static function authorizeVkUser( $appId , $accessToken )
	{
		$me = self::cmd('users.get', 'GET' , $accessToken , ['fields' => 'id,first_name,last_name,screen_name, sex, bdate,photo,followers_count,common_count'] );

		if( isset($me['error']) )
		{
			return $me;
		}

		$me = reset($me);
		$meId = $me['id'];

		$checkLoginRegistered = wpFetch('accounts' , ['user_id' => get_current_user_id() , 'driver' => 'vk', 'profile_id' => $meId]);

		$dataSQL = [
			'user_id'			=>	get_current_user_id(),
			'name'		  		=>	$me['first_name'] .' ' . $me['last_name'],
			'driver'			=>	'vk',
			'profile_id'		=>	$meId,
			'gender'			=>	$me['sex'],
			'birthday'			=>	date('Y-m-d' , strtotime($me['bdate'])),
			'profile_pic'		=>	$me['photo'],
			'followers_count'	=>	$me['followers_count'],
			'friends_count'		=>	$me['common_count'],
			'username'			=>	$me['screen_name']
		];

		if( !$checkLoginRegistered )
		{
			wpDB()->insert(wpTable('accounts') , $dataSQL);

			$accId = wpDB()->insert_id;
		}
		else
		{
			$accId = $checkLoginRegistered['id'];

			wpDB()->update(wpTable('accounts') , $dataSQL , ['id' => $accId]);

			wpDB()->delete( wpTable('account_access_tokens')  , ['account_id' => $accId , 'app_id' => $appId] );

			wpDB()->delete( wpTable('account_nodes')  , ['account_id' => $accId] );
		}

		// acccess token
		wpDB()->insert( wpTable('account_access_tokens') ,  [
			'account_id'	=>	$accId,
			'app_id'		=>	$appId,
			'expires_on'	=>	null,
			'access_token'	=>	$accessToken
		]);


		$loadedOwnPages = [];
		// admins comunications
		if( get_option('vk_load_admin_communities' , 1) == 1 )
		{
			$accountsList = self::cmd('groups.get', 'GET' , $accessToken , [
				'filter'    =>  'admin' ,
				'extended'  =>  '1',
				'fields'    =>  'members_count'
			]);
			if( isset($accountsList['items']) && is_array($accountsList['items']) )
			{
				foreach($accountsList['items'] AS $accountInfo)
				{
					$loadedOwnPages[$accountInfo['id']] = true;

					wpDB()->insert(wpTable('account_nodes') , [
						'user_id'			=>	get_current_user_id(),
						'driver'			=>	'vk',
						'screen_name'		=>	$accountInfo['screen_name'],
						'account_id'		=>	$accId,
						'node_type'			=>	$accountInfo['type'],
						'node_id'			=>	$accountInfo['id'],
						'name'				=>	$accountInfo['name'],
						'access_token'		=>	null,
						'category'			=>	'admin',
						'fan_count'			=>	$accountInfo['members_count'],
						'cover'				=>	$accountInfo['photo_50']
					]);
				}
			}
		}

		// members comunications
		if( get_option('vk_load_members_communities' , 1) == 1 )
		{
			$limit = get_option('vk_max_communities_limit' , 100);
			$limit = $limit >= 0 ? $limit : 0;

			$accountsList = self::cmd('groups.get', 'GET' , $accessToken , [
				'extended'  =>  '1',
				'fields'    =>  'members_count',
				'count'     =>  $limit
			]);

			if( isset($accountsList['items']) && is_array($accountsList['items']) )
			{
				foreach($accountsList['items'] AS $accountInfo)
				{
					if( isset($loadedOwnPages[$accountInfo['id']]) )
					{
						continue;
					}

					wpDB()->insert(wpTable('account_nodes') , [
						'user_id'			=>	get_current_user_id(),
						'driver'			=>	'vk',
						'screen_name'		=>	$accountInfo['screen_name'],
						'account_id'		=>	$accId,
						'node_type'			=>	$accountInfo['type'],
						'node_id'			=>	$accountInfo['id'],
						'name'				=>	$accountInfo['name'],
						'access_token'		=>	null,
						'category'			=>	'',
						'fan_count'			=>	$accountInfo['members_count'],
						'cover'				=>	$accountInfo['photo_50']
					]);
				}
			}
		}
	}

	public static function cmd( $cmd , $method = 'GET' , $accessToken , array $data = [] )
	{
		$data['access_token'] = $accessToken;
		$data['v'] = '5.69';

		$url = 'https://api.vk.com/method/' . $cmd ;

		$method = $method == 'POST' ? 'POST' : ( $method == 'DELETE' ? 'DELETE' : 'GET' );

		$data1 = Curl::getContents( $url , $method , $data );
		$data = json_decode( $data1 , true );

		if( !is_array($data) || !isset($data['response']) )
		{
			return [
				'error' =>  ['message' => isset($data['error']) && isset($data['error']['error_msg']) ? $data['error']['error_msg'] : 'Error!']
			];
		}

		return $data['response'];
	}

	public static function sendPost( $nodeFbId , $type , $message , $link , $images , $video , $accessToken )
	{
		$sendData = [
			'message'	=>	spintax( $message ),
			'owner_id'	=>	$nodeFbId
		];

		if( $type == 'link' )
		{
			$sendData['attachments'] = spintax( $link );
		}
		else if( $type == 'image' )
		{
			$sendData['attachments'] = [];

			$uplServer = self::cmd('photos.getWallUploadServer' , 'GET' , $accessToken /* , [group_id]*/);
			$uplServer = $uplServer['upload_url'];

			$images2 = [];
			$i = 0;
			foreach($images AS $imageURL)
			{
				$i++;
				if(function_exists('curl_file_create'))
				{
					$images2['file' . $i] = curl_file_create($imageURL);
				}
				else
				{
					$images2['file' . $i] = '@' . $imageURL;
				}
			}

			$uploadFile = Curl::getContents( $uplServer , 'POST' , $images2 );
			$uploadFile = json_decode($uploadFile , true);

			//$uploadFile['user_id'] = $nodeFbId;

			$uploadPhoto = self::cmd('photos.saveWallPhoto' , 'GET' , $accessToken , $uploadFile);

			if( is_array($uploadPhoto) && !isset($uploadPhoto['error']) )
			{
				foreach($uploadPhoto AS $photoInf)
				{
					$sendData['attachments'][] = 'photo' . $photoInf['owner_id'] . '_' . $photoInf['id'];
				}
			}
			$sendData['attachments'] = implode(',' , $sendData['attachments']);
		}
		else if( $type == 'video' )
		{
			$videoUplServer = self::cmd('video.save' , 'GET' , $accessToken , [
				'name'      =>  mb_substr($message , 0 , 50 , 'UTF-8'),
				'wallpost'  =>  1
			]);

			$ownerId = $videoUplServer['owner_id'];
			$videoId = $videoUplServer['video_id'];
			$uploadURL = $videoUplServer['upload_url'];

			$uploadFile = Curl::getContents( $uploadURL , 'POST' , [
				'file' => function_exists('curl_file_create') ? curl_file_create($video) : '@' . $video
			] );
			$uploadFile = json_decode($uploadFile , true);

			if( !isset($uploadFile['error']) )
			{
				$sendData['attachments'] = 'video'.$ownerId.'_'.$videoId;
			}
		}

		$result = self::cmd('wall.post' , 'GET' , $accessToken , $sendData);

		if( isset($result['error']) )
		{
			$result2 = [
				'status'	=>	'error',
				'error_msg'	=>	isset($result['error']['message']) ? $result['error']['message'] : 'Error!'
			];
		}
		else
		{
			$result2 = [
				'status'	=>  'ok',
				'id'		=>	$nodeFbId . '_' . $result['post_id']
			];
		}

		return $result2;
	}

	public static function getStats( $postId , $accessToken )
	{
		$stat = self::cmd('wall.getById' , 'GET' , $accessToken , ['posts' => $postId]);
		$stat = is_array($stat) && isset($stat[0]) ? $stat[0] : array();

		return [
			'comments'      =>  isset($stat['comments']['count']) ? (int)$stat['comments']['count'] : 0,
			'like'          =>  isset($stat['likes']['count']) ? (int)$stat['likes']['count'] : 0,
			'shares'        =>  isset($stat['reposts']['count']) ? (int)$stat['reposts']['count'] : 0,
			'details'       =>  ''
		];
	}

}