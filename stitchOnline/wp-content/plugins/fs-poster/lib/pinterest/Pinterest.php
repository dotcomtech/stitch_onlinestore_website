<?php

require_once __DIR__ . '/../Curl.php';

class Pinterest
{

	public static function callbackURL()
	{
		return site_url() . '/?pinterest_callback=1';
	}

	public static function authorizePinterestUser( $appId , $accessToken )
	{
		$me = self::cmd('me', 'GET' , $accessToken , ['fields' => 'id,username,image,first_name,last_name,counts'] );

		if( !isset($me['data']) )
		{
			response(false);
		}
		$me = $me['data'];

		$meId = $me['id'];

		$checkLoginRegistered = wpFetch('accounts' , ['user_id' => get_current_user_id() , 'driver' => 'pinterest', 'profile_id' => $meId]);

		$dataSQL = [
			'user_id'			=>	get_current_user_id(),
			'name'		  		=>	$me['first_name'] .' ' . $me['last_name'],
			'driver'			=>	'pinterest',
			'profile_id'		=>	$meId,
			'profile_pic'		=>	$me['image']['60x60']['url'],
			'followers_count'	=>	$me['counts']['followers'],
			'friends_count'		=>	$me['counts']['following'],
			'username'			=>	$me['username']
		];

		if( !$checkLoginRegistered )
		{
			wpDB()->insert(wpTable('accounts') , $dataSQL);

			$accId = wpDB()->insert_id;
		}
		else
		{
			$accId = $checkLoginRegistered['id'];

			wpDB()->update(wpTable('accounts') , $dataSQL , ['id' => $accId]);

			wpDB()->delete( wpTable('account_access_tokens')  , ['account_id' => $accId , 'app_id' => $appId] );
		}

		// acccess token
		wpDB()->insert( wpTable('account_access_tokens') ,  [
			'account_id'	=>	$accId,
			'app_id'		=>	$appId,
			'access_token'	=>	$accessToken
		]);

		// set default board
		$boards = self::cmd('me/boards' , 'GET' , $accessToken , ['fields' => 'id,name']);
		if( is_array($boards['data']) && !empty($boards['data']) )
		{
			$firstBoard = reset($boards['data']);
			$boardId = $firstBoard['id'];
			$boardName = $firstBoard['name'];

			wpDB()->update(wpTable('accounts') , [
				'options'   =>  json_encode(['board' => ['id' => $boardId , 'name' => $boardName]])
			] , ['id' => $accId]);
		}
	}

	public static function cmd( $cmd , $method = 'GET' , $accessToken , array $data = [] )
	{
		$data['access_token'] = $accessToken;

		$url = 'https://api.pinterest.com/v1/' . trim($cmd , '/') . '/';

		$method = $method == 'POST' ? 'POST' : ( $method == 'DELETE' ? 'DELETE' : 'GET' );

		$data1 = Curl::getContents( $url , $method , $data );
		$data = json_decode( $data1 , true );

		if( !is_array($data) )
		{
			$data = [
				'error' =>  ['message' => 'Error data!']
			];
		}

		return $data;
	}

	public static function sendPost( $accountInfo , $type , $message , $link , $images , $video , $accessToken )
	{
		$options = json_decode($accountInfo['options'] , true);

		if( !is_array($options) || !isset($options['board']['name']) )
		{
			return [
				'status'	=>	'error',
				'error_msg'	=>	'Default board for Pinterest account not selected (Settings > Accounts > Edit default Board)!'
			];
		}

		$sendData = [
			'board'   =>    $options['board']['id'],//$accountInfo['username'] . '/' . $options['board']['name'],
			'note'    =>    $message,
			'link'    =>    $link
		];

		if( $type == 'image' )
		{
			$sendData['image_url'] = reset($images);
		}
		else
		{
			return [
				'status'	=>	'error',
				'error_msg'	=>	'Image not fount for pin to board!'
			];
		}

		$result = self::cmd('pins' , 'POST' , $accessToken , $sendData);

		if( isset($result['message']) )
		{
			$result2 = [
				'status'	=>	'error',
				'error_msg'	=>	$result['message']
			];
		}
		else
		{


			$result2 = [
				'status'	=>  'ok',
				'id'		=>	$result['data']['id']
			];
		}

		return $result2;
	}

	public static function getLoginURL($appId)
	{
		do_action('registerSession');
		$_SESSION['save_app_id'] = $appId;

		$appInf = wpFetch('apps' , ['id' => $appId , 'driver' => 'pinterest']);
		if( !$appInf )
		{
			print 'Error! App not found!';
			exit();
		}
		$appId = urlencode($appInf['app_id']);

		$callbackUrl = urlencode(self::callbackUrl());

		return "https://api.pinterest.com/oauth/?response_type=code&redirect_uri=".$callbackUrl."&client_id=".$appId."&scope=read_public,write_public";
	}

	public static function getAccessToken( )
	{
		do_action('registerSession');
		if( !isset($_SESSION['save_app_id']) )
		{
			return false;
		}

		$code = _get('code' , '' , 'string');

		if( empty($code) )
		{
			if( isset($_GET['error_message']) && is_string($_GET['error_message']) )
			{
				$errorMsg = esc_html($_GET['error_message']);
				print 'Loading... <script>if( typeof window.opener.compleateOperation == "function" ){ window.opener.compleateOperation(false , "'.$errorMsg.'");window.close();}else{document.write("This account already has been added!");} </script>';
				exit;
			}
			return false;
		}

		$appId = (int)$_SESSION['save_app_id'];
		unset($_SESSION['save_app_id']);
		$appInf = wpFetch('apps' , ['id' => $appId , 'driver' => 'pinterest']);
		$appSecret = urlencode($appInf['app_secret']);
		$appId2 = urlencode($appInf['app_id']);


		$token_url = "https://api.pinterest.com/v1/oauth/token?grant_type=authorization_code&client_id={$appId2}&client_secret={$appSecret}&code={$code}";

		$response = Curl::getContents($token_url , 'POST');
		$params = json_decode($response , true);
		$access_token = esc_html($params['access_token']);

		self::authorizePinterestUser( $appId , $access_token );

		print 'Loading... <script>if( typeof window.opener.compleateOperation == "function" ){ window.opener.compleateOperation(true);window.close();}else{document.write("Error! Please try again!");} </script>';
		exit;
	}

	public static function getStats($postId , $accessToken)
	{
		$result = self::cmd('pins/' . $postId , 'GET' , $accessToken, ['fields' => 'counts']);

		return [
			'comments'      =>  isset($result['data']['counts']['comments']) ? $result['data']['counts']['comments'] : 0,
			'like'          =>  isset($result['data']['counts']['saves']) ? $result['data']['counts']['saves'] : 0,
			'shares'        =>  0,
			'details'       =>  'Saves: ' . (isset($result['data']['counts']['saves']) ? $result['data']['counts']['saves'] : 0)
		];
	}

}