<?php

require __DIR__ . "/autoload.php";

use Abraham\TwitterOAuth\TwitterOAuth;

class TwitterLib
{

	public static function authorizeUser($appInf , $oauth_token , $oauth_token_secret)
	{
		$connection = new Abraham\TwitterOAuth\TwitterOAuth($appInf['app_key'], $appInf['app_secret'], $oauth_token , $oauth_token_secret);
		$user = $connection->get("account/verify_credentials");

		if( !(!empty($user) && isset($user->id)) )
		{
			print 'Error! ';
			exit();
		}

		$checkUserExist = wpFetch('accounts' , [
			'user_id'       =>  get_current_user_id(),
			'driver'        =>  'twitter',
			'profile_id'    =>  $user->id_str
		]);

		if($checkUserExist)
		{
			print esc_html__('Loading...' , 'fs-poster') . ' <script>if( typeof window.opener.compleateOperation == "function" ){ window.opener.compleateOperation(false , "'.esc_html__('This account already has been added!' , 'fs-poster').'");window.close();}else{document.write("'.esc_html__('This account already has been added!' , 'fs-poster').'");} </script>';
			exit();
		}

		wpDB()->insert(wpTable('accounts') , [
			'user_id'           =>  get_current_user_id(),
			'driver'            =>  'twitter',
			'name'              =>  $user->name,
			'profile_id'        =>  $user->id_str,
			'email'             =>  '',
			'gender'            =>  0,
			'birthday'          =>  null,
			'is_active'         =>  1,
			'username'          =>  $user->screen_name,
			'followers_count'   =>  $user->followers_count,
			'friends_count'     =>  $user->friends_count,
			'listed_count'      =>  $user->listed_count
		]);

		wpDB()->insert(wpTable('account_access_tokens') , [
			'account_id'            =>  wpDB()->insert_id,
			'app_id'                =>  $appInf['id'],
			'access_token'          =>  $oauth_token,
			'access_token_secret'   =>  $oauth_token_secret
		]);
	}

	public static function sendPost( $appId , $type , $message , $link , $images , $video , $accessToken , $accessTokenSecret )
	{
		$appInfo = wpFetch('apps' , ['id' => $appId , 'driver' => 'twitter']);
		if( !$appInfo )
		{
			return [
				'status'	=>	'error',
				'error_msg'	=>	'Error! Twitter App not found!'
			];
		}

		$parameters['status'] = $message;

		$connection = new TwitterOAuth($appInfo['app_key'], $appInfo['app_secret'], $accessToken, $accessTokenSecret);

		if( $type == 'link' )
		{
			$parameters['status'] .= "\n" . $link;
		}
		if( !empty($images) && is_array($images) )
		{
			$uplaodedImages = [];
			$c = 0;
			foreach( $images AS $imageURL )
			{
				$c++;
				if( $c > 4 ) // max 4 image
					break;

				if( empty($imageURL) || !is_string($imageURL) )
				{
					continue;
				}

				$uploadImage = $connection->upload('media/upload', ['media' => $imageURL], true);
				if( isset($uploadImage->media_id_string) && !empty($uploadImage->media_id_string) && is_string($uploadImage->media_id_string) )
				{
					$uplaodedImages[] = $uploadImage->media_id_string;
				}
			}

			if( !empty($uplaodedImages) )
			{
				$parameters['media_ids'] = implode(',' , $uplaodedImages);
			}
		}

		if( $type == 'video' && !empty($video) && is_string($video) )
		{
			$uploadImage = $connection->upload('media/upload', [
				'media'          => $video,
				'media_type'     => mime_content_type($video),
				'media_category' => 'tweet_video'
			] , true);

			if( isset($uploadImage->media_id_string) && !empty($uploadImage->media_id_string) && is_string($uploadImage->media_id_string) )
			{
				$parameters['media_ids'] = $uploadImage->media_id_string;
			}
		}

		$result = $connection->post('statuses/update', $parameters);

		if ($connection->getLastHttpCode() == 200)
		{
			return [
				'status'	=>  'ok',
				'id'		=>	isset($result->id_str) && is_string($result->id_str) ? (string)$result->id_str : ''
			];
		}
		else if( isset($result->errors) && is_array($result->errors) && !empty($result->errors) )
		{
			$error = reset($result->errors);
			$errorMsg = isset($error->message) && is_string($error->message) ? (string)$error->message : 'Error! (-)';
			return [
				'status'	=>	'error',
				'error_msg'	=>	$errorMsg
			];
		}
		else
		{
			return [
				'status'	=>	'error',
				'error_msg'	=>	'Error! (?)'
			];
		}
	}

	public static function getStats( $postId , $accessToken , $accessTokenSecret , $appId )
	{
		$appInfo = wpFetch('apps' , ['id' => $appId , 'driver' => 'twitter']);

		$connection = new TwitterOAuth($appInfo['app_key'], $appInfo['app_secret'], $accessToken, $accessTokenSecret);
		$stat = (array)$connection->get("statuses/show/" . $postId);

		return [
			'comments'      =>  0,
			'like'          =>  isset($stat['favorite_count']) ? (int)$stat['favorite_count'] : 0,
			'shares'        =>  isset($stat['retweet_count']) ? (int)$stat['retweet_count'] : 0,
			'details'       =>  ''
		];
	}

}