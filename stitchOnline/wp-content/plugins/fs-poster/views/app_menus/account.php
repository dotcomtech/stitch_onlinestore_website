<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$accountsList = wpDB()->get_results(wpDB()->prepare("SELECT driver, COUNT(0) AS _count FROM ".wpTable('accounts')." WHERE user_id=%d  GROUP BY driver",  [get_current_user_id()]) , ARRAY_A);
$accountsCount = [
	'fb'        =>  0,
	'twitter'   =>  0,
	'instagram' =>  0,
	'linkedin'  =>  0,
	'vk'        =>  0,
	'pinterest' =>  0,
	'reddit'    =>  0,
	'tumblr'    =>  0,
	'google'    =>  0
];
foreach( $accountsList AS $aInf )
{
	if( isset($accountsCount[$aInf['driver']]) )
	{
		$accountsCount[$aInf['driver']] = $aInf['_count'];
	}
}

?>
<style>
	.social_network_div
	{
		width: 100%;
		height: 35px;
		background: #FFF;
		border: 1px solid #DDD;
		margin-top: 3px;
		padding-left: 15px;
		display: flex;
		align-items: center;
		justify-content: space-between;
		font-size: 14px;
		color: #666 !important;
		-webkit-border-radius: 3px;
		-moz-border-radius: 3px;
		border-radius: 3px;

		-webkit-box-shadow: 2px 2px 2px 0px #DDD;
		-moz-box-shadow: 2px 2px 2px 0px #DDD;
		box-shadow: 2px 2px 2px 0px #DDD;

		cursor: pointer;
	}
	.social_network_div:hover
	{
		background: #f9f9f9;
	}
	.social_network_div i
	{
		margin-right: 5px;
		color: #74b9ff;
	}
	.snd_badge
	{
		margin-right: 10px;
		background: #fd79a8;
		color: #FFF;
		width: 18px;
		height: 18px;
		-webkit-border-radius: 18px;
		-moz-border-radius: 18px;
		border-radius: 18px;
		text-align: center;
		font-size: 11px;
		font-weight: 700;
		-webkit-box-shadow: 2px 2px 2px 0px #EEE;
		-moz-box-shadow: 2px 2px 2px 0px #EEE;
		box-shadow: 2px 2px 2px 0px #EEE;
	}

	.snd_active
	{
		border-left: 3px solid #fd79a8 !important;
		background: #f9f9f9 !important;
	}
	.snd_active .snd_badge
	{
		margin-right: 12px;
	}
	#account_supports
	{
		width: 200px;
		margin-top: 70px;
		margin-left: 20px;
	}
</style>

<div style="display: flex;">
	<div id="account_supports">
		<div class="social_network_div snd_active" data-setting="fb">
			<div><i class="fab fa-facebook-square"></i> Facebook</div>
			<div class="snd_badge"><?=$accountsCount['fb']?></div>
		</div>
		<div class="social_network_div" data-setting="twitter">
			<div><i class="fab fa-twitter-square"></i> Twitter</div>
			<div class="snd_badge"><?=$accountsCount['twitter']?></div>
		</div>
		<div class="social_network_div" data-setting="instagram">
			<div><i class="fab fa-instagram"></i> Instagram</div>
			<div class="snd_badge"><?=$accountsCount['instagram']?></div>
		</div>
		<div class="social_network_div" data-setting="linkedin">
			<div><i class="fab fa-linkedin"></i> Linkedin</div>
			<div class="snd_badge"><?=$accountsCount['linkedin']?></div>
		</div>
		<div class="social_network_div" data-setting="vk">
			<div><i class="fab fa-vk"></i> VK</div>
			<div class="snd_badge"><?=$accountsCount['vk']?></div>
		</div>
		<div class="social_network_div" data-setting="pinterest">
			<div><i class="fab fa-pinterest"></i> Pinterest</div>
			<div class="snd_badge"><?=$accountsCount['pinterest']?></div>
		</div>
		<div class="social_network_div" data-setting="reddit">
			<div><i class="fab fa-reddit"></i> Reddit</div>
			<div class="snd_badge"><?=$accountsCount['reddit']?></div>
		</div>
		<div class="social_network_div" data-setting="tumblr">
			<div><i class="fab fa-tumblr-square"></i> Tumblr</div>
			<div class="snd_badge"><?=$accountsCount['tumblr']?></div>
		</div>
		<!--<div class="social_network_div" data-setting="google">
			<div><i class="fab fa-google"></i> Google+</div>
			<div class="snd_badge"><?/*=$accountsCount['google']*/?></div>
		</div>-->
	</div>
	<div style="width: 100%;" id="account_content">

	</div>
</div>

<script>
	jQuery(document).ready(function()
	{
		var currentSettings;
		$("#account_supports>[data-setting]").click(function()
		{
			currentSettings = $(this).attr('data-setting');
			$("#account_supports .snd_active").removeClass('snd_active');
			$(this).addClass('snd_active');

			fsCode.ajax('get_accounts' , {'name': currentSettings}, function(result)
			{
				$("#account_content").html(fsCode.htmlspecialchars_decode(result['html']));

				$('#account_supports .social_network_div[data-setting="'+currentSettings+'"] .snd_badge').text( $("#accounts_count").text() );
			});
		}).eq(0).trigger('click');

		$("#account_content").on('click' , '.delete_account_btn' , function()
		{
			var tr = $(this).closest('tr'),
				aId = tr.attr('data-id');

			fsCode.confirm("<?=esc_html__('Are you sure you want to delete?' , 'fs-poster');?>" , 'danger' , function ()
			{
				fsCode.ajax('delete_account' , {'id': aId} , function(result)
				{
					tr.fadeOut(300, function()
					{
						$(this).remove();
						if( $(".ws_table>tbody>tr").length == 0 )
						{
							$(".ws_table>tbody").append('<tr><td colspan="100%" style="color: #999;">No accound found</td></tr>').children('tr').hide().fadeIn(200);
						}
						$("#accounts_count").text(parseInt($("#accounts_count").text()) - 1);
						var oldCount = $('#account_supports .social_network_div[data-setting="'+currentSettings+'"] .snd_badge').text().trim();
						$('#account_supports .social_network_div[data-setting="'+currentSettings+'"] .snd_badge').text(parseInt(oldCount) - 1);
					});
				});
			}, true);
		}).on('click' , '.account_checkbox' , function()
		{
			var checked = $(this).hasClass("account_checked"),
				dataId = $(this).closest('tr').attr('data-id');

			if( checked )
			{
				$(this).removeClass('account_checked');
			}
			else
			{
				$(this).addClass('account_checked');
			}

			fsCode.ajax('account_activity_change' , {'id': dataId, 'checked': checked?0:1});
		});
	});

</script>

