<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="setting_item" style="width: 50%;">
	<div class="setting_item_label">
		<div><?=esc_html__('Share on background:' , 'fs-poster')?></div>
		<div class="s_help"><?=esc_html__('If you activated this option, share process will compleate on background and you do not must to wait share process.' , 'fs-poster')?></div>
	</div>
	<div class="s_input">
		<div class="onoffswitch">
			<input type="checkbox" name="fs_share_on_background" class="onoffswitch-checkbox" id="fs_share_on_background"<?=get_option('fs_share_on_background', '1')?' checked':''?>>
			<label class="onoffswitch-label" for="fs_share_on_background"></label>
		</div>
	</div>
</div>

<div class="setting_item" style="width: 50%;">
	<div class="setting_item_label">
		<div><?=esc_html__('Unique post link:' , 'fs-poster')?></div>
		<div class="s_help"><?=esc_html__('You will have the ability to the share the same post in numerous amount of communities. In this case Facebook will block your duplicate post. If you activate this choice, the ending of every link of a post will include random symbols, therefore creating a unique post.' , 'fs-poster')?></div>
	</div>
	<div class="s_input">
		<div class="onoffswitch">
			<input type="checkbox" name="unique_link" class="onoffswitch-checkbox" id="unique_link"<?=get_option('unique_link', '1')?' checked':''?>>
			<label class="onoffswitch-label" for="unique_link"></label>
		</div>
	</div>
</div>

<div style="height: 30px;"><hr></div>

<div class="setting_item" style="width: 50%;">
	<div class="setting_item_label">
		<div><?=esc_html__('URL shortener:' , 'fs-poster')?></div>
		<div class="s_help"><?=esc_html__('If you activate this option you will auto short your post URLs.' , 'fs-poster')?></div>
	</div>
	<div class="s_input">
		<div class="onoffswitch">
			<input type="checkbox" name="url_shortener" class="onoffswitch-checkbox" id="url_shortener"<?=get_option('url_shortener', '0')?' checked':''?> onchange="if($(this).is(':checked')){ $('#hide_shortener').fadeIn(fadeSpeed); }else{ $('#hide_shortener').fadeOut(fadeSpeed); }">
			<label class="onoffswitch-label" for="url_shortener"></label>
		</div>
	</div>
</div>

<div id="hide_shortener" style="display: none; width: 50%;">
	<div class="setting_item">
		<div class="setting_item_label">
			<div><?=esc_html__('URL shortener service:' , 'fs-poster')?></div>
			<div class="s_help"><?=esc_html__('Select which URL shortener service you wont to use your short URLs.' , 'fs-poster')?></div>
		</div>
		<div class="s_input">
			<select class="ws_form_element" id="shortener_service" name="shortener_service" style="width: 200px;" onchange="if($(this).val()=='bitly'){$('#for_bitly').show();}else{$('#for_bitly').hide();}">
				<option value="tinyurl"<?=get_option('shortener_service')=='tinyurl'?' selected':''?>>TinyURL</option>
				<option value="bitly"<?=get_option('shortener_service')=='bitly'?' selected':''?>>Bitly</option>
			</select>
		</div>
	</div>
	<div class="setting_item" id="for_bitly" style="display: none;">
		<div class="setting_item_label">
			<div><?=esc_html__('Bitly Access token:' , 'fs-poster')?></div>
			<div class="s_help"><?=esc_html__('For getting Access token you mast register bitly. After registration you will get a new access token!' , 'fs-poster')?></div>
		</div>
		<div class="s_input">
			<input type="text" name="url_short_access_token_bitly" class="ws_form_element" style="width: 300px;" value="<?=esc_html(get_option('url_short_access_token_bitly', '100'))?>">
		</div>
	</div>
</div>

<script>
	var fadeSpeed = 0;
	jQuery(document).ready(function()
	{
		$("#save_btn").click(function()
		{
			var data = fsCode.serialize($(".settings_form"));

			fsCode.ajax('settings_general_save' , data , function(result)
			{
				fsCode.toast("<?=esc_html__('Save successful!' , 'fs-poster')?>" , 'success');
			});
		});

		$("#url_shortener").trigger('change');
		$("#shortener_service").trigger('change');

		fadeSpeed = 200;
	});
</script>