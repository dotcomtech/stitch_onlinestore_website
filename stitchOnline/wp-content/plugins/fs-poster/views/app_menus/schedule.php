<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$schedules = wpFetchAll('schedules' , ['user_id' => get_current_user_id()]);
?>

<div style="margin: 40px 80px;">
	<span style="color: #888; font-size: 17px; font-weight: 600; line-height: 36px;"><span id="schedules_count"><?php print count($schedules);?></span> <?=esc_html__('schedule added' , 'fs-poster');?></span>
	<button type="button" class="ws_btn ws_bg_dark" style="float: right;" data-load-modal="add_schedule"><i class="fa fa-plus"></i> <?=esc_html__('SCHEDULE' , 'fs-poster');?></button>
</div>

<div class="ws_table_wraper">
	<table class="ws_table" id="app_list_table">
		<thead>
		<tr>
			<th><?=esc_html__('TITLE' , 'fs-poster');?> <i class="fa fa-caret-down"></i></th>
			<th style="width: 150px;"><?=esc_html__('START DATE' , 'fs-poster');?></th>
			<th style="width: 150px;"><?=esc_html__('END DATE' , 'fs-poster');?></th>
			<th style="width: 150px;"><?=esc_html__('INTERVAL' , 'fs-poster');?></th>
			<th style="width: 250px;"><?=esc_html__('STATUS' , 'fs-poster');?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		foreach($schedules AS $scheduleInf)
		{
			$statusBtn = '';
			if( $scheduleInf['status'] == 'finished' )
			{
				$statusBtn = 'success';
			}
			else if( $scheduleInf['status'] == 'paused' )
			{
				$statusBtn = 'danger';
			}
			else
			{
				$statusBtn = 'info';
			}
			?>
			<tr data-id="<?=$scheduleInf['id']?>">
				<td><i class="fa fa-rocket ws_color_danger" style="padding-right: 8px; padding-left: 8px;"></i> <?=cutText(esc_html($scheduleInf['title']))?></td>
				<td style="font-size: 14px; color: #888;">
					<i class="far fa-calendar-alt"></i>
					<?=date('Y-m-d' , strtotime($scheduleInf['start_date']))?>
				</td>
				<td style="font-size: 14px; color: #888;">
					<i class="far fa-calendar-alt"></i>
					<?=date('Y-m-d' , strtotime($scheduleInf['end_date']))?>
				</td>
				<td style="font-size: 14px; color: #888;"><i class="far fa-clock"></i> <?=($scheduleInf['interval'] % 24 == 0 ? ($scheduleInf['interval'] / 24) . ' days' : $scheduleInf['interval'] . ' hours')?></td>
				<td>

					<button type="button" class="ws_btn ws_btn_small ws_bg_<?=$statusBtn?>"><?=esc_html($scheduleInf['status'])?></button>

					<?php
					if( $scheduleInf['status'] == 'active' )
					{
						?>
						<button type="button" class="ws_btn ws_btn_small ws_bg_purple ws_tooltip changeStatus" data-title="<?=esc_html__('Pause' , 'fs-poster');?>"><i class="fa fa-pause"></i></button>
						<?php
					}
					else if($scheduleInf['status'] == 'paused')
					{
						?>
						<button type="button" class="ws_btn ws_btn_small ws_bg_purple ws_tooltip changeStatus" data-title="<?=esc_html__('Play' , 'fs-poster');?>"><i class="fa fa-play"></i></button>
						<?php
					}
					?>

					<button type="button" class="ws_btn ws_btn_small ws_bg_warning ws_tooltip" data-title="<?=esc_html__('Show posts list' , 'fs-poster');?>" data-load-modal="posts_list" data-parameter-schedule_id="<?=$scheduleInf['id']?>"><i class="fa fa-list"></i></button>

					<button class="delete_schedule_btn delete_btn_desing ws_tooltip" data-title="<?=esc_html__('Delete schedule' , 'fs-poster');?>" data-float="left"><i class="fa fa-trash"></i></button>
				</td>
			</tr>
			<?php
		}
		if( empty($schedules) )
		{
			?>
			<tr><td colspan="100%" style="color: #999;">No schedules found</td></tr>
			<?php
		}
		?>
		</tbody>
	</table>
</div>
<script src="<?=PLUGIN_URL?>js/jquery-ui.js"></script>
<link rel="stylesheet" href="<?=PLUGIN_URL?>css/jquery-ui.css">
<script>
	jQuery(document).ready(function()
	{
		jQuery("body").on('click' , '.delete_schedule_btn' , function()
		{
			var tr = $(this).closest('tr'),
				aId = tr.attr('data-id');

			fsCode.confirm("<?=esc_html__('Are you sure you want to delete?' , 'fs-poster');?>" , 'danger' , function ()
			{
				fsCode.ajax('delete_schedule' , {'id': aId} , function(result)
				{
					tr.fadeOut(300, function()
					{
						$(this).remove();
						if( $(".ws_table>tbody>tr").length == 0 )
						{
							$(".ws_table>tbody").append('<tr><td colspan="100%" style="color: #999;">No schedules found</td></tr>').children('tr').hide().fadeIn(200);
						}
						$("#schedules_count").text(parseInt($("#schedules_count").text()) - 1);
					});
				});
			}, true);
		}).on('click' , '.changeStatus' , function()
		{
			var id = $(this).closest('tr').attr('data-id'),
				btn = $(this);

			fsCode.ajax('schedule_change_status' , {'id': id} , function(result)
			{
				fsCode.loading(1);
				location.reload();
			});
		});
	});
</script>
