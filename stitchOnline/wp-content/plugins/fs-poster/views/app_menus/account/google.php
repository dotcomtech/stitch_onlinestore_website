<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$accountsList = wpDB()->get_results(wpDB()->prepare("SELECT * FROM ".wpTable('accounts')." tb1 WHERE user_id=%d AND driver='google'",  [get_current_user_id()]) , ARRAY_A);

?>

<style>
	.account_checkbox
	{
		margin-left: 15px;
	}
	.account_checkbox>i
	{
		background: #DDD;
		padding: 5px;
		border-radius: 50%;
		color: #FFF;
		cursor: pointer;
		font-size: 13px !important;
	}
	.account_checked>i
	{
		background: #86d4ea;
		-webkit-animation: fadein3 0.3s;
		animation: fadein3 0.3s;
	}
</style>

<div style="margin: 40px 80px;">
	<span style="color: #888; font-size: 17px; font-weight: 600; line-height: 36px;"><span id="accounts_count"><?php printf(__('%d Google account added', 'fs-poster') , count($accountsList));?></span>
	<button type="button" class="ws_btn ws_bg_dark" style="float: right;" data-load-modal="add_google_account"><i class="fa fa-plus"></i> ADD ACCOUNT</button>
</div>
<div class="ws_table_wraper">
	<table class="ws_table">
		<thead>
		<tr>
			<th>NAME <i class="fa fa-caret-down"></i></th>
			<th>Email</th>
			<th style="width: 15%;">ACTIVE</th>
		</tr>
		</thead>
		<tbody>
		<?php
		foreach($accountsList AS $accountInf)
		{
			?>
			<tr data-id="<?=$accountInf['id']?>">
				<td>
					<img class="ws_img_style" src="<?=profilePic($accountInf)?>">
					<span style="vertical-align: middle;"><?php print esc_html($accountInf['name']);?></span>
				</td>
				<td><?=esc_html($accountInf['email']);?></td>
				<td>
					<div class="account_checkbox<?=$accountInf['is_active']?' account_checked':''?> ws_tooltip" data-title="Click to change status">
						<i class="fa fa-check"></i>
					</div>
					<button class="delete_account_btn delete_btn_desing ws_tooltip" data-title="Delete account" data-float="left"><i class="fa fa-trash"></i></button>
				</td>
			</tr>
			<?php
		}
		if( empty($accountsList) )
		{
			?>
			<tr><td colspan="100%" style="color: #999;">No accound found</td></tr>
			<?php
		}
		?>
		</tbody>
	</table>
</div>