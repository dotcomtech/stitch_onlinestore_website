<?php defined('MODAL') or exit();?>

<?php
$accountId = _post('account_id' , '0' , 'num');
$nodeType = _post('type' , '' , 'string');

if( empty($nodeType) )
{
	$nodeList = wpDB()->get_results(wpDB()->prepare("SELECT * FROM ".wpTable('account_nodes')." WHERE user_id=%d AND account_id=%d",  [get_current_user_id() , $accountId ]) , ARRAY_A);
}
else
{
	$nodeList = wpDB()->get_results(wpDB()->prepare("SELECT * FROM ".wpTable('account_nodes')." WHERE user_id=%d AND account_id=%d AND node_type=%s",  [get_current_user_id() , $accountId , $nodeType]) , ARRAY_A);
}


?>

<style>
	#proModal<?=$mn?> .nodes_container>div
	{
		display: flex;
		flex-direction: column;
		width: 365px;
		height: 500px;
		background: #FBFBFD;
		-webkit-box-shadow: 0 0 10px 0 #DDD;
		-moz-box-shadow: 0 0 10px 0 #DDD;
		box-shadow: 0 0 10px 0 #DDD;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
		overflow: hidden;

	}
	#proModal<?=$mn?> .nodes_list
	{
		padding: 8px 0;
		overflow: auto;
		height: 100%;
	}
	#proModal<?=$mn?> .node_toolbar
	{
		padding: 20px;
		padding-right: 0;
		padding-bottom: 10px;
		padding-top: 10px;
	}
	#proModal<?=$mn?> .node_toolbar
	{
		display: flex;
		align-items: stretch;
	}
	#proModal<?=$mn?> .select_all_nodes
	{
		width: 50px;
		display: flex;
		justify-content: center;
		align-items: center;
	}
	#proModal<?=$mn?> .select_all_nodes>i
	{
		background: #DDD;
		padding: 5px;
		border-radius: 50%;
		color: #FFF;
		cursor: pointer;
	}
	#proModal<?=$mn?> .search_input
	{
		position: relative;
		flex-shrink: 1;
		width: 100%;
	}
	#proModal<?=$mn?> .search_input>i
	{
		position: absolute;
		top: 3px;
		bottom: 0;
		margin: auto;
		height: 15px;
		right: 15px;
		color: #CCC;
	}
	#proModal<?=$mn?> .search_input>input
	{
		width: 100%;
		height: 35px;
		padding-left: 20px;
		color: #999;
		font-weight: 600;

		border: 0 !important;
		outline: none !important;

		-webkit-box-shadow: 0 0 3px 0 #DDD !important;
		-moz-box-shadow: 0 0 3px 0 #DDD !important;
		box-shadow: 0 0 3px 0 #DDD !important;

		-webkit-border-radius: 50px;
		-moz-border-radius: 50px;
		border-radius: 50px;
	}

	#proModal<?=$mn?> .search_input>input::placeholder
	{
		color: #CCC;
	}

	#proModal<?=$mn?> .node_div
	{
		position: relative;
		margin: 0 20px 10px;
		padding: 13px;
		border-bottom: 1px solid #EEE;
		background: #FFF;
		-webkit-box-shadow: 0 0 10px 0 #DDD;
		-moz-box-shadow: 0 0 10px 0 #DDD;
		box-shadow: 0 0 10px 0 #DDD;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
	}
	#proModal<?=$mn?> .node_div>div
	{
		float: left;
	}
	#proModal<?=$mn?> .node_div:after
	{
		content: " ";
		clear: both;
		display: block;
	}
	#proModal<?=$mn?> .node_img>img
	{
		-webkit-border-radius: 50% !important;
		-moz-border-radius: 50% !important;
		border-radius: 50% !important;
		width: 40px;
		height: 40px;
	}
	#proModal<?=$mn?> .node_label
	{
		padding-left: 10px;
	}
	#proModal<?=$mn?> .node_label_title
	{
		white-space: nowrap;
		overflow: hidden;
		max-width: 180px;
	}
	#proModal<?=$mn?> .node_label_title>a
	{
		color: #888 !important;
		font-size: 14px;
		font-weight: 600;
		text-decoration: none;
		-webkit-box-shadow: none;
		-moz-box-shadow: none;
		box-shadow: none;
	}
	#proModal<?=$mn?> .node_category
	{
		font-weight: 500;
		color: #95a5a6;
		padding-top: 5px;
		white-space: nowrap;
		overflow: hidden;
		max-width: 180px;
	}
	#proModal<?=$mn?> .node_chckbx>i
	{
		background: #DDD;
		padding: 5px;
		border-radius: 50%;
		color: #FFF;
		margin-top: 12px;
		cursor: pointer;
	}
	#proModal<?=$mn?> .node_checked>i
	{
		background: #86d4ea;

		-webkit-animation: fadein3 0.3s;
		animation: fadein3 0.3s;
	}
	#proModal<?=$mn?> .node_box_label
	{
		text-align: center;
		padding-top: 10px;
		position: relative;
		height: 37px;
	}
	#proModal<?=$mn?> .node_box_label>div
	{
		position: absolute;
		width: 120px;
		height: 22px;
		color: #FFF;
		margin: auto;
		left: 0;
		right: 0;
		background: #94A0B2;
		-webkit-border-radius: 15px;
		-moz-border-radius: 15px;
		border-radius: 15px;
		font-weight: 600;
		font-size: 14px;
		line-height: 20px;
		border: 5px solid #FBFBFD;
	}
	#proModal<?=$mn?> .node_box_label:before
	{
		content: '';
		width: calc(100% - 60px);
		height: 1px;
		border-top: 1px solid #94A0B2;
		top: 16px;
		bottom: 0px;
		left: 0px;
		margin: 10px 30px;
		position: absolute;
	}

	#proModal<?=$mn?> .node_delete
	{
		position: absolute;
		color: #ff7675;
		right: 40px;
		bottom: 0px;
		top: 5px;
		margin: auto;
		background: #FFF;
		-webkit-border-radius: 50%;
		-moz-border-radius: 50%;
		border-radius: 50%;
		width: 20px;
		height: 20px;
		padding: 2px;
		cursor: pointer;
		text-align: center;
		display: none;
	}
	#proModal<?=$mn?> .node_div:hover .node_delete
	{
		display: block;
	}

	@-webkit-keyframes fadein3 {
		from {background-color: #DDD;}
		to {background-color: #86d4ea;}
	}
	@-webkit-keyframes fadein4 {
		from {opacity: 0;}
		to {opacity: 1;}
	}

	#proModal<?=$mn?> .modal-content
	{
		width: auto !important;
	}
</style>

<div class="nodes_container">
	<div>
		<div class="node_box_label">
			<div><?=strtoupper(empty($nodeType) ? 'Communities' : $nodeType)?></div>
		</div>
		<div class="node_toolbar">
			<div class="search_input">
				<input type="text" placeholder="<?=esc_html__('Search...' , 'fs-poster')?>">
				<i class="fa fa-search"></i>
			</div>
			<div class="select_all_nodes"><i class="fa fa-check"></i></div>
		</div>
		<div class="nodes_list">
			<?php
			$count = 0;
			foreach ($nodeList AS $node)
			{
				$count++;
				?>
				<div class="node_div" data-id="<?=(int)$node['id']?>">
					<div class="node_img"><img src="<?=profilePic($node)?>"></div>
					<div class="node_label">
						<div class="node_label_title">
							<a href="<?=profileLink($node)?>" target="_blank" title="Profile link"><?=esc_html($node['name']);?></a>
						</div>
						<div class="node_category">
							<i class="far fa-paper-plane"></i> <?=ucfirst(esc_html($node['driver'] == 'vk'?($node['node_type']).($node['category'] == 'admin' ? ' (admin)' : ''):$node['category']));?>
						</div>
					</div>
					<div class="node_chckbx ws_tooltip<?=($node['is_active']?' node_checked':'')?>" data-title="Click to change status" data-float="left" style="float: right;">
						<i class="fa fa-check"></i>
					</div>
					<div class="node_delete ws_tooltip" data-title="Delete" data-float="left">
						<i class="fa fa-trash"></i>
					</div>
				</div>
				<?php
			}
			if(!$count)
			{
				print '<div style="text-align: center;margin: 20px;font-size: 20px;color: #D2D2D2;font-weight: 700;">'.esc_html__('Empty!' , 'fs-poster').'</div>';
			}
			?>
		</div>
	</div>
</div>

<span class="close" data-modal-close="true">&times;</span>

<script>
	jQuery(document).ready(function()
	{
		$("#proModal<?=$mn?> .node_chckbx").click(function()
		{
			var checked = $(this).hasClass("node_checked"),
				dataId = $(this).closest('.node_div').attr('data-id');

			if( checked )
			{
				$(this).removeClass('node_checked');
			}
			else
			{
				$(this).addClass('node_checked');
			}

			fsCode.ajax('settings_node_activity_change' , {'id': dataId, 'checked': checked?0:1});
		});

		$("#proModal<?=$mn?> .node_delete").click(function()
		{
			var nodeDiv = $(this).closest('.node_div'),
				dataId = nodeDiv.attr('data-id');
			fsCode.confirm("<?=esc_html__('Are you sure you want to delete your account?' , 'fs-poster')?>" , 'danger' , function(modal)
			{
				fsCode.ajax('settings_node_delete' , {'id': dataId} , function()
				{
					fsCode.toast("<?=esc_html__('\'Community\' has been deleted!' , 'fs-poster')?>");
					nodeDiv.hide(500, function()
					{
						$(this).remove();
					});
					fsCode.modalHide(modal);
				});
			}, false);
		});

		$("#proModal<?=$mn?> .select_all_nodes").click(function()
		{
			var checked = $(this).hasClass("node_checked"),
				dataId = [];

			if( checked )
			{
				$(this).removeClass('node_checked');
			}
			else
			{
				$(this).addClass('node_checked');
			}

			$(this).closest('.node_toolbar').next('.nodes_list').children('.node_div').each(function()
			{
				dataId.push($(this).attr('data-id'));
				if( checked )
				{
					$(this).find('.node_chckbx').removeClass('node_checked');
				}
				else if( !$(this).find('.node_chckbx').hasClass('node_checked') )
				{
					$(this).find('.node_chckbx').addClass('node_checked');
				}
			});

			if( dataId.length > 0 )
			{
				fsCode.ajax('settings_node_activity_change_all' , {'id': dataId, 'checked': checked?0:1} , function (result)
				{

				});
			}

		});

		$("#proModal<?=$mn?> .search_input>input").keyup(function()
		{
			var val = $(this).val();

			$(this).closest('.node_toolbar').next('.nodes_list').children('.node_div:not(:contains("' + fsCode.htmlspecialchars(val) + '"))').hide(500);
			$(this).closest('.node_toolbar').next('.nodes_list').children('.node_div:contains("' + fsCode.htmlspecialchars(val) + '")').show(500);
		});

		jQuery.expr[':'].contains = function(a, i, m) {
			return jQuery(a).text().toUpperCase()
				.indexOf(m[3].toUpperCase()) >= 0;
		};
	});
</script>