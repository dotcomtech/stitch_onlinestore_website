<?php defined('MODAL') or exit();?>

<span class="close" data-modal-close="true">&times;</span>

<div style="margin-top: 70px; margin-left: 40px; margin-right: 40px; display: flex;">
	<div style="width: 50%; padding-right: 20px; margin-top: -10px;">
		<img src="<?=plugin_dir_url(__FILE__).'../../images/schedule.svg'?>" style="width: 95%;">
	</div>
	<div style="width: 50%;">
		<div style="width: 100%;">
			<input type="text" class="ws_form_element2 title" placeholder="<?=esc_html__('Title' , 'fs-poster')?>">
		</div>
		<div style="display: flex; width: 100%; margin-top: 15px; align-items: center;">
			<input type="text" class="ws_form_element2 start_date" placeholder="<?=esc_html__('Start date' , 'fs-poster')?>" style="width: calc(50% - 8px); margin-right: 16px;">
			<input type="text" class="ws_form_element2 end_date" placeholder="<?=esc_html__('End date' , 'fs-poster')?>" style="width: calc(50% - 8px);">
		</div>
		<div style="display: flex; width: 100%; margin-top: 15px; align-items: center;">
			<select class="ws_form_element interval" style="width: 90px; margin-right: 15px;">
				<option><?=esc_html__('Interval' , 'fs-poster')?></option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9">9</option>
				<option value="10">10</option>
			</select>
			<select class="ws_form_element interval_type" style="width: 100px;" onchange="if($(this).val()=='24'){ $('#shareTimeId').show(200); }else{ $('#shareTimeId').hide(200); }">
				<option value="1"><?=esc_html__('Hour' , 'fs-poster')?></option>
				<option value="24"><?=esc_html__('Day' , 'fs-poster')?></option>
			</select>
		</div>
		<div style="margin-top: 15px; display: none;" id="shareTimeId">
			<?=esc_html__('Share time:' , 'fs-poster')?> <input type="time" class="ws_form_element2 share_time" placeholder="<?=esc_html__('Share time' , 'fs-poster')?>" value="13:00" style="width: 110px;">
		</div>
		<!--<div style="margin-top: 15px;">
			<button type="button" class="ws_btn ws_btn_small ws_bg_default" data-json="{}">Filters</button>
			<button type="button" class="ws_btn ws_btn_small ws_bg_default" data-json="{}">Accounts</button>
		</div>-->
		<div style="margin-top: 15px;">
			<button type="button" class="ws_btn ws_bg_danger saveScheduleBtn"><?=esc_html__('SAVE SCHEDULE' , 'fs-poster')?></button>
			<button type="button" class="ws_btn" data-modal-close="true"><?=esc_html__('Cancel' , 'fs-poster')?></button>
		</div>
	</div>
</div>

<script>
	jQuery(document).ready(function()
	{
		$("#proModal<?=$mn?> .start_date , #proModal<?=$mn?> .end_date").datepicker({
			dateFormat: "yy-mm-dd"
		});

		$("#proModal<?=$mn?> .saveScheduleBtn").click(function()
		{
			var title = $("#proModal<?=$mn?> .title").val(),
				startDate = $("#proModal<?=$mn?> .start_date").val(),
				endDate = $("#proModal<?=$mn?> .end_date").val(),
				interval = $("#proModal<?=$mn?> .interval").val(),
				intervalType = $("#proModal<?=$mn?> .interval_type").val(),
				share_time  =   $("#proModal<?=$mn?> .share_time").val();

			if( title == '' )
			{
				fsCode.toast("<?=esc_html__('Please type the title field!' , 'fs-poster')?>" , 'danger');
				return false;
			}
			if( startDate == '' )
			{
				fsCode.toast("<?=esc_html__('Please type the start date!' , 'fs-poster')?>" , 'danger');
				return false;
			}
			if( endDate == '' )
			{
				fsCode.toast("<?=esc_html__('Please type the end date!' , 'fs-poster')?>" , 'danger');
				return false;
			}
			if( interval == '' || !( parseInt(interval) > 0 ) )
			{
				fsCode.toast("<?=esc_html__('Please type the interval!' , 'fs-poster')?>" , 'danger');
				return false;
			}

			fsCode.ajax('schedule_save' , {'title':title , 'start_date': startDate , 'end_date': endDate , 'interval': (parseInt(interval) * parseInt(intervalType)) , 'share_time': share_time} , function(result)
			{
				fsCode.loading(1);
				location.reload();
			});
		});


	});
</script>