<?php defined('MODAL') or exit();?>

<?php
$applications = wpFetchAll('apps' , ['driver' => 'tumblr']);
?>
<style>
	.tumblr_logo > img
	{
		width: 60%;
		height: 180px;
		margin: 20px;
	}
	.tumblr_logo
	{
		width: 55%;
		display: flex;
		justify-content: center;
	}
</style>
<span class="close" data-modal-close="true">&times;</span>

<div style="width: 100%; margin-top: 60px; display: flex; justify-content: center; align-items: center;">
	<div class="tumblr_logo"><img src="<?=plugin_dir_url(__FILE__).'../../images/tumblr.svg'?>"></div>
	<div style="width: 45%;">

		<div style="margin-bottom: 25px; font-size: 15px; font-weight: 500; color: #969696;margin-right: 30px; border-left: 3px solid #DDD; padding-left: 10px;"> <?=esc_html__('Select an app. Next click the "GET ACCESS" button. Next, click the "Authorize App" button within an open window.' , 'fs-poster')?></div>

		<div style="width: 200px; margin-bottom: 10px; ">
			<select class="ws_form_element" id="appIdSelect">
				<?php
				foreach ($applications AS $appInf)
				{
					print '<option value="'.(int)$appInf['id'].'" data-standart="'.(int)$appInf['is_standart'].'">'.esc_html($appInf['name']).'</option>';
				}
				?>
			</select>
		</div>

		<div>
			<button type='button' id="addAccountBTN" class="ws_btn ws_bg_danger"><?=esc_html__('GET ACCESS' , 'fs-poster')?></button>
			<button type="button" class="ws_btn" data-modal-close="true"><?=esc_html__('CANCEL' , 'fs-poster')?></button>
		</div>

	</div>
</div>

<script>

	function compleateOperation( status, errorMsg )
	{
		if( status )
		{
			fsCode.toast("<?=esc_html__('Account added successfully!' , 'fs-poster')?>" , 'success');
			fsCode.modalHide($("#proModal<?=$mn?>"));
			$('#account_supports .social_network_div[data-setting="tumblr"]').click();
		}
		else
		{
			fsCode.toast(errorMsg , 'danger', 10000);
		}
	}

	$("#addAccountBTN").click(function ()
	{
		var openURL = "<?=site_url().'?tumblr_app_redirect='?>" + $("#appIdSelect").val();
		if( $("#appIdSelect>:selected").attr('data-standart') == '1' )
		{
			openURL = "<?=standartFSAppRedirectURL('tumblr')?>";
		}

		window.open(openURL , 'fs-standart-app', 'width=750,height=550');
	});

</script>