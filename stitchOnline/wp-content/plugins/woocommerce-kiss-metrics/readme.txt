=== WooCommerce Kissmetrics ===
Author: skyverge, woocommerce
Tags: woocommerce
Requires at least: 4.1
Tested up to: 4.7.3
Requires WooCommerce at least: 2.5.5
Tested WooCommerce up to: 3.0.0

Adds Kissmetrics tracking to WooCommerce with one click!

See http://docs.woothemes.com/document/kiss-metrics/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-kiss-metrics' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
