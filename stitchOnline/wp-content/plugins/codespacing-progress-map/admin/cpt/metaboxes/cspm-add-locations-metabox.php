<?php

/**
 * This class contains all the fields used in the widget "Progress Map: Add locations" 
 *
 * @version 1.0 
 */
 
if(!defined('ABSPATH')){
    exit; // Exit if accessed directly
}

if( !class_exists( 'CspmAddLocationsMetabox' ) ){
	
	class CspmAddLocationsMetabox{
		
		private $plugin_path;
		private $plugin_url;
		
		private static $_this;	
		
		public $plugin_settings = array();
		
		protected $metafield_prefix;
		
		/**
		 * The name of the post to which we'll add the metaboxes
		 * @since 1.0 */
		 
		public $object_type;
		
		public $post_id;

		function __construct($atts = array()){
			
			extract( wp_parse_args( $atts, array(
				'plugin_path' => '', 
				'plugin_url' => '',
				'object_type' => '',
				'plugin_settings' => array(), 
				'metafield_prefix' => '',
			)));
             
			self::$_this = $this;       
				           
			$this->plugin_path = $plugin_path;
			
			$this->plugin_url = $plugin_url;
			
			$this->plugin_settings = $plugin_settings;
			
			$this->object_type = $object_type;

			$this->metafield_prefix = $metafield_prefix; 

			/**
			 * [@visible_metafield_prefix] | Remove the underscore from the begining of the custom fields names ...
			 * ... because we want them to be visible in the default WP widget "Custom fields" ... 
			 * ... and for compatibility with previous (very old) versions of the plugin! */
			
			$this->visible_metafield_prefix = str_replace('_', '', $metafield_prefix); 
			
			/**
			 * Get current post type */
			
			$post_id = 0;
			
			if(isset($_REQUEST['post'])){
				
				$post_id = $_REQUEST['post'];
			
			}elseif(isset($_REQUEST['post_ID'])){
				
				$post_id = $_REQUEST['post_ID'];
				
			}
			
			$this->post_id = $post_id;
						
			/**
			 * Include all required Libraries for this metabox */
			 
			$libs_path = array(
				'cmb2' => 'admin/libs/metabox/init.php',
				'cmb2-tabs' => 'admin/libs/metabox-tabs/cmb2-tabs.class.php',
				'cmb2-cs-gmaps' => 'admin/libs/metabox-cs-gmaps/cmb-cs-gmaps.php', //@since 3.5
			);
				
				foreach($libs_path as $lib_file_path){
					if(file_exists($this->plugin_path . $lib_file_path))
						require_once $this->plugin_path . $lib_file_path;
				}
			
			/**
			 * Load Metaboxes */

			add_action( 'cmb2_admin_init', array(&$this, 'cspm_add_locations_metabox') );

			/**
			 * Call .js and .css files */
			 
			add_filter( 'cmb2_enqueue_js', array(&$this, 'cspm_scripts') );
			
		}
	

		static function this() {
			
			return self::$_this;
		
		}
		
		
		function cspm_scripts(){
			
			global $typenow;
			
			/**
			 * Our custom metaboxes JS & CSS file must be loaded only on our CPT page */

			if(in_array($typenow, $this->plugin_settings['post_types'])){
							
				/**
				 * CSS */
				 
				wp_enqueue_style('cspm-cmb2-tabs-css');
				
				/**
				 * Our custom metaboxes CSS */
				
				wp_enqueue_style('cspm-metabox-css');
		
				/**
				 * js */
	
				wp_enqueue_script('cspm-cmb2-tabs-js');								
				
				/**
				 * Dequeue "Conditionals" JS to prevent conflict with other plugins
				 * @since 3.6 */
				 
				add_action('admin_footer', function(){
					wp_dequeue_script('cmb2-conditionals');
				}, 999999);
					
			}
			
		}
	
	
		/**
		 * "Progress Map: Add locations" Metabox.
		 * This metabox will contain the form for adding new locations to the map
		 *
		 * @since 1.0
		 */
		function cspm_add_locations_metabox(){
			
			/**
			 * Add Locations Metabox options */
			 
			$cspm_add_locations_metabox_options = array(
				'id'            => $this->metafield_prefix . '_add_locations_metabox',
				'title'         => __( 'Progress Map: Add locations', 'cspm' ),
				'object_types'  => $this->plugin_settings['post_types'], // Post types
				'priority'   => 'high',
				//'context'    => 'side',
				'show_names' => true, // Show field names on the left		
				'closed' => false,		
			);
			
				/**
				 * Create post type Metabox */
				 
				$cspm_add_locations_metabox = new_cmb2_box( $cspm_add_locations_metabox_options );

				/**
				 * Display Add Locations Metabox fields */
			
				$this->cspm_add_locations_tabs($cspm_add_locations_metabox, $cspm_add_locations_metabox_options);
			
		}
		
		
		/**
		 * Buill all the tabs that contains "Progress Map" settings
		 *
		 * @since 1.0
		 */
		function cspm_add_locations_tabs($metabox_object, $metabox_options){
			
			/**
			 * Setting tabs */
			 
			$tabs_setting = array(
				'args' => $metabox_options,
				'tabs' => array()
			);
				
				/**
				 * Tabs array */
				 
				$cspm_tabs = array(
					
					/**
				 	 * Coordinates */
					 
					array(
						'id' => 'post_coordinates', 
						'title' => 'GPS Coordinates', 
						'callback' => 'cspm_post_coordinates_fields'
					),
					
					/**
				 	 * Marker & Infobox */
					 
					array(
						'id' => 'post_marker', 
						'title' => 'Marker', 
						'callback' => 'cspm_post_marker_fields'
					),
					
					/**
				 	 * Format & Media */
					 
					array(
						'id' => 'post_format_and_media', 
						'title' => 'Format & Media', 
						'callback' => 'cspm_post_format_and_media'
					),
					
				);
				
				foreach($cspm_tabs as $tab_data){
				 
					$tabs_setting['tabs'][] = array(
						'id'     => 'cspm_' . $tab_data['id'],
						'title'  => '<span class="cspm_tabs_menu_image"><img src="'.$this->plugin_url.'admin/img/add-locations-metabox/'.str_replace('_', '-', $tab_data['id']).'.png" style="width:20px;" /></span> <span class="cspm_tabs_menu_item">'.__( $tab_data['title'], 'cspm' ).'</span>',						
						'fields' => call_user_func(array(&$this, $tab_data['callback'])),
					);
		
				}
			
			/*
			 * Set tabs */
			 
			$metabox_object->add_field( array(
				'id'   => 'cspm_add_locations_tabs',
				'type' => 'tabs',
				'tabs' => $tabs_setting
			) );
			
			return $metabox_object;
			
		}
		
		
		/**
		 * Post Coordinates Fields 
		 *
		 * @since 1.0 
		 */
		function cspm_post_coordinates_fields(){
			
			$fields = array();
			
			$fields[] = array(
				'name' => 'GPS Coordinates',
				'desc' => 'Latitude and longitude of your location. Convert an address or a place to latitude & longitude. Fill the address field, click on "Search", then, click on "Get Pinpoint" to display its latitude and longitude.',
				'type' => 'title',
				'id'   => $this->metafield_prefix . '_gps_coordinates',
				'attributes' => array(
					'style' => 'font-size:20px; color:#008fed; font-weight:400;'
				),
			);

			$fields[] = array(
				'name' => 'Address & GPS Coordinates',
				'desc' => 'Drag the marker to set the exact location',
				'id' => $this->metafield_prefix . '_location',
				'type' => 'cs_gmaps',
				'options' => array(
					'api_key' => $this->plugin_settings['api_key'],
					'disable_gmaps_api' => in_array('disable_backend', $this->plugin_settings['remove_gmaps_api']) ? true : false,
					'disable_gmap3_plugin' => false,
					'disable_livequery' => false,
					'address_field_name' => CSPM_ADDRESS_FIELD,
					'lat_field_name' => CSPM_LATITUDE_FIELD,
					'lng_field_name' => CSPM_LONGITUDE_FIELD,
					'split_values' => true,
					'map_height' => '250px',
					'map_width' => '100%',
					'map_center' => $this->plugin_settings['map_center'],
					'map_zoom' => 12,
					'labels' => array(
						'address' => 'Enter a location & search or Geolocate your position',						
						'latitude' => 'Latitude',
						'longitude' => 'Longitude',
						'search' => 'Search',
						'pinpoint' => 'Get pinpoint',
					),
					'secondary_latlng' => array(
						'show' => true,
						'field_name' => CSPM_SECONDARY_LAT_LNG_FIELD
					)
				),
			);

			return $fields;
			
		}
		
		
		/**
		 * Marker icon Fields 
		 *
		 * @since 1.0 
		 */
		function cspm_post_marker_fields(){
			
			$fields = array();
			
			$fields[] = array(
				'name' => 'Marker',
				'desc' => '',
				'type' => 'title',
				'id'   => $this->metafield_prefix . '_marker',
				'attributes' => array(
					'style' => 'font-size:20px; color:#008fed; font-weight:400;'
				),
			);
			 
			$fields[] = array(				
				'id' => CSPM_MARKER_ICON_FIELD,
				'name' => 'Marker icon',
				'desc' => 'By default, the marker image to be used for this post is the one set in your map settings 
						   under <strong>"Marker settings => Marker image"</strong>. Uplaoding an image in this field will override the default marker image and also 
						   the one uploaded for this post category in <strong>"Marker categories settings"</strong>.',
				'type' => 'file',
				'text' => array(
					'add_upload_file_text' => 'Upload marker icon',
				),
				'preview_size' => array(32, 32),
				'query_args' => array(
					'type' => 'image',
				),
			);
		
			$fields[] = array(
				'id' => $this->metafield_prefix . '_marker_icon_height',
				'name' => 'Marker image height', 
				'desc' => 'Specify the image height (in pixels). Set to -1 to ignore or to automatically calculate the height (for PNG/JPEG/GIF images).<br />
						  <span style="color:red;"><strong>Note: When set to -1, the image height will be automatically calculated but only if the image type is PNG/JPEG/GIF.
						  If you\'re using SVG icons, the image height will not be calculated and you may need to specify it.</strong></span>',
				'type' => 'text',
				'default' => '',
				'attributes' => array(
					'type' => 'number',
					'pattern' => '\d*',
					'min' => '-1',
				),
				'sanitization_cb' => function($value, $field_args, $field){
					return ($value == '0') ? '-1' : $value;
				}	
			);
			
			$fields[] = array(
				'id' => $this->metafield_prefix . '_marker_icon_width',
				'name' => 'Marker image width', 
				'desc' => 'Specify the image width (in pixels). Set to -1 to ignore or to automatically calculate the width (for PNG/JPEG/GIF images).<br />
						  <span style="color:red;"><strong>Note: When set to -1, the image width will be automatically calculated but only if the image type is PNG/JPEG/GIF.
						  If you\'re using SVG icons, the image width will not be calculated and you may need to specify it.</strong></span>',
				'type' => 'text',
				'default' => '',
				'attributes' => array(
					'type' => 'number',
					'pattern' => '\d*',
					'min' => '-1',
				),
				'sanitization_cb' => function($value, $field_args, $field){
					return ($value == '0') ? '-1' : $value;
				}	
			);

			$fields[] = array(
				'id'   => CSPM_SINGLE_POST_IMG_ONLY_FIELD,
				'name' => 'Use this image for single post only?',
				'desc' => 'Display the "Marker icon" only on this post\'s single maps.',
				'type' => 'radio_inline',
				'options' => array(
					'yes' => 'Yes',
					'no' => 'No',
				),
				'default' => 'no',
			);
				
			$fields[] = array(
				'id' => $this->metafield_prefix . '_marker_label_options',
				'name' => 'Marker label options', 
				'desc' => 'A marker label is a letter or number that appears inside a marker. 
						  <br /><br />
						  <span style="color:red">The below options will allow you to override the default marker labels options set in your map settings (Check <strong>"Marker labels settings"</strong>) but 
						  you need at least to enable marker labels by editing your map and selecting the option <strong>"Marker labels settings => Use marker labels => Yes"</strong>. 
						  <br />For example, if you don\'t want to display the marker label for this specific post, select the option <strong>"Hide marker label => No"</strong>. The same 
						  thing goes for the color & font options.</span><br />
						  <br /><strong style="color:red">To add the marker label, enter a text in the field "Text"</strong>.',
				'type' => 'group',
				'repeatable' => false,
				'options' => array(
					'group_title' => __( 'Marker label', 'cspm' ),
					'sortable' => false,
					'closed' => true,
				),
				'fields' => array(	
					array(
						'id'   => 'hide_label',
						'name' => 'Hide marker label',
						'desc' => 'This option specify the appearance of marker labels. Default "No".',
						'type' => 'radio_inline',
						'options' => array(
							'yes' => 'Yes',
							'no' => 'No',
						),
						'default' => 'no',
					),
					array(
						'id' => 'text',
						'name' => 'Label text', 
						'desc' => 'Enter the text to be displayed in the label.',
						'type' => 'text',
						'default' => '',
					),	
					array(
						'id' => 'label_origin',
						'name' => 'Label Origin',
						'desc' => 'Specify the origin of the label relative to the top-left corner of the marker image. <strong>By default, the origin is located in the center point of the image.</strong>',
						'type' => 'title',
						'attributes' => array(
							'style' => 'font-size:15px; color:#ff6600; font-weight:600;'
						),
					),
					array(
						'id' => 'top',
						'name' => 'Top position', 
						'desc' => '',
						'type' => 'text',
						'default' => '',
						'attributes' => array(
							'type' => 'number',
							'pattern' => '\d*',
							'min' => '0',
						),
					),
					array(
						'id' => 'left',
						'name' => 'Left position', 
						'desc' => '',
						'type' => 'text',
						'default' => '',
						'attributes' => array(
							'type' => 'number',
							'pattern' => '\d*',
							'min' => '0',
						),
					),
					array(
						'id' => 'label_style',
						'name' => 'Label Style',
						'desc' => 'Specify the appearance of the marker labels.',
						'type' => 'title',
						'attributes' => array(
							'style' => 'font-size:15px; color:#ff6600; font-weight:600;'
						),
					),
					array(
						'id' => 'color',
						'name' => 'Color',
						'desc' => 'The color of the label text. Default color is  the one set in your map settings in the field "Marker label settings => Color".<br />
								  <span style="color:red">Leave this field empty if your don\'t want to override the default settings!</span><br />
								  Find more about the <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/color" target="_blank">color</a> property.',
						'type' => 'colorpicker',
						'default' => '',															
					),
					array(
						'id' => 'fontFamily',
						'name' => 'Font family', 
						'desc' => 'The font family of the label text (equivalent to the CSS font-family property). Default size is  the one set in your map settings in the field "Marker label settings => Font family".<br />
								  <span style="color:red">Leave this field empty if your don\'t want to override the default settings!</span><br />
								  Find more about the <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-family" target="_blank">font-family</a> property.',
						'type' => 'text',
						'default' => '',
					),	
					array(
						'id' => 'fontWeight',
						'name' => 'Font weight', 
						'desc' => 'The font weight of the label text (equivalent to the CSS font-weight property). Default size is  the one set in your map settings in the field "Marker label settings => Font weight".<br />
								  <span style="color:red">Leave this field empty if your don\'t want to override the default settings!</span><br />
								  Find more about the <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight" target="_blank">font-weight</a> property.',
						'type' => 'text',
						'default' => '',
					),	
					array(
						'id' => 'fontSize',
						'name' => 'Font size', 
						'desc' => 'The font size of the label text (equivalent to the CSS font-size property). Default size is the one set in your map settings in the field "Marker label settings => Font size".<br />
								  <span style="color:red">Leave this field empty if your don\'t want to override the default settings!</span><br />
								  Find more about the <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-size" target="_blank">font-size</a> property.',
						'type' => 'text',
						'default' => '',
					),					
				)
			);

			return $fields;
			
		}
				
		/**
		 * Post Format & Media Fields 
		 *
		 * @since 1.0 
		 */
		function cspm_post_format_and_media(){
			
			$fields = array();
			
			$fields[] = array(
				'name' => 'Format & Media',
				'desc' => 'Set your post format & open it inside modal. You can pop-up the single post or the media files only. Choose your post/location format and upload the media files.',
				'type' => 'title',
				'id'   => $this->metafield_prefix . '_format_and_media',
				'attributes' => array(
					'style' => 'font-size:20px; color:#008fed; font-weight:400;'
				),
			);
			
			$fields[] = array(
				'id' => $this->metafield_prefix . '_post_format',
				'name' => 'Format',
				'desc' => 'Select the format. Default "Standard". By clicking on the post marker, the post link or media files will be opened inside a modal/popup!',
				'type' => 'radio',				
				'options' => array(
					'standard' => 'Standard',
					'link' => 'Link (Single post page)',
					'gallery' => 'Gallery',
					'image' => 'Image',
					'audio' => 'Audio',
					'embed' => 'Embed (Video, Audio playlist, Website and more)',
					'all' => 'All',
				),
				'default' => 'standard',
			);
			
			/**
			 * Gallery */
			  
			$fields[] = array(
				'name' => 'Gallery',
				'id' => $this->metafield_prefix . '_post_gallery',
				'type' => 'file_list',
				'desc' => 'Upload images. You can sort image by draging an image to the location where you want it to be.<br />
						   <span style="color:red;">To be used with the format "Gallery" or "All"!</span>',
				'text' => array(
					'add_upload_files_text' => 'Upload image(s)',
				),
				'preview_size' => array( 70, 50 ),
				'query_args' => array(
					'type' => 'image',
				)		
			);
			
			/**
			 * Image */
			 
			$fields[] = array(
				'name' => 'Image',
				'id' => $this->metafield_prefix . '_post_image',
				'type' => 'file',
				'desc' => 'Add or upload the image. <span style="color:red;">To be used with the format "Image" or "All"!</span>',
				'text' => array(
					'add_upload_file_text' => 'Upload image',
				),
				'preview_size' => array( 100, 100 ),
				'query_args' => array(
					'type' => 'image',
				),
				'options' => array(
					'url' => false
				),		
			);
			
			/**
			 * Audio */
			 
			$fields[] = array(
				'name' => 'Audio',
				'id' => $this->metafield_prefix . '_post_audio',
				'type' => 'file',
				'desc' => 'Add or upload the audio file. <span style="color:red;">To be used with the format "Audio" or "All"!</span>',
				'text' => array(
					'add_upload_file_text' => 'Upload audio file',
				),
				'preview_size' => array( 100, 100 ),
				'query_args' => array(
					'type' => 'audio',
				)		
			);
			
			/**
			 * Embed */
			 
			$fields[] = array(
				'name' => 'Embed URL',
				'desc' => 'Enter the URL that should be embedded. It could be a video (Youtube, vimeo ...) URL, Audio playlist URL, Website URL and more. <span style="color:red;">To be used with the format "Embed" or "All"!</span><br />
						  <span style="color:red">Check the list of supported services <a href="http://codex.wordpress.org/Embeds" target="_blank">http://codex.wordpress.org/Embeds</a>.<br />
						  If the URL is a valid url to a supported provider, the plugin will use the embed code provided to it. Otherwise, it will be ignored!</span>',
				'id' => $this->metafield_prefix . '_post_embed',
				'type' => 'text_url',
			);

			return $fields;
						 
		}

		
		/**
		 * This will get the default value of field based on the one selected in the plugin settings.
		 * If a field has a default value in the plugin settings, we'll use, otherwise, we'll use a given default value instead.
		 *
		 * @since 1.0
		 */
		function cspm_get_field_default($option_id, $default_value = ''){
			
			/**
			 * We'll check if the default settings can be found in the array containing the "(shared) plugin settings".
			 * If found, we'll use it. If not found, we'll use the one in [@default_value] instead. */
			 
			$default = $this->cspm_setting_exists($option_id, $this->plugin_settings, $default_value);
			
			return $default;
			
		}
		
		
		/**
		 * Check if array_key_exists and if empty() doesn't return false
		 * Replace the empty value with the default value if available 
		 * @empty() return false when the value is (null, 0, "0", "", 0.0, false, array())
		 *
		 * @since 1.0
		 */
		function cspm_setting_exists($key, $array, $default = ''){
			
			$array_value = isset($array[$key]) ? $array[$key] : $default;
			
			$setting_value = empty($array_value) ? $default : $array_value;
			
			return $setting_value;
			
		}

	}
	
}	
		
