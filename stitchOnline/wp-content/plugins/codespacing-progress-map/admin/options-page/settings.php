<?php

/**
 * This class contains all the fields used for the plugin settings 
 *
 * @version 1.0 
 */
 
if(!defined('ABSPATH')){
    exit; // Exit if accessed directly
}

if( !class_exists( 'CspmSettings' ) ){
	
	class CspmSettings{
		
		private $plugin_path;
		private $plugin_url;
		
		private static $_this;	
		
		public $object_type;
		protected $options_key;
		protected $plugin_version;
				
		function __construct($atts = array()){
			
			extract( wp_parse_args( $atts, array(
				'plugin_path' => '', 
				'plugin_url' => '',
				'object_type' => '',
				'option_key' => '',
				'version' => '',
			)));
             
			self::$_this = $this;       
				           
			$this->plugin_path = $plugin_path;
			$this->plugin_url = $plugin_url;
			$this->object_type = $object_type;
			$this->options_key = $option_key;
			$this->plugin_version = $version;
				
			/**
			 * Include all required Libraries for this metabox */
			 
			$libs_path = array(
				'cspm' => 'admin/libs/metabox/init.php',
				'cmb2-tabs' => 'admin/libs/metabox-tabs/cmb2-tabs.class.php',
				'cmb2-radio-image' => 'admin/libs/metabox-radio-image/metabox-radio-image.php',
				'cs-button' => 'admin/libs/metabox-cs-button-field/cs-button-field.php',
			);
				
				foreach($libs_path as $lib_file_path){
					if(file_exists($this->plugin_path . $lib_file_path))
						require_once $this->plugin_path . $lib_file_path;
				}

			
			add_action('cmb2_admin_init', array(&$this, 'cspm_settings_page'));
			
			/**
			 * Call .js and .css files */
			 
			add_filter( 'cmb2_enqueue_js', array(&$this, 'cspm_scripts') );				
			
		}
	

		static function this() {
			
			return self::$_this;
		
		}

	
		function cspm_scripts(){
			
			if(isset($_GET['page']) && $_GET['page'] == $this->options_key){
			
				/**
				 * CSS */
				 
				wp_register_style('cspm-cmb2-tabs-css', $this->plugin_url . 'admin/libs/metabox-tabs/css/cmb2-options-page-tabs.css', array(), '1.0.1');
				wp_enqueue_style('cspm-cmb2-tabs-css');
				
				/**
				 * Our custom metaboxes CSS */
				
				wp_register_style('cspm-metabox-css', $this->plugin_url . 'admin/options-page/css/options-page-style.css');
				wp_enqueue_style('cspm-metabox-css');
		
				/**
				 * js */
	
				wp_register_script('cspm-cmb2-tabs-js', $this->plugin_url . 'admin/libs/metabox-tabs/js/cmb2-tabs.js', array(), '1.0.1', true);
				wp_enqueue_script('cspm-cmb2-tabs-js');
				
				/**
				 * Hide the menu "Settings" created by CMB2 */
				
				wp_add_inline_style('cspm-metabox-css', 'li#toplevel_page_cspm_default_settings > ul.wp-submenu li.current:not(.wp-first-item){display:none;}');	

			}
			
		}
	
		
		/**
		 * "Progress Map" settings page.
		 * This will contain all the default settings needed for "Progress Map"
		 *
		 * @since 1.0
		 */
		function cspm_settings_page(){
			
			$cspm_settings_page_options = array(
				'id' => 'cspm_settings_page',
				'title' => esc_html__('Progress Map', 'cspm'),
				'description' => 'description',
				'object_types' => array('options-page'),
				
				/*
				 * The following parameters are specific to the options-page box
				 * Several of these parameters are passed along to add_menu_page()/add_submenu_page(). */
				
				'option_key' => $this->options_key,
				'menu_title' => esc_html__('Settings', 'cspm'), // Falls back to 'title' (above).
				'icon_url' => $this->plugin_url.'admin/options-page/img/menu-icon.png',				
				'parent_slug' => 'cspm_default_settings', // Make options page a submenu item of the themes menu.
				'capability' => 'manage_options', // Cap required to view options-page.
				//'position' => '99.2', // Menu position. Only applicable if 'parent_slug' is left empty.
				//'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
				'display_cb'   => array(&$this, 'cspm_settings_page_output'), // Override the options-page form output (CMB2_Hookup::options_page_output()).
				// 'save_button' => esc_html__( 'Save Theme Options', 'cspm' ), // The text for the options-page save button. Defaults to 'Save'.
				// 'disable_settings_errors' => true, // On settings pages (not options-general.php sub-pages), allows disabling.
				// 'message_cb' => 'yourprefix_options_page_message_callback',				
			);
		
			/**
			 * Create Progress Map settings page */
				 
			$cspm_settings_page = new_cmb2_box($cspm_settings_page_options);

			/**
			 * Display Progress Map settings fields */
		
			$this->cspm_settings_tabs($cspm_settings_page, $cspm_settings_page_options);
			
		}
		
		/** 
		 * This contains the HTML structre of the plugin settings page
		 *
		 * @since 1.0
		 */
		function cspm_settings_page_output($settings_page_obj){
        	
			echo '<div class="wrap cmb2-options-page option-'.$settings_page_obj->option_key.'">';
				
				/**
				 * Header */
				 
				echo $this->cspm_settings_page_header();
				
				echo '<div class="cspm_body">';
				
					echo '<form class="cspm-settings-form" action="'.esc_url(admin_url('admin-post.php')).'" method="POST" id="'.$settings_page_obj->cmb->cmb_id.'" enctype="multipart/form-data" encoding="multipart/form-data">';
						
						echo '<input type="hidden" name="action" value="'.esc_attr($settings_page_obj->option_key).'">';
						
						submit_button(esc_attr($settings_page_obj->cmb->prop('save_button')), 'primary', 'submit-cmb');
					
						$settings_page_obj->options_page_metabox();
						
						submit_button(esc_attr($settings_page_obj->cmb->prop('save_button')), 'primary', 'submit-cmb');
					
					echo '</form>';
								
				echo '</div>';
					
				/**
				 * Sidebar */
				 
				echo $this->cspm_settings_page_widget();
				
				/**
				 * Footer */
				 
				echo $this->cspm_settings_page_footer();
				
            echo '</div>';
				
			echo '<div style="clear:both;"></div>';
			
		}
		
		
		/**
		 * Plugin settings page header
		 *
		 * @since 1.0
		 */
		function cspm_settings_page_header(){
			
			$output = '<div class="cspm_header"><img src="'.$this->plugin_url.'admin/options-page/img/progress-map.png" /></div>';
			
			/**
			 * Inform about the new pricing changes of Google Maps
			 * @since 3.8.1 */

			global $current_user;	
    	 
		    $user_id = $current_user->ID;
	
			if(get_user_meta($user_id, 'cspm_dismiss_gmaps_updates_notice') && current_user_can('activate_plugins')){
				
				$output .= '<div class="notice notice-error" style="background: #fff3e0; color: #dd2c00; width:100%; box-sizing:border-box; margin:0; box-shadow: none; border-left-width: 2px;">
					<p><strong>Reminder:</strong> To use Google Maps APIs:</p>
					<ul style="padding-left:15px;">
						<li><strong>. Starting June 11, 2018:</strong> All Google Maps Platform (GMP) API requests must include an API key; GMP no longer support keyless access.</li>
						<li><strong>. Starting July 16, 2018:</strong> You\'ll need to enable billing on each of your projects.</li>			
					</ul>
					<p><strong>Show <a href="'.add_query_arg('cspm_dismiss_gmaps_updates_notice', 0, $_SERVER['REQUEST_URI']).'" style="color: #dd2c00; text-decoration:underline;">the announcement</a> for more information.</strong></p>
				</div>';
				
			}
			
			return $output;
			
		}
		
		
		/**
		 * Plugin settings page sidebar
		 *
		 * @since 1.0
		 */
		function cspm_settings_page_widget(){
			
			if(class_exists('ProgressMapList') 
			&& class_exists('CspmNearbyMap')
			&& class_exists('CspmSubmitLocations'))
				return '';
			
			$output = '<div class="cspm-settings-widget">';
				
				$output .= '<div class="cspm_widget">';
				
					$output .= '<h3>Extend "Progress Map" with these awesome and powerful add-ons:</h3>';
					
					$output .= '<div class="widget_content">';
					
						/**
						 * Progress Map List & Filter Ban */
						
						if(!class_exists('ProgressMapList'))
							$output .= '<div style="float:left; margin-right:20px;"><a target="_blank" href="https://codecanyon.net/item/progress-map-list-filter-wordpress-plugin/16134134?ref=codespacing"><img src="'.$this->plugin_url.'admin/options-page/img/list-and-filter-thumb.jpg" /></a></div>';
						
						/**
						 * Nearby Places Ban */
						
						if(!class_exists('CspmNearbyMap'))
							$output .= '<div style="float:left; margin-right:20px;"><a target="_blank" href="https://codecanyon.net/item/nearby-places-wordpress-plugin/15067875?ref=codespacing"><img src="'.$this->plugin_url.'admin/options-page/img/nearby-places-thumb.png" /></a></div>';
						
						/**
						 * Submit Locations Ban */
						
						if(!class_exists('CspmSubmitLocations'))
							$output .= '<div style="float:left; margin-right:20px;"><a target="_blank" href="https://codecanyon.net/item/progress-map-submit-location-wordpress-plugin/21974786?ref=codespacing"><img src="'.$this->plugin_url.'admin/options-page/img/submit-locations-thumb.jpg" /></a></div>';
						
						$output .= '<div style="clear:both;"></div>';
				
					$output .= '</div>';
				
				$output .= '</div>';
				
			$output .= '</div>';
			
			$output .= '<div style="clear:both;"></div>';
			
			return $output;
			
		}
		
				
		/**
		 * Plugin settings page footer
		 *
		 * @since 1.0
		 */
		function cspm_settings_page_footer(){
			
			$output = '<div class="cspm_footer">';
			
				$output .= '<div class="cspm_rates_fotter">
					<a target="_blank" href="https://codecanyon.net/item/progress-map-wordpress-plugin/5581719">
						<img src="'.$this->plugin_url.'admin/options-page/img/rates.jpg" />
					</a>
				</div>';
								
				$output .= '<div style="clear:both;"></div>';
				
				$output .= '<div class="cspm_copyright">&copy; All rights reserved <a target="_blank" href="https://codespacing.com">CodeSpacing</a>. Progress Map '.$this->plugin_version.'</div>';
			
			$output .= '</div>';
			
			return $output;
			
		}
		
		
		/**
		 * Buill all the tabs that contains "Progress Map" settings
		 *
		 * @since 1.0
		 */
		function cspm_settings_tabs($metabox_object, $metabox_options){
			
			/**
			 * Setting tabs */
			 
			$tabs_setting = array(
				'args' => $metabox_options,
				'tabs' => array()
			);
				
				/**
				 * Tabs array */
				 
				$cspm_tabs = array(
					
					/**
				 	 * Plugin Settings */
					 					
					array(
						'id' => 'plugin_settings', 
						'title' => 'Plugin settings', 
						'callback' => 'cspm_plugin_settings_fields'
					),
					
					/**
				 	 * Default Map Settings */
					 					
					array(
						'id' => 'map_settings', 
						'title' => 'Default Map settings', 
						'callback' => 'cspm_map_fields'
					),
					
					/**
				 	 * Default Map Style Settings */
					 					
					array(
						'id' => 'map_style_settings', 
						'title' => 'Default Map style settings', 
						'callback' => 'cspm_map_style_fields'
					),
					
					/**
					 * Default Marker settings */
					 
					array(
						'id' => 'marker_settings', 
						'title' => 'Default Marker settings', 
						'callback' => 'cspm_marker_fields'
					),
					
					/**
					 * Default Clustering settings */
					 
					array(
						'id' => 'clustering_settings', 
						'title' => 'Default Marker Clustering settings', 
						'callback' => 'cspm_clustering_fields'
					),
					
					/**
					 * Default Geotargeting settings */
					 					
					array(
						'id' => 'geotargeting_settings', 
						'title' => 'Default Geo-targeting settings', 
						'callback' => 'cspm_geotargeting_fields'
					),
					
					/**
				 	 * Default Infobox Settings */
					 					
					array(
						'id' => 'infobox_settings', 
						'title' => 'Default Infobox settings', 
						'callback' => 'cspm_infobox_fields'
					),
					
					/**
				 	 * Customize */
					 										
					array(
						'id' => 'customize', 
						'title' => 'Customize', 
						'callback' => 'cspm_customize_fields'
					),

					/**
				 	 * Troubleshooting & configs */
					 										
					array(
						'id' => 'troubleshooting', 
						'title' => 'Troubleshooting', 
						'callback' => 'cspm_troubleshooting_fields'
					),
					
				);
				
				foreach($cspm_tabs as $tab_data){
				 
					$tabs_setting['tabs'][] = array(
						'id'     => 'cspm_' . $tab_data['id'],
						'title'  => '<span class="cspm_tabs_menu_image"><img src="'.$this->plugin_url.'admin/img/'.str_replace('_', '-', $tab_data['id']).'.png" style="width:20px;" /></span> <span class="cspm_tabs_menu_item">'.__( $tab_data['title'], 'cspm' ).'</span>',						
						'fields' => call_user_func(array(&$this, $tab_data['callback'])),
					);
		
				}
			
			/**
			 * Set tabs */
			 
			$metabox_object->add_field( array(
				'id'   => 'cspm_pm_settings_tabs',
				'type' => 'tabs',
				'tabs' => $tabs_setting
			) );
			
			return $metabox_object;
			
		}
		
		
		/**
		 * Map Settings Fields 
		 *
		 * @since 1.0 
		 */
		function cspm_plugin_settings_fields(){
			
			$fields = array();
			
			$fields[] = array(
				'name' => 'Plugin Settings',
				'desc' => '',
				'type' => 'title',
				'id'   => 'plugin_settings',
				'attributes' => array(
					'style' => 'font-size:20px; color:#008fed; font-weight:400;'
				),
			);
			
			$fields[] = array(
				'id' => 'post_types_section',
				'name' => 'Post Types Parameters',
				'desc' => 'Select the post types for which Progress Map should be available during post creation/edition.',
				'type' => 'title',
				'attributes' => array(
					'style' => 'font-size:15px; color:#ff6600; font-weight:600;'
				),				
			);
			
				$fields[] = array(
					'id' => 'post_types',
					'name' => 'Post types',
					'desc' => 'Select the post types for which Progress Map should be available during post creation/edition. (Default: '.__('Posts').').',
					'type' => 'multicheck',
					'options' => $this->cspm_get_registred_cpts(),
					'sanitization_cb' => function($value, $field_args, $field){
						if(is_array($value) && count($value) > 0)
							return $value;
						else return array('');
					}					
				);
				
			$fields[] = array(
				'id' => 'form_fields_section',
				'name' => '"Add location" Form fields',
				'desc' => 'IMPORTANT NOTE!<br /><br />This feature is dedicated ONLY for users that want to use the plugin with their already created cutsom fields.
						   You don\'t have any interest to change the name of the form fields below if you will use the plugin with a new post type/website. Just leave them as they are!
						   <br />The "Add location" form is located in the "Add/Edit post" page of your post type.<br /><br />*After CHANGING the Latitude & Longitude fields names, 
						   SAVE your settings, then use the "Regenerate markers" option above to recreate your markers.',
				'type' => 'title',
				'attributes' => array(
					'style' => 'font-size:15px; color:#ff6600; font-weight:600;'
				),				
			);
			
				$fields[] = array(
					'id' => 'latitude_field_name',
					'name' => '"Latitude" field name',
					'desc' => 'Enter the name of your latitude custom field. Empty spaces are not allowed!',
					'type' => 'text',
					'default' => 'codespacing_progress_map_lat',
				);
				
				$fields[] = array(
					'id' => 'longitude_field_name',
					'name' => '"Longitude" field name',
					'desc' => 'Enter the name of your longitude custom field. Empty spaces are not allowed!',
					'type' => 'text',
					'default' => 'codespacing_progress_map_lng',
				);
			
			$fields[] = array(
				'id' => 'misc_fields_section',
				'name' => 'Other parameters',
				'desc' => '',
				'type' => 'title',
				'attributes' => array(
					'style' => 'font-size:15px; color:#ff6600; font-weight:600;'
				),				
			);
			
				$fields[] = array(
					'id' => 'outer_links_field_name',
					'name' => '"Outer links" custom field name',
					'desc' => 'By default, the plugin uses the function get_permalink() of wordpress to get the posts links. <strong>In some cases, users wants to use their locations
							   in the map as links to other pages in outer websites.</strong> To use this option, you MUST have your external <strong>links stored in a custom field</strong>. Enter the name of that
							   custom field or leave this field empty if you don\'t need this option.',
					'type' => 'text',
					'default' => '',
				);
				
				$fields[] = array(
					'id' => 'custom_list_columns',
					'name' => 'Display the coordinates column',
					'desc' => 'This will display a custom column for all custom post types used with the plugin indicating the coordinates of each post on the map.',
					'type' => 'radio',
					'default' => 'no',
					'options' => array(
						'yes' => 'Yes',
						'no' => 'No'
					)
				);
				
				$fields[] = array(
					'id' => 'use_with_wpml',
					'name' => 'Use the plugin with WPML <sup>BETA</sup>',
					'desc' => 'If you are using WPML plugin for translation, select "Yes" to enable the WPML compatibility code.<br />
							  <span style="color:red">Note: After duplicating a post to other languages, you must click one more time on the button "Update" of the original 
							  post in order to add the LatLng coordinates for the duplicated posts on the map.</span>',
					'type' => 'radio',
					'default' => 'no',
					'options' => array(
						'yes' => 'Yes',
						'no' => 'No'
					)
				);		

			return $fields;
			
		}
		
		
		/**
		 * Map Settings Fields 
		 *
		 * @since 1.0 
		 */
		function cspm_map_fields(){
			
			$fields = array();
			
			$fields[] = array(
				'name' => 'Default Map Settings',
				'desc' => 'These are the default map settings that are going to be used as the main settings of all single maps and as the default settings of your new maps. 
						   You can always override the default map settings when creating/editing a map.',
				'type' => 'title',
				'id'   => 'map_settings',
				'attributes' => array(
					'style' => 'font-size:20px; color:#008fed; font-weight:400;'
				),
			);
		
			$fields[] = array(
				'id' => 'api_key',
				'name' => 'API Key',
				/**
				 * https://developers.google.com/maps/signup?csw=1#google-maps-javascript-api */
				'desc' => 'Enter your Google Maps API key.<br />
						  <a href="https://developers.google.com/maps/documentation/javascript/get-api-key#get-an-api-key" target="_blank">Get an API key</a>',
				'type' => 'text',
				'default' => '',
			);
				
			$fields[] = array(
				'id' => 'map_language',
				'name' => 'Map language',
				'desc' => 'Localize your Maps API application by altering default language settings. See also the <a href="https://developers.google.com/maps/faq#languagesupport" target="_blank">supported list of languages</a>.',
				'type' => 'text',
				'default' => 'en'		
			);

			$fields[] = array(
				'id' => 'map_center',
				'name' => 'Map center',
				'desc' => 'Provide the center point of the map. (Latitude then Longitude separated by comma). Refer to <a href="https://maps.google.com/" target="_blank">https://maps.google.com/</a> to get you center point.',
				'type' => 'text',
				'default' => '51.53096,-0.121064',		
			);
				
			$fields[] = array(
				'id' => 'map_zoom',
				'name' => 'Map zoom',
				'desc' => 'Select the map zoom. <span style="color:red;">The map zoom will be ignored if you activate the option (below) <strong>"Autofit"</strong>!</span>',
				'type' => 'select',
				'default' => '12',
				'options' => array(
					'0' => '0',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
					'7' => '7',
					'8' => '8',
					'9' => '9',
					'10' => '10',
					'11' => '11',
					'12' => '12',
					'13' => '13',
					'14' => '14',
					'15' => '15',
					'16' => '16',
					'17' => '17',
					'18' => '18',
					'19' => '19'
				)
			);
				
			$fields[] = array(
				'id' => 'max_zoom',
				'name' => 'Max. zoom',
				'desc' => 'Select the maximum zoom of the map.',
				'type' => 'select',
				'default' => '19',
				'options' => array(
					'0' => '0',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
					'7' => '7',
					'8' => '8',
					'9' => '9',
					'10' => '10',
					'11' => '11',
					'12' => '12',
					'13' => '13',
					'14' => '14',
					'15' => '15',
					'16' => '16',
					'17' => '17',
					'18' => '18',
					'19' => '19'
				)
			);
				
			$fields[] = array(
				'id' => 'min_zoom',
				'name' => 'Min. zoom',
				'desc' => 'Select the minimum zoom of the map. <span style="color:red;">The Min. zoom should be lower than the Max. zoom!</span>',
				'type' => 'select',
				'default' => '0',
				'options' => array(
					'0' => '0',
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
					'7' => '7',
					'8' => '8',
					'9' => '9',
					'10' => '10',
					'11' => '11',
					'12' => '12',
					'13' => '13',
					'14' => '14',
					'15' => '15',
					'16' => '16',
					'17' => '17',
					'18' => '18',
					'19' => '19'
				)
			);
				
			$fields[] = array(
				'id' => 'zoom_on_doubleclick',
				'name' => 'Zoom on double click',
				'desc' => 'Enable/Disable zooming and recentering the map on double click. Defaults to "Disable".',
				'type' => 'radio',
				'default' => 'true',
				'options' => array(
					'false' => 'Enable',
					'true' => 'Disable'
				)
			);
				
			$fields[] = array(
				'id' => 'map_draggable',
				'name' => 'Draggable',
				'desc' => 'If Yes, prevents the map from being dragged. Dragging is enabled by default.',
				'type' => 'radio',
				'default' => 'true',
				'options' => array(
					'true' => 'Yes',
					'false' => 'No'
				)
			);
				
			$fields[] = array(
				'id' => 'autofit',
				'name' => 'Autofit',
				'desc' => 'This option extends map bounds to contain all markers & clusters. <span style="color:red;">By activating this option, the map zoom will be ignored!</span><br />
						   <strong style="color:red;">The Minimum zoom level is taken into consideration when using this option. Make sure to set the map\'s minimum zoom to a level that allows displaying all items on the map!</strong>',
				'type' => 'radio',
				'default' => 'false',
				'options' => array(
					'true' => 'Yes',
					'false' => 'No'
				)
			);
				
			$fields[] = array(
				'id' => 'traffic_layer',
				'name' => 'Traffic Layer',
				'desc' => 'Display current road traffic.',
				'type' => 'radio',
				'default' => 'false',
				'options' => array(
					'true' => 'Yes',
					'false' => 'No'
				)
			);
				
			$fields[] = array(
				'id' => 'transit_layer',
				'name' => 'Transit Layer',
				'desc' => 'Display local Transit route information.',
				'type' => 'radio',
				'default' => 'false',
				'options' => array(
					'true' => 'Yes',
					'false' => 'No'
				)
			);

			$fields[] = array(
				'id' => 'recenter_map',
				'name' => 'Recenter Map',
				'desc' => 'Show a button on the map to allow recentring the map. Defaults to "Yes".',
				'type' => 'radio',
				'default' => 'true',
				'options' => array(
					'true' => 'Yes',
					'false' => 'No'
				)
			);
				
			$fields[] = array(
				'id' => 'retinaSupport',
				'name' => 'Retina support',
				'desc' => 'Enable retina support for custom markers & Clusters images. When enabled, make sure the uploaded image is twice the size you want it to be displayed on the map. 
						   For example, if you want the marker/cluster image in the map to be displayed as 20x30 pixels, upload an image with 40x60 pixels.<br />
						   <span style="color:red;"><strong>Note: Retina will not be applied to SVG icons!</strong></span>',
				'type' => 'radio',
				'default' => 'false',
				'options' => array(
					'true' => 'Enable',
					'false' => 'Disable'
				),
				'attributes' => array(
					'data-conditional-id' => 'defaultMarker',
					'data-conditional-value' => wp_json_encode(array('customize')),								
				),									
			);

			$fields[] = array(
				'id' => 'ui_elements_section',
				'name' => 'UI elements',
				'desc' => 'The maps displayed through the Google Maps API contain UI elements to allow user interaction with the map. These elements are known as controls and you can include and/or customize variations of these controls in your map.',
				'type' => 'title',
				'attributes' => array(
					'style' => 'font-size:15px; color:#ff6600; font-weight:600;'
				),
			);
				
				$fields[] = array(
					'id' => 'mapTypeControl',
					'name' => 'Show map type control',
					'desc' => 'The MapType control lets the user toggle between map types (such as ROADMAP and SATELLITE). This control appears by default in the top right corner of the map.',
					'type' => 'radio',
					'default' => 'true',
					'options' => array(
						'true' => 'Yes',
						'false' => 'No'
					)
				);
					
				$fields[] = array(
					'id' => 'streetViewControl',
					'name' => 'Show street view control',
					'desc' => 'The Street View control contains a Pegman icon which can be dragged onto the map to enable Street View. This control appears by default in the right top corner of the map.',
					'type' => 'radio',
					'default' => 'false',
					'options' => array(
						'true' => 'Yes',
						'false' => 'No'
					)
				);
					
				$fields[] = array(
					'id' => 'scrollwheel',
					'name' => 'Scroll wheel',
					'desc' => 'Allow/Disallow the zoom-in and zoom-out of the map using the scroll wheel.',
					'type' => 'radio',
					'default' => 'false',
					'options' => array(
						'true' => 'Yes',
						'false' => 'No'
					)
				);
					
				$fields[] = array(
					'id' => 'zoomControl',
					'name' => 'Show zoom control',
					'desc' => 'The Zoom control displays a small "+/-" buttons to control the zoom level of the map. This control appears by default in the top left corner of the map on non-touch devices or in the bottom left corner on touch devices.',
					'type' => 'radio',
					'default' => 'true',
					'options' => array(
						'true' => 'Yes',
						'false' => 'No'
					)
				);
					
				$fields[] = array(
					'id' => 'zoomControlType',
					'name' => 'Zoom control Type',
					'desc' => 'Select the zoom control type.',
					'type' => 'radio',
					'default' => 'customize',
					'options' => array(
						'customize' => 'Customized type',
						'default' => 'Default type'
					)
				);

			return $fields;
			
		}
		
		
		/**
		 * Map Style Settings Fields 
		 *
		 * @since 1.0 
		 */
		function cspm_map_style_fields(){
			
			$fields = array();
			
			$fields[] = array(
				'name' => 'Map Style Settings',
				'desc' => 'Styled maps allow you to customize the presentation of the standard Google base maps, changing the visual display of such elements as roads, parks, and built-up areas. The lovely styles below are provided by <a href="http://snazzymaps.com" target="_blank">Snazzy Maps</a>. <br />
						   The following style settings are going to be used as the main style settings of all single maps and as the default style settings of your new maps. 
						   You can always override the default style settings when creating/editing a map',
				'type' => 'title',
				'id'   => 'map_style_settings',
				'attributes' => array(
					'style' => 'font-size:20px; color:#008fed; font-weight:400;'
				),
			);
				
			$fields[] = array(
				'id' => 'initial_map_style',
				'name' => 'Initial map style',
				'desc' => 'Select the initial map style.',
				'type' => 'radio',
				'default' => 'ROADMAP',
				'options' => array(
					'ROADMAP' => 'Road Map <sup>(Displays the default road map view)</sup>',
					'SATELLITE' => 'Satellite <sup>(Displays Google Earth satellite images)</sup>',
					'TERRAIN' => 'Terrain <sup>(Displays a physical map based on terrain information)</sup>',
					'HYBRID' => 'Hybrid <sup>(Displays a mixture of normal and satellite views)</sup>',
					'custom_style' => 'Custom style <sup>(Displays a custom style)</sup>'
				)
			);
					
			$fields[] = array(
				'id' => 'style_option',
				'name' => 'Style option', 
				'desc' => 'Select the style option of the map. <span style="color:red;">If you select the option <strong>Progress map styles</strong>, choose one of the available styles below.
						   If you select the option <strong>My custom style</strong>, enter your custom style code in the field <strong>Javascript Style Array</strong>.</span>',
				'type' => 'radio',
				'default' => 'progress-map',
				'options' => array(
					'progress-map' => 'Progress Map styles',
					'custom-style' => 'My custom style'
				),
			);
					
			$fields[] = array(
				'id' => 'map_style',
				'name' => 'Map style',
				'desc' => 'Select your map style.',
				'type' => 'radio',
				'default' => 'google-map',
				'options' => $this->cspm_get_all_map_styles(),
				'attributes' => array(
					'data-conditional-id' => 'style_option',
					'data-conditional-value' => wp_json_encode(array('progress-map')),								
				),													
			);
					
			$fields[] = array(
				'id' => 'custom_style_name',
				'name' => 'Custom style name',
				'desc' => 'Enter your custom style name. Defaults to "Custom style".',
				'type' => 'text',
				'default' => 'Custom style',
				'attributes' => array(
					'data-conditional-id' => 'style_option',
					'data-conditional-value' => wp_json_encode(array('custom-style')),								
				),																	
			);
					
			$fields[] = array(
				'id' => 'js_style_array',
				'name' => 'Javascript Style Array',
				'desc' => 'If you don\'t like any of the styles above, fell free to add your own style. Please include just the array definition. No extra variables or code.<br />
						  Make use of the following services to create your style:<br />
	  			          . <a href="https://mapstyle.withgoogle.com/" target="_blank">Google Maps APIs Styling Wizard</a><br />
						  . <a href="http://www.evoluted.net/thinktank/web-design/custom-google-maps-style-tool" target="_blank">Custom Google Maps Style Tool by Evoluted</a><br />
						  . <a href="http://software.stadtwerk.org/google_maps_colorizr/" target="_blank">Google Maps Colorizr by stadt werk</a><br />			  					  
						  You may also like to <a href="http://snazzymaps.com/submit" target="_blank">submit</a> your style for the world to see :)',
				'type' => 'textarea',
				'default' => '',
				'attributes' => array(
					'data-conditional-id' => 'style_option',
					'data-conditional-value' => wp_json_encode(array('custom-style')),								
				),																					
			);	

			return $fields;
			
		}
		
		
		/**
		 * Marker settings
		 * 
		 * @since 1.0
		 */
		function cspm_marker_fields(){
			
			$fields = array();
			
			$fields[] = array(
				'name' => 'Marker Settings',
				'desc' => 'The following marker settings are going to be used as the main marker settings of all single maps and as the default marker settings of your new maps. 
						   You can always override the default marker settings when creating/editing a map',
				'type' => 'title',
				'id'   => 'marker_settings',
				'attributes' => array(
					'style' => 'font-size:20px; color:#008fed; font-weight:400;'
				),
			);
				
			$fields[] = array(
				'id' => 'defaultMarker',
				'name' => 'Marker image type',
				'desc' => 'Select the marker image type.<br />
						  <span style="color:red;">Note: You can use SVG icons!</span>',
				'type' => 'radio',
				'default' => 'customize',
				'options' => array(
					'customize' => 'Customized image type',
					'default' => 'Default image type <sup>(Google Maps red marker)</sup>'
				)
			);
				
			$fields[] = array(
				'id' => 'markerAnimation',
				'name' => 'Marker animation',
				'desc' => 'You can animate a marker so that it exhibit a dynamic movement when it\'s been fired. To specify the way a marker is animated, select
						   one of the supported animations above.',
				'type' => 'radio',
				'default' => 'pulsating_circle',
				'options' => array(
					'pulsating_circle' => 'Pulsating circle',
					'bouncing_marker' => 'Bouncing marker',
					'flushing_infobox' => 'Flushing infobox <sup style="color:red;">(Use it only when <strong>Show infobox</strong> is set to <strong>Yes</strong>)</sup>'				
				)
			);
				
			$fields[] = array(
				'id' => 'marker_icon',
				'name' => 'Marker image',
				'desc' => 'Change the default custom marker image. By default, this image will be used for all markers. In case you need to restore the the original marker image, this one can be found in the plugin\'s images directory. 
						  <br /><strong>How to override the default marker image?</strong>
						  <br />1. You can override it by editing a specific post/location and uploading a custom image in the field "Marker => Marker icon". 
						  <br />2. You can also override it by uploading a custom image for a category of posts/locations. The options you need for this method can be found under the section "Marker categories settings".',
				'type' => 'file',
				'default' => $this->plugin_url.'img/svg/marker.svg',
			);
		
			$fields[] = array(
				'id' => 'marker_icon_height',
				'name' => 'Marker image height', 
				'desc' => 'Specify the image height (in pixels). Set to -1 to ignore or to automatically calculate the height (for PNG/JPEG/GIF images).<br />
						  <span style="color:red;"><strong>Note: When set to -1, the image height will be automatically calculated but only if the image type is PNG/JPEG/GIF.
						  If you\'re using SVG icons, the image height will not be calculated and you may need to specify it.</strong></span>',
				'type' => 'text',
				'default' => '32',
				'attributes' => array(
					'type' => 'number',
					'pattern' => '\d*',
					'min' => '-1',
				),
				'sanitization_cb' => function($value, $field_args, $field){
					return ($value == '0') ? '-1' : $value;
				}														
			);
			
			$fields[] = array(
				'id' => 'marker_icon_width',
				'name' => 'Marker image width', 
				'desc' => 'Specify the image width (in pixels). Set to -1 to ignore or to automatically calculate the width (for PNG/JPEG/GIF images).<br />
						  <span style="color:red;"><strong>Note: When set to -1, the image width will be automatically calculated but only if the image type is PNG/JPEG/GIF.
						  If you\'re using SVG icons, the image width will not be calculated and you may need to specify it.</strong></span>',
				'type' => 'text',
				'default' => '30',
				'attributes' => array(
					'type' => 'number',
					'pattern' => '\d*',
					'min' => '-1',
				),
				'sanitization_cb' => function($value, $field_args, $field){
					return ($value == '0') ? '-1' : $value;
				}										
			);

			$fields[] = array(
				'id' => 'marker_anchor_point_option',
				'name' => 'Set the anchor point',
				'desc' => 'Depending of the shape of the marker, you may not want the middle of the bottom edge to be used as the anchor point. 
						   In this situation, you need to specify the anchor point of the image. A point is defined with an X and Y value (in pixels). 
						   So if X is set to 10, that means the anchor point is 10 pixels to the right of the top left corner of the image. Setting Y to 10 means 
						   that the anchor is 10 pixels down from the top right corner of the image.',
				'type' => 'radio',
				'default' => 'disable',
				'options' => array(
					'auto' => 'Auto detect <sup>*Detects the center of the image.</sup>',
					'manual' => 'Manualy <sup>*Enter the anchor point in the next two fields.</sup>',
					'disable' => 'Disable'				
				)
			);
				
			$fields[] = array(
				'id' => 'marker_anchor_point',
				'name' => 'Marker anchor point',
				'desc' => 'Enter the anchor point of the Marker image. Seperate X and Y by comma. (e.g. 10,15)',
				'type' => 'text',
				'default' => '',
			);

			return $fields;
			
		}
		
		
		/**
		 * Clustering settings
		 *
		 * @since 1.0
		 */
		function cspm_clustering_fields(){
			
			$fields = array();
			
			$fields[] = array(
				'name' => 'Marker Clustering Settings',
				'desc' => 'Clustering simplifies your data visualization by consolidating data that are nearby each other on the map in an aggregate form. A cluster is displayed as a circle with a number on it. 
						   The number on a cluster indicates how many markers it contains. As you zoom into any of the cluster locations, the number on the cluster decreases, and you begin to see the individual markers on the map. 
						   Zooming out of the map consolidates the markers into clusters again.
						   <br />
						   The following marker clustering settings are going to be used as the default marker clustering settings of your new maps. 
						   You can always override the default marker clustering settings when creating/editing a map',
				'type' => 'title',
				'id'   => 'infobox_settings',
				'attributes' => array(
					'style' => 'font-size:20px; color:#008fed; font-weight:400;'
				),
			);
			
			$fields[] = array(
				'id' => 'useClustring',
				'name' => 'Use marker clustering',
				'desc' => 'Clustering simplifies your data visualization by consolidating data that are nearby each other on the map in an aggregate form. <span style="color:red;"><strong>Activating this option will significantly increase the loading speed of the map!</strong></span>',
				'type' => 'radio',
				'default' => 'true',
				'options' => array(
					'true' => 'Yes <span style="color:red;"><sup><strong>(Recommended)</strong></sup></span>',
					'false' => 'No'
				)
			);
				
			$fields[] = array(
				'id' => 'gridSize',
				'name' => 'Grid size',
				'desc' => 'Grid size or Grid-based clustering works by dividing the map into squares of a certain size (the size changes at each zoom) and then grouping the markers into each grid square.',
				'type' => 'text',
				'default' => '60',
				'attributes' => array(
					'type' => 'number',
					'pattern' => '\d*',
					'min' => '0'
				),						
			);
			
			$fields[] = array(
				'id' => 'clusters_customizations_section',
				'name' => 'Clusters Images',
				'desc' => '',
				'type' => 'title',
				'attributes' => array(
					'style' => 'font-size:15px; color:#ff6600; font-weight:600;'
				),
			);
				
				$fields[] = array(
					'id' => 'big_cluster_icon',
					'name' => 'Large cluster image',
					'desc' => 'Upload a new large cluster image. You can always find the original marker in the plugin\'s images directory.<br />
							  <span style="color:red;">Note: You can use SVG icons!</span>',
					'type' => 'file',
					'default' => $this->plugin_url.'img/svg/cluster.svg',
				);
			
					$fields[] = array(
						'id' => 'big_cluster_icon_height',
						'name' => 'Large cluster image height', 
						'desc' => 'Specify the image height (in pixels). Set to -1 to ignore or to automatically calculate the height (for PNG/JPEG/GIF images).<br />
								  <span style="color:red;"><strong>Note: When set to -1, the image height will be automatically calculated but only if the image type is PNG/JPEG/GIF.
								  If you\'re using SVG icons, the image height will not be calculated and you may need to specify it.</strong></span>',
						'type' => 'text',
						'default' => '90',
						'attributes' => array(
							'type' => 'number',
							'pattern' => '\d*',
							'min' => '-1',
						),
						'sanitization_cb' => function($value, $field_args, $field){
							return ($value == '0') ? '-1' : $value;
						}
					);
					
					$fields[] = array(
						'id' => 'big_cluster_icon_width',
						'name' => 'Large cluster image width', 
						'desc' => 'Specify the image width (in pixels). Set to -1 to ignore or to automatically calculate the width (for PNG/JPEG/GIF images).<br />
								  <span style="color:red;"><strong>Note: When set to -1, the image width will be automatically calculated but only if the image type is PNG/JPEG/GIF.
								  If you\'re using SVG icons, the image width will not be calculated and you may need to specify it.</strong></span>',
						'type' => 'text',
						'default' => '90',
						'attributes' => array(
							'type' => 'number',
							'pattern' => '\d*',
							'min' => '-1',
						),
						'sanitization_cb' => function($value, $field_args, $field){
							return ($value == '0') ? '-1' : $value;
						}
					);

				$fields[] = array(
					'id' => 'medium_cluster_icon',
					'name' => 'Medium cluster image',
					'desc' => 'Upload a new medium cluster image. You can always find the original marker in the plugin\'s images directory.',
					'type' => 'file',
					'default' => $this->plugin_url.'img/svg/cluster.svg',
				);
			
					$fields[] = array(
						'id' => 'medium_cluster_icon_height',
						'name' => 'Medium cluster image height', 
						'desc' => 'Specify the image height (in pixels). Set to -1 to ignore or to automatically calculate the height (for PNG/JPEG/GIF images).<br />
								  <span style="color:red;"><strong>Note: When set to -1, the image height will be automatically calculated but only if the image type is PNG/JPEG/GIF.
								  If you\'re using SVG icons, the image height will not be calculated and you may need to specify it.</strong></span>',
						'type' => 'text',
						'default' => '70',
						'attributes' => array(
							'type' => 'number',
							'pattern' => '\d*',
							'min' => '-1',
						),
						'sanitization_cb' => function($value, $field_args, $field){
							return ($value == '0') ? '-1' : $value;
						}
					);
					
					$fields[] = array(
						'id' => 'medium_cluster_icon_width',
						'name' => 'Medium cluster image width', 
						'desc' => 'Specify the image width (in pixels). Set to -1 to ignore or to automatically calculate the width (for PNG/JPEG/GIF images).<br />
								  <span style="color:red;"><strong>Note: When set to -1, the image width will be automatically calculated but only if the image type is PNG/JPEG/GIF.
								  If you\'re using SVG icons, the image width will not be calculated and you may need to specify it.</strong></span>',
						'type' => 'text',
						'default' => '70',
						'attributes' => array(
							'type' => 'number',
							'pattern' => '\d*',
							'min' => '-1',
						),
						'sanitization_cb' => function($value, $field_args, $field){
							return ($value == '0') ? '-1' : $value;
						}
					);

				$fields[] = array(
					'id' => 'small_cluster_icon',
					'name' => 'Small cluster image',
					'desc' => 'Upload a new small cluster image. You can always find the original marker in the plugin\'s images directory.',
					'type' => 'file',
					'default' => $this->plugin_url.'img/svg/cluster.svg',
				);
			
					$fields[] = array(
						'id' => 'small_cluster_icon_height',
						'name' => 'Small cluster image height', 
						'desc' => 'Specify the image height (in pixels). Set to -1 to ignore or to automatically calculate the height (for PNG/JPEG/GIF images).<br />
								  <span style="color:red;"><strong>Note: When set to -1, the image height will be automatically calculated but only if the image type is PNG/JPEG/GIF.
								  If you\'re using SVG icons, the image height will not be calculated and you may need to specify it.</strong></span>',
						'type' => 'text',
						'default' => '50',
						'attributes' => array(
							'type' => 'number',
							'pattern' => '\d*',
							'min' => '-1',
						),
						'sanitization_cb' => function($value, $field_args, $field){
							return ($value == '0') ? '-1' : $value;
						}
					);
					
					$fields[] = array(
						'id' => 'small_cluster_icon_width',
						'name' => 'Small cluster image width', 
						'desc' => 'Specify the image width (in pixels). Set to -1 to ignore or to automatically calculate the width (for PNG/JPEG/GIF images).<br />
								  <span style="color:red;"><strong>Note: When set to -1, the image width will be automatically calculated but only if the image type is PNG/JPEG/GIF.
								  If you\'re using SVG icons, the image width will not be calculated and you may need to specify it.</strong></span>',
						'type' => 'text',
						'default' => '50',
						'attributes' => array(
							'type' => 'number',
							'pattern' => '\d*',
							'min' => '-1',
						),
						'sanitization_cb' => function($value, $field_args, $field){
							return ($value == '0') ? '-1' : $value;
						}
					);

				$fields[] = array(
					'id' => 'cluster_text_color',
					'name' => 'Clusters text color',
					'desc' => 'Change the text color of all your clusters.',
					'type' => 'colorpicker',
					'default' => '',
				);			
				
				return $fields;
			
		}
		
		
		/**
		 * Infobox Settings Fields 
		 *
		 * @since 1.0 
		 */
		function cspm_infobox_fields(){
			
			$fields = array();
			
			$fields[] = array(
				'name' => 'Infobox Settings',
				'desc' => 'The infobox, also called infowindow is an overlay that looks like a bubble and is often connected to a marker.<br />
						   The following infobox settings are going to be used as the main infobox settings of all single maps and as the default infobox settings of your new maps. 
						   You can always override the default infobox settings when creating/editing a map.',
				'type' => 'title',
				'id'   => 'infobox_settings',
				'attributes' => array(
					'style' => 'font-size:20px; color:#008fed; font-weight:400;'
				),
			);
			
			$fields[] = array(
				'id' => 'show_infobox',
				'name' => 'Show Infobox',
				'desc' => 'Show/Hide the Infobox.',
				'type' => 'radio',
				'default' => 'true',
				'options' => array(
					'true' => 'Yes',
					'false' => 'No'
				)
			);
				
			$fields[] = array(
				'id' => 'infobox_type',
				'name' => 'Infobox type',
				'desc' => 'Select the Infobox type.',
				'type' => 'radio_image',
				'default' => 'rounded_bubble',
				'options' => array(				
					'square_bubble' => 'Square bubble (60x60)',
					'rounded_bubble' => 'Rounded bubble (60x60)',
					'cspm_type3' => 'Infobox 3 (250x50)',
					'cspm_type4' => 'Infobox 4 (250x50)',
					'cspm_type2' => 'Infobox 2 (180x180)',				
					'cspm_type5' => 'Large Infobox (400x300)',
					'cspm_type1' => 'Infobox 1 (380x120)',										
					'cspm_type6' => 'Infobox 6 (380x120)',												
				),
				'images_path' => $this->plugin_url,
				'images' => array(
					'square_bubble' => 'admin/img/radio-imgs/square_bubble.jpg',
					'rounded_bubble' => 'admin/img/radio-imgs/rounded_bubble.jpg',				
					'cspm_type1' => 'admin/img/radio-imgs/infobox_1.jpg',
					'cspm_type2' => 'admin/img/radio-imgs/infobox_2.jpg',
					'cspm_type3' => 'admin/img/radio-imgs/infobox_3.jpg',
					'cspm_type4' => 'admin/img/radio-imgs/infobox_4.jpg',
					'cspm_type5' => 'admin/img/radio-imgs/infobox_5.jpg',
					'cspm_type6' => 'admin/img/radio-imgs/infobox_6.jpg',												
				)
			);
			
			$fields[] = array(
				'id' => 'infobox_width',
				'name' => 'Infobox width', 
				'desc' => 'Override the infobox width by providing a new width (in pixels).<br />
						  <span style="color:red;"><strong>Note:<br />
						  1. If you override the infobox width, you must also provide the infobox height even if you have no intention to change it!<br />
						  2. Set to -1 to ignore this option!</strong></span>',
				'type' => 'text',
				'default' => '-1',
				'attributes' => array(
					'type' => 'number',
					'pattern' => '\d*',
					'min' => '-1',
				),
				'sanitization_cb' => function($value, $field_args, $field){
					return ($value == '0') ? '-1' : $value;
				}
			);
		
			$fields[] = array(
				'id' => 'infobox_height',
				'name' => 'Infobox height', 
				'desc' => 'Override the infobox height by providing a new height (in pixels).<br />
						  <span style="color:red;"><strong>Note:<br />
						  1. If you override the infobox height, you must also provide the infobox width even if you have no intention to change it!<br />
						  2. Set to -1 to ignore this option!</strong></span>',
				'type' => 'text',
				'default' => '-1',
				'attributes' => array(
					'type' => 'number',
					'pattern' => '\d*',
					'min' => '-1',
				),
				'sanitization_cb' => function($value, $field_args, $field){
					return ($value == '0') ? '-1' : $value;
				}
			);

			$fields[] = array(
				'id' => 'infobox_display_event',
				'name' => 'Display event',
				'desc' => 'Select from the options above when to display infoboxes on the map.',
				'type' => 'radio',
				'default' => 'onload',
				'options' => array(
					'onload' => 'On map load <sup>(Loads all infoboxes)</sup>',
					'onclick' => 'On marker click',
					'onhover' => 'On marker hover <span style="color:red;"><sup>(Doesn\'t work on touch devices)</sup></span>'
				)
			);
				
			$fields[] = array(
				'id' => 'remove_infobox_on_mouseout',
				'name' => 'Remove Infobox on mouseout?',
				'desc' => 'Choose whether you want to remove the infobox when the mouse leaves the marker or not. <span style="color:red">This option is operational only when the <strong>Display event</strong> 
						  equals to <strong>On marker click</strong> or <strong>On marker hover</strong>. This option doesn\'t work on touch devices</span>',
				'type' => 'radio',
				'default' => 'false',
				'options' => array(
					'true' => 'Yes',
					'false' => 'No'
				)
			);
				
			$fields[] = array(
				'id' => 'infobox_external_link',
				'name' => 'Post URL',
				'desc' => 'Choose an option to open the single post page. You can also disable links in the infoboxes by selecting the option "Disable"',
				'type' => 'radio',
				'default' => 'same_window',
				'options' => array(
					'new_window' => 'Open in a new window',
					'same_window' => 'Open in the same window',
					'popup' => 'Open inside a modal/popup',
					'disable' => 'Disable'
				)
			);
			
			return $fields;
			
		}
		
		
		/**
		 * Geotargeting settings
		 *
		 * @since 1.0
		 */
		function cspm_geotargeting_fields(){
			
			$fields = array();

			$fields[] = array(
				'name' => 'Geo-targeting Settings',
				'desc' => 'Geo-targeting allow the users to find and display their geographic location on the map.<br />
						   The following geotargeting settings are going to be used as the default geotargeting settings of your new maps. 
						   You can always override the default geotargeting settings when creating/editing a map.',
				'type' => 'title',
				'id'   => 'geotargeting_settings',
				'attributes' => array(
					'style' => 'font-size:20px; color:#008fed; font-weight:400;'
				),
			);
				
				$fields[] = array(
					'id' => 'geoIpControl',
					'name' => 'Allow Geo-targeting',
					'desc' => 'The Geo-targeting is the method of determining the geolocation of a website visitor.',
					'type' => 'radio',
					'default' => 'false',
					'options' => array(
						'true' => 'Yes',
						'false' => 'No'
					)
				);
					
				$fields[] = array(
					'id' => 'show_user',
					'name' => 'Show user location?',
					'desc' => 'Show a marker indicating the user\'s location on the map (when the user approves to share their location!).',
					'type' => 'radio',
					'default' => 'false',
					'options' => array(
						'true' => 'Yes',
						'false' => 'No'
					)
				);
					
				$fields[] = array(
					'id' => 'user_marker_icon',
					'name' => 'User Marker image',
					'desc' => 'Upload a marker image to display as the user location. When empty, the map will display the default marker of Google Map.',
					'type' => 'file',
					'default' => $this->plugin_url.'img/svg/user_marker.svg',
				);
		
				$fields[] = array(
					'id' => 'user_marker_icon_height',
					'name' => 'User Marker image height', 
					'desc' => 'Specify the image height (in pixels). Set to -1 to ignore or to automatically calculate the height (for PNG/JPEG/GIF images).<br />
							  <span style="color:red;"><strong>Note: When set to -1, the image height will be automatically calculated but only if the image type is PNG/JPEG/GIF.
							  If you\'re using SVG icons, the image height will not be calculated and you may need to specify it.</strong></span>',
					'type' => 'text',
					'default' => '32',
					'attributes' => array(
						'type' => 'number',
						'pattern' => '\d*',
						'min' => '-1',
					),
					'sanitization_cb' => function($value, $field_args, $field){
						return ($value == '0') ? '-1' : $value;
					}															
				);
				
				$fields[] = array(
					'id' => 'user_marker_icon_width',
					'name' => 'User Marker image width', 
					'desc' => 'Specify the image width (in pixels). Set to -1 to ignore or to automatically calculate the width (for PNG/JPEG/GIF images).<br />
							  <span style="color:red;"><strong>Note: When set to -1, the image width will be automatically calculated but only if the image type is PNG/JPEG/GIF.
							  If you\'re using SVG icons, the image width will not be calculated and you may need to specify it.</strong></span>',
					'type' => 'text',
					'default' => '30',
					'attributes' => array(
						'type' => 'number',
						'pattern' => '\d*',
						'min' => '-1',
					),
					'sanitization_cb' => function($value, $field_args, $field){
						return ($value == '0') ? '-1' : $value;
					}															
				);

				$fields[] = array(
					'id' => 'user_map_zoom',
					'name' => 'Geotarget Zoom',
					'desc' => 'Select the zoom of the map when indicating the user\'s location.',
					'type' => 'select',
					'default' => '12',
					'options' => array(
						'0' => '0',
						'1' => '1',
						'2' => '2',
						'3' => '3',
						'4' => '4',
						'5' => '5',
						'6' => '6',
						'7' => '7',
						'8' => '8',
						'9' => '9',
						'10' => '10',
						'11' => '11',
						'12' => '12',
						'13' => '13',
						'14' => '14',
						'15' => '15',
						'16' => '16',
						'17' => '17',
						'18' => '18',
						'19' => '19'
					)
				);
					
				$fields[] = array(
					'id' => 'user_circle',
					'name' => 'Draw a Circle around the user\'s location',
					'desc' => 'Draw a circle within a certain distance of the user\'s location. Set to 0 to ignore this option.',
					'type' => 'text',
					'default' => '0',
					'attributes' => array(
						'type' => 'number',
						'pattern' => '\d*',
						'min' => '0'
					),				
				);
				
				$fields[] = array(
					'id' => 'user_circle_fillColor',
					'name' => 'Fill color',
					'desc' => 'The fill color of the circle.',
					'type' => 'colorpicker',
					'default' => '#189AC9',															
				);

				$fields[] = array(
					'id' => 'user_circle_fillOpacity',
					'name' => 'Fill opacity',
					'desc' => 'The fill opacity of the circle between 0.0 and 1.0.',
					'type' => 'select',
					'default' => '0.1',
					'options' => array(
						'0,0' => '0.0',
						'0,1' => '0.1',
						'0,2' => '0.2',
						'0,3' => '0.3',
						'0,4' => '0.4',
						'0,5' => '0.5',
						'0,6' => '0.6',
						'0,7' => '0.7',
						'0,8' => '0.8',
						'0,9' => '0.9',
						'1' => '1',
					)			
				);
			
				$fields[] = array(
					'id' => 'user_circle_strokeColor',
					'name' => 'Stroke color',
					'desc' => 'The stroke color of the circle.',
					'type' => 'colorpicker',
					'default' => '#189AC9',
				);
				
				$fields[] = array(
					'id' => 'user_circle_strokeOpacity',
					'name' => 'Stroke opacity',
					'desc' => 'The stroke opacity of the circle between 0.0 and 1.',
					'type' => 'select',
					'default' => '1',
					'options' => array(
						'0,0' => '0.0',
						'0,1' => '0.1',
						'0,2' => '0.2',
						'0,3' => '0.3',
						'0,4' => '0.4',
						'0,5' => '0.5',
						'0,6' => '0.6',
						'0,7' => '0.7',
						'0,8' => '0.8',
						'0,9' => '0.9',
						'1' => '1',
					)			
				);
				
				$fields[] = array(
					'id' => 'user_circle_strokeWeight',
					'name' => 'Stroke weight',
					'desc' => 'The stroke width of the circle in pixels.',
					'type' => 'text',
					'default' => '1',
					'attributes' => array(
						'type' => 'number',
						'pattern' => '\d*',
						'min' => '0'
					),				
				);	
			
			return $fields;
					
		}
		
		
		/**
		 * Troubleshooting fields
 		 *
		 * @since 1.0 
		 */
		function cspm_troubleshooting_fields(){
		
			$fields = array();
			
			$fields[] = array(
				'name' => 'Troubleshooting',
				'desc' => '',
				'type' => 'title',
				'id'   => 'Troubleshooting_settings',
				'attributes' => array(
					'style' => 'font-size:20px; color:#008fed; font-weight:400;'
				),
			);
			
			$fields[] = array(
				'id' => 'regenerate_markers_link',
				'name' => 'Regenerate markers',
				'desc' => 'This option is for regenerating all your markers. In most cases, <strong>you wont need to use this option at all</strong> because markers 
						   are automatically created and each time you add or edit a post, the marker(s) related to this post will be regenerated automatically. 
						   Use this options <strong>only when need to</strong>.
						   <span style="color:red">This may take a while when you have too many markers (500+) to regenerate. Please be patient.</span>',
				'type' => 'cs_button',
				'options' => array(
					'disable_livequery' => false,
					'action' => 'cspm_regenerate_markers',
					'messages' => array(
						'success' => 'Done',						
						'error' => 'An error has been occured! Please try again later.',
						'confirm' => 'Are you sure you want to regenerate your markers?',
						'loading' => 'Regenerating markers is under process...',
					),
				),
        	);
			
			$fields[] = array(
				'id' => 'loading_scripts_section',
				'name' => 'Debug mode & Speed Optimization',
				'desc' => '',
				'type' => 'title',
				'attributes' => array(
					'style' => 'font-size:15px; color:#ff6600; font-weight:600;'
				),
			);
			
				$fields[] = array(
					'id' => 'combine_files',
					'name' => 'Loading scripts & styles',
					'desc' => 'By default, the plugin will load the minified version of all JS & CSS files but you can reduce the number of requests your site has to make to the server in order to 
							   render the map by choosing the option "Combine files".<br />
							   <strong><u>Hint:</u></strong> <strong>Modern browsers are able to download multiple files at a time in parallel. So that means that it might be more efficient and faster for your browser to download several smaller files all at once, then one large file. The results will vary from site to site so you will have to test this for yourself.</strong><br />
							   <a href="http://blog.wp-rocket.me/5-speed-optimization-myths/" target="_blank">A usful artice about Speed Opitimization</a>',
					'type' => 'radio',
					'default' => 'seperate_minify',
					'options' => array(
						'combine' => 'Combine files',
						'seperate' => 'Seperate files (Debugging versions)',
						'seperate_minify' => 'Seperate files (Minified versions) <sup style="color:red;"><strong>Recommended</strong></sup>'
					)
				);
			
			$fields[] = array(
				'id' => 'disabling_scripts_section',
				'name' => 'Disabling Stylesheets & Scripts',
				'desc' => 'Progress Map uses some files that may conflict with your theme. This section will allow you to disable files that are suspected of creating conflicts in your website.',
				'type' => 'title',
				'attributes' => array(
					'style' => 'font-size:15px; color:#ff6600; font-weight:600;'
				),
			);
			
				$fields[] = array(
					'id' => 'remove_bootstrap',
					'name' => 'Bootstrap (CSS File)',
					'desc' => 'If your theme uses bootstrap v3+, you can disable the one used by this plguin.
							  <br /><span style="color:red;">Important Note: This option will not take action if you select the option <strong>"Loading scripts & styles / Combine files"</strong>! Use the option <strong>"Seperate files (Debugging versions)"</strong> or <strong>"Seperate files (Minified versions)"</strong> instead!</span>',
					'type' => 'radio',
					'default' => 'enable',
					'options' => array(
						'enable' => 'Enable',
						'disable' => 'Disable'
					)
				);
				
				$fields[] = array(
					'id' => 'remove_google_fonts',
					'name' => 'Google Fonts "Source Sans Pro" (CSS File)',
					'desc' => 'If your theme uses or load this font, you can disable the one used by this plguin.',
					'type' => 'radio',
					'default' => 'enable',
					'options' => array(
						'enable' => 'Enable',
						'disable' => 'Disable'
					)
				);
			
				$fields[] = array(
					'id' => 'remove_gmaps_api',
					'name' => 'Google Maps API (JS File)<br /> <span style="color:red;">Not recommended. Read the description!</span>',
					'desc' => 'If your theme loads its own GMaps API, you can disbale the one used by this plugin.
							   <br /><span style="color:red;"><strong>Important Notes: <br />
							   1) Your theme MUST load the version 3 (v3) of Google Maps Javascript API!<br /><br />
							   2) Your theme MUST load the following libraries:<br />
							   - Geometry Library.<br />
							   - Places Libary.<br />
							   The list of libraries may change in the future!
							   <br /><br />
							   3) If you remove our GMaps API, you\'ll not be able to use the following features:<br />
							   - Changing the language of the map.<br />
							   - Adding an API Key to your map.<br />
							   - Address search.<br />
							   The list of parameters may change in the future!<br /><br />
							   Unless your theme doesn\'t provide all of this, we highly recommend you to ENABLE our GMaps API!
							   </strong></span>',
					'type' => 'multicheck',
					'select_all_button' => false,
					'default' => array(''),
					'options' => array(
						'disable_frontend' => 'Disable on Frontend',
						'disable_backend' => 'Disable on Backend',
					),
					'sanitization_cb' => function($value, $field_args, $field){
						if(is_array($value) && count($value) > 0)
							return $value;
						else return $field_args['default'];
					}
				);		
	
			return $fields;
					
		}
		
		
		/**
		 * Customize fields
		 *
		 * @since 1.0 
		 */
		function cspm_customize_fields(){
			
			$fields = array();
			
			$fields[] = array(
				'name' => 'Customize',
				'desc' => '',
				'type' => 'title',
				'id'   => 'customize_settings',
				'attributes' => array(
					'style' => 'font-size:20px; color:#008fed; font-weight:400;'
				),
			);
				
			/**
			 * Main colors */
			
			$fields[] = array(
				'id' => 'main_colors_section',
				'name' => 'Main colors',
				'desc' => '',
				'type' => 'title',
				'attributes' => array(
					'style' => 'font-size:15px; color:#ff6600; font-weight:600;'
				),
			);
								
				$fields[] = array(
					'id' => 'main_hex_color',
					'name' => 'Main color',
					'desc' => 'Pick a color for the plugin. Default to "#008fed".',
					'type' => 'colorpicker',
					'default' => '#008fed',
				);
				
				$fields[] = array(
					'id' => 'main_hex_hover_color',
					'name' => 'Hover color',
					'desc' => 'Pick the hover color for the plugin. Default to "#009bfd".',
					'type' => 'colorpicker',
					'default' => '#009bfd',
				);
				
			/**
			 * Icons */
			
			$fields[] = array(
				'id' => 'map_icons_section',
				'name' => 'Icons',
				'desc' => 'Change the colored icons that appears on the map. Icons used in this plugin are available on <a href="https://www.flaticon.com/" target="_blank">Flaticon</a>',
				'type' => 'title',
				'attributes' => array(
					'style' => 'font-size:15px; color:#ff6600; font-weight:600;'
				),
			);
								
				$fields[] = array(
					'id' => 'zoom_in_icon',
					'name' => 'Zoom-in icon',
					'desc' => 'Appears inside the button that allows to zoom-in the map. The original image can be found <a href="https://www.flaticon.com/free-icon/addition-sign_2664" target="_blank">here</a>.',
					'type' => 'file',
					'default' => $this->plugin_url.'img/svg/addition-sign.svg',
				);
					
				$fields[] = array(
					'id' => 'zoom_out_icon',
					'name' => 'Zoom-out icon',
					'desc' => 'Appears inside the button that allows to zoom-out the map. The original image can be found <a href="https://www.flaticon.com/free-icon/calculation-operations-minus-sign_42977" target="_blank">here</a>.',
					'type' => 'file',
					'default' => $this->plugin_url.'img/svg/minus-sign.svg',
				);
					
				
			/**
			 * Custom CSS */
			
			$fields[] = array(
				'id' => 'custom_css_section',
				'name' => 'Custom CSS',
				'desc' => '',
				'type' => 'title',
				'attributes' => array(
					'style' => 'font-size:15px; color:#ff6600; font-weight:600;'
				),
			);
								
				$fields[] = array(
					'id' => 'custom_css',
					'name' => 'Custom CSS code',
					'desc' => 'Add your own CSS here',
					'type' => 'textarea',
					'default' => '',
				);
			
			return $fields;
			
		}
		
		
		/**
		 * Get all registred post types & remove "Progress Map's" CPT from the list
		 *
		 * @since 1.0
		 */
		function cspm_get_registred_cpts(){
				
			$wp_post_types_array = array(
				'post' => __('Posts').' (post)', 
				'page' => __('Pages').' (page)'
			);

			$all_custom_post_types = get_post_types(array('_builtin' => false), 'objects');
			
			$return_post_types_array = array();
				
			/** 
			 * Loop through default WP post types */
			 
			foreach($wp_post_types_array as $wp_post_type_name => $wp_post_type_label){
				$return_post_types_array[$wp_post_type_name] = $wp_post_type_label;					
			}
			
			/**  
			 * Loop through all custom post types */
			 
			foreach($all_custom_post_types as $post_type){
				if($post_type->name != $this->object_type)
					$return_post_types_array[$post_type->name] = $post_type->labels->name.' ('.$post_type->name.')';
			}
				
			return $return_post_types_array;
			
		}


		/**
		 * Get the list of all Map Styles from the file "inc/cspm-map-styles.php"
		 *
		 * @since 1.0
		 */
		function cspm_get_all_map_styles(){

			$map_styles_array = array();
			
			if(file_exists($this->plugin_path . 'inc/cspm-map-styles.php')){

				$map_styles = include($this->plugin_path . 'inc/cspm-map-styles.php');
				
				array_multisort($map_styles);
				
				foreach($map_styles as $key => $value){
					
					$value_output  = '';
					$value_output .= empty($value['new']) ? '' : ' <sup class="cspm_new_tag" style="margin:0 5px 0 -2px;">NEW</sup>';		
					$value_output .= $value['title'];				
					$value_output .= empty($value['demo']) ? '' : ' <sup class="cspm_demo_tag"><a href="'.$value['demo'].'" target="_blank"><small>Demo</small></a></sup>';
					
					$map_styles_array[$key] = $value_output;
				
				}
				
			}
			
			return $map_styles_array;

		}
				
	}
	
}
