<?php
/**
 * WooCommerce 2Checkout - Inline Checkout Gateway.
 *
 * @package  WC_2Checkout_Inline_Checkout_Gateway
 * @category Gateway
 * @author   WooThemes
 */

class WC_2Checkout_Inline_Checkout_Gateway extends WC_Payment_Gateway {

	/**
	 * Init and hook in the gateway.
	 *
	 * @return void
	 */
	public function __construct() {
		global $woocommerce;

		$this->id                = '2checkout-inline-checkout';
		$this->icon              = apply_filters( 'woocommerce_2checkout_inline_icon', plugins_url( 'assets/images/2checkout.png', plugin_dir_path( __FILE__ ) ) );
		$this->has_fields        = false;
		$this->method_title      = __( '2Checkout - Inline Checkout', 'woocommerce-gateway-2checkout-inline-checkout' );
		$this->order_button_text = __( 'Proceed to payment', 'woocommerce-gateway-2checkout-inline-checkout' );

		// API URLs.
		$this->payment_prod_url    = 'https://www.2checkout.com/checkout/purchase';
		$this->payment_sandbox_url = 'https://sandbox.2checkout.com/checkout/purchase';
		$this->payment_js          = 'https://www.2checkout.com/static/checkout/javascript/direct.min.js';

		// Load the form fields.
		$this->init_form_fields();

		// Load the settings.
		$this->init_settings();

		// Define user set variables.
		$this->title          = $this->get_option( 'title' );
		$this->description    = $this->get_option( 'description' );
		$this->account_number = $this->get_option( 'account_number' );
		$this->secret_word    = $this->get_option( 'secret_word' );
		$this->invoice_prefix = $this->get_option( 'invoice_prefix', 'WC-' );
		$this->sandbox        = $this->get_option( 'sandbox' );
		$this->debug          = $this->get_option( 'debug' );

		add_action( 'woocommerce_api_wc_2checkout_inline_checkout_gateway', array( $this, 'process_return' ) );
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_receipt_' . $this->id, array( $this, 'receipt_page' ) );
		add_action( 'woocommerce_2checkout_inline_ipn', array( $this, 'successful_request' ) );

		// Active logs.
		if ( 'yes' == $this->debug ) {
			if ( class_exists( 'WC_Logger' ) ) {
				$this->log = new WC_Logger();
			} else {
				$this->log = $woocommerce->logger();
			}
		}

		$this->admin_notices();
	}

	/**
	 * Returns a bool that indicates if currency is amongst the supported ones.
	 *
	 * @return bool
	 */
	public function using_supported_currency() {
		$supported_currencies = apply_filters(
			'woocommerce_2checkout_inline_supported_currencies',
			array(
				'AFN',
				'ALL',
				'DZD',
				'ARS',
				'AUD',
				'AZN',
				'BSD',
				'BDT',
				'BBD',
				'BZD',
				'BMD',
				'BOB',
				'BWP',
				'BRL',
				'GBP',
				'BND',
				'BGN',
				'CAD',
				'CLP',
				'CNY',
				'COP',
				'CRC',
				'HRK',
				'CZK',
				'DKK',
				'DOP',
				'XCD',
				'EGP',
				'EUR',
				'FJD',
				'GTQ',
				'HKD',
				'HNL',
				'HUF',
				'INR',
				'IDR',
				'ILS',
				'JMD',
				'JPY',
				'KZT',
				'KES',
				'LAK',
				'MMK',
				'LBP',
				'LRD',
				'MOP',
				'MYR',
				'MVR',
				'MRO',
				'MUR',
				'MXN',
				'MAD',
				'NPR',
				'TWD',
				'NZD',
				'NIO',
				'NOK',
				'PKR',
				'PGK',
				'PEN',
				'PHP',
				'PLN',
				'QAR',
				'RON',
				'RUB',
				'WST',
				'SAR',
				'SCR',
				'SGD',
				'SBD',
				'ZAR',
				'KRW',
				'LKR',
				'SEK',
				'CHF',
				'SYP',
				'THB',
				'TOP',
				'TTD',
				'TRY',
				'UAH',
				'AED',
				'USD',
				'VUV',
				'VND',
				'XOF',
				'YER',
			)
		);

		if ( ! in_array( get_woocommerce_currency(), $supported_currencies ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Returns a value indicating the the Gateway is available or not. It's called
	 * automatically by WooCommerce before allowing customers to use the gateway
	 * for payment.
	 *
	 * @return bool
	 */
	public function is_available() {
		// Test if is valid for use.
		$available = parent::is_available() &&
					! empty( $this->account_number ) &&
					! empty( $this->secret_word ) &&
					$this->using_supported_currency();

		return $available;
	}

	/**
	 * Displays notifications when the admin has something wrong with the configuration.
	 *
	 * @return void
	 */
	protected function admin_notices() {
		if ( is_admin() ) {
			// Checks if email is not empty.
			if ( 'yes' == $this->get_option( 'enabled' ) && (
					empty( $this->account_number )
					|| empty( $this->secret_word )
				)
			) {
				add_action( 'admin_notices', array( $this, 'plugin_not_configured_message' ) );
			}

			// Checks that the currency is supported
			if ( ! $this->using_supported_currency() ) {
				add_action( 'admin_notices', array( $this, 'currency_not_supported_message' ) );
			}
		}
	}

	/**
	 * Initialise Gateway Settings Form Fields.
	 *
	 * @return void
	 */
	public function init_form_fields() {

		$this->form_fields = array(
			'enabled' => array(
				'title'   => __( 'Enable/Disable', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable 2Checkout - Inline Checkout', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'default' => 'yes',
			),
			'title' => array(
				'title'       => __( 'Title', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'default'     => __( '2Checkout', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'desc_tip'    => true,
			),
			'description' => array(
				'title'       => __( 'Description', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'type'        => 'textarea',
				'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'default'     => __( 'Pay with credit card via 2Checkout', 'woocommerce-gateway-2checkout-inline-checkout' ),
			),
			'account_number' => array(
				'title'       => __( 'Account Number', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'type'        => 'text',
				'description' => __( 'Please enter your 2Checkout account number; this is needed in order to take payment.', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'default'     => '',
				'desc_tip'    => true,
			),
			'secret_word' => array(
				'title'       => __( 'Secret Word', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'type'        => 'text',
				'description' => __( 'Please enter your 2Checkout Secret Word; this is needed to validate the 2Checkout return.', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'default'     => '',
				'desc_tip'    => true,
			),
			'invoice_prefix' => array(
				'title'       => __( 'Invoice Prefix', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'type'        => 'text',
				'description' => __( 'Please enter a prefix for your invoice numbers. If you use your 2Checkout account for multiple stores ensure this prefix is unqiue as 2Checkout will not allow orders with the same invoice number.', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'desc_tip'    => true,
				'default'     => 'WC-',
			),
			'testing' => array(
				'title'       => __( 'Gateway Testing', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'type'        => 'title',
				'description' => '',
			),
			'sandbox' => array(
				'title'       => __( 'Sandbox', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'type'        => 'checkbox',
				'label'       => __( 'Enable 2Checkout sandbox', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'default'     => 'no',
				/* translators: 1: 2checkout sandbox link */
				'description' => sprintf( __( '2Checkout sandbox can be used to test payments. Sign up for a developer account <a href="%s">here</a>.', 'woocommerce-gateway-2checkout-inline-checkout' ), 'http://sandbox.2checkout.com/' ),
			),
			'debug' => array(
				'title'       => __( 'Debug Log', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'type'        => 'checkbox',
				'label'       => __( 'Enable logging', 'woocommerce-gateway-2checkout-inline-checkout' ),
				'default'     => 'no',
				/* translators: 1: system status link */
				'description' => sprintf( __( 'Log 2Checkout events, you can check this log in %s.', 'woocommerce-gateway-2checkout-inline-checkout' ), '<a href="' . esc_url( admin_url( 'admin.php?page=wc-status&tab=logs&log_file=' . esc_attr( $this->id ) . '-' . sanitize_file_name( wp_hash( $this->id ) ) . '.log' ) ) . '">' . __( 'System Status &gt; Logs', 'woocommerce-gateway-2checkout-inline-checkout' ) . '</a>' ),
			),
		);
	}

	/**
	 * Limit the length of item names.
	 *
	 * @param  string $item_name
	 *
	 * @return string
	 */
	protected function item_name( $item_name ) {
		$item_name = sanitize_text_field( $item_name );

		if ( strlen( $item_name ) > 127 ) {
			$item_name = substr( $item_name, 0, 124 ) . '...';
		}

		return html_entity_decode( $item_name, ENT_NOQUOTES, 'UTF-8' );
	}

	/**
	 * Get the shop language.
	 *
	 * @return string Shop language.
	 */
	protected function shop_language() {
		$lang = apply_filters( 'woocommerce_2checkout_inline_language', '' );

		if ( '' != $lang ) {
			return $lang;
		}

		$_lang = explode( '_', get_locale() );
		$lang  = $_lang[0];
		if ( 'es' == $lang ) {
			$lang = 'es_ib';
		}

		$available = array( 'zh', 'da', 'nl', 'fr', 'gr', 'el', 'it', 'jp', 'no', 'pt', 'sl', 'es_ib', 'es_la', 'sv', 'en' );

		if ( ! in_array( $lang, $available ) ) {
			return 'en';
		}

		return $lang;
	}

	/**
	 * Generate payment arguments for 2Checkout.
	 *
	 * @param  WC_Order $order Order data.
	 *
	 * @return array           2Checkout payment arguments.
	 */
	protected function generate_payment_args( $order ) {
		$old_wc = version_compare( WC_VERSION, '3.0', '<' );

		$args = array(
			'sid'               => $this->account_number,
			'mode'              => '2CO',
			'currency_code'     => get_woocommerce_currency(),
			'lang'              => $this->shop_language(),
			'merchant_order_id' => $this->invoice_prefix . ( $old_wc ? $order->id : $order->get_id() ),
			'card_holder_name'  => $old_wc ? $order->billing_first_name . ' ' . $order->billing_last_name : $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(),
			'street_address'    => $old_wc ? $order->billing_address_1 : $order->get_billing_address_1(),
			'street_address2'   => $old_wc ? $order->billing_address_2 : $order->get_billing_address_2(),
			'city'              => $old_wc ? $order->billing_city : $order->get_billing_city(),
			'state'             => $old_wc ? $order->billing_state : $order->get_billing_state(),
			'zip'               => $old_wc ? $order->billing_postcode : $order->get_billing_postcode(),
			'country'           => $old_wc ? $order->billing_country : $order->get_billing_country(),
			'email'             => $old_wc ? $order->billing_email : $order->get_billing_email(),
			'phone'             => $old_wc ? $order->billing_phone : $order->get_billing_phone(),
		);

		if ( isset( $_POST['ship_to'] ) && 1 == $_POST['ship_to'] ) {
			$args += array(
				'ship_name'            => $old_wc ? $order->shipping_first_name . ' ' . $order->shipping_last_name : $order->get_shipping_first_name() . ' ' . $order->get_shipping_last_name(),
				'ship_street_address'  => $old_wc ? $order->shipping_address_1 : $order->get_shipping_address_1(),
				'ship_street_address2' => $old_wc ? $order->shipping_address_2 : $order->get_shipping_address_2(),
				'ship_city'            => $old_wc ? $order->shipping_city : $order->get_shipping_city(),
				'ship_state'           => $old_wc ? $order->shipping_state : $order->get_shipping_state(),
				'ship_zip'             => $old_wc ? $order->shipping_postcode : $order->get_shipping_postcode(),
				'ship_country'         => $old_wc ? $order->shipping_country : $order->get_shipping_country(),
			);
		} else {
			$args += array(
				'ship_name'            => $args['card_holder_name'],
				'ship_street_address'  => $args['street_address'],
				'ship_street_address2' => $args['street_address2'],
				'ship_city'            => $args['city'],
				'ship_state'           => $args['state'],
				'ship_zip'             => $args['zip'],
				'ship_country'         => $args['country'],
			);
		}

		$args = $this->_format_address_args_by_country( $args, $order );

		// Cart Contents.
		$item_number = 0;
		if ( sizeof( $order->get_items() ) > 0 ) {
			foreach ( $order->get_items() as $item ) {
				if ( $item['qty'] ) {

					$product = $order->get_product_from_item( $item );

					$item_name 	= $item['name'];

					$meta_display = '';
					if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
						$item_meta    = new WC_Order_Item_Meta( $item );
						$meta_display = $item_meta->display( true, true );
						$meta_display = $meta_display ? ( ' ( ' . $meta_display . '  )'  ) : '';
					} else {
						foreach ( $item->get_formatted_meta_data() as $meta_key => $meta ) {
							$meta_display .= ' ( ' . $meta->display_key . ':' . $meta->value . '  )';
						}
					}

					$item_name .= $meta_display;

					$args[ 'li_' . $item_number . '_type' ]     = 'product';
					$args[ 'li_' . $item_number . '_name' ]     = $this->item_name( $item_name );
					$args[ 'li_' . $item_number . '_quantity' ] = $item['qty'];
					$args[ 'li_' . $item_number . '_price' ]    = number_format( $order->get_item_subtotal( $item, false ), 2, '.', '' );

					if ( $product->get_sku() ) {
						$args[ 'li_' . $item_number . '_product_id' ] = $product->get_sku();
					}

					$item_number++;
				}
			}
		}

		// Tax.
		if ( 0 < $order->get_total_tax() ) {
			$args[ 'li_' . $item_number . '_type' ]     = 'tax';
			$args[ 'li_' . $item_number . '_name' ]     = __( 'Cart tax', 'woocommerce-gateway-2checkout-inline-checkout' );
			$args[ 'li_' . $item_number . '_quantity' ] = 1;
			$args[ 'li_' . $item_number . '_price' ]    = number_format( $order->get_total_tax(), 2, '.', '' );

			$item_number++;
		}

		// Discount.
		if ( 0 < $order->get_total_discount() ) {
			$args[ 'li_' . $item_number . '_type' ]     = 'coupon';
			$args[ 'li_' . $item_number . '_name' ]     = __( 'Cart discount', 'woocommerce-gateway-2checkout-inline-checkout' );
			$args[ 'li_' . $item_number . '_quantity' ] = 1;
			$args[ 'li_' . $item_number . '_price' ]    = number_format( $order->get_total_discount(), 2, '.', '' );

			$item_number++;
		}

		// Fees.
		if ( 0 < sizeof( $order->get_fees() ) ) {
			foreach ( $order->get_fees() as $item ) {
				$args[ 'li_' . $item_number . '_type' ]     = 'tax';
				$args[ 'li_' . $item_number . '_name' ]     = $this->item_name( $item['name'] );
				$args[ 'li_' . $item_number . '_quantity' ] = 1;
				$args[ 'li_' . $item_number . '_price' ]    = number_format( $item['line_total'], 2, '.', '' );

				$item_number++;
			}
		}

		// Shipping.
		if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '2.1', '>=' ) ) {
			$shipping_total = ! empty( $order->get_shipping_method() ) ? $order->get_total_shipping() : 0;

		} else {
			$shipping_total = $order->get_shipping();
		}
		$args[ 'li_' . $item_number . '_type' ]     = 'shipping';
		/* translators: 1: shipping method */
		$args[ 'li_' . $item_number . '_name' ]     = $this->item_name( sprintf( __( 'Shipping via %s', 'woocommerce-gateway-2checkout-inline-checkout' ), $order->get_shipping_method() ) );
		$args[ 'li_' . $item_number . '_quantity' ] = 1;
		$args[ 'li_' . $item_number . '_price' ]    = number_format( $shipping_total, 2, '.', '' );

		$args = apply_filters( 'woocommerce_2checkout_inline_payment_args', $args );

		return $args;
	}

	/**
	 * Format address args by country.
	 *
	 * Some countries may need to reformat the address args to pass 2checkout
	 * validation.
	 *
	 * @since 1.1.8
	 *
	 * @param array    $args  Payment args.
	 * @param WC_Order $order Order object.
	 *
	 * @return array Payment args
	 */
	public function _format_address_args_by_country( $args, $order ) {
		$formatter = '_format_address_args_for_' . $args['country'];
		if ( ! is_callable( array( $this, $formatter ) ) ) {
			return $args;
		}

		return call_user_func( array( $this, $formatter ), $args, $order );
	}

	/**
	 * Format address args for JP.
	 *
	 * Make sure street_address2 is not empty.
	 *
	 * @see https://en.wikipedia.org/wiki/Japanese_addressing_system#Address_order
	 *
	 * @since 1.1.8
	 *
	 * @param array    $args  Payment args.
	 * @param WC_Order $order Order object.
	 *
	 * @return array Payment args
	 */
	public function _format_address_args_for_jp( $args, $order ) {
		global $states;

		if ( ! is_array( $states ) ) {
			return $args;
		}

		// We need the district name to fill street_address2 in case it's empty.
		if ( empty( $states['JP'][ $args['state'] ] ) ) {
			return $args;
		}
		if ( empty( $args['street_address2'] ) ) {
			$args['street_address2'] = $states['JP'][ $args['state'] ] . ' ' . $args['zip'];
		}

		// We need the district name to fill ship_street_address2 in case it's empty.
		if ( empty( $states['JP'][ $args['ship_state'] ] ) ) {
			return $args;
		}
		if ( empty( $args['ship_street_address2'] ) ) {
			$args['ship_street_address2'] = $states['JP'][ $args['ship_state'] ] . ' ' . $args['ship_zip'];
		}

		return $args;
	}

	/**
	 * Generate the form.
	 *
	 * @param int     $order_id Order ID.
	 *
	 * @return string           Payment form.
	 */
	public function generate_form( $order_id ) {
		$order = new WC_Order( $order_id );
		$args  = $this->generate_payment_args( $order );
		$url   = $this->payment_prod_url;
		if ( 'yes' == $this->sandbox ) {
			$url = $this->payment_sandbox_url;
		}

		if ( 'yes' == $this->debug ) {
			$this->log->add( $this->id, 'Payment arguments for order ' . $order->get_order_number() . ': ' . print_r( $args, true ) );
		}

		$form = '<form action="' . esc_url( $url ) . '" method="post" id="payment-form" target="_top">';
		foreach ( $args as $key => $value ) {
			$form .= '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $value ) . '" />';
		}

		$form .= '<input type="submit" class="button alt" id="submit-payment-form" value="' . __( 'Pay via 2Checkout', 'woocommerce-gateway-2checkout-inline-checkout' ) . '" /> ';
		$form .= '<a class="button cancel" href="' . esc_url( $order->get_cancel_order_url() ) . '">' . __( 'Cancel order &amp; restore cart', 'woocommerce-gateway-2checkout-inline-checkout' ) . '</a>';
		$form .= '</form>';

		$form .= '<script src="' . esc_url( $this->payment_js ) . '"></script>';

		return $form;
	}

	/**
	 * Process the payment and return the result.
	 *
	 * @param int $order_id Order ID.
	 *
	 * @return array
	 */
	public function process_payment( $order_id ) {
		$order   = new WC_Order( $order_id );
		$ship_to = isset( $_POST['ship_to_different_address'] ) ? 1 : 0;

		return array(
			'result'   => 'success',
			'redirect' => add_query_arg(
				array(
					'ship_to' => $ship_to,
				),
				$order->get_checkout_payment_url( true )
			),
		);
	}

	/**
	 * Output for the order received page.
	 *
	 * @return void
	 */
	public function receipt_page( $order ) {
		echo '<p>' . __( 'Thank you for your order, please click the button below to pay with 2Checkout.', 'woocommerce-gateway-2checkout-inline-checkout' ) . '</p>';

		echo $this->generate_form( $order );
	}

	/**
	 * Check the 2Checkout IPN request.
	 *
	 * @return bool
	 */
	protected function check_ipn_request() {
		$posted = stripslashes_deep( $_POST );

		if (
			isset( $posted['vendor_order_id'] )
			&& isset( $posted['sale_id'] )
			&& isset( $posted['invoice_id'] )
			&& isset( $posted['md5_hash'] )
		) {
			$hash = strtoupper( md5( $posted['sale_id'] . $this->account_number . $posted['invoice_id'] . $this->secret_word ) );

			if ( $hash == $posted['md5_hash'] ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Process 2Checkout return.
	 *
	 * @return void
	 */
	public function process_return() {
		@ob_clean();

		if ( isset( $_GET['merchant_order_id'] ) && '' != $_GET['merchant_order_id'] ) {
			$posted   = stripslashes_deep( $_GET );
			$order_id = absint( str_replace( $this->invoice_prefix, '', $_GET['merchant_order_id'] ) );
			$order    = wc_get_order( $order_id );

			if ( $order ) {
				wp_redirect( $this->get_return_url( $order ) );
				exit;
			}
		} elseif ( $this->check_ipn_request() ) {
			header( 'HTTP/1.1 200 OK' );
			do_action( 'woocommerce_2checkout_inline_ipn', $_POST );
		} else {
			wp_die( '2Checkout Request Failure', '2Checkout Request', array( 'response' => 200 ) );
		}
	}

	/**
	 * Successful Payment!
	 *
	 * @param  array $posted IPN data.
	 *
	 * @return void
	 */
	function successful_request( $posted ) {
		global $woocommerce;

		$posted   = stripslashes_deep( $posted );
		$order_id = absint( str_replace( $this->invoice_prefix, '', $posted['vendor_order_id'] ) );
		$order    = wc_get_order( $order_id );

		if ( method_exists( $order, 'get_status' ) ) {
			$order_status = $order->get_status();
		} else {
			$order_status = $this->status;
		}

		if ( $order ) {

			// Update the order status.
			if ( in_array( $posted['message_type'], array( 'INVOICE_STATUS_CHANGED', 'ORDER_CREATED', 'FRAUD_STATUS_CHANGED' ) ) ) {
				if ( 'yes' == $this->debug ) {
					$this->log->add( $this->id, 'IPN type for order' . $order->get_order_number() . ': ' . $posted['message_type'] );
					$this->log->add( $this->id, 'Payment status for order ' . $order->get_order_number() . ': ' . $posted['invoice_status'] );
					$this->log->add( $this->id, 'Fraud status for order ' . $order->get_order_number() . ': ' . $posted['fraud_status'] );
				}

				// Test fraud status.
				if ( in_array( $posted['fraud_status'], array( 'pass', 'wait', '' ) ) ) {
					switch ( $posted['invoice_status'] ) {
						case 'approved' :
						case 'deposited' :

							if ( isset( $posted['sale_id'] ) && ! empty( $posted['sale_id'] ) ) {
								// Save only in older versions that not have support for transaction_id.
								if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '2.1.12', '<=' ) ) {
									update_post_meta(
										$order_id,
										__( '2Checkout Sale ID', 'woocommerce-gateway-2checkout-inline-checkout' ),
										sanitize_text_field( $posted['sale_id'] )
									);
								}

								// New WC 2.2 transaction_id.
								if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
									add_post_meta( $order_id, '_transaction_id', $posted['sale_id'], true );
								} else {
									$order->update_meta_data( '_transaction_id', $posted['sale_id'] );
								}
							}

							if ( 'pass' == $posted['fraud_status'] ) {
								if ( ! in_array( $order_status, array( 'processing', 'completed' ) ) ) {
									$order->add_order_note( __( 'Payment approved and passed in the fraud check.', 'woocommerce-gateway-2checkout-inline-checkout' ) );

									$order->payment_complete();
								}
							} else {
								$order->update_status( 'on-hold', __( 'Payment approved and waiting for fraud check.', 'woocommerce-gateway-2checkout-inline-checkout' ) );
							}

							break;
						case 'pending' :
							$order->update_status( 'on-hold', __( 'Payment pending.', 'woocommerce-gateway-2checkout-inline-checkout' ) );
							break;

						case 'declined' :
							$order->update_status( 'failed', __( 'Payment declined.', 'woocommerce-gateway-2checkout-inline-checkout' ) );
							break;

						default:
							break;
					} // End switch().
				} elseif ( 'fail' == $posted['fraud_status'] ) {
					$order->update_status( 'failed', __( 'Payment failed in 2Checkout fraud review.', 'woocommerce-gateway-2checkout-inline-checkout' ) );
				} // End if().
			} elseif ( 'REFUND_ISSUED' == $posted['message_type'] && 'refunded' != $order_status ) { // Order refunded.
				if ( 'yes' == $this->debug ) {
					$this->log->add( $this->id, 'Order ' . $order->get_order_number() . ' refunded' );
				}

				// Mark order as refunded.
				$order->update_status( 'refunded', sprintf( __( 'Payment refunded via IPN.', 'woocommerce-gateway-2checkout-inline-checkout' ) ) );

				if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '2.1', '>=' ) ) {
					$mailer = WC()->mailer();
				} else {
					$mailer = $woocommerce->mailer();
				}

				$message = $mailer->wrap_message(
					__( 'Order refunded', 'woocommerce-gateway-2checkout-inline-checkout' ),
					/* translators: 1: order number */
					sprintf( __( 'Order %s has been marked refunded, please check your 2Checkout account for more details.', 'woocommerce-gateway-2checkout-inline-checkout' ), $order->get_order_number() )
				);

				/* translators: 1: order number */
				$mailer->send( get_option( 'admin_email' ), sprintf( __( 'Payment for order %s refunded', 'woocommerce-gateway-2checkout-inline-checkout' ), $order->get_order_number() ), $message );
			} // End if().

			exit;
		} // End if().
	}

	/**
	 * Get the transaction URL.
	 *
	 * @param  WC_Order $order
	 *
	 * @return string
	 */
	public function get_transaction_url( $order ) {

		if ( 'yes' == $this->sandbox ) {
			$this->view_transaction_url = 'https://sandbox.2checkout.com/sandbox/sales/detail?sale_id=%s';
		} else {
			$this->view_transaction_url = 'https://www.2checkout.com/va/sales/detail?sale_id=%s';
		}

		return parent::get_transaction_url( $order );
	}

	/**
	 * Adds error message when the plugin is not configured properly.
	 *
	 * @return string
	 */
	public function plugin_not_configured_message() {
		$id = 'woocommerce_2checkout-inline-checkout_';
		if (
			isset( $_POST[ $id . 'account_number' ] ) && ! empty( $_POST[ $id . 'account_number' ] )
			&& isset( $_POST[ $id . 'secret_word' ] ) && ! empty( $_POST[ $id . 'secret_word' ] )
		) {
			return;
		}

		echo '<div class="error"><p><strong>' . __( 'WooCommerce 2Checkout - Inline Checkout Disabled', 'woocommerce-gateway-2checkout-inline-checkout' ) . '</strong>: ' . __( 'You must fill the Account Number and the Secret Word options.', 'woocommerce-gateway-2checkout-inline-checkout' ) . '</p></div>';
	}

	/**
	 * Adds error message when an unsupported currency is used.
	 *
	 * @return string
	 */
	public function currency_not_supported_message() {
		echo '<div class="error"><p><strong>' . __( 'WooCommerce 2Checkout - Inline Checkout Disabled', 'woocommerce-gateway-2checkout-inline-checkout' ) . '</strong>: ' . __( '2Checkout does not support your store currency.', 'woocommerce-gateway-2checkout-inline-checkout' ) . '</p></div>';
	}

}
