<?php
/**
 * Plugin Name: WooCommerce 2Checkout - Inline Checkout
 * Plugin URI: https://www.woocommerce.com/products/2checkout-inline-checkout/
 * Description: 2Checkout Inline Checkout integration for WooCommerce
 * Version: 1.1.11
 * Author: WooCommerce
 * Author URI: https://woocommerce.com
 * Text Domain: woocommerce-gateway-2checkout-inline-checkout
 * Domain Path: /languages
 *
 * Woo: 442362:5ca63a0ec8bbddfba41ebda88a990592
 *
 * Copyright 2017 WooCommerce.
 *
 * @package  WC_2Checkout_Inline_Checkout
 * @category Core
 * @author   WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), '5ca63a0ec8bbddfba41ebda88a990592', '442362' );

if ( ! class_exists( 'WC_2Checkout_Inline_Checkout' ) ) :

	define( 'WC_2CHECKOUT_INLINE_CHECKOUT_VERSION', '1.1.11' );

	/**
	 * WooCommerce 2Checkout - Inline Checkout main class.
	 */
	class WC_2Checkout_Inline_Checkout {

		/**
		 * Instance of this class.
		 *
		 * @var object
		 */
		protected static $instance = null;

		/**
		 * Initialize the plugin public actions.
		 */
		private function __construct() {
			// Load plugin text domain
			add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

			// Checks with WooCommerce is installed.
			if ( class_exists( 'WC_Payment_Gateway' ) ) {
				$this->includes();

				add_filter( 'woocommerce_payment_gateways', array( $this, 'add_gateway' ) );
				add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_action_links' ) );
			} else {
				add_action( 'admin_notices', array( $this, 'woocommerce_missing_notice' ) );
			}
		}

		/**
		 * Return an instance of this class.
		 *
		 * @return object A single instance of this class.
		 */
		public static function get_instance() {
			// If the single instance hasn't been set, set it now.
			if ( null == self::$instance ) {
				self::$instance = new self;
			}

			return self::$instance;
		}

		/**
		 * Includes.
		 *
		 * @return
		 */
		private function includes() {
			include_once 'includes/class-wc-2checkout-inline-checkout-gateway.php';
		}

		/**
		 * Load the plugin text domain for translation.
		 *
		 * @return void
		 */
		public function load_plugin_textdomain() {
			$locale = apply_filters( 'plugin_locale', get_locale(), 'woocommerce-gateway-2checkout-inline-checkout' );

			load_textdomain( 'woocommerce-gateway-2checkout-inline-checkout', trailingslashit( WP_LANG_DIR ) . 'woocommerce-gateway-2checkout-inline-checkout/woocommerce-gateway-2checkout-inline-checkout-' . $locale . '.mo' );
			load_plugin_textdomain( 'woocommerce-gateway-2checkout-inline-checkout', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
		}

		/**
		 * Add the gateway.
		 *
		 * @param  array $methods WooCommerce payment methods.
		 *
		 * @return array          WooCommerce 2Checkout - Inline Checkout gateway.
		 */
		public function add_gateway( $methods ) {
			$methods[] = 'WC_2Checkout_Inline_Checkout_Gateway';

			return $methods;
		}

		/**
		 * WooCommerce fallback notice.
		 *
		 * @return string
		 */
		public function woocommerce_missing_notice() {
			/* translators: 1: plugin name */
			echo '<div class="error"><p>' . sprintf( __( 'WooCommerce 2Checkout - Inline Checkout Gateway depends on the last version of %s to work!', 'woocommerce-gateway-2checkout-inline-checkout' ), '<a href="http://wordpress.org/extend/plugins/woocommerce/">' . __( 'WooCommerce', 'woocommerce-gateway-2checkout-inline-checkout' ) . '</a>' ) . '</p></div>';
		}

		/**
		 * Add relevant links to plugins page.
		 *
		 * @param  array $links
		 *
		 * @return array
		 */
		public function plugin_action_links( $links ) {
			$setting_link = $this->get_setting_link();

			$plugin_links = array(
				'<a href="' . $setting_link . '">' . __( 'Settings', 'woocommerce-gateway-2checkout-inline-checkout' ) . '</a>',
				'<a href="https://woocommerce.com/my-account/create-a-ticket/">' . __( 'Support', 'woocommerce-gateway-2checkout-inline-checkout' ) . '</a>',
				'<a href="https://docs.woocommerce.com/document/2checkout-inline-checkout/">' . __( 'Docs', 'woocommerce-gateway-2checkout-inline-checkout' ) . '</a>',
			);

			return array_merge( $plugin_links, $links );
		}

		/**
		 * Get setting link.
		 *
		 * @since 1.1.7
		 *
		 * @return string Setting link
		 */
		public function get_setting_link() {
			$use_id_as_section = version_compare( WC()->version, '2.6', '>=' );

			$section_slug = $use_id_as_section ? '2checkout-inline-checkout' : 'wc_2checkout_inline_checkout_gateway';

			return admin_url( 'admin.php?page=wc-settings&tab=checkout&section=' . $section_slug );
		}
	}

	add_action( 'plugins_loaded', array( 'WC_2Checkout_Inline_Checkout', 'get_instance' ) );

endif;
