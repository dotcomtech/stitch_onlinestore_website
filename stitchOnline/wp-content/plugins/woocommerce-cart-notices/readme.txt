=== WooCommerce Cart Notices ===
Author: skyverge
Tags: woocommerce
Requires at least: 4.1
Tested up to: 4.8
WC requires at least: 2.5.5
WC tested up to: 3.1.0

Add dynamic notices above the cart and checkout to help increase your sales!

See http://docs.woothemes.com/document/woocommerce-cart-notices/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-cart-notices' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
