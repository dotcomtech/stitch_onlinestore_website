# Copyright (C) 2017 SkyVerge
# This file is distributed under the GNU General Public License v3.0.
msgid ""
msgstr ""
"Project-Id-Version: WooCommerce Cart Notices 1.7.1\n"
"Report-Msgid-Bugs-To: https://woocommerce.com/my-account/tickets/\n"
"POT-Creation-Date: 2014-06-15 22:41:23+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2017-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"

#: includes/admin/class-wc-cart-notices-admin.php:99
msgid "You must choose a Notice Type"
msgstr ""

#: includes/admin/class-wc-cart-notices-admin.php:105
msgid "You must provide a Notice Name"
msgstr ""

#: includes/admin/class-wc-cart-notices-admin.php:116
msgid "That name is already in use"
msgstr ""

#: includes/admin/class-wc-cart-notices-admin.php:121
msgid "Target amount must be positive number, or empty"
msgstr ""

#: includes/admin/class-wc-cart-notices-admin.php:126
msgid "Threshold amount must be positive number, or empty"
msgstr ""

#: includes/admin/class-wc-cart-notices-admin.php:131
msgid "Deadline hour must be in 24-hour format, between 1 to 24"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "WooCommerce Cart Notices"
msgstr ""

#: includes/admin/class-wc-cart-notices-admin.php:339
#: includes/admin/class-wc-cart-notices-admin.php:483
#: includes/admin/views/admin-options.php:60
#: includes/admin/views/admin-options.php:77
msgid "Cart Notices"
msgstr ""

#: includes/admin/class-wc-cart-notices-admin.php:403
msgid "You do not have sufficient permissions to access this page."
msgstr ""

#: includes/admin/class-wc-cart-notices-admin.php:486
msgid "Delete data"
msgstr ""

#: includes/admin/class-wc-cart-notices-admin.php:487
msgid "Delete all Cart Notices data on uninstall"
msgstr ""

#: includes/admin/views/admin-options.php:63
msgid "New Notice"
msgstr ""

#: includes/admin/views/admin-options.php:70
msgid "Cart Notice"
msgstr ""

#: includes/admin/views/admin-options.php:84
#: includes/admin/views/admin-options.php:216
#: includes/admin/views/admin-options.php:329
msgid "Name"
msgstr ""

#: includes/admin/views/admin-options.php:87
#: includes/admin/views/admin-options.php:219
#: includes/admin/views/admin-options.php:290
msgid "Type"
msgstr ""

#: includes/admin/views/admin-options.php:90
#: includes/admin/views/admin-options.php:222
msgid "Message"
msgstr ""

#: includes/admin/views/admin-options.php:93
#: includes/admin/views/admin-options.php:225
#: includes/admin/views/admin-options.php:389
msgid "Call to Action"
msgstr ""

#: includes/admin/views/admin-options.php:96
#: includes/admin/views/admin-options.php:228
#: includes/admin/views/admin-options.php:403
msgid "Call to Action URL"
msgstr ""

#: includes/admin/views/admin-options.php:99
#: includes/admin/views/admin-options.php:231
msgid "Other"
msgstr ""

#: includes/admin/views/admin-options.php:109
msgid "No notices configured"
msgstr ""

#: includes/admin/views/admin-options.php:126
msgid "Edit"
msgstr ""

#: includes/admin/views/admin-options.php:132
msgid "Disable"
msgstr ""

#: includes/admin/views/admin-options.php:132
msgid "Enable"
msgstr ""

#: includes/admin/views/admin-options.php:138
msgid "Delete"
msgstr ""

#: includes/admin/views/admin-options.php:163
#. translators: %s - formatted amount quantity
msgid "Target amount: %s"
msgstr ""

#: includes/admin/views/admin-options.php:165
#. translators: %s - formatted amount quantity
msgid "Threshold amount: %s"
msgstr ""

#: includes/admin/views/admin-options.php:171
msgid "Deadline Hour: %s"
msgstr ""

#: includes/admin/views/admin-options.php:172
msgid "Active Days: %s"
msgstr ""

#: includes/admin/views/admin-options.php:179
#. translators: %s - website
msgid "Referring Site: %s"
msgstr ""

#: includes/admin/views/admin-options.php:185
msgid "Products: %s"
msgstr ""

#: includes/admin/views/admin-options.php:188
msgid "Minimum quantity: %s"
msgstr ""

#: includes/admin/views/admin-options.php:192
msgid "Maximum quantity: %s"
msgstr ""

#: includes/admin/views/admin-options.php:198
msgid "Categories: %s"
msgstr ""

#: includes/admin/views/admin-options.php:240
msgid "Shortcode Reference"
msgstr ""

#: includes/admin/views/admin-options.php:242
msgid ""
"In addition to the default placement on the cart/checkout pages, you can "
"embed one or all of the notices anywhere on the site with the following "
"shortcodes:"
msgstr ""

#: includes/admin/views/admin-options.php:247
#. translators: %s - shortcode snippet
msgid "%s will embed all notices"
msgstr ""

#: includes/admin/views/admin-options.php:250
#. translators: %s - shortcode snippet
msgid "%s will embed just the notice named XXX"
msgstr ""

#: includes/admin/views/admin-options.php:253
#. translators: %s - shortcode snippet
msgid "%s will embed just the minimum amount notices"
msgstr ""

#: includes/admin/views/admin-options.php:256
#. translators: %s - shortcode snippet
msgid "%s will embed just the deadline notices"
msgstr ""

#: includes/admin/views/admin-options.php:259
#. translators: %s - shortcode snippet
msgid "%s will embed just the referer notices"
msgstr ""

#: includes/admin/views/admin-options.php:262
#. translators: %s - shortcode snippet
msgid "%s will embed just the products in cart notices"
msgstr ""

#: includes/admin/views/admin-options.php:265
#. translators: %s - shortcode snippet
msgid "%s will embed just the categories in cart notices"
msgstr ""

#: includes/admin/views/admin-options.php:272
msgid "Create a New Cart Notice"
msgstr ""

#: includes/admin/views/admin-options.php:272
#: includes/admin/views/admin-options.php:683
msgid "Update Cart Notice"
msgstr ""

#: includes/admin/views/admin-options.php:296
msgid "Choose One"
msgstr ""

#: includes/admin/views/admin-options.php:309
msgid ""
"This notice will appear on the cart/checkout pages only when the order "
"total is less than the Target Amount, and/or is greater than or equal to "
"the Threshold Amount and is convenient for encouraging customers to "
"increase their order to qualify for free shipping."
msgstr ""

#: includes/admin/views/admin-options.php:312
msgid ""
"This notice will appear on the cart/checkout pages only on the Active Days, "
"and up to the Deadline Hour, based on your WordPress timezone."
msgstr ""

#: includes/admin/views/admin-options.php:315
msgid ""
"This notice will appear on the cart/checkout pages only when the customer "
"originated from the configured site."
msgstr ""

#: includes/admin/views/admin-options.php:318
msgid ""
"This notice will appear on the cart/checkout pages when any of the "
"configured products appear within the cart."
msgstr ""

#: includes/admin/views/admin-options.php:321
msgid ""
"This notice will appear on the cart/checkout pages when any of the cart "
"products belong to any of the categories configured below."
msgstr ""

#: includes/admin/views/admin-options.php:335
msgid "Provide a name so you can easily recognize this notice within the admin."
msgstr ""

#: includes/admin/views/admin-options.php:343
msgid "Enabled"
msgstr ""

#: includes/admin/views/admin-options.php:354
msgid "Notice Message"
msgstr ""

#: includes/admin/views/admin-options.php:358
msgid "Depending on the notice type you may use the following variables:"
msgstr ""

#: includes/admin/views/admin-options.php:363
msgid ""
"With type 'Minimum Amount' this is the amount required to meet the minimum "
"order amount."
msgstr ""

#: includes/admin/views/admin-options.php:364
msgid ""
"With type 'Deadline' this is the amount of time remaining, ie '1 hour 15 "
"minutes' or '25 minutes', etc."
msgstr ""

#: includes/admin/views/admin-options.php:365
msgid ""
"With type 'Products in Cart' or 'Categories in Cart' these are the matching "
"product names."
msgstr ""

#: includes/admin/views/admin-options.php:366
msgid "With type 'Products in Cart' this is the product quantity."
msgstr ""

#: includes/admin/views/admin-options.php:367
msgid ""
"With type 'Products in Cart' and 'Maximum Quantity for Notice' configured "
"this is the product quantity less than the maximum."
msgstr ""

#: includes/admin/views/admin-options.php:368
msgid ""
"With type 'Products in Cart' and 'Minimum Quantity for Notice' configured "
"this is the product quantity over the minimum."
msgstr ""

#: includes/admin/views/admin-options.php:369
msgid "With type 'Categories in Cart' these are the matching category names."
msgstr ""

#: includes/admin/views/admin-options.php:395
msgid "Optional call to action button text, rendered next to the cart notice"
msgstr ""

#: includes/admin/views/admin-options.php:409
msgid ""
"Optional call to action url, this is where the user will go upon clicking "
"the Call to Action button"
msgstr ""

#: includes/admin/views/admin-options.php:417
msgid "Target Amount"
msgstr ""

#: includes/admin/views/admin-options.php:424
#. translators: Placeholders: %1$s - <strong>, %2$s - </strong>, %3$s - <a>,
#. %4$s - </a>
msgid ""
"Optional target amount for the notice; the cart total must be %1$sless "
"than%2$s this amount for the notice to be displayed. If not set, and the "
"%3$sFree Shipping shipment method%4$s is enabled, the \"Minimum order "
"amount\" from the shipping method will be used."
msgstr ""

#: includes/admin/views/admin-options.php:436
msgid "Threshold Amount"
msgstr ""

#: includes/admin/views/admin-options.php:443
#. translators: Placeholders: %1$s - <strong>, %2$s - </strong>
msgid ""
"Optional threshold amount to activate the notice. If set, the cart must "
"contain %1$sat least%2$s this total amount for the notice to be displayed."
msgstr ""

#: includes/admin/views/admin-options.php:453
msgid "Deadline Hour"
msgstr ""

#: includes/admin/views/admin-options.php:459
msgid "Deadline hour in 24-hour format, this can be 1 to 24."
msgstr ""

#: includes/admin/views/admin-options.php:468
msgid "Active Days"
msgstr ""

#: includes/admin/views/admin-options.php:486
msgid "Select the days on which you want this notice to be active."
msgstr ""

#: includes/admin/views/admin-options.php:494
msgid "Referring Site"
msgstr ""

#: includes/admin/views/admin-options.php:500
msgid ""
"When the visitor originates from this server, they will be shown the "
"referer cart notice. Example: www.google.com."
msgstr ""

#: includes/admin/views/admin-options.php:508
msgid "Products"
msgstr ""

#: includes/admin/views/admin-options.php:521
#: includes/admin/views/admin-options.php:539
msgid "Search for a product&hellip;"
msgstr ""

#: includes/admin/views/admin-options.php:558
msgid "Minimum Quantity for Notice"
msgstr ""

#: includes/admin/views/admin-options.php:564
msgid ""
"Optional minimum product quantity required to activate the notice.  If set, "
"the quantity of the products selected above must be greater than or equal "
"to this amount."
msgstr ""

#: includes/admin/views/admin-options.php:572
msgid "Maximum Quantity for Notice"
msgstr ""

#: includes/admin/views/admin-options.php:578
msgid ""
"Optional maximum product quantity allowed to activate the notice.  If set, "
"the quantity of the products selected above must be less than or equal to "
"this amount."
msgstr ""

#: includes/admin/views/admin-options.php:586
msgid "Shipping Countries"
msgstr ""

#: includes/admin/views/admin-options.php:595
msgid "Choose Countries&hellip;"
msgstr ""

#: includes/admin/views/admin-options.php:602
msgid ""
"Optional list of countries used to trigger the message when the shipping "
"country is available and matches one of the countries selected here."
msgstr ""

#: includes/admin/views/admin-options.php:610
msgid "Categories"
msgstr ""

#: includes/admin/views/admin-options.php:623
#: includes/admin/views/admin-options.php:641
msgid "Search for a category&hellip;"
msgstr ""

#: includes/admin/views/admin-options.php:677
msgid "Create Cart Notice"
msgstr ""

#: includes/admin/views/admin-options.php:714
msgid "Cannot render this example notice without shipping zone."
msgstr ""

#: includes/admin/views/admin-options.php:732
msgid "Example Notice"
msgstr ""

#: includes/admin/views/admin-options.php:744
#. translators: %s - Formatted minimum order amount
msgid ""
"With the current configuration your cart notice will display when the order "
"total is less than %s and will resemble:"
msgstr ""

#: includes/admin/views/admin-options.php:752
#. translators: %s - Formatted amount
msgid ""
"With the current configuration your cart notice will display when the order "
"total is greater than or equal to %s and will resemble:"
msgstr ""

#: includes/admin/views/admin-options.php:760
#. translators: Placeholders: %1$s Threshold order amount, %2$s Target amount
msgid ""
"With the current configuration your cart notice will display when the order "
"total is between %1$s and %2$s and will resemble:"
msgstr ""

#: includes/admin/views/admin-options.php:769
msgid "With the current configuration your cart notice will resemble: "
msgstr ""

#: includes/admin/views/admin-options.php:779
msgid "No notice"
msgstr ""

#: includes/admin/views/admin-options.php:792
msgid ""
"Add <strong>{amount_under}</strong> to your cart in order to receive free "
"shipping!"
msgstr ""

#: includes/admin/views/admin-options.php:793
msgid "Order within the next <strong>{time}</strong> and your order ships today!"
msgstr ""

#: woocommerce-cart-notices.php:487
msgid "%d hour"
msgid_plural "%d hours"
msgstr[0] ""
msgstr[1] ""

#: woocommerce-cart-notices.php:490
msgid "%d minute"
msgid_plural "%d minutes"
msgstr[0] ""
msgstr[1] ""

#. Plugin URI of the plugin/theme
msgid "http://www.woocommerce.com/products/cart-notices/"
msgstr ""

#. Description of the plugin/theme
msgid "Add dynamic notices above the cart and checkout to help increase your sales!"
msgstr ""

#. Author of the plugin/theme
msgid "SkyVerge"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://www.woocommerce.com"
msgstr ""

#: includes/admin/views/admin-options.php:171
msgctxt "No deadline hour"
msgid "none"
msgstr ""

#: includes/admin/views/admin-options.php:172
msgctxt "No active days"
msgid "none"
msgstr ""

#: includes/admin/views/admin-options.php:179
msgctxt "No referring site"
msgid "none"
msgstr ""

#: includes/admin/views/admin-options.php:185
msgctxt "No products"
msgid "none"
msgstr ""

#: includes/admin/views/admin-options.php:198
msgctxt "No categories"
msgid "none"
msgstr ""