<?php
/**
 * Plugin Name:       WooCommerce Give Products
 * Description:       Allow shop owners to freely give products to users.
 * Version:           1.0.10
 * Author:            WooCommerce
 * Author URI:        http://www.woocommerce.com/
 * Requires at least: 4.0.0
 * Tested up to:      4.3.1
 *
 * Copyright:         © 2009-2017 WooCommerce.
 * License:           GNU General Public License v3.0
 * License URI:       http://www.gnu.org/licenses/gpl-3.0.html
 * Woo: 521947:c76e4d6a4935f9d2ba635d2c459e813e
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), 'c76e4d6a4935f9d2ba635d2c459e813e', '521947' );

if ( ! class_exists( 'WC_Give_Products' ) ) {
	class WC_Give_Products {

		protected static $instance = null;
		public static $plugin_file = __FILE__;

		/**
		 * Initialize the plugin.
		 *
		 * @since 1.0
		 */
		private function __construct() {
			if ( class_exists( 'WooCommerce' ) ) {
				add_action( 'admin_init', array( $this, 'init' ), 20 );

				// load Chosen scripts
				add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_chosen_scripts' ), 20 );

				// update menu
				add_action( 'admin_menu', array( $this, 'add_menu_item' ) );

				// admin notices
				add_action( 'admin_notices', array( $this, 'admin_notice' ) );

				// notice on customers order screen
				add_action( 'woocommerce_view_order', array( $this, 'display_given_status' ) );

				// notice on edit order screen
				add_action( 'woocommerce_admin_order_data_after_order_details', array( $this, 'display_given_status_admin' ) );

				// add AJAX functionality
				add_action( 'wp_ajax_give_products_json_search_products_and_variations', array( $this, 'json_search_products_and_variations' ) );

				// add Given Order email
				add_action( 'woocommerce_email_classes', array( $this, 'add_emails' ), 20, 1 );

				// Add screen id
				add_filter( 'woocommerce_screen_ids', array( $this, 'woocommerce_screen_ids' ) );
			}
		}


		/**
		 * Init
		 *
		 * @since 1.0
		 */
		public function init() {
			// make sure this processing runs on the right page
			if ( isset( $_GET['page'] ) && 'give_products' == $_GET['page'] ) {

				// Process any post data
				if ( isset( $_GET['action'] ) &&
				     'give' == $_GET['action'] &&
					 isset( $_GET['user_id'] ) &&
					 $_GET['user_id'] &&
					 isset( $_GET['products'] ) &&
					 isset( $_GET['give_products_nonce'] ) &&
					 wp_verify_nonce( $_GET['give_products_nonce'], 'give_products' )
					) {

					// we have all the data we need - create the order!
					$this->create_order( $_GET['user_id'], $_GET['products'] );

				} elseif ( isset( $_GET['action'] ) ) {
					// error
					$url = add_query_arg( array( 'post_type' => 'product', 'page' => 'give_products', 'message' => '2' ), admin_url( 'edit.php' ) );
					wp_redirect( $url );
				}
			}

		}


		/**
		 * Enqueue JS & CSS for Chosen select boxes
		 *
		 * @since 1.0
		 */
		public function enqueue_chosen_scripts() {
			global $current_screen;
			global $woocommerce;

			if ( 'product_page_give_products' === $current_screen->base ) {
				//Chosen JS
				wp_enqueue_script( 'ajax-chosen' );
				wp_enqueue_script( 'chosen' );

				//Chosen CSS
				wp_enqueue_style( 'woocommerce_admin_styles', $woocommerce->plugin_url() . '/assets/css/admin.css' );
			}
		}


		/**
		 * Add Give Products to admin menu
		 *
		 * @since 1.0
		 */
		public function add_menu_item() {
			if ( is_admin() ) {
				add_submenu_page( 'edit.php?post_type=product', __( 'Give Products', 'woocommerce-give-products' ), __( 'Give Products', 'woocommerce-give-products' ), 'edit_users', 'give_products', array( $this, 'display_page' ) );
			}
			return false;
		}


		/**
		 * Display Give Products admin page
		 *
		 * @since 1.0
		 */
		public function display_page() {
			// Display page content
			echo sprintf( '<div class="wrap">
				<h2>%s</h2>
				<p>%s</p>
				<form action="" method="get">
				<input type="hidden" name="post_type" value="product"/>
				<input type="hidden" name="page" value="give_products"/>
				<input type="hidden" name="action" value="give"/>',
				__( 'Give Products', 'woocommerce-give-products' ),
				__( '<b>Select a user</b> by typing their display name, email address or user ID here:', 'woocommerce-give-products' )
			);

			if ( version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
				?>
				<select id="user_id" style="width: 50%;" class="wc-customer-search" name="user_id" data-allow_clear="true" data-placeholder="<?php esc_attr_e( 'Search for a user', 'woocommerce-give-products' ); ?>">
				</select>
				<?php
			} else {
				?>
				<input type="hidden" class="wc-customer-search" id="user_id" name="user_id" data-placeholder="<?php _e( 'Search for a user', 'woocommerce-give-products' ); ?>" value="" data-allow_clear="true" style="max-width: 400px;" />
				<?php
			}

			echo '<p>' . __( '<b>Select products</b> by typing in their names, variation details or IDs here (you can select as many products as you like):', 'woocommerce-give-products' ) . '</p>';

			if ( version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
				?>
				<select class="wc-product-search" multiple="multiple" style="width: 50%;" name="products[]" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'woocommerce-give-products' ); ?>" data-allow_clear="true" data-action="woocommerce_json_search_products_and_variations">
				</select>
				<?php
			} else {
				?>
				<input type="hidden" class="wc-product-search" id="product_list" name="products[]" data-multiple="true" data-placeholder="<?php _e( 'Search for a product&hellip;', 'woocommerce-give-products' ); ?>" value="" data-allow_clear="true" style="max-width: 400px;" />
				<?php
			}

			wp_nonce_field( 'give_products', 'give_products_nonce' );

			echo '<p><input type="submit" value="' . __( 'Give product(s)', 'woocommerce-give-products' ) . '" class="button-primary"/></p>
				</form>
			</div>';

			if ( version_compare( WC_VERSION, '2.3', '<' ) ) {
				$give_products_vars = array(
					'admin_ajax_url' => admin_url( 'admin-ajax.php' ),
					'nonce'          => wp_create_nonce( 'search-customers' ),
				);
				wp_enqueue_script( 'give-products', plugins_url( 'assets/scripts/give-products.js' , __FILE__ ) , array( 'jquery' ), '1.0', true );
				wp_localize_script( 'give-products', 'giveProducts', $give_products_vars );
			}
		}


		/**
		 * Create new order based on selection
		 *
		 * @param int $user_id
		 * @param array $products
		 * @since 1.0
		 */
		public function create_order( $user_id, $products ) {
			global $woocommerce;

			// Get customer info
			$user = get_userdata( $user_id );

			if ( ! empty( $products ) ) {

				// create new order
				$args = array(
					'customer_id'   => $user_id,
				);
				$order = wc_create_order( $args );

				if ( version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
					$order_id = $order->get_id();
					$products = ! empty( $products ) ? $products : array();
				} else {
					$order_id = $order->id;
					$products = isset( $products[0] ) ? explode( ',', $products[0] ) : array();
				}

				// Set _given_order post meta to true
				update_post_meta( $order_id, '_wcgp_given_order', 'yes' );

				// track the order status - if we have products that need to be shipped we should change this to processing
				$order_status = 'completed';

				// loop through each product we want to give away
				foreach ( $products as $key => $value ) {

					// get product data
					$product = wc_get_product( $value );

					if ( $product ) {

						// add product to order
						$item = array(
							'order_item_name' => $product->get_title(),
						);
						$item_id = wc_add_order_item( $order_id, $item );
						$price_without_tax = version_compare( WC_VERSION, '3.0', '<' ) ? $product->get_price_excluding_tax() : wc_get_price_excluding_tax( $product );

						// now add all of the product meta
						wc_add_order_item_meta( $item_id, '_qty', 1 );
						wc_add_order_item_meta( $item_id, '_tax_class', $product->get_tax_class() );
						wc_add_order_item_meta( $item_id, '_product_id', $product->get_id() );
						wc_add_order_item_meta( $item_id, '_variation_id', ( version_compare( WC_VERSION, '3.0', '<' ) && isset( $product->variation_id ) ) ? $product->variation_id : $product->get_id() );
						wc_add_order_item_meta( $item_id, '_line_subtotal', wc_format_decimal( $price_without_tax ) );
						wc_add_order_item_meta( $item_id, '_line_subtotal_tax', '' );
						wc_add_order_item_meta( $item_id, '_line_total', wc_format_decimal( 0 ) );
						wc_add_order_item_meta( $item_id, '_line_tax', '' );
						wc_add_order_item_meta( $item_id, '_line_tax_data', array( 'total' => array(), 'subtotal' => array() ) );

						// Store variation data in meta
						if ( method_exists( $product, 'get_variation_attributes' ) ) {
							$variation_data = $product->get_variation_attributes();
							if ( $variation_data && is_array( $variation_data ) ) {
								foreach ( $variation_data as $key => $value ) {
									wc_add_order_item_meta( $item_id, str_replace( 'attribute_', '', $key ), $value );
								}
							}
						}

						// see if the product needs to be shipped
						if ( ( 'completed' === $order_status ) && $product->needs_shipping() ) {
							$order_status = 'processing';
						}
					} // End if().
				} // End foreach().

				// update the order status
				$args = array(
					'order_id' => $order_id,
					'status'   => $order_status,
				);

				// update the order
				$order = wc_update_order( $args );

				// add a note that this product was gifted
				$order->add_order_note( __( 'This order was gifted.', 'woocommerce-give-products' ) );

				if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
					$order->reduce_order_stock();
				} else {
					wc_reduce_stock_levels( $order->get_id() );
				}

				// give download permissions
				wc_downloadable_product_permissions( $order_id );

				// init the woocommerce email classes
				$woocommerce->mailer();

				do_action( 'woocommerce_order_given', $order_id );

				$redirect = esc_url_raw( $_SERVER['HTTP_REFERER'] );

				// add a success message
				$redirect = add_query_arg( 'message', '1', $redirect );
				$redirect = add_query_arg( 'order_id', $order_id, $redirect );
				$redirect = remove_query_arg( 'action', $redirect );

				wp_safe_redirect( $redirect );

			} // End if().

		}


		/**
		 * Add new WC emails
		 *
		 * @param int $email_classes
		 * @since 1.0
		 */
		public function add_emails( $email_classes ) {

			$email_classes['WC_Given_Order'] = include( 'includes/emails/class-wc-given-order.php' );
			return $email_classes;
		}


		/**
		 * Display notice when products are given
		 *
		 * @since 1.0
		 */
		public function admin_notice() {
			global $current_screen;
			if ( 'product_page_give_products' === $current_screen->base ) {
				if ( isset( $_GET['message'] ) ) {
					switch ( $_GET['message'] ) {
						case 1:
							$display_name = 'selected user';
							if ( isset( $_GET['userid'] ) && $_GET['userid'] > 0 ) {
								$user         = get_userdata( $_GET['userid'] );
								$display_name = $user->data->display_name;
							}
							$order_id = '';
							$order_url = '';
							if ( isset( $_GET['order_id'] ) ) {
								$order_id = $_GET['order_id'];
								$order_url = get_option( 'siteurl' ) . '/wp-admin/post.php?post=' . intval( $order_id ) . '&action=edit';
							}
							/* translators: 1: display name 2: order url 3: order id */
							$format = __( 'Product(s) given to %1$s in <a href="%2$s">order %3$s</a>.' , 'woocommerce-give-products' );
							$format = sprintf( $format, $display_name, $order_url, $order_id );
							echo '<div class="updated"><p>' . $format . '</p></div>';
							break;
						case 2:
							$format = __( 'Make sure you select both a user to receive the gift and a product to give.' , 'woocommerce-give-products' );
							echo '<div class="error"><p>' . $format . '</p></div>';
							break;
						case 3:
							$format = __( 'Processing error - please try again.' , 'woocommerce-give-products' );
							echo '<div class="error"><p>' . $format . '</p></div>';
							break;
						default: break;
					}
				}
			}
		}


		/**
		 * Display message on front-end order view
		 *
		 * @param int $order_id
		 * @since 1.0
		 */
		public function display_given_status( $order_id ) {
			if ( $order_id ) {
				if ( 'yes' === get_post_meta( $order_id, '_wcgp_given_order', true ) ) {
					/* translators: 1: blog info name */
					echo "<div class='given_order'>" . sprintf( __( 'The products in this order were given to you by %s.', 'woocommerce-give-products' ), get_bloginfo( 'name' ) ) . '</div>';
				}
			}
		}


		/**
		 * Display message on back-end order view
		 *
		 * @param WC_Order $order
		 * @since 1.0
		 */
		public function display_given_status_admin( $order ) {
			if ( $order ) {
				if ( version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
					$order_id = $order->get_id();
				} else {
					$order_id = $order->id;
				}

				if ( 'yes' === get_post_meta( $order_id, '_wcgp_given_order', true ) ) {
					echo "<p class='form-field form-field-wide'>" . __( 'This order was given free of charge', 'woocommerce-give-products' ) . '</p>';
				}
			}
		}


		/**
		 * Display message on back-end order view
		 *
		 * @since 1.0
		 */
		public function json_search_products_and_variations() {
			$posts = array();

			check_ajax_referer( 'search-customers', 'security' );

			$term = (string) urldecode( stripslashes( strip_tags( $_GET['term'] ) ) );

			if ( empty( $term ) ) {
				die();
			}

			$post_types = array( 'product', 'product_variation' );

			if ( is_numeric( $term ) ) {

				$args = array(
					'post_type'         => $post_types,
					'post_status'       => 'publish',
					'posts_per_page'    => -1,
					'post__in'          => array( 0, $term ),
					'fields'            => 'ids',
				);

				$args2 = array(
					'post_type'         => $post_types,
					'post_status'       => 'publish',
					'posts_per_page'    => -1,
					'post_parent'       => $term,
					'fields'            => 'ids',
				);

				$posts = array_unique( array_merge( get_posts( $args ), get_posts( $args2 ) ) );

			} else {

				$args = array(
					'post_type'         => $post_types,
					'post_status'       => 'publish',
					'posts_per_page'    => -1,
					's'                 => $term,
					'fields'            => 'ids',
				);

				$args2 = array(
					'post_type'         => $post_types,
					'post_status'       => 'publish',
					'posts_per_page'    => -1,
					'meta_query'        => array(
						array(
						'key'     => '_sku',
						'value'   => $term,
						'compare' => 'LIKE',
						),
					),
					'fields'            => 'ids',
				);

				$posts = array_unique( array_merge( get_posts( $args ), get_posts( $args2 ) ) );

			} // End if().

			$found_products = array();

			foreach ( $posts as $post ) {

				$sku = get_post_meta( $post, '_sku', true );

				if ( isset( $sku ) && $sku ) {
					$sku = ' (SKU: ' . $sku . ')';
				}

				$post_type = get_post_type( $post );

				if ( 'product_variation' === $post_type ) {
					$variation = new WC_Product_Variation( $post );
					$atts = $variation->get_variation_attributes();
					$attlist = '';
					foreach ( $atts as $att ) {
						if ( '' != $attlist ) {
							$attlist .= ', ';
						}
						$attlist .= $att;
					}
					$title = str_replace( 'Variation #' . $post . ' of ', '', get_the_title( $post ) ) . ': ' . ucwords( $attlist );
				} else {
					$title = get_the_title( $post );
				}

				$found_products[ $post ] = $title . ' &ndash; #' . $post . $sku;

			}

			echo json_encode( $found_products );
			die();
		}

		/**
		 * Screen IDS
		 * @param  array  $ids
		 * @return array
		 */
		public function woocommerce_screen_ids( $ids ) {
			return array_merge( $ids, array(
				'product_page_give_products'
			) );
		}

		/**
		 * Return an instance of this class.
		 *
		 * @return object A single instance of this class.
		 * @since  1.0
		 */
		public static function get_instance() {
			// If the single instance hasn't been set, set it now.
			if ( null == self::$instance ) {
				self::$instance = new self;
			}

			return self::$instance;
		}
	}
} // End if().

add_action( 'plugins_loaded', array( 'WC_Give_Products', 'get_instance' ), 0 );
