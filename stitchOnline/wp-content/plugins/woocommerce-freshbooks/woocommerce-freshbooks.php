<?php
/**
 * Plugin Name: WooCommerce FreshBooks
 * Plugin URI: http://www.woocommerce.com/products/woocommerce-freshbooks/
 * Description: A full-featured FreshBooks integration for WooCommerce, automatically sync your orders with invoices and more!
 * Author: SkyVerge
 * Author URI: http://www.woocommerce.com
 * Version: 3.10.1
 * Text Domain: woocommerce-freshbooks
 * Domain Path: /i18n/languages/
 *
 * Copyright: (c) 2012-2017, SkyVerge, Inc. (info@skyverge.com)
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package   WC-FreshBooks
 * @author    SkyVerge
 * @category  Integration
 * @copyright Copyright (c) 2012-2017, SkyVerge, Inc.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 *
 * Woo: 18660:9908157b35835ab64abe650e5fb17af4c631
 */

defined( 'ABSPATH' ) or exit;

// Required functions
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'woo-includes/woo-functions.php' );
}

// Plugin updates
woothemes_queue_update( plugin_basename( __FILE__ ), '157b35835ab64abe650e5fb17af4c631', '18660' );

// WC active check
if ( ! is_woocommerce_active() ) {
	return;
}

// Required library class
if ( ! class_exists( 'SV_WC_Framework_Bootstrap' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'lib/skyverge/woocommerce/class-sv-wc-framework-bootstrap.php' );
}

SV_WC_Framework_Bootstrap::instance()->register_plugin( '4.7.0', __( 'WooCommerce FreshBooks', 'woocommerce-freshbooks' ), __FILE__, 'init_woocommerce_freshbooks', array(
	'minimum_wc_version'   => '2.5.5',
	'minimum_wp_version'   => '4.1',
	'backwards_compatible' => '4.4',
) );

function init_woocommerce_freshbooks() {

/**
 * # WooCommerce FreshBooks Main Plugin Class
 *
 * ## Plugin Overview
 *
 * This plugin integrates FreshBooks with WooCommerce by exporting customers/orders
 * to FreshBooks as clients/invoices
 *
 * ## Features
 *
 * + Create invoices & clients for orders (either automatically or manually)
 * + Apply invoice payments after an order is paid (automatically or manually)
 * + Link products to FreshBooks items
 * + Keep invoices & payments synced between FreshBooks and WooCommerce
 *
 * ## Admin Considerations
 *
 * + 'FreshBooks' setting tab added to WooCommerce > Settings
 * + 'FreshBooks Item' select on Simple/Variable product data tab
 * + 'Invoice Status' column on Orders screen
 * + 'Create & Send Invoice' order action on Orders screen
 * + Custom bulk order actions for creating invoices on Orders screen
 * + Custom bulk filter for invoice status on Order screen
 * + Order actions for creating invoices on Order Edit screen
 * + Custom metabox for invoice info on Order Edit screen
 *
 * ## Frontend Considerations
 *
 * + None
 *
 * ## Database
 *
 * ### Global Options
 *
 * + `wc_freshbooks_api_url` - URL for the FreshBooks API
 * + `wc_freshbooks_authentication_token` - auth token for the API
 * + `wc_freshbooks_debug_mode` - yes to enable debug mode
 * + `wc_freshbooks_default_client` - `none` to create a new client for each customer, or the client ID of a client to create all invoices under
 * + `wc_freshbooks_invoice_language` - the language for created invoices
 * + `wc_freshbooks_invoice_sending_method` - the method to send invoices by, either `email` or `snail_mail`
 * + `wc_freshbooks_use_order_number` - yes to to use WC order number as invoice number, false to let FreshBooks generate it
 * + `wc_freshbooks_invoice_number_prefix` - a prefix for the invoice order number
 * + `wc_freshbooks_auto_create_invoices` - yes to automatically create invoices for all new orders
 * + `wc_freshbooks_auto_send_invoices` - yes to automatically send invoices upon creation
 * + `wc_freshbooks_auto_apply_payments` - yes to automatically apply order payment to the invoice
 *
 * ### Options table
 *
 * + `wc_freshbooks_version` - the current plugin version, set on install/upgrade
 * + `wc_freshbooks_upgraded_from_v2` - bool, set to true when upgraded from a version prior to v3.0
 *
 * ### Order Meta
 *
 * + `_wc_freshbooks_invoice_id` - FreshBooks ID for invoice
 * + `_wc_freshbooks_client_id` - FreshBooks client ID for associated invoice
 * + `_wc_freshbooks_payment_id` - FreshBooks payment ID for the invoice, if one exists
 * + `_wc_freshbooks_invoice_status` - invoice status
 * + `_wc_freshbooks_invoice` - invoice data as serialized array, see WC_FreshBooks_API_Request::parse_get_invoice() for format
 *
 * ### Product Meta
 *
 * + `_wc_freshbooks_item_name` - FreshBooks item name associated with the product
 *
 * ### Customer Meta
 *
 * + `_wc_freshbooks_client_id` - FreshBooks client ID for customer/user
 *
 * @since 3.0
 */
class WC_FreshBooks extends SV_WC_Plugin {


	/** string version number */
	const VERSION = '3.10.1';

	/** @var WC_FreshBooks single instance of this plugin */
	protected static $instance;

	/** string the plugin id */
	const PLUGIN_ID = 'freshbooks';

	/** @var \WC_FreshBooks_Admin instance */
	protected $admin;

	/** @var \WC_FreshBooks_Settings instance */
	protected $settings;

	/** @var \WC_FreshBooks_Orders_Admin instance */
	protected $orders_admin;

	/** @var \WC_FreshBooks_Products_Admin instance */
	protected $products_admin;

	/** @var \WC_FreshBooks_Webhooks instance */
	protected $webhooks;

	/** @var \WC_FreshBooks_Handler instance */
	protected $handler;

	/** @var \WC_FreshBooks_API instance */
	private $api;

	/** @var \WC_FreshBooks_Frontend instance */
	protected $frontend;


	/**
	 * Setup main plugin class
	 *
	 * @since 3.0
	 * @see SV_WC_Plugin::__construct()
	 */
	public function __construct() {

		parent::__construct(
			self::PLUGIN_ID,
			self::VERSION,
			array(
				'text_domain' => 'woocommerce-freshbooks',
			)
		);

		// load includes after WC is loaded
		add_action( 'sv_wc_framework_plugins_loaded', array( $this, 'includes' ), 11 );

		// Subscriptions support
		if ( $this->is_plugin_active( 'woocommerce-subscriptions.php' ) ) {

			// don't copy over FreshBooks invoice meta from the original order to the subscription (subscription objects should not have an invoice)
			add_filter( 'wcs_subscription_meta', array( $this, 'subscriptions_remove_subscription_order_meta' ), 10, 3 );

			// TODO we dropped support for Subscriptions 1.5.x since 3.10.0, but this hook is for an upgrade script for installations migrating to 2.0.0 so we can keep this around a bit longer {FN 2017-03-21}
			// don't copy over FreshBooks invoice meta to subscription object during upgrade from 1.5.x to 2.0
			add_filter( 'wcs_upgrade_subscription_meta_to_copy', array( $this, 'subscriptions_remove_subscription_order_meta_during_upgrade' ) );

			// don't copy over FreshBooks invoice meta from the subscription to the renewal order
			add_filter( 'wcs_renewal_order_meta', array( $this, 'subscriptions_remove_renewal_order_meta' ) );
		}

		// maybe disable API logging
		if ( 'on' !== get_option( 'wc_freshbooks_debug_mode' ) ) {

			remove_action( 'wc_' . $this->get_id() . '_api_request_performed', array( $this, 'log_api_request' ), 10 );
		}
	}


	/**
	 * Loads any required files
	 *
	 * @since 3.0
	 */
	public function includes() {

		require_once( $this->get_plugin_path() . '/includes/class-wc-freshbooks-order.php' );

		$this->handler     = $this->load_class( '/includes/class-wc-freshbooks-handler.php', 'WC_FreshBooks_Handler' );
		$this->webhooks    = $this->load_class( '/includes/class-wc-freshbooks-webhooks.php', 'WC_FreshBooks_Webhooks' );

		if ( is_admin() ) {
			$this->admin_includes();
		} else {
			$this->frontend = $this->load_class( '/includes/class-wc-freshbooks-frontend.php', 'WC_FreshBooks_Frontend' );
		}
	}


	/**
	 * Loads required admin files
	 *
	 * @since 3.0
	 */
	private function admin_includes() {

		// add settings page
		add_filter( 'woocommerce_get_settings_pages', array( $this, 'add_settings_page' ) );

		$this->admin          = $this->load_class( '/includes/admin/class-wc-freshbooks-admin.php', 'WC_FreshBooks_Admin' );
		$this->orders_admin   = $this->load_class( '/includes/admin/class-wc-freshbooks-orders-admin.php', 'WC_FreshBooks_Orders_Admin' );
		$this->products_admin = $this->load_class( '/includes/admin/class-wc-freshbooks-products-admin.php', 'WC_FreshBooks_Products_Admin' );
	}


	/**
	 * Return the admin class instance
	 *
	 * @since 3.9.0
	 * @return \WC_FreshBooks_Admin
	 */
	public function get_admin_instance() {
		return $this->admin;
	}


	/**
	 * Return the settomgs class instance
	 *
	 * @since 3.9.0
	 * @return \WC_FreshBooks_Settings
	 */
	public function get_settings_instance() {
		return $this->settings;
	}


	/**
	 * Return the orders admin class instance
	 *
	 * @since 3.9.0
	 * @return \WC_FreshBooks_Orders_Admin
	 */
	public function get_orders_admin_instance() {
		return $this->orders_admin;
	}


	/**
	 * Return the products admin class instance
	 *
	 * @since 3.9.0
	 * @return \WC_FreshBooks_Products_Admin
	 */
	public function get_products_admin_instance() {
		return $this->products_admin;
	}


	/**
	 * Return the webhooks class instance
	 *
	 * @since 3.9.0
	 * @return \WC_FreshBooks_Webhooks
	 */
	public function get_webhooks_instance() {
		return $this->webhooks;
	}


	/**
	 * Return the handler class instance
	 *
	 * @since 3.9.0
	 * @return \WC_FreshBooks_Handler
	 */
	public function get_handler_instance() {
		return $this->handler;
	}


	/**
	 * Return the frontend class instance
	 *
	 * @since 3.9.0
	 * @return \WC_FreshBooks_Frontend
	 */
	public function get_frontend_instance() {
		return $this->frontend;
	}


	/** Admin methods ******************************************************/


	/**
	 * Add settings page
	 *
	 * @since 3.2.0
	 * @param array $settings
	 * @return array
	 */
	public function add_settings_page( $settings ) {

		if ( ! $this->settings instanceof WC_FreshBooks_Settings ) {
			$this->settings = $this->load_class( '/includes/admin/class-wc-freshbooks-settings.php', 'WC_FreshBooks_Settings' );
		}

		$settings[] = $this->settings;

		return $settings;
	}


	/**
	 * Renders any admin notices
	 *
	 * @since 3.2.0
	 * @see SV_WC_Plugin::add_admin_notices()
	 */
	public function add_delayed_admin_notices() {

		parent::add_delayed_admin_notices();

		// onboarding!
		if ( ! get_option( 'wc_freshbooks_api_url' ) ) {

			if ( get_option( 'wc_freshbooks_upgraded_from_v2' ) ) {

				$message = __( 'Thanks for upgrading to the latest WooCommerce FreshBooks plugin! Please double-check your %1$sinvoice settings%2$s.', 'woocommerce-freshbooks' );

			} else {

				$message = __( 'Thanks for installing the WooCommerce FreshBooks plugin! To get started, please %1$sadd your FreshBooks API credentials%2$s. ', 'woocommerce-freshbooks' );
			}

			$this->get_admin_notice_handler()->add_admin_notice( sprintf( $message, '<a href="' . $this->get_settings_url() . '">', '</a>' ), 'welcome-notice', array( 'notice_class' => 'updated' ) );
		}
	}


	/** Subscriptions compatibility *******************************************/


	/**
	 * Don't copy invoice meta to renewal orders from the WC_Subscription
	 * object. Generally the subscription object should not have any order-specific
	 * meta. This allows an invoice to be created for each renewal order.
	 *
	 * @since 3.5.1
	 * @param array $order_meta order meta to copy
	 * @return array
	 */
	public function subscriptions_remove_renewal_order_meta( $order_meta ) {

		$meta_keys = $this->subscriptions_get_order_meta_keys();

		foreach ( $order_meta as $index => $meta ) {

			if ( in_array( $meta['meta_key'], $meta_keys, false ) ) {
				unset( $order_meta[ $index ] );
			}
		}

		return $order_meta;
	}


	/**
	 * Remove FreshBooks meta when creating a subscription object from an order at checkout.
	 * Subscriptions aren't true orders so they shouldn't have a FreshBooks invoice
	 *
	 * @since 3.5.1
	 * @param array $order_meta meta on order
	 * @param \WC_Subscription $to_order order meta is being copied to
	 * @param \WC_Order $from_order order meta is being copied from
	 * @return array
	 */
	public function subscriptions_remove_subscription_order_meta( $order_meta, $to_order, $from_order ) {

		// only when copying from an order to a subscription
		if ( $to_order instanceof WC_Subscription && $from_order instanceof WC_Order ) {

			$meta_keys = $this->subscriptions_get_order_meta_keys();

			foreach ( $order_meta as $index => $meta ) {

				if ( in_array( $meta['meta_key'], $meta_keys, false ) ) {
					unset( $order_meta[ $index ] );
				}
			}
		}

		return $order_meta;
	}


	/**
	 * Don't copy over FreshBooks meta during the upgrade from Subscription 1.5.x to 2.0
	 *
	 * @since 3.5.1
	 * @param array $order_meta meta to copy
	 * @return array
	 */
	public function subscriptions_remove_subscription_order_meta_during_upgrade( $order_meta ) {

		foreach ( $this->subscriptions_get_order_meta_keys() as $meta_key ) {

			if ( isset( $order_meta[ $meta_key ] ) ) {
				unset( $order_meta[ $meta_key ] );
			}
		}

		return $order_meta;
	}


	/**
	 * Returns an array of meta keys used by FreshBooks
	 *
	 * @since 3.5.1
	 * @return array
	 */
	protected function subscriptions_get_order_meta_keys() {

		return array(
			'_wc_freshbooks_invoice_id',
			'_wc_freshbooks_client_id',
			'_wc_freshbooks_payment_id',
			'_wc_freshbooks_invoice_status',
			'_wc_freshbooks_invoice',
		);
	}


	/**
	 * Returns the default FreshBooks payment type settings.
	 *
	 * @since 3.6.0
	 * @return array
	 */
	public function get_default_fb_payment_type_mapping() {

		$defaults = array(
			// Bank transfer gateways.
			'authorize_net_aim_echeck'       => 'Bank Transfer',
			'authorize_net_cim_echeck'       => 'Bank Transfer',
			'bacs'                           => 'Bank Transfer',
			'cybersource_sa_sop_echeck'      => 'Bank Transfer',
			'dwolla'                         => 'Bank Transfer',
			'netbilling_echeck'              => 'Bank Transfer',

			// Cash gateways.
			'cod'                            => 'Cash',

			// Check gateways.
			'cheque'                         => 'Check',

			// Credit card gateways.
			'authorize_net_aim'              => 'Credit Card',
			'authorize_net_cim_credit_card'  => 'Credit Card',
			'beanstream'                     => 'Credit Card',
			'braintree_credit_card'          => 'Credit Card',
			'chase_paymentech'               => 'Credit Card',
			'cybersource'                    => 'Credit Card',
			'cybersource_sa_sop_credit_card' => 'Credit Card',
			'elavon_vm'                      => 'Credit Card',
			'firstdata'                      => 'Credit Card',
			'intuit_qbms'                    => 'Credit Card',
			'moneris'                        => 'Credit Card',
			'netbilling'                     => 'Credit Card',
			'realex'                         => 'Credit Card',
			'realex_redirect'                => 'Credit Card',
			'securenet'                      => 'Credit Card',
			'simplify_commerce'              => 'Credit Card',
			'usaepay'                        => 'Credit Card',
			'wepay'                          => 'Credit Card',

			// PayPal gateways.
			'paypal'                         => 'PayPal',
			'braintree_paypal'               => 'PayPal',
			'paypal_express'                 => 'PayPal',
		);

		return $defaults;
	}


	/**
	 * Returns the current FreshBooks payment type settings.
	 *
	 * @since 3.6.0
	 * @return array
	 */
	public function get_fb_payment_type_mapping() {

		// Merge current mapping into default mapping.
		$mapping = array_merge(
			$this->get_default_fb_payment_type_mapping(),
			WC_Admin_Settings::get_option( 'wc_freshbooks_payment_type_mapping', array() )
		);

		// Remove mapping settings for gateways that aren't installed.
		$mapping = array_intersect_key(
			$mapping,
			WC()->payment_gateways()->payment_gateways()
		);

		return $mapping;
	}


	/** Helper methods ******************************************************/


	/**
	 * Main FreshBooks Instance, ensures only one instance is/can be loaded
	 *
	 * @since 3.3.0
	 * @see wc_freshbooks()
	 * @return \WC_FreshBooks
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}


	/**
	 * Lazy load the FreshBooks API wrapper
	 *
	 * @since 3.0
	 * @return \WC_FreshBooks_API instance
	 * @throws \SV_WC_API_Exception missing API URL or authentication token settings
	 */
	public function get_api() {

		if ( is_object( $this->api ) ) {
			return $this->api;
		}

		$api_url              = get_option( 'wc_freshbooks_api_url' );
		$authentication_token = get_option( 'wc_freshbooks_authentication_token' );

		// bail if required info is not available
		if ( ! $api_url || ! $authentication_token ) {
			throw new SV_WC_API_Exception( __( 'Missing API URL or Authentication Token', 'woocommerce-freshbooks' ) );
		}

		// bail if API URL does not appear to be valid
		if ( false === strpos( $api_url, 'freshbooks.com/api/2.1/xml-in' ) ) {
			throw new SV_WC_API_Exception( __( 'Incorrect API URL', 'woocommerce-freshbooks' ) );
		}

		// load API files
		require_once( $this->get_plugin_path() . '/includes/api/class-wc-freshbooks-api-request.php' );
		require_once( $this->get_plugin_path() . '/includes/api/class-wc-freshbooks-api-response.php' );
		require_once( $this->get_plugin_path() . '/includes/api/class-wc-freshbooks-api.php' );

		return $this->api = new WC_FreshBooks_API( $api_url, $authentication_token );
	}


	/** Getter methods ******************************************************/


	/**
	 * Returns the plugin name, localized
	 *
	 * @since 3.0
	 * @see SV_WC_Plugin::get_plugin_name()
	 * @return string the plugin name
	 */
	public function get_plugin_name() {
		return __( 'WooCommerce FreshBooks', 'woocommerce-freshbooks' );
	}


	/**
	 * Returns __FILE__
	 *
	 * @since 3.0
	 * @see SV_WC_Plugin::get_file()
	 * @return string the full path and filename of the plugin file
	 */
	protected function get_file() {
		return __FILE__;
	}


	/**
	 * Gets the plugin configuration URL
	 *
	 * @since 3.0
	 * @see SV_WC_Plugin::get_settings_url()
	 * @param string $_ unused
	 * @return string plugin settings URL
	 */
	public function get_settings_url( $_ = null ) {
		return admin_url( 'admin.php?page=wc-settings&tab=freshbooks' );
	}


	/**
	 * Returns conditional dependencies based on the FTP security selected
	 *
	 * @since 1.1
	 * @see SV_WC_Plugin::get_dependencies()
	 * @return array of dependencies
	 */
	protected function get_dependencies() {
		return array( 'xmlwriter' );
	}


	/**
	 * Gets the plugin documentation url
	 *
	 * @since 3.5.0
	 * @see SV_WC_Plugin::get_documentation_url()
	 * @return string documentation URL
	 */
	public function get_documentation_url() {
		return 'https://docs.woocommerce.com/document/woocommerce-freshbooks/';
	}


	/**
	 * Gets the plugin support URL
	 *
	 * @since 3.5.0
	 * @see SV_WC_Plugin::get_support_url()
	 * @return string
	 */
	public function get_support_url() {
		return 'https://woocommerce.com/my-account/tickets/';
	}


	/** Lifecycle methods ******************************************************/


	/**
	 * Run install
	 *
	 * @since 3.0
	 * @see SV_WC_Plugin::install()
	 */
	protected function install() {

		// include settings so we can install defaults
		require_once( WC()->plugin_path() . '/includes/admin/settings/class-wc-settings-page.php' );
		$this->settings = $this->load_class( '/includes/admin/class-wc-freshbooks-settings.php', 'WC_FreshBooks_Settings' );

		foreach ( $this->settings->get_settings() as $setting ) {

			if ( isset( $setting['default'] ) ) {

				update_option( $setting['id'], $setting['default'] );
			}
		}

		// versions prior to 3.0 did not set a version option, so the upgrade
		// method needs to be called manually
		if ( get_option( 'wc_fb_api_url' ) ) {

			$this->upgrade( '2.1.3' );
		}
	}


	/**
	 * Perform any version-related changes.
	 *
	 * @since 3.0
	 * @see SV_WC_Plugin::upgrade()
	 * @param int $installed_version the currently installed version of the plugin
	 */
	protected function upgrade( $installed_version ) {

		// upgrade to 3.0
		if ( version_compare( $installed_version, '3.0', '<' ) ) {

			// API URL / token
			update_option( 'wc_freshbooks_api_url',              get_option( 'wc_fb_api_url' ) );
			update_option( 'wc_freshbooks_authentication_token', get_option( 'wc_fb_api_token' ) );

			// invoice send method
			update_option( 'wc_freshbooks_invoice_sending_method', ( 'SnailMail' === get_option( 'wc_fb_send_method' ) ? 'snail_mail' : 'email' ) );

			// use order number as invoice number
			update_option( 'wc_freshbooks_use_order_number', ( get_option( 'wc_fb_use_order_number' ) ? 'yes' : 'no' ) );

			// invoice number prefix
			update_option( 'wc_freshbooks_invoice_number_prefix', get_option( 'wc_fb_inv_num_prefix' ) );

			// auto-send invoice
			update_option( 'wc_freshbooks_auto_send_invoices', ( get_option( 'wc_fb_send_invoice' ) ? 'yes' : 'no' ) );

			// auto-apply payments
			update_option( 'wc_freshbooks_auto_apply_payments', ( get_option( 'wc_fb_add_payments' ) ? 'yes' : 'no' ) );

			// mark as migrated
			update_option( 'wc_freshbooks_upgraded_from_v2', 1 );

			// remove old options
			$old_options = array(
				'wc_fb_api_url',
				'wc_fb_api_token',
				'wc_fb_create_client',
				'wc_fb_generic_client',
				'wc_fb_add_payments',
				'wc_fb_send_invoice',
				'wc_fb_send_method',
				'wc_fb_use_order_number',
				'wc_fb_inv_num_prefix'
			);

			foreach ( $old_options as $option ) {

				delete_option( $option );
			}
		}

		// upgrade to 3.8.0
		if ( version_compare( $installed_version, '3.8.0', '<' ) ) {

			if ( 'none' !== get_option( 'wc_freshbooks_default_client' ) ) {

				// we shouldn't display the "View Invoice" order action by default
				// when a site is using a default FreshBooks client because FreshBooks
				// automatically logs in when the client view link is visted
				add_option( 'wc_freshbooks_display_view_invoice_my_account', 'no' );
			}
		}
	}


} // end \WC_FreshBooks class


/**
 * Returns the One True Instance of FreshBooks
 *
 * @since 3.3.0
 * @return \WC_FreshBooks
 */
function wc_freshbooks() {
	return WC_FreshBooks::instance();
}


// fire it up!
wc_freshbooks();

} // init_woocommerce_freshbooks
