=== WooCommerce FreshBooks ===
Author: skyverge, woocommerce
Tags: woocommerce
Requires at least: 4.1
Tested up to: 4.8
WC requires at least: 2.5.5
WC tested up to: 3.1.1

A full-featured FreshBooks integration for WooCommerce, automatically sync your orders with invoices and more!

See http://docs.woothemes.com/document/woocommerce-freshbooks/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-freshbooks' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
