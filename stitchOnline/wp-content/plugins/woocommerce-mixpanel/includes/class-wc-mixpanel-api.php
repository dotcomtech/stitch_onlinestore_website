<?php
/**
 * WooCommerce Mixpanel
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Mixpanel to newer
 * versions in the future. If you wish to customize WooCommerce Mixpanel for your
 * needs please refer to http://docs.woocommerce.com/document/mixpanel/ for more information.
 *
 * @package     WC-Mixpanel/Classes
 * @author      SkyVerge
 * @copyright   Copyright (c) 2012-2017, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

defined( 'ABSPATH' ) or exit;

/**
 * Mixpanel API Class
 *
 * Wrapper for the Mixpanel HTTP API
 *
 * @link https://mixpanel.com/docs/api-documentation/http-specification-insert-data
 * @since 1.0
 */
class WC_Mixpanel_API {


	/** string hostname of the mixpanel tracking server */
	const HOST = 'https://api.mixpanel.com';

	/** @var string mixpanel auth token */
	private $token;

	/** @var string distinct id used by mixpanel to identify visitors */
	private $distinct_id;

	/** @var string email or username used to identify visitors in the stream view */
	private $name_tag;

	/** @var bool log queries */
	private $log_queries = false;

	/** @var bool log errors */
	private $log_errors = false;

	/** @var bool have mixpanel geocode visitor IPs */
	private $geocode_ips = false;


	/**
	 * Setup API
	 *
	 * @since 1.0
	 * @param string $token required API Key to use
	 * @param array $options optional API options
	 * @return \WC_Mixpanel_API
	 */
	public function __construct( $token, $options = array() ) {

		$this->token = $token;

		$this->log_queries = $this->array_get( $options, 'log_queries', $this->log_queries );

		$this->log_errors = $this->array_get( $options, 'log_errors', $this->log_errors );

		$this->geocode_ips = $this->array_get( $options, 'geocode_ips', $this->geocode_ips );
	}


	/**
	 * Set distinct ID and name tag for visitor
	 *
	 * @since 1.0
	 * @param string $id identity of person doing event / setting properties on
	 * @param string $name_tag optional name tag for display in the stream view
	 */
	public function identify( $id, $name_tag = null ) {

		$this->distinct_id = $id;

		$this->name_tag = $name_tag;
	}


	/**
	 * Record event
	 *
	 * @link https://mixpanel.com/docs/api-documentation/http-specification-insert-data
	 *
	 * @since 1.0
	 * @param string|array $event_name Name of event
	 * @param array $properties properties to set with event
	 * @param int $time optional unix timestamp to set as the time of the event, mainly for importing old data
	 */
	public function track( $event_name, $properties = array(), $time = null ) {

		if ( isset( $time ) ) {

			$properties = array_merge( $properties, array( 'time' => $time ) );
		}

		$data = array( 'event' => $event_name, 'properties' => $properties );

		$this->build_query( 'track', $data );
	}


	/**
	 * Register properties on a visitor
	 *
	 * @link https://mixpanel.com/docs/people-analytics/people-http-specification-insert-data
	 *
	 * @since 1.0
	 * @param string $action action to take when setting property, either 'set' or 'increment'
	 * @param array $properties properties to set on user
	 */
	public function register( $action, $properties ) {

		$data = array( sprintf( '$%s', $action ) => $properties );

		$this->build_query( 'engage', $data );
	}


	/**
	 * Build query string
	 *
	 * @since 1.0
	 * @param string $path API endpoint
	 * @param array $data associative array of data to build into query
	 */
	private function build_query( $path, $data ) {

		// Include token / distinct id with every API request
		$reserved_properties = array(
			'token'       => $this->token,
			'distinct_id' => $this->distinct_id,
		);

		// Include name tag if available
		if ( $this->name_tag ) {
			$reserved_properties['mp_name_tag'] = $this->name_tag;
		}

		// Include client IP to allow geolocation
		if ( $this->geocode_ips ) {
			$reserved_properties['$ip'] = isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
		}

		// Set the timestamp to now if time was not passed
		if ( ! isset( $data['properties']['time'] ) ) {
			$reserved_properties['time'] = time();
		}

		// Add reserved properties (track / engage have different required data formats )
		if ( 'track' == $path ) {
			$data['properties'] = array_merge( $data['properties'], $reserved_properties );
		} else {
			$data = array_merge( $data, array( '$token' => $this->token, '$distinct_id' => $this->distinct_id ) );
		}

		// Build the query
		$query = self::HOST . "/{$path}/?data="; // https://api.mixpanel.com/engage/?data=
		$query .= base64_encode( json_encode( $data ) );

		// Completed queries should look like:
		//http://api.mixpanel.com/engage/?data=eyIkc2V0IjogeyIkZmlyc3RfbmFtZSI6ICJKb2huIiwgIiRsYXN0X25hbWUiOiAiU21pdGgifSwiJHRva2VuIjogIjM2YWRhNWIxMGRhMzlhMTM0NzU1OTMyMWJhZjEzMDYzIiwiJGRpc3RpbmN0X2lkIjogIjEzNzkzIn0=

		// log queries
		if ( $this->log_queries ) {
			wc_mixpanel()->log( 'Query : ' . $query );
		}

		// send query via WP HTTP API
		$this->send_query( $query );
	}


	/**
	 * Sends query via WP HTTP API (GET method)
	 *
	 * @param string $query URL to GET
	 */
	private function send_query( $query ) {

		$response = wp_safe_remote_get( $query );

		// Check errors
		if ( $this->log_errors ) {

			// In case of network timeout, WP HTTP will throw WP error
			if ( is_wp_error( $response ) ) {
				wc_mixpanel()->log( sprintf( 'Failed to send query! Error Code: %s | Error Message: %s ', $response->get_error_code(), $response->get_error_message() ) );
			}

			// API always returns HTTP 200, but will return 0 if there was an error in the query
			elseif ( isset( $response['body'] ) && '0' == $response['body'] ) {
				wc_mixpanel()->log( sprintf( 'Failed to send query : %s', $query ) );
			}
		}
	}


	/**
	 * Alias visitor to a distinct id
	 *
	 * Undocumented API call to alias a user. Used to alias logged in users to their anonymous distinct IDs when they sign in.
	 *
	 * @since 1.1
	 * @param string $distinct_id distinct ID to alias
	 * @param string $alias_to ID to alias user to
	 */
	public function alias( $distinct_id, $alias_to ) {

		$data = array(
			'event'      => '$create_alias',
			'properties' => array(
				'$os'                       => 'Windows',
				'$browser'                  => 'Chrome',
				'mp_lib'                    => 'web',
				'distinct_id'               => $distinct_id,
				'$initial_referrer'         => '$direct',
				'$initial_referring_domain' => '$direct',
				'alias'                     => $alias_to,
				'token'                     => $this->token
			)
		);

		// Build the query
		$query = self::HOST . '/track/?data=';
		$query .= base64_encode( json_encode( $data ) );

		// log queries
		if ( $this->log_queries ) {
			wc_mixpanel()->log( 'Query : ' . $query );
		}

		// send query via WP HTTP API
		$this->send_query( $query );
	}

	/**
	 * Helper to parse options array
	 *
	 * @since 1.0
	 * @param array $array required array to parse
	 * @param string $key required key to return value for
	 * @param mixed $default optional return a default value if key not set
	 * @param bool $treat_empty_as_not_set optional flag
	 * @return mixed|null
	 */
	private function array_get( $array, $key, $default = null, $treat_empty_as_not_set = false ) {

		if ( ! is_array( $array ) ) {
			return( $default );
		}

		if ( array_key_exists( $key, $array ) && ( ! $treat_empty_as_not_set || ! empty( $array[$key] ) ) ) {

			return( $array[$key] );

		} else {

			return( $default );
		}
	}


}
