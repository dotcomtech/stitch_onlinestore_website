<?php
/**
 * WooCommerce Mixpanel
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Mixpanel to newer
 * versions in the future. If you wish to customize WooCommerce Mixpanel for your
 * needs please refer to http://docs.woocommerce.com/document/mixpanel/ for more information.
 *
 * @package     WC-Mixpanel/Classes
 * @author      SkyVerge
 * @copyright   Copyright (c) 2012-2017, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

defined( 'ABSPATH' ) or exit;

/**
 * Mixpanel Integration class
 *
 * Handles settings and tracking functionality
 *
 * @since 1.0
 * @extends \WC_Integration
 */
class WC_Mixpanel_Integration extends WC_Integration {


	/** @var string Mixpanel API Key */
	public $token;

	/** @var string how to identify visitors, either WP username or email */
	public $nametag_pref;

	/** @var array of event names */
	public $event_name = array();

	/** @var array of property names */
	public $property_name = array();

	/** @var \WC_Mixpanel_API instance */
	private $api;

	/** @var array $api_options */
	private $api_options;


	/**
	 * load settings and setup hooks
	 *
	 * @since 1.0
	 * @return \WC_Mixpanel_Integration
	 */
	public function __construct() {

		// Setup plugin
		$this->id                 = 'mixpanel';
		$this->method_title       = __( 'Mixpanel', 'woocommerce-mixpanel' );
		$this->method_description = __( 'Real time Web analytics tool that tracks visitors to your site as people, not pageviews. Visualize your online sales funnels and find out which ones are driving revenue and which are not.', 'woocommerce-mixpanel' );

		// Load admin form
		$this->init_form_fields();

		// Load settings
		$this->init_settings();

		// Set API Key / Identity Preference
		$this->token        = $this->settings['token'];
		$this->nametag_pref = $this->settings['nametag_pref'];
		$this->logging      = $this->settings['logging'];

		// Load event / property names
		foreach ( $this->settings as $key => $value ) {

			if ( strpos( $key, 'event_name' ) !== false ) {
				// event name setting, remove '_event_name' and use as key
				$key = str_replace( '_event_name', '', $key );
				$this->event_name[ $key ] = $value;

			} elseif ( strpos( $key, 'property_name' ) !== false ) {
				// property name setting, remove '_property_name' and use as key
				$key = str_replace( '_property_name', '', $key );
				$this->property_name[ $key ] = $value;
			}
		}

		// Setup API options
		$this->api_options = array();

		// Enable geolocation of client IPs if selected
		if ( 'yes' == $this->settings['geocode_ips'] ) {

			$this->api_options['geocode_ips'] = true;
		}

		// Logging Preference
		switch ( $this->logging ) {

			case 'queries':
				$this->api_options = array_merge( $this->api_options, array( 'log_queries' => true ) );
				break;

			case 'errors':
				$this->api_options = array_merge( $this->api_options, array( 'log_errors' => true ) );
				break;

			case 'queries_and_errors':
				$this->api_options = array_merge( $this->api_options, array( 'log_queries' => true, 'log_errors' => true ) );
				break;
		}

		// sanitize admin options before saving
		add_filter( 'woocommerce_settings_api_sanitized_fields_mixpanel', array( $this, 'filter_admin_options' ) );

		// Add hooks to record events - only add hook if event name is populated

		// Header Javascript Code, only add is Token is populated
		if ( $this->token ) {
			add_action( 'wp_head', array( $this, 'output_head' ) );
			add_action( 'login_head', array( $this, 'output_head' ) );
		}

		// Signed in
		if ( $this->event_name['signed_in'] ) {
			add_action( 'wp_login', array( $this, 'signed_in' ), 10, 2 );
		}

		// Signed out
		if ( $this->event_name['signed_out'] ) {
			add_action( 'wp_logout', array( $this, 'signed_out' ) );
		}

		// Viewed Signup page (on my account page, if enabled)
		if ( $this->event_name['viewed_signup'] ) {
			add_action( 'register_form', array( $this, 'viewed_signup' ) );
		}

		// Signed up for new account (on my account page if enabled OR during checkout)
		if ( $this->event_name['signed_up'] ) {
			add_action( 'user_register', array( $this, 'signed_up' ) );
		}

		// Viewed Product (Properties: Name)
		if ( $this->event_name['viewed_product'] ) {
			add_action( 'woocommerce_after_single_product', array( $this, 'viewed_product' ) );
		}

		// Added Product to Cart (Properties: Product Name, Quantity)
		if ( $this->event_name['added_to_cart'] ) {
			// single product add to cart button
			add_action( 'woocommerce_add_to_cart', array( $this, 'added_to_cart' ), 10, 6 );

			// AJAX add to cart
			if ( is_ajax() ) {
				add_action( 'woocommerce_ajax_added_to_cart', array( $this, 'ajax_added_to_cart' ) );
			}
		}

		// Removed Product from Cart (Properties: Product Name)
		if ( $this->event_name['removed_from_cart'] ) {
			add_action( 'woocommerce_before_cart_item_quantity_zero', array( $this, 'removed_from_cart' ) );
			add_action( 'woocommerce_remove_cart_item',               array( $this, 'removed_from_cart' ) );
		}

		// Changed Quantity of Product in Cart (Properties: Product Name, Quantity )
		if ( $this->event_name['changed_cart_quantity'] ) {
			add_action( 'woocommerce_after_cart_item_quantity_update', array( $this, 'changed_cart_quantity' ), 10, 2 );
		}

		// Viewed Cart
		if ( $this->event_name['viewed_cart'] ) {
			add_action( 'woocommerce_after_cart_contents', array( $this, 'viewed_cart' ) );
			add_action( 'woocommerce_cart_is_empty', array( $this, 'viewed_cart' ) );
		}

		// Started Checkout
		if ( $this->event_name['started_checkout'] ) {
			add_action( 'woocommerce_after_checkout_form', array( $this, 'started_checkout' ) );
		}

		// Started Payment (for gateways that direct post from payment page, eg: Braintree TR, Authorize.net AIM, etc
		if ( $this->event_name['started_payment'] ) {
			add_action( 'after_woocommerce_pay', array( $this, 'started_payment' ) );
		}

		// Completed Purchase
		if ( $this->event_name['completed_purchase'] ) {

			// most orders will call payment complete
			add_action( 'woocommerce_payment_complete', array( $this, 'completed_purchase' ) );

			// catch orders where the order is placed but not yet paid
			add_action( 'woocommerce_order_status_on-hold', array( $this, 'completed_purchase' ) );

			// catch orders where the payment previously failed and was manually changed by the admin
			add_action( 'woocommerce_order_status_failed_to_processing', array( $this, 'completed_purchase' ) );
			add_action( 'woocommerce_order_status_failed_to_completed',  array( $this, 'completed_purchase' ) );
		}

		// Completed Payment
		if ( $this->event_name['completed_payment'] ) {

			add_action( 'woocommerce_order_status_processing',            array( $this, 'completed_payment' ) );
			add_action( 'woocommerce_order_status_on-hold_to_completed',  array( $this, 'completed_payment' ) );
		}

		// Wrote Review or Commented (Properties: Product Name if review, Post Title if blog post)
		if ( $this->event_name['wrote_review'] || $this->event_name['commented'] ) {
			add_action( 'comment_post', array( $this, 'wrote_review_or_commented' ) );
		}

		// Viewed Account
		if ( $this->event_name['viewed_account'] ) {
			add_action( 'woocommerce_after_my_account', array( $this, 'viewed_account' ) );
		}

		// Viewed Order
		if ( $this->event_name['viewed_order'] ) {
			add_action( 'woocommerce_view_order', array( $this, 'viewed_order' ) );
		}

		// Updated Address
		if ( $this->event_name['updated_address'] ) {
			add_action( 'woocommerce_customer_save_address', array( $this, 'updated_address' ) );
		}

		// Changed Password
		if ( $this->event_name['changed_password'] && ! empty( $_POST['password_1'] ) ) {
			add_action( 'woocommerce_save_account_details', array( $this, 'changed_password' ) );
		}

		// Applied Coupon
		if ( $this->event_name['applied_coupon'] ) {
			add_action( 'woocommerce_applied_coupon', array( $this, 'applied_coupon' ) );
		}

		// Tracked Order
		if ( $this->event_name['tracked_order'] ) {
			add_action( 'woocommerce_track_order', array( $this, 'tracked_order' ) );
		}

		// Estimated Shipping
		if ( $this->event_name['estimated_shipping'] ) {
			add_action( 'woocommerce_calculated_shipping', array( $this, 'estimated_shipping' ) );
		}

		// Cancelled Order
		if ( $this->event_name['cancelled_order'] ) {
			add_action( 'woocommerce_cancelled_order', array( $this, 'cancelled_order' ) );
		}

		// Reordered Previous Order
		if ( $this->event_name['reordered'] ) {
			add_action( 'woocommerce_ordered_again', array( $this, 'reordered' ) );
		}

		// Save admin options
		if ( is_admin() ) {
			add_action( 'woocommerce_update_options_integration_mixpanel', array( $this, 'process_admin_options' ) );
		}

	}


	/**
	 * Track login event
	 *
	 * @since 1.0
	 * @param string $user_login
	 * @param \WP_User $user
	 */
	public function signed_in( $user_login, $user ) {

		if ( in_array( $user->roles[0], apply_filters( 'wc_mixpanel_signed_in_user_roles', array( 'subscriber', 'customer' ) ) ) ) {

			$this->api_track_event( $this->event_name['signed_in'] );
			$this->api_register_property( array_merge( $this->get_user_properties( $user ), array( '$last_login' => date( 'r' ) ) ) );
		}
	}


	/**
	 * Track sign out
	 *
	 * @since 1.0
	 */
	public function signed_out() {

		$this->api_track_event( $this->event_name['signed_out'] );
	}


	/**
	 * Track sign up
	 *
	 * @since 1.0
	 */
	public function signed_up( $user_id ) {

		$this->api_track_event( $this->event_name['signed_up'] );

		$user = new WP_User( $user_id );

		$this->api_register_property( array_merge( $this->get_user_properties( $user ), array( '$last_login' => date( 'r' ) ) ) );

		$this->get_api()->alias( $this->get_distinct_id(), $this->get_name_tag( $user_id ) );
	}


	/**
	 * Track sign up view
	 *
	 * @since  1.0
	 */
	public function viewed_signup() {

		if ( $this->not_page_reload() )
			$this->js_track_event( $this->event_name['viewed_signup'] );
	}


	/**
	 * Track product view
	 *
	 * @since  1.0
	 */
	public function viewed_product() {

		if ( $this->not_page_reload() )
			$this->js_track_event( $this->event_name['viewed_product'], array( $this->property_name['product_name'] => get_the_title() ) );
	}


	/**
	 * Track add to cart
	 *
	 * @since 1.0
	 * @param string $cart_item_key
	 * @param int $product_id
	 * @param int $quantity
	 * @param int $variation_id
	 * @param array $variation
	 * @param array $cart_item_data
	 */
	public function added_to_cart( $cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data ) {

		// don't track add to cart from AJAX POST here
		if ( is_ajax() ) {
			return;
		}

		$product = wc_get_product( $product_id );

		$properties = array(
			$this->property_name['product_name']  => htmlentities( $product->get_title(), ENT_QUOTES, 'UTF-8' ),
			$this->property_name['quantity']      => $quantity,
			$this->property_name['category']      => trim( strip_tags( SV_WC_Product_Compatibility::wc_get_product_category_list( $product ) ) ),
			$this->property_name['product_price'] => SV_WC_Helper::number_format( $product->get_price() ),
		);

		if ( ! empty( $variation ) ) {
			// Added a variable product to cart, set attributes as properties
			// Remove 'pa_' from keys to keep property names consistent
			// note: `pa_` was replaced with `attribute_` in 2.1, so `pa_` can be removed once 2.1+ is required
			$variation = array_flip( str_replace( array( 'pa_', 'attribute_' ), '', array_flip( $variation ) ) );

			$properties = array_merge( $properties, $variation );
		}

		$this->api_track_event( $this->event_name['added_to_cart'], $properties );
	}


	/**
	 * Track AJAX add to cart
	 *
	 * @since 1.0
	 * @param int $product_id
	 */
	public function ajax_added_to_cart( $product_id ) {

		$product = wc_get_product( $product_id );

		$properties = array(
			$this->property_name['product_name'] => htmlentities( $product->get_title(), ENT_QUOTES, 'UTF-8' ),
			$this->property_name['quantity']     => 1,
			$this->property_name['category']     => trim( strip_tags( SV_WC_Product_Compatibility::wc_get_product_category_list( $product ) ) ),
			$this->property_name['product_price'] => SV_WC_Helper::number_format( $product->get_price() ),
		);

		$this->api_track_event( $this->event_name['added_to_cart'], $properties );
	}


	/**
	 * Track remove from cart
	 *
	 * @since 1.0
	 * @param string $cart_item_key
	 */
	public function removed_from_cart( $cart_item_key ) {

		if ( isset( WC()->cart->cart_contents[ $cart_item_key ] ) ) {

			$item = WC()->cart->cart_contents[ $cart_item_key ];

			$this->api_track_event( $this->event_name['removed_from_cart'], array( $this->property_name['product_name'] => get_the_title( $item['product_id'] ) ) );
		}
	}


	/**
	 * Track quantity change in cart
	 *
	 * @since 1.0
	 * @param string $cart_item_key
	 * @param int $quantity
	 */
	public function changed_cart_quantity( $cart_item_key, $quantity ) {

		if ( isset( WC()->cart->cart_contents[ $cart_item_key ] ) ) {

			$item = WC()->cart->cart_contents[ $cart_item_key ];

			$this->api_track_event( $this->event_name['changed_cart_quantity'], array( $this->property_name['product_name'] => get_the_title( $item['product_id'] ), $this->property_name['quantity'] => $quantity ) );
		}
	}


	/**
	 * Track cart view
	 *
	 * @since 1.0
	 */
	public function viewed_cart() {

		if ( $this->not_page_reload() ) {
			$this->js_track_event( $this->event_name['viewed_cart'] );
		}

	}


	/**
	 * Track checkout start
	 *
	 * @since 1.0
	 */
	public function started_checkout() {

		if ( $this->not_page_reload() ) {
			$this->js_track_event( $this->event_name['started_checkout'] );
		}
	}


	/**
	 * Track payment start
	 *
	 * @since 1.0
	 */
	public function started_payment() {

		if ( $this->not_page_reload() ) {
			$this->js_track_event( $this->event_name['started_payment'] );
		}
	}


	/**
	 * Track commenting (either post or product review)
	 *
	 * @since 1.0
	 */
	public function wrote_review_or_commented() {

		$type = get_post_type();

		if ( $type == 'product' ) {

			if ( $this->event_name['wrote_review'] )
				$this->api_track_event( $this->event_name['wrote_review'], array( $this->property_name['product_name'] => get_the_title() ) );

		} elseif ( $type == 'post' ) {

			if ( $this->event_name['commented'] )
				$this->api_track_event( $this->event_name['commented'], array( $this->property_name['post_title'] => get_the_title() ) );
		}
	}


	/**
	 * Track completed purchase. Note this is triggered regardless of the payment
	 * status for the order. Payments are handled below in completed_payment()
	 *
	 * @since 1.0
	 * @param int $order_id
	 */
	public function completed_purchase( $order_id ) {

		if ( metadata_exists( 'post', $order_id, '_wc_mixpanel_tracked' ) ) {
			return;
		}

		$order = wc_get_order( $order_id );

		$identity = $order->get_user_id() ? $this->get_name_tag( $order->get_user_id() ) :  SV_WC_Order_Compatibility::get_prop( $order, 'billing_email' );

		// purchase properties
		$properties = array(
			$this->property_name['order_id']       => $order->get_order_number(),
			$this->property_name['order_total']    => $order->get_total(),
			$this->property_name['shipping_total'] => $order->get_total_shipping(),
			$this->property_name['total_quantity'] => $order->get_item_count(),
			$this->property_name['payment_method'] => SV_WC_Order_Compatibility::get_prop( $order, 'payment_method_title' ),
		);

		// record purchase
		$this->api_track_event( $this->event_name['completed_purchase'], $properties, $identity );

		// set properties on visitor
		$this->api_register_property( array(
				'$email'      => SV_WC_Order_Compatibility::get_prop( $order, 'billing_email' ),
				'$first_name' => SV_WC_Order_Compatibility::get_prop( $order, 'billing_first_name' ),
				'$last_name'  => SV_WC_Order_Compatibility::get_prop( $order, 'billing_last_name' ),
				'$ip'         => $this->get_client_ip(),
		), $identity );

		// record revenue
		if ( ! $this->disable_tracking() ) {

			$this->api_track_revenue( $order, $identity );
		}

		// mark order as tracked
		SV_WC_Order_Compatibility::update_meta_data( $order, '_wc_mixpanel_tracked', 1 );
	}


	/**
	 * Track completed payment for orders that were previously on-hold, like
	 * BACS or cheque orders
	 *
	 * @since 1.8.0
	 * @param int $order_id
	 */
	public function completed_payment( $order_id ) {

		$order = wc_get_order( $order_id );

		// orders marked as paid will be tracked in completed_purchase()
		if ( $order->is_paid() || metadata_exists( 'post', $order_id, '_wc_mixpanel_completed_payment_tracked' ) ) {
			return;
		}

		add_filter( 'wc_mixpanel_disable_tracking', '__return_false' );

		$identity = $order->get_user_id() ? $this->get_name_tag( $order->get_user_id() ) : SV_WC_Order_Compatibility::get_prop( $order, 'billing_email' );

		$properties = array( $this->get_property_name( 'order_id', 'completed_payment' ) => $order->get_order_number() );

		$this->api_track_event( $this->event_name['completed_payment'], $properties, $identity );

		// mark order as tracked
		SV_WC_Order_Compatibility::update_meta_data( $order, '_wc_mixpanel_completed_payment_tracked', 1 );
	}


	/**
	 * Track account view
	 *
	 * @since 1.0
	 */
	public function viewed_account() {

		$this->js_track_event( $this->event_name['viewed_account'] );
	}


	/**
	 * Track order view
	 *
	 * @since 1.0
	 */
	public function viewed_order() {

		$this->api_track_event( $this->event_name['viewed_order'] );
	}


	/**
	 * Track address update
	 *
	 * @since 1.0
	 */
	public function updated_address() {

		$this->api_track_event( $this->event_name['updated_address'] );
	}


	/**
	 * Track password change
	 *
	 * @since 1.0
	 */
	public function changed_password() {

		$this->api_track_event( $this->event_name['changed_password'] );
	}


	/**
	 * Track successful coupon apply
	 *
	 * @since 1.0
	 * @param string $coupon_code
	 */
	public function applied_coupon( $coupon_code ) {

		$this->api_track_event( $this->event_name['applied_coupon'], array( $this->property_name['coupon_code'] => $coupon_code ) );
	}


	/**
	 * Track order track
	 *
	 * @since 1.0
	 * @param int $order_id
	 */
	public function tracked_order( $order_id ) {

		$order = wc_get_order( $order_id );

		$this->api_track_event( $this->event_name['tracked_order'], array( $this->property_name['order_id'] => $order->get_order_number() ) );
	}


	/**
	 * Track shipping estimate on cart page
	 *
	 * @since 1.0
	 */
	public function estimated_shipping() {

		$this->api_track_event( $this->event_name['estimated_shipping'] );
	}


	/**
	 * Track order cancel from My Account area
	 *
	 * @since 1.0
	 * @param int $order_id
	 */
	public function cancelled_order( $order_id ) {

		$order = wc_get_order( $order_id );

		$this->api_track_event( $this->event_name['cancelled_order'], array( $this->property_name['order_id'] => $order->get_order_number() ) );
	}


	/**
	 * Track re-order from My Account area
	 *
	 * @since  1.0
	 * @param int $order_id
	 */
	public function reordered( $order_id ) {

		$order = wc_get_order( $order_id );

		$this->api_track_event( $this->event_name['reordered'], array( $this->property_name['order_id'] => $order->get_order_number() ) );
	}


	/**
	 * Tracks a custom event
	 *
	 * @since 1.0
	 * @param bool $event_name
	 * @param bool $properties
	 */
	public function custom_event( $event_name = false, $properties = false ) {

		if ( ! empty( $event_name ) ) {

			$this->js_track_event( $event_name, $properties );
		}
	}


	/**
	 * Tracks custom user properties via the API
	 *
	 * @since 1.6.0
	 * @param array|bool $properties
	 * @param string|null $user_id The user ID
	 */
	public function custom_user_properties_api( $properties = false, $user_id = null ) {

		if ( ! empty( $properties ) ) {

			$identity = $user_id ? $this->get_name_tag( $user_id ) : null;

			$this->api_register_property( $properties, $identity );
		}
	}


	/**
	 * Output tracking javascript in <head>
	 *
	 * @since 1.0
	 */
	public function output_head() {

		// verify tracking status
		if ( $this->disable_tracking() ) {
			return;
		}

		// no indentation for script
		?>
<!-- start WooCommerce Mixpanel -->
<script type="text/javascript">(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"===e.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f);b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2}})(document,window.mixpanel||[]);
	mixpanel.init("<?php echo esc_js( $this->token); ?>");
<?php

// when user is logged in, identify user and set their properties on every page load
if ( is_user_logged_in() ) {

	$user = wp_get_current_user();

	if ( ! get_user_meta( $user->ID, 'wc_mixpanel_user_aliased', true ) && strtotime( $user->user_registered ) >= 1422230400 ) { // 1422230400 is the time stamp representing the date this change was introduced (January 26, 2015)

		printf( 'mixpanel.alias("%s");', $this->get_name_tag( $user ) );
		update_user_meta( $user->ID, 'wc_mixpanel_user_aliased', true );
	}

	printf( 'mixpanel.identify("%1$s");mixpanel.name_tag("%1$s");mixpanel.people.set(%2$s);', $this->get_name_tag( $user ), json_encode( $this->get_user_properties( $user ) ) );
}

if ( is_front_page() && $this->event_name['viewed_homepage'] ) {
	printf( 'mixpanel.track("%s");', esc_html( $this->event_name['viewed_homepage'] ) );
}
?>
</script>
<!-- end WooCommerce Mixpanel -->
		<?php
	}


	/**
	 * Output event tracking javascript
	 *
	 * @since 1.0
	 * @param string $event_name Name of Event to be set
	 * @param array|string $properties Properties to be set with event.
	 */
	private function js_track_event( $event_name, $properties = '' ) {

		// Verify tracking status
		if ( $this->disable_tracking() ) {
			return;
		}

		// json encode properties if they exist
		if ( is_array( $properties ) ) {

			// remove blank properties
			if ( isset( $properties[''] ) ) {
				unset( $properties[''] );
			}

			$properties = ', ' . json_encode( $properties );
		}

		printf( '<script type="text/javascript">mixpanel.track( "%s"%s );</script>', esc_html( $event_name ), $properties );
	}


	/**
	 * Track event via HTTP API
	 *
	 * @since 1.0
	 * @param string $event_name Name of Event to be set
	 * @param array $properties Properties to be set with event
	 * @param string $identity Identity of visitor to track event for
	 */
	public function api_track_event( $event_name, $properties = array(), $identity = null ) {

		// Verify tracking status
		if ( $this->disable_tracking() ) {
			return;
		}

		// use passed identity if available, otherwise get distinct ID from mixpanel cookie
		if ( isset( $identity ) ) {
			$distinct_id = $identity;
		} else {
			$distinct_id =  $this->get_distinct_id();
		}

		if ( ! $distinct_id ) {

			// Distinct ID available, can't send data via API
			if ( 'errors' == $this->logging || 'queries_and_errors' == $this->logging ) {
				wc_mixpanel()->log( 'No distinct ID found! Cannot send event via API' );
			}

			return;
		}

		// identify visitor to track event for
		$this->get_api()->identify( $distinct_id, $this->get_name_tag() );

		// remove all blank properties
		if ( isset( $properties[''] ) ) {
			unset( $properties[''] );
		}

		// track the event
		$this->get_api()->track( $event_name, $properties );
	}


	/**
	 * Record 'people' properties for user via JS API
	 *
	 * @since 1.4
	 * @param array $properties Properties to be set on user
	 */
	private function js_register_property( $properties = array() ) {

		// Verify tracking status
		if ( $this->disable_tracking() ) {
			return;
		}

		if ( $properties ) {

			// remove blank properties
			if ( isset( $properties[''] ) ) {
				unset( $properties[''] );
			}

			$properties = json_encode( $properties );

			echo '<script type="text/javascript">' . "mixpanel.people.set({$properties});" .'</script>';
		}
	}


	/**
	 * Record 'people' properties for user via HTTP API
	 *
	 * @since 1.0
	 * @param array $properties Properties to be set on user
	 * @param string $identity Identity of visitor to track event for
	 */
	public function api_register_property( $properties = array(), $identity = null ) {

		// Verify tracking status
		if ( $this->disable_tracking() ) {
			return;
		}

		// use passed identity if available, otherwise get distinct ID from mixpanel cookie
		if ( isset( $identity ) ) {
			$distinct_id = $identity;
		} else {
			$distinct_id = $this->get_distinct_id();
		}

		if ( ! $distinct_id ) {

			// Distinct ID in cookie not found, can't send data via API
			// Cookies are probably disabled for visitor
			if ( 'errors' == $this->logging || 'queries_and_errors' == $this->logging ) {
				wc_mixpanel()->log( 'No distinct ID found! Cannot register property via API' );
			}

			return;
		}

		$this->get_api()->identify( $distinct_id, $this->get_name_tag() );

		if ( $properties ) {

			// remove empty properties
			foreach ( $properties as $property_name => $property_value ) {

				if ( ! $property_value ) {
					unset( $properties[ $property_name ] );
				}
			}

			$this->get_api()->register( 'set', $properties );
		}
	}


	/**
	 * Record 'people' revenue
	 *
	 * @since 1.8.0
	 * @param \WC_Order order object
	 * @param string $identity Identity of visitor to track event for
	 */
	public function api_track_revenue( $order, $identity = null ) {

		// verify tracking status
		if ( $this->disable_tracking() ) {
			return;
		}

		// verify order
		if ( ! $order instanceof WC_Order ) {
			return;
		}

		// use passed identity if available, otherwise get distinct ID from mixpanel cookie
		if ( isset( $identity ) ) {
			$distinct_id = $identity;
		} else {
			$distinct_id = $this->get_distinct_id();
		}

		if ( ! $distinct_id ) {

			// Distinct ID in cookie not found, can't send data via API
			// Cookies are probably disabled for visitor
			if ( 'errors' == $this->logging || 'queries_and_errors' == $this->logging ) {
				wc_mixpanel()->log( 'No distinct ID found! Cannot record revenue via API' );
			}

			return;
		}

		// identify
		$this->get_api()->identify( $distinct_id, $this->get_name_tag() );

		// track revenue
		$this->get_api()->register( 'append', array(
			'$transactions' => array(
				'$amount' => $order->get_total(),
				'$time'   => SV_WC_Order_Compatibility::get_date_created( $order )->date( 'Y-m-d\TH:i:s' ),
			),
		) );
	}


	/**
	 * Lazy load MP API
	 *
	 * @since 1.1.1
	 * @return \WC_Mixpanel_API instance
	 */
	private function get_api() {

		if ( is_object( $this->api ) ) {
			return $this->api;
		}

		// Load Mixpanel API class
		require_once( wc_mixpanel()->get_plugin_path() . '/includes/class-wc-mixpanel-api.php' );

		// Init MP API
		return $this->api = new WC_Mixpanel_API( $this->token, $this->api_options );
	}


	/**
	 * Disable tracking if admin, privileged user, or API key is blank
	 *
	 * @since 1.0
	 * @return bool Whether tracking should be disabled
	 */
	public function disable_tracking() {

		$disable = false;

		// don't disable tracking for ajax requests
		if ( is_admin() && ! is_ajax() ) {

			// disable tracking if admin, shop manager, or API key is blank
			if ( ! $this->token || current_user_can( 'manage_options' ) ) {

				$disable = true;
			}
		}

		/**
		 * Disable Mixpanel tracking
		 *
		 * @since 1.6.1
		 * @deprecated since 1.9.3
		 * @param bool $disable Whether tracking should be disabled
		 */
		$disable = (bool) apply_filters( 'wc_mixpanel_enable_tracking', $disable );

		/**
		 * Filter whether tracking is disabled.
		 *
		 * @since 1.9.3
		 * @param bool $disable whether tracking should be disabled
		 */
		return (bool) apply_filters( 'wc_mixpanel_disable_tracking', $disable );
	}


	/**
	 * Get distinct ID from MP cookie
	 *
	 * @since 1.0
	 * @return string distinct ID
	 */
	private function get_distinct_id() {

		// Mixpanel randomizes (maybe?) the name of it's tracking cookie, so find the name of the cookie
		// by searching the keys of the cookie array and extracting the first value (there should be only one...highlander!)

		$cookies = preg_grep( '/mp[^*]+mixpanel/', array_keys( $_COOKIE ) );
		$cookie_name = array_shift( $cookies );

		if ( isset( $_COOKIE[ $cookie_name ] ) ) {

			$cookie = json_decode( str_replace( '\\', '', urldecode( $_COOKIE[ $cookie_name ] ) ) );
			return $cookie->distinct_id;

		} else {

			return '';
		}
	}


	/**
	 * Get named identity of user
	 *
	 * @since 1.0
	 * @param mixed $user
	 * @return string visitor email or username
	 */
	public function get_name_tag( $user = null ) {

		// WP_User or user_id
		if ( isset ( $user ) ) {

			// instantiate new user if not WP_User object
			if ( ! is_object( $user ) ) {
				$user = new WP_User( $user );
			}

			return ( $this->nametag_pref == 'email' ? $user->user_email : $user->user_login );
		}

		// user is logged in
		if ( is_user_logged_in() ) {

			$user = get_user_by( 'id', get_current_user_id() );
			return ( $this->nametag_pref == 'email' ? $user->user_email : $user->user_login );

		} else {

			//nothing to identify on
			return '';
		}
	}


	/**
	 * Returns user properties to send to Mixpanel.
	 *
	 * @since 1.11.0
	 * @param \WP_User $user the user to return properties for
	 * @return string[] associative array of user properties
	 */
	private function get_user_properties( $user ) {
		return array(
			'$first_name' => $user->first_name,
			'$last_name'  => $user->last_name,
			'$email'      => $user->user_email,
			'$created'    => date( 'Y-m-d\TH:i:s', strtotime( $user->user_registered ) ),
			'Username'    => $user->user_login,
			'$ip'         => $this->get_client_ip(),
		);
	}


	/**
	 * Checks HTTP referer to see if request was a page reload
	 * Prevents duplication of tracking events when user reloads page or submits a form
	 * e.g applying a coupon on the cart page
	 *
	 * @since 1.0
	 * @return bool TRUE if not a page reload, FALSE if page reload
	 */
	private function not_page_reload() {

		if ( isset( $_SERVER['HTTP_REFERER'] ) ) {
			//return portion before query string
			$request_uri = str_replace( strstr( $_SERVER['REQUEST_URI'], '?' ), '', $_SERVER['REQUEST_URI'] );

			if ( stripos( $_SERVER['HTTP_REFERER'], $request_uri ) === false ) return true;
		}

		return false;
	}


	/**
	 * Returns the visitor's IP
	 *
	 * @since 1.4.1
	 * @return string client IP
	 */
	private function get_client_ip() {

		return isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
	}


	/**
	 * Return true if the event name is not empty, indicating that it should
	 * be tracked
	 *
	 * @since 1.6.1
	 * @param string $event_name
	 * @return bool
	 */
	public function has_event( $event_name ) {

		return ! empty( $this->event_name[ $event_name ] );
	}


	/**
	 * Helper to get a property name
	 *
	 * @since 1.6.1
	 * @param string $property_name
	 * @return string
	 */
	public function get_property_name( $property_name ) {

		return $this->property_name[ $property_name ];
	}


	/** Admin Functions */


	/**
	 * Initializes form fields in the format required by WC_Integration
	 *
	 * @since 1.0
	 */
	public function init_form_fields() {

		$settings = array(

			'api_settings_section' => array(
				'title'       => __( 'API Settings', 'woocommerce-mixpanel' ),
				'description' => __( 'Enter your project token to start tracking.', 'woocommerce-mixpanel' ),
				'type'        => 'section',
				'default'     => ''
			),

			'token' => array(
				'title'       => __( 'Token', 'woocommerce-mixpanel' ),
				'description' => __( 'Log into your account and go to Update Project settings. Leave blank to disable tracking.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => ''
			),

			'nametag_pref' => array(
				'title'       => __( 'Nametag Preference', 'woocommerce-mixpanel' ),
				'description' => __( 'Select how to identify logged in users in the Stream View.', 'woocommerce-mixpanel' ),
				'type'        => 'select',
				'default'     => '',
				'options'     => array(
					'email'    => __( 'Email Address', 'woocommerce-mixpanel' ),
					'username' => __( 'WordPress Username', 'woocommerce-mixpanel' )
				)
			),

			'logging' => array(
				'title'       => __( 'Logging', 'woocommerce-mixpanel' ),
				'description' => __( 'Select whether to log nothing, queries, errors, or both queries and errors to the WooCommerce log. Careful, this can fill up log files very quickly on a busy site.', 'woocommerce-mixpanel' ),
				'type'        => 'select',
				'default'     => '',
				'options'     => array(
					'off'                => __( 'Off', 'woocommerce-mixpanel' ),
					'queries'            => __( 'Queries', 'woocommerce-mixpanel' ),
					'errors'             => __( 'Errors', 'woocommerce-mixpanel' ),
					'queries_and_errors' => __( 'Queries & Errors', 'woocommerce-mixpanel' )
				)
			),

			'geocode_ips' => array(
				'title'   => __( 'Geocode IPs', 'woocommerce-mixpanel' ),
				'label'   => __( 'Allow Mixpanel to geolocate users.', 'woocommerce-mixpanel' ),
				'type'    => 'checkbox',
				'default' => 'yes'
			),

			'event_names_section' => array(
				'title'       => __( 'Event Names', 'woocommerce-mixpanel' ),
				'description' => __( 'Customize the event names. Leave a field blank to disable tracking of that event.', 'woocommerce-mixpanel' ),
				'type'        => 'section',
				'default'     => ''
			),

			'signed_in_event_name' => array(
				'title'       => __( 'Signed In', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer signs in.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Signed In'
			),

			'signed_out_event_name' => array(
				'title'       => __( 'Signed Out', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer signs out.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Signed Out'
			),

			'viewed_signup_event_name' => array(
				'title'       => __( 'Viewed Signup', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer views the registration form.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Viewed Signup'
			),

			'signed_up_event_name' => array(
				'title'       => __( 'Signed Up', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer registers a new account.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Signed Up'
			),

			'viewed_homepage_event_name' => array(
				'title'       => __( 'Viewed Homepage', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer views the homepage.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Viewed Homepage'
			),

			'viewed_product_event_name' => array(
				'title'       => __( 'Viewed Product', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer views a single product', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Viewed Product'
			),

			'added_to_cart_event_name' => array(
				'title'       => __( 'Added to Cart', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer adds an item to the cart.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Added to Cart'
			),

			'removed_from_cart_event_name' => array(
				'title'       => __( 'Removed from Cart', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer removes an item from the cart.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Removed from Cart'
			),

			'changed_cart_quantity_event_name' => array(
				'title'       => __( 'Changed Cart Quantity', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer changes the quantity of an item in the cart.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Changed Cart Quantity'
			),

			'viewed_cart_event_name' => array(
				'title'       => __( 'Viewed Cart', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer views the cart.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Viewed Cart'
			),

			'applied_coupon_event_name' => array(
				'title'       => __( 'Applied Coupon', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer applies a coupon', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Applied Coupon'
			),

			'started_checkout_event_name' => array(
				'title'       => __( 'Started Checkout', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer starts the checkout.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Started Checkout'
			),

			'started_payment_event_name' => array(
				'title'       => __( 'Started Payment', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer views the payment page (used with direct post payment gateways)', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Started Payment'
			),

			'completed_purchase_event_name' => array(
				'title'       => __( 'Completed Purchase', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer completes a purchase.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Completed Purchase'
			),

			'completed_payment_event_name' => array(
				'title'       => __( 'Completed Payment', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer completes payment for their purchase.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Completed Payment'
			),

			'wrote_review_event_name' => array(
				'title'       => __( 'Wrote Review', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer writes a review.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Wrote Review'
			),

			'commented_event_name' => array(
				'title'       => __( 'Commented', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer write a comment.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Commented'
			),

			'viewed_account_event_name' => array(
				'title'       => __( 'Viewed Account', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer views the My Account page.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Viewed Account'
			),

			'viewed_order_event_name' => array(
				'title'       => __( 'Viewed Order', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer views an order', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Viewed Order'
			),

			'updated_address_event_name' => array(
				'title'       => __( 'Updated Address', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer updates their address.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Updated Address'
			),

			'changed_password_event_name' => array(
				'title'       => __( 'Changed Password', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer changes their password.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Changed Password'
			),

			'estimated_shipping_event_name' => array(
				'title'       => __( 'Estimated Shipping', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer estimates shipping.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Estimated Shipping'
			),

			'tracked_order_event_name' => array(
				'title'       => __( 'Tracked Order', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer tracks an order.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Tracked Order'
			),

			'cancelled_order_event_name' => array(
				'title'       => __( 'Cancelled Order', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer cancels an order.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Cancelled Order'
			),

			'reordered_event_name' => array(
				'title'       => __( 'Reordered', 'woocommerce-mixpanel' ),
				'description' => __( 'Triggered when a customer reorders a previous order.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Reordered'
			),

			'property_names_section'  => array(
				'title'       => __( 'Property Names', 'woocommerce-mixpanel' ),
				'description' => __( 'Customize the property names. Leave a field blank to disable tracking of that property.', 'woocommerce-mixpanel' ),
				'type'        => 'section',
				'default'     => ''
			),

			'product_name_property_name' => array(
				'title'       => __( 'Product Name', 'woocommerce-mixpanel' ),
				'description' => __( 'Tracked when a customer views a product, adds / removes / changes quantities in the cart, or writes a review.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Product Name'
			),

			'product_price_property_name' => array(
				'title'       => __( 'Product Price', 'woocommerce-mixpanel' ),
				'description' => __( 'Tracked when a customer adds a product to the cart.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Product Price'
			),

			'quantity_property_name' => array(
				'title'       => __( 'Product Quantity', 'woocommerce-mixpanel' ),
				'description' => __( 'Tracked when a customer adds a product to their cart or changes the quantity in their cart.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Quantity'
			),

			'category_property_name' => array(
				'title'       => __( 'Product Category', 'woocommerce-mixpanel' ),
				'description' => __( 'Tracked when a customer adds a product to their cart.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Category'
			),

			'coupon_code_property_name' => array(
				'title'       => __( 'Coupon Code', 'woocommerce-mixpanel' ),
				'description' => __( 'Tracked when a customer applies a coupon.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Coupon Code'
			),

			'order_id_property_name' => array(
				'title'       => __( 'Order ID', 'woocommerce-mixpanel' ),
				'description' => __( 'Tracked when a customer completes their purchase.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Order ID'
			),

			'order_total_property_name' => array(
				'title'       => __( 'Order Total', 'woocommerce-mixpanel' ),
				'description' => __( 'Tracked when a customer completes their purchase.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Order Total'
			),

			'shipping_total_property_name' => array(
				'title'       => __( 'Shipping Total', 'woocommerce-mixpanel' ),
				'description' => __( 'Tracked when a customer completes their purchase.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Shipping Total'
			),

			'total_quantity_property_name' => array(
				'title'       => __( 'Total Quantity', 'woocommerce-mixpanel' ),
				'description' => __( 'Tracked when a customer completes their purchase.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Total Quantity'
			),

			'payment_method_property_name' => array(
				'title'       => __( 'Payment Method', 'woocommerce-mixpanel' ),
				'description' => __( 'Tracked when a customer completes their purchase.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Payment Method'
			),

			'post_title_property_name' => array(
				'title'       => __( 'Post Title', 'woocommerce-mixpanel' ),
				'description' => __( 'Tracked when a customer leaves a comment on a post.', 'woocommerce-mixpanel' ),
				'type'        => 'text',
				'default'     => 'Post Title'
			)
		);

		/**
		 * Mixpanel Settings Filter.
		 *
		 * Filter the settings for Mixpanel
		 *
		 * @since 1.6.1
		 * @param array $settings settings fields
		 * @param \WC_Mixpanel_Integration $this instance
		 */
		return $this->form_fields = apply_filters( 'wc_mixpanel_settings', $settings, $this );
	}


	/**
	 * Generate Section HTML so we can divide the settings page up into sections
	 *
	 * @since 1.0
	 * @param string $key
	 * @param string $data
	 * @return string section HTML
	 */
	public function generate_section_html( $key, $data ) {
		$html = '';

		if ( isset( $data['title'] ) && $data['title'] != '' ) $title = $data['title']; else $title = '';
		$data['class'] = ( isset( $data['class'] ) ) ? $data['class'] : '';
		$data['css']   = ( isset( $data['css'] ) ) ? $data['css'] : '';

		$html .= '<tr valign="top">' . "\n";
		$html .= '<th scope="row" colspan="2">';
		$html .= '<h3 style="margin:0;">' . $data['title'] . '</h3>';
		if ( $data['description'] ) $html .= '<p>' . $data['description'] . '</p>';
		$html .= '</th>' . "\n";
		$html .= '</tr>' . "\n";

		return $html;
	}


	/**
	 * Filter admin options before saving to remove section fields so they are
	 * not saved in the database
	 *
	 * @since 1.9.0
	 * @param array $sanitized_fields
	 * @return array
	 */
	public function filter_admin_options( $sanitized_fields ) {


		// remove our section 'field' so it doesn't get saved to the database
		foreach ( $this->form_fields as $id => $data ) {

			if ( isset( $data['type'] ) && $data['type'] == 'section' ) {

				unset( $sanitized_fields[ $id ] );
			}
		}

		return $sanitized_fields;
	}


}
