module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			woomsrpjs: {
				files: {
			        'js/frontend-legacy.min.js': ['js/frontend-legacy.js'],
			        'js/frontend.min.js':        ['js/frontend.js'],
			        'js/admin.min.js':           ['js/admin.js'],
			    }
			}
		},
		watch: {
			js: {
				files: [ 'js/frontend.js', 'js/frontend-legacy.js', 'js/admin.js' ],
				tasks: [ 'uglify' ],
			},
		},
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task(s).
	grunt.registerTask('default', ['uglify']);

};
