<?php
/**
 * WooCommerce 2Checkout Gateway
 * By Niklas Högefjord (niklas@krokedil.se)
 * Based on PayPal Standard Gateway by WooCommerce
 * 
 * Uninstall - removes all 2CO options from DB when user deletes the plugin via WordPress backend.
 * @since 0.3
 **/
 
if ( !defined('WP_UNINSTALL_PLUGIN') ) {
    exit();
}
	delete_option( 'woocommerce_2Checkout_settings' );		
?>