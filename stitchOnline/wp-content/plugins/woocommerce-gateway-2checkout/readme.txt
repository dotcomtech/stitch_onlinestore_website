=== WOOCOMMERCE 2CHECKOUT GATEWAY ===
By Niklas Högefjord - http://krokedil.com/
Based on PayPal Standard Gateway by WooCommerce



== DESCRIPTION ==
2Checkout Gateway is a plugin that extends WooCommerce, allowing you to take payments via 2Checkout.

2Checkout’s international payment services are available to merchants and consumers in over 200 countries. 2CO lets customers make purchases in 15 languages and 30 currencies.

When the order goes through, the user is taken to 2CO to make a secure payment. No SSL certificate is required on your site. After payment the order is confirmed and the user is taken to the thank you page.

The plugin utilizes the 2CO Pass-Through-Products Parameters that enables you to use the gateway without having to create the products in the 2CO sellers area.

2CO is a committed partner to protecting you, your business and your customers from fraudulent activities. 2CO uses statistical algorithms and data collection techniques to assess more than 300 variables in under 3 seconds to identify markers of fraud.



== IMPORTANT NOTE ==
This plugin extends WooCommerce with a 2Checkout payment gateway. The plugin will only work if WooCommerce is activated.

Testing
You can test the 2Checkout gateway payment process by enable the 2Checkout Test Mode. In Test Mode your credit card will not be charged. 


Return process
This plugin uses 2Checkout's x_receipt_link_url parameter to send the customer back to your site after payment is completed. For the return process to work properly the domain of your webshop must match the domain registered to the 2Checkout account.

In your 2Checkout account you should use the default setting for DIRECT RETURN (e.g: After completing an order, buyers should be: GIVEN LINKS BACK TO MY WEBSITE). You find this by browsing to --> Account --> Site Management.

By default the order is marked as Pending in WooCommerce until the customer clicks the button "Click here to finalize order" after payment completed in 2Checkout. If the customer don't click the button and go back to the WooCommerce Thank You Page, the order doesn't get marked as processing. To avoid this go to your 2Checkout account and enter the URL to your Thank You Page in --> Notifications --> Order Created.


Currency
The currency specified in WooCommerce must match the currency that has been specified in your 2Checkout account. Otherwise, the customer will be charged with the wrong amount.


Shipping and purchase routines
If you offer free shipping for tangible (i.e. physical) products, you need to add a New Shipping Method in your 2CO account. Under Shipping (in your 2CO account) you can add a New Shipping Method. Set the pricing to Free and then choose which countries this method should apply to.

If you sell physical products but still want to use the 2Checkout Single Page Checkout (or if you offer free shipping and don't want to set up the Free Shipping Method in your 2CO account) you can activate the "Send Order Total as one item" checkbox in the gateway settings. This feature does just what it sounds like, it collects the Order Total and send it to 2Checkout as one intangible product (i.e. non physical).



== PURCHASE ROUTINES ==
Standard Purchase Routine (purchase)
When using the Standard Purchase Routine, the customer will be required to navigate through several pages and fill in the requested billing and shipping information (if applicable) to complete their order. The Standard Purchase Routine allows the customer to pay with credit card & via Paypal.

Single Page Checkout (spurchase)
This purchase routine will list all of the customer’s cart information and billing information fields on a single page. The Single Page Checkout only allows credit cards. The Single Page Checkout is only used if the basket only contains intangible products (products set as Downloadable or Virtual Product Types in WooCommerce). If the basket contains tangible products or shipping cost, 2Checkout will automatically redirect the customer to the Standard Purchase Routine.



== INSTALLATION	 ==
1. Download and unzip the latest release zip file.
2. If you use the WordPress plugin uploader to install this plugin skip to step 4.
3. Upload the entire plugin directory to your /wp-content/plugins/ directory.
4. Activate the plugin through the 'Plugins' menu in WordPress Administration.
5. Go WooCommerce Settings --> Payment Gateways and configure your 2Checkout settings.
6. Go to your account at 2checkout.com and enter the URL to your WooCommerce Thank You Page, under --> Notifications --> Order Created.
7. Ensure that the domain of your WooCommerce installation match the domain registered to the 2Checkout account.
8. Ensure that the currency specified in WooCommerce match the currency that has been specified in your 2Checkout account. Otherwise, the customer will be charged with the wrong amount.