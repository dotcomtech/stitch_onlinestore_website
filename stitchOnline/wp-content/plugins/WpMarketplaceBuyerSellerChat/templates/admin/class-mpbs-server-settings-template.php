<?php

/**
 * @author Webkul
 * @version 2.0.0
 * This file handles server settings template at admin end.
 */

namespace WpMarketplaceBuyerSellerChat\Templates\Admin;

use WpMarketplaceBuyerSellerChat\Helper;

if (!defined('ABSPATH')) {
    exit;
}

if (! class_exists('Mpbs_Server_Settings_Template')) {
    /**
     *
     */
    class Mpbs_Server_Settings_Template
    {
        function mpbs_server_settings_template()
        {
            $helper = new Helper\Mpbs_Data();

            ?>
            <div class="wrap" id="mpbs-admin-config">

                <h1 class="wp-heading-inline"><?php echo __('Server Settings', 'mp_buyer_seller_chat'); ?></h1>

                <?php if (! $helper->mpbs_is_server_running()) : ?>
                    <a href="JavaScript:void(0);" class="page-title-action" id="mpbs-start-stop-server" data-action="start"><?php echo __('Start', 'mp_buyer_seller_chat'); ?></a>
                <?php else : ?>
                    <a href="JavaScript:void(0);" class="page-title-action" id="mpbs-start-stop-server" data-action="stop"><?php echo __('Stop', 'mp_buyer_seller_chat'); ?></a>
                <?php endif; ?>

                <div class="notice notice-warning is-dismissible">
                  <p><?php echo __( 'SHARED ON WPLOCKER.COM', 'MP_Currency' ); ?></p>
                </div>

                <form method="post" action="options.php">
                  <?php settings_fields('mpbs-settings-group'); ?>
                  <?php do_settings_sections('mpbs-settings-group'); ?>

                  <table class="form-table">
                    <tbody>

                      <tr valign="top">
                        <th scope="row" class="titledesc">
                          <label for="mpbs_host_name"><?php echo __('Host Name', 'mp_buyer_seller_chat'); ?></label>
                        </th>

                        <td class="forminp forminp-text">
                          <input type="text" id="mpbs_host_name" name="mpbs_host_name" value="<?php echo get_option('mpbs_host_name'); ?>" style="min-width: 350px;" />
                        </td>
                      </tr>

                      <tr valign="top">
                        <th scope="row" class="titledesc">
                          <label for="mpbs_port_num"><?php echo __('Port Number', 'mp_buyer_seller_chat'); ?></label>
                        </th>

                        <td class="forminp forminp-text">
                          <input type="text" id="mpbs_port_num" name="mpbs_port_number" value="<?php echo get_option('mpbs_port_number'); ?>" style="min-width: 350px;" />
                        </td>
                      </tr>

                      <tr valign="top">
                        <th scope="row" class="titledesc">
                          <label for="mpbs_chat_name"><?php echo __('Chat Name', 'mp_buyer_seller_chat'); ?></label>
                        </th>

                        <td class="forminp forminp-text">
                          <input type="text" id="mpbs_chat_name" name="mpbs_chat_name" value="<?php echo get_option('mpbs_chat_name'); ?>" style="min-width: 350px;" />
                          <p class="description"><?php echo __('The name which will display to all users in front-end.', 'mp_buyer_seller_chat'); ?></p>
                        </td>
                      </tr>

                      <!-- <tr valign="top">
                        <th scope="row" class="titledesc">
                          <label for="mpbs_https_enabled"><?php echo __('HTTPS Enabled', 'mp_buyer_seller_chat'); ?></label>
                        </th>

                        <td class="forminp forminp-text">
                          <select id="mpbs_https_enabled" name="mpbs_https_enabled" style="min-width: 350px;" >
                            <option value="">--Select--</option>
                            <option value="yes"><?php echo __('Yes', 'mp_buyer_seller_chat'); ?></option>
                            <option value="no"><?php echo __('No', 'mp_buyer_seller_chat'); ?></option>
                          </select>
                        </td>
                      </tr> -->

                    </tbody>
                  </table>

                  <?php submit_button(); ?>
                </form>

                <div id="responsedialog"><p></p></div>
            </div>
            <?php
        }
    }
}
