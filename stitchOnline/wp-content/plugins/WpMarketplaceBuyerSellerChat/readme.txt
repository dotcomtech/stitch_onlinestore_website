=== Woocommerce Marketplace Buyer Seller Chat ===

Contributors: Webkul
Tags: form, database, db, data, value, submit
WordPress :
  * Requires at least: 4.4
  * Tested up to: 4.9.0
WooCommerce: 3.2.x
License: GNU/GPL for more info see license.txt included with plugin
License URI: http://www.gnu.org/licenseses/gpl-2.0.html

WooCommerce Marketplace Buyer Seller Chat Plugin is a chat system which helps Marketplace Seller and the Marketplace Buyer to start conversation. In this plugin any buyer can start chat with any Seller.

== Description ==

WordPress WooCommerce Marketplace Buyer Seller Chat Plugin is a chat system which helps Marketplace Seller and the Marketplace Buyer to start the conversation. In this plugin, any buyer can start to chat with any seller. Buyer can ask queries related to the product that seller is selling or any upcoming products information, and seller can also reply to any buyer about their queries. A healthy conversation between a buyer and a seller leads to more sales conversions. It is a user-friendly and customizable, where the administrator can customize the theme of the chat window and users list.

== Installation ==

1. Upload `wp-marketplace-buyer-seller-chat` folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. You need to have Woocommerce and Marketplace installed in order to get this plugin working.

== Frequently Asked Questions ==

For any Query please generate a ticket at https://webkul.uvdesk.com/en/

= 2.0.0 =
* Updated using Socket.io for real time chat.
* Compatible with latest version of Marketplace.

= 1.1.0 =
* Fixed compatibility issues with new version of marketplace.

= 1.0.0 =
Current Version 2.0.0
