<?php

/**
 * @author Webkul
 * @version 2.0.0
 * This file handles admin settings interface.
 */

namespace WpMarketplaceBuyerSellerChat\Includes\Admin\Util;

if (!defined('ABSPATH')) {
    exit;
}

interface Admin_Settings_interface
{
    public function mpbs_init();
    public function mpbs_enqueue_scripts();
    /**
     * Add Menu under MP menu
     */
    public function mpbs_add_dashboard_menu();

    /**
     * Register Option Settings
     */
    public function mpbs_register_settings();
}
