<?php

/**
 * @author Webkul
 * @version 2.0.0
 * This file handles all admin end action callbacks.
 */

namespace WpMarketplaceBuyerSellerChat\Includes\Admin;

use WpMarketplaceBuyerSellerChat\Templates\Admin;
use WpMarketplaceBuyerSellerChat\Includes\Admin\Util;

if (!defined('ABSPATH')) {
    exit;
}

if (! class_exists('Mpbs_Function_Handler')) {
    /**
     *
     */
    class Mpbs_Function_Handler implements Util\Admin_Settings_Interface
    {
        /**
         * Add Menu under MP menu
         */
        public function mpbs_add_dashboard_menu()
        {
            $server_template = new Admin\Mpbs_Server_Settings_Template();
            add_submenu_page('products', __('Buyer Seller Chat', 'mp_buyer_seller_chat'), __('Buyer Seller Chat', 'mp_buyer_seller_chat'), 'manage_options', 'buyer-seller-chat', array($server_template, 'mpbs_server_settings_template'));
        }

        /**
         * Register Option Settings
         */
        public function mpbs_register_settings()
        {
            register_setting('mpbs-settings-group', 'mpbs_host_name');
            register_setting('mpbs-settings-group', 'mpbs_port_number');
            register_setting('mpbs-settings-group', 'mpbs_chat_name');
        }

        public function mpbs_init()
        {
            add_action('admin_enqueue_scripts', array($this, 'mpbs_enqueue_scripts'));
        }
        /**
        * Front scripts and style enqueue
        */
        public function mpbs_enqueue_scripts()
        {
            wp_enqueue_style('mpbs_admin_style', MPBS_URL . 'assets/css/style.css');
            wp_enqueue_style('ui-css', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
            wp_enqueue_script('ui-js', '//code.jquery.com/ui/1.12.1/jquery-ui.js');
            wp_enqueue_script('mpbs_admin_script', MPBS_URL . 'assets/js/admin.js', array( 'jquery' ));
            wp_localize_script('mpbs_admin_script', 'mpbs_admin_script_object', array(
              'ajaxurl' => admin_url('admin-ajax.php'),
              'admin_ajax_nonce' => wp_create_nonce('mpbs_admin_ajax_nonce')
            ));
        }

        /**
         * Start server ajax callback
         */
        function mpbs_start_server()
        {
            if (check_ajax_referer('mpbs_admin_ajax_nonce', 'nonce', false)) {
                $port = $_POST['port'];
                $node = exec('whereis node');
                $node_path = explode(' ', $node);
                if (!isset($node_path[1]) || $node_path[1] == '') {
                    $node = exec('whereis nodejs');
                    $node_path = explode(' ', $node);
                }

                if (count($node_path)) {
                    if (substr(php_uname(), 0, 7) == "Windows") {
                        pclose(popen("start /B PORT=" . $port . ' ' . $node_path[1] . ' ' . MPBS_FILE . 'assets/serverJs/server.js', "r"));
                    } else {
                        exec('PORT=' . $port . ' ' . $node_path[1] . ' ' . MPBS_FILE . 'assets/serverJs/server.js' . " > /dev/null &");
                    }
                    $response = array(
                      'error'   => false,
                      'status'  => __('Success', 'mp_buyer_seller_chat'),
                      'message' => __('Server has been started!', 'mp_buyer_seller_chat')
                    );
                } else {
                    $response = array(
                      'error'   => true,
                      'status'  => __('Error', 'mp_buyer_seller_chat'),
                      'message' => __('Nodejs Path not found.', 'mp_buyer_seller_chat')
                    );
                }
            } else {
                $response = array(
                  'error'   => true,
                  'status'  => __('Error', 'mp_buyer_seller_chat'),
                  'message' => __('Security check failed!', 'mp_buyer_seller_chat')
                );
            }

            echo json_encode($response);
            wp_die();
        }

        /**
         * Stop server ajax callback
         */
        function mpbs_stop_server()
        {
            if (check_ajax_referer('mpbs_admin_ajax_nonce', 'nonce', false)) {
                $get_user_path = exec('whereis fuser');
                $port = $_POST['port'];
                if ($get_user_path) {
                    $get_user_path = explode(' ', $get_user_path);
                    if (isset($get_user_path[1])) {
                        $stopServer = exec($get_user_path[1].' -k '.$port.'/tcp');
                        $response = array(
                          'error'   => false,
                          'status'  => __('Success', 'mp_buyer_seller_chat'),
                          'message' => __('Server has been stopped.', 'mp_buyer_seller_chat')
                        );
                    }
                } else {
                    $response = array(
                      'error'   => true,
                      'status'  => __('Error', 'mp_buyer_seller_chat'),
                      'message' => __('Something went wrong.', 'mp_buyer_seller_chat')
                    );
                }
            } else {
                $response = array(
                  'error'   => true,
                  'status'  => __('Error', 'mp_buyer_seller_chat'),
                  'message' => __('Security check failed!', 'mp_buyer_seller_chat')
                );
            }

            echo json_encode($response);
            wp_die();
        }
    }
}
