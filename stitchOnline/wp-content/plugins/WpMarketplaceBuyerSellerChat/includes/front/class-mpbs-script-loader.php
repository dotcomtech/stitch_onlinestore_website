<?php

/**
 * @author Webkul
 * @implements Assets_Interface
 */

namespace WpMarketplaceBuyerSellerChat\Includes\Front;

use WpMarketplaceBuyerSellerChat\Includes\Front\Util;
use WpMarketplaceBuyerSellerChat\Helper;

if (!defined('ABSPATH')) {
    exit;
}

if (! class_exists('Mpbs_Script_Loader')) {
    class Mpbs_Script_Loader implements Util\Assets_Interface
    {
        public function __construct()
        {
            $this->coreConfig = new Helper\Mpbs_Core_Config();
        }
        /**
        *
        */
        public function wkcInit()
        {
            add_action('wp_enqueue_scripts', array($this, 'wkcEnqueueScripts'));
        }
        /**
        * Front scripts and style enqueue
        */
        public function wkcEnqueueScripts()
        {
            wp_enqueue_media();

            wp_enqueue_style('mpbs_style', MPBS_URL . 'assets/css/style.css', '', MPBS_SCRIPT_VERSION);

            wp_enqueue_script('socket', MPBS_URL . 'assets/js/socket.io.js');

            wp_enqueue_script('mpbs_script', MPBS_URL . 'assets/js/plugin.js', array( 'jquery' ), MPBS_SCRIPT_VERSION);

            wp_localize_script('mpbs_script', 'chatboxCoreConfig', $this->coreConfig->mpbs_get_core_config());

            wp_localize_script('mpbs_script', 'enabledCustomerList', $this->coreConfig->mpbs_get_enabled_customer_list());

            wp_localize_script('mpbs_script', 'chatboxAjax', array(
                'url'   => admin_url('admin-ajax.php'),
                'nonce' => wp_create_nonce('mpbs-front-ajax-nonce')
            ));
        }
    }
}
