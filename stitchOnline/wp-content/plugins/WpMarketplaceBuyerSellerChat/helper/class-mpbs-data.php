<?php

/**
 * @author Webkul
 * @version 2.0.0
 * This file handles helper data class.
 */

namespace WpMarketplaceBuyerSellerChat\Helper;

if (!defined('ABSPATH')) {
    exit;
}

if (! class_exists('Mpbs_Data')) {
    /**
     *
     */
    class Mpbs_Data implements Util\Data_Interface
    {
        public function __construct()
        {
            $this->current_user = $this->mpbs_get_current_user_id();
        }

        /**
         * Return current user id
         * @return $user_id
         */
        public function mpbs_get_current_user_id()
        {
            return get_current_user_id();
        }

        /**
         * Get product author details
         * @param $product_id
         * @return array
         */
        function mpbs_get_product_seller_details($product_id)
        {
            $author_id = get_post_field('post_author', $product_id);
            $is_seller = $this->mpbs_author_is_seller($author_id);
            $return_data = array(
                'is_seller' => $is_seller,
                'seller_id' => $author_id
            );
            return $return_data;
        }

        /**
         * Check user is seller by user id
         * @param $user_id
         * @return boolean
         */
        function mpbs_author_is_seller($author_id)
        {
            global $wpdb;
            $seller_info = $wpdb->get_var("SELECT user_id FROM {$wpdb->prefix}mpsellerinfo WHERE user_id = {$author_id} and seller_value='seller'");
            if ($seller_info) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * Chat allowed or not
         * @return boolean
         */
        function mpbs_can_chat_enable($seller_id)
        {
            if (is_user_logged_in()) {
                $customer_id = get_current_user_id();
                if ($seller_id !== $customer_id) {
                    return true;
                }
                return false;
            } else {
                return true;
            }
        }

        /**
         * Get chatbox configuration product page
         * @return array $data
         */
        function mpbs_get_chatbox_config($seller_id)
        {
            $data = array(
                'customerData'   => $this->mpbs_get_customer_config($seller_id, $this->current_user),
                'sellerData'     => array(
                    'sellerId'       => $this->mpbs_get_user_unique_id($seller_id),
                    'chatStatus'     => $this->mpbs_get_user_status_by_id($seller_id),
                    'image'          => $this->mpbs_get_user_image_by_id($seller_id)
                )
            );
            return $data;
        }

        /**
         * Get customer config data if chat already started with seller
         * @param $seller_id
         */
        public function mpbs_get_customer_config($seller_id, $customer_id)
        {
            $chat_row = $this->mpbs_check_chat_started_bs($seller_id, $customer_id);

            if ($chat_row && is_user_logged_in()) {
                $data = array(
                  'customerId'         => $this->mpbs_get_user_unique_id($customer_id),
                  'isCustomerLoggedIn' => is_user_logged_in(),
                  'chatStatus'         => $this->mpbs_get_user_status_by_id($customer_id),
                  'name'               => $this->mpbs_get_user_name_by_id($customer_id),
                  'email'              => $this->mpbs_get_user_data($customer_id)->user_email,
                  'src'                => $this->mpbs_get_user_image_by_id($customer_id)
                );
            } else {
                $data = array();
            }

            return $data;
        }

        public function mpbs_get_customer_config_for_chatlist($seller_id, $customer_id)
        {
            $data = array(
              'customerId'         => $this->mpbs_get_user_unique_id($customer_id),
              'isCustomerLoggedIn' => is_user_logged_in(),
              'chatStatus'         => $this->mpbs_get_user_status_by_id($customer_id),
              'name'               => $this->mpbs_get_user_name_by_id($customer_id),
              'email'              => $this->mpbs_get_user_data($customer_id)->user_email,
              'src'                => $this->mpbs_get_user_image_by_id($customer_id)
            );

            return $data;
        }

        /**
         * Check chat started between user and seller
         * @param $buyer_id and $seller_id
         */
        public function mpbs_check_chat_started_bs($seller_id, $buyer_id)
        {
            global $wpdb;

            $query = $wpdb->prepare("SELECT chat_window from {$wpdb->prefix}user_table_meta where seller_id = '%d' and buyer_id = '%d'", $seller_id, $buyer_id);

            $chat_row = $wpdb->get_var($query);

            return intval($chat_row);
        }

        /**
         * Check server running
         * @return bool
         */
        public function mpbs_is_server_running()
        {
            $host = get_option('mpbs_host_name');
            $port = get_option('mpbs_port_number');

            $timeout = 2;

            $handle = curl_init($host.':'.$port);
            curl_setopt_array($handle, [
               CURLOPT_NOBODY          => true,
               CURLOPT_RETURNTRANSFER  => true,
               CURLOPT_TIMEOUT         => $timeout
            ]);
            curl_exec($handle);
            $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);
            return in_array($code, range(200, 399));
        }

        /**
         * Return MP seller page name
         * @return $page_name
         */
        public function mpbs_get_seller_page_name()
        {
            global $wpdb;
            $page_name_query = $wpdb->prepare("SELECT post_name FROM $wpdb->posts WHERE post_name ='%s'", get_option('wkmp_seller_page_title'));
            $page_name = $wpdb->get_var($page_name_query);
            return $page_name;
        }

        /**
         * Get seller chatbox configuration
         * @return $array
         */
        public function mpbs_get_seller_chatbox_config()
        {
            $data = array(
                'sellerChatData'         => array(
                    'sellerId'       => $this->mpbs_get_user_unique_id($this->current_user),
                    'image'          => $this->mpbs_get_user_image_by_id($this->current_user),
                    'chatStatus'     => $this->mpbs_get_user_status_by_id($this->current_user),
                    'sellerName'     => $this->mpbs_get_user_name_by_id($this->current_user)
                ),
                'chatEnabled'        => true
            );
            return $data;
        }

        /**
         * Get user data by id
         * @param $user_id
         * @return userdata object
         */
        public function mpbs_get_user_data($user_id)
        {
            return get_userdata($user_id);
        }

        /**
         * Get user status by id
         * @param $user_id
         * @return $status
         */
        public function mpbs_get_user_status_by_id($user_id)
        {
            global $wpdb;
            $query = $wpdb->prepare("SELECT status from {$wpdb->prefix}user_table where user_id = '%d'", $user_id);
            return $wpdb->get_var($query);
        }

        /**
         * Get user image by id
         * @param $user_id
         * @return $image_url
         */
        public function mpbs_get_user_image_by_id($user_id)
        {
            $dir = wp_upload_dir();
            $image = get_user_meta($user_id, 'mpbs_user_image', true);

            if ($image) {
              $image_path = $dir['baseurl'] . $image;
            } else {
               $image_path = 'http://0.gravatar.com/avatar/6d08c36207db514399f344acd45bf7bd?s=35&amp;d=mm&amp;r=g';
            }
            return $image_path;
        }

        /**
         * Get user name by id
         * @param $user_id
         * @return $name
         */
        public function mpbs_get_user_name_by_id($user_id)
        {
            return $this->mpbs_get_user_data($user_id)->last_name ? $this->mpbs_get_user_data($user_id)->first_name . ' ' . $this->mpbs_get_user_data($user_id)->last_name : $this->mpbs_get_user_data($user_id)->first_name;
        }

        /**
         * Initial chat list for seller
         */
        public function mpbs_get_initialised_chat_list()
        {
            global $wpdb;

            $customer_arr = array();
            $user_id = $this->mpbs_get_current_user_id();

            $query = $wpdb->prepare("SELECT buyer_id from {$wpdb->prefix}user_table_meta where seller_id = '%d'", $user_id);

            $customers = $wpdb->get_results($query);

            if ($customers) {
                foreach ($customers as $key => $value) {
                    $customer_arr[] = array(
                        'data'  => json_encode($this->mpbs_get_customer_config_for_chatlist($user_id, $value->buyer_id))
                    );
                }
            }
            return $customer_arr;
        }

        /**
         * Get user unique id
         */
        public function mpbs_get_user_unique_id($user_id)
        {
            $id = get_user_meta($user_id, 'mpbs_user_unique_id', true);
            if ($id) {
                return $id;
            } else {
                return $user_id;
            }
        }

        /**
         * Get user id by unique id
         */
        public function mpbs_get_userid_by_unique_id($unique_id)
        {
            global $wpdb;

            $query = $wpdb->prepare("SELECT user_id from {$wpdb->prefix}usermeta where meta_key = 'mpbs_user_unique_id' and meta_value = '%s'", $unique_id);

            $user_id = $wpdb->get_var($query);

            if ($user_id) {
                return $user_id;
            } else {
                return $unique_id;
            }
        }
    }

}
