<?php
/*
Plugin Name: WooCommerce Flat Rate Box Shipping
Plugin URI: https://woocommerce.com/products/flat-rate-box-shipping/
Description: Flat rate box shipping lets you define costs for boxes to different destinations. Items are packed into boxes based on item size and volume.
Version: 2.0.2
Author: WooCommerce
Author URI: https://woocommerce.com/
Requires at least: 4.0
Tested up to: 4.5

	Copyright: 2016 WooThemes.
	License: GNU General Public License v3.0
	License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), 'b28c4fbb609b15ef3f4a1a9db11b0a45', '182761' );

/**
 * Check if WooCommerce is active
 */
if ( is_woocommerce_active() ) {
	/**
	 * Main Class
	 */
	class WC_Flat_Rate_Box_Shipping {
		/**
		 * Constructor
		 */
		public function __construct() {
			define( 'BOX_SHIPPING_VERSION', '2.0.2' );
			define( 'BOX_SHIPPING_DEBUG', defined( 'WP_DEBUG' ) && 'true' == WP_DEBUG && ( ! defined( 'WP_DEBUG_DISPLAY' ) || 'true' == WP_DEBUG_DISPLAY ) );
			add_action( 'plugins_loaded', array( $this, 'init' ) );
			register_activation_hook( __FILE__, array( $this, 'install' ) );
		}

		/**
		 * Register method for usage
		 * @param  array $shipping_methods
		 * @return array
		 */
		public function woocommerce_shipping_methods( $shipping_methods ) {
			$shipping_methods['flat_rate_boxes'] = 'WC_Shipping_Flat_Rate_Boxes';
			return $shipping_methods;
		}

		/**
		 * Init TRS
		 */
		public function init() {
			include_once( 'includes/functions-ajax.php' );
			include_once( 'includes/functions-admin.php' );

			/**
			 * Install check (for updates)
			 */
			if ( get_option( 'box_shipping_version' ) < BOX_SHIPPING_VERSION ) {
				$this->install();
			}

			// 2.6.0+ supports zones and instances
			if ( version_compare( WC_VERSION, '2.6.0', '>=' ) ) {
				add_filter( 'woocommerce_shipping_methods', array( $this, 'woocommerce_shipping_methods' ) );
			} else {
				if ( ! defined( 'SHIPPING_ZONES_TEXTDOMAIN' ) ) {
					define( 'SHIPPING_ZONES_TEXTDOMAIN', 'woocommerce-shipping-flat-rate-boxes' );
				}
				if ( ! class_exists( 'WC_Shipping_zone' ) ) {
					include_once( 'includes/legacy/shipping-zones/class-wc-shipping-zones.php' );
				}
				add_action( 'woocommerce_load_shipping_methods', array( $this, 'load_shipping_methods' ) );
				add_action( 'admin_notices', array( $this, 'welcome_notice' ) );
			}

			// Hooks
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
			add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
			add_filter( 'plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 2 );
			add_action( 'woocommerce_shipping_init', array( $this, 'shipping_init' ) );
		}

		/**
		 * Localisation
		 */
		public function load_plugin_textdomain() {
			load_plugin_textdomain( 'woocommerce-shipping-flat-rate-boxes', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
		}

		/**
		 * Row meta
		 * @param  array $links
		 * @param  string $file
		 * @return array
		 */
		public function plugin_row_meta( $links, $file ) {
			if ( $file === plugin_basename( __FILE__ ) ) {
				$row_meta = array(
					'docs'    => '<a href="' . esc_url( apply_filters( 'woocommerce_flat_rate_boxes_shipping_docs_url', 'https://woocommerce.com/document/flat-rate-box-shipping/' ) ) . '" title="' . esc_attr( __( 'View Documentation', 'woocommerce-shipping-flat-rate-boxes' ) ) . '">' . __( 'Docs', 'woocommerce-shipping-flat-rate-boxes' ) . '</a>',
					'support' => '<a href="' . esc_url( apply_filters( 'woocommerce_flat_rate_boxes_support_url', 'https://support.woocommerce.com/' ) ) . '" title="' . esc_attr( __( 'Visit Premium Customer Support Forum', 'woocommerce-shipping-flat-rate-boxes' ) ) . '">' . __( 'Premium Support', 'woocommerce-shipping-flat-rate-boxes' ) . '</a>',
				);
				return array_merge( $links, $row_meta );
			}
			return (array) $links;
		}

		/**
		 * Admin welcome notice
		 */
		public function welcome_notice() {
			if ( get_option( 'hide_box_shipping_welcome_notice' ) ) {
				return;
			}
			wp_enqueue_style( 'woocommerce-activation', WC()->plugin_url() . '/assets/css/activation.css' );
			?>
			<div id="message" class="updated woocommerce-message wc-connect">
				<div class="squeezer">
					<h4><?php _e( '<strong>Flat Rate Box shipping is installed</strong> &#8211; Add some shipping zones to get started :)', 'woocommerce-shipping-flat-rate-boxes' ); ?></h4>
					<p class="submit"><a href="<?php echo admin_url('admin.php?page=shipping_zones'); ?>" class="button-primary"><?php _e( 'Setup Zones', 'woocommerce-shipping-flat-rate-boxes' ); ?></a> <a class="skip button-primary" href="https://docs.woocommerce.com/document/flat-rate-box-shipping/"><?php _e('Documentation', 'woocommerce-shipping-flat-rate-boxes'); ?></a></p>
				</div>
			</div>
			<?php
			update_option( 'hide_box_shipping_welcome_notice', 1 );
		}

		/**
		 * Admin styles + scripts
		 */
		public function admin_enqueue_scripts() {
			wp_enqueue_style( 'woocommerce_shipping_flat_rate_boxes_styles', plugins_url( '/assets/css/admin.css', __FILE__ ) );
			wp_register_script( 'woocommerce_shipping_flat_rate_box_rows', plugins_url( '/assets/js/flat-rate-box-rows.min.js', __FILE__ ), array( 'jquery', 'wp-util' ) );
			wp_localize_script( 'woocommerce_shipping_flat_rate_box_rows', 'woocommerce_shipping_flat_rate_box_rows', array(
				'i18n' => array(
					'delete_rates' => __( 'Delete the selected boxes?', 'woocommerce-table-rate-shipping' ),
				),
				'delete_box_nonce' => wp_create_nonce( "delete-box" ),
			) );
		}

		/**
		 * Load shipping class
		 */
		public function shipping_init() {
			include_once( 'includes/class-wc-shipping-flat-rate-boxes.php' );
		}

		/**
		 * Load shipping methods
		 */
		public function load_shipping_methods( $package ) {
			// Register the main class
			woocommerce_register_shipping_method( 'WC_Shipping_Flat_Rate_Boxes' );

			if ( ! $package ) {
				return;
			}

			// Get zone for package
			$zone = woocommerce_get_shipping_zone( $package );

			if ( BOX_SHIPPING_DEBUG ) {
				$notice_text = 'Customer matched shipping zone <strong>' . $zone->zone_name . '</strong> (#' . $zone->zone_id . ')';

				if ( ! wc_has_notice( $notice_text, 'notice' ) ) {
					wc_add_notice( $notice_text, 'notice' );
				}
			}

			if ( $zone->exists() ) {
				// Register zone methods
				$zone->register_shipping_methods();
			}
		}

		/**
		 * Installer
		 */
		public function install() {
			include_once( 'installer.php' );
			update_option( 'box_shipping_version', BOX_SHIPPING_VERSION );
		}
	}

	new WC_Flat_Rate_Box_Shipping();
}

/**
 * Callback function for loading an instance of this method
 *
 * @param mixed $instance
 * @param mixed $title
 * @return WC_Shipping_Flat_Rate_Boxes
 */
function woocommerce_get_shipping_method_flat_rate_boxes( $instance = false ) {
	return new WC_Shipping_Flat_Rate_Boxes( $instance );
}
