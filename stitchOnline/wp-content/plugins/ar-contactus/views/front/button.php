<style type="text/css">
    <?php if ($menuConfig->menu_bg){?>
        .arcontactus-widget .messangers-block{
            background-color: #<?php echo $menuConfig->menu_bg ?>;
        }
        .arcontactus-widget .messangers-block::before{
            border-top-color: #<?php echo $menuConfig->menu_bg ?>;
        }
    <?php } ?>
    <?php if ($menuConfig->menu_color){?>
        .messangers-block .messanger p{
            color:  #<?php echo $menuConfig->menu_color ?>;
        }
    <?php } ?>
    <?php if ($menuConfig->menu_hcolor){?>
        .messangers-block .messanger:hover p{
            color:  #<?php echo $menuConfig->menu_hcolor ?>;
        }
    <?php } ?>
    <?php if ($menuConfig->menu_hbg){?>
        .messangers-block .messanger:hover{
            background-color:  #<?php echo $menuConfig->menu_hbg ?>;
        }
    <?php } ?>
    #arcontactus-message-callback-phone-submit{
        font-weight: normal;
    }
    <?php if ($popupConfig->hide_recaptcha){?>
        .grecaptcha-badge{
            display: none;
        }
    <?php } ?>
    <?php if ($buttonConfig->x_offset){?>
        .arcontactus-widget.<?php echo $buttonConfig->position ?>.arcontactus-message{
            <?php if ($buttonConfig->position == 'left'){?>
                left: <?php echo (int)$buttonConfig->x_offset ?>px;
            <?php } ?>
            <?php if ($buttonConfig->position == 'right'){?>
                right: <?php echo (int)$buttonConfig->x_offset ?>px;
            <?php } ?>
        }
    <?php } ?>
    <?php if ($buttonConfig->y_offset){?>
        .arcontactus-widget.<?php echo $buttonConfig->position ?>.arcontactus-message{
            bottom: <?php echo (int)$buttonConfig->y_offset ?>px;
        }
    <?php } ?>
    .arcontactus-widget .arcontactus-message-button .pulsation{
        -webkit-animation-duration:<?php echo $buttonConfig->pulsate_speed / 1000 ?>s;
        animation-duration: <?php echo $buttonConfig->pulsate_speed / 1000 ?>s;
    }
</style>
<div id="arcontactus"></div>
<script>
    <?php if ($promptConfig->enable_prompt && $messages){?>
        var arCuMessages = <?php echo json_encode($messages) ?>;
        var arCuLoop = <?php echo $promptConfig->loop? 'true' : 'false' ?>;;
        var arCuCloseLastMessage = <?php echo $promptConfig->close_last? 'true' : 'false' ?>;
        var arCuPromptClosed = false;
        var _arCuTimeOut = null;
        var arCuDelayFirst = <?php echo (int)$promptConfig->first_delay ?>;
        var arCuTypingTime = <?php echo (int)$promptConfig->typing_time ?>;
        var arCuMessageTime = <?php echo (int)$promptConfig->message_time ?>;
        var arCuClosedCookie = 0;
    <?php } ?>
    window.addEventListener('load', function(){
        arCuClosedCookie = arCuGetCookie('arcu-closed');
        <?php if ($promptConfig->enable_prompt && $messages){ ?>
            jQuery('#arcontactus').on('arcontactus.init', function(){
                if (arCuClosedCookie){
                    return false;
                }
                arCuShowMessages();
            });
            jQuery('#arcontactus').on('arcontactus.openMenu', function(){
                clearTimeout(_arCuTimeOut);
                arCuPromptClosed = true;
                jQuery('#contact').contactUs('hidePrompt');
                arCuCreateCookie('arcu-closed', 1, 0);
            });

            jQuery('#arcontactus').on('arcontactus.hidePrompt', function(){
                clearTimeout(_arCuTimeOut);
                arCuPromptClosed = true;
                arCuCreateCookie('arcu-closed', 1, 0);
            });
        <?php } ?>
        var arcItems = [];
        <?php foreach ($items as $item){?>
            var arcItem = {};
            <?php if ($item['id']){?>
                arcItem.id = '<?php echo $item['id'] ?>';
            <?php } ?>
            <?php if ($item['js']){?>
                arcItem.onClick = function(e){
                    <?php echo stripslashes($item['js']) ?>
                }
            <?php } ?>
            arcItem.class = '<?php echo $item['class'] ?>';
            arcItem.title = '<?php echo $item['title'] ?>';
            arcItem.icon = '<?php echo $item['icon'] ?>';
            arcItem.href = '<?php echo $item['href'] ?>';
            arcItem.color = '<?php echo $item['color'] ?>';
            arcItems.push(arcItem);
        <?php } ?>
        jQuery('#arcontactus').contactUs({
            drag: <?php echo $buttonConfig->drag? 'true' : 'false' ?>,
            buttonIconUrl: '<?php echo AR_CONTACTUS_PLUGIN_URL . 'res/img/msg.svg' ?>',
            align: '<?php echo $buttonConfig->position ?>',
            reCaptcha: <?php echo $popupConfig->recaptcha? 'true' : 'false' ?>,
            reCaptchaKey: '<?php echo ArContactUsTools::escJsString($popupConfig->key) ?>',
            countdown: <?php echo (int)$popupConfig->timeout ?>,
            theme: '#<?php echo $buttonConfig->button_color ?>',
            <?php if ($buttonConfig->text){ ?>
                buttonText: '<?php echo ArContactUsTools::escJsString($buttonConfig->text) ?>',
            <?php }else{ ?>
                buttonText: false,
            <?php } ?>
            buttonSize: '<?php echo ArContactUsTools::escJsString($buttonConfig->button_size) ?>',
            menuSize: '<?php echo ArContactUsTools::escJsString($menuConfig->menu_size) ?>',
            phonePlaceholder: '<?php echo $popupConfig->phone_placeholder ?>',
            callbackSubmitText: '<?php echo ArContactUsTools::escJsString($popupConfig->btn_title) ?>',
            errorMessage: '<?php echo ArContactUsTools::escJsString($popupConfig->fail_message) ?>',
            callProcessText: '<?php echo ArContactUsTools::escJsString($popupConfig->proccess_message) ?>',
            callSuccessText: '<?php echo ArContactUsTools::escJsString($popupConfig->success_message) ?>',
            iconsAnimationSpeed: <?php echo (int)$buttonConfig->icon_speed ?>,
            callbackFormText: '<?php echo ArContactUsTools::escJsString($popupConfig->message) ?>',
            items: arcItems,
            ajaxUrl: arcontactusAjax.url,
            action: 'arcontactus_request_callback'
        });
    });
</script>