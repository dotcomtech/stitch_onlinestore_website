<table class="wp-list-table widefat fixed striped" id="arcontactus-menu-items">
    <thead>
        <tr>
            <th scope="col" class="manage-column column-name column-primary"><?php echo __('Position', AR_CONTACTUS_TEXT_DOMAIN) ?></th>
            <th scope="col" class="manage-column column-description"><?php echo __('Icon', AR_CONTACTUS_TEXT_DOMAIN) ?></th>
            <th scope="col" class="manage-column column-description"><?php echo __('Color', AR_CONTACTUS_TEXT_DOMAIN) ?></th>
            <th scope="col" class="manage-column column-description"><?php echo __('Title', AR_CONTACTUS_TEXT_DOMAIN) ?></th>
            <th scope="col" class="manage-column column-description"><?php echo __('Link', AR_CONTACTUS_TEXT_DOMAIN) ?></th>
            <th scope="col" class="manage-column column-description"><?php echo __('Active', AR_CONTACTUS_TEXT_DOMAIN) ?></th>
            <th scope="col" class="manage-column column-description"></th>
        </tr>
    </thead>

    <tbody id="the-list">
        <?php foreach ($items as $item){?>
        <tr data-id="<?php echo $item->id ?>">
            <td class="">
                <span class="drag-handle">
                    <i class="icon move"></i>
                    <span class="position">
                        <?php echo $item->position ?>
                    </span>
                </span>
            </td>
            <td>
                <span>
                    <?php echo ArContactUsConfigModel::getIcon($item->icon) ?>
                </span>
            </td>
            <td>
                <span class="lbl-color" style="background: #<?php echo $item->color ?>"><?php echo $item->color ?></span>
            </td>
            <td>
                <?php echo $item->title ?>
            </td>
            <td>
                <?php if ($item->link == 'callback'){?>
                    <?php echo __('Callback form', AR_CONTACTUS_TEXT_DOMAIN) ?>
                <?php }else{ ?>
                    <a href="<?php echo $item->link ?>" target="_blank">
                        <?php echo $item->link ?>
                    </a>
                <?php } ?>
            </td>
            <td>
                <a href="#" onclick="arCU.toggle(<?php echo $item->id ?>); return false;" class="<?php echo $item->status? 'lbl-success' : 'lbl-default' ?>">
                    <?php echo $item->status? __('Yes', AR_CONTACTUS_TEXT_DOMAIN) : __('No', AR_CONTACTUS_TEXT_DOMAIN) ?>
                </a>
            </td>
            <td>
                <a href="#" title="Edit" onclick="arCU.edit(<?php echo $item->id ?>); return false;" class="edit btn btn-default" data-id="<?php echo $item->id ?>">
                    <i class="icon-pencil"></i> <?php echo __('Edit', AR_CONTACTUS_TEXT_DOMAIN) ?>
                </a>

                <a href="#" title="Delete" onclick="arCU.remove(<?php echo $item->id ?>); return false;" data-id="<?php echo $item->id ?>" class="delete">
                    <i class="icon-trash"></i> <?php echo __('Delete', AR_CONTACTUS_TEXT_DOMAIN) ?>
                </a>
            </td>
        </tr>
        <?php } ?>
    </tbody>

    <tfoot>
        <tr>
            <th scope="col" class="manage-column column-name column-primary"><?php echo __('Position', AR_CONTACTUS_TEXT_DOMAIN) ?></th>
            <th scope="col" class="manage-column column-description"><?php echo __('Icon', AR_CONTACTUS_TEXT_DOMAIN) ?></th>
            <th scope="col" class="manage-column column-description"><?php echo __('Color', AR_CONTACTUS_TEXT_DOMAIN) ?></th>
            <th scope="col" class="manage-column column-description"><?php echo __('Title', AR_CONTACTUS_TEXT_DOMAIN) ?></th>
            <th scope="col" class="manage-column column-description"><?php echo __('Link', AR_CONTACTUS_TEXT_DOMAIN) ?></th>
            <th scope="col" class="manage-column column-description"><?php echo __('Active', AR_CONTACTUS_TEXT_DOMAIN) ?></th>
            <th scope="col" class="manage-column column-description"></th>
        </tr>
    </tfoot>
</table>