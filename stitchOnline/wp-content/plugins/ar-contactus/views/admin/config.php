<?php 
if (empty($activeSubmit) && (isset($_GET['paged']) || isset($_GET['orderby']) || isset($_GET['arcontactus_requests']))){
    $activeSubmit = 'arcontactus-requests';
}?>
<div id="arcontactus-plugin-container">
    <div class="arcontactus-masthead">
        <div class="arcontactus-masthead__inside-container">
            <div class="arcontactus-masthead__logo-container">
                <?php echo sprintf(__('All-in-one contact button %swith call-back request feature%s', AR_CONTACTUS_TEXT_DOMAIN), '<small>', '</small>') ?>
            </div>
        </div>
    </div>
    <div class="arcontactus-body">
        <?php if ($success){?>
            <div class="ui success message">
                <?php echo $success ?>
            </div>
        <?php } ?>
        <?php if ($errors){?>
            <?php foreach ($errors as $fieldErrors){?>
                <?php foreach ($fieldErrors as $error){?>
                    <div class="ui negative message">
                        <?php echo $error ?>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        <div class="ui grid">
            <div class="four wide column">
                <div class="ui vertical fluid pointing menu" id="acrontactus-menu">
                    <a class="item <?php echo ($activeSubmit == 'ArContactUsConfigGeneral' || empty($activeSubmit))? 'active' : '' ?>" data-target="#arcontactus-general">
                        <?php echo __('General configuration', AR_CONTACTUS_TEXT_DOMAIN) ?>
                    </a>
                    <a class="item <?php echo ($activeSubmit == 'ArContactUsConfigButton')? 'active' : '' ?>" data-target="#arcontactus-button">
                        <?php echo __('Button settings', AR_CONTACTUS_TEXT_DOMAIN) ?>
                    </a>
                    <a class="item <?php echo ($activeSubmit == 'ArContactUsConfigMenu')? 'active' : '' ?>" data-target="#arcontactus-menu">
                        <?php echo __('Menu settings', AR_CONTACTUS_TEXT_DOMAIN) ?>
                    </a>
                    <a class="item <?php echo ($activeSubmit == 'ArContactUsConfigPopup')? 'active' : '' ?>" data-target="#arcontactus-callback">
                        <?php echo __('Callback popup settings', AR_CONTACTUS_TEXT_DOMAIN) ?>
                    </a>
                    <a class="item <?php echo ($activeSubmit == 'ArContactUsConfigPrompt')? 'active' : '' ?>" data-target="#arcontactus-prompt">
                        <?php echo __('Prompt settings', AR_CONTACTUS_TEXT_DOMAIN) ?>
                    </a>
                    <a class="item" data-target="#arcontactus-prompt-items">
                        <?php echo __('Prompt messages', AR_CONTACTUS_TEXT_DOMAIN) ?>
                    </a>
                    <a class="item" data-target="#arcontactus-items">
                        <?php echo __('Menu items', AR_CONTACTUS_TEXT_DOMAIN) ?>
                    </a>
                    <a class="item <?php echo ($activeSubmit == 'arcontactus-requests')? 'active' : '' ?>" href="<?php echo admin_url('admin.php?page=ar-contactus-key-requests') ?>">
                        <?php echo __('Callback requests', AR_CONTACTUS_TEXT_DOMAIN) ?>
                    </a>
                    <a class="item" data-target="#arcontactus-about">
                        <?php echo __('About', AR_CONTACTUS_TEXT_DOMAIN) ?>
                    </a>
                </div>
            </div>
            <div class="twelve wide stretched column" id="arcontactus-tabs">
                <span class="hidden"></span>
                <?php echo ArContactUsAdmin::render('/admin/_general.php', array(
                    'generalConfig' => $generalConfig,
                    'activeSubmit' => $activeSubmit
                )) ?>
                <?php echo ArContactUsAdmin::render('/admin/_button.php', array(
                    'buttonConfig' => $buttonConfig,
                    'activeSubmit' => $activeSubmit
                )) ?>
                <?php echo ArContactUsAdmin::render('/admin/_menu.php', array(
                    'menuConfig' => $menuConfig,
                    'activeSubmit' => $activeSubmit
                )) ?>
                <?php echo ArContactUsAdmin::render('/admin/_callback.php', array(
                    'popupConfig' => $popupConfig,
                    'activeSubmit' => $activeSubmit
                )) ?>
                <?php echo ArContactUsAdmin::render('/admin/_prompt.php', array(
                    'promptConfig' => $promptConfig,
                    'activeSubmit' => $activeSubmit
                )) ?>
                <?php echo ArContactUsAdmin::render('/admin/_items.php', array(
                    'items' => $items
                )) ?>
                <?php echo ArContactUsAdmin::render('/admin/_prompt_items.php', array(
                    'items' => $promptItems
                )) ?>
                <?php echo ArContactUsAdmin::render('/admin/_requests.php', array(
                    'callbackList' => $callbackList,
                    'activeSubmit' => $activeSubmit
                )) ?>
                <?php echo ArContactUsAdmin::render('/admin/_about.php') ?>
                <span class="hidden"></span>
            </div>
        </div>
    </div>
</div>

<div class="ui modal small" id="arcontactus-prompt-modal">
    <i class="close icon"></i>
    <div class="header" id="arcontactus-prompt-modal-title">
        <?php echo __('Add item', AR_CONTACTUS_TEXT_DOMAIN) ?>
    </div>
    <form id="arcontactus-prompt-form" method="POST" onsubmit="arCU.prompt.save(); return false;">
        <input type="hidden" id="arcontactus_prompt_id" name="id" data-serializable="true" autocomplete="off" data-default=""/>
        <div class="ui form" style="padding: 20px;">
            <div class="ui grid">
                <div class="row">
                    <div class="three wide column">
                    </div>
                    <div class="ten wide column">
                        <div class="field required">
                            <label><?php echo __('Message', AR_CONTACTUS_TEXT_DOMAIN) ?></label>
                            <textarea placeholder="" rows="3" id="arcontactus_prompt_message" data-default="" autocomplete="off" data-serializable="true" name="message" type="text"></textarea>
                            <div class="errors"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="actions">
            <button type="button" class="button-large black deny button">
                <?php echo __('Cancel', AR_CONTACTUS_TEXT_DOMAIN) ?>
            </button>
            <button type="submit" class="button button-primary button-large icon">
                <?php echo __('Save', AR_CONTACTUS_TEXT_DOMAIN) ?>
                <i class="checkmark icon"></i>
            </button>
        </div>
    </form>
</div>

<div class="ui modal small" id="arcontactus-modal">
    <i class="close icon"></i>
    <div class="header" id="arcontactus-modal-title">
        <?php echo __('Add item', AR_CONTACTUS_TEXT_DOMAIN) ?>
    </div>
    <form id="arcontactus-form" method="POST" onsubmit="arCU.save(); return false;">
        <input type="hidden" id="arcontactus_id" name="id" data-serializable="true" autocomplete="off" data-default=""/>
        <div class="ui form" style="padding: 20px;">
            <div class="ui grid">
                <div class="row">
                    <div class="three wide column">
                    </div>
                    <div class="ten wide column">
                        <div class="field required">
                            <label><?php echo __('Title', AR_CONTACTUS_TEXT_DOMAIN) ?></label>
                            <input placeholder="" id="arcontactus_title" data-default="" data-serializable="true" name="title" type="text">
                            <div class="errors"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="three wide column">
                    </div>
                    <div class="ten wide column">
                        <div class="field required">
                            <label><?php echo __('Icon', AR_CONTACTUS_TEXT_DOMAIN) ?></label>
                            <div class="ui fluid selection search dropdown iconed" id="arcontactus-icon-dropdown">
                                <input name="icon" id="arcontactus_icon" data-default="" autocomplete="off" data-serializable="true" type="hidden">
                                <i class="dropdown icon"></i>
                                <div class="default text"><?php echo __('Select icon', AR_CONTACTUS_TEXT_DOMAIN) ?></div>
                                <div class="menu">
                                    <?php foreach (ArContactUsConfigModel::getIcons() as $key => $svg){?>
                                        <div class="item" data-value="<?php echo $key ?>">
                                            <?php echo $svg ?>
                                            <?php echo $key ?>
                                        </div>
                                    <?php } ?>
                                </div>
                              </div>
                            <div class="errors"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="three wide column">
                    </div>
                    <div class="ten wide column">
                        <div class="field required">
                            <label><?php echo __('Color', AR_CONTACTUS_TEXT_DOMAIN) ?></label>
                            <input class="jscolor" id="arcontactus_color" data-jscolor="{value:'000000'}" data-default="000000" autocomplete="off" data-serializable="true" name="color" type="text">
                            <div class="errors"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="three wide column">
                    </div>
                    <div class="ten wide column">
                        <div class="field required">
                            <label><?php echo __('Link', AR_CONTACTUS_TEXT_DOMAIN) ?></label>
                            <input placeholder="" id="arcontactus_link" data-default="" autocomplete="off" data-serializable="true" name="link" type="text">
                            <div class="errors"></div>
                            <div class="help-block">
                                <?php echo sprintf(__('You can set absolute or relative URL. Also you can use %scallback%s tag to generate callback request form.', AR_CONTACTUS_TEXT_DOMAIN), '<b>', '</b>') ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="three wide column">
                    </div>
                    <div class="ten wide column">
                        <div class="field">
                            <label><?php echo __('Javascript', AR_CONTACTUS_TEXT_DOMAIN) ?></label>
                            <textarea placeholder="" rows="3" id="arcontactus_js" data-default="" autocomplete="off" data-serializable="true" name="js" type="text"></textarea>
                            <div class="errors"></div>
                            <div class="help-block">
                                <?php echo __('JavaScript code to run onclick. Please type here JavaScript code without <b>&lt;script&gt;</b> tag.', AR_CONTACTUS_TEXT_DOMAIN) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="actions">
            <button type="button" class="button-large black deny button">
                <?php echo __('Cancel', AR_CONTACTUS_TEXT_DOMAIN) ?>
            </button>
            <button type="submit" class="button button-primary button-large icon">
                <?php echo __('Save', AR_CONTACTUS_TEXT_DOMAIN) ?>
                <i class="checkmark icon"></i>
            </button>
        </div>
    </form>
</div>
<script>
    window.addEventListener('load', function(){
        arCU.ajaxUrl = ajaxurl;
        arCU.nonce = '<?php echo wp_create_nonce('arcontactus-key') ?>';
        arCU.editTitle = '<?php echo __('Edit item', AR_CONTACTUS_TEXT_DOMAIN) ?>';
        arCU.addTitle = '<?php echo __('Add item', AR_CONTACTUS_TEXT_DOMAIN) ?>';
        arCU.init();
        arCU.callback.updateCounter();
        setInterval(function(){
            arCU.callback.updateCounter();
        }, 5000);
        jQuery('#acrontactus-menu a').on('click', function(){
            var target = jQuery(this).data('target');
            if (!target){
                return true;
            }
            jQuery('#acrontactus-menu .active').removeClass('active');
            jQuery(this).addClass('active');
            jQuery('.arconfig-panel').addClass('hidden');
            jQuery(target).removeClass('hidden');
        });
        jQuery('.ui.checkbox').checkbox();
        jQuery('#arcontactus-tabs').addClass('active');
        jQuery('#arcontactus-icon-dropdown').dropdown();
        arContactUsSwitchFields();
        jQuery('.ui.toggle.checkbox').on('click', function(){
            arContactUsSwitchFields();
        });
    });
    
    function arContactUsSwitchFields(){
        if (jQuery('.field_email #ARCUP_EMAIL').is(':checked')){
            jQuery('.field_email_list').removeClass('hidden');
        }else{
            jQuery('.field_email_list').addClass('hidden');
        }
        if (jQuery('.field_recaptcha #ARCUP_RECAPTCHA').is(':checked')){
            jQuery('.field_key, .field_secret, .field_hide_recaptcha').removeClass('hidden');
        }else{
            jQuery('.field_key, .field_secret, .field_hide_recaptcha').addClass('hidden');
        }
        if (jQuery('.field_loop #ARCUPR_LOOP').is(':checked')){
            jQuery('.field_close_last').addClass('hidden');
        }else{
            jQuery('.field_close_last').removeClass('hidden');
        }
        
        if (jQuery('.field_twilio #ARCUP_TWILIO').is(':checked')){
            jQuery('.field_twilio_api_key, .field_twilio_auth_token, .field_twilio_phone, .field_twilio_tophone, .field_twilio_message').removeClass('hidden');
        }else{
            jQuery('.field_twilio_api_key, .field_twilio_auth_token, .field_twilio_phone, .field_twilio_tophone, .field_twilio_message').addClass('hidden');
        }
    }
</script>