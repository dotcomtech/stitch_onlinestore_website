<?php
ArContactUsLoader::loadClass('ArContactUsAbstract');
ArContactUsLoader::loadModel('ArContactUsModel');
ArContactUsLoader::loadModel('ArContactUsCallbackModel');
ArContactUsLoader::loadModel('ArContactUsPromptModel');

class ArContactUs extends ArContactUsAbstract
{
    public function css()
    {
        return array(
            'jquery.contactus.css' => 'res/css/jquery.contactus.min.css'
        );
    }
    
    public function js()
    {
        return array(
            'jquery' => null,
            'jquery.contactus.scripts' => 'res/js/scripts.js',
            'jquery.contactus.min.js' => 'res/js/jquery.contactus.min.js'
        );
    }
    
    public function init()
    {
        $this->registerCss();
        $this->registerJs();
        add_action('wp_footer', array($this, 'renderContactUsButton'));
        add_action('wp_enqueue_scripts', array($this, 'registerAjaxScript'));
    }
    
    public function registerAjaxScript()
    {
        wp_localize_script('jquery.contactus.min.js', 'arcontactusAjax', 
            array(
                'url' => admin_url('admin-ajax.php')
            )
	);
    }

    public function renderContactUsButton()
    {
        $generalConfig = new ArContactUsConfigGeneral('arcug_');
        $generalConfig->loadFromConfig();
        if ($generalConfig->pages){
            $currentPage = $_SERVER['REQUEST_URI'];
            $excludePages = explode(PHP_EOL, $generalConfig->pages);
            foreach ($excludePages as $page){
                $p = str_replace(array("\n", "\r"), '', $page);
                if ($currentPage == $p){
                    return null;
                }
            }
        }
        if (!$generalConfig->mobile && $this->isMobile()){
            return null;
        }
        $buttonConfig = new ArContactUsConfigButton('arcub_');
        $menuConfig = new ArContactUsConfigMenu('arcum_');
        $popupConfig = new ArContactUsConfigPopup('arcup_');
        $promptConfig = new ArContactUsConfigPrompt('arcupr_');
        $buttonConfig->loadFromConfig();
        $menuConfig->loadFromConfig();
        $popupConfig->loadFromConfig();
        $promptConfig->loadFromConfig();
        $models = ArContactUsModel::find()->where(array('status' => 1))->orderBy('`position` ASC')->all();
        if ($popupConfig->recaptcha && $popupConfig->key){
            wp_register_script('arcontactus-google-recaptcha-v3', 'https://www.google.com/recaptcha/api.js?render=' . $popupConfig->key, array('jquery'), AR_CONTACTUS_VERSION);
            wp_enqueue_script('arcontactus-google-recaptcha-v3');
        }
        if ($buttonConfig->drag){
            wp_enqueue_script('jquery-ui-draggable');
        }
        $items = array();
        foreach ($models as $model){
            $item = array(
                'href' => $model->link,
                'color' => '#' . $model->color,
                'title' => $model->title,
                'id' => 'msg-item-' . $model->id,
                'class' => 'msg-item-' . $model->icon,
                'js' => $model->js,
                'icon' => ArContactUsConfigModel::getIcon($model->icon)
            );
            $items[] = $item;
        }
        echo self::render('front/button.php', array(
            'generalConfig' => $generalConfig,
            'buttonConfig' => $buttonConfig,
            'menuConfig' => $menuConfig,
            'popupConfig' => $popupConfig,
            'promptConfig' => $promptConfig,
            'messages' => ArContactUsPromptModel::getMessages(),
            'items' => $items
        ));
    }


    public function activate()
    {
        if (!get_option('arcu_installed')){
            ArContactUsModel::createTable();
            ArContactUsModel::createDefaultMenuItems();
            ArContactUsCallbackModel::createTable();
            ArContactUsPromptModel::createTable();
            ArContactUsPromptModel::createDefaultItems();
            
            $generalConfig = new ArContactUsConfigGeneral('arcug_');
            $buttonConfig = new ArContactUsConfigButton('arcub_');
            $menuConfig = new ArContactUsConfigMenu('arcum_');
            $popupConfig = new ArContactUsConfigPopup('arcup_');
            $promptConfig = new ArContactUsConfigPrompt('arcupr_');
            
            $generalConfig->loadDefaults();
            $generalConfig->saveToConfig();
            
            $buttonConfig->loadDefaults();
            $buttonConfig->saveToConfig();
            
            $menuConfig->loadDefaults();
            $menuConfig->saveToConfig();
            
            $popupConfig->loadDefaults();
            $popupConfig->saveToConfig();
            
            $promptConfig->loadDefaults();
            $promptConfig->saveToConfig();
            
            update_option('arcu_installed', time());
        }
        return true;
    }
}
