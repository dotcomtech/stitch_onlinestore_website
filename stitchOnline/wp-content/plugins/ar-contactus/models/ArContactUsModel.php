<?php
ArContactUsLoader::loadModel('ArContactUsModelAbstract');

class ArContactUsModel extends ArContactUsModelAbstract
{
    public $id;
    public $icon;
    public $color;
    public $link;
    public $js;
    public $title;
    public $status;
    public $position;
    
    public function rules()
    {
        return array(
            array(
                array(
                    'icon',
                    'color',
                    'link',
                    'js',
                    'title',
                    'status',
                    'position'
                ), 'safe'
            ),
            array(
                array(
                    'icon',
                    'color',
                    'link',
                    'title'
                ), 'validateRequired'
            )
        );
    }
    
    public function scheme()
    {
        return array(
            'id' => self::FIELD_INT,
            'icon' => self::FIELD_STRING,
            'color' => self::FIELD_STRING,
            'link' => self::FIELD_STRING,
            'js' => self::FIELD_STRING,
            'title' => self::FIELD_STRING,
            'status' => self::FIELD_INT,
            'position' => self::FIELD_INT
        );
    }
    
    public static function tableName()
    {
        return self::dbPrefix().'arcontactus';
    }
    
    public static function createTable()
    {
        return self::getDb()->query("CREATE TABLE IF NOT EXISTS `" . self::tableName() . "` (
            `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `icon` VARCHAR(50) NULL DEFAULT NULL,
            `color` VARCHAR(10) NULL DEFAULT NULL,
            `link` VARCHAR(255) NULL DEFAULT NULL,
            `js` TEXT NULL DEFAULT NULL,
            `title` VARCHAR(255) NULL DEFAULT NULL,
            `status` TINYINT(3) UNSIGNED NULL DEFAULT '1',
            `position` INT(10) UNSIGNED NULL DEFAULT '0',
            PRIMARY KEY (`id`),
            INDEX `position` (`position`)
        )
        COLLATE='utf8_general_ci'
        ENGINE=InnoDB;");
    }
    
    public static function dropTable()
    {
        return self::getDb()->query("DROP TABLE IF EXISTS `" . self::tableName() . "`");
    }
    
    public static function getLastPostion()
    {
        $model = self::find()->orderBy('`position` DESC')->one();
        return $model? $model->position : 0;
    }
    
    public static function getDefaultMenuItems()
    {
        return array(
            array(
                'icon' => 'facebook-messenger',
                'color' => '567AFF',
                'link' => 'https://m.me/[mypage]',
                'title' => 'Messenger',
                'status' => 0
            ),
            array(
                'icon' => 'whatsapp',
                'color' => '1EBEA5',
                'link' => 'https://www.whatsapp.com/',
                'title' => 'Whatsapp',
                'status' => 0
            ),
            array(
                'icon' => 'viber',
                'color' => '812379',
                'link' => 'viber://pa?chatURI=[nickname]',
                'title' => 'Viber',
                'status' => 0
            ),
            array(
                'icon' => 'slack-hash',
                'color' => '3EB891',
                'link' => 'https://slack.com',
                'title' => 'Slack',
                'status' => 0
            ),
            array(
                'icon' => 'telegram-plane',
                'color' => '20AFDE',
                'link' => 'https://t.me/[nickname]',
                'title' => 'Telegram',
                'status' => 0
            ),
            array(
                'icon' => 'skype',
                'color' => '1C9CC5',
                'link' => 'skype://[nickname]?chat',
                'title' => 'Skype',
                'status' => 0
            ),
            array(
                'icon' => 'envelope',
                'color' => 'FF643A',
                'link' => 'mailto:[email@mysite.com]',
                'title' => 'Email us',
                'status' => 0
            ),
            array(
                'icon' => 'phone',
                'color' => '4EB625',
                'link' => 'callback',
                'title' => 'Callback request',
                'status' => 1
            ),
        );
    }
    
    public static function createDefaultMenuItems()
    {
        foreach (self::getDefaultMenuItems() as $k => $item){
            $model = new self();
            $model->icon = $item['icon'];
            $model->color = $item['color'];
            $model->link = $item['link'];
            $model->title = $item['title'];
            $model->status = $item['status'];
            $model->position = $k + 1;
            $model->save();
        }
    }
}
