<?php
ArContactUsLoader::loadModel('ArContactUsConfigModelAbstract');

abstract class ArContactUsConfigModel extends ArContactUsConfigModelAbstract
{
    public static function getIcons()
    {
        return array(
            'facebook-messenger' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M224 32C15.9 32-77.5 278 84.6 400.6V480l75.7-42c142.2 39.8 285.4-59.9 285.4-198.7C445.8 124.8 346.5 32 224 32zm23.4 278.1L190 250.5 79.6 311.6l121.1-128.5 57.4 59.6 110.4-61.1-121.1 128.5z"></path></svg>',
            'facebook' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M448 56.7v398.5c0 13.7-11.1 24.7-24.7 24.7H309.1V306.5h58.2l8.7-67.6h-67v-43.2c0-19.6 5.4-32.9 33.5-32.9h35.8v-60.5c-6.2-.8-27.4-2.7-52.2-2.7-51.6 0-87 31.5-87 89.4v49.9h-58.4v67.6h58.4V480H24.7C11.1 480 0 468.9 0 455.3V56.7C0 43.1 11.1 32 24.7 32h398.5c13.7 0 24.8 11.1 24.8 24.7z"></path></svg>',
            'facebook-f' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 264 512"><path fill="currentColor" d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5 70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229"></path></svg>',
            //'facebook' => '',
            'viber' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M444 49.9C431.3 38.2 379.9.9 265.3.4c0 0-135.1-8.1-200.9 52.3C27.8 89.3 14.9 143 13.5 209.5c-1.4 66.5-3.1 191.1 117 224.9h.1l-.1 51.6s-.8 20.9 13 25.1c16.6 5.2 26.4-10.7 42.3-27.8 8.7-9.4 20.7-23.2 29.8-33.7 82.2 6.9 145.3-8.9 152.5-11.2 16.6-5.4 110.5-17.4 125.7-142 15.8-128.6-7.6-209.8-49.8-246.5zM457.9 287c-12.9 104-89 110.6-103 115.1-6 1.9-61.5 15.7-131.2 11.2 0 0-52 62.7-68.2 79-5.3 5.3-11.1 4.8-11-5.7 0-6.9.4-85.7.4-85.7-.1 0-.1 0 0 0-101.8-28.2-95.8-134.3-94.7-189.8 1.1-55.5 11.6-101 42.6-131.6 55.7-50.5 170.4-43 170.4-43 96.9.4 143.3 29.6 154.1 39.4 35.7 30.6 53.9 103.8 40.6 211.1zm-139-80.8c.4 8.6-12.5 9.2-12.9.6-1.1-22-11.4-32.7-32.6-33.9-8.6-.5-7.8-13.4.7-12.9 27.9 1.5 43.4 17.5 44.8 46.2zm20.3 11.3c1-42.4-25.5-75.6-75.8-79.3-8.5-.6-7.6-13.5.9-12.9 58 4.2 88.9 44.1 87.8 92.5-.1 8.6-13.1 8.2-12.9-.3zm47 13.4c.1 8.6-12.9 8.7-12.9.1-.6-81.5-54.9-125.9-120.8-126.4-8.5-.1-8.5-12.9 0-12.9 73.7.5 133 51.4 133.7 139.2zM374.9 329v.2c-10.8 19-31 40-51.8 33.3l-.2-.3c-21.1-5.9-70.8-31.5-102.2-56.5-16.2-12.8-31-27.9-42.4-42.4-10.3-12.9-20.7-28.2-30.8-46.6-21.3-38.5-26-55.7-26-55.7-6.7-20.8 14.2-41 33.3-51.8h.2c9.2-4.8 18-3.2 23.9 3.9 0 0 12.4 14.8 17.7 22.1 5 6.8 11.7 17.7 15.2 23.8 6.1 10.9 2.3 22-3.7 26.6l-12 9.6c-6.1 4.9-5.3 14-5.3 14s17.8 67.3 84.3 84.3c0 0 9.1.8 14-5.3l9.6-12c4.6-6 15.7-9.8 26.6-3.7 14.7 8.3 33.4 21.2 45.8 32.9 7 5.7 8.6 14.4 3.8 23.6z"></path></svg>',
            'telegram-plane' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M446.7 98.6l-67.6 318.8c-5.1 22.5-18.4 28.1-37.3 17.5l-103-75.9-49.7 47.8c-5.5 5.5-10.1 10.1-20.7 10.1l7.4-104.9 190.9-172.5c8.3-7.4-1.8-11.5-12.9-4.1L117.8 284 16.2 252.2c-22.1-6.9-22.5-22.1 4.6-32.7L418.2 66.4c18.4-6.9 34.5 4.1 28.5 32.2z"></path></svg>',
            'skype' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M424.7 299.8c2.9-14 4.7-28.9 4.7-43.8 0-113.5-91.9-205.3-205.3-205.3-14.9 0-29.7 1.7-43.8 4.7C161.3 40.7 137.7 32 112 32 50.2 32 0 82.2 0 144c0 25.7 8.7 49.3 23.3 68.2-2.9 14-4.7 28.9-4.7 43.8 0 113.5 91.9 205.3 205.3 205.3 14.9 0 29.7-1.7 43.8-4.7 19 14.6 42.6 23.3 68.2 23.3 61.8 0 112-50.2 112-112 .1-25.6-8.6-49.2-23.2-68.1zm-194.6 91.5c-65.6 0-120.5-29.2-120.5-65 0-16 9-30.6 29.5-30.6 31.2 0 34.1 44.9 88.1 44.9 25.7 0 42.3-11.4 42.3-26.3 0-18.7-16-21.6-42-28-62.5-15.4-117.8-22-117.8-87.2 0-59.2 58.6-81.1 109.1-81.1 55.1 0 110.8 21.9 110.8 55.4 0 16.9-11.4 31.8-30.3 31.8-28.3 0-29.2-33.5-75-33.5-25.7 0-42 7-42 22.5 0 19.8 20.8 21.8 69.1 33 41.4 9.3 90.7 26.8 90.7 77.6 0 59.1-57.1 86.5-112 86.5z"></path></svg>',
            'envelope' => '<svg  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M464 64H48C21.5 64 0 85.5 0 112v288c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h416c8.8 0 16 7.2 16 16v41.4c-21.9 18.5-53.2 44-150.6 121.3-16.9 13.4-50.2 45.7-73.4 45.3-23.2.4-56.6-31.9-73.4-45.3C85.2 197.4 53.9 171.9 32 153.4V112c0-8.8 7.2-16 16-16zm416 320H48c-8.8 0-16-7.2-16-16V195c22.8 18.7 58.8 47.6 130.7 104.7 20.5 16.4 56.7 52.5 93.3 52.3 36.4.3 72.3-35.5 93.3-52.3 71.9-57.1 107.9-86 130.7-104.7v205c0 8.8-7.2 16-16 16z"></path></svg>',
            'comments' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M416 192c0-88.4-93.1-160-208-160S0 103.6 0 192c0 34.3 14.1 65.9 38 92-13.4 30.2-35.5 54.2-35.8 54.5-2.2 2.3-2.8 5.7-1.5 8.7S4.8 352 8 352c36.6 0 66.9-12.3 88.7-25 32.2 15.7 70.3 25 111.3 25 114.9 0 208-71.6 208-160zm122 220c23.9-26 38-57.7 38-92 0-66.9-53.5-124.2-129.3-148.1.9 6.6 1.3 13.3 1.3 20.1 0 105.9-107.7 192-240 192-10.8 0-21.3-.8-31.7-1.9C207.8 439.6 281.8 480 368 480c41 0 79.1-9.2 111.3-25 21.8 12.7 52.1 25 88.7 25 3.2 0 6.1-1.9 7.3-4.8 1.3-2.9.7-6.3-1.5-8.7-.3-.3-22.4-24.2-35.8-54.5z"></path></svg>',
            'phone' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path></svg>',
            'whatsapp' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7.9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z"></path></svg>',
            'twitter' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg>',
            'odnoklassniki' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M275.1 334c-27.4 17.4-65.1 24.3-90 26.9l20.9 20.6 76.3 76.3c27.9 28.6-17.5 73.3-45.7 45.7-19.1-19.4-47.1-47.4-76.3-76.6L84 503.4c-28.2 27.5-73.6-17.6-45.4-45.7 19.4-19.4 47.1-47.4 76.3-76.3l20.6-20.6c-24.6-2.6-62.9-9.1-90.6-26.9-32.6-21-46.9-33.3-34.3-59 7.4-14.6 27.7-26.9 54.6-5.7 0 0 36.3 28.9 94.9 28.9s94.9-28.9 94.9-28.9c26.9-21.1 47.1-8.9 54.6 5.7 12.4 25.7-1.9 38-34.5 59.1zM30.3 129.7C30.3 58 88.6 0 160 0s129.7 58 129.7 129.7c0 71.4-58.3 129.4-129.7 129.4s-129.7-58-129.7-129.4zm66 0c0 35.1 28.6 63.7 63.7 63.7s63.7-28.6 63.7-63.7c0-35.4-28.6-64-63.7-64s-63.7 28.6-63.7 64z"></path></svg>',
            'vk' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M545 117.7c3.7-12.5 0-21.7-17.8-21.7h-58.9c-15 0-21.9 7.9-25.6 16.7 0 0-30 73.1-72.4 120.5-13.7 13.7-20 18.1-27.5 18.1-3.7 0-9.4-4.4-9.4-16.9V117.7c0-15-4.2-21.7-16.6-21.7h-92.6c-9.4 0-15 7-15 13.5 0 14.2 21.2 17.5 23.4 57.5v86.8c0 19-3.4 22.5-10.9 22.5-20 0-68.6-73.4-97.4-157.4-5.8-16.3-11.5-22.9-26.6-22.9H38.8c-16.8 0-20.2 7.9-20.2 16.7 0 15.6 20 93.1 93.1 195.5C160.4 378.1 229 416 291.4 416c37.5 0 42.1-8.4 42.1-22.9 0-66.8-3.4-73.1 15.4-73.1 8.7 0 23.7 4.4 58.7 38.1 40 40 46.6 57.9 69 57.9h58.9c16.8 0 25.3-8.4 20.4-25-11.2-34.9-86.9-106.7-90.3-111.5-8.7-11.2-6.2-16.2 0-26.2.1-.1 72-101.3 79.4-135.6z"></path></svg>',
            'slack-hash' => '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M446.2 270.4c-6.2-19-26.9-29.1-46-22.9l-45.4 15.1-30.3-90 45.4-15.1c19.1-6.2 29.1-26.8 23-45.9-6.2-19-26.9-29.1-46-22.9l-45.4 15.1-15.7-47c-6.2-19-26.9-29.1-46-22.9-19.1 6.2-29.1 26.8-23 45.9l15.7 47-93.4 31.2-15.7-47c-6.2-19-26.9-29.1-46-22.9-19.1 6.2-29.1 26.8-23 45.9l15.7 47-45.3 15c-19.1 6.2-29.1 26.8-23 45.9 5 14.5 19.1 24 33.6 24.6 6.8 1 12-1.6 57.7-16.8l30.3 90L78 354.8c-19 6.2-29.1 26.9-23 45.9 5 14.5 19.1 24 33.6 24.6 6.8 1 12-1.6 57.7-16.8l15.7 47c5.9 16.9 24.7 29 46 22.9 19.1-6.2 29.1-26.8 23-45.9l-15.7-47 93.6-31.3 15.7 47c5.9 16.9 24.7 29 46 22.9 19.1-6.2 29.1-26.8 23-45.9l-15.7-47 45.4-15.1c19-6 29.1-26.7 22.9-45.7zm-254.1 47.2l-30.3-90.2 93.5-31.3 30.3 90.2-93.5 31.3z"></path></svg>'
        );
    }
    
    public function menuSizeSelectOptions()
    {
        return array(
            'large' => __('Large', AR_CONTACTUS_TEXT_DOMAIN),
            'small' => __('Small', AR_CONTACTUS_TEXT_DOMAIN)
        );
    }
    
    public function buttonSizeSelectOptions()
    {
        return array(
            'large' => __('Large', AR_CONTACTUS_TEXT_DOMAIN),
            'medium' => __('Medium', AR_CONTACTUS_TEXT_DOMAIN),
            'small' => __('Small', AR_CONTACTUS_TEXT_DOMAIN)
        );
    }
    
    public function positionSelectOptions()
    {
        return array(
            'left' => __('Left', AR_CONTACTUS_TEXT_DOMAIN),
            'right' => __('Right', AR_CONTACTUS_TEXT_DOMAIN)
        );
    }
    
    public static function getIcon($name)
    {
        $icons = self::getIcons();
        return isset($icons[$name])? $icons[$name] : null;
    }
    
    public function rules()
    {
        return array(
            array(
                array(
                    'mobile',
                    'pages',
                    
                    'button_color',
                    'button_size',
                    'position',
                    'x_offset',
                    'y_offset',
                    'pulsate_speed',
                    'icon_speed',
                    'text',
                    'drag',
                    
                    'menu_size',
                    'menu_bg',
                    'menu_color',
                    'menu_hbg',
                    'menu_hcolor',
                    
                    'enable_prompt',
                    'first_delay',
                    'loop',
                    'close_last',
                    'typing_time',
                    'message_time',
                    
                    'twilio',
                    'twilio_api_key',
                    'twilio_auth_token',
                    'twilio_phone',
                    'twilio_tophone',
                    'twilio_message',
                    
                    'timeout',
                    'message',
                    'phone_placeholder',
                    'proccess_message',
                    'success_message',
                    'fail_message',
                    'btn_title',
                    'onesignal',
                    'email',
                    'email_list',
                    'recaptcha',
                    'key',
                    'secret',
                    'hide_recaptcha'
                ), 'safe'
            ),
            array(
                array(
                    'timeout',
                    'x_offset',
                    'y_offset',
                    'pulsate_speed',
                    'icon_speed'
                ), 'isInt'
            )
        );
    }
    
    public function isInt($value)
    {
        return ((string)(int)$value === (string)$value || $value === false);
    }
    
    public function attributeLabels()
    {
        return array(
            'mobile' => __('Enable on mobile', AR_CONTACTUS_TEXT_DOMAIN),
            'pages' => __('Disable widget on pages', AR_CONTACTUS_TEXT_DOMAIN),
            
            'button_color' => __('Color theme', AR_CONTACTUS_TEXT_DOMAIN),
            'button_size' => __('Button size', AR_CONTACTUS_TEXT_DOMAIN),
            
            'position' => __('Position', AR_CONTACTUS_TEXT_DOMAIN),
            'x_offset' => __('X-axis offset', AR_CONTACTUS_TEXT_DOMAIN),
            'y_offset' => __('Y-axis offset', AR_CONTACTUS_TEXT_DOMAIN),
            'pulsate_speed' => __('Pulsate speed', AR_CONTACTUS_TEXT_DOMAIN),
            'icon_speed' => __('Icon slider speed', AR_CONTACTUS_TEXT_DOMAIN),
            'text' => __('Text', AR_CONTACTUS_TEXT_DOMAIN),
            'drag' => __('Enable button drag', AR_CONTACTUS_TEXT_DOMAIN),

            'enable_prompt' => __('Enable', AR_CONTACTUS_TEXT_DOMAIN),
            'first_delay' => __('Delay first message', AR_CONTACTUS_TEXT_DOMAIN),
            'loop' => __('Loop mesages', AR_CONTACTUS_TEXT_DOMAIN),
            'close_last' => __('Close last message', AR_CONTACTUS_TEXT_DOMAIN),
            'typing_time' => __('Typing time', AR_CONTACTUS_TEXT_DOMAIN),
            'message_time' => __('Message time', AR_CONTACTUS_TEXT_DOMAIN),
            
            'menu_size' => __('Menu size', AR_CONTACTUS_TEXT_DOMAIN),
            'menu_bg' => __('Background color', AR_CONTACTUS_TEXT_DOMAIN),
            'menu_color' => __('Text color', AR_CONTACTUS_TEXT_DOMAIN),
            'menu_hbg' => __('Hovered item background color', AR_CONTACTUS_TEXT_DOMAIN),
            'menu_hcolor' => __('Hovered item text color', AR_CONTACTUS_TEXT_DOMAIN),
            
            'timeout' => __('Timeout', AR_CONTACTUS_TEXT_DOMAIN),
            'message' => __('Message', AR_CONTACTUS_TEXT_DOMAIN),
            'phone_placeholder' => __('Phone field placeholder', AR_CONTACTUS_TEXT_DOMAIN),
            'proccess_message' => __('Proccess message', AR_CONTACTUS_TEXT_DOMAIN),
            'success_message' => __('Success message', AR_CONTACTUS_TEXT_DOMAIN),
            'fail_message' => __('Fail message', AR_CONTACTUS_TEXT_DOMAIN),
            'btn_title' => __('Button title', AR_CONTACTUS_TEXT_DOMAIN),
            'onesignal' => __('Enable Onesignal integration', AR_CONTACTUS_TEXT_DOMAIN),
            'email' => __('Send email', AR_CONTACTUS_TEXT_DOMAIN),
            'email_list' => __('Email list', AR_CONTACTUS_TEXT_DOMAIN),
            'recaptcha' => __('Integrate with Google reCaptcha', AR_CONTACTUS_TEXT_DOMAIN),
            'key' => __('Google reCaptcha Site Key', AR_CONTACTUS_TEXT_DOMAIN),
            'secret' => __('Google reCaptcha Secret', AR_CONTACTUS_TEXT_DOMAIN),
            'hide_recaptcha' => __('Hide Google reCaptcha logo', AR_CONTACTUS_TEXT_DOMAIN),
            
            'twilio' => __('Enable Twilio integration', AR_CONTACTUS_TEXT_DOMAIN),
            'twilio_api_key' => __('Twilio API Key', AR_CONTACTUS_TEXT_DOMAIN),
            'twilio_auth_token' => __('Twilio Auth Token', AR_CONTACTUS_TEXT_DOMAIN),
            'twilio_phone' => __('Twilio phone', AR_CONTACTUS_TEXT_DOMAIN),
            'twilio_tophone' => __('Send SMS to this phone', AR_CONTACTUS_TEXT_DOMAIN),
            'twilio_message' => __('SMS text', AR_CONTACTUS_TEXT_DOMAIN)
        );
    }
    
    public function fieldSuffix()
    {
        return array(
            'x_offset' => __('px', AR_CONTACTUS_TEXT_DOMAIN),
            'y_offset' => __('px', AR_CONTACTUS_TEXT_DOMAIN),
            'pulsate_speed' => __('ms', AR_CONTACTUS_TEXT_DOMAIN),
            'icon_speed' => __('ms', AR_CONTACTUS_TEXT_DOMAIN),
            'menu_border' => __('px', AR_CONTACTUS_TEXT_DOMAIN),
            'timeout' => __('seconds', AR_CONTACTUS_TEXT_DOMAIN),
            'first_delay' => __('ms', AR_CONTACTUS_TEXT_DOMAIN),
            'typing_time' => __('ms', AR_CONTACTUS_TEXT_DOMAIN),
            'message_time' => __('ms', AR_CONTACTUS_TEXT_DOMAIN)
        );
    }
    
    public function attributeTypes()
    {
        return array(
            'mobile' => 'switch',

            'button_color' => 'color',
            'button_size' => 'select',
            'position' => 'select',
            'x_offset' => 'text',
            'y_offset' => 'text',
            'pulsate_speed' => 'text',
            'icon_speed' => 'text',
            'text' => 'textarea',
            'drag' => 'switch',
            
            'enable_prompt' => 'switch',
            'loop' => 'switch',
            'close_last' => 'switch',
            
            'menu_size' => 'select',
            'menu_bg' => 'color',
            'menu_color' => 'color',
            'menu_hbg' => 'color',
            'menu_hcolor' => 'color',
            
            'twilio' => 'switch',
            'message' => 'textarea',
            'proccess_message' => 'textarea',
            'success_message' => 'textarea',
            'fail_message' => 'textarea',
            'onesignal' => 'switch',
            'email_list' => 'textarea',
            'email' => 'switch',
            'recaptcha' => 'switch',
            'hide_recaptcha' => 'switch',
            'pages' => 'textarea'
        );
    }
    
    public function attributeDescriptions()
    {
        return array(
            'timeout' => __('Set to 0 to disable countdown.', AR_CONTACTUS_TEXT_DOMAIN),
            'email_list' => __('One email per line.', AR_CONTACTUS_TEXT_DOMAIN),
            'recaptcha' => __('You can use Google reCaptcha to prevent bots from sending callback requests. This module uses invisible reCaptcha V3', AR_CONTACTUS_TEXT_DOMAIN),
            'onesignal' => __('Onesignal module is detected on your shop. You can use this option to enable send web push notification to admin users if customer requested callback. To receive these messages you need to subscribe for onesignal admin push notifications.', AR_CONTACTUS_TEXT_DOMAIN),
            'key' => __('You can get your Key here https://g.co/recaptcha/v3', AR_CONTACTUS_TEXT_DOMAIN),
            'secret' => __('You can get your Secret here https://g.co/recaptcha/v3', AR_CONTACTUS_TEXT_DOMAIN),
            'pages' => __('You can disable widget on several pages. Please use relative URLs. For example, to disable widget on home page write "/" (without qoutes). One URL per line.', AR_CONTACTUS_TEXT_DOMAIN),
            'icon_speed' => __('Type 0 here to disable button animation', AR_CONTACTUS_TEXT_DOMAIN),
            'twilio_message' => __('{phone} token will be replaced to phone entered in callback request form', AR_CONTACTUS_TEXT_DOMAIN),
            'twilio_phone' => __('Your Twilio phone in international format', AR_CONTACTUS_TEXT_DOMAIN),
            'twilio_tophone' => __('SMS message will be send to this phone number. Use international format', AR_CONTACTUS_TEXT_DOMAIN),
        );
    }
    
    public function multiLangFields()
    {
        return array(
            'text' => true,
            'message' => true,
            'phone_placeholder' => true,
            'proccess_message' => true,
            'success_message' => true,
            'fail_message' => true,
            'btn_title' => true
        );
    }
}
