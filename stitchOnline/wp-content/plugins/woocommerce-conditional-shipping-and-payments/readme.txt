=== WooCommerce Conditional Shipping and Payments ===

Contributors: franticpsyx
Tags: woocommerce, conditional, checkout, restrictions, countries, gateways, shipping, methods, exclude, access
Requires at least: 4.1
Tested up to: 4.8
Stable tag: 1.2.8
WC requires at least: 2.3
WC tested up to: 3.1
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Exclude payment gateways, shipping methods and shipping countries/states using conditional logic.

== Description ==

Get full control over the countries/states, payment gateways and shipping methods available at checkout. Exclude payment and shipping options by creating powerful rules using conditional logic.

Need help? Read the full documentation [here](https://docs.woocommerce.com/document/woocommerce-conditional-shipping-and-payments/).