<?php 

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Model Class
 *
 * Handles generic plugin functionality.
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
class Woo_Cl_Model {
	
	//Constructor
	public function __construct() {
		
	}
	
	/**
	 * Escape Tags & Slashes
	 *
	 * Handles escapping the slashes and tags
	 *
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_escape_attr( $data ) {
		
		$woo_cl_escape_attr	= esc_attr( stripslashes( $data ) );
		return apply_filters( 'woo_cl_escape_attr', $woo_cl_escape_attr, $data );
	}
	
	/**
	 * Strip Slashes From Array
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_escape_slashes_deep( $data = array(), $flag = false, $limited = false ) {
		
		$temp_data	= $data;
		
		if( $flag != true ) {
			$data = $this->woo_cl_nohtml_kses( $data );
		} else {
			if( $limited == true ) {
				$data = wp_kses_post( $data );
			}
		}
		
		$data	= stripslashes_deep( $data );
		
		return apply_filters( 'woo_cl_escape_slashes_deep', $data, $temp_data, $flag, $limited );
	}
	
	/**
	 * Strip Html Tags 
	 * 
	 * It will sanitize text input (strip html tags, and escape characters)
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_nohtml_kses($data = array()) {
		
		$temp_data	= $data;
		
		if ( is_array( $data ) ) {
			
			$data	= array_map( array( $this, 'woo_cl_nohtml_kses' ), $data );
		} elseif ( is_string( $data ) ) {
			
			$data	= wp_filter_nohtml_kses( $data );
		}
		
		return apply_filters( 'woo_cl_nohtml_kses', $data, $temp_data );
	}
	
	/**
	 * Convert Object To Array
	 * 
	 * Converting Object Type Data To Array Type
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_object_to_array( $result ) {
		
	    $array	= array();
	    foreach( $result as $key => $value ) {
			if( is_object( $value ) ) {
				$array[$key]	= $this->woo_cl_object_to_array($value);
			} else {
				$array[$key]	= $value;
			}
	    }
	    
	    return apply_filters( 'woo_cl_object_to_array', $array, $result );
	}
	
	/**
	 * Get Date Format
	 * 
	 * Handles to return formatted date which format is set in backend
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_date_format( $date, $time = false ) {
		
		$format	= $time ? get_option( 'date_format' ).' '.get_option('time_format') : get_option('date_format');
		$date	= date_i18n( $format, strtotime( $date ) );
		
		return apply_filters( 'woo_cl_get_date_format', $date );
	}
	
	/**
	 * Add product to collection
	 * 
	 * return created collection item id
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_add_product_to_collection( $args = array() ) {
		
		global $current_user;
		
		//Get meta prefix
		$prefix = WOO_CL_META_PREFIX;
		
		//Get User Id
		if( isset($args['user_id']) ) {
			$user_ID = $args['user_id'];
		} else {
			$user_ID = isset( $current_user->ID ) ? $current_user->ID : '';
		}
		
		//Get all Messages
		$messages	= woo_cl_messages();
		
		$coll_id 		= isset($args['coll_id']) 			? $args['coll_id'] 					: '';
		$coll_productid = isset($args['coll_productid']) 	? $args['coll_productid'] 			: '';
		$variations		= isset($args['variations'])		? serialize( $args['variations'] ) 	: '';
		
		//Get Product Data
		$product_data = woo_cl_wc_get_product( $coll_productid );
		
		//Get Product Type if exist
		$product_type = isset( $product_data->product_type ) ? $product_data->product_type : '';

		//Get title if exist
		$coll_producttitle = woo_cl_get_product_title( $coll_productid );
		
		if(isset($args['collitem_disc']) && $args['collitem_disc'] != __('Enter a description. Say why you chose this item or why you love it.', 'woocl')) {
			
			$collitem_disc = $this->woo_cl_escape_slashes_deep( $args['collitem_disc'] );
		} else {
			
			$collitem_disc = '';
		}
		
		// Get post author id
		$author_id = get_post_field( 'post_author', $coll_id );
		$author_id = !empty($author_id) ? $author_id : $user_ID;
		
		//get next menu order
		$next_menu_order	= $this->woo_cl_get_coll_item_next_manu_order( $coll_id );

		$collection_item = array(
									'post_type' 	=> WOO_CL_POST_TYPE_COLLITEMS,
									'post_status' 	=> 'publish',
									'post_title' 	=> $coll_producttitle,
									'post_content' 	=> $collitem_disc,
									'post_author' 	=> $author_id,
									'menu_order' 	=> $next_menu_order,
									'post_parent'   => $coll_id,
									'comment_status'=> 'closed'
								);
			
		//Add product to collection
		$collection_item_id = wp_insert_post( $collection_item );

		//Update collection product id
		update_post_meta( $collection_item_id, $prefix.'coll_product_id', $coll_productid );

		//Update collection product Type
		update_post_meta( $collection_item_id, $prefix.'coll_product_type', $product_type );
		
		if( $product_type == 'variation' ) {
			
			//Update collection variation selected
			update_post_meta( $collection_item_id, $prefix.'coll_product_selected_variations', $variations );
		}

		return apply_filters( 'woo_cl_add_product_to_collection', $collection_item_id, $args );
	}
	
	/**
	 * Get Collections
	 *
	 * Handles get all Collections from database
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_collections( $args = array() ) {

		$queryargs	= array( 'post_type' => WOO_CL_POST_TYPE_COLL );

		$queryargs 	= wp_parse_args( $args, $queryargs );

		//get data by author id
		if( isset( $args['author'] ) && !empty( $args['author'] ) ) {
			$queryargs['author']	= $args['author'];
		}
		
		//get data by author id
		if( isset( $args['paged'] ) && !empty( $args['paged'] ) ) {
			$queryargs['paged']	= $args['paged'];
		}

		//get collection by custom field
		if(isset($args['meta_query']) && !empty($args['meta_query'])) {
			$queryargs['meta_query']	= $args['meta_query'];
		}
		
		//get collection order by
		if(isset($args['orderby']) && !empty($args['orderby'])) {
			$queryargs['orderby']	= $args['orderby'];
		}
		
		//get collection order type
		if(isset($args['order']) && !empty($args['order'])) {
			$queryargs['order']	= $args['order'];
		}
		
		//show collections by status
		if( isset( $args['post_status'] ) && !empty( $args['post_status'] ) ) {
			$queryargs['post_status'] = $args['post_status'];
		} else {
			$queryargs['post_status'] = 'publish';
		}
		
		//get particulate collection id data
		if(isset($args['p']) && !empty($args['p'])) {
			$queryargs['p']	= $args['p'];
		}
		
		//show how many per page records
		if( isset( $args['posts_per_page']) && !empty( $args['posts_per_page'] ) ) {
			$queryargs['posts_per_page'] = $args['posts_per_page'];
		} else {
			$queryargs['posts_per_page'] = -1;
		}
		
		// Get OrderBy parameter if set
		if(isset($args['orderby']) && !empty($args['orderby'])) {
			$queryargs['orderby']	= $args['orderby'];
		}
		
		//fire query in to table for retriving data
		$result = new WP_Query( $queryargs );

		if(isset($args['getcount']) && $args['getcount'] == '1') {
			$postslist	= $result->post_count;
		}  else {
			//retrived data is in object format so assign that data to array for listing
			$postslist = $this->woo_cl_object_to_array($result->posts);
		}
		
		return apply_filters( 'woo_cl_get_collections', $postslist, $args );
	}
	
	/**
	 * Get Collection Items
	 *
	 * Handles get all Collection Items from database
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_collection_items( $args = array() ) {
		
		//get post ststus
		$post_status	= isset( $args['post_status'] ) ? $args['post_status'] : 'publish';
		
		$queryargs = array('post_type' => WOO_CL_POST_TYPE_COLLITEMS, 'post_status' => $post_status );

		$queryargs 	= wp_parse_args( $args, $queryargs );

		//get collection items by author id
		if(isset($args['author']) && !empty($args['author'])) {
			$queryargs['author']	= $args['author'];
		}
		
		//get collection items by post parent
		if(isset($args['post_parent']) && !empty($args['post_parent'])) {
			$queryargs['post_parent']	= $args['post_parent'];
		}
		
		//get collection items by custom field product id
		if(isset($args['meta_query']) && !empty($args['meta_query'])) {
			
			$queryargs['meta_query']	= $args['meta_query'];
		}
		
		//show how many per page records
		if(isset($args['posts_per_page']) && !empty($args['posts_per_page'])) {
			$queryargs['posts_per_page'] = $args['posts_per_page'];
		} else {
			$queryargs['posts_per_page'] = -1;
		}
		
		//get data by author id
		if( isset( $args['paged'] ) && !empty( $args['paged'] ) ) {
			$queryargs['paged']	= $args['paged'];
		}
		
		if( empty( $args['orderby'] ) && empty( $args['order'] ) ) { //If both orderby and order is empty
			
			$args['orderby']	= 'menu_order';
			$args['order']		= 'ASC';
		}
		
		//get data sort by orderby
		if( isset( $args['orderby'] ) && !empty( $args['orderby'] ) ) {
			$queryargs['orderby']	= $args['orderby'];
		}
		
		//get data by order
		if( isset( $args['order'] ) && !empty( $args['order'] ) ) {
			$queryargs['order']	= $args['order'];
		}
		
		//fire query in to table for retriving data
		$result = new WP_Query( $queryargs );
		
		if( isset($args['getcount']) && $args['getcount'] == '1' ) {
			
			$postslist = $result->post_count;	
			
		}  else {
			//retrived data is in object format so assign that data to array for listing
			$postslist = $this->woo_cl_object_to_array( $result->posts );
		}		
		
		return $postslist;		
	}
	
	/**
	 * Check if collection Title exists or not
	 *
	 * Handles check Collections Title exists or not
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_collection_exists( $args = array() ) {
		
		global $wpdb;
		
		//Initilize post id
		$postsid	= false;
		
		//Query for get collection data
		$queryargs = "SELECT ID FROM $wpdb->posts WHERE 1=1";
		
		if( isset( $args['post_title'] ) && !empty( $args['post_title'] ) ) {
			$queryargs .= " AND post_title = '". esc_sql( $args['post_title'] )."'";	
		}

		if(isset($args['post_type']) && !empty($args['post_type'])) {
			$queryargs .= " AND post_type = '".$args['post_type']."'";	
		}

		if(isset($args['post_author']) && !empty($args['post_author'])) {
			$queryargs .= " AND post_author = '".$args['post_author']."'";	
		}

		if(isset($args['post_status']) && !empty($args['post_status'])) {
			$queryargs .= " AND post_status = '".$args['post_status']."'";	
		}

		//Get collection data
	  	$post_row = $wpdb->get_row( $queryargs );
  		
  		if( !empty( $post_row->ID ) ) {//If empty result
   			
   			$postsid	= $post_row->ID;
  		}
  
  		return $postsid;	
	}
	
	/**
	 * Check if product type is Simple, Variable, External
	 *
	 * Handles Check if product type is Simple, variable, External then return true else return false
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_allowed_product_type( $product_id = '' ) {
		
		global $product;
		
		// default allowed product types
		$allowed_product_types	= array( 'simple', 'variable', 'variation', 'external' );
		
		// apply filter to modify product types array
		$allowed_product_types	= apply_filters( 'woo_cl_allowed_product_type', $allowed_product_types );
		
		if( !empty( $product_id ) ) { //If product Id is empty
			$_product = woo_cl_wc_get_product( $product_id );

		} else { //Assign global product
			$_product = $product;
		}
		
		if( !empty( $_product ) ) {//If product is not empty
			foreach ( $allowed_product_types as $allowed_product_type ) {
				if( $_product->is_type( $allowed_product_type ) ) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Get Product price
	 *
	 * Handles get Product price
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_collection_product_price( $product_id = '', $formated = false ) {
		
		//Initilize product price
		$product_price	= false;
		
		if( !empty( $product_id ) ) {//If product Id is not empty
			
			//Get product
			$_product = wc_get_product( $product_id );
			
			if( $formated == true ) {//If formatted is true
				
				//Get formatted price
				$product_price	= $_product->get_price_html();
			}
			
			if( $formated == false ) {//if formatted is false
				
				//Get price without html format
				$product_price	= $_product->get_price();
			}
		}
		
		return apply_filters( 'woo_cl_collection_product_price', $product_price, $product_id );
	}
	
	/**
	 * Get Collection Page URL
	 * 
	 * Handles to get collection page URL, if user name passed then it will return collection author page URL
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_collection_page_url( $user_name = '' ) {

		//Get user endpoint
		$woo_cl_user_slug	= get_option( 'cl_collection_author_text' );
		
		//Get collectio page URL
		$coll_page_url	= get_permalink( woo_cl_get_collection_page_id() );
		
		if( !empty( $user_name ) ) {
			// get collection author page URL
			$coll_page_url	= woo_cl_get_endpoint_url( $woo_cl_user_slug, $coll_page_url, $user_name );
		}
		
		return apply_filters( 'woo_cl_get_collection_page_url', $coll_page_url, $user_name );
	}
	
	/**
	 * Get Collection Item Page URL
	 * 
	 * Handles to get collection item page URL
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_collection_item_page_url( $collection_id = '' ) {
		
		//Set default collection item URL to collection list page URL
		$item_page_url	= get_permalink( woo_cl_get_collection_page_id() );
		
		if( !empty( $collection_id ) ) { //If collection ID is not empty
			
			//Get collection view page end point
			$woo_cl_view_slug	= get_option( 'cl_view_collection_text' );
			
			//Get collection item page URL
			$item_page_url	= woo_cl_get_endpoint_url( $woo_cl_view_slug, $item_page_url, $collection_id );
		}
		
		return apply_filters( 'woo_cl_get_collection_item_page_url', $item_page_url, $collection_id );
	}
	
	/**
	 * Get Collection Edit Page URL
	 * 
	 * Handles to get collection edit page URL
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_collection_edit_page_url( $collection_id = '' ) {
		
		//Set default edit collection URL to collection list page URL
		$edit_page_url	= get_permalink( woo_cl_get_collection_page_id() );
		
		if( !empty( $collection_id ) ) { //If collection ID is not empty
			
			//Get collection edit end point
			$woo_cl_edit_slug	= get_option( 'cl_edit_collection_text' );
			
			//Get collection item page URL
			$edit_page_url	= woo_cl_get_endpoint_url( $woo_cl_edit_slug, $edit_page_url, $collection_id );
		}
		
		return apply_filters( 'woo_cl_get_collection_edit_page_url', $edit_page_url, $collection_id );;
	}
	
	/**
	 * Get Collection Product Total price
	 *
	 * Handles get collection Product total price
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_collection_products_total_price( $coll_id ) {
		
		//Get meta prefix
		$prefix = WOO_CL_META_PREFIX;
		
		//Initilize total price
		$total_price	= 0;
		
		//collection items query arguments
		$args = array(
						'post_parent' 	=> $coll_id
					); 
		
		//get total collection items
		$coll_products	= $this->woo_cl_get_collection_items( $args );
		
		foreach( $coll_products as $key => $coll_product ) {
			
			// get product id of collection item
			$curr_product_id	= get_post_meta( $coll_product['ID'], $prefix.'coll_product_id', true );
			
			//Get collection product price
			$product_price = $this->woo_cl_collection_product_price( $curr_product_id );
			
			//Add item price in total price
			$total_price	= $total_price + $product_price;
			
		} // for loop end
		
		$total_price = wc_price( $total_price );
		
		return apply_filters( 'woo_cl_collection_products_total_price', $total_price, $coll_id );
	}
	
	/**
	 * Get UserId By Username
	 *
	 * Handles get userid by username
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_userid_by_username( $user_name = '' ) {
		
		$return_value	= false;
		
		//If username is blank
		if( !empty( $user_name ) ) {
			
			//Get user by username
			$user_data	= get_user_by( 'login', $user_name );
			
			//If userid not empty
			if( !empty( $user_data->ID ) ) $return_value = $user_data->ID;
		}
		
		return apply_filters( 'woo_cl_get_userid_by_username', $return_value, $user_name );
	}
	
	/**
	 * Get Limit Character Content
	 *
	 * Handles get get limited character content with read more icon
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_limit_char( $content = '', $limit = 65, $readmore_icon = true, $url = '' ) {
		
		$data	= '';
		
		if( strlen( $content ) <= $limit ) { //check content is less then 65 character
			$data	= $content;
		} else {
			
			// do_shortcode to replace shortcode from content with respective html
			$content = do_shortcode( $content );

			//Strip html tags
			$content = $this->woo_cl_escape_slashes_deep( $content );

			$data	= substr( $content, 0, $limit );
			
			if( $readmore_icon && !empty( $url ) ) {
				
				$data	.= '&nbsp;<a href="'. $url .'"><span class="woocl-read-more"></span></a>';
			} else {
				
				$data	.= '...';
			}
			
			return $data;
		}
		
		return apply_filters( 'woo_cl_get_limit_char', $data, $content, $limit, $readmore_icon, $url );
	}
	
	/**
	 * Get Minimum Price of Collection
	 *
	 * Handles get minimum price from collection items prices
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_min_price( $coll_id = '' ) {
		
		//get prefix
		$prefix	= WOO_CL_META_PREFIX;

		//Initialize variable
		$min_price	= 0;
		$flag = true;
		
		//Collection item arguments
		$args = array(
						'post_parent' 		=> $coll_id,
					);
		
		//get all collection items
		$coll_products = $this->woo_cl_get_collection_items( $args );
		
		foreach ( $coll_products as $coll_product ) {//loop for collection items
			
			//Get product id , Product Price
			$curr_coll_product_id	= get_post_meta( $coll_product['ID'], $prefix.'coll_product_id', true );				
			$product_price			= $this->woo_cl_collection_product_price( $curr_coll_product_id );
			
			if( $flag ) {//check if first time
				$min_price	= $product_price;
				$flag	= false;
			}
	
			if( $min_price > $product_price ) {//check if product price is bigger
				
				//asign product price
				$min_price	= $product_price;
			}
			
		}
		
		return apply_filters( 'woo_cl_get_min_price', wc_price( $min_price ), $coll_id );
	}
	
	/**
	 * Delete a collection or collection item
	 *
	 * Handles delete a collection or collection items  by id
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_post_delete( $post_id ) {
		
		$response = false;
		
		if( !empty( $post_id ) ) {//if post id not empty
			
			do_action( 'woo_cl_post_before_delete', $post_id );
			
			//delete a post
			wp_delete_post( $post_id, true );
			$response = true; //response for success
			
			do_action( 'woo_cl_post_after_delete', $post_id );
		}
		
		return apply_filters( 'woo_cl_post_delete', $response );
	}
	
	/**
	 * Get the image url
	 * 
	 * Handles to get image url with support varible product
	 *
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_feature_image_url( $product_id = '' ) {
		
		$product_imgurl	= '';
		
		//Get image URL
		$product_imgurl	= wp_get_attachment_image_src( get_post_thumbnail_id( $product_id) , 'large');
		
		//if product image url empty then get parent image
		if( empty( $product_imgurl ) ) {
			
			$p_id			= wp_get_post_parent_id( $product_id );//Get parents id when variation
			
			if( !empty( $p_id ) ) {
				
				$product_imgurl	= wp_get_attachment_image_src( get_post_thumbnail_id( $p_id), 'large' );
			}
		}
		
		//if product image url empty then set no image
		if( empty( $product_imgurl ) ) {
			
			$product_imgurl[0] = WOO_CL_IMG_URL.'/woo-cl-no-item.gif';
		}
		
		return apply_filters( 'woo_cl_get_feature_image_url', $product_imgurl[0], $product_id );
	}
	
	/**
	 * Get the image url
	 * 
	 * Handles to get image url with support varible product
	 *
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_feature_small_image_url( $product_id = '' ) {
		
		$product_small_imgurl	= '';
		
		//Get small size image URL
	
		 	$product_small_imgurl	= wp_get_attachment_image_src( get_post_thumbnail_id( $product_id, 'small' ) );
	
		
		//if product small size image url empty then get parent image
		if( empty( $product_small_imgurl ) ) {
			
			$p_id	= wp_get_post_parent_id( $product_id );//Get parents id when variation
			
			if( !empty( $p_id ) ) {
				
				$product_small_imgurl	= wp_get_attachment_image_src( get_post_thumbnail_id( $p_id, 'small' ) );
			}
		}
		
		//if product image url empty then set no image
		if( empty( $product_small_imgurl ) ) {
			
			$product_small_imgurl[0] = WOO_CL_IMG_URL.'/woo-cl-no-item.gif';
		}
		
		return apply_filters( 'woo_cl_get_feature_small_image_url', $product_small_imgurl[0], $product_id );
	}
	
	
	
	/**
	 * Get the Cover image url
	 * 
	 * Handles to get cover image url with support varible product
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_cover_image_url( $collection_id = '' ) {
		
		//get prefix
		$prefix	= WOO_CL_META_PREFIX;
		
		//initilize cover image
		$cover_imgurl	= '';
		
		//collection item sort by menu order
		$coll_item_arg	= array(
								'post_parent'	=> $collection_id,
								'posts_per_page'=> 1
							);
		
		//get collection item
		$order_items	= $this->woo_cl_get_collection_items( $coll_item_arg );
		
		if( is_array( $order_items ) ) {//If item data is not empty
			
			$cover_img_coll_item= reset( $order_items );
			
			//Get Product Id
			$coll_product_id	= get_post_meta( $cover_img_coll_item['ID'], $prefix.'coll_product_id', true );
			
			if( !empty( $coll_product_id ) ) {//Item id for cover image
				
				//Get image URL
				$cover_imgurl	= $this->woo_cl_get_feature_image_url( $coll_product_id );
			}
		}

		//Apply filter for cover image url
  		$cover_imgurl = apply_filters( 'woo_cl_cover_image_url', $cover_imgurl, $collection_id );
 
		if( empty( $cover_imgurl ) ) { //if product image url empty then set default image url
			
			//Default image URL
			$cover_imgurl = WOO_CL_IMG_URL.'/woo-cl-no-item.gif';				
		}
		
		return apply_filters( 'woo_cl_get_cover_image_url', $cover_imgurl, $collection_id );
	}
	
	/**
	 * Get Next Collection Item Menu Order
	 * 
	 * Handles to get next collection item menu order
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_coll_item_next_manu_order( $coll_id = '' ) {
		
		global $wpdb;
		
		$menu_order	= 0;
		
		if( !empty( $coll_id ) ) { // If collection item is not empty
			
			//Get max menu order
			$max_order_sql	= 'SELECT MAX(menu_order) FROM '. $wpdb->posts.' WHERE post_type = "' . WOO_CL_POST_TYPE_COLLITEMS . '" AND post_parent = "' . $coll_id . '"';
			$menu_order		= $wpdb->get_var( $max_order_sql );
			
			//Next menu order
			$menu_order = $menu_order + 1;
		}
		
		return apply_filters( 'woo_cl_get_coll_item_next_manu_order', $menu_order, $coll_id );
	}
	
	/**
	 * Handles Admin side add and display,
	 * create list of added products
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.1
	 */
	public function woo_cl_get_admin_products_by_collection_id( $coll_id ) {
		
		$prefix = WOO_CL_META_PREFIX;
		
		if( empty($coll_id) ) {
			return;
		}
		
		$total_price = 0;
		
		$args = array(
						'post_parent' => $coll_id
					);
		
		$coll_products = $this->woo_cl_get_collection_items( $args ); //get all collection items
		
		// Check user can edit file
		$user_can = woo_cl_user_can_update_collection( $coll_id );
		
		ob_start(); ?>
		
		<table class="woo_cl_products_disc" cellpadding="8" cellspacing="0">
			<thead>
				<tr>
					<th id="drag" class="item" align="left"><?php _e( 'Drag', 'woocl' );?></th>
					<th id="cb" class="item" align="left"><input type="checkbox" class="woocl-checkall"></th>
					<th class="item" align="left"><?php _e('Image','woocl');?></th>
					<th class="item" align="left"><?php _e('Item','woocl');?></th>
					<th class="line_cost" align="left"><?php _e('Total','woocl');?></th>	
					<?php do_action( 'woo_cl_admin_meta_after_item_heading' ); ?>
					<?php if( $user_can == true ) { ?>
						<th width="1%" align="left"><?php _e('Action','woocl');?></th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
			<?php 
				if( !empty($coll_products) ) {

					foreach ($coll_products as $key => $coll_product) {
						
						echo '<tr valign="top">';
						
						//Declare $product_id and $product_variation
						$product_id = $product_variation = '';

						// get product id collection item
						$curr_product_id 	= get_post_meta($coll_product['ID'], $prefix.'coll_product_id', true); 

						// get product type collection item
						$curr_product_type 	= get_post_meta($coll_product['ID'], $prefix.'coll_product_type', true); 
						
						//get post edit url			
						$product_admin_url = get_edit_post_link( $curr_product_id );

						if( $curr_product_type == 'variation' )	{//if product is variation
							
							//get perent id when variarion product
							$productid = wp_get_post_parent_id( $curr_product_id );
							
							if( !empty( $productid ) ) {
								//get post edit url when variation product
								$product_admin_url = get_edit_post_link( $productid );
							} else {
								//get post edit url when variation product
								$product_admin_url = get_edit_post_link( $curr_product_id );
							}
						}

						//Get Product Image URL
						$product_imgurl	= wp_get_attachment_url( get_post_thumbnail_id( $curr_product_id, array(35, 35) ) );
						
						if( empty( $product_imgurl ) ) {//If URL is empty
				
							//Get placeholder image URL
							$product_imgurl = WOO_CL_IMG_URL.'/woo-cl-no-item.gif';
						}

						$product_price  = $this->woo_cl_collection_product_price( $curr_product_id );
							
						$total_price 	= $total_price + $product_price;
						
						$woo_cl_coll_item_variation_meta 	= get_post_meta( $coll_product['ID'], $prefix.'coll_product_selected_variations', true );
						$woo_cl_coll_item_variation 		= !empty( $woo_cl_coll_item_variation_meta ) ? unserialize( $woo_cl_coll_item_variation_meta ) : '';
						
						if( !empty( $woo_cl_coll_item_variation ) ) {
			
							//Get Variation data
							$product_variation 		= apply_filters( 'woo_cl_get_product_variation', wc_get_formatted_variation( $woo_cl_coll_item_variation, false ), $curr_product_id, false, $coll_product['ID'] );
						} else {
							if( !empty( $productid ) ) {
								//Get Variation data
								$product_variation		= woo_cl_get_product_variation( $curr_product_id, false, $coll_product['ID'] );
							}
						}
						
						echo '<td width="20px" class="column-drag"><span class="dashicons-before dashicons dashicons-menu"></span></td>';
						echo '<td width="45px" class="check-column"><input type="checkbox">
							  <input class="woo_cl_coll_item_id" type="hidden" value="'. $coll_product['ID'] .'" name="woo_cl_coll_item_id"></td>';
						
						
						echo '<td width="45px"><img src="'. $product_imgurl .'" alt="" width="35" height="35"/></td>
								<td><a href="'.$product_admin_url.'" title="'.$coll_product['post_title'].'">'.$coll_product['post_title'].'</a>
							  	<div class="view">'. $product_variation .'</div>
							  	'.'
							  	</td>
							  	<td>'.wc_price( $product_price ).'</td>';
						
						do_action( 'woo_cl_admin_meta_after_total_content', $curr_product_id, $coll_product['ID'] );
						
						if( $user_can == true ) {
							echo '<td><a href="javascript:void(0);" title="'.__( 'Delete','woocl' ).'" class="woo_cl_delete_coll_item"></a>';
						}
						
						echo '</tr>';
					} // end for loop
				
				} else {
				
					echo '<tr valign="top"><td colspan="6">'.sprintf( __("There is no products for this %s.","woocl"), woo_cl_get_label_singular() ).'</td></tr>';
				} ?>
			</tbody>
			
			<tfoot>
					<tr valign="top" class="woo_cl_product_total">
						<th></th>
						<td colspan="1">
							<?php if( $user_can == true ) { ?>
								<input type="checkbox" class="woocl-checkall">
							<?php } ?>
						</td>	
						<td colspan="2">
							<?php if( $user_can == true ) { ?>
						
								<select class="woo_cl_bulk_select">
									<option value=""><?php _e('Actions','woocl');?></option>
									<option value="save"><?php _e('Save','woocl');?></option>
									<option value="delete"><?php _e('Delete','woocl');?></option>
								</select>
								<input type="button" name="save" id="woo_cl_apply_button" class="button button-primary" value="Apply">
								<span class="woo_cl_feedback"></span>
								<button class="button woo_cl_add_product" type="button"><?php _e( 'Add Product', 'woocl' )?></button>
							<?php } ?>
						</td>
						<td colspan="2" class="woo_cl_total_price"><?php echo wc_price( $total_price );?></td>
					</tr>
			</tfoot>
		</table>
		<span class="spinner woo_cl_add_product_spinner"></span><?php
		
		$content = ob_get_clean();
		
		return apply_filters( 'woo_cl_get_admin_products_by_collection_id', $content, $coll_id );
	}
	
	/**
	 * Update author in collection items
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.1
	 */
	public function woo_cl_update_author_in_coll_items( $coll_id, $user_id ) {
		
		if( empty( $coll_id ) || empty( $user_id ) ) {
			return false;
		}
		
		$args = array( 'post_parent' => $coll_id );
		
		//Get collection items
		$collection_items	= $this->woo_cl_get_collection_items( $args ); 
		
		if( !empty( $collection_items ) ) {
			foreach ( $collection_items as $collection_item ) {
				
				$items_args = array(
					'ID'          => $collection_item['ID'],
					'post_author' => $user_id
				);
				
				// Update collection post item
				wp_update_post( $items_args );
			}
		}
		
		do_action( 'woo_cl_updated_author_in_coll_items', $coll_id, $user_id );
	}
}