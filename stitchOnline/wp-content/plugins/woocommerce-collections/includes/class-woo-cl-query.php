<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Contains the query functions for WooCommerce Collection which alter the front-end post queries and loops.
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
if ( ! class_exists( 'Woo_Cl_Query' ) ) {
	
	/**
	 * Woo_Cl_Query Class
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	class Woo_Cl_Query {
		
		public $query_vars = array();
		
		/**
		 * Constructor for the query class.
		 * 
		 * @package WPWeb WooCommerce Collections
		 * @since 1.0.0
		 */
		public function __construct() {
			
			//initialize query variable
			$this->woo_cl_init_query_vars();
		}
		
		/**
		 * Init query vars by loading options.
		 * 
		 * @package WPWeb WooCommerce Collections
		 * @since 1.0.0
		 */
		public function woo_cl_init_query_vars() {

			//Get collection view page end point
			$woo_cl_view_slug	= get_option( 'cl_view_collection_text' );
			
			//Get collection edit end point
			$woo_cl_edit_slug	= get_option( 'cl_edit_collection_text' );
			
			//Get user endpoint
			$woo_cl_user_slug	= get_option( 'cl_collection_author_text' );
			
			//Query vars to add to WP
			$this->query_vars	= apply_filters( 'woo_cl_endpoints_query_args', array(
															'view-collection'	=> $woo_cl_view_slug,
															'edit-collection'	=> $woo_cl_edit_slug,
															'collection-author'	=> $woo_cl_user_slug
														)
											);
		}
		
		/**
		 * Add endpoints for query vars
		 * 
		 * @package WPWeb WooCommerce Collections
		 * @since 1.0.0
		 */
		public function woo_cl_add_endpoints() {
			foreach ( $this->query_vars as $key => $var ) {
				add_rewrite_endpoint( $var, EP_ROOT | EP_PAGES );
			}
		}
		
		/**
		 * woo_cl_add_query_vars function.
		 * 
		 * @package WPWeb WooCommerce Collections
		 * @since 1.0.0
		 */
		public function woo_cl_add_query_vars( $vars = array() ) {
			
			foreach ( $this->query_vars as $key => $var ) {
				$vars[] = $key;
			}
			
			return $vars;
		}
		
		/**
		 * Get query vars
		 * 
		 * @package WPWeb WooCommerce Collections
		 * @since 1.0.0
		 */
		public function get_query_vars() {
			
			return $this->query_vars;
		}
		
		/**
		 * Parse the request and look for query vars - endpoints may not be supported
		 * 
		 * @package WPWeb WooCommerce Collections
		 * @since 1.0.0
		 */
		public function woo_cl_parse_request() {
			
			global $wp;
			
			// Map query vars to their keys, or get them if endpoints are not supported
			foreach ( $this->query_vars as $key => $var ) {
				
				if ( isset( $_GET[ $var ] ) ) {
					$wp->query_vars[ $key ] = $_GET[ $var ];
				} elseif ( isset( $wp->query_vars[ $var ] ) ) {
					$wp->query_vars[ $key ] = $wp->query_vars[ $var ];
				}
			}
		}
		
		/**
		 * Adding Hooks
		 * 
		 * @package WPWeb WooCommerce Collections
		 * @since 1.0.0
		 */
		public function add_hooks() {
			
			//call to add_endpoints for rewriting query vars end points
			add_action( 'init', array( $this, 'woo_cl_add_endpoints' ) );
			
			if ( ! is_admin() ) {
				
				//call to woo_cl_add_query_vars for adding quert vars
				add_filter( 'query_vars', array( $this, 'woo_cl_add_query_vars'), 0 );
				
				// call to woo_cl_parse_request for mapping query var to their keys
				add_action( 'parse_request', array( $this, 'woo_cl_parse_request'), 0 );
			}
		}
	}
}