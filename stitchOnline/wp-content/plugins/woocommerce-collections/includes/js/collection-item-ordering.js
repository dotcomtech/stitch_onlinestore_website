jQuery( function( $ ) {

	// Initialize sortable jquery for collection items
	$( 'table.woo_cl_products_disc tbody' ).sortable({
		items: 'tr:not(.inline-edit-row)',
		cursor: 'move',
		handle: 'td.column-drag',
		axis: 'y',
		containment: 'table.woo_cl_products_disc',
		scrollSensitivity: 40,
		helper: function( event, ui ) {
			ui.each( function() {
				$( this ).width( $( this ).width() );
			});
			return ui;
		},
		start: function( event, ui ) {
			ui.placeholder.children().each( function() {
				var $original = ui.item.children().eq( ui.placeholder.children().index( this ) ),
					$this = $( this );

				$.each( $original[0].attributes, function( k, attr ) {
					$this.attr( attr.name, attr.value );
				});
			});
			if ( ! ui.item.hasClass( 'alternate' ) ) {
				ui.item.css( 'background-color', '#ffffff' );
			}
			ui.item.children( 'td, th' ).css( 'border-bottom-width', '0' );
			ui.item.css( 'outline', '1px solid #dfdfdf' );
		},
		stop: function( event, ui ) {
			ui.item.removeAttr( 'style' );
			ui.item.children( 'td,th' ).css( 'border-bottom-width', '1px' );
		},
		update: function( event, ui ) {
			//$( 'table.woo_cl_products_disc tbody th, table.woo_cl_products_disc tbody td' ).css( 'cursor', 'default' );
			$( 'table.woo_cl_products_disc tbody' ).sortable( 'disable' );

			var coll_id    = $( '#post_ID' ).val();
			var postid     = ui.item.find( '.check-column input.woo_cl_coll_item_id' ).val();
			var prevpostid = ui.item.prev().find( '.check-column input.woo_cl_coll_item_id' ).val();
			var nextpostid = ui.item.next().find( '.check-column input.woo_cl_coll_item_id' ).val();

			// Show Spinner
			ui.item.find( '.check-column input[type="checkbox"]' ).hide().after( '<img alt="processing" src="images/wpspin_light.gif" class="waiting" style="margin-left:6px;width:16px;height:16px;" />' );

			// Go do the sorting stuff via ajax
			$.post( ajaxurl, { action: 'woo_cl_collection_item_ordering', coll_id: coll_id, id: postid, previd: prevpostid, nextid: nextpostid }, function( response ) {
				$.each( response, function( key, value ) {
					$( '#inline_' + key + ' .menu_order' ).html( value );
				});
				ui.item.find( '.check-column input' ).show().siblings( 'img' ).remove();
				//$( 'table.woo_cl_products_disc tbody th, table.woo_cl_products_disc tbody td' ).css( 'cursor', 'move' );
				$( 'table.woo_cl_products_disc tbody' ).sortable( 'enable' );
			});

			// fix cell colors
			$( 'table.woo_cl_products_disc tbody tr' ).each( function() {
				var i = $( 'table.woo_cl_products_disc tbody tr' ).index( this );
				if ( i%2 === 0 ) {
					$( this ).addClass( 'alternate' );
				} else {
					$( this ).removeClass( 'alternate' );
				}
			});
		}
	});
});