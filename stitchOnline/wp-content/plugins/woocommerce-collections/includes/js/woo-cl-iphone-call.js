jQuery(document).ready(function($) {	  

	//Call to iphonestyle jquery
	woocl_initialize_iphoneStyle();

	//Click on save button on quick edit
  	$( '.save', $('body.post-type-woocollections #inline-edit') ).click( function() {
  		setTimeout(function() {
			woocl_initialize_iphoneStyle();
		},1000);
	});

	//Function to initialize iphone style
	function woocl_initialize_iphoneStyle() {
		var onchange_checkbox = (jQuery('.woocl_featuredbtn')).iphoneStyle({
		    onChange: function(elem, value) { 
		    	var featured 
		    	if( value == true){
		    		featured = 'yes';
		    	} else{
		    		
		    		featured = 'no';
		    	}    	    	   
		    	var id = jQuery(elem).attr('id');
		    	var res = id.split("_"); 
		    	id = res[2];    	
		    	$.ajax({
		              type: "POST",
		              url: ajax_url,
		              data: 'post_id='+id+'&woo_cl_featured='+featured,
		              success: function(msg) {
			                  
		               }
		        });    	
		    }
		  });
	}
});