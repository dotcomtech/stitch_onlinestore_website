jQuery(document).ready(function($) {
	
	// click on popup close button
	$( document ).on( 'click', '.woocl-close', function() {
		
		$('.woocl-overlay').hide();
		$('.woocl-middle').hide();
		$('.woocl-modal-wrapper').hide();
		$('html').removeClass('woocl-prevent-scroll');
		$('body').removeClass('woocl-prevent-scroll');
	});
	
	// click on success popup close button
	$( document ).on( 'click', '.woocl-closer, .woocl-addbtn', function() {
		
		$('.woocl-addoverlay').hide();
		$('.woocl-imageadd-middlepart').hide();
		$('.woocl-modal-imageadd-inwrapper').hide();
	});
	
	// show add to collection  popup with Ajax
	$(document).on('click', '.show_dropbox, .woocl-add-to-collbtn', function() {

		//Check if add to collection button is disabled
		if( $(this).parent().hasClass( 'woo-same-msg' ) && $(this).parent().hasClass( 'disable' ) ) {
			var message = $(this).parents( 'form.variations_form' ).find( '.single_variation_wrap .woocommerce-variation p' ).text();
			alert( message );
			return false;
		} else if( $(this).parent().hasClass( 'disable' ) ) {
			alert( Woo_cl_Ajax.must_choose_product_option );
			return false;
		}

		coll_productid   = jQuery( this ).siblings( '.woo-cl-product_id' ).val();//Get Product id
		coll_variationid = jQuery( this ).siblings( '.woo-cl-variable_id' ).val();//Get Variation id
		
		var data = {
						action 				:	'show_add_to_collection_content',
						coll_productid		:	coll_productid,
						coll_variationid	:	coll_variationid,
					};
		//ajax call to save data to database
		$( '.woocl-inner' ).html( '' );
		
		$('.woocl-middle').show();
		$('.woocl-overlay').show();
		$('.woocl-modal-wrapper').show();
		$('html').addClass('woocl-prevent-scroll');
		$('body').addClass('woocl-prevent-scroll');
		
		//show loader while process is running
		$('#woo_cl_popup_loader').show();
		
		$.post(Woo_cl_Ajax.ajaxurl,data,function(response) {
			
			var response_data = JSON.parse(response);

			if( response_data.success ) {
				
				$( '.woocl-inner' ).html( response_data.html );
				
				//hide loader after process completed
				$('#woo_cl_popup_loader').hide();
				woocl_collection_dropdown();

				//Add trigger for after add to collection popup load
				$( document ).trigger( "show_add_to_collection_popup_load_after", data, response_data );

			} else if( response_data.error ) {
				
			}
		});
	});

	//Call when press ESC
	$(document).keydown(function(e) {
    // ESCAPE key pressed
	    if (e.keyCode == 27) {
	        $('.woocl-close, .woocl-closer').click();
	    }
	});

	//click on Create collection button in popup
	$( document ).on( 'click', '#woo-cl-createbtn', function() {

		//Remove collection select error message		
		$( '#woo-cl-atc-msg' ).removeClass( 'woocl-error woocl-success' ).html('');

		var collname = $('#woocl-colwname').val();
		var collprivacy = $("#woo-cl-coll_privacy option:selected").val();

		if( collname == '' || collname == Woo_cl_Ajax.coll_title_hint || collname == undefined || collname == false ) { //check if null
			
			$( '#woo-cl-create-msg' ).removeClass( 'woocl-success' ).addClass( 'woocl-error' ).html( Woo_cl_Ajax.coll_empty_error ).show();
			$( '#woocl-colwname' ).val('');
			
			return;
		}
		
		var data = {
						action 		:	'woo_cl_create_collection',
						collname	:	collname,
						collprivacy :   collprivacy
					};
		//ajax call to save data to database
		
		//hide text & show loader while process is running
		$( '#woo-cl-createbtn' ).hide();
		$( '.woo_cl_loader' ).show();

		$.post(Woo_cl_Ajax.ajaxurl,data,function(response) {
			
			//show text & hide loader when process is end
			$( '.woo_cl_loader' ).hide();
			$( '#woo-cl-createbtn' ).show();
			
			var response_data = JSON.parse(response);
			
			if( response_data.success ) {
				
				var newoption = '<option value=' + response_data.collid + ' selected>' + response_data.colltitle + '</option>';
				
				$( "#woo-cl-coll_list" ).append(newoption);
				woocl_collection_dropdown();
				$( '#woo-cl-create-msg' ).removeClass( 'woocl-error' ).addClass( 'woocl-success' ).html( response_data.success ).show();
				$( '#woocl-colwname' ).val('');
				
			} else if( response_data.error ) {
				
				$( '#woo-cl-create-msg' ).removeClass( 'woocl-success' ).addClass( 'woocl-error' ).html( response_data.error ).show();
				$( '#woocl-colwname' ).val('');
			}
		});
	});
	
	//click on Add item to collection button
	$( document ).on( 'click', '#woo-cl-addtocoll', function() {

		//Remove Create collection error message		
		$( '#woo-cl-create-msg' ).removeClass( 'woocl-error woocl-success' ).html('');

		var coll_id 		= $('#woo-cl-coll_list').val();
		var collitem_disc 	= $('#woo-cl-coll_disc').val();
		var coll_productid 	= $('.woo-cl-collproduct_id').val();
		var form 			= $('.variations');
		var variations		= {};
		
		if( coll_id == '' || coll_id == undefined || coll_id == false ) {
			
			$( '#woo-cl-atc-msg' ).removeClass( 'woocl-success' ).addClass( 'woocl-error' ).html( Woo_cl_Ajax.coll_selection_error ).show();
			//$( '#woocl-colwname' ).val('');
			
			return;
		}
		
		if( form.length != 0 ) {
			form.find('select').each(function(){
				
				if($(this).attr('name').indexOf('attribute_') > -1){
					variations[$(this).attr('data-attribute_name')] = $(this).val();
				}
			});
		}
		
		var data = { 
						action			:	'woo_cl_add_product_to_collection',
						coll_id			:	coll_id,
						collitem_disc	:	collitem_disc,
						coll_productid	:	coll_productid,
						variations		:	variations
					};
					
		//Add trigger for after item details popup load
		$( document ).trigger( "woocl_add_to_collection_ajax_before", [data] );
		
		//ajax call to save data to database
		
		//hide add to collection button while process is running
		$('#woo-cl-addtocoll').hide();
		
		//show loader while process is running
		$('#woo_cl_add_coll_loader').show();
		
		//show adding text while process is running
		$('.woocl_adding_msg').show();

		$.post(Woo_cl_Ajax.ajaxurl,data,function(response) {
			
			//hide loader after process completed
			$('#woo_cl_add_coll_loader').hide();
			
			//hide adding text when process is completed
			$('.woocl_adding_msg').hide();
			
			//show add to collection button when process is completed
			$('#woo-cl-addtocoll').show();

			var response_data = JSON.parse(response);
			
			if( response_data.success ) {
				
				//make all fields to null
				$('#woo-cl-coll_disc').val('');
				$('.woocl-overlay').hide();
				$('.woocl-middle').hide(); 
				$('.woocl-modal-wrapper').hide();
				$('html').removeClass('woocl-prevent-scroll');
				$('body').removeClass('woocl-prevent-scroll');
				
				$( '.woocl-innerdata' ).html( response_data.successhtml );
				
				$('.woocl-imageadd-middlepart').show();
				$('.woocl-addoverlay').show();
				$('.woocl-modal-imageadd-inwrapper').show();
			} else if( response_data.error ) {
				$( '#woo-cl-create-msg' ).removeClass( 'woocl-success' ).addClass( 'woocl-error' ).html( response_data.error ).show();
			}
		});
	});

	//clcik on image to  show item details popup with Ajax
	$(document).on('click', '.woocl-click-image', function() {
		
		//Get collection item block
		var coll_item_block	= jQuery( this ).parents( '.woocl-item-block' );
		
		//get collection item id
		var coll_item_id	= coll_item_block.find( '.woocl-coll-item-id' ).val();
		
		//get product id
		var product_id	= coll_item_block.find( '.woo-cl-product_id' ).val();
		
		// Collection ID
		var woo_cl_coll_id			= $('.woo-cl-collection-id').val();
		
		var data = {
						action 			:	'woo_cl_coll_item_details',
						coll_item_id	:	coll_item_id,
						product_id		:	product_id,
						coll_id			: 	woo_cl_coll_id
					};
		//ajax call to save data to database
		$( '.woocl-inner' ).html( '' );
		
		$('.woocl-middle').show();
		$('.woocl-overlay').show();
		$('.woocl-modal-wrapper').show();
		$('html').addClass('woocl-prevent-scroll');
		$('body').addClass('woocl-prevent-scroll');
		
		//show loader while process is running
		$('#woo_cl_popup_loader').show();
		
		$.post(Woo_cl_Ajax.ajaxurl,data,function(response) {
			
			var response_data = JSON.parse(response);
			
			if( response_data.success ) {

				$( '.woocl-inner' ).html( response_data.html );
		
				//hide loader after process completed
				$('#woo_cl_popup_loader').hide();
			} else if( response_data.error ) {
				
			}

			//Add trigger for after item details popup load
			$( document ).trigger( "woocl_itemdetail_popup_load_after", data, response_data );
		});
	});
	
	//click on add to cart link with Ajax
	$(document).on('click', '#woocl-add-to-cart', function() {
		
		var main_wrap	= $( this ).parents( '.woocl-summary-cart' );
		
		//get variation id
		var product_id	= main_wrap.find('.woo-cl-product_id').val();
		
		//Get product id
		var variation_id	= main_wrap.find('.woo-cl-variarion_id').val();
		var variation_data	= main_wrap.find('.woo-cl-variarion_data').val();

		//add to cart message clear
		$( '.woo_cl_add_cart_message' ).html( '' );
		
		//hide cart page link
		$( '.woocl-view-cart' ).hide();
		
		var data = {
						action 			:	'woo_cl_add_to_cart',
						product_id		:	product_id,
						variation_id	:	variation_id,
						variation_data	:	variation_data,
					};
		//ajax call to save data to database
		
		//show loader while process is running
		$('.woo_cl_add_cart_loader').show();
		
		$.post(Woo_cl_Ajax.ajaxurl,data,function(response) {
			
			var response_data = JSON.parse(response);
			
			//hide loader when process is completed
			$('.woo_cl_add_cart_loader').hide();
			$('.woo-cl-cart-success').removeClass( 'fa fa-check' );
			
			if( response_data.success ) {
				
				//show cart page link
				$('.woocl-view-cart').show();
				$('.woo-cl-cart-success').addClass( 'fa fa-check' );
				$( '.woo_cl_add_cart_message' ).removeClass( 'woocl-error' ).addClass( 'woocl-success' ).html( Woo_cl_Ajax.add_cart_success ).show();
				//$( '.woo_cl_add_cart_message' ).removeClass( 'woocl-error' ).addClass( 'woocl-success' ).html( response_data.message ).show();
				
			} else if( response_data.error ) {
				
				//$( '.woo_cl_add_cart_message' ).removeClass( 'woocl-success' ).addClass( 'woocl-error' ).html( Woo_cl_Ajax.add_cart_error ).show();
				$( '.woo_cl_add_cart_message' ).removeClass( 'woocl-success' ).addClass( 'woocl-error' ).html( response_data.message ).show();
			}
		});
	});
	
	//click on cancel or close button for product details popup
	$(document).on('click', '.woocl-close , .woocl-overlay', function() {
		
		$('.woocl-overlay').hide();
		$('.woocl-middle').hide(); 
		$('.woocl-modal-wrapper').hide();
		$('html').removeClass('woocl-prevent-scroll');
		$('body').removeClass('woocl-prevent-scroll');
		
	});
	
	// click on share via email button link
	$(document).on('click', '.woocl-email-btn', function() {

		$('.woocl-share-middle').show();
		$('.woocl-overlay').show();
		$('.woocl-modal-share-wrapper').show();
		$('html').addClass('woocl-prevent-scroll');
		$('body').addClass('woocl-prevent-scroll');
		
	});
	
	//click on Send Now button for email share with ajax
	$( document ).on( 'click', '#woocl-email-share-btn', function() {
		//alert(1);
		//get all details of email
		var coll_id  	  = $('#woocl-coll-id').val();
		var sender_name   = $('#woocl-your-name').val();
		var sender_email  = $('#woocl-your-emailadd').val();
		var friend_emails = $('#woocl-friend-emailadd').val();
		var message 	  = $('#woocl-your-message').val();
		
		var is_error = false;
		
		$( '.woocl-sendername-msg' ).hide();
		$( '.woocl-senderemail-msg' ).hide();		
		$( '.woocl-friendemail-msg' ).hide();		
		
		if( sender_name == '' || sender_name == undefined || sender_name == false ) { //check if sender name null
			
			$( '.woocl-sendername-msg' ).show();

			$( '#woocl-your-name' ).val('');

			is_error = true;
			//return;
		}
		
		if( woocl_validate_email( sender_email ) ) {//validate sender email
			is_error = true;
			//return;
		}
		if( woocl_validate_sep_email( friend_emails ) ) { //validate friends email
			is_error = true;
			//return;
		}
		
		if( is_error ) {//check if error then return
			return;
		}

		var data = { 
						action 			:	'woo_cl_coll_item_email_share',
						coll_id			:	coll_id,
						sender_name		:	sender_name,
						sender_email	:	sender_email,
						friend_emails	:	friend_emails,
						message			:	message
					};
		//ajax call to save data to database
		
		//hide send now button while process is running
		$('#woocl-email-share-btn').hide();
		
		//show loader while process is running
		$('.woo_cl_email_share_loader').show();
			
		$.post(Woo_cl_Ajax.ajaxurl,data,function(response) {
			
			//hide loader after process completed
			$('.woo_cl_email_share_loader').hide();
			
			//show send now button when process is completed
			$('#woocl-email-share-btn').show();
			
			var response_data = JSON.parse(response);
			
			if( response_data.success ) {

				//Clean all fields				
				$( '#woocl-coll-id, #woocl-your-name, #woocl-your-emailadd, #woocl-friend-emailadd, #woocl-your-message' ).val( '' );

				$('.woocl-overlay').hide();
				$('.woocl-share-middle').hide(); 
				$('.woocl-modal-share-wrapper').hide();
				$('html').removeClass('woocl-prevent-scroll');
				$('body').removeClass('woocl-prevent-scroll');	

				$('.woocl-success-middle').show();
				$('.woocl-overlay').show();
				$('.woocl-modal-email-success-wrapper').show();
				$('html').addClass('woocl-prevent-scroll');
				$('body').addClass('woocl-prevent-scroll');
							
			} else if( response_data.error ) {

				$('.woocl-overlay').hide();
				$('.woocl-share-middle').hide(); 
				$('.woocl-modal-share-wrapper').hide();
				$('html').removeClass('woocl-prevent-scroll');
				$('body').removeClass('woocl-prevent-scroll');	
																
			}
			
		});
	});
	
	//click on share via email popup cancel or close button
	$(document).on('click', '.woocl-close, .woocl-success-btn, .woocl-overlay', function() {
		
		$('.woocl-overlay').hide();
		$('.woocl-share-middle').hide(); 
		$('.woocl-modal-share-wrapper').hide();
		$('html').removeClass('woocl-prevent-scroll');
		$('body').removeClass('woocl-prevent-scroll');

		$('.woocl-success-middle').hide(); 
		$('.woocl-modal-email-success-wrapper').hide();
		$('html').removeClass('woocl-prevent-scroll');
		$('body').removeClass('woocl-prevent-scroll');
		
	});	
	
	//on blur for sender name
	$(document).on('blur', '#woocl-your-name', function() {
	
		//Get sender name
		var sender_name   = jQuery( this ).val();

		if( sender_name == '' || sender_name == undefined || sender_name == false ) { //check if sender name null
			
			$( '.woocl-sendername-msg' ).show();
			return;
			
		} else {
			
			$( '.woocl-sendername-msg' ).hide();			
			return;
		}
	});

	//on blur for sender email address
	$(document).on('blur', '#woocl-your-emailadd', function() {
		woocl_validate_email( jQuery( this ).val() );
	});

	//on blur for friends email address
	$(document).on('blur', '#woocl-friend-emailadd', function() {
		woocl_validate_sep_email( jQuery( this ).val() );
	});

});

//function that validate comma sep emails
function woocl_validate_sep_email( friend_emails ) {

	var notvalid = true; //default true
	
	var email_pattern  = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

	var emailarray 	   = friend_emails.split(",");

	jQuery.each(emailarray,function(i){
		
		if( emailarray[i] == '' || emailarray[i] == undefined || emailarray[i] == false ) { //check if sender email null
			
			jQuery( '.woocl-friendemail-msg p.woo_cl_message' ).html( Woo_cl_Ajax.email_fri_error );
			jQuery( '.woocl-friendemail-msg' ).show();
			notvalid = true; //email not valid so true
			return false;
			
		} else if( !email_pattern.test( emailarray[i] ) ) {//check if sender email valid
			
			jQuery( '.woocl-friendemail-msg p.woo_cl_message' ).html( Woo_cl_Ajax.email_fri_valid_error );
			jQuery( '.woocl-friendemail-msg' ).show();			
			notvalid = true; //email not valid so true
			return false;
		} else {

			jQuery( '.woocl-friendemail-msg' ).hide();
			notvalid = false;//email valid so false
			return false;
		}
	});	
	return notvalid;
}

//function that validate email
function woocl_validate_email( sender_email ) {

	var email_pattern  = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

	if( sender_email == '' || sender_email == undefined || sender_email == false ) { //check if sender email null
		
		jQuery( '.woocl-senderemail-msg p.woo_cl_message' ).html( Woo_cl_Ajax.email_error );
		jQuery( '.woocl-senderemail-msg' ).show();
		return true;
		
	} else if( !email_pattern.test( sender_email ) ) {//check if sender email valid
		
		jQuery( '.woocl-senderemail-msg p.woo_cl_message' ).html( Woo_cl_Ajax.email_valid_error );
		jQuery( '.woocl-senderemail-msg' ).show();			
		return true;
	} else {
		
		jQuery( '.woocl-senderemail-msg' ).hide();			
		return false;
	}
}

// function for collection dropdown
function woocl_collection_dropdown() {
	
	jQuery('.woocl-custom-options').selectric({
		optionsItemBuilder: function(itemData, element, index){
			
			var private_cls = '';
			var private_ele = element.attr('data-attr');
			
			if( typeof(private_ele) != 'undefined' && private_ele == 'prv' ) {
				private_cls = 'ico-private';
			}
			
			return element.val().length ?  itemData.text+'<span class=" ico-' + element.val() + ' ' + private_cls + '"></span>'  : itemData.text;
		}
	});
}