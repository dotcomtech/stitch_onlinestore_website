jQuery( document ).ready( function( $ ) {
	
	//mouseout event
	$( ".woocl-hoverbox" ).mouseleave( function() {
	  	$( '.woocl-hoverbox' ).hide();
	});
	
	//click on Item delte buton
	$( document ).on( 'click', '.woocl-delete-button', function() {
		
		//console.log($(this).closest(".woocl-hoverbox"));
		$( this ).parent().find( '.woocl-hoverbox' ).fadeIn();
	});
	
	// click on collection item delete cancel or close button
	$( document ).on( 'click', '.woocl-cancelbtn , .woocl-hoverbox', function() {
		
		$( '.woocl-hoverbox' ).hide();
		
		$( 'html' ).removeClass( 'woocl-prevent-scroll' );
		$( 'body' ).removeClass( 'woocl-prevent-scroll' );
	});
	
	// delete collection item ajax call
	$( document ).on( 'click', '.woocl-remove-coll-item', function() {
		
		//Get collection item block
		var coll_item_block	= jQuery( this ).parents( '.woocl-item-block' );
		
		//get collection item id
		var coll_item_id	= coll_item_block.find( '.woocl-coll-item-id' ).val();
		
		var data = { 
						action 			:	'woo_cl_collection_item_delete',
						coll_item_id	:	coll_item_id,
					};
		
		//ajax call to save data to database
		$.post( Woo_cl_Public_Ajax.ajaxurl, data, function( response ) {
			
			var response_data = JSON.parse( response );
			
			if( response_data.success ) {
				
				coll_item_block.find('.woocl-delete-success').fadeIn();
				
			} else if( response_data.error ) {
				

			}
			
		});	
		
	});
	 
    // click on Collection delete link   
	$(document).on('click', '.woocl-delete', function() {
		
		$('.woocl-delete-middlepart').show();
		$('.woocl-overlay').show();
		$('.woocl-modal-delete-inwrapper').show();
		$('html').addClass('woocl-prevent-scroll');
		$('body').addClass('woocl-prevent-scroll');
		
	});

	// click on collection delete popup cancel button or close button
	$(document).on('click', '.woocl-closer,.woocl-cancel , .woocl-overlay', function() {
		
		$('.woocl-overlay').hide();
		$('.woocl-delete-middlepart').hide(); 
		$('.woocl-modal-delete-inwrapper').hide();
		$('html').removeClass('woocl-prevent-scroll');
		$('body').removeClass('woocl-prevent-scroll');
		
	});	

	// click on "yes,delete" button ajax call to remove item
 	$(document).on('click', '#woocl-coll-delete', function() {

		//get collection id
		var coll_id = $('.woocl-coll_id').val();

		//hide text & show loader while process is running
		$( '#woocl-coll-delete' ).hide();
		$( '.woo_cl_loader' ).show();

		var data = { 
						action 		:	'woo_cl_collection_delete',
						coll_id		:	coll_id,
					};
		//ajax call to save data to database
		
		$.post(Woo_cl_Public_Ajax.ajaxurl,data,function(response) {
			
			var response_data = JSON.parse(response);

			//show text & hide loader when process is end
			$( '.woo_cl_loader' ).hide();
			$( '#woocl-coll-delete' ).show();

			if( response_data.success ) {
				
				$('.woocl-delete-middlepart').show();
				$('.woocl-overlay').show();
				$('.woocl-modal-delete-inwrapper').show();
				$('html').addClass('woocl-prevent-scroll');
				$('body').addClass('woocl-prevent-scroll');
				
				// redirect page on collection list
				window.location.replace( response_data.successurl );
				
			} else if( response_data.error ) {
				

			}
			
		});	
		
	});
	
	//click on Make cover image button
	$(document).on('click', '.woocl-coverimage', function() {
		
		//get product url
		var product_imgurl  = jQuery( this ).siblings( '.woocl-product-imgurl' ).val();
		
		//get collectoion item id
		var coll_item_id 	= jQuery( this ).siblings( '.woocl-coll-item-id' ).val();
		
		//get item product id
		var product_id 		= jQuery( this ).siblings( '.woo-cl-product_id' ).val();
		
		// replace hidden & img srg values
		$( '.woocl-coverimg' ).attr( 'src', product_imgurl );
		$( '.woocl-product-id' ).attr( 'value', product_id );
		$( '.woocl-coll_item_id' ).attr( 'value', coll_item_id );
		
		$( '.woocl-image-middlepart' ).show();
		$( '.woocl-overlay' ).show();
		$( '.woocl-modal-image-inwrapper' ).show();
		$( 'html' ).addClass( 'woocl-prevent-scroll' );
		$( 'body' ).addClass( 'woocl-prevent-scroll' );
	});
	
	//click on Make cover popup  Cancel or close button
	$( document ).on( 'click', '.woocl-closer,.woocl-cancel , .woocl-overlay', function() {
		
		$( '.woocl-overlay' ).hide();
		$( '.woocl-image-middlepart' ).hide(); 
		$( '.woocl-modal-image-inwrapper' ).hide();
		$( 'html' ).removeClass( 'woocl-prevent-scroll' );
		$( 'body' ).removeClass( 'woocl-prevent-scroll' );
	});
	
	//Change collection Cover Image ajax call
	$( document ).on('click', '#woocl-coll-cover-img', function() {
		
		//Get collection id, product id
		var coll_id			= $( '.woocl-coll-id' ).val();
		var product_id		= $( '.woocl-product-id' ).val();
		var coll_item_id	= $( '.woocl-coll_item_id' ).val();
		
		//hide text & show loader while process is running
		$( '#woocl-coll-cover-img' ).hide();
		$( '.woo_cl_loader' ).show();
		
		var data = { 
						action			: 'woo_cl_collection_coverimg_update',
						coll_id			: coll_id,
						product_id		: product_id,
						coll_item_id	: coll_item_id
					};
		
		//ajax call to save data to database
		$.post( Woo_cl_Public_Ajax.ajaxurl, data, function( response ) {
			
			var response_data = JSON.parse( response );
			
			//show text & hide loader when process is end
			$( '.woo_cl_loader' ).hide();
			$( '#woocl-coll-cover-img' ).show();
			
			if( response_data.success ) {
				
				$( '.woocl-overlay' ).hide();
				$( '.woocl-image-middlepart' ).hide();
				$( '.woocl-modal-image-inwrapper' ).hide();
				$( 'html' ).removeClass( 'woocl-prevent-scroll' );
				$( 'body' ).removeClass( 'woocl-prevent-scroll' );
				
				location.reload();
				
			} else if( response_data.error ) {
				
			}
		});
	});
	
	// clcik on make collection public / private link
	$(document).on('click', '.woocl-pr-text', function() {
		
		$( '.woocl-middlepart' ).show();
		$( '.woocl-overlay' ).show();
		$( '.woocl-modal-inwrapper' ).show();
		$( 'html' ).addClass( 'woocl-prevent-scroll' );
		$( 'body' ).addClass( 'woocl-prevent-scroll' );
	});
	
	//click on make collection public / private popup cancel or close button
	$( document ).on( 'click', '.woocl-closer,.woocl-cancel , .overlay', function() {
		
		$( '.overlay' ).hide();
		$( '.woocl-middlepart' ).hide(); 
		$( '.woocl-modal-inwrapper' ).hide();
		$( 'html' ).removeClass( 'woocl-prevent-scroll' );
		$( 'body' ).removeClass( 'woocl-prevent-scroll' );
	});
	
	//Change collection Status Private / public ajax call
	$(document).on('click', '#woocl-coll-status', function() {
		
		//Get collection id, status
		var coll_id		= $( '.woocl-coll-id' ).val();
		var coll_status	= $( '.woocl-coll-status' ).val();
		
		//hide text & show loader while process is running
		$( '#woocl-coll-status' ).hide();
		$( '.woo_cl_loader' ).show();
		
		var data = { 
						action		: 'woo_cl_collection_status_update',
						coll_id		: coll_id,
						coll_status	: coll_status,
					};
		//ajax call to save data to database
		$.post( Woo_cl_Public_Ajax.ajaxurl, data, function( response ) {
			
			var response_data = JSON.parse( response );
			
			//show text & hide loader when process is end
			$( '.woo_cl_loader' ).hide();
			$( '#woocl-coll-status' ).show();
			
			if( response_data.success ) {
				//redirect page
				window.location.replace( response_data.successurl );
			} else if( response_data.error ) {
				
			}
		});
	});
	
	//Update collection details with Ajax and make redirection
	$(document).on('click', '#woocl-update-collection', function() {

		var is_tinyMCE_active = false;
		if (typeof(tinyMCE) != "undefined") {
			is_tinyMCE_active = true;	
		}
		if(is_tinyMCE_active) {
			var content;
		    var inputid = 'woocl_coll_desc';
		    var editor = tinyMCE.get(inputid);
		    var textArea = jQuery('textarea#' + inputid);    
		    if (textArea.length>0 && textArea.is(':visible')) {
		        content = textArea.val();        
		    } else {
		        content = editor.getContent();
		    } 

		    $("textarea#woocl_coll_desc").val(content);
		}
		//Get collection form data
		var coll_data			= $( '.woocl-edit-form' ).serialize();
		
		var data = { 
						action 			:	'woo_cl_collection_update',
						coll_data		:	coll_data,
					};
		//ajax call to save data to database
		
		// hide done edit button while process running
		$('#woocl-update-collection').hide();
		
		//show loader while process is running
		$('#woo_cl_update_loader').show();
		
		//show adding text while process is running
		$('.woo_cl_update_msg').show();
		
		$.post(Woo_cl_Public_Ajax.ajaxurl,data,function(response) {

			//hide loader after process completed
			$('#woo_cl_update_loader').hide();
			
			//hide adding text when process is completed
			$('.woo_cl_update_msg').hide();
			
			//show add to done edit button when process is completed
			$('#woocl-update-collection').show();
			
			var response_data = JSON.parse(response);
			
			if( response_data.success ) {
					
				$('.woocl-overlay').hide();
				$('.woocl-image-middlepart').hide(); 
				$('.woocl-modal-image-inwrapper').hide();
				$('html').removeClass('woocl-prevent-scroll');
				$('body').removeClass('woocl-prevent-scroll');
				
				// redirect page
				window.location.replace( response_data.successurl );
				
			} else if( response_data.error ) {
				

			}
			
		});	
		
	});
	
	//Click on load more button for collection
	$(document).on('click', '.woocl_load_more', function() {
		
		//Hide Load more button & show loader while processing
		jQuery('.woocl_load_more').hide();
		jQuery('.woo_cl_loader').show();

		var current_page		= jQuery('.woocl_load_more').attr('data-pagi');
			
		if( jQuery('.no-collection-loop').is(":visible") ) {
	
			//jQuery('.woocl_load_more_wrap').hide(); //load more wrap disabled when no any collection
			return false;
		}
		
		woocl_call_ajax_collection( '.woocl_load_more', current_page );
		
	});
	
	//jQuery(".entry-content").append('<div data-pagi = "2" id="woo-collection-loop-loader"></div>');
	$("#woo-collection-loop-loader").bind("inview",function(event,visible){
    	
    	if(visible == true){
    		
    		jQuery('#woo-collection-loop-loader img').show();
			var current_page		= jQuery('#woo-collection-loop-loader').attr('data-pagi');
			
			if( jQuery('.no-collection-loop').is(":visible") ) {
		
				jQuery('#woo-collection-loop-loader img').hide();
				return false;
			}
			
			woocl_call_ajax_collection( '#woo-collection-loop-loader', current_page );
    	}
    });
	
	//Click on load more button for collection items
	$(document).on('click', '.woocl_load_more_item', function() {
		
		var is_ajax				= jQuery('.woocl_load_more_item').attr('data-is_ajax');
		if( is_ajax == 2 ){
			
			return false;    			
		}
		jQuery('.woocl_load_more_item').attr('data-is_ajax','2');

		//Hide Load more button & show loader while processing
		jQuery('.woocl_load_more_item').hide();
		jQuery('.woo_cl_loader').show();

		var current_page		= jQuery('.woocl_load_more_item').attr('data-pagi');
		var woocl_coll_id		= jQuery('#woocl-coll-id').val();
		
		if( jQuery('.no-collection-loop').is(":visible") ) {
			
			//jQuery('.woocl_load_more_wrap').hide(); //load more wrap disabled when no any collection
			return false;
		}
		
		woocl_call_ajax_collection_items( '.woocl_load_more_item', woocl_coll_id, current_page );		
	});
	  
	//jQuery(".entry-content").append('<div data-pagi = "2" id="woo-collection-item-loop-loader"></div>');
	$("#woo-collection-item-loop-loader").bind("inview",function(event,visible){
    	
    	if( visible == true ){
    		
    		var is_ajax				= jQuery('#woo-collection-item-loop-loader').attr('data-is_ajax');
    		if( is_ajax == 2 ){
    			
    			return false;    			
    		}
    		jQuery('#woo-collection-item-loop-loader').attr('data-is_ajax','2');
    		
    		jQuery('#woo-collection-item-loop-loader img').show();
			var current_page		= jQuery('#woo-collection-item-loop-loader').attr('data-pagi');
			var woocl_coll_id		= jQuery('#woocl-coll-id').val();
			
			if( jQuery('.no-collection-loop').is(":visible") ) {
				
				jQuery('#woo-collection-item-loop-loader img').hide();
				return false;
			}
			
			woocl_call_ajax_collection_items( '#woo-collection-item-loop-loader', woocl_coll_id, current_page );
    	}
    });

	//Change sorting options
	$(document).on( 'change', '#woocl-coll-sorting', function() {
		this.form.submit();
	});

    //function for call ajax to collection pagination
	function woocl_call_ajax_collection( load_more_wrap, current_page ) {

		//Get Author of collections
		var data_author = $( load_more_wrap ).attr( 'data-author' );

		//Get Search and sort field value
		var search 	= jQuery( '#woocl-coll-item_search' ).val();
		var order 	= jQuery( '#woocl-coll-sorting' ).val();
		var cat_ids = jQuery( '#woocl-categories' ).val();
		
		var data = { 
						action			:	'woo_cl_get_loop_content',
						current_page	:	current_page,
						data_author		:	data_author,
						sort			:	order,
						search			:	search,
						cat_ids			:	cat_ids,
					};
		
		jQuery.post(Woo_cl_Public_Ajax.ajaxurl,data,function(response) {

			//Show Load more button & Hide loader while processing
			jQuery('.woo_cl_loader').hide();
			jQuery( load_more_wrap + ' img' ).hide();
			jQuery('.woocl_load_more').show();

			var no_collection_html = jQuery('.no-collection-loop').html();
			jQuery(".woocl-collections-gallery").append(response);
			
			if( no_collection_html != 'No Collection Found.' ) {
				jQuery(load_more_wrap).attr('data-pagi',parseInt(current_page)+1);
			}
			
			var current_page_check		= jQuery(load_more_wrap).attr( 'data-pagi' );
			
			if( current_page_check >= 1 ) {
				jQuery('.no-collection-loop').html('');
			}
			
			//Add No More Collection Button
			var woocl_total_pages	= jQuery('.woocl-total-pages').val();
			var woocl_curr_page		= jQuery('.woocl-curr-page').val();
			
			if( woocl_total_pages == woocl_curr_page ) {
				
				jQuery( '.woocl-collections-gallery' ).append( '<div class="no-more-collection">'+Woo_cl_Public_Ajax.no_more_coll+'</div>' );

				//load more wrap disabled when no any collection
				jQuery( '.woocl_load_more_wrap' ).hide();

				jQuery( ".no-more-collection" ).fadeOut( 2000, function() {
					jQuery( '.no-more-collection' ).remove();
				});
			}
			woocl_curr_page++;
			jQuery('.woocl-curr-page').val( woocl_curr_page );

			woocl_resize_layout();//Resize collection layout
	    });
	}
	
    //function for call ajax to collection items pagination
	function woocl_call_ajax_collection_items( load_more_wrap, woocl_coll_id, current_page ) {

		//Get Search and sort field value
		var search 	= jQuery( '#woocl-coll-item_search' ).val();
		var order 	= jQuery( '#woocl-coll-sorting' ).val();

		var data = { 
						action			:	'woo_cl_item_get_loop_content',
						current_page	:	current_page,
						woocl_coll_id	:	woocl_coll_id,
						sort			:	order,
						search			:	search,
					};
		
		jQuery.post(Woo_cl_Public_Ajax.ajaxurl,data,function(response) {
			
			//Show Load more button & Hide loader while processing
			jQuery('.woo_cl_loader').hide();
			jQuery( load_more_wrap + ' img' ).hide();
			jQuery('.woocl_load_more_item').show();

			jQuery(load_more_wrap).attr('data-is_ajax','1');
			var no_collection_html = jQuery('.no-collection-loop').html();
			jQuery(".woocl-collections").append(response);
			
			if( no_collection_html != 'No Collection Item Found.' ) {
				jQuery(load_more_wrap).attr('data-pagi',parseInt(current_page)+1);
			}
			
			var current_page_check		= jQuery(load_more_wrap).attr( 'data-pagi' );
			
			if( current_page_check >= 1 ) {
	    		
				jQuery('.no-collection-loop').html('');
			}

			// Change button value on show more products
			if($('.woocl-coll-item-checkbox').length == $('.woocl-coll-item-checkbox:checkbox:checked').length) {
				$('#woo_cl_chk_unchk_btn').html(Woo_cl_Public_Ajax.unselect_all_chk);				
			} else {
				$('#woo_cl_chk_unchk_btn').html(Woo_cl_Public_Ajax.select_all_chk);
			}
			
			//Add No More Collection Button
			var woocl_item_total_pages	= jQuery('.woocl-item-total-pages').val();
			var woocl_item_curr_page	= jQuery('.woocl-item-curr-page').val();
			
			if( woocl_item_total_pages == woocl_item_curr_page ) {
				
				jQuery( '.woocl-collections' ).append( '<div class="no-more-coll-item">'+Woo_cl_Public_Ajax.no_more_coll_item+'</div>' );

				//load more wrap disabled when no any collection items
				jQuery( '.woocl_load_more_wrap' ).hide();

				jQuery( ".no-more-coll-item" ).fadeOut( 2000, function() {
					jQuery( '.no-more-coll-item' ).remove();
				});
			}
			woocl_item_curr_page++;
			jQuery('.woocl-item-curr-page').val( woocl_item_curr_page );
			
			woocl_resize_layout();//Resize collection layout
	    });
	}
	
	$( function() {
		woocl_resize_layout();//Resize collection layout
	});
	
	$( window ).resize(function () {
		woocl_resize_layout();//Resize collection layout
	});

	/* Function for resized collection layout */
	function woocl_resize_layout() {

		var width	= $( ".woocl-cv-body" ).width();
		$( ".woocl-cv-body" ).css( "height", width );
		
		var width1	= $( ".woocl-cv-imgwrp" ).width();
		$( ".woocl-cv-imgwrp" ).css( "height", width1 );
		
		var product_image	= $( ".pro-col-3" ).width();
		$( ".pro-col-3" ).css( "height", product_image );
		$( ".woocl-coll_img1" ).css( "height", product_image );
		
		var product_image1	= $( ".woocl-click-image" ).width();
		$( ".woocl-click-image" ).css( "height", product_image1 );
	}

	/*** Select All / Unselect All button for selection items ***/
	if($('.woocl-coll-item-checkbox').length == $('.woocl-coll-item-checkbox:checkbox:checked').length) {
		$('#woo_cl_chk_unchk_btn').html(Woo_cl_Public_Ajax.unselect_all_chk);				
	} else {
		$('#woo_cl_chk_unchk_btn').html(Woo_cl_Public_Ajax.select_all_chk);
	}

	if($('.woocl-coll-item-checkbox:checked').length <= 0) {
		$('#woo_cl_add_to_cart_btn').addClass('woo-cl-disabled');
	} else {
		$('#woo_cl_add_to_cart_btn').removeClass('woo-cl-disabled');
	}

	// call action on change checkbox selection
	$(document).on('change', '.woocl-coll-item-checkbox', function(){

		if($('.woocl-coll-item-checkbox:checked').length <= 0) {

			$('#woo_cl_chk_unchk_btn').html(Woo_cl_Public_Ajax.select_all_chk);
			$('#woo_cl_add_to_cart_btn').addClass('woo-cl-disabled');

		} else if ($('.woocl-coll-item-checkbox').length == $('.woocl-coll-item-checkbox:checked').length) {

			$('#woo_cl_chk_unchk_btn').html(Woo_cl_Public_Ajax.unselect_all_chk);

		} else {
			$('#woo_cl_chk_unchk_btn').html(Woo_cl_Public_Ajax.select_all_chk);
			$('#woo_cl_add_to_cart_btn').removeClass('woo-cl-disabled');
		}
	});

	// Button action for check / uncheck all checkboxes 
	$('#woo_cl_chk_unchk_btn').on('click', function(){

		if($('.woocl-coll-item-checkbox').length == $('.woocl-coll-item-checkbox:checkbox:checked').length) {

			$('.woocl-coll-item-checkbox').each(function(){
				$('#woo_cl_chk_unchk_btn').html(Woo_cl_Public_Ajax.select_all_chk);
				$(this).prop('checked', false);
			});

		} else {
			$('.woocl-coll-item-checkbox').each(function(){
				$('#woo_cl_chk_unchk_btn').html(Woo_cl_Public_Ajax.unselect_all_chk);
				$(this).prop('checked', true);
			});
		}

		if($('.woocl-coll-item-checkbox:checked').length <= 0) {
			$('#woo_cl_add_to_cart_btn').addClass('woo-cl-disabled');
		} else {
			$('#woo_cl_add_to_cart_btn').removeClass('woo-cl-disabled');
		}

	});

	//Call ajax to add bulk products to the cart on click add to cart button
	$('#woo_cl_add_to_cart_btn').on('click', function(e){

		if($('.woocl-coll-item-checkbox:checked').length <= 0) {

	        e.preventDefault();
			return false;

		} else {

			var product_ids = '';

			$('.woocl-coll-item-checkbox').each(function(){

				if($(this).prop('checked')==true) {

					var product_id = $(this).val();

					product_ids += product_id + ',';
				}
			});

			$('.woo_cl_loader').show();

			var data = { 
						action			:	'woo_cl_add_bulk_products_to_cart',
						product_ids		:	product_ids,
					};

			//ajax call to Add bulk product to the cart
			$.post( Woo_cl_Public_Ajax.ajaxurl, data, function( response ) {

				var response_data = JSON.parse( response );

				if( response_data.success ) { // if response is success

					$('.woocl-add-to-cart-message').html(response_data.message);
					$('#woo_cl_chk_unchk_btn').html(Woo_cl_Public_Ajax.select_all_chk);
					$('.woocl-coll-item-checkbox').prop('checked', false);
					$('#woo_cl_add_to_cart_btn').addClass('woo-cl-disabled');

				} else if( response_data.error ) {

					alert(Woo_cl_Public_Ajax.add_bulk_products_to_cart_error);
				}

				$('.woo_cl_loader').hide();
			});	
		}
	});
	/*** End Select All / Unselect All button for selection items ***/
});