jQuery(document).ready(function($) {

	//Initialize select2
	jQuery('#cl_disable_user_role').select2();

	//Hide all hidden declare from settings
	jQuery( '.woocl-hidden-field' ).parents( 'tr' ).hide();

	// Change collection terminology
	jQuery( document ).on( 'change', '#cl_collection_terminology', function() {
		var terminology = jQuery(this).val();
		if( terminology == 'custom' ) {
			jQuery( '#cl_collection_terminology_singular_text, #cl_collection_terminology_plural_text' ).parents( 'tr' ).show();
		} else {
			jQuery( '#cl_collection_terminology_singular_text, #cl_collection_terminology_plural_text' ).parents( 'tr' ).hide();			
		}
	});

	//Trigger hide/show on load
	jQuery( '#cl_collection_terminology' ).trigger("change");

	// Change button text preview
	jQuery( document ).on( 'keyup', '#cl_add_to_collection_btn_text', function() {
		var button_text = jQuery(this).val().replace( '{no_of_collections}', 0 );
		jQuery( '#cl_add_to_collection_btn_preview .show_dropbox span.woocl-btn-label' ).html( button_text );
	});

	// Change button type preview
	jQuery( document ).on( 'change', '#cl_add_to_collection_btn_type', function() {

		//Get button type
		var button_type = jQuery(this).val();

		//Change classes based on choosed
		if( button_type == 'default' ) {
			jQuery( '#cl_add_to_collection_btn_preview .show_dropbox' ).removeClass( 'woocl-button' ).addClass( 'alt button' );
			button_class = 'alt button';
		} else if( button_type == 'button' ) {
			jQuery( '#cl_add_to_collection_btn_preview .show_dropbox' ).removeClass( 'alt button' ).addClass( 'woocl-button' );
			button_class = 'woocl-button';
		} else {
			jQuery( '#cl_add_to_collection_btn_preview .show_dropbox' ).removeClass('alt button woocl-button');
		}
	});

	// Change button icon preview
	jQuery( document ).on( 'change', '#cl_add_to_collection_link_icon', function() {

		//Get button icon
		var button_icon = jQuery(this).val();
		jQuery( '#cl_add_to_collection_btn_preview .show_dropbox i' ).attr( 'class', button_icon +' woocl-left' );
	});

	//Change users dropdown to get user collections
	$( document ).on( "change", "#woo_cl_dropdown_user", function() {

		//Get user id
		var user_id = $( this ).val();

		//Check if user id not null
		if( user_id != '' ) {

			// Show Loader
			$( this ).closest('td').find( '.woo-cl-collection-loader' ).show();
			$('.woo-cl-success-msg-tr').hide();

			var data = { 
							action		:	'woo_cl_get_user_collections',
							user_id		:	user_id
						};

			jQuery.post( ajaxurl,data,function(response) {

				var response_data = JSON.parse(response);

				if( response_data.sucess_html ) {
	
					// Hide Loader
					$( '.woo-cl-collection-loader' ).hide();
					
					$('.woo-cl-success-msg-tr').hide();
					$('.user_type_post_error').html('');
					$( '#user_collection_list' ).html( response_data.sucess_html  );
					
					$( '.collection_list' ).show();

					//window.location.reload();
	
				} else if( response_data.error ) {
					
					// Hide Loader
					$( '.woo-cl-collection-loader' ).hide();
					$( '.woo-cl-display-none' ).hide();
					$( '.user_type_post_error' ).html( response_data.error ).show();
				}
					
			});
		} else {

			//Hide objects
			$( '.woo-cl-post-tr, .woo-cl-user-type-tr, .woo-cl-users-tr, .woo-cl-guest-user-tr' ).hide();
		}
	});

	//Change user's collection to allow action
	$( document ).on( "change", "#user_collection_list", function() {
	
		var col_id 	= $( this ).val();
		var col_item = $(this).find(':selected').data('item');
		
		if( col_id != '' ) {
			$( '.woo-cl-product-list' ).show();
			$( '.woo_cl_add_product_id' ).attr( 'data-exclude' , col_item );
		} else {
			$( '.woo-cl-product-list' ).hide();
		}
	});

	//Show/Hide action when need
	$( document ).on( "change", ".woo_cl_add_product_id", function() {
	
		var pro_id = $( this ).val();
		
		if( pro_id != '' ) {
			$( '.woo-cl-save-tr' ).show();
		} else {
			$( '.woo-cl-save-tr' ).hide();
		}
	});

	// Click on button Add to Collection
	$( document ).on( "click", "#woo-cl-collection", function() {

		// user data
		var user_id		= $( '#woo_cl_dropdown_user option:selected' ).val();
		
		// collection data
		var col_id		= $( '#user_collection_list option:selected' ).val();
		
		// products data
		var products	= $( '.woo_cl_add_product_id' ).val();
		
		if( user_id == '' && user_id != undefined ) { 
			$('.user_type_post_error').show(); 
 			return false;
		} else { 
			$('.user_type_post_error').hide();
		}

		if( col_id == '' && col_id != undefined ) { 
			$('.user_collection_list_error').show();
			 return false;
		} else { 
			$('.user_collection_list_error').hide();
		}

		if( user_id != '' || col_id != '' || products != '' ) {

			var data = { 
				action			:	'woo_cl_add_to_collection',
				user_id			:	user_id,
				col_id			:	col_id,
				products		:	products,
				
			};

			// Show Loader
			$( '.woo-cl-save-loader' ).show();

			jQuery.post( ajaxurl,data,function(response) {

				// Hide Loader
				$( '.woo-cl-save-loader' ).hide();
				var response_data = JSON.parse(response);
				if( response_data.success ) {
					$('.woo-cl-success-msg-tr').show();
					$('.woo-cl-display-none').hide();
					$('.woo_cl_add_product_id').val(null).trigger('change');
					$('#woo_cl_dropdown_user').val(null).trigger('change');
					$('.woo-cl-save-tr').hide();
					$('.woo-cl-success-msg-tr').find('.woo-cl-success-message').removeClass( 'woocl-error' ).addClass( 'woocl-success' ).html( response_data.success ).show();
				}else{
					$('.woo-cl-success-msg-tr').show();
					$('.woo_cl_add_product_id').val(null).trigger('change');
					$('.woo-cl-success-msg-tr').find('.woo-cl-success-message').removeClass( 'woocl-success' ).addClass( 'woocl-error' ).html( response_data.error ).show();
				}
			});
		} else {
			$( '.collection_list' ).hide();
			$( '.woo-cl-product-list' ).hide();
		}
	});
});