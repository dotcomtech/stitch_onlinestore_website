// for variation 
jQuery(function($) {
	
    // "use strict";
    $(document).bind("reset_image", function() {
        $(".woo-cl-add-coll-btn").removeClass( 'woo-same-msg' ).addClass( 'disable' );
        $( '.woocl-product-variable-product').hide();
    });
	
    // on variation select
    $(document).bind( "show_variation", function( event, variation ) {

    	$(".woo-cl-add-coll-btn").addClass( 'disable' );
    	$(".woo-cl-variable_id").val(variation.variation_id);	

    	// if variation is in stock then only enabled collection button
    	if( variation.is_in_stock && variation.is_purchasable ) {    		
	        $(".woo-cl-add-coll-btn").removeClass( 'disable woo-same-msg' );
    	}

        // Add class when in stock and product not purchasable
        if( variation.is_in_stock && ! variation.is_purchasable ) {
            $(".woo-cl-add-coll-btn").addClass( 'woo-same-msg' );            
        }

        // Hide all variation default
        $( '.woocl-product-variable-product' ).hide();

        // Check if variation and display element exists
        if( variation.variation_id && $( '.woocl-product-attribute-'+ variation.variation_id ).length > 0 ) {
            $( '.woocl-product-attribute-'+ variation.variation_id ).show();            
        }
    })
})