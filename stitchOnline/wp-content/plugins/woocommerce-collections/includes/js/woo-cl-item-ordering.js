jQuery( function( $ ) {

	// style on hover image cursor move
	$("div.woocl-click-image").hover(function() {
	  $(this).css("cursor","move");
	});

	// Initialize sortable jquery for collection items
	$( '.woocl-collections' ).sortable({
		items: '.woocl-item-block',
		cursor: 'move',
		scrollSensitivity: 40,
		helper: function( event, ui ) {
			ui.each( function() {
				$( this ).width( $( this ).width() );
			});
			return ui;
		},
		start: function( event, ui ) {
			ui.placeholder.children().each( function() {
				var $original = ui.item.children().eq( ui.placeholder.children().index( this ) ),
					$this = $( this );

				$.each( $original[0].attributes, function( k, attr ) {
					$this.attr( attr.name, attr.value );
				});
			});
		},
		stop: function( event, ui ) {
			//Code here for drop
		},
		update: function( event, ui ) {
			
			$( '.woocl-collections' ).sortable( 'disable' );
			var coll_id    = $( '.woocl-collection-id' ).val();
			var postid     = ui.item.find( '.woocl-coll-item-id' ).val();
			var prevpostid = ui.item.prev().find( '.woocl-coll-item-id' ).val();
			var nextpostid = ui.item.next().find( '.woocl-coll-item-id' ).val();
		
			// Show Spinner
			ui.item.find( '.woocl-coll_img' ).hide().after( '<img alt="processing" src="'+ Woo_Cl_Item_Ajax.loader_img +'" class="waiting" style="position: absolute;left: 50%;top: 50%;"/>' );

			// Go do the sorting stuff via ajax
			$.post( Woo_Cl_Item_Ajax.ajaxurl, { action: 'woo_cl_collection_item_ordering', coll_id: coll_id, id: postid, previd: prevpostid, nextid: nextpostid }, function( response ) {
				$.each( response, function( key, value ) {
					$( '.woocl-collections' ).sortable( 'enable' );
					ui.item.find( '.woocl-coll_img' ).show().siblings( 'img' ).remove();
				});
			});
		}
	});
});