jQuery(document).ready(function($) {

	// click on add product to show/hide add product to collection block
	$( document ).on( 'click', '.woo_cl_add_product', function() {

		$( '.woo_cl_msg_info' ).hide();
		jQuery('.woo-cl-popup-overlay').fadeIn();
        jQuery('.woo-cl-addpop-content').fadeIn();
	});

	// click on add product to show/hide add product to collection block
	$( document ).on( 'click', '.woo-cl-addpop-close-btn, .woo-cl-popup-overlay', function() {

		jQuery('.woo_cl_add_product_id').removeClass('select2-dropdown-open');
		jQuery('.select2-with-searchbox').fadeOut();
        jQuery('.select2-drop-mask').fadeOut();
		
		jQuery('.woo-cl-popup-overlay').fadeOut();
        jQuery('.woo-cl-addpop-content').fadeOut();
        
	});

	// click on add product to edited collection
	$( document ).on( 'click', '#woo_cl_add_product_this_coll', function() {

		var coll_id = $('#post_ID').val();
		var coll_productid = $(this).parents( '.woo-cl-addpop-content' ).find('#woo_cl_add_product_coll').val();

		if( coll_productid == '' || coll_productid == undefined || coll_productid == false ) {

			$('.woo-cl-addpop-content').find('.woo_cl_msg_info').removeClass( 'woocl-success' ).addClass( 'woocl-error' ).html( Woo_cl_Admin_Ajax.product_unselect ).show();
			return;
		}

		var data = { 
						action			: 'woo_cl_add_product_to_collection',
						coll_id			: coll_id,
						coll_productid	: coll_productid,
						html_type		: 'admin_side_meta',
					};
		//ajax call to save data to database

		//hide add to collection button while process is running
		$('#woo_cl_add_product_this_coll').hide();

		//show loader while process is running
		$('.woo_cl_loader_img').show();
		
		//show adding text while process is running
		$('.woocl_adding_msg').show();

		$.post(Woo_cl_Admin_Ajax.ajaxurl,data,function(response) {
			
			//hide loader after process completed
			$('.woo_cl_loader_img').hide();
			
			//hide adding text when process is completed
			$('.woocl_adding_msg').hide();
			
			//show add to collection button when process is completed
			$('#woo_cl_add_product_this_coll').show();

			var response_data = JSON.parse(response);

			if( response_data.success ) {

				//$('.woo-cl-addpop-content').find('.woo_cl_msg_info').removeClass( 'woocl-error' ).addClass( 'woocl-success' ).html( Woo_cl_Admin_Ajax.add_product_success ).show();
				jQuery('.woo-cl-popup-overlay').fadeOut();
        		jQuery('.woo-cl-addpop-content').fadeOut();
        		
        		jQuery('#woo_cl_products_meta').find('div.inside').html(response_data.successhtml);
        		
				//window.location.reload();

			} else if( response_data.error ) {
				$('.woo-cl-addpop-content').find('.woo_cl_msg_info').removeClass( 'woocl-success' ).addClass( 'woocl-error' ).html( response_data.error ).show();
			}
		});
	});

	$( ".woocl-checkall" ).change(function () {
	    $( ".woo_cl_products_disc input:checkbox" ).prop('checked', $(this).prop("checked"));
	});

	// click on delete collection item ajax call
	$( document ).on( 'click', '.woo_cl_delete_coll_item', function() {
		
		var main_wrap	= $( this ).parents( 'tr' );

		//get collection item id
		var coll_item_id	= main_wrap.find( '.woo_cl_coll_item_id' ).val();

		//Alert Message
		var confirm_res = confirm( Woo_cl_Admin_Ajax.confirm_delete );
		if( confirm_res ){

		    //delete collection Item
		    woo_cl_delete_coll_item( coll_item_id, main_wrap );
		}

	});

	// click on bulk delete collection item bulk ajax call
	$( document ).on( 'click', '#woo_cl_apply_button', function() {
		
		var bulk_select	= $( '.woo_cl_bulk_select' ).val();

		if( bulk_select == 'delete' ) {//check if bulk delete selected

			//get collection item id
			var coll_items	= $( this ).parents( '.woo_cl_products_disc' ).find( 'tbody' );
			coll_items.find( 'input:checked' ).each(function()
			{
			    var coll_item_id =  $(this).siblings( '.woo_cl_coll_item_id' ).val()

			    //Get main wrap
			    var main_wrap	 = $(this).parents( 'tr' );

			    //delete collection Item
			    woo_cl_delete_coll_item( coll_item_id, main_wrap );
			});

		}
	});

});

//function for delete collection item with Ajax
function woo_cl_delete_coll_item( coll_item_id, main_wrap ) {

	var data = { 
					action 			:	'woo_cl_collection_item_delete',
					coll_item_id	:	coll_item_id,
				};
				
	jQuery('table.woo_cl_products_disc').siblings('.woo_cl_add_product_spinner').addClass('is-active');
				
	//ajax call to save data to database
	jQuery.post(Woo_cl_Admin_Ajax.ajaxurl,data,function(response) {
		
		var response_data = JSON.parse(response);
		
		if( response_data.success ) {
			
			//remove collection item from display if success
			main_wrap.remove();

			if( jQuery( '.woo_cl_products_disc tbody tr' ).length == 0 ) {
				jQuery( '.woo_cl_products_disc tbody' ).html( '<tr valign="top"><td colspan="5">'+ Woo_cl_Admin_Ajax.no_item_found +'</td></tr>' );
			}
			//Get New Total Price
			jQuery( '.woo_cl_total_price' ).html( response_data.totalprice );
		}
		
		jQuery('table.woo_cl_products_disc').siblings('.woo_cl_add_product_spinner').removeClass('is-active');
	});	
}