// JavaScript Document
jQuery(document).ready(function($) {

// Start Shortcodes Click
(function() {
    tinymce.create('tinymce.plugins.woo_cl_coll_shortcodes', {
        init : function(ed, url) {
        	
            ed.addButton('woo_cl_coll_shortcodes', {
                title : 'Woo Collections',
                image : url+'/images/collection-dark.png',
                onclick : function() {
                	
					jQuery('.woo-cl-popup-overlay').fadeIn();
                    jQuery('.woo-cl-popup-content').fadeIn();
                    
                    jQuery('.woo-cl-shortcodes-options').hide();
                    
                    jQuery('#woo_cl_shortcode').change();
                    //jQuery('#woo_cl_add_to_collection_wrap').show();
                    
                    jQuery('#woo_cl_add_to_collection_text').val("Add to Collection");

                    jQuery('#woo_cl_featured_colls_title').val("Featured Collections");
                    jQuery('#woo_cl_no_of_coll').val("10");
					jQuery('#woo_cl_show_private').attr( 'checked', false );
					
					jQuery('#woo_cl_my_colls_title').val("My Collections");
					jQuery('#woo_cl_per_page_collection').val(3);
					jQuery('#woo_cl_show_all_colls_count').attr( 'checked', true );
					jQuery('#woo_cl_show_all_colls_link').attr( 'checked', true );
					
					$("html, body").animate({ scrollTop: 0 });
 				}
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
 
    tinymce.PluginManager.add('woo_cl_coll_shortcodes', tinymce.plugins.woo_cl_coll_shortcodes);
})();
	
	jQuery( document ).on('click', '.woo-cl-popup-close-button, .woo-cl-popup-overlay', function () {
		jQuery('.woo-cl-popup-overlay').fadeOut();
		jQuery('.woo-cl-popup-content').fadeOut();
		
	});
	jQuery( document ).on('click', '#woo_cl_insert_shortcode', function () {
		
		var wooclshortcode  = jQuery('#woo_cl_shortcode').val();
		var wooclshortcodestr 	= '';
		if( wooclshortcode == '' ) {
			jQuery('.woo-cl-popup-error').fadeIn();
			return false;
		} else {
			jQuery('.woo-cl-popup-error').hide();
				
				switch(wooclshortcode) {

					case 'woo_cl_add_to_collection' 		:
					
										var id  	= jQuery('#woo_cl_add_to_collection_id').val();
										var text  	= jQuery('#woo_cl_add_to_collection_text').val();
										var type	= jQuery('#woo_cl_add_to_collection_type').val();
										var icon	= jQuery('#cl_add_to_collection_link_icon').val();
										
										wooclshortcodestr	+= '['+wooclshortcode;
										
										if( id != '' ) {
											wooclshortcodestr	+= ' id="'+id+'"';
										}

										if( text != '' ) {
											wooclshortcodestr	+= ' text="'+text+'"';
										}

										if( type != '' ) {
											wooclshortcodestr	+= ' type="'+type+'"';
										}

										if( icon != '' ) {
											wooclshortcodestr	+= ' icon="'+icon+'"';
										}
										
										wooclshortcodestr	+= '][/'+wooclshortcode+']';
										
										break;

					case 'woo_cl_featured_collections' 		:
					
										var featured_title  = jQuery('#woo_cl_featured_colls_title').val();
										var no_of_colls  	= jQuery('#woo_cl_no_of_coll').val();
										var showprivate		= jQuery('#woo_cl_show_private');
										
										wooclshortcodestr	+= '['+wooclshortcode;
										
										if( featured_title != '' ) {
											wooclshortcodestr	+= ' fcolls_title="'+featured_title+'"';
										}

										if( no_of_colls != '' ) {
											wooclshortcodestr	+= ' no_of_colls="'+no_of_colls+'"';
										}
										
										if( showprivate.is(":checked") ) {
											wooclshortcodestr	+= ' showprivate="true"';
										}
										
										wooclshortcodestr	+= '][/'+wooclshortcode+']';
										
										break;
										
					case 'woo_cl_my_collections' 		:
					
										var colls_title = jQuery('#woo_cl_my_colls_title').val();
										var per_page	= jQuery('#woo_cl_per_page_collection').val();
										var show_counts	= jQuery('#woo_cl_show_all_colls_count');
										var show_link	= jQuery('#woo_cl_show_all_colls_link');
										
										wooclshortcodestr	+= '['+wooclshortcode;
										
										wooclshortcodestr	+= ' collection_title="'+colls_title+'"';
										
										/*if( per_page != '' ) {
											wooclshortcodestr	+= ' per_page="'+per_page+'"';
										}*/
										
										if( show_counts.is(":checked") ) {
											wooclshortcodestr	+= ' show_count="true"';
										}
										
										if( show_link.is(":checked") ) {
											wooclshortcodestr	+= ' enable_see_all="true"';
										}
										
										wooclshortcodestr	+= '][/'+wooclshortcode+']';
										
										break;
								
					default:break;
				}
			 	
			 	 //send_to_editor(str);
				window.send_to_editor( wooclshortcodestr );
		  		jQuery('.woo-cl-popup-overlay').fadeOut();
				jQuery('.woo-cl-popup-content').fadeOut();
		}
	});
	jQuery('#woo_cl_shortcode').change(function() {
		
		var wooclshortcode = jQuery(this).val();
		jQuery('.woo-cl-shortcodes-options').hide();
		switch(wooclshortcode) {
			case 'woo_cl_add_to_collection' 	:
								jQuery('#woo_cl_add_to_collection_wrap').show();
								break;

			case 'woo_cl_featured_collections' 	:
								jQuery('#woo_cl_featured_coll_wrap').show();
								break;

			case 'woo_cl_my_collections' 	:
								jQuery('#woo_cl_my_collections_wrap').show();
								break;							
			default:break;
		}
	});
});