<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Misc Functions
 * 
 * All misc functions handles to  
 * different functions 
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

/**
 * Create a token for logged out users
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_create_token( $coll_id = '' ) {

	//Get meta prefix
	$prefix = WOO_CL_META_PREFIX;
	
	// if user is not logged in, we check to see if they already have a token.
	// If not, we create a token and store it as a cookie
	// Each list that is created will have this token stored with it
	if ( ! is_user_logged_in() ) {
		$cl_token = woo_cl_get_list_token();

		if ( $cl_token ) {
			update_post_meta( $coll_id, $prefix.'cl_token', $cl_token );
		}
		else {
			// append their first Collection id to the front of the timestamp. The list ID auto increments and will always be unique
			$cl_token 		= $coll_id . time();
			$token_expire 	= apply_filters( 'woo_cl_token_expire', time()+3600*24*30 );

			$cookie = setcookie( 'woo_cl_token', $cl_token, $token_expire, COOKIEPATH, COOKIE_DOMAIN );
			// store woo_cl_token against list with the same time stamp
			// we'll use this to verify that this belongs to the user
			update_post_meta( $coll_id, $prefix.'cl_token', $cl_token );

		}

		return $cl_token;
	}

}

/**
 * Check if the list belongs to the current user.
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
*/
function woo_cl_is_users_list( $coll_id = '', $for_edit = false ) {

	//Get meta prefix
	$prefix = WOO_CL_META_PREFIX;

	$return	= false;

	$list = get_post( $coll_id );
	$current_user_id = get_current_user_id();

	//Get collection view page end point
	$woo_cl_view_slug	= get_option( 'cl_view_collection_text' );
	$woo_cl_view_slug	= trim( $woo_cl_view_slug ) != '' ? $woo_cl_view_slug : 'view-collection';

	//Get collection edit end point
	$woo_cl_edit_slug	= get_option( 'cl_edit_collection_text' );
	$woo_cl_edit_slug	= trim( $woo_cl_edit_slug ) != '' ? $woo_cl_edit_slug : 'edit-collection';

	if ( ! ( get_query_var( $woo_cl_edit_slug ) || get_query_var( $woo_cl_view_slug ) ) ) { // should only be on view or edit page

		$return = false;

	} else {

		if ( is_user_logged_in() && (int) $list->post_author === $current_user_id ) { // logged in users

			$return = true;

		} elseif( !is_user_logged_in() && $for_edit && !empty( $list->post_author ) ) {//hide edit coll button for guest user and aollecion author is not blank
			
			$return = false;
			
		} elseif ( !is_user_logged_in() ) { // non logged in users

			// get token
			$cl_token = woo_cl_get_list_token();
			$cl_token = isset( $cl_token ) ? $cl_token : '';

			if ( $cl_token ) {

				// get custom meta of post
				$current_list_token = get_post_meta( $coll_id, $prefix.'cl_token', true );

				if ( $cl_token === $current_list_token ) { // if token matches the woo_cl_token then this list belongs to user
					$return = true;
				}
			}
		}
	}

	return apply_filters( 'woo_cl_is_users_list', $return, $coll_id );
}

/**
 * Retrieve a saved wish list token. Used in validating wish list
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_list_token() {

	if( ! is_user_logged_in() ) {
		$cl_token = isset( $_COOKIE['woo_cl_token'] ) ? $_COOKIE['woo_cl_token'] : null;

		return apply_filters( 'woo_cl_get_list_token', $cl_token );
	}
		
	return null;
}

/**
 * Get lists by specific user (author)
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_guest_lists( $cl_token ) {

	global $woo_cl_model;

	//Get meta prefix
	$prefix = WOO_CL_META_PREFIX;

	//Arguments for get guest collections
	$args	= array(
					'post_status'	=> array( 'publish', 'private' ),
					'meta_query'	=> array(
											array( 
												'key' 	=> $prefix.'cl_token', 
												'value' => $cl_token
												)
											)
				);

	//Get collections
	$lists	= $woo_cl_model->woo_cl_get_collections( $args );

	if ( $lists )
		return $lists;

	return null;
}

/**
 * Get All Messages
 * 
 * Handle to get all messages
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_messages() {

	$messages = array(
		'must_login' 			=> sprintf( __( "Sorry, you need to be logged in to edit %s items.", 'woocl' ), woo_cl_get_label_singular( true ) ),
		'create_must_login' 	=> sprintf( __( 'You need to be logged in to your account to create a %s', 'woocl' ), woo_cl_get_label_singular( true ) ),
		'can_not_add_product' 	=> sprintf( __( 'Sorry, you can\'t add this product to %s.', 'woocl' ), woo_cl_get_label_singular( true ) ),
		'no_more_coll' 			=> sprintf( __( "No more %s to load.", "woocl"), woo_cl_get_label_plural( true ) ),
		'no_more_coll_item'		=> sprintf( __( "No more %s items to load.", "woocl"), woo_cl_get_label_singular( true ) ),
		'no_coll_found'			=> sprintf( __( 'Sorry, No %s Found.', 'woocl'), woo_cl_get_label_singular() ),
		'no_user_coll_found'	=> sprintf( __( 'You currently have no %s.', 'woocl'), woo_cl_get_label_plural() ),
		'no_coll_item_found'	=> sprintf( __( 'No %s Item Found.', 'woocl'), woo_cl_get_label_singular() ),
		'no_item_found' 		=> sprintf( __( "There is no products for this %s.","woocl"), woo_cl_get_label_singular( true ) ),
		'coll_valid_error' 		=> sprintf( __( "Please enter valid title of the %s.", "woocl"), woo_cl_get_label_singular( true ) ),
		'coll_empty_error' 		=> sprintf( __( "Please enter title of the %s.", "woocl"), woo_cl_get_label_singular( true ) ),
		'coll_selection_error' 	=> sprintf( __( "Please select any %s to add product.", "woocl"), woo_cl_get_label_singular( true ) ),
		'email_error' 			=> __( "Please enter your email address.", "woocl"),
		'email_valid_error' 	=> __( "Please enter a valid email address.", "woocl"),
		'email_fri_error' 		=> __( "Please enter one or more email addresses.", "woocl"),
		'email_fri_valid_error' => __( "You must enter a valid email, or comma separate multiple emails.", "woocl"),
		'add_cart_success' 		=> __( "Added successfully to your cart.", "woocl"),
		'confirm_delete' 		=> sprintf( __( "Click OK to delete product from this %s.","woocl"), woo_cl_get_label_singular() ),
		'empty_coll_id' 		=> sprintf( __( 'Sorry, you need to pass %s id in URL.', 'woocl' ), woo_cl_get_label_singular( true ) ),
		'coll_not_available' 	=> sprintf( __( 'Sorry, This %s is no longer available.', 'woocl' ), woo_cl_get_label_singular( true ) ),
		'user_not_available' 	=> __( 'Sorry, This user is no longer available.', 'woocl' ),
		'prevent_edit_access' 	=> sprintf( __( "Sorry, you don't have access to edit this %s items.", 'woocl' ), woo_cl_get_label_singular( true ) ),
		'prevent_add_access' 	=> sprintf( __( "Sorry, you don't have access to add items in this %s.", 'woocl' ), woo_cl_get_label_singular( true ) ),
		'already_added' 		=> sprintf( __( "Product already added to this %s", "woocl"), woo_cl_get_label_singular( true ) ),
		'coll_already_exist' 	=> sprintf( __( 'Sorry, you already have a %1$s with that name. Choose another name, or add this item to your existing %1$s.', 'woocl'), woo_cl_get_label_singular( true ) ),
		'coll_create_success' 	=> sprintf( __( '%s created successfully', 'woocl' ), woo_cl_get_label_singular() ),
		'something_wrong' 		=> __( 'Something wrong, try again.', 'woocl' ),
		'save_success' 			=> __( 'Save Successfully.', 'woocl' ),
		'colls_limit_over' 		=> sprintf( __( 'Sorry, You can\'t create more %s.', 'woocl' ), woo_cl_get_label_singular( true ) ),
		'products_limit_over' 	=> sprintf( __( 'Sorry, you can\'t add more products in this %s.', 'woocl' ), woo_cl_get_label_singular( true ) ),
		'product_unselect' 		=> sprintf( __( 'Please select any product to add product to this %s.', 'woocl' ), woo_cl_get_label_singular( true ) ),
		'add_product_success' 	=> sprintf( __( 'Product added successfully to %s.', 'woocl' ), woo_cl_get_label_singular( true ) ),
		'must_choose_product_option' 	=> sprintf( __( 'Please select some product options before adding this product to your %s.', 'woocl' ), woo_cl_get_label_singular( true ) ),
	);

	return apply_filters( 'woo_cl_messages', $messages );
}

/**
 * Get Default Labels
 * 
 * Handle to get default labels
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.1.5
 */
function woo_cl_get_default_labels() {

	$collection_terminology = get_option( 'cl_collection_terminology' );
	switch ( $collection_terminology ) {
		case 'lightbox':
			$defaults = array(
				'singular'	=> __( 'Light Box', 'woocl' ),
				'plural'	=> __( 'Light Boxes', 'woocl')
			);
			break;
		case 'watchlist':
			$defaults = array(
				'singular'	=> __( 'Watch List', 'woocl' ),
				'plural'	=> __( 'Watch Lists', 'woocl')
			);
			break;
		case 'wishlist':
			$defaults = array(
				'singular'	=> __( 'Wish List', 'woocl' ),
				'plural'	=> __( 'Wish Lists', 'woocl')
			);
			break;
		case 'wantlist':
			$defaults = array(
				'singular'	=> __( 'Want List', 'woocl' ),
				'plural'	=> __( 'Want Lists', 'woocl')
			);
			break;
		case 'custom':

			//Get custom texts
			$singular_text 	= get_option( 'cl_collection_terminology_singular_text' );
			$plural_text 	= get_option( 'cl_collection_terminology_plural_text' );
			$defaults = array(
				'singular'	=> trim( $singular_text ) ? $singular_text : __( 'Collection', 'woocl' ),
				'plural'	=> trim( $plural_text ) ? $plural_text : __( 'Collections', 'woocl')
			);
			break;
		default:
			$defaults = array(
				'singular'	=> __( 'Collection', 'woocl' ),
				'plural'	=> __( 'Collections', 'woocl')
			);
			break;
	}

	return apply_filters( 'woo_cl_default_labels', $defaults );
}

/**
 * Get Singular Label
 * 
 * Handle to get singular label
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_label_singular( $lowercase = false ) {
	$defaults = woo_cl_get_default_labels();
	return ($lowercase) ? strtolower( $defaults['singular'] ) : $defaults['singular'];
}

/**
 * Get Plural Label
 * 
 * Handle to get plural label
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_label_plural( $lowercase = false ) {
	$defaults = woo_cl_get_default_labels();
	return ( $lowercase ) ? strtolower( $defaults['plural'] ) : $defaults['plural'];
}

/**
 * Check Add Collection Should Display
 * 
 * Handle to check that add to collection
 * page is display on that page or not
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_is_enable_collection_button( $page_id = '' ) {
	
	$enable_collection = false;
	
	if( is_shop() || is_product_category() || is_product_tag() || is_product() ) {
		$enable_collection = true;
	}
	
	return apply_filters( 'woo_cl_is_enable_collection_button', $enable_collection, $page_id );
}

/**
 * Check It IS Collection
 * 
 * Handle to check it is collection or not
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_is_collection( $collection_id = '' ) {
	
	$coll_post_type	= WOO_CL_POST_TYPE_COLL;
	$is_collection	= false;
	
	if( !empty( $collection_id ) ) { 
	
		$collection	= get_post( $collection_id );
		
		if( !empty( $collection ) ) {
			if( isset( $collection->post_type ) && $collection->post_type == $coll_post_type ) {
				$is_collection	= true;
			}
		}
	}
	return apply_filters( 'woo_cl_is_collection', $is_collection, $collection_id );
}

/**
 * Check if we're on a certain page
 * 
 * Handle to check if we're on a certain page
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_is_page( $page = '' ) {
	
	global $wp;

	//Get collection view page end point
	$woo_cl_view_slug	= get_option( 'cl_view_collection_text' );
	$woo_cl_view_slug	= trim( $woo_cl_view_slug ) != '' ? $woo_cl_view_slug : 'view-collection';

	//Get collection edit end point
	$woo_cl_edit_slug	= get_option( 'cl_edit_collection_text' );
	$woo_cl_edit_slug	= trim( $woo_cl_edit_slug ) != '' ? $woo_cl_edit_slug : 'edit-collection';

	//Get user endpoint
	$woo_cl_user_slug	= get_option( 'cl_collection_author_text' );
	$woo_cl_user_slug	= trim( $woo_cl_user_slug ) != '' ? $woo_cl_user_slug : 'collection-author';
	
	$id	= woo_cl_get_collection_page_id();
	
	if ( is_page( $id ) ) {
		
		switch ( $page ) {
			case $woo_cl_view_slug :
				if( isset( $wp->query_vars[$woo_cl_view_slug] ) ) {
					return true;
				}
				break;
			case $woo_cl_edit_slug :
				if( isset( $wp->query_vars[$woo_cl_edit_slug] ) ) {
					return true;
				}
				break;
			case $woo_cl_user_slug :
				if( isset( $wp->query_vars[$woo_cl_user_slug] ) ) {
					return true;
				}
				break;
		}
	}
	
	return false;
}

/**
 * Check if we're on a valid certain page
 * 
 * Handle to check if we're on a valid certain page
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_is_invalid_page( $page = '' ) {
	
	global $wp;

	//Get collection view page end point
	$woo_cl_view_slug	= get_option( 'cl_view_collection_text' );
	$woo_cl_view_slug	= trim( $woo_cl_view_slug ) != '' ? $woo_cl_view_slug : 'view-collection';

	//Get collection edit end point
	$woo_cl_edit_slug	= get_option( 'cl_edit_collection_text' );
	$woo_cl_edit_slug	= trim( $woo_cl_edit_slug ) != '' ? $woo_cl_edit_slug : 'edit-collection';

	//Get user endpoint
	$woo_cl_user_slug	= get_option( 'cl_collection_author_text' );
	$woo_cl_user_slug	= trim( $woo_cl_user_slug ) != '' ? $woo_cl_user_slug : 'collection-author';
	
	$id	= woo_cl_get_collection_page_id();
	
	if ( is_page( $id ) ) {
		
		switch ( $page ) {
			case $woo_cl_view_slug :
				if( isset( $wp->query_vars[$woo_cl_view_slug] ) && empty( $wp->query_vars[$woo_cl_view_slug] ) ) {
					return true;
				}
				break;
			case $woo_cl_edit_slug :
				if( isset( $wp->query_vars[$woo_cl_edit_slug] ) && empty( $wp->query_vars[$woo_cl_edit_slug] ) ) {
					return true;
				}
				break;
			case $woo_cl_user_slug :
				if( isset( $wp->query_vars[$woo_cl_user_slug] ) && empty( $wp->query_vars[$woo_cl_user_slug] ) ) {
					return true;
				}
				break;
		}
	}
	
	return false;
}

/**
 * Validate emails used in the email share box
 * 
 * Handle to validate emails used in the email share box
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_validate_share_emails( $emails = '' ) {
	
	// explode string into array
	$emails	= explode( ',', $emails );
	
	// remove whitespace and clean
	$emails	= array_filter( array_map( 'trim', $emails ) );
	
	if ( $emails ) {
		foreach ( $emails as $email ) {
			if ( ! is_email( $email ) ) {
				$valid_email = false;
				break;
			} else {
				$valid_email = true;
				continue;
			}
		}
	}
	
	if ( $valid_email ) {
		return $valid_email;
	}
	
	return null;
}

/**
 * Get Collection Share URL
 * 
 * Handle to get collection share URL
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_collection_share_url( $collecion_id = '' ) {
	
	$share_url	= '';
	
	if( !empty( $collecion_id ) ) {
		
		//Get shortlink for share
		$share_url	= wp_get_shortlink( $collecion_id );
	}
	
	return apply_filters( 'woo_cl_collection_share_url', $share_url, $collecion_id );
}

/**
 * Get Product url
 * 
 * Handle to get Product url by id
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_product_url( $product_id = '' ) {	
		
	$product_url	= '';
	if( !empty( $product_id ) ) {

		//Get product data
		$_product   = woo_cl_wc_get_product( $product_id );
		
		if ( version_compare( WOOCOMMERCE_VERSION, "3.0.0" ) == -1 )
			$_product_id	= isset( $_product->id ) ? $_product->id : '';
		else
			$_product_id	= $_product->get_id();
		
		//Get product page URL
		$product_url	= get_permalink( $_product_id );
		
		//Check is varible product
		if( $_product->is_type( 'variation' ) || $_product->is_type( 'variable' ) ) {

			//Get object of product variation class
			$product_variation_list	= wc_get_product( $product_id );
			
			// To make compatible with previous versions of 3.0.0
	 		if ( version_compare( WOOCOMMERCE_VERSION, "3.0.0" ) == -1 ) {
	 			$variation_data = $product_variation_list->variation_data;
	 		} else {
	 			$variation_data = $product_variation_list->get_variation_attributes();
	 		}
			
			foreach ( $variation_data as $key => $value ) {	
						
				$product_url = add_query_arg( $key , $value, $product_url );			
			}
		}
	}
	
	return apply_filters( 'woo_cl_get_product_url', $product_url, $product_id );
}

/**
 * Get Product Title
 * 
 * Handle to get Product Title by id
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_product_title( $product_id = '' ) {
	
	$product_title	= '';
	
	if( !empty( $product_id ) ) {

		//Get product data
		$_product   = woo_cl_wc_get_product( $product_id );
		
		//Get Product Title
		$product_title	= isset( $_product->post->post_title ) ? $_product->post->post_title :'';
	}
	
	return apply_filters( 'woo_cl_get_product_title', $product_title, $product_id );
}

/**
 * Get Product Variation
 * 
 * Handle to get Product Variation by id
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

function woo_cl_get_product_variation( $product_id = '', $flag = true, $coll_product_id = '' ) {

	$variation_data	= '';
	
	if( !empty( $product_id ) ) {

		// Get product
		$product = woo_cl_wc_get_product( $product_id );
		
		if( is_object( $product ) ) {

			//Check is varible product
			if( $product->is_type( 'variation' ) || $product->is_type( 'variable' ) ) {
	
				//Get object of product variation class
				$product_data	= wc_get_product ( $product_id );

				//get formetted variation
				if ( version_compare( WOOCOMMERCE_VERSION, "3.0.0" ) == -1 ) {
					$variation_data = wc_get_formatted_variation( $product_data->variation_data, $flag );
				} else {
					$variation_data = wc_get_formatted_variation( $product_data->get_variation_attributes(), $flag );
				}
			}
		}
	}
	
	return apply_filters( 'woo_cl_get_product_variation', $variation_data, $product_id, $flag, $coll_product_id );
}

/**
 * Check is follow plugin activate
 * 
 * Handle check the follow plugin activate or not
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_is_follow_activate() {
	
	$active_follower	= false;
	
	if( class_exists( 'Wpw_Fp_Model' ) ) {
		$active_follower	= true;
	} else {
		$active_follower	= false;
	}
	
	return apply_filters( 'woo_cl_is_follow_activate', $active_follower );
}

/**
 * Get Collection Page ID
 * 
 * Handle to Get Collection Page ID
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_collection_page_id() {
	
	//Get collection page id
	$cl_coll_page_id	= get_option( 'cl_coll_lists_page' );
	
	return apply_filters( 'woo_cl_get_collection_page_id', $cl_coll_page_id );
}

/**
 * Woocommerce Collection Endpoints
 * 
 * Handle to woocommerce collection endpoints
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_endpoint_url( $endpoint, $permalink = '', $value = '' ) {
	
	if ( ! $permalink ) {
		$permalink	= get_permalink();
	}
	
	if ( get_option( 'permalink_structure' ) ) {
		if ( strstr( $permalink, '?' ) ) {
			$query_string = '?' . parse_url( $permalink, PHP_URL_QUERY );
			$permalink    = current( explode( '?', $permalink ) );
		} else {
			$query_string = '';
		}
		$url = trailingslashit( $permalink ) . $endpoint . '/' . $value . $query_string;
	} else {
		$url = add_query_arg( $endpoint, $value, $permalink );
	}
	
	return apply_filters( 'woo_cl_get_endpoint_url', $url );
}

/**
 * Get Collection Per Page Count
 * 
 * Handle to Get Collection Per Page Count
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_collection_per_page_count() {
	
	//Get collection page id
	$per_page_count	= get_option( 'cl_pagination_options' );
	$per_page_count	= !empty( $per_page_count ) ? $per_page_count : WOO_CL_POST_PER_PAGE;
	
	return apply_filters( 'woo_cl_get_collection_per_page_count', $per_page_count );
}

/**
 * Get Collection Per Page Count
 * 
 * Handle to Get Collection Per Page Count
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.3
 */
function woo_cl_get_collection_column_display() {
	
	//Get column value per page
	$per_page_count	= get_option( 'cl_no_of_col_items_list' );
	$per_page_count	= !empty( $per_page_count ) ? $per_page_count : WOO_CL_POST_PER_PAGE;
	
	return apply_filters( 'woo_cl_get_collection_column_display', $per_page_count );
}


/**
 * Get No of Collections to display on My Account page
 * under collection sections
 * 
 * Handle to Get No of Collections to disaply from my account page
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_no_of_collection_on_my_account_page() {
	
	//Get no of collection items settings
	$no_of_collections	= get_option( 'cl_no_of_my_collection_items' );
	$no_of_collections	= !empty( $no_of_collections ) ? $no_of_collections : WOO_CL_NO_OF_MY_COLLECTION_ITEMS;
	
	return apply_filters( 'woo_cl_get_no_of_collection_on_my_account_page', $no_of_collections );
}

/**
 * Get Slug Using Parameter
 * 
 * Handle to Get Slug Using Parameter
 * Like View / Edit / Author
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_slug( $param = '' ) {
	
	$slug	= '';
	
	switch ( $param ) {
		
		case 'view' : //Get collection view page end point
			$slug	= get_option( 'cl_view_collection_text' );
			$slug	= trim( $slug ) != '' ? $slug : 'view-collection';
			break;
			
		case 'edit': //Get collection edit end point
			$slug	= get_option( 'cl_edit_collection_text' );
			$slug	= trim( $slug ) != '' ? $slug : 'edit-collection';
			break;
			
		case 'author' : //Get user endpoint
			$slug	= get_option( 'cl_collection_author_text' );
			$slug	= trim( $slug ) != '' ? $slug : 'collection-author';
			break;
	}
	
	return apply_filters( 'woo_cl_get_slug', $slug, $param );
}

/**
 * Add To Collection Button
 * 
 * Handle to Display Add to Collection button
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_add_to_collection_button( $args = array() ) {
	
	global $post, $woo_cl_model, $current_user;

	//Get details
	$prefix 	= WOO_CL_META_PREFIX;
	$user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';
	$product_id	= isset( $post->ID ) ? $post->ID : '';

	//Get add to collection button text with product level override
	$add_to_coll_btn_text 	= get_post_meta( $product_id, $prefix .'add_to_collection_btn_text', true );
	if( empty( $add_to_coll_btn_text ) ) {

		$add_to_coll_btn_text	= get_option( 'cl_add_to_collection_btn_text' );
		if( empty( $add_to_coll_btn_text ) ) {
			$add_to_coll_btn_text	= sprintf( __( 'Add to %s', 'woocl' ), woo_cl_get_label_singular() );
		}
	}

	//Get add to collection button type with product level override
	$cl_get_button_type 	= get_post_meta( $product_id, $prefix .'add_to_collection_btn_type', true );
	if( empty( $cl_get_button_type ) ) {
		$cl_get_button_type	= get_option( 'cl_add_to_collection_btn_type' );
	}

	// Get button class based on option
	if( $cl_get_button_type == 'default' ) {
		$button_class = (is_single()) ? 'alt button' : 'button';
	} else if( $cl_get_button_type == 'button' ) {
		$button_class = 'woocl-button';
	}

	//Get add to collection button icon with product level override
	$cl_add_to_coll_btn_icon 	= get_post_meta( $product_id, $prefix .'add_to_collection_link_icon', true );
	if( $cl_add_to_coll_btn_icon == 'none' ) {
		$cl_add_to_coll_btn_icon = '';
	} elseif ( empty( $cl_add_to_coll_btn_icon ) ) {
		$cl_add_to_coll_btn_icon	= get_option( 'cl_add_to_collection_link_icon' );
	}

	$defaults	= apply_filters( 'woo_cl_add_to_collection_defaults', 
						array(
								'product_id'			=> $post->ID,
								'variation_class'		=> '',
								'button_class'			=> ( isset( $button_class ) ) ? $button_class : '',
								'add_to_coll_btn'		=> $add_to_coll_btn_text,
								'add_to_coll_btnicon'	=> $cl_add_to_coll_btn_icon,
								'icon_position'			=> 'left',
							)
						);
	
	// merge arrays
	$add_to_cll_btn_arg	= apply_filters( 'woo_cl_add_to_collection_button_args', wp_parse_args( $args, $defaults ) );
	
	//get products
	$product	= woo_cl_wc_get_product( $add_to_cll_btn_arg['product_id'] );
	
	//if product is empty
	if( empty( $product ) ) return;

	//Prevent display add to collection button on shop page for variable product.
	if( is_shop() && $product->is_type( 'variable' ) ) return;

	//variation ID
	$variation_id	= isset( $product->variation_id ) ? $product->variation_id : '';
	
	// Product ID
	if ( version_compare( WOOCOMMERCE_VERSION, "3.0.0" ) == -1 )
		$_product_id	= isset( $product->id ) ? $product->id : '';
	else
		$_product_id	= $product->get_id();
	
	if( !woo_cl_collection_btn_isenable( $_product_id, $variation_id ) ) { // Check add to collection button is enable
		return;
	}

	//Filter for check condition if there us no add to cart button
	$check	= apply_filters( 'woo_cl_add_to_collection_button_check',
					!( $product->is_purchasable() && $product->is_in_stock() ) && !$product->is_type( 'external' ) , $product );
	
	//add to collection not display if there us no add to cart button
	if( $check ) return;
	
	//Product ID
	$product_id	= $add_to_cll_btn_arg['product_id'];

	// To make compatible with previous versions of 3.0.0
	if ( version_compare( WOOCOMMERCE_VERSION, "3.0.0" ) == -1 ) {
		//Product type
		$product_type	= isset( $product->product_type ) ? $product->product_type : '';
	} else {
		$product_type	= $product->get_type();
	}
	
	//Get product type is allowed ot not
	$allow_product_type	= $woo_cl_model->woo_cl_allowed_product_type( $product_id );
	
	//If this allow product type
	if( $allow_product_type == false ) return;
	
	//set variation class if variable product
	$add_to_cll_btn_arg['variation_class']	= ( empty( $variation_id ) && ( $product_type == 'variable' || $product_type == 'variation' ) ) ? ' woo-cl-add-coll-btn' : '';

	//Replace no of collections template tag
	if( isset( $add_to_cll_btn_arg['add_to_coll_btn'] ) && strpos( $add_to_cll_btn_arg['add_to_coll_btn'], '{no_of_collections}' ) !== false ) {
		$add_to_cll_btn_arg['add_to_coll_btn'] 	= str_replace( '{no_of_collections}', woo_cl_count_product_collections( $post->ID ), $add_to_cll_btn_arg['add_to_coll_btn'] );
	}

	$coll_list 	= '';

	//Get product collection text & user login
	$product_colls_text = get_option( 'cl_display_product_collections_text' );
	if ( ! is_shop() && is_user_logged_in() && strpos( $product_colls_text, '{collections}' ) !== false ) {

		//Check if variable product
		if ( $product_type == 'variable' || $product_type == 'variation' ) {

			//Get all child variations
		    $childrens   = $product->get_children(); 

		    //Child variations exists
		    if( !empty( $childrens ) ) {
			    foreach ( $childrens as $key => $var_id ) {

			    	//Get variation details
			    	$product_variatons = new WC_Product_Variation( $var_id );

			    	//Check variation exists or visible
			    	if ( $product_variatons->exists() && $product_variatons->variation_is_visible() ) {

						//Get collections
						$coll_ids[$var_id] = woo_cl_count_product_collections( $var_id, true, $user_ID );
			        }
			    }
			}

			//Check if collections exists
			if( !empty( $coll_ids ) ) {
				echo '<div class="woocl-product-collections woocl-clearfix">';
				foreach( $coll_ids as $key => $coll_id ) {

					$coll_list = '';//Init Default Blank

					if( !empty( $coll_id ) ) {
						foreach( $coll_id as $id ) {

							//Get detail
							$collection_name	= get_the_title( $id );
							$collection_url		= $woo_cl_model->woo_cl_get_collection_item_page_url( $id );
							$coll_list	.= ' <a href="'.$collection_url.'">'. $collection_name .'</a>,';
						}
					}

					//Check if collections exists then Display collections message
					if( !empty( $coll_list ) ) {
						echo '<div class="woocl-product-variable-product woocl-product-attribute-'. $key .'">';
						echo apply_filters( 'woo_cl_display_product_collections_text', str_replace( '{collections}', rtrim( $coll_list, ',' ), $product_colls_text ), $coll_ids );
						echo '</div>';
					}
				}
				echo '</div>';
			}

		} else { //For simple products

			//Get collections
			$coll_ids = woo_cl_count_product_collections( $post->ID, true, $user_ID );

			if( !empty( $coll_ids ) ) {
				foreach( $coll_ids as $coll_id ) {

					//Get detail
					$collection_name	= get_the_title( $coll_id );
					$collection_url		= $woo_cl_model->woo_cl_get_collection_item_page_url( $coll_id );
					$coll_list	.= ' <a href="'.$collection_url.'">'. $collection_name .'</a>,';
				}

				//Display collections message
				echo '<div class="woocl-product-collections woocl-clearfix">';
				echo apply_filters( 'woo_cl_display_product_collections_text', str_replace( '{collections}', rtrim( $coll_list, ',' ), $product_colls_text ), $coll_ids );
				echo '</div>';
			}
		}
	}

	//create add to collection button
	do_action( 'woo_cl_add_to_collection_button', $add_to_cll_btn_arg );
}

/**
 * Maximum collections per user
 * 
 * Return max number of collaction per user
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.1
 */
function woo_cl_max_collections_per_user() {
	
	global $current_user;
	
	//Get user Id
	$user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';
	
	// Get max collactions per user
	$max_collections = get_user_meta( $user_ID, 'cl_max_collections_per_user', true );
	
	// Assign global settings value
	if( $max_collections == '' ) {
		$max_collections = get_option( 'cl_max_collections_per_user' );
	}
	
	return apply_filters( 'woo_cl_max_collections_per_user', $max_collections, $user_ID );
}

/**
 * Maximum products per collection
 * 
 * Return max number of products per collection
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.1
 */
function woo_cl_max_products_per_collection() {
	
	global $current_user;
	
	//Get user Id
	$user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';
	
	// Get user meta for max products per collection
	$max_products = get_user_meta( $user_ID, 'cl_max_products_per_collection', true );
	
	// Assign global settings value
	if( $max_products == '' ) {
		$max_products = get_option( 'cl_max_products_per_collection' );
	}
	
	return apply_filters( 'woo_cl_max_products_per_collection', $max_products );
}

/**
 * User's Created Collections
 * 
 * Return number of collection user created
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.1
 */
function woo_cl_count_user_collections( $user_ID, $post_type, $public_only = false ) {

	$total_colls = 0;

	if( !empty( $user_ID ) ) {

		global $wpdb;

		//Build Query
		$query = "SELECT COUNT(*) FROM $wpdb->posts WHERE 1=1 AND $wpdb->posts.post_type = '". $post_type ."' ";
		$query .= "AND $wpdb->posts.post_author = $user_ID ";

		//Query based on public_only param
		if( $public_only ) $query .= "AND ( $wpdb->posts.post_status = 'publish' )";
		else $query .= "AND ( $wpdb->posts.post_status = 'publish' OR $wpdb->posts.post_status = 'private' )";

		//Get total colls
		$total_colls = $wpdb->get_var( $query );

	} else {
		
		$cl_token	= woo_cl_get_list_token();
		$cl_list	= woo_cl_get_guest_lists( $cl_token );
		
		$total_colls = count($cl_list);
	}
	return $total_colls;
}

/**
 * Count the number of collections which has product.
 * 
 * return number of collections which has product
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.1.5
 */
function woo_cl_count_product_collections( $productid, $return_ids = false, $user_id = false ) {

	global $wpdb;

	//Get meta prefix
	$prefix = WOO_CL_META_PREFIX;

	//Check if product id exists
	if( !empty( $productid ) ) {

		//Get field to be return
		$field = $return_ids ? 'c.ID' : 'count(*)';

		//Build Query for get collections counts
		$queryargs = "SELECT $field FROM $wpdb->posts AS c ";

		$queryargs .= "INNER JOIN $wpdb->posts AS cit ON ( cit.post_type = '". WOO_CL_POST_TYPE_COLLITEMS ."' AND c.ID = cit.post_parent ) ";

		$queryargs .= "INNER JOIN $wpdb->postmeta AS citm ON ( cit.ID = citm.post_id ) ";

		$queryargs .= "WHERE 1=1 AND c.post_type = '". WOO_CL_POST_TYPE_COLL ."' AND citm.meta_key = '". $prefix ."coll_product_id' AND citm.meta_value = $productid ";

		//Check if user id then added in query
		if( $user_id ) {
			$queryargs .= "AND c.post_author = $user_id";
		}

		//Get no of collections
	  	return $return_ids ? $wpdb->get_col( $queryargs ) : $wpdb->get_var( $queryargs );
	}

	return false;
}

/**
 * Check Guest Collection Enable Or Not
 * 
 * Handle to check guest collection enable or not
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.1
 */
function woo_cl_get_enable_guest_collection() {
	
	$cl_guest_options	= get_option( 'cl_guest_options' );
	
	if( $cl_guest_options == 'yes' ) {
		$return_value	= true;
	} else {
		$return_value	= false;
	}
	
	return apply_filters( 'woo_cl_get_enable_guest_collection', $return_value );
}

/**
 * Check User can update/edit/delete collection
 * 
 * Return true if user can edit collection
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.1
 */
function woo_cl_user_can_update_collection( $coll_id ) {
	
	$user_can = false;
	
	if ( !is_user_logged_in() ) {
		if( woo_cl_get_enable_guest_collection() ) {
			$user_can	= true;
		}
	} else {
		$user_can = true;
	}
	
	return apply_filters( 'woo_cl_user_can_update_collection', $user_can, $coll_id );
}

/**
 * Check collection limit of product
 * 
 * return true if collection limit is larger
 * or available space for collection
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.1
 */
function woo_cl_check_product_limit_per_collection( $coll_id ) {
	
	$check_return	= true;
	
	//Get Maximum products per collection
	$cl_max_products = woo_cl_max_products_per_collection();

	if( $cl_max_products != '' ) {//Check Maximum product per collection set

		//Arguments
		$itemargs = array(
			'post_parent' => $coll_id,
			'post_type'   => WOO_CL_POST_TYPE_COLLITEMS, 
			'numberposts' => -1,
			'post_status' => 'publish' 
		);

		//Get items of collection
		$items = get_children( $itemargs, ARRAY_A );
		
		if( count( $items ) >= $cl_max_products ) {//check total products in collection 

			$check_return = false;
		}
	}
	
	return apply_filters( 'woo_cl_check_product_limit_per_collection', $check_return, $coll_id );
}

/**
 * Check product is available in collection
 * 
 * return collection item if available
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.1
 */
function woo_cl_product_available_in_collection( $coll_id, $coll_productid, $author_id = '', $return_val = false ) {
	
	global  $woo_cl_model;
	
	//Get meta prefix
	$prefix = WOO_CL_META_PREFIX;
	
	//Get User Id
	if( empty($author_id ) ) {
		$author_id = isset( $current_user->ID ) ? $current_user->ID : '';
	}
	
	$args = array(
					'author' 		=> $author_id,
					'post_type' 	=> WOO_CL_POST_TYPE_COLLITEMS,
					'post_parent' 	=> $coll_id,
					'meta_query'	=> array(
												array( 
													'key' => $prefix.'coll_product_id', 
													'value' => $coll_productid
													)
											)
				 ); 
	
	//Get collection items
	$collection_items = $woo_cl_model->woo_cl_get_collection_items( $args );
	
	if( $return_val ) {
		if( !empty( $collection_items ) ) {
			$return_data	= true;
		} else {
			$return_data	= false;
		}
	} else {
		
		$return_data	= $collection_items;
	}
	
	return apply_filters( 'woo_cl_product_available_in_collection', $return_data, $coll_id, $coll_productid, $author_id, $return_val );
}

/**
 * Display My collections
 * 
 * Handle to display my collections
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.2
 */
function woo_cl_my_collections( $args = array() ) {
	
	global $current_user, $woo_cl_model;
	
	//Get current user Id
	if( isset($args['user_id']) && !empty( $args['user_id'] ) ) {
		$user_id = $args['user_id'];
	} else {
		$user_id = isset( $current_user->ID ) ? $current_user->ID : '';
		$args['user_id'] = $user_id;
	}
	
	// If account page then take global settings
	if( isset($args['posts_per_page']) && !empty( $args['posts_per_page'] ) ) {
		$per_page = $args['posts_per_page'];
	} else {
		$per_page	=  woo_cl_get_collection_per_page_count();
	}

	//Get fixed collection no next page
	$fixed_coll	= isset( $args['fixed_coll'] ) && $args['fixed_coll'] ? $args['fixed_coll'] : false;

	// Defaults collection argument
	$defaults	= apply_filters( 'woo_cl_my_collection_defaults', 
						array(
								'author' 		 => $user_id,
								'post_status'	 => array( 'publish', 'private' ),
								'posts_per_page' => $per_page,
								'paged'	 		 => 1,
								'fixed_coll'	 => $fixed_coll
							)
						);
	
	// merge arrays
	$args = wp_parse_args( $args, $defaults );
	
	$all_clll_count_arg	= array_merge( $args, array( 'getcount' => 1, 'posts_per_page' => -1 ) );
	$all_coll_count		= $woo_cl_model->woo_cl_get_collections( $all_clll_count_arg );
	
	//Get Collection of Page 1
	$collections = $woo_cl_model->woo_cl_get_collections( $args );
	
	do_action( 'woo_cl_collection_listing', $collections, $user_id, $all_coll_count, $args );
}

/**
 * Check per product collection button
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.2
 */
function woo_cl_collection_btn_isenable( $product_id, $variation_id = '' ) {
	
	$return = true;
	
	// Get meta prefix
	$prefix	= WOO_CL_META_PREFIX;
	
	// Get product meta for enable/disable button
	$enable_button	= get_post_meta( $product_id, $prefix.'hide_coll_btn', true );
	
	if( $enable_button == 'disable' ) { // check if button disable
		$return = false;
	}
	
	return apply_filters( 'woo_cl_collection_btn_isenable', $return, $product_id, $variation_id );
}

/**
 * Enable / Disable Collections Listing
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.3
 */
function woo_cl_is_enable_collections( $page_id = '' ) {
	
	return apply_filters( 'woo_cl_is_enable_collections', false, $page_id );
}

/**
 * Check Author is Enable Or Not
 * 
 * Handle to check author is enable / disable
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.3
 */
function woo_cl_display_author_enable() {
	
	return apply_filters( 'woo_cl_display_author_enable', true );
}

/**
 * Add to collection redirect link for non login user
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.4
 */
function woo_cl_redirect_link_for_non_login_user() {
	
	return apply_filters( 'woo_cl_redirect_link_for_non_login_user', wp_login_url( get_permalink() ) );
}

/**
 * Get the WC Product instance for a given product ID or post
 * get_product() is soft-deprecated in WC 2.2
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.7
 */
function woo_cl_wc_get_product( $the_product = false, $args = array() ) {
	
	if ( woo_cl_is_wc_version_gte_2_2() ) {
		return wc_get_product( $the_product, $args );
	} else {
		return get_product( $the_product, $args );
	}
}

/**
 * Returns true if the installed version of WooCommerce is 2.2 or greater
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.7
 */
function woo_cl_is_wc_version_gte_2_2() {
	return woo_cl_get_wc_version() && version_compare( woo_cl_get_wc_version(), '2.2', '>=' );
}

/**
 * Helper method to get the version of the currently installed WooCommerce
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.7
 */
function woo_cl_get_wc_version() {
	
	return defined( 'WC_VERSION' ) && WC_VERSION ? WC_VERSION : null;
}

/**
 * Helper method to get maximum columns
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.7
 */
function woo_cl_get_max_listing_columns() {

	return apply_filters( 'woo_cl_col_items_max_columns', 4 );
}