<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Templates Functions
 * 
 * Handles to manage templates of plugin
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

/**
 * Returns the path to the WooCommerce - Collections templates directory
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_templates_dir() {
	
	return apply_filters( 'woo_cl_template_dir', WOO_CL_DIR . '/includes/templates/' );
}

/**
 * Get other templates 
 * 
 * (e.g. WooCommerce - Collections attributes) passing attributes and including the file.
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_get_template( $template_name, $args = array(), $template_path = '', $default_path = '' ) {

	$plugin_absolute	= woo_cl_get_templates_dir();

	if ( ! $template_path ) {
		$template_path = WC()->template_path() . WOO_CL_BASENAME . '/';
	}

	wc_get_template( $template_name, $args, $template_path, $plugin_absolute );
}

/* **************************************************************************
   Add to Collection Button Template Functions
   ************************************************************************** */
if( !function_exists( 'woo_cl_add_to_collection_button_html' ) ) {
	
	/**
	 * Load Add To Collection Button full HTML Template
	 * 
	 * Handles to load add to collection Button full HTML Template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_add_to_collection_button_html( $add_to_cll_btn_arg = array() ) {
		
		//add to collection full HTML popup template
		woo_cl_get_template( 'collection-button/collection-button.php', $add_to_cll_btn_arg );
	}
}

/* **************************************************************************
   Add to Collection Popup Template Functions
   ************************************************************************** */

if( !function_exists( 'woo_cl_add_to_collection_popup_html' ) ) {
	
	/**
	 * Load Add To Collection Popup full HTML Template
	 * 
	 * Handles to load add to collection Popup full HTML Template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_add_to_collection_popup_html() {
		
		//add to collection full HTML popup template
		woo_cl_get_template( 'add-to-collection/add-to-collection.php' );
	}
}

if( !function_exists( 'woo_cl_popup_title' ) ) {
	
	/**
	 * Load Add To Collection Popup Content Template
	 * 
	 * Handles to load add to collection Title content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_popup_title( $product, $user_ID ) {
		
		//add to collection header popup template
		woo_cl_get_template( 'add-to-collection/add-to-collection-header.php' );
	}
}

if( !function_exists( 'woo_cl_popup_content' ) ) {
	
	/**
	 * Load Add To Collection Popup Content Template
	 * 
	 * Handles to load add to collection content content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_popup_content( $product, $user_ID ) {
		
		global $woo_cl_model;
		
		$model	= $woo_cl_model;
		
		//Get meta prefix
		$prefix 		= WOO_CL_META_PREFIX;
		
		//Get Product Type if exist
		$product_type = isset( $product->product_type ) ? $product->product_type : '';

		//get product id if exist
		if ( version_compare( WOOCOMMERCE_VERSION, "3.0.0" ) == -1 )
			$product_id	= isset( $product->id ) ? $product->id : '';
		else
			$product_id	= $product->get_id();
		
		//get product title if exist
		$product_disc 	= $product->get_title();

		if( $product_type == 'variation' ) {//check if product is variable
	
			//get product variation id if exist
			$product_id 	= isset( $product->variation_id ) ? $product->variation_id : '';
	
		}
		
		//Get Variation data
		$product_variation		= woo_cl_get_product_variation( $product_id, false );

		//Get image URL
		$product_imgurl		= $woo_cl_model->woo_cl_get_feature_image_url( $product_id );

		//Get Guest option
		$cl_guest_options = get_option( 'cl_guest_options' );

		//Get New Collection Privacy
		$cl_new_coll_privacy = get_option( 'cl_new_coll_privacy' );

		//Get New Collection Privacy front end
		$cl_front_coll_privacy = get_option( 'cl_front_coll_privacy' );
		
		if ( is_user_logged_in() ) { // check if user is logged in, then get collection private and public both
			$args = array(
							'author' 		=> $user_ID,
							'post_status'	=> array( 'publish', 'private' )
						  );	
		} elseif ( $cl_guest_options == 'yes' ) {

			//Get Collection guest tocken
			$cl_token = woo_cl_get_list_token();

			//Arguments for get guest collections
			$args	= array(
							'post_status'	=> array( 'publish', 'private' ),
							'meta_query'	=> array(
													array( 
														'key' 	=> $prefix.'cl_token', 
														'value' => $cl_token
														)
													)
						);
		} else {
			$args = array(
							'author' => $user_ID,
						 );	
		}
		
		//Get collections
		$collections	= $model->woo_cl_get_collections( $args );
		
		//Initilize collections data
		$collections_data	= array();
		
		if( !empty( $collections ) ) {
			
			foreach ( $collections as $collection ) {
				
				//Initilize single collection data
				$collection_data	= array();
				
				//get collection status
				$poststatus	= isset( $collection['post_status'] ) ? $collection['post_status'] : '';

				//if post status private then add icon class 
				$icon_class = $poststatus == 'private' ? 'class="prv"' : '';

				//argument for get collection item based on product id
				$args = array(
								'author' 		=> $user_ID,
								'post_parent' 	=> $collection['ID'],
								'meta_query'	=> array(
															array( 
																'key' => $prefix.'coll_product_id', 
																'value' => $product_id
																)
														)
							 ); 
				
				//Get collection item
				$collection_item	= $model->woo_cl_get_collection_items( $args );

				 // if item not available then option is disabled
				$option_element 	= !empty( $collection_item ) ? ' disabled ' : '';
				
				
				$collection_data['ID']				= isset( $collection['ID'] ) ? $collection['ID'] : '';
				$collection_data['post_title']		= isset( $collection['post_title'] ) ? $woo_cl_model->woo_cl_escape_attr( $collection['post_title'] ) : '';
				$collection_data['icon_class']		= $icon_class;
				$collection_data['option_element']	= $option_element;
				
				$collections_data[]	= $collection_data;
			}
		}
		
		$args = array(
					'product'			=> $product,
					'user_ID'			=> $user_ID,
					'product_id'		=> $product_id,
					'product_disc'		=> $woo_cl_model->woo_cl_escape_attr( $product_disc ),
					'product_imgurl'	=> $product_imgurl,
					'product_variation'	=> $product_variation,
					'collections_data'	=> $collections_data,
					'cl_new_coll_privacy' => $cl_new_coll_privacy,
					'cl_front_coll_privacy' => $cl_front_coll_privacy
				);
		
		//add to collection content popup template
		woo_cl_get_template( 'add-to-collection/add-to-collection-content.php', $args );
	}
}

if( !function_exists( 'woo_cl_popup_footer' ) ) {
	
	/**
	 * Load Add To Collection Popup Content Template
	 * 
	 * Handles to load add to collection footer content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_popup_footer( $product, $user_ID ) {

		//Get options
		$cl_front_coll_privacy = get_option( 'cl_front_coll_privacy' );
		$cl_new_coll_privacy = get_option( 'cl_new_coll_privacy' );

		$args = array(
					'cl_front_coll_privacy' => $cl_front_coll_privacy,
					'cl_new_coll_privacy' 	=> $cl_new_coll_privacy,
				);

		//add to collection footer popup template
		woo_cl_get_template( 'add-to-collection/add-to-collection-footer.php', $args );
	}
}

if( !function_exists( 'woo_cl_popup_success' ) ) {
	
	/**
	 * Load Add To Collection Popup for success message Template
	 * 
	 * Handles to load add to collection Popup for success message Template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_popup_success( $product_id, $coll_id , $product_title ) {

		global $woo_cl_model;

		//Get image URL
		//$product_imgurl	= wp_get_attachment_url( get_post_thumbnail_id($product_id, 'thumbnail') );
		$product_imgurl		= $woo_cl_model->woo_cl_get_feature_image_url( $product_id );
		
		//if product image url empty then set no image
		if( empty( $product_imgurl ) ) {
			$product_imgurl = WOO_CL_IMG_URL.'/woo-cl-no-item.gif';
		}
		
		$cl_item_listsurl	= '';
		$coll_title			= '';
		
		//if collection id is not empty then get url
		if( !empty( $coll_id ) ) {
			
			//Get collection Item page URL
			$cl_item_listsurl	= $woo_cl_model->woo_cl_get_collection_item_page_url( $coll_id );
			
			//Get Title of Collection
			$coll_title			= get_the_title( $coll_id );
			
		}
		
		$args = array(
					'coll_id'			=> $coll_id,
					'coll_title'		=> $woo_cl_model->woo_cl_escape_attr( $coll_title ),
					'product_title'		=> $woo_cl_model->woo_cl_escape_attr( $product_title ),
					'product_imgurl'	=> $product_imgurl,
					'cl_item_listsurl'	=> $cl_item_listsurl,
				);
		
		//add to collection full HTML popup template
		woo_cl_get_template( 'add-to-collection/success-message/add-to-collection-success.php', $args );
	}
}

/* **************************************************************************
   Collection Listing Template Functions
   ************************************************************************** */

if( !function_exists( 'woo_cl_collection_list_wrapper' ) ) {
	
	/**
	 * Load Collection Listing Content Template
	 * 
	 * Handles to load collection Listing content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_collection_list_wrapper( $collections, $user_ID, $all_coll_count, $others = array() ) {

		global $woo_cl_model, $current_user, $wp_query;
		
		//Get user endpoint
		$woo_cl_user_slug	= get_option( 'cl_collection_author_text' );
		$woo_cl_user_slug	= trim( $woo_cl_user_slug ) != '' ? $woo_cl_user_slug : 'collection-author';

		if( empty( $user_ID ) ) {//If current user is empty

			//Get user ID, Name
			$user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';
		}

		$author_coll_url = '';
		$single_user	 = '';

		//Get Collection user name from URL parameter
		$woo_cl_user	= get_query_var( $woo_cl_user_slug );
		
		if( !empty( $woo_cl_user ) ) {// if user name pass by url

			$single_user = $woo_cl_user;

			$author_coll_url  = $woo_cl_model->woo_cl_get_collection_page_url( $woo_cl_user );
		}

		//Get total collections
		$coll_total			= $all_coll_count;

		// If account page then take global settings
		if( isset($others['posts_per_page']) && !empty( $others['posts_per_page'] ) ) {
			$per_page_count = $others['posts_per_page'];
		} else {
			$per_page_count	=  woo_cl_get_collection_per_page_count();
		}

		if( !empty( $coll_total ) ) {
			$total_coll_pages	= ceil( $all_coll_count / $per_page_count );
		} else {
			$total_coll_pages	= 0;
		}
		
		//Get Login User name
		$user_login	= isset( $current_user->data->user_login ) ? $current_user->data->user_login : '';
		
		if( !empty( $user_login ) ) {//check if user name not empty

			$my_coll_url  = $woo_cl_model->woo_cl_get_collection_page_url( $user_login );
		} else {

			$my_coll_url  = $woo_cl_model->woo_cl_get_collection_page_url( 'guest' );
		}
		
		//get URL of all collection listing
		$all_coll_url  = $woo_cl_model->woo_cl_get_collection_page_url();

		//Get Collection author for Ajax coll loop
		$data_author = '';
		
		if( !empty( $woo_cl_user ) ) {
			$data_author = $user_ID;
		} elseif ( is_account_page() || (isset($others['user_id']) && !empty($others['user_id'])) ) {
			$data_author = isset( $current_user->ID ) ? $current_user->ID : '';
		}

		// Get others argument
		$collection_title	= isset($others['collection_title']) ? $others['collection_title'] : 'default';
		$show_count			= isset($others['show_count']) ? $others['show_count'] : 'false';
		$enable_see_all		= isset($others['enable_see_all']) ? $others['enable_see_all'] : 'false';

		//Get fixed collection no next page
		$fixed_coll	= isset( $others['fixed_coll'] ) && $others['fixed_coll'] ? $others['fixed_coll'] : false;

		//Make Arguments for follow collection args
		$follow_coll_author = get_option( 'cl_enable_follow_coll_author' );
		$coll_follow_args 	= ( $follow_coll_author == 'yes' && isset( $wp_query->query_vars['collection-author'] ) ) ? array( 'author_id'	=> $data_author ) : array();

		//Check if display search & sort
		$allow_search_sort = false;
		if( is_page( get_option( 'cl_coll_lists_page' ) ) ) {
			$allow_search_sort = true;
		}

		// Declare Sortable options array
		$sort_optns = array(
			'menu_order title-asc'	=> __( 'Custom Ordering + Title ( ASC )', 'woocl' ),
			'menu_order title-desc'	=> __( 'Custom Ordering + Title ( DESC )', 'woocl' ),
			'date-asc'		=> __( 'Date ( ASC )', 'woocl' ),
			'date-desc'		=> __( 'Date ( DESC )', 'woocl' ),
			'title-asc'		=> __( 'Title ( ASC )', 'woocl' ),
			'title-desc'	=> __( 'Title ( DESC )', 'woocl' ),
		);
		
		//Get sorting and search value if set
		$search 	= isset( $_GET['search'] ) ? $_GET['search'] : '';
		$sorting 	= isset( $_GET['sort'] ) ? $_GET['sort'] : '';

		//arguments pass to collection listing template
		$args = array(
						'collections'		=> $collections,
						'coll_total'		=> $coll_total,
						'user_id'			=> $user_ID,
						'single_user'		=> $single_user,
						'author_coll_url'	=> $author_coll_url,
						'all_coll_count'	=> $all_coll_count,
						'total_coll_pages'	=> $total_coll_pages,
						'my_coll_url'		=> $my_coll_url,
						'data_author'		=> $data_author,
						'all_coll_url'		=> $all_coll_url,
						'collection_title'	=> $collection_title,
						'show_count'		=> $show_count,
						'enable_see_all'	=> $enable_see_all,
						'fixed_coll'		=> $fixed_coll,
						'coll_follow_args'	=> $coll_follow_args,
						'sort_optns'		=> $sort_optns,
						'cl_enable_search'	=> get_option( 'cl_enable_search_on_collections' ),
						'cl_enable_sorting'	=> get_option( 'cl_enable_sorting_on_collections' ),
						'cl_search'			=> $search,
						'cl_sorting'		=> $sorting,
						'allow_search_sort'	=> apply_filters( 'woo_cl_allow_sorting_searching', $allow_search_sort ),
					);
		
		//collection listing template
		woo_cl_get_template( 'collection-listing/collection-listing.php', $args );
	}
}


if( !function_exists( 'woo_cl_collection_listing_loop_content' ) ) {
		
	/**
	 * Load Collection Item Content In Loop Template
	 * 
	 * Handles to load collection item content in loop template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_collection_listing_loop_content( $collections, $user_id ) {
		
		$args = array(
					'collections'	=> $collections,
					'user_id'		=> $user_id,
					'col_count'		=> 1,	
				);
		
		//collection listing template
		woo_cl_get_template( 'collection-listing/collection-listing-loop-content.php', $args );				
	}
}

if( !function_exists( 'woo_cl_collection_list_content' ) ) {

	/**
	 * Load Collection Listing block Content Template
	 * 
	 * Handles to load collection listing block content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_collection_list_content( $collection, $user_ID, $col_count ) {
		
		global $woo_cl_model, $current_user;
		
		$model	= $woo_cl_model;

		//Get user endpoint
		$woo_cl_user_slug	= get_option( 'cl_collection_author_text' );
		$woo_cl_user_slug	= trim( $woo_cl_user_slug ) != '' ? $woo_cl_user_slug : 'collection-author';
		
		//get prefix
		$prefix	= WOO_CL_META_PREFIX;
		
		//Get current user ID
		$curr_user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';
		
		$collection_id		= isset( $collection['ID'] ) ? $collection['ID'] : '';
		$collection_title	= isset( $collection['post_title'] ) ? $woo_cl_model->woo_cl_escape_attr( $collection['post_title'] ) : '';
		
		//Initilaize variables
		$status_icon_class	= $status_title_msg = $status_value = '';
		$max_columns		= woo_cl_get_max_listing_columns();

		if( $collection['post_status'] == 'publish' ) { //check collection is public or not
			
			$status_icon_class = 'public';
			$status_title_msg  = __('Public - visible to everyone','woocl');
			$status_value 	   = 'Public';
		}
		
		if( $collection['post_status'] == 'private' ) { //check collection is private or not
		
			$status_icon_class = 'private';
			$status_title_msg  = sprintf( __('This %s is private - only you can see it.','woocl'), woo_cl_get_label_singular() );
			$status_value 	   = 'Private';
		}
		
		//Get collection item page link
		$cl_item_listsurl	= $model->woo_cl_get_collection_item_page_url( $collection_id );

		//Get collection edit page link
		$cl_edit_pageurl	= $model->woo_cl_get_collection_edit_page_url( $collection_id );
		
		$author_enable		= woo_cl_display_author_enable();
		
		//Collection item arguments
		$args = array(
						'post_parent' 		=> $collection_id,
					);
		
		//get total collection items
		$coll_products = $woo_cl_model->woo_cl_get_collection_items( $args );
		
		//assign arguments to count arguments
		$countargs				= $args;
		
		//Get collection count arguments
		$countargs['getcount']	= 1;
		
		//get total Item in collection
		$product_total	   = $woo_cl_model->woo_cl_get_collection_items( $countargs );
		
		//Get Cover Image for collection
		$letest_product_imgurl = $woo_cl_model->woo_cl_get_cover_image_url( $collection_id );

		$author_coll_url = '';
		$single_user	 = '';
		
		//Get Collection user name from URL parameter
		$woo_cl_user	= isset( $_GET[$woo_cl_user_slug] ) ? $_GET[$woo_cl_user_slug] : '';

		if( empty( $woo_cl_user ) ) {// if user name pass by url

			if( !empty( $collection['post_author'] ) ) {
				$single_user = get_the_author_meta( 'user_login', $collection['post_author'] );
			} else {
				$single_user = 'guest';
			}

			$author_coll_url  = $woo_cl_model->woo_cl_get_collection_page_url( $single_user );
		}
			
		// for follower count
		$follow_count = '';
		
		//check follow my blog post is activated or not
		if( woo_cl_is_follow_activate() ) {
			
			// get followers counts
			$follow_count 	= wpw_fp_get_post_followers_count( $collection_id );
		}
		
		//Get Collection status
		$collection_status	= get_post_meta($collection['ID'], $prefix.'collection_status', true);
		
		// class variable for collection block
		$class_value	= '';
		
		// number of column per row
		$col_items_per_page	= woo_cl_get_collection_column_display();
		
		// set collection column per row
		if( isset( $col_items_per_page ) && $col_items_per_page <= $max_columns && $col_items_per_page > 0 ) {
			$class_value	.= $col_items_per_page;
		} else {
	 		$class_value	.= WOO_CL_NUM_COL_COLL_LIST;
		}
		
		if( $col_count%$col_items_per_page == 0 ) {// check for right margin
	 		$class_value	.= " right-margin ";
	 	}
	 	
		$col_left	= $col_count-1;
		
		if( $col_left%$col_items_per_page == 0 ) {// check for left margin
	 		$class_value	.= " left-margin";
	 	}
		
		//Template arguments
		$args	= array(
					'collection'		=> $collection,
					'collection_id'		=> $collection_id,
					'follow_count'		=> $follow_count,
					'collection_title'	=> apply_filters( 'woo_cl_collection_block_title', $collection_title, $collection_id ),
					'cl_item_listsurl'	=> $cl_item_listsurl,
					'cl_edit_pageurl'	=> $cl_edit_pageurl,
					'status_icon_class'	=> $status_icon_class,
					'status_title_msg'	=> $status_title_msg,
					'status_value'		=> $status_value ,
					'curr_user_ID'		=> $curr_user_ID,
					'single_user'		=> $single_user,
					'author_coll_url'   => $author_coll_url,
					'product_total' 	=> $product_total,
					'product_imgurl'	=> $letest_product_imgurl,
					'coll_status'		=> $collection_status,
					'class_value'		=> $class_value,
					'author_enable'		=> $author_enable,
				);
		
		//collection listing block template
		woo_cl_get_template( 'collection-listing/collection-listing-content.php', $args );
	}
}

/* **************************************************************************
   Collection Product Listing Template Functions
   ************************************************************************** */

if( !function_exists( 'woo_cl_collection_item_list_wrapper' ) ) {
	
	/**
	 * Load Collection Listing Content Template
	 * 
	 * Handles to load collection listing content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_collection_item_list_wrapper( $coll_items, $coll_id, $all_coll_item_count ) {

		global $woo_cl_model, $current_user;
		
		//Get model object
		$model	= $woo_cl_model;
		
		//Meta Prefix
		$prefix = WOO_CL_META_PREFIX;
		
		//Get User ID
		$user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';
		
		//get perticular collection
		$coll_data	= get_post( $coll_id );
		
		//get collection author if exist
		$coll_author	= isset( $coll_data->post_author ) ? $coll_data->post_author : '';
		
		//Current user is owner of collection ot not
		$owner	= false;
		
		if( ( !empty( $coll_author ) && $coll_author == $user_ID ) || woo_cl_is_users_list( $coll_id, true ) ) {
			
			$owner	= true;
		}
		
		//Get Title, content of Collection
		$coll_title		= isset( $coll_data->post_title ) ? $coll_data->post_title : '';
		$coll_content	= isset( $coll_data->post_content ) ? $coll_data->post_content : '';
		$coll_status	= isset( $coll_data->post_status ) ? $coll_data->post_status : '';
		
		// if empty then describe content
		$coll_content = !empty( $coll_content ) ? $coll_content : sprintf( __('Describe your %s. Say something about why it was created or why it is special.','woocl'), woo_cl_get_label_singular() ); 

		//Get date formated
		$coll_modified_date = $woo_cl_model->woo_cl_get_date_format( $coll_data->post_modified );
		
		//Get collection edit page link
		$cl_edit_pageurl	= $model->woo_cl_get_collection_edit_page_url( $coll_id );
		
		//Collection Items arguments
		$countargs = array(
							//'author' 		=> $user_ID,
							'post_parent' 	=> $coll_id,
							'getcount'		=> 1,
						);
		
		//Get Collection Item datas
		$coll_item_total= $woo_cl_model->woo_cl_get_collection_items($countargs); //get total Item in collection
		
		// for follower count
		$follow_count = '';
		
		//check follow my blog post is activated or not
		if( woo_cl_is_follow_activate() ) {
			
			// get followers counts
			$follow_count 	= wpw_fp_get_post_followers_count( $coll_id );
		}
		
		//Initialize variables
		$user_name	= $author_coll_url = $author_avatar = '';
		
		if( !empty( $coll_author ) ) {// if author available
			
			//get user name from user id
			$user_name = get_the_author_meta( 'user_login', $coll_author );
			
			//get collection page url of perticular user
			$author_coll_url  = $woo_cl_model->woo_cl_get_collection_page_url( $user_name );
			
			//get avatar of user with size 32
			$author_avatar	  = get_avatar( $coll_author, 55 );
		}

		$per_page_count	= woo_cl_get_collection_per_page_count();
		if( !empty( $per_page_count ) ) {
			$total_coll_item_pages	= ceil( $all_coll_item_count / $per_page_count );
		} else {
			$total_coll_item_pages	= 0;
		}

		//Get collectyion sub title from option
		$collection_subtitle_text = get_option( 'cl_collection_subtitle_text' );
		if( strpos( $collection_subtitle_text, '{total_price}' ) !== false ) {
			$collection_subtitle_text = str_replace( '{total_price}', $woo_cl_model->woo_cl_collection_products_total_price( $coll_id ), $collection_subtitle_text );
		}

		//Replace template tags values
		$collection_subtitle_text = str_replace( '{total_items}', $coll_item_total, $collection_subtitle_text );

		//Get options
		$cl_bulk_add_to_cart 	= get_option( 'cl_enable_coll_add_all_to_cart' );
		$cl_scroll_options		= get_option( 'cl_scroll_options' );

		// Declare Sortable options array
		$sort_optns = array(
			'menu_order title-asc'	=> __( 'Custom Ordering + Title ( ASC )', 'woocl' ),
			'menu_order title-desc'	=> __( 'Custom Ordering + Title ( DESC )', 'woocl' ),
			'date-asc'		=> __( 'Date ( ASC )', 'woocl' ),
			'date-desc'		=> __( 'Date ( DESC )', 'woocl' ),
			'title-asc'		=> __( 'Title ( ASC )', 'woocl' ),
			'title-desc'	=> __( 'Title ( DESC )', 'woocl' ),
		);
		//Get sorting and search value if set
		$search 	= isset( $_GET['search'] ) ? $_GET['search'] : '';
		$sorting 	= isset( $_GET['sort'] ) ? $_GET['sort'] : '';
		//Collection Items listing template arguments
		$args = array(
					'coll_id'				=> $coll_id,
					'coll_items'			=> $coll_items,
					'owner'					=> $owner,
					'coll_author'			=> $coll_author,
					'curr_user_ID'			=> $user_ID,
					'user_name'				=> $user_name,
					'author_coll_url'		=> $author_coll_url,
					'author_avatar'			=> $author_avatar,
					'follow_count'			=> $follow_count,
					'coll_title'			=> $coll_title,
					'coll_content'			=> $coll_content,
					'coll_item_total'		=> $coll_item_total,
					'coll_modified_date'	=> $coll_modified_date,
					'cl_edit_pageurl'		=> $cl_edit_pageurl,
					'coll_status'			=> $coll_status,
					'all_coll_item_count'	=> $all_coll_item_count,
					'total_coll_item_pages'	=> $total_coll_item_pages,
					'collection_subtitle_text' => $collection_subtitle_text,
					'cl_bulk_add_to_cart'	=> $cl_bulk_add_to_cart,
					'cl_scroll_options'		=> $cl_scroll_options,
					'sort_optns'			=> $sort_optns,
					'cl_enable_search'		=> get_option( 'cl_enable_search_on_collection_items' ),
					'cl_enable_sorting'		=> get_option( 'cl_enable_sorting_on_collection_items' ),
					'cl_search'				=> $search,
					'cl_sorting'			=> $sorting,
				);

		//collection listing template
		woo_cl_get_template( 'collection-item-listing/collection-item-listing.php', $args );
	}
}

if( !function_exists( 'woo_cl_collection_item_listing_loop_content' ) ) {
		
	/**
	 * Load Collection Item Content In Loop Template
	 * 
	 * Handles to load collection item content in loop template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_collection_item_listing_loop_content( $coll_items, $coll_id, $collection_image = true ) {
		
		//Collection Items listing template arguments
		
		$args = array(
					'coll_items'	=> $coll_items,
					'collection_image' => $collection_image,
					'coll_id'		=> $coll_id,
					'product_count'	=> apply_filters( 'woo_cl_collection_item_listing_product_count', 1 , $coll_id ),
				);
		
		//collection listing template
		
		woo_cl_get_template( 'collection-item-listing/collection-item-listing-loop-content.php', $args );				
	}
}

if( !function_exists( 'woo_cl_product_list_content' ) ) {
	
	/**
	 * Load Collection Item List Content Template
	 * 
	 * Handles to load collection item list content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_product_list_content( $coll_item, $coll_id, $collection_image=false, $product_count ) {
		
		global $woo_cl_model;
		
		//Get Meta Prefix
		$prefix	= WOO_CL_META_PREFIX;
		
		if( !empty( $coll_item ) ) {
			
			//Get Collection Content
			$coll_disc	=	$woo_cl_model->woo_cl_get_limit_char( $coll_item['post_content'] );
			
			//Get Product Id
			$curr_coll_product_id	= get_post_meta( $coll_item['ID'], $prefix.'coll_product_id', true );

			//Get product type allowed
			$allow_product_type		= $woo_cl_model->woo_cl_allowed_product_type( $curr_coll_product_id );
			
			if( $allow_product_type == false ) { // check if simple, variation product
				return;
			}
			
			$woo_cl_coll_item_variation_meta 	= get_post_meta( $coll_item['ID'], $prefix.'coll_product_selected_variations', true );
			$woo_cl_coll_item_variation 		= !empty( $woo_cl_coll_item_variation_meta ) ? unserialize( $woo_cl_coll_item_variation_meta ) : '';
			
			if( !empty( $woo_cl_coll_item_variation ) ) {

				//Get Variation data
				$product_variation 		= wc_get_formatted_variation( $woo_cl_coll_item_variation, false );
			} else {
				
				//Get Variation data
				$product_variation		= woo_cl_get_product_variation( $curr_coll_product_id, false );
			}
			
			//Get product page URL
			$product_url			= woo_cl_get_product_url( $curr_coll_product_id );
			
			//Get product price
			$product_price			= $woo_cl_model->woo_cl_collection_product_price( $curr_coll_product_id, true );
			
			//Get Product Image URL
			$product_imgurl			= $woo_cl_model->woo_cl_get_feature_image_url( $curr_coll_product_id );
			
			//Get Prodcut Small Size Image URL
			$product_small_imgurl	= $woo_cl_model->woo_cl_get_feature_small_image_url($curr_coll_product_id);
						
			if( empty( $product_imgurl ) ) {//If URL is empty
				
				//Get placeholder image URL
				$product_imgurl = WOO_CL_IMG_URL.'/woo-cl-no-item.gif';
			}

			//class of add to collection button
			$add_to_collbtn_class = 'woocl-add-to-collbtn';
			
			//Current user is owner of collection or not
			$is_not_login	= false;

			if( !is_user_logged_in() && get_option( 'cl_guest_options' ) != 'yes' ) {
				$is_not_login		  = true;
				$add_to_collbtn_class 	  = '';
			} else if( !is_user_logged_in() && get_option( 'cl_guest_options' ) == 'yes' ) {
				$is_not_login		  = true;
			}
			
			//Get login Url
			$login_url	= wp_login_url( get_permalink() );
		}
		
		// number of column per row
		$col_items_per_page=woo_cl_get_collection_column_display();

		//Initialize variables
		$class_value="";
		$max_columns		= woo_cl_get_max_listing_columns();
		
		// set collection column per row
		if( isset($col_items_per_page) && $col_items_per_page <= $max_columns && $col_items_per_page > 0 ){  
			$class_value	.= $col_items_per_page;
		}else{
			 $class_value	.= WOO_CL_NUM_COL_COLL_LIST;
		} 
		if($product_count%$col_items_per_page==0){// check for right margin
		 	$class_value	.= " right-margin";
		}
		$col_left=$product_count-1;
		
		if($product_count==1){ // check for left margin
			$class_value	.= " left-margin";
		}
	
		else if($col_left%$col_items_per_page==0){ // check for left margin
		 	$class_value.= " left-margin";
		}
		//Template arguments
		$args = array(
					'coll_id'			=> $coll_id,
					'coll_item'			=> $coll_item,
					'coll_title'		=> $woo_cl_model->woo_cl_escape_attr( $coll_item['post_title'] ),
					'coll_disc'			=> $coll_disc,
					'is_not_login'		=> $is_not_login,
					'login_url'			=> $login_url,
					'add_collbtn_class' => $add_to_collbtn_class,
					'product_imgurl'	=> $product_imgurl,
					'product_id'		=> $curr_coll_product_id,
					'product_url'		=> $product_url,
					'product_price'		=> $product_price,
					'product_variation'	=> $product_variation,
					'product_count' 	=> $product_count,
					'class_value'		=> $class_value,
					'product_small_imgurl' 	=> $product_small_imgurl,
				);
		if($collection_image==false){
		
		//collection item listing template
		woo_cl_get_template( 'collection-item-listing/collection-item-listing-content.php', $args );
		
		}
		else {
			
			//collection item listing template
			woo_cl_get_template( 'collection-item-listing/collection-item-image-listing-content.php', $args );
		
			
		}
	}
}

/* **************************************************************************
   Collection Edit Template Functions
   ************************************************************************** */

if( !function_exists( 'woo_cl_edit_collection_wrapper' ) ) {
	
	/**
	 * Load Collection Edit Content Template
	 * 
	 * Handles to load collection edit content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_edit_collection_wrapper( $coll_items, $coll_id ) {

		global $woo_cl_model, $current_user;

		//Meta Prefix
		$prefix = WOO_CL_META_PREFIX;

		//get perticular collection
		$coll_data	= get_post( $coll_id );
		
		//Get date formated
		$coll_modified_date = $woo_cl_model->woo_cl_get_date_format( $coll_data->post_modified );

		//Check privacy on front option enabled
		$collection_status = $collection_status_text = $status_icon = '';
		$front_coll_privacy = get_option( 'cl_front_coll_privacy' );
		if( $front_coll_privacy == 'yes' ) {

			//Get collection status
			$collection_status	= isset( $coll_data->post_status ) ? $coll_data->post_status : '';

			if( $collection_status == 'publish' ) {//if status publish then public text
				$collection_status_text = sprintf( __( ' This %s is Public', 'woocl' ), woo_cl_get_label_singular() );
				$status_icon			= '';
			}

			if( $collection_status == 'private' ) {//if status private then private text
				$collection_status_text = sprintf( __( ' This %s is Private', 'woocl' ), woo_cl_get_label_singular() );
				$status_icon			= 'woocl-pr-icon';
			}
		}

		//Get Title, content of Collection
		$coll_title   = isset( $coll_data->post_title ) ? $coll_data->post_title : '';
		$coll_content = isset( $coll_data->post_content ) ? $coll_data->post_content : '';

		//get collection author if exist
		$coll_author	= isset( $coll_data->post_author ) ? $coll_data->post_author : '';
		
		//Initialize variables
		$user_name = $author_coll_url = $author_avatar = '';
		
		if( !empty( $coll_author ) ) {// if author available
			
			$user_name = get_the_author_meta( 'user_login', $coll_author );
			
			$author_coll_url  = $woo_cl_model->woo_cl_get_collection_page_url( $user_name );
			
			//get avatar of user with size 32
			$author_avatar	  = get_avatar( $coll_author, 55 );
		}

		//Collection Items arguments
		$countargs = array(
							'post_parent' 	=> $coll_id,
							'getcount'		=> 1,
						);
		
		//Get Collection Item datas
		$coll_item_total= $woo_cl_model->woo_cl_get_collection_items($countargs); //get total Item in collection
		$enable_editor_coll_desc = get_option( 'cl_enable_editor_coll_desc' );
		$coll_content = $enable_editor_coll_desc == 'yes' ? $coll_content : $woo_cl_model->woo_cl_escape_attr( $coll_content );

		//Collection edit template arguments
		$args = array(
					'coll_id'			 => $coll_id,
					'coll_items'		 => $coll_items,
					'coll_item_total'	 => $coll_item_total,
					'coll_modified_date' => $coll_modified_date,
					'user_name'		     => $user_name,
					'author_coll_url'    => $author_coll_url,
					'author_avatar'    	 => $author_avatar,
					'coll_title' 		 => $woo_cl_model->woo_cl_escape_attr( $coll_title ),
					'coll_content' 		 => $coll_content,
					'coll_status'		 => $collection_status,
					'coll_status_text'	 => $collection_status_text,
					'coll_status_class'	 => $status_icon,
					'front_coll_privacy' => $front_coll_privacy,
					'col_count'			 => 1,
					'enable_editor_coll_desc' => $enable_editor_coll_desc
				);
		
		//collection edit template
		woo_cl_get_template( 'collection-edit/collection-edit.php', $args );
	}
}

if( !function_exists( 'woo_cl_edit_collection_content' ) ) {
	
	/**
	 * Load Collection Edit Content Template
	 * 
	 * Handles to load collection edit content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_edit_collection_content( $coll_item, $coll_item_total, $col_count ) {
		
		global $woo_cl_model;
		
		//Get Meta Prefix
		$prefix = WOO_CL_META_PREFIX;
		
		if( !empty( $coll_item ) ) {
	
			//Get Product Id
			$curr_coll_product_id = get_post_meta( $coll_item['ID'], $prefix.'coll_product_id', true );

			$allow_product_type = $woo_cl_model->woo_cl_allowed_product_type( $curr_coll_product_id );//check product type
			
			if( $allow_product_type == false ) { // check if allowed product type
				return;
			}
			
			//Get Variation data
			$product_variation		= woo_cl_get_product_variation( $curr_coll_product_id, false );
			
			//Get Product Image URL
			$product_imgurl 		= $woo_cl_model->woo_cl_get_feature_image_url( $curr_coll_product_id );

			//Get product page URL
			$product_url   = woo_cl_get_product_url( $curr_coll_product_id );

			$product_price = $woo_cl_model->woo_cl_collection_product_price( $curr_coll_product_id, true );//get product price
	
		}
		
		//class of add to collection button
		$add_to_collbtn_class = 'woocl-add-to-collbtn';
		
		if( !is_user_logged_in() ) {

			$add_to_collbtn_class = '';
		}

		//Initialize variables
		$class_value="";
		$max_columns		= woo_cl_get_max_listing_columns();

		// number of column per row
		$col_items_per_page=woo_cl_get_collection_column_display();
		
		// set collection column per row
		if( isset($col_items_per_page) && $col_items_per_page <= $max_columns && $col_items_per_page > 0 ){  
			$class_value.=$col_items_per_page;
		}else{
		 	$class_value.=WOO_CL_NUM_COL_COLL_LIST;
		 }
		 if($col_count%$col_items_per_page==0){// check for right margin
		 	$class_value.= " right-margin";
		 }
		
		 $col_left=$col_count-1;
		
		if($col_left%$col_items_per_page==0){ // check for left margin
		 	$class_value.= " left-margin";
		 }		
		
		
		$args = array(
					'coll_item'			=> $coll_item,
					'coll_item_total'	=> $coll_item_total,
					'add_collbtn_class' => $add_to_collbtn_class,
					'product_imgurl'	=> $product_imgurl,
					'product_id'		=> $curr_coll_product_id,
					'product_url'		=> $product_url,
					'product_price'		=> $product_price,
					'product_variation'	=> $product_variation,
					'class_value'		=> $class_value,
				);
		
		//collection edit content template
		woo_cl_get_template( 'collection-edit/collection-edit-content.php', $args );
	}
}

if( !function_exists( 'woo_cl_item_details_popup_content' ) ) {
	
	/**
	 * Load Collection Item details Template
	 * 
	 * Handles to load collection item detail popup content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_item_details_popup_content( $coll_item_id, $product, $coll_id ) {
		
		global $woo_cl_model, $woocommerce;
		
		$prefix	= WOO_CL_META_PREFIX;
		
		//get product id if exist
		if ( version_compare( WOOCOMMERCE_VERSION, "3.0.0" ) == -1 )
			$product_id	= isset( $product->id ) ? $product->id : '';
		else
			$product_id	= $product->get_id();
		
		//Get Woocommerce Cart page url
		$cart_page_url = $woocommerce->cart->get_cart_url();		
		
		$curr_coll_product_id	= get_post_meta( $coll_item_id , $prefix.'coll_product_id', true );
				
		//Get Url of Product
		$product_url 	= woo_cl_get_product_url( $curr_coll_product_id );
		
		
		//Get Product Type if exist
		$product_type = isset( $product->product_type ) ? $product->product_type : '';
		
		if( $product_type == 'variation' ) {//check if product is variable
	
			//get product variation id if exist
			$product_id 	= isset( $product->variation_id ) ? $product->variation_id : '';
	
		}

		$woo_cl_coll_item_variation_meta 	= get_post_meta( $coll_item_id, $prefix.'coll_product_selected_variations', true );
		$woo_cl_coll_item_variation 		= maybe_unserialize( $woo_cl_coll_item_variation_meta );
			
		if( !empty( $woo_cl_coll_item_variation ) ) {
			//Get Variation data
			$product_variation 		= wc_get_formatted_variation( $woo_cl_coll_item_variation, false );
		} else {
			//Get Variation data
			$product_variation	= woo_cl_get_product_variation( $product_id, false );
		}

		//Get Collection Item Id
		$coll_item_id 		= isset( $coll_item_id ) ? $coll_item_id : '';
		
		//get collection Item title if exist
		$coll_item_title 	= get_the_title( $coll_item_id );
		
		//Get image URL
		$product_imgurl		= $woo_cl_model->woo_cl_get_feature_image_url( $product_id );

		//Get collection item page link
		$cl_item_listsurl	= $woo_cl_model->woo_cl_get_collection_item_page_url( $coll_id );
		
		//Get product type is allowed ot not
		$allow_product_type	= $woo_cl_model->woo_cl_allowed_product_type( $product_id );
		
		$saved_price = '';
		$saved_per	 = 0;
		
		if( $product->is_on_sale() && $allow_product_type ) {

			//get regular price and sale price if exist
			$regular_price 	= isset( $product->regular_price ) ? $product->regular_price : '';
			$sale_price 	= isset( $product->sale_price ) ? $product->sale_price : '';
			
			//Saved price data
			$saved_amount	= $regular_price - $sale_price;
			$saved_price 	= wc_price( $saved_amount );//get formatted saved price
			
			if( !empty( $regular_price ) && !empty( $sale_price ) && ( $saved_amount > 0 ) ) {//check if both price are not empty
				
				$saved_per		= round( ( ( $regular_price - $sale_price ) / $regular_price ) * 100 );//get saved percentage
			}
		}

		//Get items available for sale
		$sale_stock	= $product->is_in_stock() ? sprintf( __('%s In Stock', 'woocl'), $product->get_stock_quantity() ) : 'Out of Stock';

		//Get SKU of product
		$sku		= $product->get_sku();
		$product_sku= !empty( $sku ) ? $sku : __('N/A','woocl');

		//Get Weight of product
		$product_weight	= $product->get_weight();

		//Get Dimensions of product
		$product_dimention	= $product->get_dimensions();

		//Get Dimensions of product
		$product_categories	= $product->get_categories();

		//Get Dimensions of product
		$product_tags	= $product->get_tags();

		//Get Product Description
		$product_desc	= !empty( $product->post->post_excerpt ) ? $product->post->post_excerpt : $product->post->post_content;
		$product_desc	= $woo_cl_model->woo_cl_get_limit_char( $product_desc );

		//Get product price
		$product_price	= $woo_cl_model->woo_cl_collection_product_price( $product_id, true );
		
		//For param pass only product id & variation id if exist
		if ( version_compare( WOOCOMMERCE_VERSION, "3.0.0" ) == -1 )
			$product_id	= isset( $product->id ) ? $product->id : '';
		else
			$product_id	= $product->get_id();

		$variation_id 	= isset( $product->variation_id ) ? $product->variation_id : '';

		$external_url	= $product->is_type('external') ? $product->get_product_url() : '';
		
		$purchasable 	= $product->is_purchasable() ? $product->is_purchasable() : '';
		$in_stock	 	= $product->is_in_stock() ? $product->is_in_stock() : '';

		//Collection item detail popup template arguments
		$args = apply_filters( 'woo_cl_add_item_details_data', array(
						'coll_id'			=> $coll_id,
						'coll_item_id'		=> $coll_item_id,
						'coll_item_title'	=> $woo_cl_model->woo_cl_escape_attr( $coll_item_title ),
						'product'			=> $product,
						'product_id'		=> $product_id,
						'product_imgurl'	=> $product_imgurl,
						'product_url'		=> $product_url,
						'external_url'		=> $external_url,
						'purchasable'		=> $purchasable,
						'in_stock'			=> $in_stock,
						'coll_item_url'		=> $cl_item_listsurl,
						'product_variation'	=> $product_variation,
						'selected_variation'=> $woo_cl_coll_item_variation,
						'product_price'		=> $product_price,
						'saved_price'		=> $saved_price,
						'saved_per'			=> $saved_per,
						'sale_stock'		=> $sale_stock,
						'product_sku'		=> $product_sku,
						'product_desc'		=> $product_desc,
						'product_weight'	=> $product_weight,
						'product_dimention'	=> $product_dimention,
						'product_categories'=> $product_categories,
						'product_tags'		=> $product_tags,
						'variation_id'		=> $variation_id,
						'cart_page_url'		=> $cart_page_url,
						'is_published'		=> ( $product->post->post_status == 'publish' ) ? true : false,
						'is_trashed'		=> ( $product->post->post_status == 'trash' ) ? true : false
					), $coll_item_id, $product, $coll_id );
					
		//collection item detail popup template
		woo_cl_get_template( 'collection-edit/popups/item-details.php', $args );
	}
}

if( !function_exists( 'woo_cl_delete_collection_popup_content' ) ) {
	
	/**
	 * Load Collection delete Template
	 * 
	 * Handles to load collection delete popup content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_delete_collection_popup_content( $coll_items, $coll_id ) {

		//Collection delete popup template arguments
		$args = array(
						'coll_id'	=> $coll_id
					);
					
		//collection delete popup template
		woo_cl_get_template( 'collection-edit/popups/collection-delete.php', $args );
	}
}

if( !function_exists( 'woo_cl_cover_image_popup_content' ) ) {
	
	/**
	 * Load Collection Cover Image Template
	 * 
	 * Handles to load collection cover image popup content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_cover_image_popup_content( $coll_items, $coll_id ) {

		//Collection cover image popup template arguments
		$args = array(
						'coll_id'	=> $coll_id
					);
					
		//collection cover image popup template
		woo_cl_get_template( 'collection-edit/popups/cover-image.php', $args );
	}
}

if( !function_exists( 'woo_cl_status_popup_content' ) ) {
	
	/**
	 * Load Collection Status Template
	 * 
	 * Handles to load collection status popup content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_status_popup_content( $coll_items, $coll_id ) {

		//Check if front collection privacy option not enabled
		$cl_front_coll_privacy = get_option( 'cl_front_coll_privacy' );
		if( $cl_front_coll_privacy != 'yes' ) return;

		//Get Collection Status
		$coll_status = get_post_status( $coll_id );
		
		if( $coll_status == 'publish' ) {//if status publish then public text
			
			$collection_status_text = sprintf( __( ' to make this a private %s?', 'woocl' ), woo_cl_get_label_singular() );
			$coll_status_subtext 	= sprintf( __( 'Other members will not able to view your %s.', 'woocl' ), woo_cl_get_label_singular() );
			$coll_status_btntext 	= __( 'Yes, make Private', 'woocl' );
			$coll_status_change		= 'private';
		}
		
		if( $coll_status == 'private' ) {//if status private then private text
			
			$collection_status_text = sprintf( __( ' to make this a public %s?', 'woocl' ), woo_cl_get_label_singular() );
			$coll_status_subtext 	= sprintf( __( 'Other members will be able to view and follow your %s.', 'woocl' ), woo_cl_get_label_singular() );
			$coll_status_btntext 	= __( 'Yes, make Public', 'woocl' );
			$coll_status_change		= 'publish';
		}
		
		$args = array(
						'coll_id'		  	  => $coll_id,
						'coll_status_val'	  => $coll_status_change,
						'coll_status_text'	  => $collection_status_text,
						'coll_status_subtext' => $coll_status_subtext,
						'coll_status_btntext' => $coll_status_btntext
					);
		
		//collection status popup template
		woo_cl_get_template( 'collection-edit/popups/status.php', $args );
	}
}

/* **************************************************************************
   Featured Collection Listing Template Functions
   ************************************************************************** */

if( !function_exists( 'woo_cl_featured_collection_list_wrapper' ) ) {
	
	/**
	 * Load Featured Collection Listing Content Template
	 * 
	 * Handles to load featured collection Listing content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_featured_collection_list_wrapper( $collections, $user_ID, $fcolls_title ) {

		global $woo_cl_model, $current_user;

		//Get user endpoint
		$woo_cl_user_slug	= get_option( 'cl_collection_author_text' );
		$woo_cl_user_slug	= trim( $woo_cl_user_slug ) != '' ? $woo_cl_user_slug : 'collection-author';
		
		if( empty( $user_ID ) ) {//If current user is empty
			
			//Get user ID, Name
			$user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';
		}

		//Get total collections
		$coll_total	= count( (array) $collections );
		
		//arguments pass to collection listing template
		$args = array(
						'collections'	  => $collections,
						'coll_total'	  => $coll_total,
						'user_id'		  => $user_ID,
						'fcolls_title'	  => $woo_cl_model->woo_cl_escape_attr( $fcolls_title ),
					);
		
		//featured collection listing template
		woo_cl_get_template( 'featured-collection-listing/featured-collection-listing.php', $args );
	}
}

if( !function_exists( 'woo_cl_featured_collection_list_content' ) ) {

	/**
	 * Load Featured Collection Listing block Content Template
	 * 
	 * Handles to load featured collection listing block content template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_featured_collection_list_content( $collection, $user_ID ) {

		global $woo_cl_model, $current_user;

		$model	= $woo_cl_model;

		//Get user endpoint
		$woo_cl_user_slug	= get_option( 'cl_collection_author_text' );
		$woo_cl_user_slug	= trim( $woo_cl_user_slug ) != '' ? $woo_cl_user_slug : 'collection-author';
		
		//get prefix
		$prefix	= WOO_CL_META_PREFIX;

		//Default image URL
		$default_imgurl = WOO_CL_IMG_URL.'/woo-cl-no-item.gif';				

		//Get current user ID
		$curr_user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';

		//Get Collection id, title, content
		$collection_id		= isset( $collection['ID'] ) ? $collection['ID'] : '';
		$collection_title	= isset( $collection['post_title'] ) ? $woo_cl_model->woo_cl_escape_attr( $collection['post_title'] ) : '';
		$collection_disc	= isset( $collection['post_content'] ) ? $woo_cl_model->woo_cl_nohtml_kses( $collection['post_content'] ) : '';

		//Get collection item page link
		$cl_item_listsurl	= $model->woo_cl_get_collection_item_page_url( $collection_id );

		//get limit content with read more icon
		$collection_disc	=  $woo_cl_model->woo_cl_get_limit_char( $collection_disc, 100, true, $cl_item_listsurl );

		//Collection item arguments
		$args = array(
						'post_parent' 		=> $collection_id,
					);

		//get total collection items
		$coll_products = $woo_cl_model->woo_cl_get_collection_items( $args );

		//get total Item in collection
		$product_total	   = count( $coll_products );

		if( !empty( $coll_products ) ) {//if collection item is empty

			$product_imgurl	= array();//Initialize variable

			for( $index = 0;$index < 4;$index++ ) {//loop for get four collection items image

				if( !isset( $coll_products[$index] ) ) {

					$product_imgurl['imgurl_' . $index ] = $default_imgurl;	
					continue;
				}

				//Get letest product Id & image URL
				$curr_coll_product_id	= get_post_meta( $coll_products[$index]['ID'], $prefix.'coll_product_id', true );				
				$letest_product_imgurl	= $woo_cl_model->woo_cl_get_feature_image_url( $curr_coll_product_id );
				
				$product_imgurl['imgurl_' . $index ]	= !empty( $letest_product_imgurl ) ? $letest_product_imgurl : $default_imgurl;				
			}

			$min_price	=	$woo_cl_model->woo_cl_get_min_price( $collection_id );

			//Template arguments
			$args	= array(
						'collection'		=> $collection,
						'collection_id'		=> $collection_id,
						'collection_title'	=> apply_filters( 'woo_cl_collection_block_title', $collection_title, $collection_id ),
						'collection_disc'	=> $collection_disc,
						'cl_item_listsurl'	=> $cl_item_listsurl,
						'curr_user_ID'		=> $curr_user_ID,
						'product_total' 	=> $product_total,
						'product_imgurl'	=> $product_imgurl,
						'product_min_price'	=> $min_price,
					);

			//featured collection listing block template
			woo_cl_get_template( 'featured-collection-listing/featured-collection-listing-content.php', $args );

		}
	}
}

/* **************************************************************************
   Social Sharing Button Template Functions
   ************************************************************************** */
   
if( !function_exists( 'woo_cl_social_share_buttons' ) ) {

	/**
	 * Loads the social buttons template.
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_social_share_buttons() {

		//Get collection view page end point
		$woo_cl_view_slug	= get_option( 'cl_view_collection_text' );
		$woo_cl_view_slug	= trim( $woo_cl_view_slug ) != '' ? $woo_cl_view_slug : 'view-collection';
		
		//Get Collection id
		$coll_id = get_query_var( $woo_cl_view_slug );
		
		$collection_data	= get_post( $coll_id );
		
		if( isset( $collection_data->post_status ) && $collection_data->post_status != 'private' ) {
			
			// arguments for social button template
			$args = array( 'coll_id' => $coll_id );
			
			// get the template
			woo_cl_get_template( 'social-buttons/social-button.php', $args );
		}
	}
}
   
if( !function_exists( 'woo_cl_social_facebook' ) ) {

	/**
	 * Loads the fscebook button template.
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_social_facebook( $coll_id ) {
		
		global $woo_cl_model;
		
		$facebook_enable 	= get_option( 'cl_sharing_facebook' ); // get facebook share enable
		
		if( isset( $facebook_enable ) && $facebook_enable == 'yes' ) {
			
			//get collection share url
			$coll_share_url	= woo_cl_get_collection_share_url( $coll_id );
			
			// enqueue the Facebook script
			wp_enqueue_script( 'facebook' );
			
			// set the args, which we will pass to the template
			$args = array( 
							'coll_share_url'	=> $coll_share_url,
							'coll_id'			=> $coll_id
						);
			
			// get the template
			woo_cl_get_template( 'social-buttons/facebook.php', $args );
		}
	}
}
   
if( !function_exists( 'woo_cl_social_twitter' ) ) {

	/**
	 * Loads the twitter button template.
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_social_twitter( $coll_id ) {
	
		global $woo_cl_model;
		
		$twitter_enable  = get_option( 'cl_sharing_twitter' ); // get twitter share enable
		
		//check twitter share button is emable or not
		if( isset( $twitter_enable ) && $twitter_enable == 'yes' )  {
			
			//get collection share url
			$coll_share_url	= woo_cl_get_collection_share_url( $coll_id );
			
			// enqueue the twitter script
			wp_enqueue_script( 'twitter' );
			
			// set the args, which we will pass to the template
			$args = array( 
							'coll_share_url'	=> $coll_share_url,
							'coll_id'			=> $coll_id,
							'coll_title'		=> get_the_title( $coll_id ),
							'tw_user_name'		=> '',
						);
								
			// get the template
			woo_cl_get_template( 'social-buttons/twitter.php', $args );
		}
	}
}
   
if( !function_exists( 'woo_cl_social_google' ) ) {

	/**
	 * Loads the google button template.
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_social_google( $coll_id ) {
			
		global $woo_cl_model;
			
		$google_enable 	 = get_option( 'cl_sharing_google' ); // get google share enable
				
		//check google share button is emable or not
		if( isset( $google_enable ) && $google_enable == 'yes' )  {
			
			//get collection share url
			$coll_share_url	= woo_cl_get_collection_share_url( $coll_id );
						
			// enqueue the google plus script
			wp_enqueue_script( 'google' );
			
			// set the args, which we will pass to the template
			$args = array( 
							'coll_share_url' => $coll_share_url,
							'coll_id' 		 => $coll_id
						);
						
			// get the template
			woo_cl_get_template( 'social-buttons/google.php', $args );
		}
	}
}
 
if( !function_exists( 'woo_cl_social_linkedin' ) ) {

	/**
	 * Loads the linedin button template.
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_social_linkedin( $coll_id ) {
	
		global $woo_cl_model;
		
		$linkedin_enable  = get_option( 'cl_sharing_linkedin' ); // get email share enable
		
		//check linkedin share button is emable or not
		if( isset( $linkedin_enable ) && $linkedin_enable == 'yes' )  {

			//get collection share url
			$coll_share_url	= woo_cl_get_collection_share_url( $coll_id );

			// enqueue the linkedin plus script
			wp_enqueue_script( 'linkedin' );
			
			// set the args, which we will pass to the template
			$args = array( 
							'coll_share_url' => $coll_share_url,
							'coll_id' 		 => $coll_id,
							'coll_title'	 => get_the_title( $coll_id )
						);
								
			// get the template
			woo_cl_get_template( 'social-buttons/linkedin.php', $args );
		}
	}
}
 
if( !function_exists( 'woo_cl_share_via_email' ) ) {

	/**
	 * Loads the share via email button template.
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_share_via_email( $coll_id ) {
	
		global $woo_cl_model;
		
		$email_enable  = get_option( 'cl_sharing_email' ); // get email share enable
		
		//check email share button is emable or not
		if( isset( $email_enable ) && $email_enable == 'yes' )  {
			
			// get the template
			woo_cl_get_template( 'social-buttons/email.php' );
		}
	}
}

if( !function_exists( 'woo_cl_share_via_email_popup' ) ) {
	
	/**
	 * Loads the share via email popup template.
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_share_via_email_popup( $coll_id ) {
	
		// set the args, which we will pass to the template
		$args = array( 
						'coll_id' 	=> $coll_id,
					);

		// get the template
		woo_cl_get_template( 'social-buttons/popup/email-share.php', $args );
	}
}

if( !function_exists( 'woo_cl_share_via_email_success_popup' ) ) {

	/**
	 * Loads the share via email success popup template.
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_share_via_email_success_popup() {
		
		// get the template
		woo_cl_get_template( 'social-buttons/popup/email-share-success.php' );
	}
}