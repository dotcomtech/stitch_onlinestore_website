<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Register Post Type
 *
 * Register Custom Post Type for Collections
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_register_post_types() {
	
	$labels = array(
					    'name'				=> '%2$s',
					    'singular_name' 	=> '%1$s',
					    'add_new' 			=> __( 'Add New','woocl' ),
					    'add_new_item' 		=> __( 'Add New %1$s','woocl' ),
					    'edit_item' 		=> __( 'Edit %1$s','woocl' ),
					    'new_item' 			=> __( 'New %1$s','woocl' ),
					    'all_items' 		=> '%2$s',
					    'view_item' 		=> __( 'View %1$s','woocl' ),
					    'search_items' 		=> __( 'Search %1$s','woocl' ),
					    'not_found' 		=> __( 'No %1$s found','woocl' ),
					    'not_found_in_trash'=> __( 'No %1$s found in Trash','woocl' ),
					    'parent_item_colon' => '',
					    'menu_name' 		=> '%2$s',
					);
					
	foreach ( $labels as $key => $value ) {
		$labels[ $key ] = sprintf( $value, woo_cl_get_label_singular(), woo_cl_get_label_plural() );
	}
	
	$args = array(
					    'labels' 			=> $labels,
					    'public'			=> true,
						'show_ui' 			=> true, 
						'show_in_menu' 		=> WOO_CL_MAIN_MENU_NAME, 
					    'query_var' 		=> true,
					    'rewrite' 			=> array( 'slug' => WOO_CL_POST_TYPE_COLL ),
					    'capability_type' 	=> 'post',//WOO_CL_POST_TYPE_COLL,
					    'hierarchical' 		=> false,
					    'supports' 			=> array( 'title' , 'editor', 'author', 'page-attributes' )
				    ); 
	
	//Apply Filter to modify the arguments of collection post type
	$coll_args	= apply_filters( 'woo_cl_reg_coll_post_args', $args );
	
	//Register collection post type
	register_post_type( WOO_CL_POST_TYPE_COLL, $coll_args );
	
	$labels = array(
					    'name'				=> __( '%1$s Items', 'woocl' ),
					    'singular_name' 	=> __( '%1$s Item', 'woocl' ),
					    'add_new' 			=> __( 'Add New', 'woocl' ),
					    'add_new_item' 		=> __( 'Add New %1$s Item', 'woocl' ),
					    'edit_item' 		=> __( 'Edit %1$s Item', 'woocl' ),
					    'new_item' 			=> __( 'New %1$s Item', 'woocl' ),
					    'all_items' 		=> __( 'All %1$s Item', 'woocl' ),
					    'view_item' 		=> __( 'View %1$s Item', 'woocl' ),
					    'search_items' 		=> __( 'Search %1$s Item', 'woocl' ),
					    'not_found' 		=> __( 'No %1$s Items found', 'woocl' ),
					    'not_found_in_trash'=> __( 'No %1$s Items found in Trash', 'woocl' ),
					    'parent_item_colon' => '',
					    'menu_name' 		=> __( '%1$s Items', 'woocl' ),
					);
	
	foreach ( $labels as $key => $value ) {
		$labels[ $key ] = sprintf( $value, woo_cl_get_label_singular(), woo_cl_get_label_plural() );
	}
	
	$args	= array(
					'labels'			=> $labels,
					'query_var'			=> false,
					'rewrite'			=> array( 'slug' => WOO_CL_POST_TYPE_COLLITEMS ),
					'capability_type'	=> 'post',//WOO_CL_POST_TYPE_COLLITEMS,
					'hierarchical'		=> false,
					'supports'			=> array( 'title' )
				);
	
	//Apply Filter to modify the arguments of collection item post type
	$collitem_args = apply_filters( 'woo_cl_reg_collitem_post_args', $args );
	
	//Register collection item post type
	register_post_type( WOO_CL_POST_TYPE_COLLITEMS, $collitem_args );
}

//creating custom post type
add_action( 'init', 'woo_cl_register_post_types' );