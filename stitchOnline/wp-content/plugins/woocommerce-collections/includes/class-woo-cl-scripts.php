<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Scripts Class
 * 
 * Handles adding scripts functionality to the admin pages
 * as well as the front pages.
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
class Woo_Cl_Scripts {
	
	public $model;
	
	public function __construct() {
		global $woo_cl_model;
		$this->model	= $woo_cl_model;
	}

	/**
	 * Enqueuing woocommerce Scripts & Style
	 * 
	 * Loads the required script edit page in the WordPress admin section.
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_woocommerce_style_scripts( $hook_suffix ) {
		
		//Get woocommerce script & style path
		$assets_path  = str_replace( array( 'http:', 'https:' ), '', WC()->plugin_url() ) . '/assets/';
		
		wp_enqueue_script( 'wc-enhanced-select' );
		wp_enqueue_style( 'select2', $assets_path . 'css/select2.css' );
	}
	
	/**
	 * Enqueue style for meta box page
	 * 
	 * Handles style which is enqueue in collections post type
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_metabox_styles( $hook ) {
		
		global $post_type;
	
		if( $post_type == WOO_CL_POST_TYPE_COLL ) {
			
			// Register Meta Box Style
			wp_register_style( 'woo-cl-meta-box', WOO_CL_META_URL. '/css/meta-box.css', array(), WOO_CL_LIB_VER );

			// Enqueue Meta Box Style
			wp_enqueue_style( 'woo-cl-meta-box' );
		}
	}
	
	/**
	 * Enqueue Styles for public
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_public_style() {
		
		global $post;

		//Get post content if exist
		$post_id = isset( $post->ID ) ? $post->ID : '';
		$post_content = isset( $post->post_content ) ? $post->post_content : '';

		if( has_shortcode( $post_content, 'woo_cl_collections' ) || has_shortcode( $post_content, 'woo_cl_my_collections' )
			|| woo_cl_is_enable_collections( $post_id )
			|| ( get_option( 'cl_enable_woo_myaccount_page' ) == 'yes' && is_account_page() )
			|| ( class_exists( 'BuddyPress' ) && get_option( 'cl_enable_buddypress_myaccount_page' ) == 'yes' && bp_is_current_action('mycollection') )
			|| ( class_exists( 'UM_API' ) && get_option( 'cl_enable_um_account_page' ) == 'yes' && um_is_core_page('account') ) ) {// add css for only collection page

			$this->woo_cl_wp_register_style( 'woo-cl-collection-listing', '', array(), WOO_CL_LIB_VER );
			wp_enqueue_style( 'woo-cl-collection-listing' ); // Enqueue Collection Listing style

			$this->woo_cl_wp_register_style( 'woo-cl-item-listing', '', array(), WOO_CL_LIB_VER );
			wp_enqueue_style( 'woo-cl-item-listing' ); // Enqueue Item listing style

			$this->woo_cl_wp_register_style( 'woo-cl-collection-edit', '', array(), WOO_CL_LIB_VER );
			wp_enqueue_style( 'woo-cl-collection-edit' ); // Enqueue collection edit style

			$this->woo_cl_wp_register_style( 'woo-cl-email-share', '', array(), WOO_CL_LIB_VER );
			wp_enqueue_style( 'woo-cl-email-share' ); // Enqueue email share style
		}

		if( has_shortcode( $post_content, 'woo_cl_featured_collections' ) ) {// add css for only featured collection page

			$this->woo_cl_wp_register_style( 'woo-cl-featured-collections', '', array(), WOO_CL_LIB_VER );
			wp_enqueue_style( 'woo-cl-featured-collections' ); // Enqueue featured collection listing style
		}
	}
	
	/**
	 * Enqueue Styles for all kind of Popup
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_popup_style() {
		
		global $post;
		
		//Get post content if exist
		$post_content = isset( $post->post_content ) ? $post->post_content : '';
		
		$this->woo_cl_wp_register_style( 'woo-cl-popup', '', array(), WOO_CL_LIB_VER );
		$this->woo_cl_wp_register_style( 'font-awesome.min', 'font-awesome/css/', array(), WOO_CL_LIB_VER );
		
		//wp_register_style( 'woo-cl-popup-style', WOO_CL_URL. 'includes/css/woo-cl-popup.css', array(), WOO_CL_LIB_VER );
		//wp_register_style( 'woo-cl-awesome-icons', WOO_CL_URL. 'includes/css/font-awesome/css/font-awesome.min.css', array(), WOO_CL_LIB_VER );
		
		/**
		 * We have comment if condition because the add to collection popup will 
		 * not display for custom product lising using shortcode on other pages.
		 */
		//if( has_shortcode( $post_content, 'woo_cl_collections' ) || woo_cl_is_enable_collection_button() || is_account_page() ) {// add css for collection page or woocommerce pages
			wp_enqueue_style( 'woo-cl-popup' );
			wp_enqueue_style( 'font-awesome.min' ); // Enqueue font awesome Icons styles
			//wp_enqueue_style( 'woo-cl-popup-style' );
			//wp_enqueue_style( 'woo-cl-awesome-icons' ); // Enqueue font awesome Icons styles
		//}
	}
	
	/**
	 * Enqueue Style for Collection Dropdown list
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_coll_dropdown_styles() {
		
		global $post;
		
		//Get post content if exist
		$post_content = isset( $post->post_content ) ? $post->post_content : '';
		
		$this->woo_cl_wp_register_style( 'woo-cl-coll-dropdown', '', array(), WOO_CL_LIB_VER );
		//wp_register_style( 'woo-cl-coll-dropdown-style', WOO_CL_URL. 'includes/css/woo-cl-coll-dropdown.css', array(), WOO_CL_LIB_VER );
		
		/**
		 * We have comment if condition because the add to collection popup will 
		 * not display for custom product lising using shortcode on other pages.
		 */
		//if( has_shortcode( $post_content, 'woo_cl_collections' ) || woo_cl_is_enable_collection_button() || is_account_page() ) {// add css for collection page or woocommerce pages
			wp_enqueue_style( 'woo-cl-coll-dropdown' ); // Enqueue  dropdown style
			//wp_enqueue_style( 'woo-cl-coll-dropdown-style' ); // Enqueue  dropdown style
		//}
	}
	
	/**
	 * Enqueue Script for all kind of Popup and collection listing
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_public_scripts() {
		
		global $post, $wp_query;

		//Get all Messages
		$messages	= woo_cl_messages();
		
		//Get post content if exist
		$post_id = isset( $post->ID ) ? $post->ID : '';
		$post_content = isset( $post->post_content ) ? $post->post_content : '';
		
		wp_register_script( 'woo-cl-popup-script', WOO_CL_URL. 'includes/js/woo-cl-popup.js', array('jquery' ), WOO_CL_LIB_VER, true );
		
		//localize script to pass some variable to javascript file from php file
		//pass ajax url to access wordpress ajax file at front side
		wp_localize_script( 'woo-cl-popup-script','Woo_cl_Ajax',array(
																		'ajaxurl' 				=> admin_url( 'admin-ajax.php', ( is_ssl() ? 'https' : 'http' ) ),
																		'coll_title_hint' 		=> sprintf( __("Give your %s title.", "woocl"), woo_cl_get_label_singular() ),
																		'coll_empty_error' 		=> isset( $messages['coll_empty_error'] ) 		? $messages['coll_empty_error'] 	: '',
																		'coll_selection_error' 	=> isset( $messages['coll_selection_error'] ) 	? $messages['coll_selection_error'] : '',
																		'email_error' 			=> isset( $messages['email_error'] ) 			? $messages['email_error'] 			: '',
																		'email_valid_error' 	=> isset( $messages['email_valid_error'] ) 		? $messages['email_valid_error'] 	: '',
																		'email_fri_error' 		=> isset( $messages['email_fri_error'] ) 		? $messages['email_fri_error'] 		: '',
																		'email_fri_valid_error' => isset( $messages['email_fri_valid_error'] ) 	? $messages['email_fri_valid_error']: '',
																		'add_cart_success' 		=> isset( $messages['add_cart_success'] ) 		? $messages['add_cart_success'] 	: '',
																		'must_choose_product_option' 	=> isset( $messages['must_choose_product_option'] ) 		? $messages['must_choose_product_option'] 	: ''
																	));
		
		// register script for dropdown
		wp_register_script( 'woo-cl-dropdown-script', WOO_CL_URL. 'includes/js/jquery.selectric.min.js', array('jquery' ), WOO_CL_LIB_VER, true );
		
		/**
		 * We have comment if condition because the add to collection popup will 
		 * not display for custom product lising using shortcode on other pages.
		 */
		//if( has_shortcode( $post_content, 'woo_cl_collections' ) || woo_cl_is_enable_collection_button() || is_account_page() || has_shortcode( $post_content, 'woo_cl_my_collections' ) || woo_cl_is_enable_collections() ) {// add Js for collection page or woocommerce pages
			
			wp_enqueue_script( 'woo-cl-popup-script' ); // Enqueue script on for add to collection popup
			wp_enqueue_script( 'woo-cl-dropdown-script' ); // Enqueue script on for dropdown
		//}

		if( has_shortcode( $post_content, 'woo_cl_collections' ) || has_shortcode( $post_content, 'woo_cl_my_collections' )
			|| woo_cl_is_enable_collections( $post_id )
			|| ( get_option( 'cl_enable_woo_myaccount_page' ) == 'yes' && is_account_page() )
			|| ( class_exists( 'BuddyPress' ) && get_option( 'cl_enable_buddypress_myaccount_page' ) == 'yes' && bp_is_current_action('mycollection') )
			|| ( class_exists( 'UM_API' ) && get_option( 'cl_enable_um_account_page' ) == 'yes' && um_is_core_page('account') ) ) {// add Js for collection page

			wp_register_script( 'woo-cl-ajax-listing', WOO_CL_URL. 'includes/js/woo-cl-ajax-listing.js', array('jquery' ), WOO_CL_LIB_VER, true );
			wp_enqueue_script( 'woo-cl-ajax-listing' ); //enqueue ajax listing script
			wp_register_script( 'woo-cl-public-script', WOO_CL_URL. 'includes/js/woo-cl-public.js', array('jquery' ), WOO_CL_LIB_VER, true );
			
			//localize script to pass some variable to javascript file from php file
			//pass ajax url to access wordpress ajax file at front side for public
			wp_localize_script( 'woo-cl-public-script','Woo_cl_Public_Ajax',array(
																				'ajaxurl' 			=> admin_url( 'admin-ajax.php', ( is_ssl() ? 'https' : 'http' ) ),
																				'no_more_coll' 		=> isset( $messages['no_more_coll'] ) 		? $messages['no_more_coll'] 		: '',
																				'no_more_coll_item'	=> isset( $messages['no_more_coll_item'] ) 	? $messages['no_more_coll_item']	: '',
																				'select_all_chk'	=> __("Select All", "woocl"),
																				'unselect_all_chk'	=> __("Unselect All", "woocl"),
																				'add_bulk_products_to_cart_error'	=> __("Something worng, No item added to cart.", "woocl"),
																			));
			wp_enqueue_script( 'woo-cl-public-script' ); //enqueue public script for item & edit item listing
		}
		
		wp_register_script( 'woo-cl-variation-script', WOO_CL_URL. 'includes/js/woo-cl-variation.js', array('jquery' ), WOO_CL_LIB_VER, true );
		
		/**
		 * We have comment if condition because the add to collection popup will 
		 * not display for custom product lising using shortcode on other pages.
		 */
		//if( woo_cl_is_enable_collection_button() ) { //Enqueue script woocommerce pages
			wp_enqueue_script( 'woo-cl-variation-script' ); // Enqueue script on for variation product
		//}

		//Check if collection edit page and settings enabled.
		if( get_option( 'cl_enable_front_drag_item_ordering' ) == 'yes' && has_shortcode( $post_content, 'woo_cl_collections' ) && !empty( $wp_query->query['edit-collection'] ) ) {

			//Register drag order js and enqueue
			wp_register_script( 'woo-cl-item-ordering', WOO_CL_URL .'includes/js/woo-cl-item-ordering.js', array( 'jquery', 'jquery-ui-sortable' ), WOO_CL_LIB_VER, true );
			wp_localize_script( 'woo-cl-item-ordering', 'Woo_Cl_Item_Ajax', array(
																				'ajaxurl' => admin_url( 'admin-ajax.php', ( is_ssl() ? 'https' : 'http' ) ),
																				'loader_img' => WOO_CL_IMG_URL .'/woo-cl-loader-big.gif',
																				) );
			wp_enqueue_script( 'woo-cl-item-ordering' );
		}
	}
	
	/**
	 * Enqueue Script for Social Share
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_social_share_scripts() {
		
		$urlsuffix = is_ssl() ? 'https://' : 'http://';
		
		$facebook_enable = get_option( 'cl_sharing_facebook' ); // get facebook share enable
		$twitter_enable  = get_option( 'cl_sharing_twitter' ); // get twitter share enable
		$google_enable 	 = get_option( 'cl_sharing_google' ); // get google share enable
		$linkedin_enable = get_option( 'cl_sharing_linkedin' ); // get linkedin share enable
		
		if( isset( $facebook_enable ) && $facebook_enable == 'yes' )  {//check facebook share button is emable or not
			
			wp_deregister_script( 'facebook' );
			wp_register_script( 'facebook', $urlsuffix.'connect.facebook.net/en/all.js#xfbml=1', false, null, true );
		}
		
		if( isset( $twitter_enable ) && $twitter_enable == 'yes' )  {//check twitter share button is emable or not
			
			wp_deregister_script( 'twitter' );
			wp_register_script( 'twitter', $urlsuffix.'platform.twitter.com/widgets.js',array( 'jquery' ), WOO_CL_LIB_VER, true );
		}
		
		if( isset( $google_enable ) && $google_enable == 'yes' )  {//check google share button is emable or not
			
			wp_deregister_script('google');
			wp_register_script( 'google', $urlsuffix.'apis.google.com/js/plusone.js',array( 'jquery' ), WOO_CL_LIB_VER, true );
		}
		
		if( isset( $linkedin_enable ) && $linkedin_enable == 'yes' )  {//check linkedin share button is emable or not
			
			wp_deregister_script('linkedin');
			wp_register_script( 'linkedin', $urlsuffix.'//platform.linkedin.com/in.js',array( 'jquery' ), WOO_CL_LIB_VER, true );
		}
	}
	
	/**
	 * style on head of page
	 * 
	 * Handles style code display when wp head initialize
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_custom_style() {
		
		//Get custom css code
		$custom_css	= get_option( 'cl_custom_css' );
		
		if( !empty( $custom_css ) )	{//if custom css code not available
			echo '<style type="text/css">' . $custom_css . '</style>';
		}
	}
	
	/**
	 * Display button in post / page container
	 * 
	 * Handles to display button in post / page container
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_shortcode_display_button( $buttons ) {
		
		array_push( $buttons, "|", "woo_cl_coll_shortcodes" );
		return $buttons;
	}
	
	/**
	 * Include js for add button in post / page container
	 * 
	 * Handles to include js for add button in post / page container
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_shortcode_button( $plugin_array ) {
		
		wp_enqueue_script( 'tinymce' );
		
		$plugin_array['woo_cl_coll_shortcodes'] = WOO_CL_URL . 'includes/js/woo-cl-shortcodes.js';
		return $plugin_array;
	}
	
	/**
	 * Display button in post / page container
	 * 
	 * Handles to display button in post / page container
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_add_shortcode_button() {
		
		if( current_user_can( 'manage_options' ) || current_user_can( 'edit_posts' ) ) {
			add_filter( 'mce_external_plugins', array( $this, 'woo_cl_shortcode_button' ) );
   			add_filter( 'mce_buttons', array( $this, 'woo_cl_shortcode_display_button' ) );
		}
	}
	
	/**
	 * Enqueuing Styles for backend
	 * 
	 * Loads the required stylesheets for backend
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_admin_styles( $hook_suffix ) {
		
		$pages_hook_suffix = array( 'post.php', 'post-new.php', 'woocommerce_page_wc-settings' );
		
		$pages = array('edit.php');
		
		if( in_array( $hook_suffix, $pages_hook_suffix ) || $hook_suffix == 'woocommerce_page_woo_cl_add_to_coll' ) {//Check pages when you needed
			
			// loads the required styles for the plugin settings page
			wp_register_style( 'woo-cl-admin', WOO_CL_URL . 'includes/css/woo-cl-admin.css', array(), WOO_CL_LIB_VER );
			wp_enqueue_style( 'woo-cl-admin' );
		}
		
		if( in_array( $hook_suffix, $pages ) && isset($_GET['post_type']) && $_GET['post_type'] == WOO_CL_POST_TYPE_COLL ) {
			
			wp_register_style( 'woo-cl-iphone-btn', WOO_CL_URL.'includes/css/woo-cl-iphone-check-btn.css', array(), WOO_CL_LIB_VER );
			wp_enqueue_style( 'woo-cl-iphone-btn' );
		}
	}
	
	/**
	 * Enqueuing Scripts
	 * 
	 * Loads the required script edit page in the WordPress admin section.
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_admin_scripts( $hook_suffix ) {
		
		global $post_type, $wp_query;
	
		//Get all Messages
		$messages	= woo_cl_messages();
		
		$pages = array( 'edit.php' );
		
		if( $post_type == WOO_CL_POST_TYPE_COLL ) {
			
			wp_register_script( 'woo-cl-admin-script', WOO_CL_URL. 'includes/js/woo-cl-admin.js', array('jquery', 'jquery-ui-sortable' ), WOO_CL_LIB_VER );
			wp_localize_script( 'woo-cl-admin-script','Woo_cl_Admin_Ajax',array(
																				'ajaxurl' 			=> admin_url( 'admin-ajax.php', ( is_ssl() ? 'https' : 'http' ) ),
																				'no_item_found' 	=> isset( $messages['no_item_found'] ) 		? $messages['no_item_found'] 	: '',
																				'confirm_delete' 	=> isset( $messages['confirm_delete'] ) 	? $messages['confirm_delete'] 	: '',
																				'product_unselect' 	=> isset( $messages['product_unselect'] ) 	? $messages['product_unselect'] 	: '',
																				'add_product_success' 	=> isset( $messages['add_product_success'] ) 	? $messages['add_product_success'] 	: '',
																			));
			wp_enqueue_script( 'woo-cl-admin-script' ); // Enqueue script on for variation product
		}
		
		if( in_array( $hook_suffix, $pages ) && isset($_GET['post_type']) && $_GET['post_type'] == WOO_CL_POST_TYPE_COLL ) {

			// Collection sorting - only when sorting by menu order on the collections listing page
			if ( current_user_can( 'edit_others_pages' ) && isset( $wp_query->query['orderby'] ) && $wp_query->query['orderby'] == 'menu_order title' ) {
				wp_register_script( 'woo-cl-collection-ordering', WOO_CL_URL . 'includes/js/collection-ordering.js', array( 'jquery', 'jquery-ui-sortable' ), WOO_CL_LIB_VER, true );
				wp_enqueue_script( 'woo-cl-collection-ordering' );
			}

			wp_register_script( 'woo-cl-iphone-check-scripts', WOO_CL_URL . 'includes/js/woo-cl-iphone-check-btn.js', array( 'jquery' ), WOO_CL_LIB_VER, false );
			wp_enqueue_script( 'woo-cl-iphone-check-scripts' );
			
			// Registring admin script
			wp_register_script( 'woo-cl-iphone-call-scripts', WOO_CL_URL.'includes/js/woo-cl-iphone-call.js', array( 'woo-cl-iphone-check-scripts' ), WOO_CL_LIB_VER, false );
			wp_localize_script( 'woo-cl-iphone-call-scripts', 'ajax_url', admin_url() );
			wp_enqueue_script( 'woo-cl-iphone-call-scripts' );
		}

		// Collection item sorting
		if ( in_array( $hook_suffix, array( 'post.php' ) ) && !empty( $_GET['post'] ) && WOO_CL_POST_TYPE_COLL == $post_type ) {

			wp_register_script( 'woo-cl-collection-item-ordering', WOO_CL_URL . 'includes/js/collection-item-ordering.js', array( 'jquery', 'jquery-ui-sortable' ), WOO_CL_LIB_VER, true );
			wp_enqueue_script( 'woo-cl-collection-item-ordering' );
		}
	}
	
	/**
	 * Woocl Register Css
	 * 
	 * Adding proper hoocks for the scripts.
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_wp_register_style( $handle, $sub_dir_path = '', $deps = array(), $ver = false, $media = 'all' ) {

		//get woocommerce folder name
		$woocommerce_folder	= WC()->template_path();

		// woocommerce collection folder name
		$templates_dir_name	= WOO_CL_BASENAME;

		//get style sheet path from child theme
		$child_theme_style_sheet	= trailingslashit( get_stylesheet_directory() ) . $woocommerce_folder . $templates_dir_name . '/css/' . $sub_dir_path . $handle . '.css';
		//get style sheet path from main theme
		$parent_theme_style_sheet	= trailingslashit( get_template_directory() ) . $woocommerce_folder . $templates_dir_name . '/css/' . $handle . $sub_dir_path . '.css';
		//get style sheet path from plugin
		$plugin_style_sheet			= trailingslashit( WOO_CL_DIR . '/includes/templates/css' ) . $handle . $sub_dir_path . '.css';

		//default css URL ( From Plugin )
		$css_url	= WOO_CL_URL . 'includes/templates/css/' . $sub_dir_path . $handle . '.css';

		if( file_exists( $child_theme_style_sheet ) ) { //overwrite from child theme
			$css_url	= trailingslashit( get_stylesheet_directory_uri() ) . $woocommerce_folder . $templates_dir_name . '/css/' . $sub_dir_path . $handle . '.css';
		} elseif( file_exists( $parent_theme_style_sheet ) ) {//overwrite from main theme
			$css_url	= trailingslashit( get_template_directory_uri() ) . $woocommerce_folder . $templates_dir_name . '/css/' . $sub_dir_path . $handle . '.css';
		}

		//add filter support to modify css path
		$css_url	= apply_filters( 'woo_cl_wp_register_style_url', $css_url, $handle, $sub_dir_path );

		//register scripts
		wp_register_style( $handle, $css_url, $deps, $ver, $media );
	}
	
	public function woo_cl_admin_sett_scripts ( $hook_suffix ) {
		
		global $woocommerce;

		if ( $hook_suffix == 'woocommerce_page_wc-settings' || $hook_suffix == 'woocommerce_page_woo_cl_add_to_coll' ) {

			//js directory url
			$js_dir = $woocommerce->plugin_url() . '/assets/js/';
			
			// Use minified libraries if SCRIPT_DEBUG is turned off
			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
			
			// Select2
			wp_register_script( 'select2', $js_dir . 'select2/select2'.$suffix . '.js', array( 'jquery' ), '3.5.2' );
			wp_register_script( 'wc-enhanced-select', $woocommerce->plugin_url() . '/assets/js/admin/wc-enhanced-select' . $suffix . '.js', array( 'jquery', 'select2' ), WC_VERSION );
			wp_enqueue_script( 'wc-enhanced-select' );
			
			// Register admin settings script
			wp_register_script( 'woo-cl-admin-sett-scripts', WOO_CL_URL . 'includes/js/woo-cl-admin-sett.js', array( 'jquery', 'wc-enhanced-select' ), WOO_CL_LIB_VER, false );
			wp_enqueue_script( 'woo-cl-admin-sett-scripts' );
		}
	}
	
	public function woo_cl_admin_sett_styles ( $hook_suffix ) {
		
		global $woocommerce;

		if ( $hook_suffix == 'woocommerce_page_wc-settings' || $hook_suffix == 'woocommerce_page_woo_cl_add_to_coll' ) {

			//css directory url
			$css_dir = $woocommerce->plugin_url() . '/assets/css/';
			
			// Admin styles for WC pages only
			wp_enqueue_style( 'woo_vou_admin_styles', $css_dir . 'admin.css', array(), WOOCOMMERCE_VERSION );
				
			wp_register_style( 'select2', $css_dir . 'select2.css', array(), WOOCOMMERCE_VERSION );
			wp_enqueue_style( 'select2' );
		}

		//Check if collection admin display settings
		if ( $hook_suffix == 'woocommerce_page_wc-settings' && isset( $_GET['section'] ) && $_GET['section'] == 'cl_display_settings' ) {

			//Register font awesome icon and enqueue
			$this->woo_cl_wp_register_style( 'font-awesome.min', 'font-awesome/css/', array(), WOO_CL_LIB_VER );
			wp_enqueue_style( 'font-awesome.min' );
		}

		//Check if admin add to colection page
		if ( $hook_suffix == 'woocommerce_page_woo_cl_add_to_coll' ) {

			// loads the required scripts for the meta boxes
			wp_enqueue_script( 'common' );
			wp_enqueue_script( 'wp-lists' );
			wp_enqueue_script( 'postbox' );
		}
	}

	/**
	 * Loading Additional Java Script
	 *
	 * Loads the JavaScript required for toggling the meta boxes on add to collection admin page.
	 *
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function woo_cl_addtocoll_page_load_scripts( $hook_suffix ) { ?>				
		<script type="text/javascript">
			//<![CDATA[
			jQuery(document).ready( function($) {
				$('.if-js-closed').removeClass('if-js-closed').addClass('closed');
				postboxes.add_postbox_toggles( 'woocommerce_page_woo_cl_add_to_coll' );
			});
			//]]>
		</script>
	<?php
	}

	/**
	 * Adding admin CSS.
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
	public function woo_cl_admin_custom_style() {

		//Check if product post
		if( isset( $_GET['post_type'] ) && $_GET['post_type'] == 'product' ) {
			echo '<style>#name{width:20%;}#coll_count{width:66px;}</style>';			
		}
	}

	/**
	 * Adding Hooks
	 * 
	 * Adding proper hoocks for the scripts.
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function add_hooks() {
		
		if( woo_cl_is_edit_page() ) {
			
			//add action for enqueue woocommerce script
			add_action( 'admin_enqueue_scripts', array( $this, 'woo_cl_woocommerce_style_scripts' ), 100 );

			//add styles for new and edit post
			add_action( 'admin_enqueue_scripts', array( $this, 'woo_cl_metabox_styles' ) );
		}
		//style for public
		add_action( 'wp_enqueue_scripts', array( $this, 'woo_cl_public_style' ) );
		
		//style for popup
		add_action( 'wp_enqueue_scripts', array( $this, 'woo_cl_popup_style' ) );
		
		//style for Dropdown
		add_action( 'wp_enqueue_scripts', array( $this, 'woo_cl_coll_dropdown_styles' ) );
		
		//script for popup
		add_action( 'wp_enqueue_scripts', array( $this, 'woo_cl_public_scripts' ) );
		
		//script for Social Share
		add_action( 'wp_enqueue_scripts', array( $this, 'woo_cl_social_share_scripts' ) );

		//style code on wp head
		add_action( 'wp_head', array( $this, 'woo_cl_custom_style' ) );

		// add filters for add button in post / page container
		add_action( 'admin_init', array( $this, 'woo_cl_add_shortcode_button' ) );
		
		// for shortcode popup css
		add_action( 'admin_enqueue_scripts', array( $this,'woo_cl_admin_styles' ) );
		
		// for admin js
		add_action( 'admin_enqueue_scripts', array( $this,'woo_cl_admin_scripts' ), 100 );
		
		// for admin settings js
		add_action( 'admin_enqueue_scripts', array( $this,'woo_cl_admin_sett_scripts' ), 100 );
		
		// for admin settings styles
		add_action( 'admin_enqueue_scripts', array( $this,'woo_cl_admin_sett_styles' ), 100 );

		//style code on admin head
		add_action( 'admin_head', array( $this, 'woo_cl_admin_custom_style' ) );
	}
}