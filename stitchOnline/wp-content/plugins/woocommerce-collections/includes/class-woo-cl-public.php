<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Public Collection Pages Class
 * 
 * Handles all the different features and functions
 * for the front end pages.
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
class Woo_Cl_Public	{

	public $model;

	public function __construct() {

		global $woo_cl_model;
		$this->model	= $woo_cl_model;
	}

	/**
	 * Add to Collection Button
	 * 
	 * Add to Collection Button after add to cart button on product page and shop pages
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_add_to_collection_button() {
		
		//Add to collection on archives and single product page
		woo_cl_add_to_collection_button();
	}
	/**
	 * Add to Collection Button
	 * 
	 * Add to Collection Button after add to cart button on product page and shop pages
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.8
	 */
	public function woo_cl_add_col_btn_to_product_summary() {

		global $product;
	
		if ( !$product->is_purchasable() && !$product->is_type( 'external' ) ) {//check if product not purchasable
			
			//Add to collection on single product page when product not purchable
			woo_cl_add_to_collection_button();
		}
	}
	
	/**
	 * Add to Collection Button
	 * 
	 * Add to Collection Button after add to cart button on product page and shop pages
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.8
	 */
	public function woo_cl_add_to_collection_button_html_deprecated( $availability_html, $availability, $product ) {

		// initlize collection button blank 
		$collection_btn_html = '';
		
		// Check product is not variable && product is purchasable or is in stock
		if( !$product->is_type( 'variation' ) && (!$product->is_purchasable() || !$product->is_in_stock()) ) {
			
			ob_start();
			
			//Add to collection on single product page when out of stack
			woo_cl_add_to_collection_button();
			
			// collection button HTML
			$collection_btn_html	= ob_get_clean();
		}
		
		return $availability_html . $collection_btn_html;
	}
	
	/**
	 * Add to Collection Button
	 * 
	 * Add to Collection Button after add to cart button on product page and shop pages
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.8
	 */
	public function woo_cl_add_to_collection_button_html( $availability_html, $product ) {

		// initlize collection button blank 
		$collection_btn_html = '';
		
		// Check product is not variable && product is purchasable or is in stock
		if( !$product->is_type( 'variation' ) && (!$product->is_purchasable() || !$product->is_in_stock()) ) {
			
			ob_start();
			
			//Add to collection on single product page when out of stack
			woo_cl_add_to_collection_button();
			
			// collection button HTML
			$collection_btn_html	= ob_get_clean();
		}
		
		return $availability_html . $collection_btn_html;
	}

	/**
	 * Create Add To Collection & Item details Popup Wrapper
	 * 
	 * Handle to create add to collection & Item details popup Wrapper
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_create_collection_popups_wrapper() {

		global $post;

		$post_content	= !empty( $post->post_content ) ? $post->post_content : '';

		/**
		 * We have comment if condition because the add to collection popup will 
		 * not display for custom product lising using shortcode on other pages.
		 */
		//if( woo_cl_is_enable_collection_button() || has_shortcode( $post_content, 'woo_cl_collections' ) || has_shortcode( $post_content, 'woo_cl_add_to_collection' ) ) {

			// add to collection / item details popup wrapper
			do_action( 'woo_cl_add_to_collection_popup' );
		//}
	}

	/**
	 * AJAX Call for show dropbox
	 * 
	 * Handles AJAX Call to  show add to collection data in popup
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_show_add_to_collection_content() {

		global $current_user;

		//Initilize content
		$content = '';

		//User Id
		$user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';

		//Get product ID
		$product_id	= isset($_POST['coll_productid']) ? $_POST['coll_productid'] : '';

		//Get Variation ID
		$variation_id	= isset($_POST['coll_variationid']) ? $_POST['coll_variationid'] : '';

		//asign ID for get product detail
		$argid		= !empty( $variation_id ) ? $variation_id : $product_id;

		//Get product data
		$_product   = woo_cl_wc_get_product( $argid );
		
		ob_start();
		
		do_action( 'woo_cl_add_to_collection_popup_container', $_product, $user_ID  );
		
		$content .= ob_get_clean();
		
		//Responce array for collenction insert or update
		$response['success'] = __( 'Success', 'woocl' );
		$response['html'] 	 = $content;
		
		echo json_encode( $response );
		
		//Must write exit to return proper result in ajax
		exit;
	}
	
	/**
	 * AJAX Call for Add Product to Collection
	 * 
	 * Handles to add a specific product to selected Collection 
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_add_product_to_collection_ajax() {

		global $current_user;
		
		//Initilize collection flag
		$collflag	= false;
		
		//Get User Id
		$user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';

		//Get all Messages
		$messages	= woo_cl_messages();

		$coll_id        = isset( $_POST['coll_id']) 		? $_POST['coll_id']			: '';
		$coll_productid	= isset( $_POST['coll_productid'])	? $_POST['coll_productid']	: '';
		$html_type		= isset( $_POST['html_type'])		? $_POST['html_type'] 		: '';
		$variations		= isset( $_POST['variations'] ) 	? $_POST['variations']		: '';

		//Get title if exist
		$coll_producttitle = woo_cl_get_product_title( $coll_productid );
		
		// get collection description
		$collitem_disc = isset($_POST['collitem_disc']) ? $_POST['collitem_disc'] : '';

		// Get post author id
		$author_id = get_post_field( 'post_author', $coll_id );
		
		// check for post parent
		$main_id = wp_get_post_parent_id($coll_productid);
		$variation_id = $coll_productid;
		
		// Check if empty parent id
		if( empty($main_id) ) {
			
			$main_id = $coll_productid;
			$variation_id = '';
		}
		
		// Check collection button is enable
		if( !woo_cl_collection_btn_isenable($main_id, $variation_id) ) {
			
			$response['error'] = isset( $messages['can_not_add_product'] ) ? $messages['can_not_add_product'] : '';

			echo json_encode ($response);
			exit;
		}
		
		// check product limit per collection
		if( !woo_cl_check_product_limit_per_collection( $coll_id ) ) {//check total products in collection 

			$response['error'] = isset( $messages['products_limit_over'] ) ? $messages['products_limit_over'] : '';

			echo json_encode ($response);
			exit;
		}
		
		//Get collection items
		$collection_items = woo_cl_product_available_in_collection( $coll_id, $coll_productid, $author_id );
		
		if( !empty( $collection_items ) ) { // check if Product allready exist in that collection or not
			
			$response['error'] = isset( $messages['already_added'] ) ? $messages['already_added'] : '';

			echo json_encode( $response );
			exit;
		}
		
		if( !woo_cl_user_can_update_collection( $coll_id ) ) {
			
			$response['error'] = isset( $messages['prevent_add_access'] ) ? $messages['prevent_add_access'] : '';

			echo json_encode( $response );
			exit;
		}

		//Hook for add to collection after
		do_action('woo_cl_add_to_collection_save_data_after' );

		//get next menu order
		$next_menu_order	= $this->model->woo_cl_get_coll_item_next_manu_order( $coll_id );

		$args = array(
					'coll_id'			=> $coll_id,
					'coll_productid'	=> $coll_productid,
					'collitem_disc'		=> $collitem_disc,
					'variations'		=> $variations
				);
								
		//Add product to collection
		$collection_item_id = $this->model->woo_cl_add_product_to_collection( $args );
		
		$contenthtml = '';

		ob_start();
		
		if( $html_type == 'admin_side_meta' ) {
			echo $this->model->woo_cl_get_admin_products_by_collection_id($coll_id);
		} else {
			do_action( 'woo_cl_add_to_collection_success_popup', $coll_productid, $coll_id , $coll_producttitle, $collection_item_id );	
		}
		$contenthtml .= ob_get_clean();

		//Responce array for add to collection
		$response['success'] 	 = __( 'Success', 'woocl');
		$response['successhtml'] = $contenthtml;

		echo json_encode( $response );

		//Must write exit to return proper result in ajax
		exit;
	}

	/**
	 * AJAX Call for Create Collection
	 * 
	 * Handles AJAX Call for create Collection
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_create_collection() {

		global $current_user;

		//Get meta prefix
		$prefix		= WOO_CL_META_PREFIX;

		//Get collection post type
		$post_type = WOO_CL_POST_TYPE_COLL;

		//Get user Id
		$user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';

		//Get Guest option
		$cl_guest_options = get_option( 'cl_guest_options' );

		//Get New Collection Privacy
		$cl_new_coll_privacy = get_option( 'cl_new_coll_privacy' );

		//Get New Collection Privacy front end
		$cl_front_new_coll_privacy = get_option( 'cl_front_coll_privacy' );

		//Get all Messages
		$messages	= woo_cl_messages();

		//Get Maximum collections per user
		$cl_max_coll = woo_cl_max_collections_per_user( $user_ID );
		
		if( $cl_max_coll != '' ) {//Check Maximum Collection per user set

			//Get total collection of user id
			$total_colls	= woo_cl_count_user_collections( $user_ID, $post_type );
			
			if( $total_colls >= $cl_max_coll ) {//check total collections of user 

				$response['error'] = isset( $messages['colls_limit_over'] ) ? $messages['colls_limit_over'] : '';
	
				echo json_encode ($response);
				exit;
			}
		}

		if( isset( $_POST['collname'] ) && $_POST['collname'] != sprintf( __("Give your %s title.", "woocl"), woo_cl_get_label_singular() ) ) {
			$collname	= trim( $this->model->woo_cl_escape_slashes_deep( $_POST['collname'] ) );
		} else {
			$collname	= '';
		}

		if ( !is_user_logged_in() && $cl_guest_options != 'yes' ) { // check if user is not logged in, then not allow to create a collection

			$response['error'] = isset( $messages['create_must_login'] ) ? $messages['create_must_login'] : '';

			echo json_encode ($response);
			exit;
		}

		if( empty( $collname ) ) { // check if collection title is empty

			$response['error'] = isset( $messages['coll_valid_error'] ) ? $messages['coll_valid_error'] : '';

			echo json_encode( $response );
			exit;
		}

		//Collection data arguments
		$collargs = array(
						'post_title'	=> $collname,
						'post_type'		=> $post_type,
						'post_author'	=> $user_ID,
						'post_status'	=> 'publish',
					);

		//Get collection is exist or not
		$coll_exists = $this->model->woo_cl_collection_exists( $collargs );
		
		if( $coll_exists ) { // check if collection title is Exist or not
			
			$response['error'] = isset( $messages['coll_already_exist'] ) ? $messages['coll_already_exist'] : '';
			
			echo json_encode( $response );
			exit;
		}
		
		// collection argument
		$new_collection_args	= array(
									'post_type' 	=> $post_type,
									'post_status' 	=> 'publish',
									'post_title' 	=> $collname,
									'post_content' 	=> !empty( $_POST['coll_desc'] ) ? $_POST['coll_desc'] : '',
									'post_author' 	=> $user_ID,
									'menu_order' 	=> 0,
									'comment_status'=> 'closed'
								);

		//Set param to new collection privacy
		if( !empty($cl_front_new_coll_privacy) && $cl_front_new_coll_privacy == 'yes') {
			if( !empty( $_POST['collprivacy'] ) && $_POST['collprivacy'] != 'public' ) {
				$new_collection_args['post_status']	= $_POST['collprivacy'];
			}
		} else {
			if( !empty( $cl_new_coll_privacy ) && $cl_new_coll_privacy != 'public' ) {
				$new_collection_args['post_status']	= $cl_new_coll_privacy;
			}
		}

		//create new collection
		$new_collection_id		= wp_insert_post( $this->model->woo_cl_escape_slashes_deep( $new_collection_args ) );
		
		//Create access tocken for guest option
		woo_cl_create_token( $new_collection_id );
		
		$response['success'] 	= isset( $messages['coll_create_success'] ) ? $messages['coll_create_success'] : '';
		$response['collid'] 	= $new_collection_id;
		$response['colltitle'] 	= $collname;
		
		echo json_encode( $response );				
		exit;
	}
	
	/**
	 * AJAX Call for show item details
	 * 
	 * Handles AJAX Call for show item details popup
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_coll_item_details() {
			
		//Initilize content
		$content = '';

		//Get collection view page end point
		$woo_cl_view_slug	= get_option( 'cl_view_collection_text' );
		$woo_cl_view_slug	= trim( $woo_cl_view_slug ) != '' ? $woo_cl_view_slug : 'view-collection';

		//Get Collection ID
		$coll_id = isset($_POST['coll_id']) ? $_POST['coll_id'] : '';//get_query_var( $woo_cl_view_slug );
	
		//Get Collection Item ID
		$coll_item_id	= isset($_POST['coll_item_id']) ? $_POST['coll_item_id'] : '';
		
		//Get Product ID
		$product_id		= isset($_POST['product_id']) ? $_POST['product_id'] : '';
		
		//Get Product Data
		$_product 	= woo_cl_wc_get_product( $product_id );

		ob_start();
		
		// to show collection item popup
		do_action( 'woo_cl_collection_item_details_container', $coll_item_id, $_product, $coll_id );
		
		$content .= ob_get_clean();

		//Responce array for collenction insert or update
		$response['success'] = __( 'Success', 'woocl' );
		$response['html'] 	 = $content;
		
		echo json_encode( $response );
		
		//Must write exit to return proper result in ajax
		exit;
	}
	
	/**
	 * AJAX Call for Delete Collection
	 * 
	 * Handles AJAX Call for delete a collection
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_collection_delete() {
		
		global $current_user;
		
		//Get Collection ID
		$coll_id	= isset( $_POST['coll_id'] ) ? $_POST['coll_id'] : '';

		//Get current user name
		$user_name	= isset( $current_user->user_login ) ? $current_user->user_login : '';

		$coll_list_url = '';

		$isdelete = $this->model->woo_cl_post_delete( $coll_id ); // delete collection by id
		
		if( $isdelete == true ) {
			
			// get collection pag url by username
			$coll_list_url = $this->model->woo_cl_get_collection_page_url( $user_name );

			//Responce array for collenction delete success
			$response['success'] 	= __( 'Success', 'woocl' );
			$response['successurl'] = $coll_list_url;
			
			echo json_encode( $response );
			
			//Must write exit to return proper result in ajax
			exit;			
		}
		
		//Responce array for collenction delete error
		$response['error'] = __( 'error', 'woocl' );
		
		echo json_encode( $response );
		
		//Must write exit to return proper result in ajax
		exit;			
	}
	
	/**
	 * AJAX Call for Delete Collection Item
	 * 
	 * Handles AJAX Call for delete a collection item
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_collection_item_delete() {

		//Get meta prefix
		$prefix 	= WOO_CL_META_PREFIX;

		//Get all Messages
		$messages	= woo_cl_messages();

		//Get Collection Item ID
		$coll_item_id	= isset($_POST['coll_item_id']) ? $_POST['coll_item_id'] : '';
		
		//Get collection id of collection item
		$coll_id	= wp_get_post_parent_id( $coll_item_id ); 

		$isdelete 	= $this->model->woo_cl_post_delete( $coll_item_id ); // delete collection item by id
		
		if( $isdelete == true ) {
	
			//Get total price
			$totalprice	= $this->model->woo_cl_collection_products_total_price( $coll_id );

			//Responce array for collenction item delete success
			$response['totalprice'] = $totalprice;
			$response['success'] 	= __( 'Success', 'woocl' );
			
			echo json_encode( $response );
			
			//Must write exit to return proper result in ajax
			exit;			
		}
		
		//Responce array for collenction delete error
		$response['error'] = isset( $messages['something_wrong'] ) ? $messages['something_wrong'] : '';
		
		echo json_encode( $response );
		
		//Must write exit to return proper result in ajax
		exit;			
	}

	/**
	 * Update Collection Status
	 * 
	 * Handles Update a collection Status
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_collection_status_update() {
		
		//Get Collection ID , title, discription, item description
		$coll_id		= isset($_POST['coll_id']) ? $_POST['coll_id'] : '';
		$coll_status	= isset($_POST['coll_status']) ? $_POST['coll_status'] : '';
		
		//update coll_id, coll_status when both are not empty
		if( !empty( $coll_id ) && !empty( $coll_status ) ) {
			
			$edit_page_url 	= $this->model->woo_cl_get_collection_edit_page_url( $coll_id );//get Edit page url
			
			$statusargs = array(
			      					'ID'           => $coll_id,
			      					'post_status'  => $coll_status,
			  					);
			
			// update collection status
			wp_update_post( $statusargs );

			//Responce array for collenction status update success
			$response['success'] 	= __( 'Success', 'woocl' );
			$response['successurl'] = $edit_page_url;
			
			echo json_encode( $response );
			
			//Must write exit to return proper result in ajax
			exit;
		}
		
		//Responce array for collenction status update error
		$response['error'] = __( 'error', 'woocl' );
		
		echo json_encode( $response );
		
		//Must write exit to return proper result in ajax
		exit;			
	}
	
	/**
	 * Update Collection Cover Image
	 * 
	 * Handles Update a collection Cover Image
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_collection_coverimg_update() {
		
		//Get meta prefix
		$prefix = WOO_CL_META_PREFIX;
		
		//Get Collection ID product ID
		$coll_id		= isset( $_POST['coll_id'] ) ? $_POST['coll_id'] : '';
		$coll_item_id	= isset( $_POST['coll_item_id'] ) ? $_POST['coll_item_id'] : '';
		
		//update product id when both are not empty
		if( !empty( $coll_id ) && !empty( $coll_item_id ) ) {
			
			//collection item sort by menu order
			$coll_item_arg	= array(
									'post_parent'	=> $coll_id,
									'posts_per_page'=> -1,
									'post__not_in'	=> array( $coll_item_id )
								);
			
			//Collections Data
			$collectiions_data	= $this->model->woo_cl_get_collection_items( $coll_item_arg );
			
			if( !empty( $collectiions_data ) ) {// collection data
				
				$menu_order	= 1;
				
				// set this collection item to first position
				wp_update_post( array( 'ID' => $coll_item_id, 'menu_order' => 0 ) );
				
				foreach ( $collectiions_data as $collection ) {
					
					//get collection item id
					$order_item_id	= isset( $collection['ID'] ) ? $collection['ID'] :'';
					
					if( $order_item_id == $coll_item_id ) continue;
					
					// update collection item arguments
					$save_post_menu_order	= array(
													'ID'			=> $order_item_id,
													'menu_order'	=> $menu_order
												);
					
					// update collection item menu_order
					wp_update_post( $save_post_menu_order );
					
					$menu_order++;
				}
			}
			
			//Responce array for collenction cover update success
			$response['success'] 	= __( 'Success', 'woocl' );
			
			echo json_encode( $response );
			
			//Must write exit to return proper result in ajax
			exit;
		}
		
		//Responce array for collenction cover update error
		$response['error'] = __( 'error', 'woocl' );
		
		echo json_encode( $response );
		
		//Must write exit to return proper result in ajax
		exit;			
	}
	
	/**
	 * Update Collection
	 * 
	 * Handles Update a collection details
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_collection_update() {
		
		$coll_data			= isset($_POST['coll_data']) ? $_POST['coll_data'] : '';
		
		// get deserialize post data
		parse_str( $coll_data, $coll_details );

		//Get Collection ID , title, discription, item description
		$coll_id			= isset($coll_details['woocl_collection_id']) ? $coll_details['woocl_collection_id'] : '';
		$coll_title			= isset($coll_details['woocl_coll_title']) ? $this->model->woo_cl_escape_slashes_deep( $coll_details['woocl_coll_title'] ) : '';
		$coll_description	= isset($coll_details['woocl_coll_desc']) ? $coll_details['woocl_coll_desc'] : '';
		$allow_editor 		= get_option( 'cl_enable_editor_coll_desc' );

		//Check and escape html and slashes
		if( $allow_editor == 'yes') {
			$coll_description = $this->model->woo_cl_escape_slashes_deep( $coll_description, true);
		} else if(apply_filters( 'woo_cl_collection_discription_escape_slashes_deep', true ) || $allow_editor == 'no'){
			$coll_description = $this->model->woo_cl_escape_slashes_deep( $coll_description);
		}

		$coll_items			= isset($coll_details['woocl_coll_item_disc']) ? $coll_details['woocl_coll_item_disc'] : '';

		$item_page_url 	= $this->model->woo_cl_get_collection_item_page_url( $coll_id );//get collection item page url
			
		if( !empty( $coll_id ) || !empty( $coll_items ) ) {//if collection id and collection item description array not empty
			
			//update title when not empty
			if( !empty( $coll_title ) ) {
				
				$coll_details = array(
				      					'ID'           => $coll_id,
				      					'post_title'   => $coll_title,
				      					'post_content' => $coll_description
				  					);
				
				// update collection
				wp_update_post( $coll_details );
			}
			
			if( !empty( $coll_items ) ) { // if not empty collection item
				
				foreach ( $coll_items as $key => $value ) {
	
					if( !empty( $key ) ) {//if item id not empty 
	
						$item_desc 	= array(
						      					'ID'           => $key,
						      					'post_content' => $this->model->woo_cl_escape_slashes_deep( $value['desc'] )
						  					);
						
						// update collection item description
						wp_update_post( $item_desc );
					}
				}
			}
			
			//Responce array for collenction status update success
			$response['success'] 	= __( 'Success', 'woocl' );
			$response['successurl'] = $item_page_url;
			
			echo json_encode( $response );
			
			//Must write exit to return proper result in ajax
			exit;			
		}
		
		//Responce array for collenction status update error
		$response['error'] 	= __( 'error', 'woocl' );
		
		echo json_encode( $response );
		exit;
	}
	
	/**
	 * Share via Email
	 * 
	 * Handles Ajax for Share via email
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_coll_item_email_share() {
		
		// sender details
		$coll_id 		= isset( $_POST['coll_id'] ) ? $_POST['coll_id'] : '';
		$sender_name 	= isset( $_POST['sender_name'] ) ? $this->model->woo_cl_escape_attr( $_POST['sender_name'] ) : '';
		$sender_email 	= isset( $_POST['sender_email'] ) ? $_POST['sender_email'] : '';
	
		$friend_emails 	= isset( $_POST['friend_emails'] ) ? $_POST['friend_emails'] : '';
		$site_name 		= get_bloginfo('name');
	
		$message 		= isset( $_POST['message'] ) ? $this->model->woo_cl_escape_attr( $_POST['message'] ) : '';

		//make link on sender email and site url
		//$sender_email	= '<a href="mailto:' . $sender_email . '">' . $sender_email . '</a>';
		$site_url		= '<a href="' . get_site_url() . '">' . get_site_url() . '</a>';//get site url
		
		$sharelink 		= woo_cl_get_collection_share_url( $coll_id ); // get collection share link
	
	    // validation
	    if ( ! ( $sender_name || $sender_email || ! woo_cl_validate_share_emails( $friend_emails ) ) ) {
	        $has_error = true;
	    }

		if ( ! isset( $has_error ) ) {//no error then all mailing process

			//Get All Data for share collection
			$share_data	= array(
									'sender_name'	=> $sender_name,
									'sender_email'	=> $sender_email,
									'friend_emails'	=> $friend_emails,
									'message'		=> $message,
									'sharelink'		=> $sharelink,
									'site_url'		=> $site_url,
									'site_name'		=> $site_name,
								);
			
			
	        // Fires after share email collection field validated.
	       do_action( 'woo_cl_share_collection_email', $share_data );

			//response for success mail done
			$response['success'] = __( 'Success','woocl' );
		 
			echo json_encode( $response );
			exit;
		}
		
		//response for error
		$response['error'] = __( 'Error','woocl' );
	 
		echo json_encode( $response );
		exit;
	}
	
	/**
	 * Add Product to Cart
	 * 
	 * Handles Ajax for add product to woocommerce cart
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_add_to_cart_process() {
		
		global $woocommerce;

		// Get Product Id if exist
		$product_id 	= isset( $_POST['product_id'] ) ? $_POST['product_id'] : '';

		// Get Variation Id if exist
		$variation_id 	= isset( $_POST['variation_id'] ) ? $_POST['variation_id'] : '';
		$variations 	= isset( $_POST['variation_data'] ) ? json_decode( stripslashes( $_POST['variation_data'] ), true ) : '';

		if( !is_user_logged_in() ) {//If user not log
			$woocommerce->session->set_customer_session_cookie(true);
		}

		$cart_responce	= '';
		if( !empty( $product_id ) && empty( $variation_id ) ) {//check if simple product

			//Cart Responce
			$cart_responce	= $woocommerce->cart->add_to_cart( $product_id );			
		
		} elseif ( !empty( $product_id ) && !empty( $variation_id ) ) {//check if variation available
			
			//Check if selected variation not exists in collection item
			if( empty( $variations ) ) {

				//Get object of product variation class
				$variation_data	= new WC_Product_Variation( $variation_id );
				$variations		= $variation_data->get_variation_attributes();
			}

			//Cart Responce
			$cart_responce	= $woocommerce->cart->add_to_cart( $product_id, 1, $variation_id, $variations );
		}
		
		ob_start();
		wc_print_notices();
		$message = ob_get_clean();
		
		if( !empty( $cart_responce ) ) {//check if product added to cart successfully
			
			//response for success
			$response['success']	= __( 'Success','woocl' );			
			$response['message']	= $message;			
		} else {
			
			//response for success
			$response['error']		= __( 'error','woocl' );
			$response['message']	= $message;
		}
	 	
		echo json_encode( $response );
		exit;
	}
	
	/**
	 * Template Redirect
	 * 
	 * Handle to redirect on collection listing and single collection page
	 * which are set in the collection setting
	 * When open CPT collection listing and CPT collection single page 
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_template_redirect() {

		global $post;

		//Get collection view page end point
		$woo_cl_view_slug	= get_option( 'cl_view_collection_text' );
		$woo_cl_view_slug	= trim( $woo_cl_view_slug ) != '' ? $woo_cl_view_slug : 'view-collection';

		//Get collection edit end point
		$woo_cl_edit_slug	= get_option( 'cl_edit_collection_text' );
		$woo_cl_edit_slug	= trim( $woo_cl_edit_slug ) != '' ? $woo_cl_edit_slug : 'edit-collection';

		//Get user endpoint
		$woo_cl_user_slug	= get_option( 'cl_collection_author_text' );
		$woo_cl_user_slug	= trim( $woo_cl_user_slug ) != '' ? $woo_cl_user_slug : 'collection-author';

		$post_id				= isset( $post->ID ) ? $post->ID : '';
		$coll_item_page			= get_option( 'cl_coll_items_page' );
		$collection_page_url	= $this->model->woo_cl_get_collection_page_url();

		if ( is_single() && get_post_type() == WOO_CL_POST_TYPE_COLL ) {//check it is single collection page

			$coll_item_page_url	= $this->model->woo_cl_get_collection_item_page_url( $post_id );

			wp_redirect( $coll_item_page_url );
			exit;

		} else if ( is_front_page() && get_post_type() == WOO_CL_POST_TYPE_COLL ) {//check it is collection listing page

			if( !empty( $collection_page_url ) ) {

				wp_redirect( $collection_page_url );
				exit;
			}
		}

		//If invalid collection pages
		if ( ( woo_cl_is_invalid_page( $woo_cl_view_slug ) 
			|| ( woo_cl_is_invalid_page( $woo_cl_edit_slug ) ) 
				|| woo_cl_is_invalid_page( $woo_cl_user_slug ) ) ) {

					if( !empty( $collection_page_url ) ) {
						wp_redirect( $collection_page_url );
						exit;
					}
		}
	}

	/**
	 * Adding Hooks
	 * 
	 * Adding proper hooks for the public pages.
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_add_rewrite_tag() {

		//Get collection view page end point
		$woo_cl_view_slug	= get_option( 'cl_view_collection_text' );
		$woo_cl_view_slug	= trim( $woo_cl_view_slug ) != '' ? $woo_cl_view_slug : 'view-collection';

		//Get collection edit end point
		$woo_cl_edit_slug	= get_option( 'cl_edit_collection_text' );
		$woo_cl_edit_slug	= trim( $woo_cl_edit_slug ) != '' ? $woo_cl_edit_slug : 'edit-collection';

		//Get user endpoint
		$woo_cl_user_slug	= get_option( 'cl_collection_author_text' );
		$woo_cl_user_slug	= trim( $woo_cl_user_slug ) != '' ? $woo_cl_user_slug : 'collection-author';

		add_rewrite_tag( '%'.$woo_cl_view_slug.'%', '([^/]+)');
		add_rewrite_tag( '%'.$woo_cl_edit_slug.'%', '([^/]+)');
		//add_rewrite_tag( '%'.$woo_cl_user_slug.'%', '([^/]+)');
	}

	/**
	 * Add My Colection Menu In Admin Bar
	 * 
	 * Handle to add my colection menu in admin bar
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_action_admin_bar_menu( $wp_admin_bar ) {

		global $current_user;

		//Get User ID
		$user_name	= isset( $current_user->user_login ) ? $current_user->user_login : '';

		if( !empty( $user_name ) ) {//If user is not empty

			$mycoll_url	= $this->model->woo_cl_get_collection_page_url( $user_name );

			if( !empty( $mycoll_url ) ) {//Get my collection URL

				$wp_admin_bar->add_group( array(
					'parent' => 'my-account',
					'id'     => 'woo-cl-mycoll',
				));

				$wp_admin_bar->add_menu( array(
					'parent'	=> 'woo-cl-mycoll',
					'id'		=> 'my-collection',
					'title'		=> sprintf( __( 'My %s', 'woocl' ), woo_cl_get_label_singular() ),
					'href'		=> $mycoll_url
				) );
			}
		}
	}

	/**
	 * Get Collection Data For Pagination Loop
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_get_loop_content() {

		global $wp, $woo_cl_shortcode, $woo_cl_model;
		
		// Declare required variables
		$cl_get_collection_orderby = $cl_get_collection_order = $cl_get_collection_array = '';

		//Get meta prefix
		$prefix = WOO_CL_META_PREFIX;

		//Get all Messages
		$messages	= woo_cl_messages();

		//Get guest token
		$cl_token	= woo_cl_get_list_token();

		//Get user endpoint
		$collection_user_args	= get_option( 'cl_collection_author_text' );
		$collection_user_args	= trim( $collection_user_args ) != '' ? $collection_user_args : 'collection-author';
		
		// Get option for collection sorting
		$cl_get_collection_sort_by 	= get_option( 'cl_sort_collection_by' );
		
		// Explode setting for getting 'Orderby' and 'Order' settings
		$cl_get_collection_array 	= !empty ( $cl_get_collection_sort_by ) ? explode( '-', $cl_get_collection_sort_by ) : '';
		
		if ( ! empty ( $cl_get_collection_array ) && is_array ( $cl_get_collection_array ) ) {
			$cl_get_collection_orderby 	= !empty ( $cl_get_collection_array[0] ) ? $cl_get_collection_array[0] : '';
			$cl_get_collection_order 	= !empty ( $cl_get_collection_array[1] ) ? strtoupper( $cl_get_collection_array[1] ) : '';
		}

		$woo_cl_user			= isset( $wp->query_vars[$collection_user_args] ) ? $wp->query_vars[$collection_user_args] : '';
		//$user_ID				= $woo_cl_model->woo_cl_get_userid_by_username( $woo_cl_user );
		$current_page			= isset( $_POST['current_page'] ) ? $_POST['current_page'] : 2;
		$user_ID				= isset( $_POST['data_author'] ) ? $_POST['data_author'] : '';
		$per_page_count 		= get_option( 'cl_pagination_options' );
		$per_page_count 		= !empty( $per_page_count )? $per_page_count : WOO_CL_POST_PER_PAGE;
		
		$args = array(
						'author' 		 => $user_ID,
						'post_status'	 => array( 'publish', 'private' ),
						'posts_per_page' => $per_page_count,
						'paged' 		 => $current_page,
						'orderby'		 => !empty( $cl_get_collection_orderby ) ? $cl_get_collection_orderby : 'date', // Set Orderby as of Global settings, 'date' if not set
						'order'   		 => !empty( $cl_get_collection_order ) ? $cl_get_collection_order : 'DESC', // Set order as of Global settings, 'DESC' if not set
					);

		if( empty( $user_ID ) ) {

			//Arguments for guest user collection
			$args = array(
							'meta_query'	=> array(
													'relation' => 'OR',
													array(
															'key' 		=> $prefix.'cl_token',
															'compare'	=> 'NOT EXISTS'
														),
													array( 
															'key' 	=> $prefix.'cl_token', 
															'value' => $cl_token
														)
													),
							'post_status'	 => array( 'publish' ),
							'posts_per_page' => $per_page_count,
							'paged' 		 => $current_page,
							'orderby'		 => !empty( $cl_get_collection_orderby ) ? $cl_get_collection_orderby : 'date', // Set Orderby as of Global settings, 'date' if not set
							'order'   		 => !empty( $cl_get_collection_order ) ? $cl_get_collection_order : 'DESC', // Set order as of Global settings, 'DESC' if not set
						);
		}

		//Change order and order by if sorting
		if( !empty( $_POST['sort'] ) ) {

			// Explode setting for getting 'Orderby' and 'Order' settings
			$cl_get_collection_array 	= !empty ( $_POST['sort'] ) ? explode( '-', $_POST['sort'] ) : '';
			$cl_get_collection_orderby 	= !empty ( $cl_get_collection_array[0] ) ? $cl_get_collection_array[0] : '';
			$cl_get_collection_order 	= !empty ( $cl_get_collection_array[1] ) ? strtoupper( $cl_get_collection_array[1] ) : '';
			$args['orderby']        = !empty( $cl_get_collection_orderby ) ? $cl_get_collection_orderby : 'date'; // Set Orderby as of Global settings, 'date' if not set
			$args['order']			= !empty( $cl_get_collection_order ) ? $cl_get_collection_order : 'DESC'; // Set order as of Global settings, 'DESC' if not set
		}

		//Get collection data by search if searching
		if( !empty( $_POST['search'] ) ){
			$args['s']	= $_POST['search'];
		}
		
		//Get Collection data
		$collections	= $woo_cl_model->woo_cl_get_collections( $args );
		
		if( !empty( $collections ) ) {
			
			ob_start();
			
			do_action( 'woo_cl_collection_listing_loop', $collections, $user_ID );		
			
			$content_html = ob_get_clean();
		} else {
			
			$no_coll_found	= isset( $messages['no_coll_found'] ) ? $messages['no_coll_found'] : '';
			$content_html = "<div id='no-collection-loop' class='no-collection-loop'>".$no_coll_found."</div>";
		}
		
		echo $content_html;
		exit;
	}
	
	/**
	 * Get Collection Data For Pagination Loop
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_item_get_loop_content() {
		
		global $wp, $woo_cl_shortcode, $woo_cl_model;
		
		// Declare required variables
		$cl_get_collection_orderby = $cl_get_collection_order = $cl_get_collection_array = '';

		//Get all Messages
		$messages	= woo_cl_messages();
		
		// Get Collection Item List by option
		$cl_sort_collection_item_by = get_option( 'cl_sort_collection_item_by' );
		
		// Explode setting for getting 'Orderby' and 'Order' settings
		$cl_get_collection_array 	= !empty ( $cl_sort_collection_item_by ) ? explode( '-', $cl_sort_collection_item_by ) : '';
		
		if ( ! empty ( $cl_get_collection_array ) && is_array ( $cl_get_collection_array ) ) {
			$cl_get_collection_orderby 	= !empty ( $cl_get_collection_array[0] ) ? $cl_get_collection_array[0] : '';
			$cl_get_collection_order 	= !empty ( $cl_get_collection_array[1] ) ? strtoupper( $cl_get_collection_array[1] ) : '';
		}

		/*$view_collection_args	= get_option( 'cl_view_collection_text' );
		$view_collection_args	= trim( $view_collection_args ) != '' ? $view_collection_args : 'view-collection';

		$coll_id				= $wp->query_vars[$view_collection_args];*/
		$current_page			= isset( $_POST['current_page'] ) ? $_POST['current_page'] : 2;
		$coll_id				= isset( $_POST['woocl_coll_id'] ) ? $_POST['woocl_coll_id'] : '';
		$per_page_count 		= get_option( 'cl_pagination_options' );
		$per_page_count 		= !empty( $per_page_count )? $per_page_count : WOO_CL_POST_PER_PAGE;
		$args = array(
						'post_parent'		=> $coll_id,
						'posts_per_page'	=> $per_page_count,
						'paged'				=> $current_page,
						'orderby'			=> !empty( $cl_get_collection_orderby ) ? $cl_get_collection_orderby : 'date', // Set Orderby as of Global settings, 'date' if not set
						'order'				=> !empty( $cl_get_collection_order ) ? $cl_get_collection_order : 'DESC' // Set order as of Global settings, 'DESC' if not set
					);

		//Change order and order by if sorting
		if( !empty( $_POST['sort'] ) ) {

			// Explode setting for getting 'Orderby' and 'Order' settings
			$cl_get_collection_array 	= !empty ( $_POST['sort'] ) ? explode( '-', $_POST['sort'] ) : '';
			$cl_get_collection_orderby 	= !empty ( $cl_get_collection_array[0] ) ? $cl_get_collection_array[0] : '';
			$cl_get_collection_order 	= !empty ( $cl_get_collection_array[1] ) ? strtoupper( $cl_get_collection_array[1] ) : '';
			$args['orderby']        = !empty( $cl_get_collection_orderby ) ? $cl_get_collection_orderby : 'date'; // Set Orderby as of Global settings, 'date' if not set
			$args['order']			= !empty( $cl_get_collection_order ) ? $cl_get_collection_order : 'DESC'; // Set order as of Global settings, 'DESC' if not set
		}

		//Get collection data by search if searching
		if( !empty( $_POST['search'] ) ){
			$args['s']	= $_POST['search'];
		}
		
		//Get Collection data
		$coll_items	= $woo_cl_model->woo_cl_get_collection_items( $args );
		
		if( !empty( $coll_items ) ) {
			
			ob_start();
			
			do_action( 'woo_cl_collection_item_listing_loop', $coll_items, $coll_id, false );		
			
			$content_html = ob_get_clean();
		} else {
			
			$no_coll_item_found	= isset( $messages['no_coll_item_found'] ) ? $messages['no_coll_item_found'] : '';
			$content_html = "<div id='no-collection-loop' class='no-collection-loop'>".$no_coll_item_found."</div>";
		}
		
		echo $content_html;
		exit;
	}

	/**
	 * Add custom email notification to woocommerce
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_add_email_notification( $email_actions ) {
		
		$email_actions[]	= 'woo_cl_share_collection_email';
		
		return $email_actions;
	}
	
	/**
	 * Insert Collection Item Notification
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function wpw_fp_insert_collection_item( $coll_productid, $collection_id , $collection_item_name, $collection_item_id ) {
		
		if( woo_cl_is_follow_activate() ) {
			
			global $wpw_fp_model;
			
			$prefix 	= WOO_CL_META_PREFIX;
			
			//Get Collection name
			$collection_name	= get_the_title( $collection_id );
			
			//Update collection product id
			$product_id		= get_post_meta( $collection_item_id, $prefix.'coll_product_id', true );
			
			//Get collection data
			$coll_item_data	= get_post( $collection_item_id );
			
			//Get Collection Content
			$collection_item_description	= $this->model->woo_cl_get_limit_char( $coll_item_data->post_content );
			
			$cl_fp_shortcodes	= array( "{collection_name}", "{collection_item_name}", "{collection_item_description}" );
			$cl_fp_replacecodes	= array( $collection_name, $collection_item_name, $collection_item_description );
			
			$subject	= get_option( 'cl_email_subject' );
			$subject	= str_replace( $cl_fp_shortcodes, $cl_fp_replacecodes, $subject );
			
			$message	= get_option( 'cl_email_body' );
			$message	= str_replace( $cl_fp_shortcodes, $cl_fp_replacecodes, nl2br( $message ) );
			
			//For Notify collection followers
			$flag		= $wpw_fp_model->wpw_fp_post_send_mail( $subject, $message, $collection_id );

			//For Notify author's followers
			$coll_author= get_post_field( 'post_author', $collection_id );
			$flag		= $wpw_fp_model->wpw_fp_author_send_mail( $coll_author, $subject, $message, $collection_id );
		}
	}
	
	/**
	 * Display Follow message on Collection page
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_follow_message_collection_id( $post_id = '' ) {

		//Get collection view page end point
		$woo_cl_view_slug	= get_option( 'cl_view_collection_text' );
		$woo_cl_view_slug	= trim( $woo_cl_view_slug ) != '' ? $woo_cl_view_slug : 'view-collection';
		
		if( woo_cl_is_page( $woo_cl_view_slug ) ) {
			
			//Get collection ID
			$collection_id	= get_query_var( $woo_cl_view_slug );
			
			//Assign collection id to post ID
			$post_id		= $collection_id;
		}
		
		return $post_id;
	}
	
	/**
	 * Get Collection Subscribed URL
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_subscribed_collection_url( $redirect_url = '', $colloection_id = '' ) {
		
		if( woo_cl_is_collection( $colloection_id ) ) {
			
			$redirect_url	= $this->model->woo_cl_get_collection_item_page_url( $colloection_id );
		}
		
		return $redirect_url;
	}		
		
	/**
	 * Send follow my blog post
	 * email using woocommerce template
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_send_email_externaly( $externally_send_email, $email, $subject, $headers, $message ) {
				
		// load the mailer class
		$mailer        = WC()->mailer();	
		$emails = new WC_Email(); // create a new email to send mail using woocommerce
		
		// get site name and set it to heading
		$site_name = get_bloginfo( 'name' ); 	
		$attachments = '';
		
		// get unsubscibe message
		$unsubscibe_message = wpw_fp_get_unsubscribe_message();
		if( !empty( $unsubscibe_message ) )
			$message .= $unsubscibe_message;
	
		ob_start();
		
		// get header template
		wc_get_template( 'emails/email-header.php', array( 'email_heading' => $site_name ) );
		
		// actual message
		echo $message;
		
		// get footer template
		wc_get_template( 'emails/email-footer.php' );
		
		$message = ob_get_clean();
		
		// send email
		$externally_send_email['success'] = $emails->send( $email, $subject, $message, $headers, $attachments );
		
		$externally_send_email['send_mail_externally'] = true;
		
		return $externally_send_email;
	}

	/**
	 * Display Collection to WooCommerce My Account page
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_woocommerce_account_page_collection() {

		$no_of_collections	=  woo_cl_get_no_of_collection_on_my_account_page();		

		$args = array(
						'post_status'	 	=> array( 'publish', 'private' ),							
						'posts_per_page' 	=> $no_of_collections,							
						'paged'	 		 	=> 1,							
						'collection_title'	=> sprintf( __( 'My %s', 'woocl' ), woo_cl_get_label_plural() ),							
						'enable_see_all'	=> 'true',							
						'show_count'		=> 'true',
						'fixed_coll'	 	=> true
					);
		
		woo_cl_my_collections( apply_filters( 'woo_cl_woocommerce_account_page_collection_args', $args ) );
	}

	/**
	 * Add My Collections tab on Ultimate Member
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function woo_cl_um_mycollection_tab( $tabs ) {

		$tabs[800]['mycollection']['icon'] = 'um-icon-heart';
		$tabs[800]['mycollection']['title'] = sprintf( __( 'My %s', 'woocl' ), woo_cl_get_label_plural() );
		$tabs[800]['mycollection']['custom'] = true;
		return $tabs;
	}

	/**
	 * Link tab content My Collections tab on Ultimate Member
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function woo_cl_um_account_tab__mycollection( $info ) {
		global $ultimatemember;
		extract( $info );

		$output = $ultimatemember->account->get_tab_output( 'mycollection' );
		if ( $output ) { 
			echo $output; 
		}
	}

	/**
	 * Build Tab Content My Collections tab on Ultimate Member
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	function woo_cl_um_account_content_hook_mycollection( $output ){

		ob_start(); ?>

		<div class="um-field">
			
		<?php
			$no_of_collections	=  woo_cl_get_no_of_collection_on_my_account_page();		

			$args = array(
				'post_status'	 	=> array( 'publish', 'private' ),							
				'posts_per_page' 	=> $no_of_collections,							
				'paged'	 		 	=> 1,							
				'collection_title'	=> sprintf( __( 'My %s', 'woocl' ), woo_cl_get_label_plural() ),							
				'enable_see_all'	=> 'true',							
				'show_count'		=> 'true',
				'fixed_coll'	 	=> true
			);
			woo_cl_my_collections( apply_filters( 'woo_cl_um_account_page_collection_args', $args ) );
		?>
		</div>		

		<?php
		$output .= ob_get_contents();
		ob_end_clean();

		return $output;
	}

	/**
	 * Added Tab My Collections on BuddyPress Profile
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function woo_cl_buddypress_profile_tab_mycollections() {

		global $bp;

		//Build counts for collections tab
		$count = woo_cl_count_user_collections( bp_current_user_id(), WOO_CL_POST_TYPE_COLL );
		$count_msg = !empty( $count ) ? '<span class="count">'. $count .'</span>' : '<span class="no-count">0</span>';

		//Create tab
	    bp_core_new_nav_item( array( 
            'name' => sprintf( __( 'My %s %s', 'woocl' ), woo_cl_get_label_plural(), $count_msg ),
            'slug' => 'mycollection', 
            'screen_function' 	=> array( $this, 'woo_cl_buddypress_mycollection_callback' ), 
            'position' 			=> 40,
            'show_for_displayed_user' => false,
            'parent_url'      	=> $bp->displayed_user->domain,
			'parent_slug'     	=> $bp->profile->slug,
			'default_subnav_slug' => 'mycollection'
      	) );
	}

	/**
	 * My Collections Tab Callback on BuddyPress Profile
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function woo_cl_buddypress_mycollection_callback() {

		//Action to display title and content
		add_action( 'bp_template_title', array( $this, 'woo_cl_buddypress_mycollections_title' ) );
    	add_action( 'bp_template_content', array( $this, 'woo_cl_buddypress_mycollections_content' ) );

    	//Load buddypress template
    	bp_core_load_template( apply_filters( 'woo_cl_bp_core_template_plugin', 'members/single/plugins' ) );
	}

	/**
	 * My Collections Title on BuddyPress Profile
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function woo_cl_buddypress_mycollections_title() {
	  	echo sprintf( __( 'My %s', 'woocl' ), woo_cl_get_label_plural() );
	}

	/**
	 * My Collections Tab Content on BuddyPress Profile
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function woo_cl_buddypress_mycollections_content() {

		//Get no of collections to display
	  	$no_of_collections	=  woo_cl_get_no_of_collection_on_my_account_page();		

		$args = array(
						'post_status'	 	=> array( 'publish', 'private' ),							
						'posts_per_page' 	=> $no_of_collections,							
						'paged'	 		 	=> 1,							
						'collection_title'	=> sprintf( __( 'My %s', 'woocl' ), woo_cl_get_label_plural() ),							
						'enable_see_all'	=> 'true',							
						'show_count'		=> 'true',
						'fixed_coll'	 	=> true
					);

		woo_cl_my_collections( apply_filters( 'woo_cl_buddypress_account_page_collection_args', $args ) );
	}

	/**
	 * Add Selected products to cart
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_add_bulk_products_to_cart() {
	
		global $woocommerce;

		// Get products Ids if exist
		$product_ids 	= isset( $_POST['product_ids'] ) ? $_POST['product_ids'] : '';
		$product_ids	= explode( ',', rtrim( $product_ids, ',' ) );

		$cart_responce	= '';
		$cnt_added		= 0;
		if( !empty( $product_ids ) ) {
			foreach ( $product_ids as $product_id ) {
	
				$_product	= woo_cl_wc_get_product( $product_id );
				if( $_product->is_type('variation') || $_product->is_type('variable') ) {
	
					//Cart Responce
					$variation		= $_product->get_variation_attributes();
					$variation_id	= $product_id;
					$product_id		= $_product->id;
					$cart_responce	= $woocommerce->cart->add_to_cart( $product_id, 1, $variation_id, $variation );
					$products[] 	= $_product->get_title();
					$cnt_added++;
				} else {
	
					//Cart Responce
					$cart_responce	= $woocommerce->cart->add_to_cart( $product_id );
					$products[] 	= $_product->get_title();
					$cnt_added++;
				}
			}
		}

		$products_name = implode(', ', $products);

		$message = "<div class='woocommerce woocommerce-message'>
					<a href='" . WC()->cart->get_cart_url() . "' class='button wc-forward'>". __( 'View cart', 'woocl' ) . "</a> 
					". __( '"' . $products_name . '" has been added to your cart.', 'woocl' ) . "
					</div>";
		
		if( !empty( $cart_responce ) ) {//check if product added to cart successfully
	
			//response for success
			$response['success']	= __( 'Success','woocl' );
			$response['message']	= $message;
		} else {

			//response for error
			$response['error']		= __( 'error','woocl' );
		}
	 
		echo json_encode( $response );
		exit;
	}

	/**
	 * My Collections on WC Vendors Dashboard
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function woo_cl_wcvendors_dashboard_tab_mycollections() {

		//Get collection page and added tab
		global $current_user;
		$username = !empty( $current_user->data->user_login ) ? $current_user->data->user_login : 'guest';
		$collection_author_url = $this->model->woo_cl_get_collection_page_url( $username );
		echo '<a target="_TOP" href="'. $collection_author_url .'" class="button">'. sprintf( __( 'My %s', 'woocl' ), woo_cl_get_label_plural() ) .'</a>';
	}

	/**
	 * My Collections on WC Vendors Dashboard Pro
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function woo_cl_wcvendors_dashboard_pro_tab_mycollections( $dashboard_pages ) {

		//Get collection page and added tab
		global $current_user;
		$username = !empty( $current_user->data->user_login ) ? $current_user->data->user_login : 'guest';
		$collection_author_url = $this->model->woo_cl_get_collection_page_url( $username );
		$dashboard_pages[ 'mycollection' ] = array( 
			'slug'			=> $collection_author_url, 
			'label'			=> sprintf( __( 'My %s', 'woocl' ), woo_cl_get_label_plural() ), 
			'actions'		=> array()
		);

		return $dashboard_pages;
	}

	/**
	 * Track viewed collections & their order
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function woo_cl_track_collection_recent_viewed() {

		if ( ! is_active_widget( false, false, 'woo_cl_recently_viewed_collections', true ) ) {
			return;
		}

		global $wp;

		//Get collection view page end point
		$woo_cl_view_slug	= woo_cl_get_slug( 'view' );

		//Get Collection id
		$coll_id	= !empty( $wp->query_vars[$woo_cl_view_slug] ) ? $wp->query_vars[$woo_cl_view_slug] : '';
		
		if ( empty( $_COOKIE['woo_cl_recently_viewed'] ) )
			$viewed_collections = array();
		else
			$viewed_collections = (array) explode( '|', $_COOKIE['woo_cl_recently_viewed'] );
	
		if ( ! in_array( $coll_id, $viewed_collections ) ) {
			//$viewed_collections[] = $coll_id;
			//array_unshift( $viewed_collections, $coll_id );
			$coll_arr	= array( $coll_id );
			$viewed_collections = array_merge( $coll_arr , $viewed_collections );
		} else {
			$coll_arr	= array( $coll_id );
			$key = array_search($coll_id, $viewed_collections);
			unset($viewed_collections[$key]);
			$viewed_collections = array_merge( $coll_arr, $viewed_collections );
		}

		if ( sizeof( $viewed_collections ) > 15 ) {
			array_shift( $viewed_collections );
		}

		// Store for session only
		wc_setcookie( 'woo_cl_recently_viewed', implode( '|', $viewed_collections ) );
	}

	/**
	 * Add Collections tab to Dokan Vendor Dashboard page
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.2.2
	 */
	public function woo_cl_dokan_dashboard_tab_mycollections( $urls ) {

        global $current_user;

        //Get User ID
        $user_name  = isset( $current_user->user_login ) ? $current_user->user_login : '';

        if( !empty( $user_name ) ) {//If user is not empty

            $mycoll_url = $this->model->woo_cl_get_collection_page_url( $user_name );

	        $urls['mycollection'] = array( 
	            
	            'title' => sprintf( __( 'My %s', 'woocl' ), woo_cl_get_label_plural() ),
	            'icon'  => '<i class="fa fa-heart"></i>',
	            'url'   => $mycoll_url,
	            'pos'=> 198
	    
	        );

	        return $urls;
	    }
	}

	/**
	 * Add Collections tab to
	 * WCMp Vendor Dashboard page
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.2.2
	 */
	public function woo_cl_wcmp_dashboard_tab_mycollections( $vendor_nav ){

		global $current_user;

        //Get User ID
        $user_name  = isset( $current_user->user_login ) ? $current_user->user_login : '';

        if( !empty( $user_name ) ) {//If user is not empty

            $mycoll_url = $this->model->woo_cl_get_collection_page_url( $user_name );
			$vendor_nav['mycollection'] = array(
	            'label' => sprintf( __( 'My %s', 'woocl' ), woo_cl_get_label_plural() ),
	            'url' => $mycoll_url,
	            'capability' => apply_filters('wcmp_vendor_dashboard_menu_collection_capability', true),
	            'position' => 70,
	            'submenu' => array(),
	            'link_target' => '_blank',
	            'nav_icon' => 'wcmp-font ico-add-icon',
			);
		}

		return $vendor_nav;
	}

	/**
	 * Adding Hooks
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function add_hooks() {
		
		// add action to call function after add to cart button 
		add_action( 'woocommerce_after_add_to_cart_button', array( $this, 'woo_cl_add_to_collection_button' ) );		
		
		//Add to collection button on shop page
		add_action( 'woocommerce_after_shop_loop_item', array( $this, 'woo_cl_add_to_collection_button' ) );
		
		//add to collection & item details  popup wrapper
		add_action( 'wp_footer', array( $this, 'woo_cl_create_collection_popups_wrapper' ) );
		
		//add action to call ajax for show dropbox
		add_action( 'wp_ajax_show_add_to_collection_content', array( $this, 'woo_cl_show_add_to_collection_content' ) );
		add_action( 'wp_ajax_nopriv_show_add_to_collection_content', array( $this, 'woo_cl_show_add_to_collection_content' ) );
		
		//add action to call ajax for add product to collection
		add_action( 'wp_ajax_woo_cl_add_product_to_collection', array($this, 'woo_cl_add_product_to_collection_ajax'));
		add_action( 'wp_ajax_nopriv_woo_cl_add_product_to_collection', array($this, 'woo_cl_add_product_to_collection_ajax'));
		
		//add action to call ajax for create collection
		add_action( 'wp_ajax_woo_cl_create_collection', array( $this, 'woo_cl_create_collection' ));
		add_action( 'wp_ajax_nopriv_woo_cl_create_collection', array( $this, 'woo_cl_create_collection' ) );
		
		//add action to call ajax for delete collection
		add_action( 'wp_ajax_woo_cl_collection_delete', array( $this, 'woo_cl_collection_delete' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_collection_delete', array( $this, 'woo_cl_collection_delete' ) );
		
		//add action to call ajax for delete collection item
		add_action( 'wp_ajax_woo_cl_collection_item_delete', array( $this, 'woo_cl_collection_item_delete' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_collection_item_delete', array( $this, 'woo_cl_collection_item_delete' ) );

		//add action to call ajax for change collection status
		add_action( 'wp_ajax_woo_cl_collection_status_update', array( $this, 'woo_cl_collection_status_update' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_collection_status_update', array( $this, 'woo_cl_collection_status_update' ) );
		
		//add action to call ajax for change collection cover image
		add_action( 'wp_ajax_woo_cl_collection_coverimg_update', array( $this, 'woo_cl_collection_coverimg_update' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_collection_coverimg_update', array( $this, 'woo_cl_collection_coverimg_update' ) );
		
		//add action to call ajax for update collection details
		add_action( 'wp_ajax_woo_cl_collection_update', array( $this, 'woo_cl_collection_update' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_collection_update', array( $this, 'woo_cl_collection_update' ) );
		
		//add action to call ajax for show collection details popup
		add_action( 'wp_ajax_woo_cl_coll_item_details', array( $this, 'woo_cl_coll_item_details' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_coll_item_details', array( $this, 'woo_cl_coll_item_details' ) );
		
		//add action to call ajax for share via email
		add_action( 'wp_ajax_woo_cl_coll_item_email_share', array( $this, 'woo_cl_coll_item_email_share' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_coll_item_email_share', array( $this, 'woo_cl_coll_item_email_share' ) );
		
		//add action to call ajax for add to cart process
		add_action( 'wp_ajax_woo_cl_add_to_cart', array( $this, 'woo_cl_add_to_cart_process' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_add_to_cart', array( $this, 'woo_cl_add_to_cart_process' ) );
		
		//add action to call ajax for get collections data for pagination loop
		add_action( 'wp_ajax_woo_cl_get_loop_content', array( $this, 'woo_cl_get_loop_content' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_get_loop_content', array( $this, 'woo_cl_get_loop_content' ) );
		
		//add action to call ajax for get collection items data for pagination loop
		add_action( 'wp_ajax_woo_cl_item_get_loop_content', array( $this, 'woo_cl_item_get_loop_content' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_item_get_loop_content', array( $this, 'woo_cl_item_get_loop_content' ) );

		//add action to call ajax for add selected products in cart
		add_action( 'wp_ajax_woo_cl_add_bulk_products_to_cart', array( $this, 'woo_cl_add_bulk_products_to_cart' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_add_bulk_products_to_cart', array( $this, 'woo_cl_add_bulk_products_to_cart' ) );

		//Redirect on collection listing or single collection page
		add_action( 'template_redirect', array( $this, 'woo_cl_template_redirect' ) );
		
		//Add rewite tags
		add_action( 'init', array( $this, 'woo_cl_add_rewrite_tag' ) );
		
		//Add menu in admin bar
		add_action( 'admin_bar_menu', array( $this, 'woo_cl_action_admin_bar_menu' ) );
		
		//add action for add custom notifications
		add_filter( 'woocommerce_email_actions', array( $this, 'woo_cl_add_email_notification' ) );
		
		//message send when collection item has been added from follo my blog post plugin
		add_action( 'woo_cl_add_to_collection_success_popup', array( $this, 'wpw_fp_insert_collection_item' ), 10, 4 );
		
		//Display message on collection page
		add_filter( 'wpw_fp_follow_display_message_post_id', array( $this, 'woo_cl_follow_message_collection_id' ) );
		add_filter( 'wpw_fp_subscribed_post_url', array( $this, 'woo_cl_subscribed_collection_url' ), 10, 2 );
		
		// add filter to send email using woocommece template
		add_filter( 'wpw_fp_send_email_externaly', array( $this, 'woo_cl_send_email_externaly' ), 10, 5 );

		//Check if my collections on WooCommerce My account page enabled
		if( get_option( 'cl_enable_woo_myaccount_page' ) == 'yes' ) {

			// add action for collection to woocommerce myaccount page
			add_action( 'woocommerce_before_my_account', array( $this, 'woo_cl_woocommerce_account_page_collection' ) );
		}

		//add action for 
		add_action( 'woocommerce_single_product_summary', array( $this, 'woo_cl_add_col_btn_to_product_summary' ) );
		
		// To make compatible with previous versions of 3.0.0
	 	if ( version_compare( WOOCOMMERCE_VERSION, "3.0.0" ) == -1 ) {
	 		// woocommerce_stock_html is deprecated since version 2.6!
	 		add_action( 'woocommerce_stock_html', array( $this, 'woo_cl_add_to_collection_button_html_deprecated'), 10, 3 );
	 	} else {	 		
	 		add_action( 'woocommerce_get_stock_html', array( $this, 'woo_cl_add_to_collection_button_html'), 10, 2 );
	 	}

		//Check if my collections on Ultimate Member account page enabled
		if( get_option( 'cl_enable_um_account_page' ) == 'yes' ) {

			//Action to add My Collections on Ultimate Member page
		 	add_filter( 'um_account_page_default_tabs_hook', array( $this, 'woo_cl_um_mycollection_tab' ), 100 );
		 	add_action( 'um_account_tab__mycollection', array( $this, 'woo_cl_um_account_tab__mycollection' ) );
		 	add_filter( 'um_account_content_hook_mycollection', array( $this, 'woo_cl_um_account_content_hook_mycollection' ) );
	 	}

		//Check if my collections on BuddyPress profile page enabled
		if( get_option( 'cl_enable_buddypress_myaccount_page' ) == 'yes' ) {

			//Action to add My Collections on BuddyPress page
	 		add_action( 'bp_setup_nav', array( $this, 'woo_cl_buddypress_profile_tab_mycollections' ) );
	 	}

		//Check if my collections on WC Vendors dashboard page enabled
		if( get_option( 'cl_enable_wcvendor_dashboard_page' ) == 'yes' ) {

			//Action to add My Collections on WC Vendors page
	 		add_action( 'wcvendors_after_links', array( $this, 'woo_cl_wcvendors_dashboard_tab_mycollections' ) );
	 		add_filter( 'wcv_pro_dashboard_urls', array( $this, 'woo_cl_wcvendors_dashboard_pro_tab_mycollections' ) );
	 	}

	 	//Action to track recently viewed collections - Commented
		//add_action( 'wp', array( $this, 'woo_cl_track_collection_recent_viewed' ) );

		// Check if my collections on Dokan page enabled
		if( get_option( 'cl_enable_dokan_account_page' ) == 'yes' ) {

			// Add filter to add collections on Dokan dashboard
			add_filter( 'dokan_get_dashboard_nav', array( $this, 'woo_cl_dokan_dashboard_tab_mycollections' ));
		}

		// Check if my collections on Dokan page enabled
		if( get_option( 'cl_enable_wcmp_account_page' ) == 'yes' ) {

			// Add filter to add collections on WooCommerce Marketplace dashboard
			add_filter( 'wcmp_vendor_dashboard_nav', array( $this, 'woo_cl_wcmp_dashboard_tab_mycollections' ) );
		}
	}
}