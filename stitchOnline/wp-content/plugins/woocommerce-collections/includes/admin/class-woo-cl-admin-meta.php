<?php 
/**
 * Admin Meta Panel HTML Class
 *
 * To handles some small panel HTML content for backend
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class Woo_Cl_Admin_Meta { 

	public $model;

	function __construct() {

		global $woo_cl_model;

		$this->model = $woo_cl_model;
	}

	/**
	 * WooCommerce custom product tab
	 * 
	 * Adds a new tab to the Product Data postbox in the admin product interface
	 *
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_product_write_panel_tab() {
		 
		echo "<li class=\"woo_cl_collections_tab hide_if_grouped\"><a href=\"#woo_cl_collections\"><span>" . woo_cl_get_label_plural() . "</span></a></li>";
	}
	
	/**
	 * WooCommerce custom product tab data
	 * 
	 * Adds the panel to the Product Data postbox in the product interface
	 *
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_product_write_panel() {

		global $post;

		$prefix = WOO_CL_META_PREFIX;

		$icons	= array(
			'' 				=> __( 'Global Settings', 'woocl' ),
			'none' 			=> __( 'None', 'woocl' ),
			'fa fa-star' 	=> __( 'Star', 'woocl' ),
			'fa fa-gift' 	=> __( 'Present', 'woocl' ),
			'fa fa-plus' 	=> __( 'Add', 'woocl' ),
			'fa fa-bookmark'=> __( 'Bookmark', 'woocl' ),
			'fa fa-heart' 	=> __( 'Heart', 'woocl' )
		);

		// display the custom tab panel
		echo '<div id="woo_cl_collections" class="panel wc-metaboxes-wrapper woocommerce_options_panel tabs-content hide-all">';

			//Field for collection button disable
			woocommerce_wp_checkbox( array( 
										'id' 	=> $prefix .'hide_coll_btn',
										'label' => sprintf(__( 'Hide %s Button', 'woocl' ), woo_cl_get_label_singular()), 
										'cbvalue' 	=> 'disable',
										'desc_tip' 	=> true,
										'description' => sprintf(__( 'Check this box if you want to disable add to %s feature for this product.', 'woocl' ), woo_cl_get_label_singular( true )),
										'value' => esc_attr( $post->_woo_cl_hide_coll_btn ) 
									) );

			//Field for add to collection button text
			woocommerce_wp_text_input( array(
				'id'          => $prefix .'add_to_collection_btn_text',
				'label'       => __( 'Button / Link Text', 'woocl' ),
				'description' => '<br />'. sprintf( __( 'Here you can override the Add to %s button text for this product. Available template tag is :','woocl'), woo_cl_get_label_singular() ) .' <code>{no_of_collections}</code> - '.sprintf( __( 'display the no of %s', 'woocl' ), woo_cl_get_label_plural() ),
				'data_type'   => 'text',
				'desc_tip'    => false,
			) );

			//Field for add to collection button type
			woocommerce_wp_select( array(
				'id'      => $prefix .'add_to_collection_btn_type',
				'label'   => __( 'Button / Link Type', 'woocl' ),
				'options' => array(
					''			=> __( 'Global Settings', 'woocl' ),
					'default'	=> __( 'Default', 'woocl' ),
					'button' 	=> __( 'Button', 'woocl' ),
					'link' 		=> __( 'Link', 'woocl' )
				),
				'description' => sprintf( __('Here you can override a add to %s button type for this product.','woocl'), woo_cl_get_label_singular(true) ),
				'desc_tip'    => true,
			) );

			//Field for add to collection button Icon
			woocommerce_wp_select( array(
				'id'      => $prefix .'add_to_collection_link_icon',
				'label'   => __( 'Icon', 'woocl' ),
				'options' => $icons,
				'description' => '<br />'. sprintf( __( 'Override the icon to show next to the add to %s links for this product.','woocl'), woo_cl_get_label_singular(true) ),
				'desc_tip'    => true,
			) );

		echo '</div>';
	}
}