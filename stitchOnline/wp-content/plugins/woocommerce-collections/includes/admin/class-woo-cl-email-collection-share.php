<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Email Class for Collection Share
 * 
 * Handles to the email notification template.
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
class Woo_Cl_Email_Collection_Share extends WC_Email {

	public $model;

	/**
	 * Constructor
	 *
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function __construct() {
		
		global $woo_cl_model;
		
		$this->model	= $woo_cl_model;
		
		$this->id          = 'woo_cl_collection_share_notification';
		$this->title       = sprintf( __( '%s Share', 'woocl' ), woo_cl_get_label_singular() );
		$this->description = sprintf( __( '%s Share Notification Email Template.', 'woocl' ), woo_cl_get_label_singular() );
		
		$this->heading     = sprintf( __( '%s Share Notification', 'woocl' ), woo_cl_get_label_singular() );
		$this->subject     = sprintf( __( '%s has suggested you look at this %s from %s', 'woocl' ), '{sender_name}', woo_cl_get_label_singular(), '{site_name}' );
		
		$this->template_html  = 'emails/collection-share.php';
		$this->template_plain = 'emails/plain/collection-share.php';
		
		$this->template_base  = WOO_CL_DIR . '/includes/templates/';
		
		$this->share_data  	= array();
		
		// Triggers for this email via our do_action
		add_action( 'woo_cl_share_collection_email_notification', array( $this, 'trigger' ), 20, 1 );
		
		parent::__construct();
	}

	/**
	 * Insert Collection Item Notification
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function trigger( $share_data ) {
		
		// replace variables in the subject/headings
	    $this->find[] 		= '{sender_name}';
	    $this->replace[] 	= $share_data['sender_name'];
	    $this->find[] 		= '{site_name}';
	    $this->replace[] 	= $share_data['site_name'];
		
	    //Asign required object for feature use
	    $this->object		= $share_data;

	    if( isset( $share_data['sender_name'] ) && !empty( $share_data['sender_name'] ) ) {//check if sender name not empty
	    	add_filter( 'wp_mail_from_name', array( $this, 'get_from_name' ) );
	    }
	    if( isset( $share_data['sender_email'] ) && !empty( $share_data['sender_email'] ) ) {//check if sender email not empty
	    	add_filter( 'wp_mail_from', array( $this, 'get_from_address' ) );
	    }

		$this->send( $share_data['friend_emails'], $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
	}

	/**
     * Get from name.
     *
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
     */
    public function get_from_name() {

         return wp_specialchars_decode( esc_html( $this->object['sender_name'] ), ENT_QUOTES );
	}

	/**
     * Get from email address.
     *
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
     */
    public function get_from_address() {

         return sanitize_email( $this->object['sender_email'] );
	}

	/**
	 * Gets the email subject
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function get_subject() {
		
		return apply_filters( 'woocommerce_email_subject_' . $this->id, $this->format_string( $this->subject ), $this->object );
	}
	
	/**
	 * Gets the email heading
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function get_heading() {
		
		return apply_filters( 'woocommerce_email_heading_' . $this->id, $this->format_string( $this->heading ), $this->object );
	}
	
	/**
	 * Gets the email HTML content
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function get_content_html() {
		ob_start();
		wc_get_template(
			$this->template_html,
			array(
				'email_heading' => $this->get_heading(),
				'sender_name'	=> $this->object['sender_name'],
				'sender_email'	=> $this->object['sender_email'],
				'message'		=> $this->object['message'],
				'sharelink'		=> $this->object['sharelink'],
				'site_url'		=> $this->object['site_url'],
				'site_name'		=> $this->object['site_name'],
			),
			'',
			$this->template_base
		);
		return ob_get_clean();
	}
	
	/**
	 * Gets the email plain content
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function get_content_plain() {
		ob_start();
		wc_get_template(
			$this->template_plain,
			array(
				'email_heading' => $this->get_heading(),
				'sender_name'	=> $this->object['sender_name'],
				'sender_email'	=> $this->object['sender_email'],
				'message'		=> $this->object['message'],
				'sharelink'		=> $this->object['sharelink'],
				'site_url'		=> $this->object['site_url'],
				'site_name'		=> $this->object['site_name'],
			),
			'',
			$this->template_base
		);
		return ob_get_clean();
	}
	
	/**
	 * Initialize Settings Form Fields
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function init_form_fields() {
		
		$this->form_fields = array(
			'enabled' => array(
				'title'   => __( 'Enable/Disable', 'woocl' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable this email notification', 'woocl' ),
				'default' => 'yes',
			),
			'subject' => array(
				'title'       => __( 'Subject', 'woocl' ),
				'type'        => 'text',
				'description' => '<p class="description">'.
									sprintf( __( 'This is the subject of the email that will be sent to the shared recipient of the selected %1$s when %1$s share vie email. Available template tags for subject fields are :','woocl' ), woo_cl_get_label_singular() ).
									'<br /><code>{sender_name}</code> - '.__( 'displays the name of the sender', 'woocl' ).
									'<br /><code>{site_name}</code> - '.__( 'displays the name of your site', 'woocl' ).'</p>',
				'placeholder' => '',
				'default'     => '',
			),
			'heading' => array(
				'title'       => __( 'Email Heading', 'woocl' ),
				'type'        => 'text',
				'description' => __( 'This controls the main heading contained within the email notification. Leave blank to use the default heading:', 'woocl' ) . '<code> '. $this->heading . '</code>.',
				'placeholder' => '',
				'default'     => '',
			),
			'email_type' => array(
				'title'       => __( 'Email type', 'woocl' ),
				'type'        => 'select',
				'description' => __( 'Choose which format of email to send.', 'woocl' ),
				'default'     => 'html',
				'class'       => 'email_type',
				'options' => array(
					'plain'     => __( 'Plain text', 'woocl' ),
					'html'      => __( 'HTML', 'woocl' ),
				),
			),
		);
	}
}