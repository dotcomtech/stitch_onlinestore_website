<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Woo_Cl_Settings_Tabs' ) ) :

/**
 * Setting page Class
 * 
 * Handles Settings page functionality of plugin
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.1.5
 */
class Woo_Cl_Settings_Tabs extends WC_Settings_Page {

	/**
	 * Constructor
	 * 
	 * Handles to add hooks for adding settings
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function __construct() {

		global $woo_cl_model; // Declare global variables

		$this->id    	= 'woo-cl-settings'; // Get id
		$this->label 	= woo_cl_get_label_plural();// Get tab label
		$this->model 	= $woo_cl_model; // Declare variable $this->model

		// Add filter for adding tab
		add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_page' ), 20 );

		// Add action to show output
		add_action( 'woocommerce_settings_' . $this->id, array( $this, 'woo_cl_output' ) );

		// Add action for saving data
		add_action( 'woocommerce_settings_save_' . $this->id, array( $this, 'woo_cl_save' ) );

		// Add action for adding sections
		add_action( 'woocommerce_sections_' . $this->id, array( $this, 'output_sections' ) );

		// Add a custom field types
		add_action( 'woocommerce_admin_field_woo_cl_textarea', array( $this, 'woo_cl_admin_field_woo_cl_textarea' ) );
		add_action( 'woocommerce_admin_field_woo_cl_multiselect', array( $this, 'woo_cl_admin_field_woo_cl_multiselect' ) );
		add_action( 'woocommerce_admin_field_woo_cl_button_preview', array( $this, 'woo_cl_admin_field_woo_cl_button_preview' ) );
	}

	/**
	 * Add woo_cl_textarea type Setting
	 * 
	 * Handles to add woo_cl_textarea field
	 * type for admin side
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
	public function woo_cl_admin_field_woo_cl_textarea( $field ) {

		global $woocommerce;

		if ( isset( $field['title'] ) && isset( $field['id'] ) ) :

			$file_val		= get_option( $field['id'] );
			$file_val		= !empty($file_val) ? $file_val : '';
			$editor			= ( isset( $field['editor'] ) && $field['editor'] == true ) ? true : false;
			$editor_cofig	= array(
									'media_buttons'	=> true,
									'textarea_rows'	=> 5,
									'editor_class'	=> 'woo-cl-wpeditor'
								); ?>
				
				<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo wp_kses_post( $field['title'] ); ?></label>
					</th>
					<td class="forminp forminp-text">
						<fieldset><?php 
							if( $editor ) {
								
								wp_editor( $file_val, esc_attr( $field['id'] ), $editor_cofig );
								
							} else { ?>
								
								<textarea name="<?php echo esc_attr( $field['id']  ); ?>" id="<?php echo esc_attr( $field['id'] ); ?>" style="<?php echo esc_attr( $field['css'] ); ?>;"/><?php echo esc_attr( $file_val ); ?></textarea><?php 
							} ?>
						</fieldset>
						<span class="description"><?php echo $field['desc'];?></span>
					</td>
				</tr><?php
		endif;
	}

	/**
	 * Add woo_cl_multiselect type Setting
	 * 
	 * Handles to add woo_cl_multiselect field
	 * type for admin side
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
	public function woo_cl_admin_field_woo_cl_multiselect ( $field ) {

		if ( isset( $field['title'] ) && isset( $field['id'] ) && ! empty( $field['val'] ) ) {

			$available_fields = $field['val'];
			
			wp_enqueue_script('jquery');
			
			$file_val		= get_option( $field['id'] );
			
			echo '<tr valign="top">';
			echo '<th scope="row" class="titledesc">';
			echo '<label for="' . esc_attr( $field['id'] ) . '">' . wp_kses_post( $field['title'] ) . '</label>';
			echo '</th><td class="forminp forminp-multiselect woo-cl-disable-add-collection-btn"><fieldset>';
			echo '<select multiple name="' . esc_attr( $field['id']  ) . '[]" id="' . esc_attr( $field['id']  ) . '" class="'. esc_attr( $field['class']  ) . '">';
			foreach ( $available_fields as $available_field_key => $available_field_val ) {
				$option = '';
				$option .= '<option value="' . $available_field_key . '"';
				if ( !empty( $file_val ) && in_array( $available_field_key, $file_val ) ) {
					$option .= ' selected = "selected"';
				}
				$option .= ' >' . $available_field_val . '</option>';
				echo $option;
			}
			echo '</select></label></fieldset>';
			echo '<span class="description">' . $field['desc'] . '</span></td>';
		}
	}

	/**
	 * Add woo_cl_button_preview type to Display add to collection button preview
	 * 
	 * Handles to add button_preview field
	 * type for admin side
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
	public function woo_cl_admin_field_woo_cl_button_preview( $field ) {

		if ( isset( $field['title'] ) && isset( $field['id'] ) ) : ?>
			<tr valign="top">
				<th scope="row" class="titledesc">
					<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo wp_kses_post( $field['title'] ); ?></label>
				</th>
				<td id="<?php echo esc_attr( $field['id'] ); ?>" class="forminp forminp-text">
					<?php
					// Get button class based on option
					$button_type = get_option( 'cl_add_to_collection_btn_type' );
					if( $button_type == 'default' ) {
						$button_class = 'alt button';
					} else if( $button_type == 'button' ) {
						$button_class = 'woocl-button';
					}

					//create add to collection button
					do_action( 'woo_cl_add_to_collection_button', array(
						'variation_class'		=> '',
						'button_class'			=> !empty( $button_class ) ? $button_class : '',
						'add_to_coll_btn'		=> str_replace( '{no_of_collections}', 0, get_option( 'cl_add_to_collection_btn_text' ) ),
						'add_to_coll_btnicon'	=> get_option( 'cl_add_to_collection_link_icon' ),
						'icon_position'			=> 'left',
					));?>
				</td>
			</tr><?php
		endif;
	}

	/**
	 * Handles to add sections for Settings tab
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function get_sections() {

		// Create array
		$sections = array(
			''          				=> __( 'General Settings', 'woocl' ),
			'cl_display_settings'  		=> __( 'Display Settings', 'woocl' ),
			'cl_misc_settings'  		=> __( 'Misc Settings', 'woocl' ),
		);

		//Check if follow my blog post
		if( woo_cl_is_follow_activate() ) {

			// Add follow email template options
			$sections['cl_follow_email_options'] = __( 'Follow Settings', 'woocl' );
		}

		return apply_filters( 'woo_cl_setting_sections', $sections );
	}

	/**
	 * Handles to output data
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function woo_cl_output() {

		// Get global variable
		global $current_section;

		// Get settings for current section
		$settings = $this->get_settings( $current_section );

		WC_Admin_Settings::output_fields( $settings );
	}

	/**
	 * Handles to save data
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function woo_cl_save() {

		global $current_section;
		
		if( empty( $_POST['cl_add_to_collection_btn_text'] ) ) {//default value set to add to collection button
			$_POST['cl_add_to_collection_btn_text'] = __( 'Add to Collections', 'woocl' );
		}
		
		if( isset( $_POST['cl_pagination_options'] ) ) {//set integer value in per page field
			
			//typecast per page to integer value
			$cl_pagination_options	= intval( $_POST['cl_pagination_options'] );
			
			if( empty( $cl_pagination_options ) ) {//if per page is empty then set default per page
				$_POST['cl_pagination_options'] = WOO_CL_POST_PER_PAGE;
			}
		}
		
		if( isset( $_POST['cl_no_of_col_items_list'] ) ) {//set integer value in column per page field
			
			//typecast per page to integer value
			$cl_pagination_options	= intval( $_POST['cl_no_of_col_items_list'] );
			
			if( empty( $cl_pagination_options ) ) {//if per page is empty then set default per page
				$_POST['cl_no_of_col_items_list'] = WOO_CL_NUM_COL_COLL_LIST;
			}
		}

		if( isset( $_POST['cl_no_of_my_collection_items'] ) ) {//set integer value in no of my collections field
			
			//typecast per page to integer value
			$cl_no_of_my_collections_items = intval( $_POST['cl_no_of_my_collection_items'] );
			
			if( empty( $cl_no_of_my_collections_items ) ) {//if per page is empty then set default per page
				$_POST['cl_no_of_my_collection_items'] = WOO_CL_NO_OF_MY_COLLECTION_ITEMS;
			}
		}

		if( isset( $_POST['cl_max_collections_per_user'] ) ) {//set integer value in max collection per user field

			//typecast per page to integer value
			$cl_max_collections_per_user = intval( $_POST['cl_max_collections_per_user'] );

			if( empty( $cl_max_collections_per_user ) ) {//if per page is empty then set default max collections
				$_POST['cl_max_collections_per_user'] = '';
			}
		}

		if( isset( $_POST['cl_max_products_per_collection'] ) ) {//set integer value in max products per collection field

			//typecast per page to integer value
			$cl_max_products_per_collection = intval( $_POST['cl_max_products_per_collection'] );

			if( empty( $cl_max_products_per_collection ) ) {//if per page is empty then set default max products
				$_POST['cl_max_products_per_collection'] = '';
			}
		}

		if( isset( $_POST['cl_view_collection_text'] ) ) {//set default to view collections endpoints
			
			$cl_view_options	= trim( $_POST['cl_view_collection_text'] );
			
			if( empty( $cl_view_options ) ) {
				$_POST['cl_view_collection_text'] = WOO_CL_DEFAULT_VIEW;
			}
		}
		
		if( isset( $_POST['cl_edit_collection_text'] ) ) {//set default to edit collections endpoints
			
			$cl_edit_options	= trim( $_POST['cl_edit_collection_text'] );
			
			if( empty( $cl_edit_options ) ) {
				$_POST['cl_edit_collection_text'] = WOO_CL_DEFAULT_EDIT;
			}
		}
		
		if( isset( $_POST['cl_collection_author_text'] ) ) {//set default to author collections endpoints
			
			$cl_author_options	= trim( $_POST['cl_collection_author_text'] );
			
			if( empty( $cl_author_options ) ) {
				$_POST['cl_collection_author_text'] = WOO_CL_DEFAULT_AUTHOR;
			}
		}
		
		if ( !isset( $_POST['cl_disable_user_role'] ) ) {
			$_POST['cl_disable_user_role'] = '';
		}

		$settings = $this->get_settings( $current_section );				
		WC_Admin_Settings::save_fields( $settings );
	}

	/**
	 * Handles to get setting
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.1.5
	 */
	public function get_settings( $current_section = '' ) {
		
		//Get Pagination options
		$cl_pagination_options	= get_option('cl_pagination_options');
		$cl_pagination_options	= ( trim( $cl_pagination_options ) != '' && is_numeric( $cl_pagination_options ) ) ? $cl_pagination_options : WOO_CL_POST_PER_PAGE;

		$icons	= array(
			'' 				=> __( 'None', 'woocl' ),
			'fa fa-star' 	=> __( 'Star', 'woocl' ),
			'fa fa-gift' 	=> __( 'Present', 'woocl' ),
			'fa fa-plus' 	=> __( 'Add', 'woocl' ),
			'fa fa-bookmark'=> __( 'Bookmark', 'woocl' ),
			'fa fa-heart' 	=> __( 'Heart', 'woocl' )
		);
		
		// Declare Sortable options array
		$sort_optns = array(
			'menu_order title-asc'	=> __( 'Custom Ordering + Title ( ASC )', 'woocl' ),
			'menu_order title-desc'	=> __( 'Custom Ordering + Title ( DESC )', 'woocl' ),
			'date-asc'		=> __( 'Date ( ASC )', 'woocl' ),
			'date-desc'		=> __( 'Date ( DESC )', 'woocl' ),
			'title-asc'		=> __( 'Title ( ASC )', 'woocl' ),
			'title-desc'	=> __( 'Title ( DESC )', 'woocl' ),
		);
		
		//Get columns details
		$columns 		= array();
		$max_columns	= woo_cl_get_max_listing_columns();
		if( $max_columns > 2 ) {
			for( $itr = 2; $itr <= $max_columns; $itr++ ) {
				$columns[$itr]	= sprintf( __( '%s columns', 'woocl' ), $itr );
			}
		}

		// Load user.php before use get_editable_roles function
		if ( ! function_exists( 'get_editable_roles' ) ) {
		    require_once ABSPATH . 'wp-admin/includes/user.php';
		}

		// Get User roles
		$user_roles		= get_editable_roles();

		$user_editable_role = array();
		foreach( $user_roles as $user_role_key => $user_role_val ) {
			$user_editable_role[$user_role_key] = $user_role_val['name'];
		}
		
		// If collection settings are selected
		if ( 'cl_display_settings' == $current_section ) {

			$settings = array(
			
				array( 
					'name'	=>	sprintf( __( 'Display Settings', 'woocl' ), woo_cl_get_label_singular() ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'cl_display_settings'
				),
				array(
					'id'		=> 'cl_pagination_options',
					'name'		=> __( 'Items Per Page', 'woocl' ),
					'desc'		=> sprintf( __( '<p class="description">Enter number of %s / %s items you want to display per page.</p>', 'woocl' ), woo_cl_get_label_plural(), woo_cl_get_label_singular() ),
					'type'		=> 'text',
					'class'		=> 'small-text',
					'desc_tip'	=> ''
				),
				array(
					'id'		=> 'cl_no_of_col_items_list',
					'name'		=> __( 'No. Of Column for Item List', 'woocl' ),
					//'desc'		=> '<br />'. sprintf( __( 'Enter number of %s / %s column you want to display per page.', 'woocl' ), woo_cl_get_label_plural(), woo_cl_get_label_singular() ),
					'desc'		=> sprintf( __( '<p class="description">Here you can select number of columns for %s listing/%s items page. Default is : 3.</p>', 'woocl' ), woo_cl_get_label_plural(), woo_cl_get_label_singular() ),
					'type'		=> 'select',
					'class'		=> 'small-text',
					'desc_tip'	=> '',
					'default' 	=> '3',
					'options' 	=> $columns,
                    'css'       => 'padding: 0px 6px;'
				),
				array(
					'id'		=> 'cl_no_of_my_collection_items',
					'name'		=> sprintf( __( 'No. Of My %s Items', 'woocl' ), woo_cl_get_label_plural() ),
					'desc'		=> sprintf( __( '<p class="description">Enter number of %s you want to display on My Account page in "My %s" section.</p>', 'woocl' ), woo_cl_get_label_plural(true), woo_cl_get_label_plural(true) ),
					'type'		=> 'text',
					'class'		=> 'small-text',
					'desc_tip'	=> ''
				),
				array(
					'id'		=> 'cl_collection_subtitle_text',
					'name'		=> sprintf( __( '%s Sub Title Text', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> sprintf( __( '<p class="description">Enter the %1$s sub title which you want to display on view %1$s page under the %1$s title. Available template tag are :', 'woocl' ), woo_cl_get_label_singular(true) )
						.'<br /><code>{total_price}</code> - '.sprintf( __( 'display the total price', 'woocl' ) )
						.'<br /><code>{total_items}</code> - '.sprintf( __( 'display the no of items</p>', 'woocl' ) ),
					'type'		=> 'text',
					'class'		=> 'small-text',
					'desc_tip'	=> ''
				),
				
				array(
					'id'		=> 'cl_enable_editor_coll_desc',
					'name'		=> sprintf( __( 'Enable Editor On %s', 'woocl' ), woo_cl_get_label_plural() ),
					'desc'		=> '',
					'type'		=> 'checkbox',
					'desc_tip'	=> sprintf( __( 'Check this box if you want to enable tinyMCE editor for description field on edit %s page.','woocl' ), woo_cl_get_label_singular() )
				),
				array(
					'id'		=> 'cl_enable_front_drag_item_ordering',
					'name'		=> __( 'Enable Item Drag & Drop Ordering', 'woocl' ),
					'desc'		=> '',
					'type'		=> 'checkbox',
					'desc_tip'	=> sprintf( __( 'Check this box if you want to enable %1$s item(s) drag & drop ordering on edit %1$s page.','woocl' ), woo_cl_get_label_singular(true) )
				),
				array(
	                'name' 			=> sprintf( __( 'Display My %s On', 'woocl' ), woo_cl_get_label_plural() ),
	                'desc' 			=> sprintf( __( 'Check this box to display My %s on WooCommerce My Account page.', 'woocl' ), woo_cl_get_label_plural() ),
					'id' 			=> 'cl_enable_woo_myaccount_page',
	                'std' 			=> 'yes',
	                'default' 		=> 'yes',
	                'type' 			=> 'checkbox',
			            'checkboxgroup' => 'start'
		        ),
		    );
	       
	        //Check if Ultimate Member plugin active
	        if( class_exists( 'UM_API' ) ) {

	            $um_settings = array(
					array(
		                'desc' 			=> sprintf( __( 'Check this box to display My %s on Ultimate Member Account page.', 'woocl' ), woo_cl_get_label_plural() ),
						'id' 			=> 'cl_enable_um_account_page',
		                'std' 			=> '',
		                'default' 		=> '',
		                'type' 			=> 'checkbox',
				        'checkboxgroup' => ''
			        ),
	            );

	            $settings = array_merge( $settings, $um_settings );
	        }

			//Check if BuddyPress plugin active
	        if( class_exists( 'BuddyPress' ) ) {

	            $bp_settings = array(
					array(
		                'desc' 			=> sprintf( __( 'Check this box to display My %s on BuddyPress Profile page.', 'woocl' ), woo_cl_get_label_plural() ),
						'id' 			=> 'cl_enable_buddypress_myaccount_page',
		                'std' 			=> '',
		                'default' 		=> '',
		                'type' 			=> 'checkbox',
				        'checkboxgroup' => ''
			        ),
	            );

	            $settings = array_merge( $settings, $bp_settings );
	        }

			//Check if WC Vendors plugin active
	        if( class_exists( 'WC_Vendors' ) ) {

	            $wcv_settings = array(
					array(
		                'desc' 			=> sprintf( __( 'Check this box to display My %s on WC Vendors Dashboard page.', 'woocl' ), woo_cl_get_label_plural() ),
						'id' 			=> 'cl_enable_wcvendor_dashboard_page',
		                'std' 			=> '',
		                'default' 		=> '',
		                'type' 			=> 'checkbox',
				        'checkboxgroup' => ''
			        ),
	            );

	            $settings = array_merge( $settings, $wcv_settings );
	        }

	        // Check if Dokan Plugin Active
	        if( class_exists( 'WeDevs_Dokan' ) ) {

	            $dokan_settings = array(
					array(
		                'desc' 			=> sprintf( __( 'Check this box to display My %s on Dokan\'s vendor dashboard page.', 'woocl' ), woo_cl_get_label_plural() ),
						'id' 			=> 'cl_enable_dokan_account_page',
		                'std' 			=> '',
		                'default' 		=> '',
		                'type' 			=> 'checkbox',
				        'checkboxgroup' => ''
			        ),
	            );

	            $settings = array_merge( $settings, $dokan_settings );
	        }

	        // Check if WC Market place Plugin Active
	        if(class_exists('WCMp')){

	            $dokan_settings = array(
					array(
		                'desc' 			=> sprintf( __( 'Check this box to display My %s on WooCommerce Marketplace\'s vendor dashboard page.', 'woocl' ), woo_cl_get_label_plural() ),
						'id' 			=> 'cl_enable_wcmp_account_page',
		                'std' 			=> '',
		                'default' 		=> '',
		                'type' 			=> 'checkbox',
				        'checkboxgroup' => ''
			        ),
	            );

	            $settings = array_merge( $settings, $dokan_settings );
	        }

			$settings_2 = array(
				array(
					'type' 	=> 'sectionend',
					'id' 	=> 'cl_display_settings'
				),
				array( 
					'name'	=>	sprintf( __( 'Searching & Sorting Settings', 'woocl' ), woo_cl_get_label_singular() ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'cl_search_and_sort_settings'
				),
				array(
					'id'		=> 'cl_enable_search_on_collections',
					'name'		=> sprintf( __( 'Enable %s Search', 'woocl' ), woo_cl_get_label_plural() ),
					'desc'		=> '',
					'type'		=> 'checkbox',
					'desc_tip'	=> sprintf( __( 'Check this box if you want to allow users to search %s on collections listing page.','woocl' ), woo_cl_get_label_singular() )
				),
				array(
					'id'		=> 'cl_enable_sorting_on_collections',
					'name'		=> sprintf( __( 'Enable %s Sorting', 'woocl' ), woo_cl_get_label_plural() ),
					'desc'		=> '',
					'type'		=> 'checkbox',
					'desc_tip'	=> sprintf( __( 'Check this box if you want to enable the sorting options on %s listing page.','woocl' ), woo_cl_get_label_plural() )
				),
				array(
					'id'		=> 'cl_sort_collection_by',
					'name'		=> sprintf( __( 'Default %s Sorting', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> sprintf( __( '<p class="description">This controls the default sort order of the %s.</p>','woocl'), woo_cl_get_label_plural(true) ),
					'type'		=> 'select',
					'class'		=> '',
				    'std' 		=> 'date-asc',
					'default' 	=> 'date-asc',
					'options' 	=> $sort_optns									
				),
				array(
					'id'		=> 'cl_enable_search_on_collection_items',
					'name'		=> sprintf( __( 'Enable %s item(s) Search', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> '',
					'type'		=> 'checkbox',
					'desc_tip'	=> sprintf( __( 'Check this box if you want to allow users to search Collection items on %s item(s) listing page.','woocl' ), woo_cl_get_label_singular() )
				),
				array(
					'id'		=> 'cl_enable_sorting_on_collection_items',
					'name'		=> sprintf( __( 'Enable %s item(s) Sorting', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> '',
					'type'		=> 'checkbox',
					'desc_tip'	=> sprintf( __( 'Check this box if you want to enable sorting options on %s item(s) listing page.','woocl' ), woo_cl_get_label_singular() )
				),
				array(
					'id'		=> 'cl_sort_collection_item_by',
					'name'		=> sprintf( __( 'Default %s Items Sorting', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> sprintf( __( '<p class="description">This controls the default sort order of the %s items.</p>','woocl'), woo_cl_get_label_singular(true) ),
					'type'		=> 'select',
					'class'		=> '',
				    'std' 		=> 'date-asc',
					'default' 	=> 'date-asc',
					'options' 	=> $sort_optns
				),
				array(
					'type' 	=> 'sectionend',
					'id' 	=> 'cl_search_and_sort_settings'
				),
				array( 
					'name'	=>	sprintf( __( 'Button Settings', 'woocl' ), woo_cl_get_label_singular() ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'cl_collection_button_settings'
				),
				
				array(
					'id'		=> 'cl_add_to_collection_btn_text',
					'name'		=> __( 'Button / Link Text', 'woocl' ),
					'desc'		=> sprintf( __('<p class="description">Here you can give the Add to %s button custom text. Default is: Add to Collections. Available template tag is :','woocl'), woo_cl_get_label_singular() ) .'<br /><code>{no_of_collections}</code> - '.sprintf( __( 'display the no of %s</p>', 'woocl' ), woo_cl_get_label_plural() ),
					'type'		=> 'text',
					'class'		=> '',
				    'std' 		=> __( 'Add to Collections', 'woocl' ),
					'default' 	=> __( 'Add to Collections', 'woocl' ),
				),
				array(
					'id'		=> 'cl_add_to_collection_btn_type',
					'name'		=> __( 'Button / Link Type', 'woocl' ),
					'desc'		=> sprintf( __('<p class="description">Here you can select a add to %s button type.</p>','woocl'), woo_cl_get_label_singular(true) ),
					'type'		=> 'select',
					'class'		=> '',
				    'std' 		=> 'button',
					'default' 	=> 'button',
					'options' 	=> array(  'default' => __( 'Default', 'woocl' ),  'button' => __( 'Button', 'woocl' ), 'link' => __( 'Link', 'woocl' ) )	
				),
				array(
					'id'		=> 'cl_add_to_collection_link_icon',
					'name'		=> __( 'Icon', 'woocl' ),
					'desc'		=> sprintf( __( '<p class="description">The icon to show next to the add to %s links.</p>','woocl'), woo_cl_get_label_singular(true) ),
					'type'		=> 'select',
					'class'		=> '',
				    'std' 		=> 'fa fa-gift',
					'default' 	=> 'fa fa-gift',
					'options' 	=> $icons									
				),
				array(
					'id'		=> 'cl_add_to_collection_btn_preview',
					'name'		=> __( 'Button Preview', 'woocl' ),
					'type'		=> 'woo_cl_button_preview',
					'class'		=> '',
				),
				array(
					'id'		=> 'cl_display_product_collections_text',
					'name'		=> sprintf( __( 'Show product in %s', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> sprintf( __( '<p class="description">Enter the text which you want to display before add to %s button. Available template tag is :', 'woocl' ), woo_cl_get_label_plural(true) )
						.'<br /><code>{collections}</code> - '.sprintf( __( 'displays the %s(s) title with comma separated. </p>', 'woocl' ) ,  woo_cl_get_label_singular(true) ),
					'type'		=> 'text',
					'class'		=> '',
					'desc_tip'	=> ''
				),
				array(
					'type' 	=> 'sectionend',
					'id' 	=> 'cl_collection_button_settings'
				),
				
				array( 
					'name'	=>	__( 'Style Settings', 'woocl' ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'cl_style_settings'
				),
				array(
	                'name' 		=> __('Custom CSS', 'woocl'),
	                'class' 	=> '',
	                'css' 		=> 'width:100%;min-height:100px',
	                'desc' 		=> __('<p class="description">Here you can enter your custom css for the woocommerce collections. The css will automatically added to the header, when you save it.</p>', 'woocl'),
	                'id' 		=> 'cl_custom_css',
	                'type' 		=> 'woo_cl_textarea',
	                'default'	=> ''
	            ),
				
				// End Collection Option section
				array( 
					'type' 		=> 'sectionend',
					'id' 		=> 'cl_style_settings'
				),
				
			);

			//Merge all settings
	        $settings = apply_filters( 'woo_cl_display_settings', array_merge( $settings, $settings_2 ) );

		} elseif ( 'cl_misc_settings' == $current_section ) { // If misc settings is selected

			//Get Terminologies
			$terminologies = array(
				'' 				=> __( 'Collection', 'woocl' ),
				'lightbox' 		=> __( 'Light Box', 'woocl' ),
				'watchlist' 	=> __( 'Watch List', 'woocl' ),
				'wishlist' 		=> __( 'Wish List', 'woocl' ),
				'wantlist' 		=> __( 'Want List', 'woocl' ),
				'custom' 		=> __( 'Custom Text', 'woocl' )
			);

			$settings = apply_filters( 'cl_misc_settings', array(

				// Start Misc Settings section
				array( 
					'name'	=>	__( 'Misc Settings', 'woocl' ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'cl_misc_settings'
				),
				array(
					'id'		=> 'cl_delete_options',
					'name'		=> __( 'Delete Options', 'woocl' ),
					'desc'		=> '',
					'type'		=> 'checkbox',
					'desc_tip'	=> sprintf( __( 'If you don\'t want to use the %s Plugin on your site anymore, you can check that box. This makes sure, that all the settings and tables are being deleted from the database when you deactivate the plugin.','woocl' ), woo_cl_get_label_plural() )
				),
				array(
					'id'		=> 'cl_scroll_options',
					'name'		=> __( 'Disable Infinite Scroll', 'woocl' ),
					'desc'		=> '',
					'type'		=> 'checkbox',
					'desc_tip'	=> sprintf( __( 'Check this box if you want to display "Load More" button after %s items per page.','woocl' ), $cl_pagination_options )
				),
				array( 
					'type' 		=> 'sectionend',
					'id' 		=> 'cl_misc_settings'
				),
				array( 
					'name'	=>	sprintf( __( '%s Terminology & Endpoints', 'woocl' ), woo_cl_get_label_plural() ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'cl_endpoints_settings'
				),
				array(
					'id'		=> 'cl_collection_terminology',
					'name'		=> sprintf( __( '%s Terminology', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> sprintf( __('<p class="description">Here you can select the terminology which you want to show in place of text %s.</p>','woocl'), woo_cl_get_label_singular(true) ),
					'type'		=> 'select',
					'class'		=> '',
				    'std' 		=> '',
					'default' 	=> '',
					'options' 	=> $terminologies
				),
				array(
					'id'		=> 'cl_collection_terminology_singular_text',
					'name'		=> sprintf( __( 'Singular Text', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> sprintf( __('<p class="description">Enter singular text for custom collection terminology.</p>','woocl'), woo_cl_get_label_singular(true) ),
					'type'		=> 'text',
					'class'		=> 'woocl-hidden-field',
				    'std' 		=> '',
					'default' 	=> '',
				),
				array(
					'id'		=> 'cl_collection_terminology_plural_text',
					'name'		=> sprintf( __( 'Plural Text', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> sprintf( __('<p class="description">Enter plural text for custom collection terminology.</p>','woocl'), woo_cl_get_label_singular(true) ),
					'type'		=> 'text',
					'class'		=> 'woocl-hidden-field',
				    'std' 		=> '',
					'default' 	=> '',
				),
				array(
					'id'		=> 'cl_view_collection_text',
					'name'		=> sprintf( __( 'View %s', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> sprintf( __('<p class="description">Endpoint for the %s -> View page, Default is: view-collection.</p>','woocl'), woo_cl_get_label_singular() ),
					'type'		=> 'text',
					'class'		=> '',
				    'std' 		=> 'view-collection',
					'default' 	=> 'view-collection',
				),
				array(
					'id'		=> 'cl_edit_collection_text',
					'name'		=> sprintf( __( 'Edit %s', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> sprintf( __('<p class="description">Endpoint for the %s -> Edit page, Default is: edit-collection.</p>','woocl'), woo_cl_get_label_singular() ),
					'type'		=> 'text',
					'class'		=> '',
				    'std' 		=> 'edit-collection',
					'default' 	=> 'edit-collection',
				),
				array(
					'id'		=> 'cl_collection_author_text',
					'name'		=> sprintf( __( '%s Author', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> sprintf( __('<p class="description">Endpoint for the %s -> Author page, Default is: collection-author.</p>','woocl'), woo_cl_get_label_singular() ),
					'type'		=> 'text',
					'class'		=> '',
				    'std' 		=> 'collection-author',
					'default' 	=> 'collection-author',
				),
				// End Misc Option section
				array( 
					'type' 		=> 'sectionend',
					'id' 		=> 'cl_endpoints_settings'
				),
			));

		}  elseif ( woo_cl_is_follow_activate() && 'cl_follow_email_options' == $current_section ) { // If misc settings is selected

			$settings = apply_filters( 'woo_cl_follow_email_options', array(

				// Start Follow Settings section
				array( 
					'name'	=>	__( 'Follow Settings', 'woocl' ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'cl_follow_setting'
				),
				array(
					'id'		=> 'cl_enable_follow_coll_author',
					'name'		=> __( 'Enable Follow Author Button', 'woocl' ),
					'desc'		=> '',
					'type'		=> 'checkbox',
					'desc_tip'	=> sprintf( __( 'Check this box if you want to enable follow %s\'s author.','woocl' ), woo_cl_get_label_singular() )
				),
				// End follow Option section
				array( 
					'type' 		=> 'sectionend',
					'id' 		=> 'cl_follow_setting'
				),
				// Start email Settings section
				array( 
					'name'	=>	__( 'Email Settings', 'woocl' ),
					'type'	=>	'title',
					'desc'	=>	'',
					'id'	=>	'cl_email_setting'
				),
				array(
					'id'		=> 'cl_email_subject',
					'name'		=> __( 'Email Subject', 'woocl' ),
					'desc'		=> '<p class="description">'.sprintf( __( 'This is the subject of the email that will be sent to the followers of the %1$s when new product added for that %1$s. Available template tags for subject fields are :','woocl' ), woo_cl_get_label_singular() ).
									'<br /><code>{collection_name}</code> - '.sprintf( __( 'displays the %s name', 'woocl' ), woo_cl_get_label_singular() ).
									'<br /><code>{collection_item_name}</code> - '.sprintf( __( 'displays the %s item name', 'woocl' ), woo_cl_get_label_singular() ).'</p>',
					'type'		=> 'text',
					'class'		=> 'regular-text'
				),
				array(
					'id'		=> 'cl_email_body',
					'name'		=> __( 'Email Body', 'woocl' ),
					'desc'		=> '<p class="description">'.sprintf( __( 'This is the body, main content of the email that will be sent to the followers of the %1$s when new product added for that %1$s. The available tags are:','woocl' ), woo_cl_get_label_singular() ).
									'<br /><code>{collection_name}</code> - '.sprintf( __('displays the %s name', 'woocl' ), woo_cl_get_label_singular() ).
									'<br /><code>{collection_item_name}</code> - '.sprintf( __('displays the %s item name', 'woocl' ), woo_cl_get_label_singular() ).
									'<br /><code>{collection_item_description}</code> - '.sprintf( __('displays the %s item description', 'woocl' ), woo_cl_get_label_singular() ).'</p>',
					'type'		=> 'woo_cl_textarea',
					'class'		=> 'large-text',
					'css' 		=> 'height: 200px;',
					'editor'	=> true
				),
				// End email Option section
				array( 
					'type' 		=> 'sectionend',
					'id' 		=> 'cl_email_setting'
				),
			));

		} else { // Else go for general settings

			$settings = apply_filters( 'woo_cl_after_collection_settings', array(

				array(
						'title' => __( 'General Settings', 'woocommerce' ),
						'type' 	=> 'title',
						'id' 	=> 'cl_general_settings'
					),
				
	            array(
	                'name' 		=> sprintf( __('%s Page', 'woocl'), woo_cl_get_label_plural() ),
	                'desc' 		=> sprintf( __('<p class="description">Select the page where users will view their %s. This page should include the %s shortcode.</p>', 'woocl'), woo_cl_get_label_plural(), '<code>[woo_cl_collections]</code>' ),
	                'id' 		=> 'cl_coll_lists_page',
	                'type' 		=> 'single_select_page',
	                'class' 	=> 'chosen_select_nostd',
	                'desc_tip' 	=> '',
	            ),
				array(
					'id'		=> 'cl_guest_options',
					'name'		=> sprintf( __( 'Enable Guest %s', 'woocl' ), woo_cl_get_label_singular() ) ,
					'desc'		=> '',
					'type'		=> 'checkbox',
					'desc_tip'	=> sprintf( __( 'Check this box if you want to allow Guest customers to create their own %s.','woocl' ), woo_cl_get_label_plural() )
				),
				array(
					'id'		=> 'cl_disable_user_role',
					'name' 		=> sprintf( __('Hide Add To %s Button', 'woocl'), woo_cl_get_label_singular() ),
					'desc' 		=> sprintf( __( '<p class="description">Select the user roles for whom you wanted to hide add to %s button.</p>', 'woocl' ), woo_cl_get_label_singular(true) ),
	                'default' 	=> '',
	                'std' 		=> '',
	                'type' 		=> 'woo_cl_multiselect',
	                'val'		=> $user_editable_role
				),
				array(
					'id'		=> 'cl_max_collections_per_user',
					'name'		=> sprintf( __( 'Maximum %s Per User', 'woocl' ), woo_cl_get_label_plural() ),
					'desc'		=> sprintf( __( '<p class="description">Enter the maximum number of %s allowed per user, leave it blank for unlimited.</p>', 'woocl' ), woo_cl_get_label_plural(true) ),
					'type'		=> 'text',
					'class'		=> 'small-text',
					'desc_tip'	=> ''
				),
				array(
					'id'		=> 'cl_max_products_per_collection',
					'name'		=> sprintf( __( 'Maximum Products Per %s', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> sprintf( __( '<p class="description">Enter the maximum number of products allowed per %s, leave it blank for unlimited.</p>', 'woocl' ), woo_cl_get_label_singular(true) ),
					'type'		=> 'text',
					'class'		=> 'small-text',
					'desc_tip'	=> ''
				),
				array(
					'id'		=> 'cl_enable_coll_add_all_to_cart',
					'name'		=> __( 'Enable Bulk Add To Cart', 'woocl' ),
					'desc'		=> '',
					'type'		=> 'checkbox',
					'desc_tip'	=> __( 'Check this box if you wanted to allow users to add multiple products to cart by one click.','woocl' )
				),
				array(
					'id'		=> 'cl_front_coll_privacy',
					'name'		=> sprintf( __( 'Enable %s Privacy On Front End', 'woocl' ), woo_cl_get_label_singular() ) ,
					'desc'		=> '',
					'default'   => 'yes',
					'type'		=> 'checkbox',
					'desc_tip'	=> sprintf( __( 'Check this box if you want to allow user to manage %s privacy option on front end.','woocl' ), woo_cl_get_label_plural() )
				),
				array(
					'id'		=> 'cl_new_coll_privacy',
					'name'		=> sprintf( __( 'Default %s Privacy', 'woocl' ), woo_cl_get_label_singular() ),
					'desc'		=> sprintf( __('<p class="description">Select privacy for newly created %s, Default is : %s.</p>', 'woocl'), woo_cl_get_label_plural(), __( 'Public', 'woocl' ) ),
					'type'		=> 'select',
					'class'		=> 'chosen_select_nostd',
				    'std' 		=> 'public',
					'default' 	=> 'public',
					'options' 	=> array(  'public' => __( 'Public', 'woocl' ), 'private' => __( 'Private', 'woocl' ) )	
				),
		       ));

				$settings_2 = array(
					
					array(
						'type' 	=> 'sectionend',
						'id' 	=> 'cl_social_options'
					),
			        array(
							'title' => __( 'Social Settings', 'woocommerce' ),
							'type' 	=> 'title',
							'id' 	=> 'cl_social_settings'
						),
					array(
		                'name' 			=> __('Social Options', 'woocl'),
		                'desc' 			=> __('Facebook', 'woocl'),
						'id' 			=> 'cl_sharing_facebook',
		                'default' 		=> '',
		                'std' 			=> '',
		                'type' 			=> 'checkbox',
				            'checkboxgroup' => 'start'
			            ),
		            array(
		                'desc' 			=> __('Twitter', 'woocl'),
		                'id' 			=> 'cl_sharing_twitter',
		                'default' 		=> '',
		                'std' 			=> '',
		                'type' 			=> 'checkbox',
		                'checkboxgroup' => ''
		            ),
					array(
		                'desc' 			=> __('Google', 'woocl'),
		                'id' 			=> 'cl_sharing_google',
						'default' 		=> '',
		                'std' 			=> '',
		                'type' 			=> 'checkbox',
		                'checkboxgroup' => ''
		            ),
					array(
		                'desc' 			=> __('LinkedIn', 'woocl'),
		                'id' 			=> 'cl_sharing_linkedin',
						'default' 		=> '',
		                'std' 			=> '',
		                'type' 			=> 'checkbox',
		                'checkboxgroup' => ''
		            ),
					array(
		                'desc' 			=> __('Share Via Email', 'woocl'),
		                'id' 			=> 'cl_sharing_email',
						'default' 		=> '',
		                'std' 			=> '',
		                'type' 			=> 'checkbox',
		                'checkboxgroup' => 'end'
		            ),
					array(
						'type' 	=> 'sectionend',
						'id' 	=> 'cl_general_settings'
					),
				);

			//Merge all settings
		    $settings = apply_filters( 'woo_cl_general_settings', array_merge( $settings, $settings_2 ) );
	    }

		return apply_filters( 'woocommerce_get_settings_' . $this->id, $settings, $current_section );
	}
}

endif;

return new Woo_Cl_Settings_Tabs();
