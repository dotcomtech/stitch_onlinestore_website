<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Admin Class
 *
 * Handles generic Admin functionality.
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

class Woo_Cl_Admin {
	
	public $model, $prefix, $clmeta, $scripts;
	
	public function __construct(){
		
		global $woo_cl_model, $woo_cl_admin_meta, $woo_cl_scripts;

		$this->model 	= $woo_cl_model;
		$this->clmeta 	= $woo_cl_admin_meta;
		$this->scripts 	= $woo_cl_scripts;
		
		//Get meta prefix
		$this->prefix = WOO_CL_META_PREFIX;
	}

	/**
	 * When user registers and has guest lists, remove token meta key so their lists are saved indefinately
	 *
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_new_user_registration( $user_id ) {

		//Get meta prefix
		$prefix = WOO_CL_META_PREFIX;

		// get user's token if present
		$lists 	= woo_cl_get_guest_lists( woo_cl_get_list_token() );
	
		// attribute posts to new author
		if ( $lists ) {

			// loop throgh each list and assign the new user ID to their list
			foreach ( $lists as $key => $list ) {

				//Get Collection id
				$list_id	= isset( $list['ID'] ) ? $list['ID'] : '';

				$args = array(
					'ID'          => $list_id,
					'post_author' => $user_id
				);
				wp_update_post( $args );

				// Update author in collection item post type
				$this->model->woo_cl_update_author_in_coll_items($list_id, $user_id);

				// delete token one each list
				delete_post_meta( $list_id, $prefix.'cl_token', woo_cl_get_list_token() );
			}
			
			// remove cookie
			setcookie( 'woo_cl_token', '', time()-3600, COOKIEPATH, COOKIE_DOMAIN );
		}
	}

	/**
	 * Custom column
	 *
	 * Handles the custom columns to collections listing page
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_manage_custom_columns( $column_name, $post_id ) {
		
		global $wpdb, $post, $current_user;
		
		//get prefix
		$prefix		= WOO_CL_META_PREFIX;
		
		//get user ID
		$user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';
		
		switch( $column_name ) {

				case 'products' :
									$countargs = array(
														'author' 			=> $post->post_author,
														'post_parent' 		=> $post_id,
														'getcount'			=> 1,
													); 
									
									$total_items = $this->model->woo_cl_get_collection_items($countargs);
									echo $total_items;
									echo '<div id="inline_' . $post->ID . '_coll_downloads" class="hidden">' . $total_items . '</div>';
									break;
				case 'total' :
									$total_price = $this->model->woo_cl_collection_products_total_price($post_id);
									echo $total_price;					
									echo '<div id="inline_' . $post->ID . '_items_total" class="hidden">' . $total_price . '</div>';
									break;
									
				case 'featured' :
									// get Featured collection custom field
									$is_featured = get_post_meta( $post->ID, $prefix . 'featured_coll', true );
						            $html = "<input type='checkbox'  id='woocl_featuredbtn_".$post_id."' class='woocl_featuredbtn' ".(( $is_featured == 'yes' ) ? "checked='checked'" : "")."/>";							            
					            	echo $html;
									break;
									
				case 'follower':
									$follow_count	= 0;
									
									//check follow my blog post is activated or not
									if( woo_cl_is_follow_activate() ) {
										
										// get followers counts
										$follow_count 	= wpw_fp_get_post_followers_count( $post->ID );
									}
									
									echo $follow_count;
									echo '<div id="inline_' . $post->ID . '_coll_follower" class="hidden">' . $follow_count . '</div>';
									break;
			}
	}
	
	/**
	 * Add New Column to collections listing page
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	function woo_cl_add_new_collections_columns( $new_columns ) {
 		
 		unset( $new_columns['author'] );
 		unset( $new_columns['title'] );
 		unset( $new_columns['date'] );

		$new_columns['title'] 		= __( 'Title', 'woocl' );
		$new_columns['products'] 	= __( 'Products', 'woocl' );
		$new_columns['featured'] 	= __( 'Featured?', 'woocl' );
		
		if( woo_cl_is_follow_activate() ) {
			
			$new_columns['follower'] = __( 'Followers', 'woocl' );
		}
		
		$new_columns['author'] 		= __( 'Author', 'woocl' );
 		$new_columns['total'] 		= __( 'Total', 'woocl' );
 		$new_columns['date'] 		= __( 'Date', 'woocl' );
 		
		return $new_columns;
	}
	
	/**
	 * Add New Action For Create Preview
	 * 
	 * Handles to add new action for
	 * Create Preview link of that collection
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_preview_action_new_link_add( $actions, $post ) {
		
		//check current user can have administrator rights
		//post type is collections post type
		if ( ! current_user_can( 'manage_options' ) || $post->post_type != WOO_CL_POST_TYPE_COLL ) 
			return $actions;
		
		//Get collection items URL
		$collection_items_url	= $this->model->woo_cl_get_collection_item_page_url( $post->ID );
		
		$actions['view'] = '<a href="' . $collection_items_url . '" title="' 
									. sprintf( __( 'Make a preview for this %s', 'woocl' ), woo_cl_get_label_singular() )
									. '" rel="permalink">' .  __( 'View', 'woocl' ) . '</a>';
		
		// return all actions
		return $actions ;
	}
	
	
	/**
	 * Pop Up On Editor
	 * 
	 * Includes the pop up on the WordPress editor
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_shortcode_popup_markup() {

		global $post;

		$coll_post_type	= WOO_CL_POST_TYPE_COLL;

		include_once( WOO_CL_ADMIN . '/forms/woo-cl-admin-popup.php' );

		if( isset( $post->post_type ) && $post->post_type == $coll_post_type ) {

			include_once( WOO_CL_ADMIN . '/forms/woo-cl-admin-add-product-popup.php' );
		}
	}
	
	/**
	 * Hide update notification checkbox
	 * 
	 * Handle to hide update notification checkbox
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_follow_notification_checkbox_display( $is_display ) {
		
		global $post;
		
		$coll_post_type	= WOO_CL_POST_TYPE_COLL;
		
		if( isset( $post->post_type ) && $post->post_type == $coll_post_type ) {
			
			$is_display	= false;
		}
		
		return $is_display;
	}
	
	/**
	 * Remove Follow Metabox
	 * 
	 * Handle to remove follow metabox in collection post type
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_remove_followm_metabox() {
		
		$post_type	= WOO_CL_POST_TYPE_COLL;
		
		//Remove follow post type from collection post type page
		remove_meta_box( 'wpw_fp_follow_me_metabox' , $post_type , 'normal' );
	}
	
	/**
	 * Add Email Class
	 * 
	 * Handle to add email class
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_add_email_classes( $email_classes ) {

		//Include Collection share email class file
		require_once ( WOO_CL_ADMIN . '/class-woo-cl-email-collection-share.php' );
		$email_classes['Woo_Cl_Email_Collection_Share'] = new Woo_Cl_Email_Collection_Share();
		
		return $email_classes;
	}
	
	/**
	 * Delete Collection Items
	 * 
	 * Handles to Delete Collection Items When Perticular Collection deleted OR
	 * delete product from Collection  when product deleted from the woocommerce
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_delete_all_collection_product( $post_id = '' ){
		
		global $woo_cl_model;
		
		$prefix	= WOO_CL_META_PREFIX;
		
		if( !empty( $post_id ) ) { // check if collection id is not empty
			
			$post_type = get_post_type( $post_id ); // get	post type from collection id
			
			// Delete Collection Items When Perticular Collection deleted
			if( $post_type == WOO_CL_POST_TYPE_COLL ){ // check if post type is Collection's post type
				
				//argument for get collection item based on collection id
				$args = array(
								'post_status'	=> 'any',
								'post_parent'	=> $post_id,
				 			 );
				
				//Get collection item
				$posts	= $woo_cl_model->woo_cl_get_collection_items( $args );
				
				if( !empty( $posts ) ){ // check if get any post
					
					foreach ( $posts as $post ) {
						
						// delete all collection items
						wp_delete_post( $post['ID'], true );
					}
				}
			}
			
			// Delete Product from Collection item when product is deleted from woocommerce
			if( $post_type == WOO_CL_PRODUCT_POSTTYPE ){ // check if post type is woocommerce product's post type
				
				//argument for get collection item based on product id
				$args = array(
								'meta_query'	=> array(
															'post_status'	=> 'any',
															 array( 
																	'key' 	=> $prefix.'coll_product_id',
																	'value' => $post_id
															 )
														)
							 );
				
				//Get collection item
				$posts	= $woo_cl_model->woo_cl_get_collection_items( $args );
				
				if( !empty( $posts ) ){ // check if get any post
					
					foreach ( $posts as $post ) {
						
						// delete all collection items
						wp_delete_post( $post['ID'], true );
					}
				}
			}
		}
	}
	
	/**
	 * Trash Collection Item
	 * 
	 * Handle to trash collection item
	 * when product move in trash
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_trash_collection_item( $product_id = '' ) {
		
		global $woo_cl_model;
		
		$prefix	= WOO_CL_META_PREFIX;
		
		if( !empty( $post_id ) ) { // check if product id is not empty
			
			// get post type from product id
			$post_type	= get_post_type( $post_id );
			
			// check if post type is woocommerce product's post type
			if( $post_type == WOO_CL_PRODUCT_POSTTYPE ) {
				
				//argument for get collection item based on product id
				$args = array(
								'meta_query'	=> array(
															'post_status'	=> 'any',
															 array( 
																	'key' 	=> $prefix.'coll_product_id',
																	'value' => $post_id
															 )
														)
							 );
				
				//Get collection item
				$posts	= $woo_cl_model->woo_cl_get_collection_items( $args );
				
				if( !empty( $posts ) ){ // check if get any post
					
					foreach ( $posts as $post ) {
						
						// delete all collection items
						wp_delete_post( $post['ID'] );
					}
				}
			}
		}
	}
	
	/**
	 * Untrash Collection Item
	 * 
	 * Handle to untrash collection item
	 * when product is restore
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_untrash_collection_item( $product_id = '' ) {
		
		global $woo_cl_model;
		
		$prefix	= WOO_CL_META_PREFIX;
		
		if( !empty( $post_id ) ) { // check if product id is not empty
			
			// get post type from product id
			$post_type = get_post_type( $post_id );
			
			// check if post type is woocommerce product's post type
			if( $post_type == WOO_CL_PRODUCT_POSTTYPE ) { 
				
				//argument for get collection item based on product id
				$args = array(
								'meta_query'	=> array(
															'post_status'	=> 'any',
															 array( 
																	'key' 	=> $prefix.'coll_product_id',
																	'value' => $post_id
															 )
														)
							 );
				
				//Get collection item
				$posts	= $woo_cl_model->woo_cl_get_collection_items( $args );
				
				if( !empty( $posts ) ){ // check if get any post
					
					foreach ( $posts as $post ) {
						
						// delete all collection items
						wp_untrash_post( $post['ID'] );
					}
				}
			}
		}
	}

	/**
	 * Collection featured / unfeatured update
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	function woo_cl_featured_ajax() {

 		$prefix = WOO_CL_META_PREFIX;

		if( isset( $_POST['post_id'],  $_POST['woo_cl_featured'] ) && !empty( $_POST['post_id'] ) ) {

			$featured =  $_POST['woo_cl_featured'];
			update_post_meta( $_POST['post_id'], $prefix.'featured_coll', $featured );
			echo $featured;
			exit;
		}
	}

	/**
	 * Rewrite Collection Parameter
	 * 
	 * Handle to rewrite collection parameter when cllection setting has been changed
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_woocommerce_settings_saved() {

		global $current_tab, $woo_cl_query;

		if( $current_tab == 'collection' ) {

			//add endpoints to query vars
			//Need to call before flush rewrite rules
			$woo_cl_query->woo_cl_init_query_vars();
			$woo_cl_query->woo_cl_add_endpoints();
		}
	}

	/**
	 * Reset Guest User
	 * 
	 * Handle to reset guest user
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_restore_guest_author( $postid, $post ) {

		$prefix	= WOO_CL_META_PREFIX;

		$post_type_object = get_post_type_object( $post->post_type );

		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )	// Check Autosave
			|| ( ! isset( $_POST['post_ID'] ) || $postid != $_POST['post_ID'] )
			|| ( ! current_user_can( $post_type_object->cap->edit_post, $postid ) )
			|| ( $post->post_type != WOO_CL_POST_TYPE_COLL ) )  {
			return $postid;
		}

		//if set collection tocken
		$coll_list_token = get_post_meta( $postid, $prefix.'cl_token', true );

		if( !empty( $coll_list_token ) ) { // if access token is not empty

			$user_id = 0;
			$this->woo_cl_update_collection_author( $postid, $user_id );
		}
	}

	/**
	 * Update Author In Collection
	 * 
	 * Handle to update author in collection
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_update_collection_author( $post_id, $user_id ) {

		global $wpdb;

		$wpdb->update( $wpdb->posts, array( 'post_author' => $user_id ), array( 'ID' => $post_id ) );
	}
	
	/**
	 * Add custom fields to customers
	 * 
	 * Manage custom meta fieds for,
	 * maximum collection per user and,
	 * maximum products per collection.
	 * 
	 * displays only admin user
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_add_customer_meta_fields( $user ) { 
		
		if ( !current_user_can( 'edit_user' ) ) {
			return false;
		}
		
		$is_col_disable 	= get_the_author_meta( 'cl_disable_col_user', $user->ID );
		$is_disable_checked = ( !empty( $is_col_disable ) && $is_col_disable == 'yes' ) ? 'checked="checked"' : '';
		?>
			
		<h3><?php echo sprintf( __( '%s Settings', 'woocl' ), woo_cl_get_label_singular() ); ?></h3>
		
		<table class="form-table">
			<tr>
				<th><label for="cl_disable_col_user"><?php echo sprintf( __( 'Disable Add to %s Button', 'woocl' ), woo_cl_get_label_singular() ); ?></label></th>
				
				<td>
					<input type="checkbox" name="cl_disable_col_user" id="cl_disable_col_user" value="yes" <?php echo $is_disable_checked; ?> /><br />
					<span class="description"><?php echo sprintf( __( 'Check this box to disable add to %s button for this user.', 'woocl' ), woo_cl_get_label_singular( true ) );?></span>
				</td>
			</tr>
			
			<tr>
				<th><label for="cl_max_collections_per_user"><?php echo sprintf( __( 'Maximum %s per user', 'woocl' ), woo_cl_get_label_plural( true ) ); ?></label></th>
				
				<td>
					<input type="text" name="cl_max_collections_per_user" id="cl_max_collections_per_user" value="<?php echo esc_attr( get_the_author_meta( 'cl_max_collections_per_user', $user->ID ) ); ?>" /><br />
					<span class="description"><?php echo sprintf( __( 'Enter the maximum %s allowed per user, Leave it empty to use this setting from the settings page.', 'woocl' ), woo_cl_get_label_plural( true ) ) ?></span>
				</td>
			</tr>
			
			<tr>
				<th><label for="cl_max_products_per_collection"><?php echo sprintf( __( 'Maximum products per %s', 'woocl' ), woo_cl_get_label_singular( true ) ); ?></label></th>
				
				<td>
					<input type="text" name="cl_max_products_per_collection" id="cl_max_products_per_collection" value="<?php echo esc_attr( get_the_author_meta( 'cl_max_products_per_collection', $user->ID ) ); ?>" /><br />
					<span class="description"><?php echo sprintf( __( 'Enter the maximum products allowed per %s, Leave it empty to use this setting from the settings page.', 'woocl' ), woo_cl_get_label_singular( true ) ) ?></span>
				</td>
			</tr>
		</table>
	<?php
	}
	
	/**
	 * Save collection settings for user
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	public function woo_cl_save_customer_meta_fields( $user_id ) {
		
		if ( !current_user_can( 'edit_user', $user_id ) ) {
			return false;
		}

		$user_max_collection 	= isset($_POST['cl_max_collections_per_user']) ? $this->model->woo_cl_escape_slashes_deep( trim($_POST['cl_max_collections_per_user']) ) : '';
		$user_max_products	 	= isset($_POST['cl_max_products_per_collection']) ? $this->model->woo_cl_escape_slashes_deep( trim($_POST['cl_max_products_per_collection']) ) : '';
		$user_disable_add_col	= isset($_POST['cl_disable_col_user']) ? $_POST['cl_disable_col_user'] : 'no';
		
		update_user_meta( $user_id, 'cl_max_collections_per_user', $user_max_collection );
		update_user_meta( $user_id, 'cl_max_products_per_collection', $user_max_products );
		update_user_meta( $user_id, 'cl_disable_col_user', $user_disable_add_col );
	}

	/**
	 * Save product meta
	 * 
	 * Save product meta for hide,
	 * collection button in product
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.3
	 */
	public function woo_cl_save_product_meta_settings( $post_id, $post ) {
		
		$prefix	= WOO_CL_META_PREFIX;

		if( isset( $_POST[$this->prefix.'hide_coll_btn'] ) ) {
			update_post_meta( $post_id, $prefix . 'hide_coll_btn', $_POST[$this->prefix.'hide_coll_btn'] );
		} else {
			update_post_meta( $post_id, $prefix . 'hide_coll_btn', '' );
		}

		//Get button text and update
		$add_to_collection_btn_text = isset( $_POST[$this->prefix.'add_to_collection_btn_text'] ) && trim( $_POST[$this->prefix.'add_to_collection_btn_text'] ) != '' ? $_POST[$this->prefix.'add_to_collection_btn_text'] : '';
		update_post_meta( $post_id, $this->prefix .'add_to_collection_btn_text', $add_to_collection_btn_text );

		//Get button type and update
		$add_to_collection_btn_type = !empty( $_POST[$this->prefix.'add_to_collection_btn_type'] ) ? $_POST[$this->prefix.'add_to_collection_btn_type'] : '';
		update_post_meta( $post_id, $this->prefix .'add_to_collection_btn_type', $add_to_collection_btn_type );

		//Get button icon and update
		$add_to_collection_link_icon = !empty( $_POST[$this->prefix.'add_to_collection_link_icon'] ) ? $_POST[$this->prefix.'add_to_collection_link_icon'] : '';
		update_post_meta( $post_id, $this->prefix .'add_to_collection_link_icon', $add_to_collection_link_icon );

	}
	
	/**
	 * Filter main variable product
	 * 
	 * Handles to remove Parent Variation Product
	 * from Woocommerce Search form result
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_json_search_found_products( $found_products ) {
		
		// If $_GET is set to 'woo-cl-admin-pro-search' and $found_products is not empty
		if( isset( $_GET['exclude'] ) && $_GET['exclude'] == 'woo-cl-admin-pro-search' && !empty( $found_products ) ) {
			
			foreach( $found_products as $product_id => $product_name ) { // foreach loop on result product
				
				$product = wc_get_product( $product_id ); // Get product details
				if( $product->has_child() ) { // If the product has child variation products
					
					unset( $found_products[$product_id] ); // Unset the product_id
				}
			}
		}
				
		return $found_products;
	}

	/**
	 * Collections sorting link.
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.1
	 */
	public function woo_cl_collection_sorting_link( $views ) {
		global $post_type, $wp_query;

		if ( ! current_user_can( 'edit_others_pages' ) ) {
			return $views;
		}

		$class            = ( isset( $wp_query->query['orderby'] ) && $wp_query->query['orderby'] == 'menu_order title' ) ? 'current' : '';
		$query_string     = remove_query_arg(array( 'orderby', 'order' ));
		$query_string     = add_query_arg( 'orderby', urlencode('menu_order title'), $query_string );
		$query_string     = add_query_arg( 'order', urlencode('ASC'), $query_string );
		$views['byorder'] = '<a href="' . esc_url( $query_string ) . '" class="' . esc_attr( $class ) . '">' . sprintf( __( 'Sort %s', 'woocl' ), woo_cl_get_label_plural() ) . '</a>';

		return $views;
	}

	/**
	 * Ajax request handling for collection ordering.
	 *
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.1
	 */
	public static function woo_cl_collection_ordering() {
		global $wpdb;

		$post_type	= WOO_CL_POST_TYPE_COLL;

		ob_start();

		// check permissions again and make sure we have what we need
		if ( ! current_user_can('edit_posts') || empty( $_POST['id'] ) || ( ! isset( $_POST['previd'] ) && ! isset( $_POST['nextid'] ) ) ) {
			die(-1);
		}

		// real post?
		if ( ! $post = get_post( $_POST['id'] ) ) {
			die(-1);
		}

		$previd  = isset( $_POST['previd'] ) ? $_POST['previd'] : false;
		$nextid  = isset( $_POST['nextid'] ) ? $_POST['nextid'] : false;
		$new_pos = array(); // store new positions for ajax

		$siblings = $wpdb->get_results( $wpdb->prepare( "
			SELECT ID, menu_order FROM {$wpdb->posts} AS posts
			WHERE 	posts.post_type 	= '$post_type'
			AND 	posts.post_status 	IN ( 'publish', 'pending', 'draft', 'future', 'private' )
			AND 	posts.ID			NOT IN (%d)
			ORDER BY posts.menu_order ASC, posts.ID DESC
		", $post->ID ) );

		$menu_order = 0;

		foreach ( $siblings as $sibling ) {

			// if this is the post that comes after our repositioned post, set our repositioned post position and increment menu order
			if ( $nextid == $sibling->ID ) {
				$wpdb->update(
					$wpdb->posts,
					array(
						'menu_order' => $menu_order
					),
					array( 'ID' => $post->ID ),
					array( '%d' ),
					array( '%d' )
				);
				$new_pos[ $post->ID ] = $menu_order;
				$menu_order++;
			}

			// if repositioned post has been set, and new items are already in the right order, we can stop
			if ( isset( $new_pos[ $post->ID ] ) && $sibling->menu_order >= $menu_order ) {
				break;
			}

			// set the menu order of the current sibling and increment the menu order
			$wpdb->update(
				$wpdb->posts,
				array(
					'menu_order' => $menu_order
				),
				array( 'ID' => $sibling->ID ),
				array( '%d' ),
				array( '%d' )
			);
			$new_pos[ $sibling->ID ] = $menu_order;
			$menu_order++;

			if ( ! $nextid && $previd == $sibling->ID ) {
				$wpdb->update(
					$wpdb->posts,
					array(
						'menu_order' => $menu_order
					),
					array( 'ID' => $post->ID ),
					array( '%d' ),
					array( '%d' )
				);
				$new_pos[$post->ID] = $menu_order;
				$menu_order++;
			}

		}

		do_action( 'woo_cl_after_collection_ordering' );

		wp_send_json( $new_pos );
	}

	/**
	 * Ajax request handling for collection item ordering.
	 *
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.1
	 */
	public static function woo_cl_collection_item_ordering() {
		global $wpdb;

		$post_type	= WOO_CL_POST_TYPE_COLLITEMS;

		ob_start();

		// check permissions again and make sure we have what we need
		if ( ! current_user_can('edit_posts') || empty( $_POST['id'] ) || ( ! isset( $_POST['previd'] ) && ! isset( $_POST['nextid'] ) ) || empty( $_POST['coll_id'] ) ) {
			die(-1);
		}

		// real post?
		if ( ! $post = get_post( $_POST['id'] ) ) {
			die(-1);
		}

		$coll_id  = isset( $_POST['coll_id'] ) ? $_POST['coll_id'] : false;
		$previd   = isset( $_POST['previd'] ) ? $_POST['previd'] : false;
		$nextid   = isset( $_POST['nextid'] ) ? $_POST['nextid'] : false;
		$new_pos  = array(); // store new positions for ajax

		$siblings = $wpdb->get_results( $wpdb->prepare( "
			SELECT ID, menu_order FROM {$wpdb->posts} AS posts
			WHERE 	1=1  AND posts.post_type 	= '$post_type'
			AND 	posts.post_status 	IN ( 'publish' )
			AND 	posts.ID			NOT IN (%d)
			AND 	posts.post_parent	= $coll_id
			ORDER BY posts.menu_order ASC, posts.ID DESC
		", $post->ID ) );

		$menu_order = 0;

		foreach ( $siblings as $sibling ) {

			// if this is the post that comes after our repositioned post, set our repositioned post position and increment menu order
			if ( $nextid == $sibling->ID ) {
				$wpdb->update(
					$wpdb->posts,
					array(
						'menu_order' => $menu_order
					),
					array( 'ID' => $post->ID ),
					array( '%d' ),
					array( '%d' )
				);
				$new_pos[ $post->ID ] = $menu_order;
				$menu_order++;
			}

			// if repositioned post has been set, and new items are already in the right order, we can stop
			if ( isset( $new_pos[ $post->ID ] ) && $sibling->menu_order >= $menu_order ) {
				break;
			}

			// set the menu order of the current sibling and increment the menu order
			$wpdb->update(
				$wpdb->posts,
				array(
					'menu_order' => $menu_order
				),
				array( 'ID' => $sibling->ID ),
				array( '%d' ),
				array( '%d' )
			);
			$new_pos[ $sibling->ID ] = $menu_order;
			$menu_order++;

			if ( ! $nextid && $previd == $sibling->ID ) {
				$wpdb->update(
					$wpdb->posts,
					array(
						'menu_order' => $menu_order
					),
					array( 'ID' => $post->ID ),
					array( '%d' ),
					array( '%d' )
				);
				$new_pos[$post->ID] = $menu_order;
				$menu_order++;
			}

		}

		do_action( 'woo_cl_after_collection_item_ordering' );

		wp_send_json( $new_pos );
	}

	/**
	 * Handles to add plugin settings in Woocommerce -> Settings
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
	function woo_cl_collection_admin_settings_tab( $settings ) {
	
	    // Add settings in array
	    $settings[] = require_once( WOO_CL_ADMIN . '/class-woo-cl-admin-settings-tabs.php' );
	
	    return $settings; // Return
	}

	/**
	 * Control admin menues ( Added collection Category Menu )
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
	public function woo_cl_control_admin_menu() {

		//Add Menu for add to collections
		$menu_name = sprintf( __( 'Add To %s', 'woocl' ), woo_cl_get_label_singular() );
		$addtocoll_page_slug = add_submenu_page( WOO_CL_MAIN_MENU_NAME, $menu_name, $menu_name, 'manage_options', 'woo_cl_add_to_coll', array( $this, 'woo_cl_add_to_collection_menu_page' ) );

		//Action to load script on admin page
		add_action( "admin_head-$addtocoll_page_slug", array( $this->scripts, 'woo_cl_addtocoll_page_load_scripts' ) );
	}

	/**
	 * Add to collection menu page
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
	public function woo_cl_add_to_collection_menu_page() {
		include_once( WOO_CL_ADMIN .'/forms/woo-cl-admin-add-to-collection.php' );
	}

	/**
	 * Select parent menu if collection Category Menu
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
	public function woo_cl_admin_menu_parent_selected( $parent_slug ) {

		global $parent_file;

		//Check if collection category menu then select woocommerce menu as parent
		if( $parent_file == 'edit.php' && !empty( $_GET['taxonomy'] ) && $_GET['taxonomy'] == WOO_CL_TAXONOMY ) {
		  	$parent_file = WOO_CL_MAIN_MENU_NAME;
		}

	  	return $parent_file;
	}

	/**
	 * Ajax request handling for get user collections.
	 *
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
	function woo_cl_get_user_collections() {

		$response = array();

		//Get meta prefix
		$prefix = WOO_CL_META_PREFIX;

		//Get all Messages
		$messages	= woo_cl_messages();

		if( isset( $_POST['user_id'] ) && !empty( $_POST['user_id'] ) ) {

			$user_collections		= $this->model->woo_cl_get_collections( array( 'post_status'	=> array( 'publish', 'private' ), 'author' =>  $_POST['user_id'] ) );

			if( !empty( $user_collections ) ) {

				$html = '<option value="">'. sprintf( __( 'Select %s', 'woocl' ), woo_cl_get_label_singular() ). '</option>';

				foreach ( $user_collections as $key => $value ) {

					//collection items query arguments
					$args = array(
									'post_parent' 	=> $value['ID']
								); 

					//get total collection items
					$coll_products	= $this->model->woo_cl_get_collection_items( $args );
				
					foreach( $coll_products as $key => $coll_product ) {
						
						// get product id of collection item
						$curr_product_id[]	= get_post_meta( $coll_product['ID'], $prefix.'coll_product_id', true );
					}

					$html .= '<option value="' . $value['ID'] . '" data-item="'.implode(',', $curr_product_id ).'">' . $value['post_title'] . '</option>';
				}

				$response['sucess_html'] = $html;
				echo json_encode( $response );
				exit;

			} else {

				$response['error'] = isset( $messages['no_coll_found'] ) ? $messages['no_coll_found'] : '';
				echo json_encode( $response );
				exit;
			}
		}
	}

	/**
	 * Ajax request handling for add products to collections.
	 *
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
	function woo_cl_add_to_collection(){
		
		global $current_user;
		
		//Initilize collection flag
		$collflag	= false;
		
		//Get User Id
		$user_ID	= isset(  $_POST['user_id']) ?  $_POST['user_id'] : '';

		//Get all Messages
		$messages	= woo_cl_messages();
		
		$coll_id        	= isset( $_POST['col_id']) 		? $_POST['col_id']		: '';
		$coll_productids	= isset( $_POST['products'])	? $_POST['products']	: '';
		
		//Get title if exist
		$coll_producttitle = woo_cl_get_product_title( $coll_productid );
		
		// get collection description
		$collitem_disc = isset($_POST['collitem_disc']) ? $_POST['collitem_disc'] : '';

		// Get post author id
		$author_id = get_post_field( 'post_author', $coll_id );
		
		// check for post parent
		$main_id = wp_get_post_parent_id($coll_productid);
		$variation_id = $coll_productid;
		
		// Check if empty parent id
		if( empty($main_id) ) {
			
			$main_id = $coll_productid;
			$variation_id = '';
		}
		
		// Check collection button is enable
		if( !woo_cl_collection_btn_isenable($main_id, $variation_id) ) {
			
			$response['error'] = isset( $messages['can_not_add_product'] ) ? $messages['can_not_add_product'] : '';

			echo json_encode ($response);
			exit;
		}
		
		// check product limit per collection
		if( !woo_cl_check_product_limit_per_collection( $coll_id ) ) {//check total products in collection 

			$response['error'] = isset( $messages['products_limit_over'] ) ? $messages['products_limit_over'] : '';

			echo json_encode ($response);
			exit;
		}
		foreach( $coll_productids as $coll_productid ){
			
			$product = wc_get_product( $coll_productid );
			
			$product_title[] = $product->get_title();
			
			//Get collection items
			$collection_items = woo_cl_product_available_in_collection( $coll_id, $coll_productid, $author_id );
			
			if( !empty( $collection_items ) ) { // check if Product allready exist in that collection or not
				
				$response['error'] = isset( $messages['already_added'] ) ? "'".$product->get_title()."' ".$messages['already_added'] : '';
	
				echo json_encode( $response );
				exit;
			}
		}
		
		
		if( !woo_cl_user_can_update_collection( $coll_id ) ) {
			
			$response['error'] = isset( $messages['prevent_add_access'] ) ? $messages['prevent_add_access'] : '';

			echo json_encode( $response );
			exit;
		}

		//get next menu order
		$next_menu_order	= $this->model->woo_cl_get_coll_item_next_manu_order( $coll_id );

		foreach ($coll_productids as $coll_productid){
			
			$args = array(
					'coll_id'			=> $coll_id,
					'coll_productid'	=> $coll_productid,
					);
			//Add product to collection
			$collection_item_id = $this->model->woo_cl_add_product_to_collection( $args );
		}
		
		//Responce array for add to collection
		$response['success'] 	 = sprintf( __('Products %s successfully added in to collection', 'woocl'), implode(',',$product_title));
		
		echo json_encode( $response );

		//Must write exit to return proper result in ajax
		exit;
	}

	/**
	 * Custom column
	 *
	 * Handles the custom columns to Product listing page
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
	public function woo_cl_manage_custom_product_columns( $column_name, $post_id ) {

		switch( $column_name ) {
			case 'coll_count' :
				echo woo_cl_count_product_collections( $post_id );
				break;
		}
	}

	/**
	 * Add New Column to Product/Users listing page
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
	function woo_cl_add_new_collection_counts_columns( $new_columns ) {
 		$new_columns['coll_count'] = sprintf( __( '%s Count', 'woocl' ), woo_cl_get_label_singular() );
		return $new_columns;
	}

	/**
	 * Custom column
	 *
	 * Handles the custom columns to Users listing page
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
	public function woo_cl_manage_custom_users_columns( $value, $column_name, $user_id ) {
		if( $column_name == 'coll_count' ) {
			return woo_cl_count_user_collections( $user_id, WOO_CL_POST_TYPE_COLL );
		}
		return $value;
	}

	/**
	 * Adding Hooks
	 * 
	 * Adding proper hooks for the admin post list.
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function add_hooks() {

		//add action for guest user collection update
		add_action( 'user_register', array( $this, 'woo_cl_new_user_registration' ), 10, 1 );
		add_action( 'wpmu_new_user', array( $this, 'woo_cl_new_user_registration' ), 10, 1 );

		//add new field to post listing page
		add_action('manage_'.WOO_CL_POST_TYPE_COLL.'_posts_custom_column', array($this,'woo_cl_manage_custom_columns'), 10, 2);
		add_filter('manage_edit-'.WOO_CL_POST_TYPE_COLL.'_columns', array($this,'woo_cl_add_new_collections_columns'));

		//add filter to add new action "view preview" on admin Collections page
		add_filter( 'post_row_actions', array( $this , 'woo_cl_preview_action_new_link_add' ), 10, 2 );

		//Rewrite urls when settings has been changed
		// this action called after settings saved in database
		add_action( 'woocommerce_update_options', array( $this, 'woo_cl_woocommerce_settings_saved' ), 100 );

		// mark up for popup
		add_action( 'admin_footer-post.php', array( $this,'woo_cl_shortcode_popup_markup' ) );
		add_action( 'admin_footer-post-new.php', array( $this,'woo_cl_shortcode_popup_markup' ) );

		//Hide follow my blog post  metaboxes from collections post type
		add_filter( 'wpw_fp_check_post_update_notification', array($this, 'woo_cl_follow_notification_checkbox_display' ) );
		add_action( 'do_meta_boxes', array( $this, 'woo_cl_remove_followm_metabox' ) );

		//add action for email templates classes for woo collection
		add_filter( 'woocommerce_email_classes', array( $this, 'woo_cl_add_email_classes' ) );

		// delete collection item when delete collection or delete product
		add_action('before_delete_post', array( $this, 'woo_cl_delete_all_collection_product' ) );

		//add action for update featured collection
		add_action( 'admin_init', array( $this, 'woo_cl_featured_ajax' ) );

		// trash collection item when product are moved to trash
		//add_action( 'wp_trash_post', array( $this, 'woo_cl_trash_collection_item' ) );
		
		// restore collection item when product are restore
		//add_action( 'untrash_post', array( $this, 'woo_cl_untrash_collection_item' ) );
		
		add_action( 'save_post', array( $this, 'woo_cl_restore_guest_author' ), 10, 2 );
		
		// Add custom fields for maximum collection per user and maximum products per collection
		add_action( 'show_user_profile', array($this, 'woo_cl_add_customer_meta_fields'), 20 );
		add_action( 'edit_user_profile', array($this, 'woo_cl_add_customer_meta_fields'), 20 );
		
		// Save customer collection meta fields
		add_action( 'personal_options_update', array($this, 'woo_cl_save_customer_meta_fields') );
		add_action( 'edit_user_profile_update', array($this, 'woo_cl_save_customer_meta_fields') );

		// Add filter to remove parent variation product from search result
		add_filter( 'woocommerce_json_search_found_products', array( $this, 'woo_cl_json_search_found_products' ) );

		//Action to add sorting link on collections
		add_filter( 'views_edit-'.WOO_CL_POST_TYPE_COLL, array( $this, 'woo_cl_collection_sorting_link' ) );

		//Action to call ajax for ordering collections
		add_action( 'wp_ajax_woo_cl_collection_ordering', array( $this, 'woo_cl_collection_ordering' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_collection_ordering', array( $this, 'woo_cl_collection_ordering' ) );

		//Action to call ajax for ordering collection items
		add_action( 'wp_ajax_woo_cl_collection_item_ordering', array( $this, 'woo_cl_collection_item_ordering' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_collection_item_ordering', array( $this, 'woo_cl_collection_item_ordering' ) );

        // add metabox in products
        add_action( 'woocommerce_product_write_panel_tabs', array( $this->clmeta, 'woo_cl_product_write_panel_tab' ) );

        // To make compatible with previous versions of 3.0.0
        if (version_compare(WOOCOMMERCE_VERSION, "3.0.0") == -1) {
            // woocommerce_product_write_panels is deprecated since version 2.6!
            add_action( 'woocommerce_product_write_panels', array( $this->clmeta, 'woo_cl_product_write_panel' ) );
        } else {
            add_action( 'woocommerce_product_data_panels', array( $this->clmeta, 'woo_cl_product_write_panel' ) );
        }

		// Save product meta in collection tab
		add_action( 'woocommerce_process_product_meta', array( $this, 'woo_cl_save_product_meta_settings' ), 10, 2 );

		// Add filter for adding plugin settings
		add_filter( 'woocommerce_get_settings_pages', array( $this, 'woo_cl_collection_admin_settings_tab' ) );

		//Action to control admin menu and highlight menu
		add_action( 'admin_menu', array( $this, 'woo_cl_control_admin_menu' ) );

		//Action to call ajax for get user collections
		add_action( 'wp_ajax_woo_cl_get_user_collections', array( $this, 'woo_cl_get_user_collections' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_get_user_collections', array( $this, 'woo_cl_get_user_collections' ) );
		//Action to call ajax for add products to collections
		add_action( 'wp_ajax_woo_cl_add_to_collection', array( $this, 'woo_cl_add_to_collection' ) );
		add_action( 'wp_ajax_nopriv_woo_cl_add_to_collection', array( $this, 'woo_cl_add_to_collection' ) );

		//add new field to product listing page
		add_action( 'manage_product_posts_custom_column', array( $this, 'woo_cl_manage_custom_product_columns' ), 10, 2);
		add_filter( 'manage_edit-product_columns', array( $this, 'woo_cl_add_new_collection_counts_columns' ) );

		//add new field to users listing page
		add_action( 'manage_users_custom_column', array( $this, 'woo_cl_manage_custom_users_columns' ), 10, 3 );
		add_filter( 'manage_users_columns', array( $this, 'woo_cl_add_new_collection_counts_columns' ) );
	}
}