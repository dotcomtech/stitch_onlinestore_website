<?php 

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Shortocde UI
 *
 * This is the code for the pop up editor.
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
?>

<div class="woo-cl-popup-content">

	<div class="woo-cl-header-popup">
		<div class="woo-cl-popup-header-title"><?php _e( 'Add a Shortcodes', 'woocl' );?></div>
		<div class="woo-cl-popup-close"><a href="javascript:void(0);" class="woo-cl-popup-close-button"><img src="<?php echo WOO_CL_IMG_URL;?>/close_icon.png"></a></div>
	</div>
	<div class="woo-cl-popup-error"><?php _e( 'Select a Shortcode', 'woocl' );?></div>
	<div class="woo-cl-popup-container">
		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row">
						<label><?php _e( 'Select a Shortcode', 'woocl' );?></label>		
					</th>
					<td>
						<select class="wc-enhanced-select woo-cl-select-box woo-cl-icon-select" id="woo_cl_shortcode">				
							<option value="woo_cl_add_to_collection"><?php echo sprintf( __( 'Add to %s', 'woocl' ), woo_cl_get_label_singular() );?></option>
							<option value="woo_cl_featured_collections"><?php echo sprintf( __( 'Featured %s', 'woocl' ), woo_cl_get_label_plural() );?></option>
							<option value="woo_cl_my_collections"><?php echo sprintf( __( 'My %s', 'woocl' ), woo_cl_get_label_plural() );?></option>
						</select>		
					</td>
				</tr>
			</tbody>
		</table>

		<div id="woo_cl_add_to_collection_wrap" class="woo-cl-shortcodes-options">
			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row">
							<label for="woo_cl_add_to_collection_id"><?php _e( 'Product ID', 'woocl' );?></label>		
						</th>
						<td>
							<select class="wc-product-search" style="width:250px;" id="woo_cl_add_to_collection_id" name="woo_cl_add_to_collection_id" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'woocl' ); ?>" data-action="woocommerce_json_search_products_and_variations"></select>
							<br/><span class="description"><?php echo sprintf( __('Here you can select product which you want to add to collection.','woocl'), woo_cl_get_label_singular() );?></span>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="woo_cl_add_to_collection_text"><?php _e( 'Button Text', 'woocl' );?></label>		
						</th>
						<td>
							<input type="text" value="" class="regular-text woo-cl-icon-select" id="woo_cl_add_to_collection_text"><br />
							<span class="description"><?php echo sprintf( __( 'Here you can give the Add to %s button custom text. Default is: Add to Collection. Available template tag is :', 'woocl' ), woo_cl_get_label_singular() ) .'<br /><code>{no_of_collections}</code> - '.sprintf( __( 'display the no of %s', 'woocl' ), woo_cl_get_label_plural() );?></span>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="woo_cl_add_to_collection_type"><?php _e( 'Button / Link Type', 'woocl' );?></label>		
						</th>
						<td>
							<select class="wc-enhanced-select woo-cl-select-box woo-cl-icon-select" id="woo_cl_add_to_collection_type">
								<option value="default"><?php _e( 'Default', 'woocl' );?></option>
								<option value="button"><?php _e( 'Button', 'woocl' );?></option>
								<option value="link"><?php _e( 'Link', 'woocl' );?></option>
							</select><br />
							<span class="description"><?php echo sprintf( __('Here you can select a add to %s button type.','woocl'), woo_cl_get_label_singular() );?></span>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="cl_add_to_collection_link_icon"><?php _e( 'Icon', 'woocl' );?></label>		
						</th>
						<td>
							<select class="wc-enhanced-select woo-cl-select-box woo-cl-icon-select" id="cl_add_to_collection_link_icon" name="cl_add_to_collection_link_icon">
								<option value=""><?php _e( 'None', 'woocl' );?></option>
								<option value="fa fa-star"><?php _e( 'Star', 'woocl' );?></option>
								<option value="fa fa-gift"><?php _e( 'Present', 'woocl' );?></option>
								<option value="fa fa-plus"><?php _e( 'Add', 'woocl' );?></option>
								<option value="fa fa-bookmark"><?php _e( 'Bookmark', 'woocl' );?></option>
								<option value="fa fa-heart"><?php _e( 'Heart', 'woocl' );?></option>
						  	</select><br />
							<span class="description"><?php echo sprintf( __('The icon to show next to the add to %s links.','woocl'), woo_cl_get_label_singular() );?></span>
						</td>
					</tr>
				</tbody>
			</table>
		</div><!--.woo-cl-popup-box-type-->
		
		<div id="woo_cl_featured_coll_wrap" class="woo-cl-shortcodes-options">
			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row">
							<label for="woo_cl_featured_colls_title"><?php echo sprintf( __( 'Featured %s Title :', 'woocl' ), woo_cl_get_label_singular() );?></label>		
						</th>
						<td>
							<input type="text" value="" class="regular-text woo-cl-icon-select" id="woo_cl_featured_colls_title"><br />
							<span class="description"><?php echo sprintf( __('Enter Featured %s Title.','woocl'), woo_cl_get_label_singular() );?></span>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="woo_cl_no_of_coll"><?php echo sprintf( __( 'Number of %s :', 'woocl' ), woo_cl_get_label_plural() );?></label>		
						</th>
						<td>
							<input type="text" value="" class="small-text" id="woo_cl_no_of_coll"><br />
							<span class="description"><?php echo sprintf( __('Enter a number of %s you want to display','woocl'), woo_cl_get_label_singular() );?></span>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="woo_cl_show_private"><?php echo __( 'Show Private :', 'woocl' );?></label>		
						</th>
						<td>
							<input type="checkbox" value="1" id="woo_cl_show_private"><br />
							<span class="description"><?php echo sprintf( __('Check this box if you want to display private %s.','woocl'), woo_cl_get_label_plural() );?></span>
						</td>
					</tr>
				</tbody>
			</table>
		</div><!--.woo-cl-popup-box-type-->
		
		<div id="woo_cl_my_collections_wrap" class="woo-cl-shortcodes-options">
			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row">
							<label for="woo_cl_my_colls_title"><?php echo sprintf( __( 'My %s Title :', 'woocl' ), woo_cl_get_label_plural() );?></label>		
						</th>
						<td>
							<input type="text" value="" class="regular-text woo-cl-icon-select" id="woo_cl_my_colls_title"><br />
							<span class="description"><?php echo sprintf( __('Enter My %s Title.','woocl'), woo_cl_get_label_singular() );?></span>
						</td>
					</tr>
					<!--<tr>
						<th scope="row">
							<label for="woo_cl_per_page_collection"><?php echo sprintf( __( '%s per page :', 'woocl' ), woo_cl_get_label_plural() );?></label>		
						</th>
						<td>
							<input type="text" value="3" class="small-text" id="woo_cl_per_page_collection"><br />
							<span class="description"><?php echo sprintf( __('Enter a Number of %s you want to dispaly per page.','woocl'), woo_cl_get_label_plural() );?></span>
						</td>
					</tr>-->
					<tr>
						<th scope="row">
							<label for="woo_cl_show_all_colls_count"><?php echo sprintf( __( 'Show %s Count :', 'woocl' ), woo_cl_get_label_plural() );?></label>		
						</th>
						<td>
							<input type="checkbox" value="1" id="woo_cl_show_all_colls_count"><br />
							<span class="description"><?php echo sprintf( __('Check this box if you want to display total number of %s.','woocl'), woo_cl_get_label_plural() );?></span>
						</td>
					</tr>
					<tr>
						<th scope="row">
							<label for="woo_cl_show_all_colls_link"><?php echo sprintf( __( 'Show See All %s Link :', 'woocl' ), woo_cl_get_label_plural() );?></label>		
						</th>
						<td>
							<input type="checkbox" value="1" id="woo_cl_show_all_colls_link"><br />
							<span class="description"><?php echo sprintf( __('Check this box if you want to display link for See All %s.','woocl'), woo_cl_get_label_plural() );?></span>
						</td>
					</tr>
				</tbody>
			</table>
		</div><!--.woo-cl-popup-box-type-->
		
		<div id="woo_cl_insert_container" >
			<input type="button" class="button-secondary" id="woo_cl_insert_shortcode" value="<?php _e( 'Insert Shortcode', 'woocl' ); ?>">
		</div>
		 
	</div>	
	
</div><!--.woo-cl-popup-content-->
<div class="woo-cl-popup-overlay"></div><!--.woo-cl-popup-overlay-->