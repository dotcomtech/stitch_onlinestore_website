<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Add To Collection page
 *
 * The code fro adding collection product from backend
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.1.5
 */
 ?>
<div class="wrap">

<?php
	global $wpdb,$woo_cl_model,$woo_cl_message; // call globals to use them in this page
	
	// model class
	$model = $woo_cl_model; 
	
	// Get prefix
	$prefix = WOO_CL_META_PREFIX;
?>
<!-- Page name -->
<h2><?php echo sprintf( __( 'Add Product(s) to %s', 'woocl' ), woo_cl_get_label_singular() ); ?></h2><br />

<!-- beginning of the general settings meta box -->
<div id="woo-cl-general" class="post-box-container">
	<div class="metabox-holder">	
		<div class="meta-box-sortables ui-sortable">
			<div id="general" class="postbox">	
				<div class="handlediv" title="<?php _e( 'Click to toggle', 'woocl' ); ?>"><br /></div>
					<!-- Add To Collection page title -->
					<h3 class="hndle">
						<span style='vertical-align: top;'><?php echo sprintf( __( 'Add Product(s) to %s', 'woocl' ), woo_cl_get_label_singular() ); ?></span>
					</h3>
					<div class="inside">
					<form name="add-to-collection" id="woocl_add_to_collection" method="POST">
					<table class="form-table">
						<tbody>
							<!-- All User List -->
							<tr valign="top">
								<th scope="row">
									<label for="users"><?php _e( 'Users:', 'woocl' ); ?></label>
								</th>
								<td>
								<div class="woo-cl-post-select-wrap">
									<?php wp_dropdown_users( array( 'show_option_none'  => __( 'Select User','woocl' ),'option_none_value' => '','class' => 'woo_cl_dropdown wc-enhanced-select woo-cl-select-box woo-cl-icon-select', 'id' => 'woo_cl_dropdown_user') );?>
									</div>
									<span class="woo-cl-collection-loader woo-cl-post-collection-loader"><img src="<?php echo WOO_CL_IMG_URL . '/woo-cl-loader.gif'; ?>" alt="..." /></span>
									<div class="clear"></div>
									<span class="description"><?php _e( 'Select user in which you would like to add product in perticular user collection.', 'woocl' );?></span>
									<div class="user_type_post_error woo_cl_add_to_collection_error" style="display: none;"><?php echo __( 'Please select user.', 'woocl' );?></div>
								</td>
							</tr>

							<!-- User Collection List -->
							<tr valign="top" class="woo-cl-display-none collection_list">
								<th scope="row">
									<label for="user_collection_list"><?php _e( 'Select User Collection:', 'woocl' ); ?><span class="woo_cl_add_to_collection_error">*</span></label>
								</th>
								<td>
								<div class="woo-cl-post-select-wrap">
									<select name="user_collection_list" id="user_collection_list" data-placeholder="<?php _e( '-- Select --', 'woocl' );?>" class="wc-enhanced-select woo-cl-select-box woo-cl-icon-select">
									</select>
									</div>
									<span class="woo-cl-collection-loader woo-cl-post-collection-loader"><img src="<?php echo WOO_CL_IMG_URL . '/woo-cl-loader.gif'; ?>" alt="..." /></span>
									<div class="clear"></div>
									<span class="description"><?php _e( 'Select user collection in which you would like to add collection product.', 'woocl' );?></span>
									<div class="user_collection_list_error woo_cl_add_to_collection_error" style="display: none;"><?php echo sprintf( __( 'Please select %s.', 'woocl' ), woo_cl_get_label_singular() );?></div>
								</td>
							</tr>
								
							<!-- Product list -->
							<tr class="woo-cl-display-none woo-cl-product-list">
								<th scope="row">
									<label for="woo_cl_product_list"><?php _e( 'Select Product(s):', 'woocl' );?><span class="woo_cl_add_to_collection_error">*</span></label>		
								</th>
								<td>
									<div class="woo-cl-post-select-wrap">
										<select multiple class="woo_cl_add_product_id wc-product-search woo-cl-select-box" data-exclude="woo-cl-admin-pro-search" style="width: 95%;" id="woo_cl_add_product_coll" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'woocl' ); ?>" data-action="woocommerce_json_search_products_and_variations"></select>
									</div>
									<span class="woo-cl-collection-loader woo-cl-post-name-loader"><img src="<?php echo WOO_CL_IMG_URL . '/woo-cl-loader.gif'; ?>" alt="..." /></span>
									<span class="col_type_post_name_error woo_cl_add_to_collection_error" style="padding:0 10px;"></span>
									<div class="clear"></div>
									<span class="description"><?php _e( 'Select multiple product which you want to add product(s) into user collection.', 'woocl' );?></span>
									<div class="col_product_error woo_cl_add_to_collection_error" style="display: none;"> </div>
								</td>
							</tr>
							<!-- Add To Collection -->
							<tr class="woo-cl-display-none woo-cl-save-tr">
								<th scope="row"></th>
								<td>
									<div class="woo-cl-post-select-wrap">
									<?php echo apply_filters( 'woo_cl_admin_add_to_collection', '<input type="button" id="woo-cl-collection" class="button-primary" name="add_to_collection" value="' . __( 'Add To Collection', 'woocl' ) . '">' ); ?>
									</div>
									<span class="woo-cl-collection-loader woo-cl-save-loader"><img src="<?php echo WOO_CL_IMG_URL . '/woo-cl-loader.gif'; ?>" alt="..." /></span>
									<div class="clear"></div>
								</td>
							</tr>

							<!-- Success Message -->
							<tr class="woo-cl-success-msg-tr">
								<th scope="row"></th>
								<td>
									<div class="woo_cl_msg_info">
										<div class="woo-cl-success-message"></div>
									</div>
									<div class="clear"></div>
								</td>
							</tr>

						</tbody>
					</table>
				  </form>
				</div><!-- .inside -->
			</div><!-- #general -->
		</div><!-- .meta-box-sortables ui-sortable -->
	</div><!-- .metabox-holder -->
</div><!-- #woo-cl-general -->
</div><!--end .wrap-->