<?php 

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Add Product to Edited collection
 *
 * This is the code for the pop up editor
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
?>

<div class="woo-cl-addpop-content">

	<div class="woo-cl-header-popup woocl-clearfix">
		<div class="woo-cl-popup-header-title"><?php _e( 'Add a Product', 'woocl' );?></div>
		<div class="woo-cl-popup-close"><a href="javascript:void(0);" class="woo-cl-addpop-close-btn"><img src="<?php echo WOO_CL_IMG_URL;?>/close_icon.png"></a></div>
	</div>
	<div class="woo-cl-popup-container ">

		<select class="woo_cl_add_product_id wc-product-search woo-cl-select-box" data-exclude="woo-cl-admin-pro-search" style="width: 95%;" id="woo_cl_add_product_coll" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'woocl' ); ?>" data-action="woocommerce_json_search_products_and_variations"></select>

		<div class="woo_cl_hide woo_cl_msg_info"></div>

		<div id="woo_cl_add_product_wrap">
			<input type="button" id="woo_cl_add_product_this_coll" class="button button-primary" value="<?php _e( 'Add', 'woocl' );?>">
	 		<img src="<?php echo WOO_CL_IMG_URL.'/woo-cl-loader.gif';?>" class="woo_cl_hide woo_cl_loader_img">
	 		<span class="woo_cl_hide woocl_adding_msg"><?php _e('Adding...','woocl');?></span>
		</div>
	</div>	
	
</div><!--.woo-cl-popup-content-->
<!--<div class="woo-cl-popup-overlay"></div>.woo-cl-popup-overlay-->