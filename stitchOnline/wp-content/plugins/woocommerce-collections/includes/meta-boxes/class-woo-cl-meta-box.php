<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Meta Box Class
 *
 * Handles MetaBox related functionality.
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
class Woo_Cl_Meta_Box {
	
	public $model;
	
	public function __construct() {		
	
		global $woo_cl_model;
				
		$this->model = $woo_cl_model;
	}
	
	/*
	 * Add metabox on Collection post type
	 *
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	function woo_cl_add_meta_box() {
		
		$post_type = WOO_CL_POST_TYPE_COLL;
		
		// add meta box for Product Details
		add_meta_box( 'woo_cl_products_meta', __( 'Products', 'woocl'), array($this, 'woo_cl_products_meta_box_show'), $post_type, 'normal', 'high' );			

		// add meta box for Featured Collection
		add_meta_box( 'woo_cl_featured_coll_meta', sprintf( __( 'Featured %s', 'woocl'), woo_cl_get_label_singular() ), array($this, 'woo_cl_featured_coll_meta_box_show'), $post_type, 'side', 'default' );
		
		do_action('woo_cl_add_replace_variation_feature');
	}

	
	/**
	 * Display Meta Box for Featured Collection
	 * 
	 * Handles to display meta box for Featured Collection
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_featured_coll_meta_box_show() {

		global $post;

		$prefix = WOO_CL_META_PREFIX;
		
		$featured_checked	= get_post_meta( $post->ID, $prefix . 'featured_coll', true );
		
		$checked = ( isset( $featured_checked ) && $featured_checked == 'yes' ) ? 'checked="checked"' : '';
		
		echo '<table class="woocl-featured-wrapper form-table">
				<tbody>
					<tr valign="top">
						<th class="woocl-label-box">
							<label for="'.$prefix.'featured_coll">'.__('Featured ?', 'woocl').'</label>
						</th>
						<td>
							<input type="checkbox" '.$checked.' id="'.$prefix.'featured_coll" name="'.$prefix.'featured_coll" value="yes"><br /><span class="description">'.sprintf( __('Enable this %1$s as featured %1$s.','woocl'), woo_cl_get_label_singular() ).'</span>
						</td>
					</tr>
				 </tbody>
				</table>';
	}

	/**
	 * Save featured collection Meta
	 * 
	 * Handles to save featured collection meta
	 *
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_featured_coll_save_meta( $post_id ) {
		
		global $post_type;
		
		$prefix    = WOO_CL_META_PREFIX;
		
		$post_type_object = get_post_type_object( WOO_CL_POST_TYPE_COLL );

		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )                // Check Autosave
		|| ( ! isset( $_POST['post_ID'] ) || $post_id != $_POST['post_ID'] )        // Check Revision
		|| ( $post_type != WOO_CL_POST_TYPE_COLL ) // Check if current post type is supported.
		|| ( ! current_user_can( $post_type_object->cap->edit_post, $post_id ) ) )       // Check permission
		{
		  return $post_id;
		}

		if( !isset( $_POST[ $prefix . 'featured_coll' ] ) ) {//check if featured collection checked
			
			// Update Custom Featured when not checked
			update_post_meta( $post_id, $prefix . 'featured_coll', '' );
		
		} else {
		
			// Update Custom Featured checked
			update_post_meta( $post_id, $prefix . 'featured_coll', $_POST[ $prefix . 'featured_coll' ] );
		}
	}
	
	/**
	 * Display Meta Box for Product details
	 * 
	 * Handles to display meta box for Product details
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_products_meta_box_show() {
		
		global $post;

		$prefix = WOO_CL_META_PREFIX;
		
		$coll_id = $post->ID;
		
		echo $this->model->woo_cl_get_admin_products_by_collection_id( $coll_id );
		
	}
	
	/**
	 * Adding Hooks
	 *
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function add_hooks() {
		
		// add action for add metaboxes
		add_action( 'add_meta_boxes', array( $this, 'woo_cl_add_meta_box' ) );

		// add action to save custom meta
		add_action( 'save_post', array( $this, 'woo_cl_featured_coll_save_meta' ) );		
	}
}