<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Shortcode Class
 * 
 * Handles adding Shortcode functionality to the front pages.
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
class Woo_Cl_Shortcodes {

	public $model;

	public function __construct() {

		global $woo_cl_model;

		$this->model	= $woo_cl_model;
	}

	/**
	 * Woocommerce Collection Shortcode
	 * 
	 * Handles to display collection shortcode content
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_collection_shortcode( $atts, $content ) {

		global $wp;

		//Get collection view page end point
		//$view_collection_args	= get_option( 'cl_view_collection_text' );
		//$view_collection_args	= trim( $view_collection_args ) != '' ? $view_collection_args : 'view-collection';

		$view_collection_args	= woo_cl_get_slug( 'view' );

		//Get collection edit end point
		//$edit_collection_args	= get_option( 'cl_edit_collection_text' );
		//$edit_collection_args	= trim( $edit_collection_args ) != '' ? $edit_collection_args : 'edit-collection';

		$edit_collection_args	= woo_cl_get_slug( 'edit' );

		//Get user endpoint
		//$collection_user_args	= get_option( 'cl_collection_author_text' );
		//$collection_user_args	= trim( $collection_user_args ) != '' ? $collection_user_args : 'collection-author';

		$collection_user_args	= woo_cl_get_slug( 'author');

		//if query vars is view page
		if( isset( $wp->query_vars[$view_collection_args] ) ) {

			$collction_id	= $wp->query_vars[$view_collection_args];
		
			$content		= $this->woo_cl_collection_product_lists( $atts, $content, $collction_id );

		} else if( isset( $wp->query_vars[$edit_collection_args] ) ) {

			$collction_id	= $wp->query_vars[$edit_collection_args];
			$content		= $this->woo_cl_collection_edit_page( $atts, $content, $collction_id );

		} else {

			$woo_cl_user	= isset( $wp->query_vars[$collection_user_args] ) ? $wp->query_vars[$collection_user_args] : '';
			$content		= $this->woo_cl_collection_lists( $atts, $content, $woo_cl_user );

		}
		
		return apply_filters( 'woo_cl_collection_shortcode', $content, $atts, $content );
	}

	/**
	 * Replace Shortcode with Custom
	 * Content for Collection Item Listing
	 *
	 * Handles to display Collection Item Listing Page
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_collection_product_lists( $atts, $content, $coll_id ) {

		global $current_user;
		
		// Declare required variables
		$cl_get_collection_orderby = $cl_get_collection_order = $cl_get_collection_array = '';

		//Get collection view page end point
		$woo_cl_view_slug	= get_option( 'cl_view_collection_text' );
		$woo_cl_view_slug	= trim( $woo_cl_view_slug ) != '' ? $woo_cl_view_slug : 'view-collection';

		//Get current user Id
		$user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';

		//Get all Messages
		$messages	= woo_cl_messages();

		// get collection author id
		$coll_authorid	= get_post_field( 'post_author', $coll_id );
		
		// Get Collection Item List by option
		$cl_sort_collection_item_by = get_option( 'cl_sort_collection_item_by' );
		
		// Explode setting for getting 'Orderby' and 'Order' settings
		$cl_get_collection_array 	= !empty ( $cl_sort_collection_item_by ) ? explode( '-', $cl_sort_collection_item_by ) : '';
		
		if ( ! empty ( $cl_get_collection_array ) && is_array ( $cl_get_collection_array ) ) {
			$cl_get_collection_orderby 	= !empty ( $cl_get_collection_array[0] ) ? $cl_get_collection_array[0] : '';
			$cl_get_collection_order 	= !empty ( $cl_get_collection_array[1] ) ? strtoupper( $cl_get_collection_array[1] ) : '';
		}

		if( empty( $coll_id ) ) { //check if collection id empty then error message

			$content .= '<div class="woo_cl_errors woo_cl_msgs"><p class="woo_cl_message">';
			$content .= isset( $messages['empty_coll_id'] ) ? $messages['empty_coll_id'] : '';
			$content .= '</p></div>';

			return $content;
		}

		//Get Post Data
		$result	= get_post( $coll_id );

		//Check if collection exists OR private for guest
		if( empty( $result ) || ( !empty( $result->post_author ) && $result->post_author != $user_ID && !empty( $result->post_status ) && $result->post_status == 'private' ) ) {//If result not empty

			$content .= '<div class="woo_cl_errors woo_cl_msgs"><p class="woo_cl_message">';
			$content .= isset( $messages['coll_not_available'] ) ? $messages['coll_not_available'] : '';
			$content .= '</p></div>';

			return $content;
		}

		$per_page_count	= woo_cl_get_collection_per_page_count();

		$args = array(
						//'author' 			=> $user_ID,
						'post_parent' 		=> $coll_id,
				);

		$all_coll_item_count_arg	= array_merge( $args, array( 'getcount' => 1, 'posts_per_page' => -1 ) );
		$all_coll_item_count		= $this->model->woo_cl_get_collection_items( $all_coll_item_count_arg );

		//Get Collection Items
		$args['posts_per_page']	= $per_page_count;
		$args['paged']			= 1;
		$args['orderby']        = !empty( $cl_get_collection_orderby ) ? $cl_get_collection_orderby : 'date'; // Set Orderby as of Global settings, 'date' if not set
		$args['order']			= !empty( $cl_get_collection_order ) ? $cl_get_collection_order : 'DESC'; // Set order as of Global settings, 'DESC' if not set

		//Change order and order by if sorting
		if( !empty( $_GET['sort'] ) ) {

			// Explode setting for getting 'Orderby' and 'Order' settings
			$cl_get_collection_array 	= !empty ( $_GET['sort'] ) ? explode( '-', $_GET['sort'] ) : '';
			$cl_get_collection_orderby 	= !empty ( $cl_get_collection_array[0] ) ? $cl_get_collection_array[0] : '';
			$cl_get_collection_order 	= !empty ( $cl_get_collection_array[1] ) ? strtoupper( $cl_get_collection_array[1] ) : '';
			$args['orderby']        = !empty( $cl_get_collection_orderby ) ? $cl_get_collection_orderby : 'date'; // Set Orderby as of Global settings, 'date' if not set
			$args['order']			= !empty( $cl_get_collection_order ) ? $cl_get_collection_order : 'DESC'; // Set order as of Global settings, 'DESC' if not set
		}
		//Get collection data by search if searching
		if( !empty( $_GET['search'] ) ){
			$args['s']	= $_GET['search'];
		}
	
		$coll_items = $this->model->woo_cl_get_collection_items( $args );
		
		ob_start();
		do_action( 'woo_cl_collection_item_listing', $coll_items, $coll_id, $all_coll_item_count );
		$content .= ob_get_clean();

		return $content;
	}

	/**
	 * Replace Shortcode with Custom
	 * Content for Edit Collection Item Page
	 * 
	 * Handles to display Collection Item Edit Page
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_collection_edit_page( $atts, $content, $coll_id ) {

		global $current_user;

		// Declare required variables
		$cl_get_collection_orderby = $cl_get_collection_order = $cl_get_collection_array = '';
		
		//Get all Messages
		$messages	= woo_cl_messages();

		//Get collection edit end point
		$woo_cl_edit_slug	= get_option( 'cl_edit_collection_text' );
		$woo_cl_edit_slug	= trim( $woo_cl_edit_slug ) != '' ? $woo_cl_edit_slug : 'edit-collection';

		//Get current user Id
		$user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';

		// get collection author id
		$coll_authorid	= get_post_field( 'post_author', $coll_id );
		
		// Get Collection Item List by option
		$cl_sort_collection_item_by = get_option( 'cl_sort_collection_item_by' );
		
		// Explode setting for getting 'Orderby' and 'Order' settings
		$cl_get_collection_array 	= !empty ( $cl_sort_collection_item_by ) ? explode( '-', $cl_sort_collection_item_by ) : '';
		
		if ( ! empty ( $cl_get_collection_array ) && is_array ( $cl_get_collection_array ) ) {
			$cl_get_collection_orderby 	= !empty ( $cl_get_collection_array[0] ) ? $cl_get_collection_array[0] : '';
			$cl_get_collection_order 	= !empty ( $cl_get_collection_array[1] ) ? strtoupper( $cl_get_collection_array[1] ) : '';
		}

		if( empty( $coll_id ) ) { //check if collection id empty then error message

			$content .= '<div class="woo_cl_errors woo_cl_msgs"><p class="woo_cl_message">';
			$content .= isset( $messages['empty_coll_id'] ) ? $messages['empty_coll_id'] : '';
			$content .= '</p></div>';

			return $content;
		}

		if ( empty( $user_ID ) && !woo_cl_is_users_list( $coll_id ) ) { // check if user is not logged in, then error message

			//Error content
			$content .= '<div class="woo_cl_errors woo_cl_msgs"><p class="woo_cl_message">';
			$content .= isset( $messages['must_login'] ) ? $messages['must_login'] : '';
			$content .= '</p></div>';
						
			return $content;
		}
		
		if( !woo_cl_user_can_update_collection($coll_id) ) {
			
			//Error content
			$content .= '<div class="woo_cl_errors woo_cl_msgs"><p class="woo_cl_message">';
			$content .= isset( $messages['prevent_edit_access'] ) ? $messages['prevent_edit_access'] : '';
			$content .= '</p></div>';

			return $content;
		}
		
		if ( $coll_authorid != $user_ID ) { // check if author not same, then error message
			
			//Error content
			$content .= '<div class="woo_cl_errors woo_cl_msgs"><p class="woo_cl_message">';
			$content .= isset( $messages['prevent_edit_access'] ) ? $messages['prevent_edit_access'] : '';
			$content .= '</p></div>';

			return $content;
		}
		
		//Get Post Data
		$result	= get_post( $coll_id );
		
		if( empty( $result ) ) {//If result not empty
			
			$content .= '<div class="woo_cl_errors woo_cl_msgs"><p class="woo_cl_message">';
			$content .= isset( $messages['coll_not_available'] ) ? $messages['coll_not_available'] : '';
			$content .= '</p></div>';

			return $content;
		}

		//collection items argument
		$args = array(
						'author' 		=> $user_ID,
						'post_parent' 	=> $coll_id,
						'orderby'		=> !empty( $cl_get_collection_orderby ) ? $cl_get_collection_orderby : 'date', // Set Orderby as of Global settings, 'date' if not set
						'order'			=> !empty( $cl_get_collection_order ) ? $cl_get_collection_order : 'DESC', // Set order as of Global settings, 'DESC' if not set
				);

		//Get Collection Items
		$coll_items = $this->model->woo_cl_get_collection_items( $args );

		ob_start();
		do_action( 'woo_cl_edit_collection', $coll_items, $coll_id );
		$content .= ob_get_clean();

		return $content;
	}
	
	/**
	 * Replace Shortcode with Custom Content for Collection Listing
	 * 
	 * Handles to display Collection listing
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_collection_lists( $atts, $content, $woo_cl_user ) {
		
		global $current_user, $wp_query;
		
		// Declare required variables
		$cl_get_collection_orderby = $cl_get_collection_order = $cl_get_collection_array = '';

		//Get meta prefix
		$prefix = WOO_CL_META_PREFIX;

		//Get login user ID
		$login_uid	= get_current_user_id();

		$per_page_count	=  woo_cl_get_collection_per_page_count();
		$args		= array();
		$user_ID	= '';

		//Get user endpoint
		$woo_cl_user_slug	= get_option( 'cl_collection_author_text' );
		$woo_cl_user_slug	= trim( $woo_cl_user_slug ) != '' ? $woo_cl_user_slug : 'collection-author';
		
		// Get option for collection sorting
		$cl_get_collection_sort_by = get_option( 'cl_sort_collection_by' );
		
		// Explode setting for getting 'Orderby' and 'Order' settings
		$cl_get_collection_array 	= !empty ( $cl_get_collection_sort_by ) ? explode( '-', $cl_get_collection_sort_by ) : '';
		
		if ( ! empty ( $cl_get_collection_array ) && is_array ( $cl_get_collection_array ) ) {
			$cl_get_collection_orderby 	= !empty ( $cl_get_collection_array[0] ) ? $cl_get_collection_array[0] : '';
			$cl_get_collection_order 	= !empty ( $cl_get_collection_array[1] ) ? strtoupper( $cl_get_collection_array[1] ) : '';
		}

		//Get all Messages
		$messages	= woo_cl_messages();

		//Get guest token
		$cl_token	= woo_cl_get_list_token();
		
		if( $woo_cl_user == 'guest' && !empty( $cl_token ) ) {

			//Get status
			$status	= ! is_user_logged_in() ? array( 'publish', 'private' ) : array( 'publish' );

			//Arguments for get guest collections
			$args	= array(
							'post_status'	=> $status,
							'meta_query'	=> array(
													array( 
														'key' 	=> $prefix.'cl_token', 
														'value' => $cl_token
														)
													)
						);

		} elseif ( !empty( $woo_cl_user ) ) {
			
			//Get user id
			$user_ID	= $this->model->woo_cl_get_userid_by_username( $woo_cl_user );
			
			if( empty( $user_ID ) ) {
				
				//Get User Not Available Message
				$user_not_avail_msg	= isset( $messages['user_not_available'] ) ? $messages['user_not_available'] : '';
				
				ob_start();
				echo '<div class="woo_cl_errors woo_cl_msgs"><p class="woo_cl_message">'.$user_not_avail_msg.'</p></div>';
				$content .= ob_get_clean();
				
				return $content;
			}
		}

		if( !empty( $user_ID ) ) {

			//Get status
			$status	= $user_ID == $login_uid ? array( 'publish', 'private' ) : array( 'publish' );

			$args = array(
							'author' 		 => $user_ID,
							'post_status'	 => $status							
						);
		}

		if( empty( $user_ID ) && $woo_cl_user != 'guest' ) {

			//Arguments for guest user collection
			$args = array(
							/*'meta_query'=> array(
												array(
														'key' 		=> $prefix.'cl_token',
														'compare'	=> 'NOT EXISTS'
													)
												),*/
							'meta_query'	=> array(
													'relation' => 'OR',
													array(
															'key' 		=> $prefix.'cl_token',
															'compare'	=> 'NOT EXISTS'
														),
													array( 
															'key' 	=> $prefix.'cl_token', 
															'value' => $cl_token
														)
													)
						);
		}
		
		$args = apply_filters( 'woo_cl_collection_listing_args' , $args);

		$all_clll_count_arg	= array_merge( $args, array( 'getcount' => 1, 'posts_per_page' => -1 ) );
		$all_coll_count		= $this->model->woo_cl_get_collections( $all_clll_count_arg );
		
		$args['posts_per_page']	= $per_page_count;
		$args['paged']			= 1;
		$args['orderby']        = !empty( $cl_get_collection_orderby ) ? $cl_get_collection_orderby : 'date'; // Set Orderby as of Global settings, 'date' if not set
		$args['order']			= !empty( $cl_get_collection_order ) ? $cl_get_collection_order : 'DESC'; // Set order as of Global settings, 'DESC' if not set
		
		//Change order and order by if sorting
		if( !empty( $_GET['sort'] ) ) {

			// Explode setting for getting 'Orderby' and 'Order' settings
			$cl_get_collection_array 	= !empty ( $_GET['sort'] ) ? explode( '-', $_GET['sort'] ) : '';
			$cl_get_collection_orderby 	= !empty ( $cl_get_collection_array[0] ) ? $cl_get_collection_array[0] : '';
			$cl_get_collection_order 	= !empty ( $cl_get_collection_array[1] ) ? strtoupper( $cl_get_collection_array[1] ) : '';
			$args['orderby']        = !empty( $cl_get_collection_orderby ) ? $cl_get_collection_orderby : 'date'; // Set Orderby as of Global settings, 'date' if not set
			$args['order']			= !empty( $cl_get_collection_order ) ? $cl_get_collection_order : 'DESC'; // Set order as of Global settings, 'DESC' if not set
		}

		//Get collection data by search if searching
		if( !empty( $_GET['search'] ) ){
			$args['s']	= $_GET['search'];
		}

		//Get Collection of Page 1
		$collections	= $this->model->woo_cl_get_collections( $args );

		//Start Content Display
		ob_start();
		
		do_action( 'woo_cl_collection_listing', $collections, $user_ID, $all_coll_count );		
		
		$content .= ob_get_clean();
		
		return $content;
	}

	/**
	 * Replace Shortcode with Custom Content for Featured Collection Listing
	 * 
	 * Handles to display Featured Collection listing
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_featured_coll_lists( $atts, $content ) {

		global $current_user, $wp_query;

		if( is_singular() ) {

			//content to replace with your content and with attributes
			extract( shortcode_atts( array(
										
											'fcolls_title'	=>	sprintf( __('Featured %s','woocl'), woo_cl_get_label_plural() ),
											'no_of_colls'	=>	"10",
											'showprivate'	=>  false
									), $atts ) );

			$prefix     = WOO_CL_META_PREFIX;

			//Get current user Id
			$user_ID	= isset( $current_user->ID ) ? $current_user->ID : '';

			//argument for get featured collections	
			$args = array(
							'posts_per_page'=> $no_of_colls,
							'orderby'		=> 'post_date',
							'order'			=> 'DESC',
							'meta_query'	=> array(
														array( 
															'key' => $prefix.'featured_coll',
															'value' => 'yes'
															)
													)
						);

			//check if showprivate is true then arguments for private collection
			if( $showprivate ) {
				$args['post_status']	= array( 'publish', 'private' );
			}

			//Get Collection data
			$collections	= $this->model->woo_cl_get_collections( $args );

			ob_start();
			do_action( 'woo_cl_featured_collection_listing', $collections, $user_ID, $fcolls_title );
			$content .= ob_get_clean();
		}

		return $content;
	}
	
	/**
	 * Add To Collection Button Shortcode
	 * 
	 * Handle to display add to collection button using shortcode
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function woo_cl_add_to_collection_button_shortcode( $atts, $content ) {
		
		global $post;
		
		extract( shortcode_atts( array(
					'id' 		=> $post->ID,
					'text' 		=> '',
					'icon'		=> '',
					'type'		=> '',
				), $atts, 'woo_cl_add_to_collection' )
		);
		
		//set arguments
		$shortcode_args	= array( 'product_id' => $id );

		// Get button class based on option
		if( $type == 'default' ) {
			$button_class = (is_single()) ? 'alt button' : 'button';
		} else if( $type == 'button' ) {
			$button_class = 'woocl-button';
		}
		$shortcode_args['button_class'] = ( isset( $button_class ) ) ? $button_class : '';

		if( !empty( $text ) ) { //if text is not empty
			$shortcode_args['add_to_coll_btn'] = $text;
		}
		
		if( !empty( $icon ) ) { //if icon is not empty
			$shortcode_args['add_to_coll_btnicon'] = $icon;
		}
		
		$shortcode_args['shortcode']	= true;
		
		$shortcode_args	= apply_filters( 'woo_cl_add_to_collection_shortcode', $shortcode_args, $id );
		
		wp_enqueue_style( 'woo-cl-coll-dropdown' ); // Enqueue  dropdown style
		wp_enqueue_style( 'woo-cl-popup' );
		wp_enqueue_style( 'font-awesome.min' ); // Enqueue font awesome Icons styles
		wp_enqueue_script( 'woo-cl-popup-script' ); // Enqueue script on for add to collection popup
		wp_enqueue_script( 'woo-cl-dropdown-script' ); // Enqueue script on for dropdown
		wp_enqueue_script( 'woo-cl-variation-script' );
		
		ob_start();
		woo_cl_add_to_collection_button ( $shortcode_args );
		$content .= ob_get_clean();
		
		return $content;
	}
	
	/**
	 * My Collection Shortcode
	 * 
	 * Handles to my collection shortcode
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.2
	 */
	public function woo_cl_my_collections_shortcode( $atts, $content ) {
		
		extract( shortcode_atts( array(
					'collection_title'	=> '',
					'show_count'		=> 'false',
					'enable_see_all'	=> 'false',
					//'per_page'		=> '',
					'user_id'			=> '',
				), $atts, 'woo_cl_my_collections' )
		);
		
		// Defaults collection argument
		$defaults	= apply_filters( 'woo_cl_my_collections_shortcode_defaults', 
							array(
									'collection_title'	=> $collection_title,
									'per_page'			=> '',
									'show_count'		=> $show_count,
									'enable_see_all' 	=> $enable_see_all,
									'user_id'	 	 	=> $user_id,
								)
							);
		
		// display my recent collections
		ob_start();
		woo_cl_my_collections( $defaults );
		$content .= ob_get_clean();
		
		return apply_filters( 'woo_cl_my_collections_shortcode_content', $content, $atts );
	}
	
	/**
	 * Adding Hooks
	 * 
 	 * @package WPWeb WooCommerce Collections
 	 * @since 1.0.0
	 */
	public function add_hooks() {
		
		//replace shortcodes with custom content or HTML for Collection Listing
		add_shortcode( 'woo_cl_collections', array( $this, 'woo_cl_collection_shortcode') );
		
		//replace shortcodes with custom content or HTML for Featured Collection Listing
		add_shortcode( 'woo_cl_featured_collections', array( $this, 'woo_cl_featured_coll_lists' ) );
		
		//replace shortcodes with add to collection button
		add_shortcode( 'woo_cl_add_to_collection', array( $this, 'woo_cl_add_to_collection_button_shortcode' ) );
		
		//replace shortcodes with user's collection
		add_shortcode( 'woo_cl_my_collections', array( $this, 'woo_cl_my_collections_shortcode' ) );
	}
}