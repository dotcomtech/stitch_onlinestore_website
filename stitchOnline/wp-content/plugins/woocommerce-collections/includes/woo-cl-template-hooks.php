<?php

/**
 * Template Hooks
 * 
 * Handles to add all hooks of template
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
	
	/********************** Template Hooks For add to collection button **************************/
	
	// add action to display add to collection button html
	add_action( 'woo_cl_add_to_collection_button', 'woo_cl_add_to_collection_button_html', 5 );
	
	/********************** Template Hooks For add to collection popup **************************/
	
	//add_action to load add to collection / item details popup wrapper
	add_action( 'woo_cl_add_to_collection_popup', 'woo_cl_add_to_collection_popup_html', 5 );
	
	//add_action to load add to collection popup header
	add_action( 'woo_cl_add_to_collection_popup_container', 'woo_cl_popup_title', 5, 2 );
	
	//add_action to load add to collection popup content
	add_action( 'woo_cl_add_to_collection_popup_container', 'woo_cl_popup_content', 10, 2 );
	
	//add_action to load add to collection popup footer
	add_action( 'woo_cl_add_to_collection_popup_container', 'woo_cl_popup_footer', 15, 2 );
	
	//add_action to load add to collection popup for success
	add_action( 'woo_cl_add_to_collection_success_popup', 'woo_cl_popup_success', 5, 3 );
	
	/********************** Template Hooks For collection listing  **************************/
	
	//add_action to load collection listing wrapper
	add_action( 'woo_cl_collection_listing', 'woo_cl_collection_list_wrapper', 5, 4 );
	
	//add_action to load collection content in loop
	add_action( 'woo_cl_collection_listing_loop', 'woo_cl_collection_listing_loop_content', 5, 2 );
	
	//add_action to load collection listing
	add_action( 'woo_cl_collection_listing_data', 'woo_cl_collection_list_content', 5, 3 );
	
	/********************** Template Hooks For collection items listing **************************/
	
	//add_action to load collection listing content
	add_action( 'woo_cl_collection_item_listing', 'woo_cl_collection_item_list_wrapper', 5, 3 );
	
	//add_action to load collection content in loop
	add_action( 'woo_cl_collection_item_listing_loop', 'woo_cl_collection_item_listing_loop_content', 5, 3 );
	
	//add_action to load collection product listing content
	add_action( 'woo_cl_collection_product_listing_data', 'woo_cl_product_list_content', 5, 4 );
	
	/********************** Template Hooks For collection & their product edit **************************/
	
	//add_action to load edit a collection wrapper
	add_action( 'woo_cl_edit_collection', 'woo_cl_edit_collection_wrapper', 5, 3 );
	
	//add_action to load delete a collection popup
	add_action( 'woo_cl_edit_collection', 'woo_cl_delete_collection_popup_content', 10, 2 );
	
	//add_action to load cover image popup
	add_action( 'woo_cl_edit_collection', 'woo_cl_cover_image_popup_content', 15, 2 );
	
	//add_action to load collection status popup
	add_action( 'woo_cl_edit_collection', 'woo_cl_status_popup_content', 20, 2 );
	
	//add_action to load edit a collection content
	add_action( 'woo_cl_edit_collection_data', 'woo_cl_edit_collection_content', 5, 3 );
	
	//add_action to load collection item details popup ( using ajax )
	add_action( 'woo_cl_collection_item_details_container', 'woo_cl_item_details_popup_content', 5, 3 );
	
	/********************** Template Hooks For featured collection Listing **************************/
	
	//add_action to load featured collection listing wrapper
	add_action( 'woo_cl_featured_collection_listing', 'woo_cl_featured_collection_list_wrapper', 5, 3 );
	
	//add_action to load featured collection listing
	add_action( 'woo_cl_featured_collection_listing_data', 'woo_cl_featured_collection_list_content', 5, 2 );
	
	/********************** Template Hooks For Social Share Button **************************/
	
	//add_action to disaply share buttons on collection item listing page
	add_action( 'woo_cl_collection_share_buttons', 'woo_cl_social_share_buttons', 5 );
	
	//add_action to social button for facebook
	add_action( 'woo_cl_social_buttons', 'woo_cl_social_facebook', 10 );
	
	//add_action to social button for twitter
	add_action( 'woo_cl_social_buttons', 'woo_cl_social_twitter', 15 );
	
	//add_action to social button for google
	add_action( 'woo_cl_social_buttons', 'woo_cl_social_google', 20 );
	
	//add_action to social button for linkedin
	add_action( 'woo_cl_social_buttons', 'woo_cl_social_linkedin', 25 );
	
	//add_action to social button for email
	add_action( 'woo_cl_social_buttons', 'woo_cl_share_via_email', 30 );
	
	//add_action to email share popup
	add_action( 'woo_cl_social_buttons', 'woo_cl_share_via_email_popup', 30 );
	
	//add_action to email share success popup
	add_action( 'woo_cl_social_buttons', 'woo_cl_share_via_email_success_popup', 30 );