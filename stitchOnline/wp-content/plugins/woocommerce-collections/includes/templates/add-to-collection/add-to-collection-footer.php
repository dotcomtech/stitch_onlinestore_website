<?php

/**
 * Template For Add To Collection Footer
 * 
 * Handles to return design add to collection Footer popup
 * 
 * Override this template by copying it to 
 * yourtheme/woocommerce/woocommerce-collections/add-to-collection/add-to-collection-footer.php
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
	<div class="woocl-footer">
		<?php if( $cl_front_coll_privacy != 'yes' ) :?>
		<div class="woocl-myicon">
			<i class="woocl-icon"></i>
		</div>
		<div class="woocl-notes"><?php 
			if( $cl_new_coll_privacy == 'private' ) {
				echo sprintf( __( 'Your %2$s will be private on your profile. You can make them public from edit %1$s page.', 'woocl' ), woo_cl_get_label_singular(), woo_cl_get_label_plural() );
			} else {
				echo sprintf( __( 'Your %2$s will be public on your profile. You can make them private from edit %1$s page.', 'woocl' ), woo_cl_get_label_singular(), woo_cl_get_label_plural() );
			}?>
		</div>
	<?php endif;?>
		<div class="woocl-acts">
			<a class="woocl-close"><?php _e('Cancel', 'woocl');?></a>
			<button type="button" class="clnwbtn woocl-btn btn-prim" id="woo-cl-addtocoll"><?php echo sprintf( __('Add to %s', 'woocl'), woo_cl_get_label_singular() ); ?></button>
			<img id="woo_cl_add_coll_loader" src="<?php echo WOO_CL_IMG_URL.'/woo-cl-loader.gif' ?>"/>
			<span class="woocl_adding_msg">
			<?php _e(' Adding...','woocl');?>
			</span>
		</div>
	</div><!-- end of footer-->