<?php

/**
 * Template For Add To Collection Button
 * 
 * Handles to return design add to collection Button
 * 
 * Override this template by copying it to 
 * yourtheme/woocommerce/woocommerce-collections/add-to-collection/add-to-collection.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<!-- Add to collection Popup Start -->
<div class="woocl-modal-wrapper">
	<div class="woocl-middle">
		<div class="woocl-top">
			<div class="woocl-close woocl-close-style"></div>
		</div>
		
		<!-- Main Add Collection Popup Content Here -->
		<div class="woocl-inner">
		</div><!-- end of inner-->
		
		<div class="woocl-inner-loader">
			<img id="woo_cl_popup_loader" src="<?php echo WOO_CL_IMG_URL.'/woo-cl-loader-big.gif'; ?>"/>
		</div>
	</div><!-- end of mid-->
	<div class="woocl-overlay"></div>
</div>	<!-- end of modal-wrapper-->
<!-- Add to collection Popup End -->

<?php // below is for success message popup ?>
<!-- Success Message For Collection Popup Start -->
<div class="woocl-modal-imageadd-inwrapper">
	<div class="woocl-modal-addwrapper">
		<div class="woocl-imageadd-middlepart">
			<div class="woocl-wrapper">
				<div class="woocl-border">
					<div class="woocl-topper">
						<div class="woocl-closer"></div>
					</div><!-- end of top -->
				</div><!-- end of wrapper -->
				
				<!-- Main Success Popup Content Here -->
				<div class="woocl-innerdata">
				</div><!-- end .woocl-innerdata-->
				
				<div class="clear"></div>
			</div> <!-- end of border -->
		</div>	<!-- end of middlepart-->
	</div><!-- end of modal-nextwrapper -->
	<div class="woocl-overlay"></div>
</div>	<!-- end of modal-inwrapper -->
<!-- Success Message For Collection Popup End -->