<?php

/**
 * Template For Add To Collection Content
 * 
 * Handles to return design add to collection Content popup
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/add-to-collection/add-to-collection-content.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woo_cl_model;

//Get meta prefix
$prefix = WOO_CL_META_PREFIX;

//Get Placeholder of collection
$coll_placeholder = sprintf( __('Give your %s title.', 'woocl'), woo_cl_get_label_singular() );
?>
		
<div class="woocl-left-part">
	<div class="woocl-viewport">
		 <img src="<?php echo $product_imgurl; ?>" alt="image" />
	</div>
</div><!-- end of left-part -->

<input type="hidden" value="<?php echo $product_id;?>" class="woo-cl-collproduct_id">

<div class="woocl-right-part">
	<div class="woocl-colwItem"><p><?php echo $product_disc;?></p>
		<?php if( !empty( $product_variation ) ) {
			echo '<div class="woocl-item-variation">' . $product_variation . '</div>';
		}?>
	</div>
	<div class="woocl-dropdown">
		<select name="woo-cl-coll_list" id="woo-cl-coll_list" class="woocl-custom-options">
			<option value=""><?php _e('Please Select', 'woocl');?></option>
			<?php
				foreach( $collections_data as $collection ) { 
					
					echo '<option '.$collection['option_element'].$collection['icon_class'].' value="'. $collection['ID'] .'">'. $collection['post_title'] .'</option>';
			 } ?>
		</select>
		<div id="woo-cl-atc-msg"></div>
	</div><!-- end of selection-->
	<?php do_action( 'woo_cl_add_to_collection_popup_select_collection_after', $user_ID, $collections_data ); ?>
	<div class="woocl-colwcreate" >
			<table class="woocl-colwCreateFields">
				<tbody>
					<tr>
						<td class="woocl-txt woocl-row-area">
							<input id="woocl-colwname" type="text" value="<?php echo $coll_placeholder;?>" maxlength="68" class="woocl-colwName" onfocus="if (this.value == '<?php echo $coll_placeholder;?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php echo $coll_placeholder;?>';}" name="title">
						<?php if( isset( $cl_front_coll_privacy ) && $cl_front_coll_privacy=='yes' ) { ?>
							<div class="woocl-coll_privacy-dropdown">
								<select name="woo-cl-coll_privacy" id="woo-cl-coll_privacy" class="woocl-custom-options">
									<option value="public" <?php echo ($cl_new_coll_privacy =='public')? "selected=selected": "";?>><?php _e('Public', 'woocl');?></option>
									<option value="private" <?php echo ($cl_new_coll_privacy =='private')? "selected=selected": "";?>><?php _e('Private', 'woocl');?></option>
									
								</select>
							</div>
						<?php } ?>
							<div class="woocl-clnwbtn">
								<button id="woo-cl-createbtn" class="creBtn woocl-btn btn-m btn-prim" type="button"><?php _e('Create', 'woocl');?></button>
					 			<div class="woo_cl_loader">
							 		<img src="<?php echo WOO_CL_IMG_URL.'/woo-cl-loader.gif';?>" class="woo_cl_loader_img">
							 		<span class="woo_cl_updating_msg"><?php _e('Creating...','woocl');?></span>
					 			</div>
							</div>
						</td>
					</tr>
					<tr class="woocl-colwnoborder">
						<td colspan="2">
							<div id="woo-cl-create-msg"></div>
						</td>
					</tr>
				</tbody>
			</table>
	</div><!-- end of colwcreate-->
	
	<div class="woocl-textarea">
		<textarea rows="6" id="woo-cl-coll_disc" name="woo-cl-coll_disc" onfocus="if (this.value == '<?php _e('Enter a description. Say why you chose this item or why you love it.', 'woocl');?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e('Enter a description. Say why you chose this item or why you love it.', 'woocl');?>';}"><?php _e('Enter a description. Say why you chose this item or why you love it.', 'woocl');?></textarea>
	</div>
</div><!-- end of right-part-->