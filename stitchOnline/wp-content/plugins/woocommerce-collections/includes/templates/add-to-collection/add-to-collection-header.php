<?php

/**
 * Template For Add To Collection Header
 * 
 * Handles to return design add to collection Header popup
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/add-to-collection/add-to-collection-header.php
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

	<div class="woocl-header">
		<div class="woocl-title"><?php echo sprintf( __( 'Add this item to a %s', 'woocl'), woo_cl_get_label_singular() );?></div>
	</div><!-- end of header -->
	<div class="woocl-lnSeporator"></div>