<?php

/**
 * Template For Add To Collection Success popup
 * 
 * Handles to return design add to collection success popup
 * 
 * Override this template by copying it to 
 * yourtheme/woocommerce/woocommerce-collections/add-to-collection/success-message/add-to-collection-success.php
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="woocl-wrapper">
	<div class="woocl-collection-top">
		<div class="coll-success"> </div>
		<div class="coll-textmsg">
			<span><?php _e( 'Added to', 'woocl' );?></span>
			<span><?php
				if( empty( $cl_item_listsurl ) ) {?>
					<?php echo $coll_title;?><?php
				} else {?>
					<a title="<?php echo $coll_title;?>" href="<?php echo $cl_item_listsurl;?>">
						<?php echo $coll_title;?>
					</a><?php 
				} ?>
			</span>
		</div>
	</div>
	<div class="woocl-popup-img">
		<div class="woocl-image-display">
			<img src="<?php echo $product_imgurl; ?>" alt="image" />
		</div>
	</div>
	<div class="woocl-messagepart">
		<div class="woocl-msginfo">
			<p><?php echo $product_title;?></P>
		</div>
		<div class="woocl-shopping">
			<button type="button" class="woocl-addbtn" id="collectItem"><?php 
				_e('Continue Shopping','woocl');?>
			</button>
		</div>
	</div>
	<div class="clear"></div>	 
</div><!-- end of wrapper-->