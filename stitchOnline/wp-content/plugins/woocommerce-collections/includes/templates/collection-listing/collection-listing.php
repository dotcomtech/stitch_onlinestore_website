<?php

/**
 * Template For Collection Listing
 * 
 * Handles to return design collection Listing
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/collection-listing/collection-listing.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Check if follow collection author enabled
if( !empty( $coll_follow_args ) ) {
	do_action( 'wpw_fp_follow_author', $coll_follow_args );			
}

//if collection available
if( !empty( $collections ) ) {
	
	if( !empty( $single_user ) ) { //if user name pass by url
		
		$created_by_text = sprintf( '%s %s <a href="%s">%s</a>', woo_cl_get_label_plural(), __('by','woocl'), $author_coll_url, $single_user );
		
		echo '<div class="woocl-created-author">'.$created_by_text.'</div>';
	}?>

	<div class="woocl-coll-size woocl-clearfix">
		<div class="woocl-pull-left"><?php
			
			if( is_account_page() ) {?>
				<a href="<?php echo $my_coll_url;?>"><?php 
					echo sprintf( __( 'My %s', 'woocl' ), woo_cl_get_label_plural() );
				?></a>
				( <a href="<?php echo $my_coll_url;?>"><span class="count"><?php echo $coll_total;?></span></a> )<?php
				
			} else if( $collection_title == 'default' ) {
				
				?><span class="count"><?php echo $coll_total;?></span><?php echo sprintf( ' %s', woo_cl_get_label_plural() );
			} else if ( !empty($collection_title) ) { ?>

				<a href="<?php echo $my_coll_url;?>"><?php echo $collection_title; ?></a>
					
				<?php // Show count
				if( $show_count == 'true' ) { ?>
					( <a href="<?php echo $my_coll_url;?>"><span class="count"><?php echo $coll_total;?></span></a> )<?php
				}
			} ?>
		</div><?php 
		if( is_account_page() || $enable_see_all == "true" ) { ?>
			<div class="woocl-pull-right">
				<a href="<?php echo $all_coll_url;?>" title="<?php echo sprintf( __( 'See All %s', 'woocl' ), woo_cl_get_label_plural() );?>"><?php echo sprintf( __( 'See All %s', 'woocl' ), woo_cl_get_label_plural() );?></a>
			</div><?php 
		}?>
	</div>
<?php
}?>

<div class="woocl-collections-gallery">
	<?php if( ( $cl_enable_search == 'yes' || $cl_enable_sorting == 'yes' ) && $allow_search_sort ) {?>
	<div class="woocl-search-bar-wrap">
		<form method="GET" action="">
		  	<?php
			if( $cl_enable_search == 'yes' ){
			?>
	  		<div id="woocl-coll-item-searching">
		  		<input type="text" name="search" placeholder="Search" id="woocl-coll-item_search" value="<?php echo $cl_search; ?>">
		  		<input type="submit" name="col-item-search" id="col-item-search-btn" value="Search">
		  	</div>
		  	<?php }
		  	if( $cl_enable_sorting == 'yes' ){ 
		  	?>
		  	<select id="woocl-coll-sorting" name="sort">
		  		<option value=""><?php _e( 'Default Sorting', 'woocl' );?></option>
		  		<?php
		  		if( !empty( $sort_optns ) ) {
		  			
		  			foreach ( $sort_optns as $key => $value ) {
		  				echo "<option value='$key' ".selected( $key, $cl_sorting, false)." > $value </option>";
		  			}
		  		}?>
		  	</select>
		  <?php } ?>
	  	</form>
   </div>
   <?php }
   	//Collection listing hook
	do_action( 'woo_cl_collection_listing_loop', $collections, $user_id );?>
</div>

<input class="woocl-total-pages" type="hidden" value="<?php echo $total_coll_pages;?>" />
<input class="woocl-curr-page" type="hidden" value="1" /><?php 

if( !$fixed_coll && $total_coll_pages > 1 ) {
	
	//get scroll option
	$cl_scroll_options	= get_option( 'cl_scroll_options' );

	if( $cl_scroll_options == 'yes' ) {?>
		<div class="woocl_load_more_wrap coll-button-wrp woocl-clearfix">
			<button class="woocl_load_more woocl-btn btn-s woocl-btn-ter" data-pagi = "2" data-author="<?php echo $data_author;?>"><?php _e('Load More','woocl');?></button>
			<div class="woo_cl_loader">
		 		<img src="<?php echo WOO_CL_IMG_URL.'/woo-cl-loader.gif';?>" class="woo_cl_loader_img">
		 		<span class="woo_cl_load_msg"><?php _e(' Loading...','woocl');?></span>
 			</div>
		</div>
	<?php 
	} else {?>
		<span data-pagi = "2" data-author="<?php echo $data_author;?>" id="woo-collection-loop-loader"><img class="woo_cldisplay_none" src="<?php echo WOO_CL_URL; ?>/includes/images/woo-cl-loader-big.gif" /></span><?php 
	}
}?>