<?php

/**
 * Template For Collection Listing
 * 
 * Handles to return design collection Listing
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/collection-listing/collection-listing.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

global $wp;

$message		= woo_cl_messages();
$author_slug	= woo_cl_get_slug( 'author' );

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( !empty( $collections ) ) {//If collection is not empty
 	
	foreach( $collections as $key => $collection ) {
		
		do_action( 'woo_cl_collection_listing_data', $collection, $user_id, $col_count);
		$col_count++;
	}
	 
} else {
	
	$no_coll_message = $content = '';
	
	if( is_account_page() || !empty(  $wp->query_vars[$author_slug] ) ) {
		$no_coll_message = isset( $message['no_user_coll_found'] ) ? $message['no_user_coll_found'] : '';
	} else {
		$no_coll_message = isset( $message['no_coll_found'] ) ? $message['no_coll_found'] : '';
	}
	
	if( !empty( $no_coll_message ) ) {
		
		$content = '<div class="woo_cl_errors woo_cl_msgs"><p class="woo_cl_message">';
		$content .= $no_coll_message;
		$content .= '</p></div>';
	}
	
	echo $content;

}