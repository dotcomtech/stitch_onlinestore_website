<?php
/**
 * Template For Collection List block
 * 
 * Handles to return design collection list block
 * 
 * Override this template by copying it to yourtheme/woocommerce/woocommerce-collections/collection-listing/collection-listing-content.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woo_cl_model;

?>
	<div class="woocl-cv-wrp woocl-cv-res col-<?php echo $class_value; ?> ">
		
		<div class="woocl-cv-header"><?php
			if( empty( $cl_item_listsurl ) ) {?>
				<span class="woocl-cv-title woocl-cv-txt"><?php echo $collection_title;?></span><?php
			} else { ?>
				<a class="woocl-cv-title woocl-cv-txt" href="<?php echo $cl_item_listsurl;?>">
					<?php echo $collection_title;?>
				</a><?php 
			}
			if( !empty( $single_user ) && isset($author_enable) && $author_enable=='yes') { //if user name available
				
				$created_by_text = sprintf( '%s %s <a href="%s">%s</a>', woo_cl_get_label_plural(), __('by','woocl'), $author_coll_url, $single_user );
				
				echo '<div class="woocl-coll-author">'.$created_by_text.'</div>';
			}
		
			?>
		</div>
		
		<?php if( empty( $cl_item_listsurl ) ) { ?>
			<div class="woocl-cv-body">
				<div class="woocl-cv-imgwrp woocl-cv-thumb">
					<img title="<?php echo $collection_title;?>" alt="image" class="woocl-coll_img" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $product_id, array(200,200) ) );;?>">
				</div>
			</div><?php 
		} else { ?>
			<a href="<?php echo $cl_item_listsurl; ?>">
			<div class="woocl-cv-body">
				<div class="woocl-cv-imgwrp woocl-cv-thumb">
			
					<img title="<?php echo $collection_title;?>" alt="image" class="woocl-coll_img" src="<?php echo $product_imgurl;?>">
				</div>
			</div>
			</a><?php 
		} ?>
		
		<span class="woocl-cv-items">
			<span><?php echo $product_total;?></span>
			<?php echo __( 'item', 'woocl' );?>
		</span>
		
		<div class="product_list woocl-clearfix"><?php 
		
		$args = array(
						'post_parent'		=> $collection_id,
						'posts_per_page'	=> apply_filters( 'woo_cl_collection_posts_per_page',4 , $collection_id ),
					);
		
		//Get Collection data
		$coll_items	= $woo_cl_model->woo_cl_get_collection_items( $args );
		
		do_action( 'woo_cl_collection_item_listing_loop', $coll_items, $collection_id );
	
		?>
		</div><?php
		
		//check collection is not login user and follow my blog post is activated or not
		if ( $curr_user_ID != $collection['post_author'] && woo_cl_is_follow_activate() ) { ?>
			<div class="woocl-cv-footer">
				<span class="woocl-cv-followers">
				<?php 
					//check follow my blog post is activated or not
					if( woo_cl_is_follow_activate() ) {
						
						$follow_text = sprintf( '<span>%s</span>%s', $follow_count, __( ' followers','woocl' ) );
						//echo $follow_text;
					} ?>
				</span>
				
			</div>
			
			<div class="woocl-cv-btnwrp">
				<div class="woocl-coll-button-wrp">
					<div class="woocl-widget woocl-flex">
						<?php echo do_shortcode( '[wpw_follow_me id="'.$collection_id.'" disablecount="true"][/wpw_follow_me]' );?>
					</div>
				</div>
			</div><?php 
		}
		
		//check collection is same user and user name available
		if( $curr_user_ID == $collection['post_author'] && woo_cl_user_can_update_collection($collection_id) ) { ?>
			
			<div class="coll-button-wrp woocl-clearfix"><button onclick="window.location.href='<?php echo $cl_edit_pageurl;?>'" class="woocl-btn btn-s woocl-btn-ter edit"><?php _e( 'Edit','woocl' );?></button></div><?php 
			if( empty( $single_user ) ) { ?>
				<div class="woocl-ctrlbr <?php echo $status_icon_class;?>"><a title="<?php echo $status_title_msg;?>" alt="<?php echo $status_icon_class;?>" class="woocl-ctrlbr-visbtogl enable">
					<span class="icon woocl-icons"></span><span class="woocl-ctrlbr-txt"><?php echo $status_value;?></span></a>
				</div><?php 
			}
		}
		
		?>
	</div>