<?php

/**
 * Template For Collection Listing
 * 
 * Handles to return design collection Listing
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/collection-listing/collection-listing.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


if( !empty( $coll_items ) ) {

	foreach ( $coll_items as $key => $coll_item ) {
		 do_action( 'woo_cl_collection_product_listing_data', $coll_item, $coll_id, $collection_image, $product_count );
		 $product_count++;
	}
	if($product_count<=4 && $collection_image==true){
		for($i=$product_count; $i<=4; $i++ ){
	?>
		<div class="product_img_main pro-col-3"><img class="woocl-image-wrapper" src="<?php echo WOO_CL_IMG_URL."/whitebox.jpg"?>" ></div>
	<?php }
	}
	
} else {

	$content = '<div class="woo_cl_errors woo_cl_msgs"><p class="woo_cl_message">';
	
	$content .= __( 'Nothing here yet, how about adding some products?', 'woocl' );
	
	$content .= '</p></div>';
	
	echo $content;
	
}