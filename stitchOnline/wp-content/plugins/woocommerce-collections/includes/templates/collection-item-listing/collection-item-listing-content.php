<?php

/**
 * Template For Collection Item List
 * 
 * Handles to return design collection item listing
 * 
 * Override this template by copying it to 
 * yourtheme/woocommerce/woocommerce-collections/collection-item-listing/collection-item-listing-content.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 

// Get current_user
$cl_current_user 		= wp_get_current_user();

// Get Disable role options
$cl_disable_roles 		= get_option( 'cl_disable_user_role' );
$cl_disable_roles		= !empty ( $cl_disable_roles ) ? $cl_disable_roles : array();

// Get user level setting for disabling "Add To Collection" button
$user_disable_ad_col 	= get_the_author_meta( 'cl_disable_col_user', $cl_current_user->ID );

// Get option for enable add all to cart
$woo_cl_set_option		= get_option( 'cl_enable_coll_add_all_to_cart' );
?>

<div class="woocl-item-block col-<?php echo $class_value;  ?>">

	<?php if( $woo_cl_set_option == 'yes' ) : ?>
	<div class="woocl-coll-item-checkbox-wrap">
		<input type="checkbox" id="woocl-coll-item-<?php echo $product_id; ?>" value="<?php echo $product_id; ?>" class="woocl-coll-item-checkbox" name="woocl_checked_products[]" />
		<label for="woocl-coll-item-<?php echo $product_id; ?>"></label>
	</div>
	<?php endif; ?>

	<a class="woocl-image-wrapper">
		<span class="woocl-imgwrp woocl-thumb">
			<div class="woocl-click-image image_height">
				<img class="woocl-coll_img" src="<?php echo $product_imgurl;?>" alt="image"  title="<?php echo $coll_title;?>" />
			</div>
		</span>
	</a>
	
	<?php do_action( 'woo_cl_after_collection_item_image', $coll_id ); ?>
	
	<div class="woocl-arr"></div>
	<div class="woocl-note">
		<?php echo $coll_disc;?>
		
		<?php do_action( 'woo_cl_after_collection_item_description', $coll_id ); ?>
	</div>

	<div class="woocl-item-details">
		<div class="woocl-item-detailsFull">
			<div class="woocl-item-title">
			   <a class="woocl-lens-item" href="<?php echo $product_url;?>"><?php echo $coll_title;?></a>		   
			   
			   <?php do_action( 'woo_cl_after_collection_item_title', $coll_id ); ?>
			</div> <!--- end woocl-itemtitle -->
			
			<?php if( !empty( $product_variation ) ) {
				echo '<div class="woocl-item-variation">' . $product_variation . '</div>';
			}?>
			
			<div class="woocl-item-price">
				<?php echo $product_price;?>
				
				<?php do_action( 'woo_cl_after_collection_item_price', $coll_id ); ?>
			</div>
		</div> <!--- end woocl-item-detailsFull-->
		
		<?php do_action( 'woo_cl_after_collection_item_details', $coll_id , $product_id ); ?>
		
	</div>     <!--- end woocl-item-details-->
	
	<?php
	if ( ( empty( $cl_current_user->roles ) || ( !empty( $cl_current_user->roles ) && !in_array( $cl_current_user->roles[0], $cl_disable_roles ) ) ) &&
		( ( empty( $user_disable_ad_col ) || ( !empty( $user_disable_ad_col ) && $user_disable_ad_col == 'no' ) ) ) ) : ?>
	<div class="woocl-iconbtn <?php echo $add_collbtn_class;?>">
		
		<?php if( !is_user_logged_in() && get_option( 'cl_guest_options' ) != 'yes' ) { echo '<a href="'. woo_cl_redirect_link_for_non_login_user().'">'; }?>
			<div class="woocl-iconsmall"></div>
		<?php if( !is_user_logged_in() && get_option( 'cl_guest_options' ) != 'yes' ) { echo '</a>'; }?>

		<div class="woocl-colw-tooltip"><p><?php echo sprintf( __('Save, organize and share what you love by adding this to a %s.','woocl'), woo_cl_get_label_singular() );?></p></div>

	</div>

	<?php endif; ?>
	<input type="hidden" class="woocl-coll-item-id" value="<?php echo $coll_item['ID'];?>"/>
	<input type="hidden" class="woo-cl-product_id" value="<?php echo $product_id; ?>" />

</div><!--- end woocl-cv-wrp woocl-cv-res-->