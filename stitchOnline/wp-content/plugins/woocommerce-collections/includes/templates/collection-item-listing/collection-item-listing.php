<?php

/**
 * Template For Collection Item Listing
 * 
 * Handles to return design collection item listing
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/collection-item-listing/collection-item-listing.php
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

  <div class="woocl-collection woocl-clearfix">
	  <div class="woocl-collection-info">
	     	<div class="woocl-titlename">
	     		<h2>
	     			<?php
	     			echo $coll_title;

	     			//Display if total price enabled
			     	if( !empty( $collection_subtitle_text ) ) {
			     		echo '<div class="woocl-coll-total-price">'. $collection_subtitle_text. '</span>';
			     	}?>
	     		</h2>
	     		<input type="hidden" value="<?php echo $coll_id; ?>" class="woo-cl-collection-id">
	     		<?php 
	     		
	     		//Action after collection title
	     		do_action( 'woo_cl_collection_title_after', $coll_id, $coll_items );

	     		if ( $curr_user_ID != $coll_author ) { ?>
					<div class="woocl-widget woocl-item-list-followbtn"><?php 

						//check follow my blog post is activated or not
						if( woo_cl_is_follow_activate() ) {
							echo do_shortcode( '[wpw_follow_me id="'.$coll_id.'" disablecount="true"][/wpw_follow_me]' );
						}?>
					</div><?php 
	     		} ?>
	     	</div>

		    <div class="woocl-acts">
		     	<?php
		     	if( $owner && woo_cl_user_can_update_collection($coll_id) ) { ?>
		     		<span class="woocl-acts-item">
			     		<button type="button" onclick="window.location.href='<?php echo $cl_edit_pageurl;?>'" class="clnwbtn woocl-btn btn-prim" id="collectItem"><?php echo sprintf( __('Edit %s','woocl'), woo_cl_get_label_singular() );?></button>
			     	</span>
		     	<?php }?>
	     	</div>
	     	<div class="woocl-clear"></div>

	     	<?php
			if( !empty( $user_name ) ) { //if user name pass by url

				$created_by_text = sprintf( '%s <a href="%s">%s</a>', __('Created by','woocl'), $author_coll_url, $user_name );
				?>

				<div class="woocl-titlename woocl-created-text">
					<a href="<?php echo $author_coll_url;?>">
						<div class="woocl-user-photo">
							<?php echo $author_avatar;?>
						</div>
					</a>
					<div class="woocl-user-curatedby">
						<div class="woocl-user-name"><?php echo $created_by_text;?></div>
						<div class="woocl-lastupdated">
							<span class="woocl-lastupdatedLabel"><?php _e('Last updated: ','woocl');?></span><span class="woocl-collectionupdated"><?php echo $coll_modified_date;?></span>
						</div>
					</div>
				</div><?php 
			}?>
	     	<div class="woocl-innerline">
	     		<p>
	     			<?php echo nl2br( $coll_content );?>
	     		</p>
	     	</div><!-- end .woocl-innerline -->
	   </div><!--- end woocl-collection-info -->
	  <div class="woocl-clear"></div>

    <!-- div for show success message for add bulk product to cart-->
	<div class="woocl-add-to-cart-message"></div>

	<?php 
	if( !empty( $coll_items ) ) {//check if collection items not empty
		
		if( !( $coll_status == 'private' ) || $owner ) {//check if not private or owner
			
			// get all social share buttons 
			do_action( 'woo_cl_collection_share_buttons' ); 
		}

	if( $cl_bulk_add_to_cart == 'yes') : ?>
	<div class="woocl-btn-actions">
		<span class="woo_cl_loader">
			<img src="<?php echo WOO_CL_IMG_URL.'/woo-cl-loader.gif';?>" class="woo_cl_loader_img">
			<span class="woo_cl_load_msg"><?php _e(' Adding to Cart...','woocl');?></span>
		</span>

		<a class="woocl-button" href="javascript:void(0);" id="woo_cl_chk_unchk_btn"><?php _e( 'Select All', 'woocl' );?></a>
		<a class="woocl-button woo-cl-disabled" href="javascript:void(0);" id="woo_cl_add_to_cart_btn"><?php _e( 'Add to Cart', 'woocl' );?></a>
	</div>
	<?php endif; ?>

	<div class="woocl-clear"></div>
	<div class="woocl-items">
  		<div class="woocl-collect-item woocl-clearfix">
  		   <span><?php echo $coll_item_total;?></span><p><?php _e('items','woocl');?></p>
  		</div>
	<?php 
		//check follow my blog post is activated or not
		if( woo_cl_is_follow_activate() ) {
			
	  		echo '<div class="header-separator">|</div><div class="woocl-follower">';
					
			$follow_text = sprintf( '<span>%s</span><p>%s</p>', $follow_count, __( ' followers','woocl' ) );
			echo $follow_text;
			
	  		echo '</div>';
		} ?>
	  		 
	  </div><!--- end woocl-items -->
	  	
	  <div class="woocl-update">	
	  	<div class="woocl-lastUpdated">
	  		<span class="woocl-lastUpdatedLabel"><?php _e('Last updated: ','woocl');?></span>
	  		<span class="woocl-collectionUpdated"><?php echo $coll_modified_date;?></span>
	  	</div>
	  </div>	<!--- end woocl-update -->
	  <?php }?>
	  <div class="woocl-row-item woocl-clear">
	  	<?php if( $cl_enable_search == 'yes' || $cl_enable_sorting == 'yes' ){?>
		<div class="woocl-search-bar-wrap">
			<form method="GET" action="">
			  	<?php if( $cl_enable_search == 'yes' ) {?>
		  		<div id="woocl-coll-item-searching">
			  		<input type="text" name="search" placeholder="Search" id="woocl-coll-item_search" value="<?php echo $cl_search;?>">
			  		<input type="submit" name="col-item-search" id="col-item-search-btn" value="Search">
			  	</div>
			  	<?php }
			  	if( $cl_enable_sorting == 'yes' ) {?>
			  	<select id="woocl-coll-sorting" name="sort">
			  		<option value=""><?php _e( 'Default Sorting', 'woocl' );?></option>
			  		<?php
			  		if( !empty( $sort_optns ) ) {
						foreach ( $sort_optns as $key => $value ) {
			  				echo "<option value='$key' ".selected( $key, $cl_sorting, false )." > $value </option>";
			  			}
			  		}?>
			  	</select>
			  <?php }?>
		  	</form>
		</div>
	   <?php }?>
	  </div>
	  <div class="woocl-collections">
		<?php do_action( 'woo_cl_collection_item_listing_loop', $coll_items, $coll_id, false ); ?>
	  </div> 	<!--- end woocl-collections -->
	</div>	<!--- end woocl-collection -->
	
	<?php 
	// do acttion for Add To Collection popup
	//do_action( 'woo_cl_add_to_collection_popup' );
	?>
	<input type="hidden" id="woocl-coll-id" value="<?php echo $coll_id;?>" />
	<input class="woocl-item-total-pages" type="hidden" value="<?php echo $total_coll_item_pages;?>" />
	<input class="woocl-item-curr-page" type="hidden" value="1" /><?php 

if( $total_coll_item_pages > 1 ) {

	if( $cl_scroll_options == 'yes' ) {?>
		<div class="woocl_load_more_wrap coll-button-wrp woocl-clearfix">
			<button class="woocl_load_more_item woocl-btn btn-s woocl-btn-ter" data-pagi = "2"  data-is_ajax = "1"><?php _e('Load More','woocl');?></button>
			<div class="woo_cl_loader">
		 		<img src="<?php echo WOO_CL_IMG_URL.'/woo-cl-loader.gif';?>" class="woo_cl_loader_img">
		 		<span class="woo_cl_load_msg"><?php _e(' Loading...','woocl');?></span>
 			</div>
		</div>
	<?php 
	} else { ?>
		<div data-pagi = "2"  data-is_ajax = "1" id="woo-collection-item-loop-loader"><img class="woo_cldisplay_none" src="<?php echo WOO_CL_URL; ?>/includes/images/woo-cl-loader-big.gif" /></div><?php 
	}
}