<?php 
/**
 * Template For Collection Item List of image
 * 
 * Handles to return design collection item listing
 * 
 * Override this template by copying it to 
 * yourtheme/woocommerce/woocommerce-collections/collection-item-listing/collection-item-listing-content.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if($product_count != 1){
?>
<div class="product_img_main pro-col-3">
	<div class="box-inner">
	<a class="woocl-image-wrapper" href="<?php echo $product_url; ?> ">
		<img class="woocl-coll_img1" src="<?php echo $product_small_imgurl; ?>" alt="image"  title="<?php echo $coll_title;?>" />
	</a>
	</div>
</div>
	<?php do_action( 'woo_cl_after_collection_item_image', $coll_id ); 
} 
?>
