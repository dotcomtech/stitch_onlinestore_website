<?php
/**
 * Add To Collection Button Template
 * 
 * Override this template by copying it to yourtheme/woocommerce/woocommerce-collections/collection-button/collection-button.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Get current_user
$cl_current_user 		= wp_get_current_user();

//Get Guest option
$cl_guest_options 		= get_option( 'cl_guest_options' );

// Get Disable role options
$cl_disable_roles 		= get_option( 'cl_disable_user_role' );
$cl_disable_roles		= !empty ( $cl_disable_roles ) ? $cl_disable_roles : array();

// Get user level setting for disabling "Add To Collection" button
$user_disable_ad_col 	= get_the_author_meta( 'cl_disable_col_user', $cl_current_user->ID );

$icon_position = !empty( $icon_position ) ? $icon_position : 'left';

if ( !is_user_logged_in() && $cl_guest_options != 'yes' ) {//If user is not login
?>
	
	<div class="woocl-click woocl-clearfix <?php echo $variation_class;?>">
			<a class="<?php echo $button_class;?>" href="<?php echo woo_cl_redirect_link_for_non_login_user(); ?>" title="<?php echo $add_to_coll_btn;?>">
			<?php if( $icon_position == 'left' ) { ?>
				<i class="<?php echo $add_to_coll_btnicon;?> woocl-left"></i>
			<?php } ?>
			<span class="woocl-btn-label"><?php echo $add_to_coll_btn;?></span>
			<?php if( $icon_position == 'right' ) { ?>
				<i class="<?php echo $add_to_coll_btnicon;?> woocl-right"></i>
			<?php } ?>
		</a>
	</div><?php 
	
} else {

	if ( ( empty( $cl_current_user->roles ) || ( !empty( $cl_current_user->roles ) && !in_array( $cl_current_user->roles[0], $cl_disable_roles ) ) ) &&
		( ( empty( $user_disable_ad_col ) || ( !empty( $user_disable_ad_col ) && $user_disable_ad_col == 'no' ) ) ) ) { ?>
	
		<div class="woocl-click woocl-clearfix <?php echo $variation_class;?>">
			<a href="javascript:void(0);" class="show_dropbox <?php echo $button_class;?>" title="<?php echo $add_to_coll_btn;?>">
				<?php if( $icon_position == 'left' ) { ?>
					<i class="<?php echo $add_to_coll_btnicon;?> woocl-left"></i>
				<?php } ?>
				<span class="woocl-btn-label"><?php echo $add_to_coll_btn;?></span>
				<?php if( $icon_position == 'right' ) { ?>
					<i class="<?php echo $add_to_coll_btnicon;?> woocl-right"></i>
				<?php } ?>
			</a>
			<input type="hidden" class="woo-cl-product_id" value="<?php echo $product_id;?>" />
			<input type="hidden" class="woo-cl-variable_id" value="" />
		</div><?php
	}
}?>