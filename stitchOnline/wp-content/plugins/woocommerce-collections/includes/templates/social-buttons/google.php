<?php

/**
 * Google Plus Button Template
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/social-buttons/google.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>

<div class="woocl-gplus woocl-share-button woocl-col-3">
	<div class="woocl-gp-button">
		<g:plusone href="<?php echo $coll_share_url;?>" size="tall" data-annotation="bubble" onendinteraction="woocl_gp_share_<?php echo $coll_id;?>"></g:plusone>
	</div>
</div>