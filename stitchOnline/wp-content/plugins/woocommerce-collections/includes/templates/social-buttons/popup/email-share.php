<?php

/**
 * Template For Share via Email
 * 
 * Handles to return design for Email Share
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/social-buttons/popup/email-share.php
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="woocl-modal-share-wrapper">  
 <div class="woocl-share-middle">
	<div class="woocl-top"><div class="woocl-close woocl-close-style"></div></div>
        <div class="woocl-share-inner">
			<div class="woocl-share-header">
				<div class="woocl-title"><p><?php echo sprintf( __('Share This %s','woocl'), woo_cl_get_label_singular() );?></p></div>
			</div><!-- end of share-header -->
	        <div class="woocl-inseporator"></div>

	       <div class="woocl-share-inputarea">
	       		<input type="hidden" id="woocl-coll-id" value="<?php echo $coll_id;?>" />
				<div class="woocl-yourname"><p><label for="woocl-your-name"><?php _e('Your Name','woocl');?></label></p>
					<input type="text" id="woocl-your-name" class="woocl-your-name" />
					<div class="woocl-sendername-msg woo_cl_errors woo_cl_msgs"><p class="woo_cl_message"><?php _e('Please enter your name','woocl');?></p></div>
				</div>
				<div class="woocl-youremailadd"><p><label for="woocl-your-emailadd"><?php _e('Your Email Address','woocl');?></label></p>
					<input type="text" id="woocl-your-emailadd" class="woocl-your-emailadd" />
					<div class="woocl-senderemail-msg woo_cl_errors woo_cl_msgs"><p class="woo_cl_message"><?php _e('Please enter your email address','woocl');?></p></div>
				</div>										
				
				<div class="woocl-friendemailadd"><p><label for="woocl-friend-emailadd"><?php _e("Friend's Email Address",'woocl');?></label></p>
					<div class="woocl-notification"><p><?php _e('To send to multiple email addresses,seperate each email with a comma','woocl');?></p></div>
					<input type="text" id="woocl-friend-emailadd" class="woocl-friend-emailadd" />
					<div class="woocl-friendemail-msg woo_cl_errors woo_cl_msgs"><p class="woo_cl_message"><?php _e('Please enter one or more email addresses','woocl');?></p></div>
				</div>										

				<div class="woocl-yourmessage"><p><label for="woocl-your-message"><?php _e('Your Message','woocl');?></label></p>
					<div class="woocl-notification"><p><?php _e('optional','woocl');?></p></div>
				 	<textarea rows="3" id="woocl-your-message"></textarea>	
				</div>										
				
			</div><!-- end of share-inputarea -->
				
			<div class="woocl-share-footer">
				<div class="woocl-share-acts">
					<button type="button" id="woocl-email-share-btn" class="woocl-share-btn"><?php _e('Send Now','woocl');?></button>
					<div class="woo_cl_email_share_loader"><img src="<?php echo WOO_CL_IMG_URL.'/woo-cl-loader.gif' ?>" alt="loader"/><span class="woo_cl_email_share_msg"><?php _e(' Sending...','woocl');?></span></div>
				</div>
            </div><!-- end of footer-->	
			
		</div>	<!-- end of inner-->	
	</div>	<!-- end of mid-->
	 <div class="woocl-overlay"></div>
 	</div>