<?php

/**
 * Template For Share via Email Success Popup
 * 
 * Handles to return design for Email Share Success popup
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/social-buttons/popup/email-share-success.php
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="woocl-modal-email-success-wrapper">  
 <div class="woocl-success-middle">
	<div class="woocl-top"><div class="woocl-close woocl-close-style"></div></div>
        <div class="woocl-success-inner">
	       <div class="woocl-success-inputarea">
	             <p><?php _e('Successfully shared.','woocl');?></p>
			</div><!-- end of share-inputarea -->
			<div class="woocl-success-footer">
				<div class="woocl-success-acts"><button type="button" class="woocl-success-btn"><?php _e("Great, i'm done","woocl");?></button></div>
            </div><!-- end of footer-->	
			
		</div>	<!-- end of inner-->	
 </div>	<!-- end of mid-->
 <div class="woocl-overlay"></div>
</div>