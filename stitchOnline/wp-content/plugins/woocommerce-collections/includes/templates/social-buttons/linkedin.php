<?php
/**
 * LinkedIn Button Template
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/social-buttons/linkedin.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>

<div class="woocl-linkedin woocl-share-button woocl-col-3">
	<div class="woocl-linkedin-button">
		<script type="IN/Share" data-url="<?php echo $coll_share_url;?>" data-onSuccess="share" data-counter="top"></script>
	</div>
</div>