<?php

/**
 * Twitter Button Template
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/social-buttons/twitter.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="woocl-twitter woocl-share-button woocl-col-3">
	<div data-id="woocltwbutton_<?php echo $coll_id;?>">
		<a href="http://twitter.com/share"  data-url="<?php echo $coll_share_url;?>"  class="twitter-share-button" data-text="<?php echo $coll_title;?>" data-count="vertical" data-via="<?php echo $tw_user_name;?>"><?php _e( 'Tweet', 'woocl' );?></a>
	</div>
</div>