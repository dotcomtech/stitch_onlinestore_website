<?php

/**
 * Social Button Template
 *
 * Displays all the activated social sharing buttons.
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/social-buttons/social-buttons.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	global $post;
?>

<div class="woocl-social-buttons">
	<div class="woocl-social-sub-btn">
		<?php 
			/**
			 * woo cl social buttons hook (outputs the social sharing buttons)
			 */
			do_action( 'woo_cl_social_buttons', $coll_id );
		?>	
	</div>
</div>