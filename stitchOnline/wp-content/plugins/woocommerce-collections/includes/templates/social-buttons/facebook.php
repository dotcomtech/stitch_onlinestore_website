<?php
/**
 * Facebook Button Template
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/social-buttons/facebook.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>

<div id="fb-root"></div>
<div class="woocl-facebook woocl-share-button woocl-col-3">
	<fb:share-button href="<?php echo $coll_share_url; ?>" type="box_count"></fb:share-button>
</div>