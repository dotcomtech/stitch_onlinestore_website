<?php
/**
 * Share via Email Button Template
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/social-buttons/email.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="woocl-email woocl-share-button woocl-col-3">
	<div class="woocl-email-button">
		<a href="javascript:void(0);" class="woocl-button woocl-email-btn" title="<?php _e( 'Share via Email', 'woocl' );?>">
			<span class="woocl-btn-label"><?php _e( 'Share via Email', 'woocl' );?></span>
		</a>
	</div>
</div>