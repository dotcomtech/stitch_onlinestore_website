<?php

/**
 * Template For Featured Collection
 * 
 * Handles to return design featured collection
 * 
 * Override this template by copying it to
 * yourtheme/woocommerce/woocommerce-collections/featured-collection-listing/featured-collection-listing.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$message = woo_cl_messages();
$no_coll_message = isset( $message['no_coll_found'] ) ? $message['no_coll_found'] : '';
?>
	
<div class="woocl-featured-col-wrap woocl-clearfix">
    <div class="woocl-featured-heading">
		<h2><?php echo $fcolls_title;?></h2>
	</div><!-- end of heading -->
	
	<?php 	
		if( !empty( $collections ) ) {//If collection is not empty
			
			foreach( $collections as $key => $collection ) {
				
				do_action( 'woo_cl_featured_collection_listing_data', $collection, $user_id );
			}
			 
		} else {
			
			$content = '<div class="woo_cl_errors woo_cl_msgs"><p class="woo_cl_message">';
			
			$content .= $no_coll_message;
			
			$content .= '</p></div>';
			
			echo $content;

		}
		?>
</div>