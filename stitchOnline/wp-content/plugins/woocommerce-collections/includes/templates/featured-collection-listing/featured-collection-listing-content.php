<?php
/**
 * Template For Featured Collection List block
 * 
 * Handles to return design featured collection list block
 * 
 * Override this template by copying it to yourtheme/woocommerce/woocommerce-collections/featured-collection-listing/featured-collection-listing-content.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<div class="woocl-featured-col">
	<div class="woocl-featured-wrapper">	
<div class="woocl-featured-cover">
<div class="woocl-featured-display">
	<div class="woocl-featured-framing-title-over">
	 	<h2 class="woocl-featured-title">
	 	<?php if( empty( $cl_item_listsurl ) ) {
	 			echo '<span>'. $collection_title. '</span>';
	 		  } else { ?>
	 			<a title="<?php echo sprintf( __('%s of products named %s', 'woocl'), woo_cl_get_label_singular(), $collection_title );?>"  href="<?php echo $cl_item_listsurl;?>">
	 				<?php echo $collection_title;?>
	 			</a>
		<?php } ?>
	 	</h2>
	 	<span class="woocl-featured-shop-now-link">
		 		<?php if( empty( $cl_item_listsurl ) ) {
		 			echo '<span>'. __('Shop now', 'woocl') .'</span>';
		 		 } else { ?>
		 		<a title="<?php echo sprintf( __('%s of products named %s', 'woocl'), woo_cl_get_label_singular(), $collection_title );?>" href="<?php echo $cl_item_listsurl;?>">
		 			<?php _e('Shop now', 'woocl');?>
		 		</a>
			<?php } ?>
	 	</span>
	</div><!-- end of featured-cover -->
	
	<div class="woocl-featured-big-image">
	  	<div class="woocl-featured-image-pos">
 			<a href="<?php echo $cl_item_listsurl;?>">
			    <img src="<?php echo $product_imgurl['imgurl_0'];?>" alt="image" />
			</a>
		    <span class="woocl-featured-hide-text"><?php echo sprintf( __('%s of products named %s', 'woocl'), woo_cl_get_label_singular(), $collection_title );?></span>
		</div>
		<div class="woocl-featured-thumbs">
			<div class="woocl-featured-small-thumb">
			<?php if( !empty( $cl_item_listsurl ) ) echo '<a href="'. $cl_item_listsurl .'">';?>
				<img src="<?php echo $product_imgurl['imgurl_1'];?>" alt="image" />
			<?php if( !empty( $cl_item_listsurl ) ) echo '</a>';?>
			</div>
			<div class="woocl-featured-small-thumb">
			<?php if( !empty( $cl_item_listsurl ) ) echo '<a href="'. $cl_item_listsurl .'">';?>
					<img src="<?php echo $product_imgurl['imgurl_2'];?>" alt="image" />
			<?php if( !empty( $cl_item_listsurl ) ) echo '</a>';?>
			</div>
			<div class="woocl-featured-small-thumb">
			<?php if( !empty( $cl_item_listsurl ) ) echo '<a href="'. $cl_item_listsurl .'">';?>
				<img src="<?php echo $product_imgurl['imgurl_3'];?>" alt="image" />
			<?php if( !empty( $cl_item_listsurl ) ) echo '</a>';?>
			</div>
		</div><!-- end of featured-thumbs -->
	 </div><!-- end of featured-big-image-->
	 <div class="woocl-clear"></div>
	 <div class="woocl-featured-framing-description-under">
	   	<div class="woocl-featured-description-left">
		   	<h2 class="woocl-featured-inner-title">
		 	<?php if( empty( $cl_item_listsurl ) ) {
		 			echo '<span>'. $collection_title. '</span>';
		 		  } else { ?>
		 			<a title="<?php echo sprintf( __('%s of products named %s', 'woocl'), woo_cl_get_label_singular(), $collection_title );?>"  href="<?php echo $cl_item_listsurl;?>">
		 				<?php echo $collection_title;?>
		 			</a>
			<?php } ?>
		   	</h2>
		   	<div class="woocl-featured-inner-subtitle"><?php echo $collection_disc;?></div>
		</div><!-- end of featured-description-left -->
		<div class="woocl-featured-description-right">
		    <div class="woocl-featured-firstiteam"><?php echo sprintf( __( '%s items', 'woocl' ), $product_total );?></div>
			<div class="woocl-featured-price"><?php echo sprintf( __('%s - Up','woocl'), $product_min_price );?></div>
			<div class="woocl-featured-lastiteam">
		 		<?php if( empty( $cl_item_listsurl ) ) {
		 				echo '<span>'. __('Shop now', 'woocl') .'</span>';
		 		 	} else { ?>
			 		<a title="<?php echo sprintf( __('%s of products named %s', 'woocl'), woo_cl_get_label_singular(), $collection_title );?>" href="<?php echo $cl_item_listsurl;?>">
			 			<?php _e('Shop now', 'woocl');?>
			 		</a>
				<?php } ?>
			</div>
		</div><!-- end of featured-description-right -->
	</div><!-- end of featured-framing-desc-under -->
</div><!-- end of featured-display -->
</div>
</div><!-- end of featured-wrapper -->
</div><!-- end of featured-col -->