<?php
/**
 * Template For Collection Edit
 * 
 * Handles to return design collection edit
 * 
 * Override this template by copying it to yourtheme/woocommerce/woocommerce-collections/collection-edit/collection-edit-content.php
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woo_cl_model;

// Get current_user
$cl_current_user 		= wp_get_current_user();

// Get Disable role options
$cl_disable_roles 		= get_option( 'cl_disable_user_role' );
$cl_disable_roles		= !empty ( $cl_disable_roles ) ? $cl_disable_roles : array();

// Get user level setting for disabling "Add To Collection" button
$user_disable_ad_col 	= get_the_author_meta( 'cl_disable_col_user', $cl_current_user->ID );
?>
	
<div class="woocl-item-block col-<?php echo $class_value; ?>">
	<a class="woocl-image-wrapper">
		<span class="woocl-vishide"><?php echo $woo_cl_model->woo_cl_escape_attr( $coll_item['post_title'] );?></span>
		<span class="woocl-imgwrp woocl-thumb">
			<div class="woocl-click-image">
				<img class="woocl-coll_img" src="<?php echo $product_imgurl;?>" alt="image"  title="<?php echo $woo_cl_model->woo_cl_escape_attr( $coll_item['post_title'] );?>" />
			</div>
			<i></i>
		</span>
	</a>
	<div class="woocl-arr"></div>
		<div class="woocl-note">
			<input type="text" placeholder="<?php _e('Say something about this item','woocl');?>" name="woocl_coll_item_disc[<?php echo $coll_item['ID'];?>][desc]" class="woocl-entertesttext woocl_coll_item_disc[<?php echo $coll_item['ID'];?>][desc]" value="<?php echo $woo_cl_model->woo_cl_escape_attr( $coll_item['post_content'] );?>" >
	    </div>  
		<div class="woocl-item-details">
			<div class="woocl-item-details-full">
				<div class="woocl-item-title">
				   <a class="woocl-lens-item" href="<?php echo $product_url;?>"><?php echo $woo_cl_model->woo_cl_escape_attr( $coll_item['post_title'] );?></a>
				</div> <!--- end woocl-itemtitle -->
				<?php if( !empty( $product_variation ) ) {
					echo '<div class="woocl-item-variation">' . $product_variation . '</div>';
				}?>
				<div class="woocl-itemprice"><?php echo $product_price;?></div>
					
			</div> <!--- end woocl-itemDetailsFull-->
		</div>     <!--- end woocl-itemdetails-->
		<?php
			if ( ( empty( $cl_current_user->roles ) || ( !empty( $cl_current_user->roles ) && !in_array( $cl_current_user->roles[0], $cl_disable_roles ) ) ) &&
		( ( empty( $user_disable_ad_col ) || ( !empty( $user_disable_ad_col ) && $user_disable_ad_col == 'no' ) ) ) ) : ?>
		<div class="woocl-iconbtn <?php echo $add_collbtn_class;?>">
			<div class="woocl-iconsmall"></div>
			<div class="woocl-iconsmall2"></div>
			<div class="woocl-colw-tooltip"><p><?php echo sprintf( __('Save, organize and share what you love by adding this to a %s.','woocl'), woo_cl_get_label_singular() );?></p></div>
		</div>
		<?php endif; ?>
		<div class="woocl-delete-button">
			<div class="woocl-deleteicon"></div><!-- CANCEL BUTTON HOVER EFFECT -->
		</div>
		<div class="woocl-delete-success">
			<div class="woocl-imgtxt"><p><?php echo sprintf( __( 'This item has been removed from your %s.', 'woocl' ), woo_cl_get_label_singular() );?></p></div>
		</div>
		<div class="woocl-hoverbox">
			<div class="woocl-imgtxt"><p><?php echo sprintf( __('Want to remove this item from your %s?','woocl'), woo_cl_get_label_singular() );?></p></div>
			<div class="woocl-hoverbtn">
			  	<div class="woocl-cancelbtn"><button type="button" class="woocl-hover-click-btn" ><?php _e('Cancel','woocl');?></button></div>
				<div class="woocl-removebtn"><button type="button" class="woocl-hover-click-btn woocl-remove-coll-item"><?php _e('Remove','woocl');?></button></div>
			</div>  <!--- end hoverbtn-->
		</div><!--- end hoverbox-->
		<?php if( $coll_item_total > 1 ) {?>
		<div class="woocl-coverimage">
			<a class="woocl-cover-image"><?php _e('Make cover image','woocl');?></a>
		</div>
		<?php }?>
		<input type="hidden" class="woocl-product-imgurl" value="<?php echo $product_imgurl;?>"/>
		<input type="hidden" class="woocl-coll-item-id" value="<?php echo $coll_item['ID'];?>"/>
		<input type="hidden" class="woo-cl-product_id" value="<?php echo $product_id;?>"/>
		
</div><!-- end woocl-item-block-->