<?php

/**
 * Template For Cover Image
 * 
 * Handles to return design for confirm cover image
 * 
 * Override this template by copying it to yourtheme/woocommerce/woocommerce-collections/collection-edit/popups/cover-image.php
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<!-- Cover Image Popup -->
  
<div class="woocl-modal-image-inwrapper">
<div class="woocl-modal-nextwrapper">
<div class="woocl-image-middlepart">
<div class="woocl-wrapper">
	<div class="woocl-border">
		<div class="woocl-toper">
			<div class="woocl-closer"></div>
		</div><!-- end of top -->
</div><!-- end of wrapper -->
<div class="woocl-innerblock">
	<div class="woocl-wrapper">
		<div class="woocl-popup-img">
			<div class="woocl-image-display">
				<img class="woocl-coverimg" src="" alt="image" />
			</div>
		</div>
		<div class="woocl-messagepart">
			<div class="woocl-message">
				<div class="woocl-warning">
					<div class="woocl-pub"><p><?php _e('Do you want this image to be the cover?','woocl');?></P></div>
				</div><!-- end of warning -->
				<div class="woocl-subtext"><P><?php echo sprintf( __('This will become the first image in your %1$s and will represent your %1$s in your profile.','woocl'), woo_cl_get_label_singular() );?></P></div>
			</div><!-- end of message -->
		</div>
		<div class="woocl-clear"></div>
		<div class="woocl-click">
		 	<div class="woocl-buttons">
		 		<div class="woocl-cancel"><button type="button" class="woocl-btn" id="collectitem"><?php _e('Cancel','woocl');?></button></div>
		 		<div class="woocl-confirm">
		 			<button type="button" class="woocl-btn" id="woocl-coll-cover-img">
				 		<span class="woocl_btn_text"><?php _e('Confirm','woocl');?></span>
		 			</button>
		 			<div class="woo_cl_loader">
				 		<img src="<?php echo WOO_CL_IMG_URL.'/woo-cl-loader.gif';?>" class="woo_cl_loader_img">
				 		<span class="woo_cl_updating_msg"><?php _e('Updating...','woocl');?></span>
		 			</div>
		 			<input type="hidden" class="woocl-coll-id" value="<?php echo $coll_id;?>"/>
		 			<input type="hidden" class="woocl-product-id" value=""/>
		 			<input type="hidden" class="woocl-coll_item_id" value=""/>
		 		</div>
		 	</div><!-- end of button -->
		</div><!-- end of click -->
	 </div><!-- end of wrapper-->
 </div><!-- end of inner-->
 <div class="woocl-clear"></div>
</div> <!-- end of border -->
</div>	<!-- end of middlepart-->
</div><!-- end of modal-wrapper1 -->
<div class="woocl-overlay"></div>
</div>	<!-- end of modal-inwrapper -->