<?php

/**
 * Template For Collection Delete
 * 
 * Handles to return design for delete a collection popup
 * 
 * Override this template by copying it to yourtheme/woocommerce/woocommerce-collections/collection-edit/popups/collection-delete.php
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<!-- Collection Delete Popup -->

<div class="woocl-modal-delete-inwrapper">
  <div class="woocl-modal-nextwrapper">
	<div class="woocl-delete-middlepart">
		<div class="woocl-wrapper">
		<div class="woocl-border">
				<div class="woocl-toper"><div class="woocl-closer"></div>
				</div><!-- end of top -->
		</div><!-- end of wrapper -->
		<div class="woocl-innerblock">
			<div class="woocl-wrapper">
				<div class="woocl-message">
					<div class="woocl-warning">
					<div class="woocl-pub"><p><?php _e('Are you sure want to','woocl');?></p><p><?php echo sprintf( __('delete this %s?','woocl'), woo_cl_get_label_singular() );?></p></div>
					</div><!-- end of warning -->
					<div class="woocl-subtext"><?php echo sprintf( __('(This will be permanent and you will not be able to view this %s again.)','woocl'), woo_cl_get_label_singular() );?></div>
				</div><!-- end of message -->
				<div class="woocl-clear"></div>
				 <div class="woocl-click">
				 	<div class="woocl-buttons">
				 		<div class="woocl-cancel"><button type="button" class="woocl-btn" id="collectitem"><?php _e('Cancel','woocl');?></button></div>
				 		<div class="woocl-confirm">
				 			<button type="button" class="woocl-btn" id="woocl-coll-delete"><?php _e('Yes, Delete','woocl');?></button>
				 			<div class="woo_cl_loader">
						 		<img src="<?php echo WOO_CL_IMG_URL.'/woo-cl-loader.gif';?>" class="woo_cl_loader_img">
						 		<span class="woo_cl_updating_msg"><?php _e('Deleting...','woocl');?></span>
				 			</div>
				 			<input type="hidden" class="woocl-coll_id" value="<?php echo $coll_id;?>"/>
				 		</div>
				 	</div><!-- end of button -->
				 </div><!-- end of click -->
			 </div><!-- end of wrapper-->
		 </div><!-- end of inner-->
			 <div class="woocl-clear"></div>
		</div> <!-- end of border -->
	</div>	<!-- end of middlepart-->
	</div><!-- end of modal-wrapper1 -->
	<div class="woocl-overlay"></div>
</div>	<!-- end of modal-inwrapper -->