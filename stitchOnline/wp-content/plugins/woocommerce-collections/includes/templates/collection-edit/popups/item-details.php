<?php

/**
 * Template For Collection Item Details
 * 
 * Handles to return design for collection item details popup
 * 
 * Override this template by copying it to yourtheme/woocommerce/woocommerce-collections/collection-edit/popups/item-details.php
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="woocl-left-part">
	<div class="woocl-viewport"><img src="<?php echo $product_imgurl;?>" alt="image" /></div>
</div><!-- end of left-part -->

<div class="woocl-right-part"><?php
	
	if( $is_trashed ) {?>
		<div class="woo-cl-notice"><?php echo __( 'Item Not Availble', 'woocl' );?></div><?php
	}?>
	
	<div class="woocl-colwItem">
		<h2 class="woocl-summery">
			<?php if( $is_trashed ) {?>
				<?php echo $coll_item_title;?>
			<?php } else { ?>
				<a class="woocl-summary-title regular" target="_blank" href="<?php echo $product_url;?>"><?php echo $coll_item_title;?></a>
			<?php } ?>
		</h2><?php 
		if( !empty( $product_variation ) ) {
			echo '<div class="woocl-item-variation">' . $product_variation . '</div>';
		}?>
	</div>
	
	<div class="woocl-price">
		<span class="woocl-x-large"><span class="woocl-price-bin"><?php echo $product_price;?></span></span><?php 
		if( !empty( $saved_price ) ) {?>
			<div class="woocl-discount"><?php _e('You save','woocl');?><span class="woocl-price-amount-saved"><?php echo ' '.$saved_price;?></span> (<span class="woocl-price-savings-percent"><?php echo $saved_per;?></span>%)</div><?php 
		} ?>
		<div class="woocl-instock"><?php echo apply_filters( 'woo_cl_coll_item_stock_popup_cntnt', $sale_stock );?></div>
		<div class="woocl-product-disc"><?php echo apply_filters( 'woo_cl_coll_item_desc_popup_cntnt', $product_desc );?></div>
	</div>

	<?php do_action( 'woo_cl_change_variation_product_data', $product, $coll_id, $coll_item_id, $product_id ); ?>
	
	<div class="woocl-summary-cart"><?php 
			
		if( $is_published ) { ?>
			<div class="woocl-add-btn-wrapper ">
				<a href="<?php echo empty($external_url) ? $product_url : $external_url;?>" class="woocl-img-button" id="woocl-collectitem"><?php _e('More details','woocl');?></a>
			</div>
			<?php if( $in_stock && $purchasable ) { ?>
			<div class="woocl-position woocl-add-cart-btn">
				<div class="woocl-add-cart-wrap woo_cl_star_icon">
					<a title="<?php _e('Add to Cart','woocl');?>" id="woocl-add-to-cart" class="woocl-add-to-cart" href="javascript:void(0);"><?php _e('Add to Cart','woocl');?></a>
					<i class="woo-cl-cart-success">&nbsp;</i>
					<div class="woo_cl_add_cart_loader">
						<img src="<?php echo WOO_CL_IMG_URL.'/woo-cl-loader.gif' ?>" alt="loader"/>
						<span class="woo_cl_add_cart_msg"><?php _e(' Adding...','woocl');?></span>
					</div>
					<a title="<?php _e('View Cart','woocl');?>" target="_blank" class="woocl-view-cart" href="<?php echo $cart_page_url;?>"><?php _e('View Cart','woocl');?></a>
					<div class="woocl-clear"></div>
				</div>
				<div class="woo_cl_add_cart_message"></div>
				<input type="hidden" class="woo-cl-product_id" value="<?php echo $product_id;?>" />
				<input type="hidden" class="woo-cl-variarion_id" value="<?php echo $variation_id;?>"/>
				<input type="hidden" class="woo-cl-variarion_data" value='<?php echo htmlspecialchars( json_encode($selected_variation) );?>'/>
			</div>
		<?php }
		}?>
		
		<table class="woocl-product-details">
			<tbody>
				<tr>
					<th class="th-width"><?php _e('SKU','woocl')?></th>
					<td><?php echo sprintf( ': %s', $product_sku );?></td>
				</tr><?php 
				if( !empty( $product_weight ) ) {?>
					
					<tr>
						<th class="th-width"><?php _e('Weight','woocl')?></th>
						<td><?php echo sprintf( ': %s', $product_weight );?></td>
					</tr><?php 
					
				} if( !empty( $product_dimention ) ) { ?>
					
					<tr>
						<th class="th-width"><?php _e('Dimensions','woocl')?></th>
						<td><?php echo sprintf( ': %s', $product_dimention );?></td>
					</tr><?php 
					
				} if( !empty( $product_categories ) ) {?>
					
					<tr>
						<th class="th-width"><?php _e(' Category','woocl')?></th>
						<td><?php echo sprintf( ': %s', $product_categories );?></td>
					</tr><?php 
					
				} if( !empty( $product_tags ) ) {?>
					
					<tr>
						<th class="th-width"><?php _e('Tags','woocl')?></th>
						<td><?php echo sprintf( ': %s', $product_tags );?></td>
					</tr><?php 
					
				} ?>
			</tbody>
		</table>
	</div>
</div><!-- end of right-part-->