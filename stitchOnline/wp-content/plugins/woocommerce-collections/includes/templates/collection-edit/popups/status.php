<?php

/**
 * Template For Collection Status
 * 
 * Handles to return design for change collection status
 * 
 * Override this template by copying it to yourtheme/woocommerce/woocommerce-collections/collection-edit/popups/status.php
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<!--- PUBLIC POPUP  -->
<div class="woocl-modal-inwrapper">
	<div class="woocl-modal-nextwrapper">
		<div class="woocl-middlepart">
			<div class="woocl-wrapper">
			  <div class="woocl-border">
				<div class="woocl-toper">
					<div class="woocl-closer"></div>
				</div><!-- end of top -->
				</div><!-- end of wrapper -->
				<div class="woocl-innerblock">
					<div class="woocl-wrapper">
						<div class="woocl-message">
							<div class="woocl-warning">
							<div class="woocl-pub"><p><?php _e('Are you sure you want','woocl');?></p><p><?php echo $coll_status_text;?></p></div>
							</div><!-- end of warning -->
							<div class="woocl-subtext"><?php echo $coll_status_subtext;?></div>
						</div><!-- end of message -->
								 <div class="woocl-clear"></div>
						 <div class="woocl-click">
						 <div class="woocl-buttons">
						 	<div class="woocl-cancel"><button type="button" class="woocl-btn" id="woocl-collectitem"><?php _e('Cancel','woocl');?></button></div>
						 	<div class="woocl-confirm">
						 		<button type="button" class="woocl-btn" id="woocl-coll-status">
						 			<span class="woocl_btn_text"><?php echo $coll_status_btntext;?></span>
						 		</button>
					 			<div class="woo_cl_loader">
							 		<img src="<?php echo WOO_CL_IMG_URL.'/woo-cl-loader.gif';?>" class="woo_cl_loader_img">
							 		<span class="woo_cl_updating_msg"><?php _e('Updating...','woocl');?></span>
					 			</div>
						 		<input type="hidden" class="woocl-coll-id" value="<?php echo $coll_id;?>"/>
						 		<input type="hidden" class="woocl-coll-status" value="<?php echo $coll_status_val;?>"/>
						 	</div>
						 </div><!-- end of button -->
						 </div><!-- end of click -->
					 </div><!-- end of wrapper-->
				 </div><!-- end of inner-->
				<div class="woocl-clear"></div>
			</div> <!-- end of border -->
		</div>	<!-- end of middlepart-->
		</div><!-- end of modal-wrapper1 -->
	<div class="woocl-overlay"></div>
</div>	<!-- end of modal-inwrapper -->