<?php
/**
 * Template For Collection Edit
 * 
 * Handles to return design collection edit
 * 
 * Override this template by copying it to yourtheme/woocommerce/woocommerce-collections/collection-edit/collection-edit.php
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="woocl-collection">
	<form method="POST" class="woocl-edit-form" action="">
	  <div class="woocl-collection-info">
	     	<div class="woocl-titlename">
	     		<h1 title="<?php echo sprintf( __('Explore %1$s: My %1$s','woocl'), woo_cl_get_label_singular() );?>" class="woocl-collectiontitle"><input type="text" name="woocl_coll_title" value="<?php echo $coll_title;?>" class="woocl-edittitle"></h1>
	     	</div>
	     	<div class="woocl-acts">
	     		<img src="<?php echo WOO_CL_IMG_URL.'/woo-cl-loader.gif' ?>" id="woo_cl_update_loader" alt="image"><span class="woo_cl_update_msg"><?php _e(' Updating...','woocl');?></span>
	     		<button type="button" id="woocl-update-collection" class="woocl-click-button"><?php _e('Done editing','woocl');?></button>
	     		<input type="hidden" name="woocl_collection_id" class="woocl-collection-id" value="<?php echo $coll_id;?>"/>
	     	</div>
	     	<div class="woocl-clear"></div>   	
		  <?php 
			if( !empty( $user_name ) ) { //if user name pass by url
				
				$created_by_text = sprintf( '%s <a href="%s">%s</a>', __('Created by','woocl'), $author_coll_url, $user_name );
			?>
			
			<div class="woocl-titlename woocl-created-text">
				<a href="<?php echo $author_coll_url;?>">
					<div class="woocl-user-photo">
						<?php echo $author_avatar;?>
					</div>
				</a>
				<div class="woocl-user-curatedby">
					<div class="woocl-user-name"><?php echo $created_by_text;?></div>
					<div class="woocl-lastupdated">
						<span class="woocl-lastupdatedLabel"><?php _e('Last updated: ','woocl');?></span><span class="woocl-collectionupdated"><?php echo $coll_modified_date;?></span>
					</div>
				</div>
			</div>
			<?php }?>	     	
	     	<div class="woocl-innerline">
	     		<div class="woocl-entertext">
	     			<?php 
	     			if( $enable_editor_coll_desc == 'yes' ) {

						$settings = array( 'media_buttons' => false , 'tinymce' => true, 'textarea_rows' => 2 );
	     				wp_editor( $coll_content, 'woocl_coll_desc', $settings );
	     			} else {?>
	     			<textarea name="woocl_coll_desc" placeholder="<?php echo sprintf( __("Describe your %s. Say something about why you created it or why it's special.", "woocl"), woo_cl_get_label_singular() );?>" class="woocl-editdescription"><?php echo $coll_content;?></textarea>
	     			<?php } ?>
	     		</div>
	     	</div>
	  </div><!--- end woocl-collection-info -->
	  <div class="woocl-clear"></div>
	  <div class="woocl-items">
	  <?php
		if(!empty($coll_items)) {?>
	
	  		<div class="woocl-collect-item woocl-clearfix">
	  		   <span><?php echo $coll_item_total;?></span><span><?php _e('items','woocl');?></span>
	  		</div>
	  		<div class="woocl-headerseparator">|</div>
	 <?php }

			//Check privacy option enabled
			if( $front_coll_privacy == 'yes' ) {

				$coll_change_status = '';
		  		$coll_change_status .= '<div class="woocl-privacy">';
		  		$coll_change_status .= '<div class="' . $coll_status_class . '"></div>';
		  		$coll_change_status .= '<div class="woocl-pr-text"><span>' . $coll_status_text . '</span></div>';
		  		$coll_change_status .= '</div>';
		  		$coll_change_status .= '<div class="woocl-headerseparator">|</div>';

		  		echo apply_filters( 'wool_allow_status_change', $coll_change_status );
		  	}?>
	  		<div class="woocl-delete">
	  		<span><?php echo sprintf( __('Delete this %s','woocl'), woo_cl_get_label_singular() );?></span>
	  		</div>		 
	  </div><!--- end woocl-items -->
	  	
	  <div class="woocl-update">	
	  		<div class="woocl-lastupdated">
	  		<span class="woocl-lastupdated-label"><?php _e('Last updated: ','woocl');?></span>
	  		<span class="woocl-collection-updated"><?php echo $coll_modified_date;?></span>
	  		</div>
	  </div>	<!--- end woocl-update -->
	  <div class="woocl-clear"></div>
	  <div class="woocl-collections">
		<?php
		if(!empty($coll_items)) {
			
			foreach ($coll_items as $key=>$coll_item) {
				 do_action('woo_cl_edit_collection_data', $coll_item, $coll_item_total,$col_count );
				 $col_count++;
			}
	
		} else {
	
			$content = '<div class="woo_cl_errors woo_cl_msgs"><p class="woo_cl_message">';
			
			$content .= sprintf( __( 'You dont have any %s Items.', 'woocl' ), woo_cl_get_label_singular() );
			
			$content .= '</p></div>';
			
			echo $content;
			
		} ?>
	  </div>
	  <div class="woocl-clear"></div>
	</form>
</div><!--- end woocl-coolection -->

<!-- do action for Collection Delete ,Colver Image ,Status Popup -->
<?php //do_action( 'woo_cl_collection_edit_popup_container', $coll_id );?>


<?php

	// Create Add To Collection Popup
	//do_action( 'woo_cl_add_to_collection_popup' );
?>