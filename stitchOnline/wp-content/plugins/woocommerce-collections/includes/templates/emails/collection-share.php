<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Collection Share Notification
 * 
 * Type : HTML
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
?>

<?php do_action( 'woocommerce_email_header', $email_heading ); ?>

<p><?php _e( 'Hi!', 'woocl' ); ?></p>

<p><?php 
	echo sprintf( __( '%s has suggested you look at this %s from %s:', 'woocl' ), $sender_name, woo_cl_get_label_singular(), $site_name );
	echo '<br />';
	echo '<a href="' . $sharelink . '">' . $sharelink . '</a>';
?></p>

<p><?php echo $message;?></p>

<p><?php echo sprintf( __( 'Reply to %s by emailing %s', 'woocl' ), $sender_name, '<a href="mailto:'.$sender_email.'">' . $sender_email . '</a>' );?></p>

<?php do_action( 'woocommerce_email_footer' ); ?>