<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Collection Share Notification
 * 
 * Type : Plain
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

//echo $email_heading . "\n\n";

echo __( "Hi!", 'woocl' ) . "\n\n";

echo sprintf( __( "%s has suggested you look at this %s from %s:", 'woocl' ), $sender_name, woo_cl_get_label_singular(), $site_name );

echo "\n" . $sharelink . "\n\n";

echo $message . "\n\n";

echo sprintf( __( "Reply to %s by emailing %s", 'woocl' ), $sender_name, $sender_email ) . "\n\n";

echo apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) );