<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//Action to initialize widget
add_action( 'widgets_init', 'woo_cl_recently_viewed_widget' );

/**
 * Register the Recently Viewed Collections
 * 
 * Handles to register a widget
 * for showing recently viewed collections
 *
 * @package WPWeb WooCommerce Collections
 * @since 1.1.5
 */
function woo_cl_recently_viewed_widget() {
	register_widget( 'Woo_Cl_Recently_Viewed' );
}

/**
 * Recent Collections Widget Class
 * 
 * Handles all the features and functions for recently widget.
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.1.5
 */
class Woo_Cl_Recently_Viewed extends WP_Widget {

	/**
	 * Widget setup.
	 */
    function __construct() {

    	global $woo_cl_model;

    	parent::__construct(

	        // base ID of the widget
	        'woo_cl_recently_viewed_collections',

	        // name of the widget
	        sprintf( __( 'Recently Viewed %s', 'woocl' ), woo_cl_get_label_plural() ),

	        // widget options
	        array (
	            'description' 	=> sprintf( __( 'Display a list of recently viewed %s.', 'woocl' ), woo_cl_get_label_plural(true) ),
	            'classname' 	=> 'woo_cl_recently_viewed_collections',
	        )
	    );

	    $this->model = $woo_cl_model;
    }
	
	/**
	 * Displays the widget form in the admin panel
	 * 
	 * Handles to show widget settings at backend
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
    function form( $instance ) {

    	// outputs the options form on admin
		$instance 		= wp_parse_args( (array) $instance, array( 'title' => sprintf( __( 'Recently Viewed %s', 'woocl' ), woo_cl_get_label_plural() ), 'no_of_colls' => 10 ) );
    	$title 			= $this->model->woo_cl_escape_attr($instance['title']);
		$number_of_cl 	= $this->model->woo_cl_escape_attr($instance['no_of_colls']);
		?>
		
		<!-- Title: Text Box -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'woocl' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		
		<!-- First Name: Text Box -->
		<p>
			<label for="<?php echo $this->get_field_id( 'no_of_colls' ); ?>"><?php sprintf( __( 'Number of %s to show:', 'woocl' ), woo_cl_get_label_plural(true) ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'no_of_colls' ); ?>" name="<?php echo $this->get_field_name( 'no_of_colls' ); ?>" type="number" value="<?php echo esc_attr($number_of_cl); ?>" />
		</p>
		<?php
    }
	
	/**
	 * Updates the widget control options for the particular instance of the widget
	 *
	 * Handles to update widget data
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
    function update( $new_instance, $old_instance ) {

    	// processes widget options to be saved
		$instance = $old_instance;
		$instance['title'] 		 = $this->model->woo_cl_escape_slashes_deep( $new_instance['title'] );
		$instance['no_of_colls'] = $this->model->woo_cl_escape_slashes_deep( $new_instance['no_of_colls'] );

		return $new_instance;
    }

	/**
	 * Outputs the content of the widget
	 * 
	 * Handles to show output of widget 
	 * at front side sidebar
	 * 
	 * @package WPWeb WooCommerce Collections
 	 * @since 1.1.5
	 */
    function widget( $args, $instance ) {

    	// outputs the content of the widget
		extract($args);

		//Get viewed collections
		$viewed_collections = ! empty( $_COOKIE['woo_cl_recently_viewed'] ) ? (array) explode( '|', $_COOKIE['woo_cl_recently_viewed'] ) : array();
		$viewed_collections = array_filter( array_map( 'absint', $viewed_collections ) );

		//Return if none of viewed yet
		if ( empty( $viewed_collections ) )	return;

		//Get details
		$title = apply_filters( 'woo_cl_recently_viewed_widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$no_of_colls = empty( $instance['no_of_colls'] ) ? '' : $instance['no_of_colls'];

		//Arguments
		$query_args = array(
			'posts_per_page'=> $no_of_colls,
			'post__in' 		=> $viewed_collections,
			'orderby' 		=> 'post__in'
		);

		//Get recent collections
		$recent_collections = $this->model->woo_cl_get_collections( $query_args );

		echo $before_widget;

		if ( $title ) echo $before_title . $title . $after_title;

		if ( !empty( $recent_collections ) ) {

			echo '<ul class="woocl_collection_list_widget">';
			foreach ( $recent_collections as $key => $collection ) {?>
				<li>
					<a href="<?php echo esc_url( get_permalink( $collection['ID'] ) ); ?>" title="<?php echo esc_attr( $collection['post_title'] );?>">
						<span class="product-title"><?php echo $collection['post_title'];?></span>
					</a>
				</li>
				<?php
			}
			echo '</ul>';
		}

		echo $after_widget;
	}
}