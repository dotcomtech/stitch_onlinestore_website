<?php
/**
 * Plugin Name: WPWeb WooCommerce Collections
 * Plugin URI:  http://wpweb.co.in/
 * Description: WooCommerce Collections gives your customers the ability to gather and share products they like, want or recommend.
 * Version: 1.2.4
 * Author: WPWeb
 * Author URI: http://wpweb.co.in
 * Text Domain: woocl
 * Domain Path: languages
 * 
 * @package WPWeb WooCommerce Collections
 * @category Core
 * @author WPWeb
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Basic plugin definitions
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */

if( !defined( 'WOO_CL_LIB_VER' ) ) {
	define( 'WOO_CL_LIB_VER', '1.2.4' ); //libraray version of js and css
}
if( !defined( 'WOO_CL_DIR' ) ) {
	define( 'WOO_CL_DIR', dirname( __FILE__ ) ); // plugin dir
}
if( !defined( 'WOO_CL_URL' ) ) {
	define( 'WOO_CL_URL', plugin_dir_url( __FILE__ ) ); // plugin url
}
if( !defined( 'WOO_CL_IMG_URL' ) ) {
	define( 'WOO_CL_IMG_URL', WOO_CL_URL . 'includes/images' ); // plugin images url
}
if( !defined( 'WOO_CL_META_DIR' ) ) {
	define( 'WOO_CL_META_DIR', WOO_CL_DIR . '/includes/meta-boxes' ); // path to meta boxes
}
if( !defined( 'WOO_CL_META_URL' ) ) {
	define( 'WOO_CL_META_URL', WOO_CL_URL . 'includes/meta-boxes' ); // url to meta boxes
}
if( !defined( 'WOO_CL_ADMIN' ) ) {
	define( 'WOO_CL_ADMIN', WOO_CL_DIR . '/includes/admin' ); // plugin admin dir
}
if( !defined( 'WOO_CL_POST_TYPE_COLL' ) ) {
	define( 'WOO_CL_POST_TYPE_COLL', 'woocollections' ); // post type for Collection
}
if( !defined( 'WOO_CL_POST_TYPE_COLLITEMS' ) ) {
	define( 'WOO_CL_POST_TYPE_COLLITEMS', 'woocollitems' ); // post type for Collection Items
}
if( !defined( 'WOO_CL_PRODUCT_POSTTYPE' ) ) {
	define( 'WOO_CL_PRODUCT_POSTTYPE', 'product' ); //woocommerce Product Post type
}
if( !defined( 'WOO_CL_META_PREFIX' ) ) {
	define( 'WOO_CL_META_PREFIX', '_woo_cl_' ); // meta box prefix
}
if( !defined( 'WOO_CL_BASENAME' ) ) {
	define( 'WOO_CL_BASENAME', basename( WOO_CL_DIR ) ); // base name
}
if( !defined( 'WOO_CL_MAIN_MENU_NAME' ) ) {
	define( 'WOO_CL_MAIN_MENU_NAME', 'woocommerce' ); //woocommerce main menu name
}
if( !defined( 'WOO_CL_POST_PER_PAGE' ) ) {
	define( 'WOO_CL_POST_PER_PAGE', 10 ); //default value for collections per page
}
if( !defined( 'WOO_CL_NUM_COL_COLL_LIST' ) ) {
	define( 'WOO_CL_NUM_COL_COLL_LIST', 3 ); //default value for collections per page
}
if( !defined( 'WOO_CL_NO_OF_MY_COLLECTION_ITEMS' ) ) {
	define( 'WOO_CL_NO_OF_MY_COLLECTION_ITEMS', 3 ); //default value for no of collections items on my acocunt section
}
if ( ! defined( 'WOO_CL_PLUGIN_KEY' ) ) {
	define( 'WOO_CL_PLUGIN_KEY', 'woocl' );
}
if( !defined( 'WOO_CL_DEFAULT_VIEW' ) ) {
	define( 'WOO_CL_DEFAULT_VIEW', 'view-collection' ); //libraray version of js and css
}
if( !defined( 'WOO_CL_DEFAULT_EDIT' ) ) {
	define( 'WOO_CL_DEFAULT_EDIT', 'edit-collection' ); //libraray version of js and css
}
if( !defined( 'WOO_CL_DEFAULT_AUTHOR' ) ) {
	define( 'WOO_CL_DEFAULT_AUTHOR', 'collection-author' ); //libraray version of js and css
}

// Required Wpweb updater functions file
if ( ! function_exists( 'wpweb_updater_install' ) ) {
	require_once( 'includes/wpweb-upd-functions.php' );
}

//Include functions file
require_once( WOO_CL_DIR . '/includes/woo-cl-misc-functions.php' );

//post type class handles most of functionalities of plugin
require_once( WOO_CL_DIR . '/includes/woo-cl-post-types.php' );

global $woo_cl_query;

//loads Query class
include( WOO_CL_DIR . '/includes/class-woo-cl-query.php' );
$woo_cl_query = new Woo_Cl_Query();
$woo_cl_query->add_hooks();

/**
 * Activation Hook
 * 
 * Register plugin activation hook.
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
register_activation_hook( __FILE__, 'woo_cl_install' );

/**
 * Plugin Setup (On Activation)
 * 
 * Does the initial setup,
 * stest default values for the plugin options.
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_install() {
	
	global $wpdb, $current_user, $woo_cl_query;
	
	//Meta Prefix
	$prefix = WOO_CL_META_PREFIX;
	
	//Get User ID
	$user_ID = $current_user->ID;
	
	//Register Post Type
	woo_cl_register_post_types();
	
	//add endpoints to query vars
	//Need to call before flush rewrite rules
	$woo_cl_query->woo_cl_add_endpoints();
	
	//Need to call when custom post type is being used in plugin
	flush_rewrite_rules();
	
	//get option for when plugin is activating first time
	$woo_cl_set_option = get_option( 'cl_set_option' );
	
	if( empty( $woo_cl_set_option ) ) { //check plugin version option
		
		//update default options
		woo_cl_default_settings();
		
		//update plugin version to option
		update_option( 'cl_set_option', '1.0' );
	}
	
	//get version option
	$woo_cl_set_option = get_option( 'cl_set_option' );
	
	if( $woo_cl_set_option == '1.0' ) {
		
		update_option( 'cl_no_of_col_items_list', WOO_CL_NUM_COL_COLL_LIST );
		
		//update plugin version to option
		update_option( 'cl_set_option', '1.1' );
	}

	//get version option
	$woo_cl_set_option = get_option( 'cl_set_option' );
	
	if( $woo_cl_set_option == '1.1' ) {

		//Update front coll privacy option enabled
		update_option( 'cl_front_coll_privacy', 'yes' );
		update_option( 'cl_enable_woo_myaccount_page', 'yes' );

		//update plugin version to option
		update_option( 'cl_set_option', '1.2' );
	}

	//get version option
	$woo_cl_set_option = get_option( 'cl_set_option' );
	
	if( $woo_cl_set_option == '1.2' ) {
		
		update_option( 'cl_display_product_collections_text', sprintf(__('Collections: {collections}','woocl'), woo_cl_get_label_plural(true), woo_cl_get_label_plural(true) ) );
		update_option( 'cl_enable_search_on_collections', 'yes' );
		update_option( 'cl_enable_search_on_collection_items', 'yes' );
		update_option( 'cl_enable_sorting_on_collections', 'yes' );
		update_option( 'cl_enable_sorting_on_collection_items', 'yes' );
		update_option( 'cl_enable_front_drag_item_ordering', 'yes' );
		
		//update plugin version to option
		update_option( 'cl_set_option', '1.3' );
	}
	
	//get version option
	$woo_cl_set_option = get_option( 'cl_set_option' );
	
	if( $woo_cl_set_option == '1.3' ) {
		//future code here.
	}
}

/**
 * Load Text Domain
 * 
 * This gets the plugin ready for translation.
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_load_text_domain() {
	
	// Set filter for plugin's languages directory
	$woo_cl_lang_dir	= dirname( plugin_basename( __FILE__ ) ) . '/languages/';
	$woo_cl_lang_dir	= apply_filters( 'woo_cl_languages_directory', $woo_cl_lang_dir );
	
	// Traditional WordPress plugin locale filter
	$locale	= apply_filters( 'plugin_locale',  get_locale(), 'woocl' );
	$mofile	= sprintf( '%1$s-%2$s.mo', 'woocl', $locale );
	
	// Setup paths to current locale file
	$mofile_local	= $woo_cl_lang_dir . $mofile;
	$mofile_global	= WP_LANG_DIR . '/' . WOO_CL_BASENAME . '/' . $mofile;
	
	if ( file_exists( $mofile_global ) ) { // Look in global /wp-content/languages/woocommerce-collections folder
		load_textdomain( 'woocl', $mofile_global );
	} elseif ( file_exists( $mofile_local ) ) { // Look in local /wp-content/plugins/woocommerce-collections/languages/ folder
		load_textdomain( 'woocl', $mofile_local );
	} else { // Load the default language files
		load_plugin_textdomain( 'woocl', false, $woo_cl_lang_dir );
	}
}

/**
 * Check if current page is edit page.
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_is_edit_page() {
	
	global $pagenow;
	
	return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
}

/**
 * Default settings
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_default_settings() {
	
	//Arguments for create mycollection page
	$mycollection_page = array(
								'post_type' 	=> 'page',
								'post_status' 	=> 'publish',
								'post_title' 	=> woo_cl_get_label_plural(),
								'post_content' 	=> '[woo_cl_collections][/woo_cl_collections]',
								'menu_order' 	=> 0,
								'comment_status'=> 'closed'
							);
	
	//create mycollection page
	$mycollection_page_id = wp_insert_post( $mycollection_page );
	
	// this option contains all page ID
	$cl_all_pages = array(
							'cl_coll_lists_page'	=> 	$mycollection_page_id,
						);
	
	//Loop for update all page options
	foreach ($cl_all_pages as $key => $value) {
		
		update_option( $key, $value );
	}
	
	//Get default email subject option
	$cl_email_subject	= '[' . sprintf( __( 'New %s Item', 'woocl' ), woo_cl_get_label_singular() ) . '] {collection_item_name}';
	
	//Get default email body option
	$cl_email_body		= sprintf( __(' New %1$s item added under the %1$s: %s', 'woocl'), woo_cl_get_label_singular(), '{collection_name}' ).
							"\n\n{collection_item_name}".
							"\n\n{collection_item_description}";

	//Get all settings options array
	$options = array(
					'cl_delete_options'				=> '',
					'cl_add_to_collection_btn_text'	=> __('Add to Collections','woocl'),
					'cl_add_to_collection_btn_type'	=> 'default',
					'cl_add_to_collection_link_icon'=> 'fa fa-star',
					'cl_pagination_options'			=> 10,
					'cl_no_of_col_items_list'		=> WOO_CL_NUM_COL_COLL_LIST,
					'cl_scroll_options'				=> '',
					'cl_enable_coll_add_all_to_cart'=> '',
					'cl_collection_subtitle_text'	=> '',
					'cl_enable_follow_coll_author'	=> '',
					'cl_enable_editor_coll_desc'	=> '',
					'cl_front_coll_privacy'     	=> 'yes',
					'cl_new_coll_privacy'     		=> 'public',
					'cl_enable_woo_myaccount_page'  => 'yes',
					'cl_collection_terminology'  	=> '',
					'cl_collection_terminology_singular_text'=> '',
					'cl_collection_terminology_plural_text'  => '',
					'cl_enable_front_drag_item_ordering'	 => '',
					'cl_display_product_collections_text'	 => sprintf(__('you already have added this in below %s: {%s}','woocl'), woo_cl_get_label_plural(true), woo_cl_get_label_plural(true) ),
					'cl_enable_search_on_collections'	 	 => 'yes',
					'cl_enable_search_on_collection_items'	 => 'yes',
					'cl_enable_sorting_on_collections'	 	 => 'yes',
					'cl_enable_sorting_on_collection_items'	 => 'yes',
					'cl_max_collections_per_user'	=> '',
					'cl_max_products_per_collection'=> '',
					'cl_sharing_facebook'			=> '',
					'cl_sharing_twitter'			=> '',
					'cl_sharing_google'				=> '',
					'cl_sharing_linkedin'			=> '',
					'cl_sharing_email'				=> 'yes',
					'cl_custom_css'					=> '',
					'cl_email_subject'				=> $cl_email_subject,
					'cl_email_body'					=> $cl_email_body
				);
	
	//Loop for update all settings options
	foreach( $options as $key => $value ) {
		update_option( $key, $value );
	}
}

/**
 * Add plugin action links
 * 
 * Adds a Settings, Support and Docs link to the plugin list.
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_add_plugin_links( $links ) {
	
	$plugin_links = array(
		'<a href="admin.php?page=wc-settings&tab=woo-cl-settings">' . __( 'Settings', 'woocl' ) . '</a>',
		'<a href="http://support.wpweb.co.in/">' . __( 'Support', 'woocl' ) . '</a>',
		'<a href="http://wpweb.co.in/documents/woocommerce-collections/">' . __( 'Docs', 'woocl' ) . '</a>'
	);
	
	return array_merge( $plugin_links, $links );
}

add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'woo_cl_add_plugin_links' );

//add action to load plugin
add_action( 'plugins_loaded', 'woo_cl_plugin_loaded' );

/**
 * Load Plugin
 * 
 * Handles to load plugin after
 * dependent plugin is loaded
 * successfully
 * 
 * @package WPWeb WooCommerce Collections
 * @since 1.0.0
 */
function woo_cl_plugin_loaded() {
	
	if( class_exists( 'Woocommerce' ) ) { //check Woocommerce is activated or not
		
		// load first plugin text domain
		woo_cl_load_text_domain();
		
		/**
		 * Deactivation Hook
		 * 
		 * Register plugin deactivation hook.
		 * 
		 * @package WPWeb WooCommerce Collections
		 * @since 1.0.0
		 */
		register_deactivation_hook( __FILE__, 'woo_cl_uninstall');
		
		/**
		 * Plugin Setup (On Deactivation)
		 * 
		 * Delete  plugin options.
		 * 
		 * @package WPWeb WooCommerce Collections
		 * @since 1.0.0
		 */
		function woo_cl_uninstall() {
			
			global $wpdb;
			
			//IMP Call of Function
			//Need to call when custom post type is being used in plugin
			flush_rewrite_rules();
			
			// Getting delete option
			$woo_cl_delete_options = get_option( 'cl_delete_options' );
			
			// If option is set
			if( isset( $woo_cl_delete_options ) && !empty( $woo_cl_delete_options ) && $woo_cl_delete_options == 'yes' ) {
				
				// Delete collection data
				$post_types = array( 'woocollections', 'woocollitems' );
				
				//loop for all post type posts remove
				foreach ( $post_types as $post_type ) {
					
					$args		= array( 'post_type' => $post_type, 'post_status' => 'any', 'numberposts' => '-1' );
					$all_posts	= get_posts( $args );
					foreach ( $all_posts as $post ) {
						wp_delete_post( $post->ID, true);
					}
				}
				
				//Get all settings options for remove
				$options = array(
								'cl_add_to_collection_btn_text',
								'cl_add_to_collection_btn_type',
								'cl_add_to_collection_link_icon',
								'cl_sort_collection_by',
								'cl_sort_collection_item_by',
								'cl_sharing_facebook',
								'cl_sharing_twitter',
								'cl_sharing_google',
								'cl_sharing_linkedin',
								'cl_sharing_email',
								'cl_custom_css',
								'cl_disable_role_list_administrator',
								'cl_pagination_options',
								'cl_scroll_options',
								'cl_enable_coll_add_all_to_cart',
								'cl_collection_subtitle_text',
								'cl_enable_follow_coll_author',
								'cl_enable_editor_coll_desc',
								'cl_front_coll_privacy',
								'cl_new_coll_privacy',
								'cl_enable_woo_myaccount_page',
								'cl_collection_terminology',
								'cl_collection_terminology_singular_text',
								'cl_collection_terminology_plural_text',
								'cl_enable_front_drag_item_ordering',
								'cl_display_product_collections_text',
								'cl_enable_search_on_collections',
								'cl_enable_search_on_collection_items',
								'cl_enable_sorting_on_collections',
								'cl_enable_sorting_on_collection_items',
								'cl_delete_options',
								'cl_set_option',
								'cl_email_subject',
								'cl_email_body',
								'cl_max_collections_per_user',
								'cl_max_products_per_collection',
								'cl_no_of_col_items_list',
							);
				
				// Delete all options
				foreach ( $options as $option ) {
					delete_option( $option );
				}
				
				// this option contains all page ID
				$cl_all_pages = array(
										'cl_coll_lists_page'
									);
				
				//loop for all page option remove
				foreach ($cl_all_pages as $key) {
					
					$keyvalue = get_option( $key );
					wp_delete_post($keyvalue, true);
					delete_option( $key );
				}
			} // End of if
		}
		
		//global variables
		global $woo_cl_model,$woo_cl_admin,$woo_cl_scripts,$woo_cl_shortcode,
				$woo_cl_settings_tabs,$woo_cl_public, $woo_cl_metabox, $woo_cl_admin_meta;
		
		//Model class handles most of functionalities of plugin
		include_once( WOO_CL_DIR . '/includes/class-woo-cl-model.php' );
		$woo_cl_model = new Woo_Cl_Model();
		
		// Script Class to manage all scripts and styles
		include_once( WOO_CL_DIR . '/includes/class-woo-cl-scripts.php' );
		$woo_cl_scripts = new Woo_Cl_Scripts();
		$woo_cl_scripts->add_hooks();
		
		//collection public class for handling
		require_once( WOO_CL_DIR . '/includes/class-woo-cl-public.php' );
		$woo_cl_public = new Woo_Cl_Public();
		$woo_cl_public->add_hooks();

		// Admin meta class to handles most of html design for collections panel
		require_once( WOO_CL_ADMIN . '/class-woo-cl-admin-meta.php' );
		$woo_cl_admin_meta = new Woo_Cl_Admin_Meta();

		//Admin Pages Class for admin side
		require_once( WOO_CL_ADMIN . '/class-woo-cl-admin.php' );
		$woo_cl_admin = new Woo_Cl_Admin();
		$woo_cl_admin->add_hooks();
		
		//shortcode class for handling shortcode content
		require_once( WOO_CL_DIR . '/includes/class-woo-cl-shortcodes.php' );
		$woo_cl_shortcode = new Woo_Cl_Shortcodes();
		$woo_cl_shortcode->add_hooks();
		
		if( woo_cl_is_edit_page() ) {//If new post or edit post page
			
			//include the meta functions file for metabox
			require_once ( WOO_CL_META_DIR . '/class-woo-cl-meta-box.php' );
			$woo_cl_metabox = new Woo_Cl_Meta_Box();
			$woo_cl_metabox->add_hooks();
		}

		//Register Widget - Commented
		//require_once( WOO_CL_DIR . '/includes/widgets/class-woo-cl-recent-viewed.php' );

		// loads the Templates Functions file
		require_once( WOO_CL_DIR . '/includes/woo-cl-template-functions.php' );

		//Load Template Hook File
		require_once( WOO_CL_DIR . '/includes/woo-cl-template-hooks.php' );

	}//end if to check class Woocommerce is exist or not
}//end if to check plugin loaded is called or not


if( class_exists( 'Wpweb_Upd_Admin' ) ) {//check Social Updater is activated
	
	// Plugin updates
	wpweb_queue_update( plugin_basename( __FILE__ ), WOO_CL_PLUGIN_KEY );
	
	/**
	 * Include Auto Updating Files
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	require_once( WPWEB_UPD_DIR . '/updates/class-plugin-update-checker.php' ); // auto updating
	
	$WpwebWooclUpdateChecker = new WpwebPluginUpdateChecker (
		'http://wpweb.co.in/Updates/WOOCL/license-info.php',
		__FILE__,
		WOO_CL_PLUGIN_KEY
	);
	
	/**
	 * Auto Update
	 * 
	 * Get the license key and add it to the update checker.
	 * 
	 * @package WPWeb WooCommerce Collections
	 * @since 1.0.0
	 */
	function woo_cl_add_secret_key( $query ) {
		
		$plugin_key	= WOO_CL_PLUGIN_KEY;
		
		$query['lickey'] = wpweb_get_plugin_purchase_code( $plugin_key );
		return $query;
	}
	
	$WpwebWooclUpdateChecker->addQueryArgFilter( 'woo_cl_add_secret_key' );
} // end check WPWeb Updater is activated
