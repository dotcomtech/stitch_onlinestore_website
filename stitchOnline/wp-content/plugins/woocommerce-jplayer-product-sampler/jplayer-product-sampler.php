<?php
/*
Plugin Name: WooCommerce JPlayer Product Sampler
Plugin URI: http://woothemes.com/woocommerce
Description: Enable customers to view samples of digital video and music products through JPlayer on the product page.
Version: 1.4.1
Author: Automattic
Author URI: https://woocommerce.com
Requires at least: 3.8
Tested up to: 4.3.1

Copyright: � 2014 Gerhard Potgieter
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Required functions.
 */
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

/**
 * Plugin updates.
 */
woothemes_queue_update( plugin_basename( __FILE__ ), 'f9dded9a5c65a4555e5ce51ab6e1a3f5', '18662' );

if ( is_woocommerce_active() ) {

	/**
	 * Localisation.
	 **/
	load_plugin_textdomain( 'wc_jplayer', false, dirname( plugin_basename( __FILE__ ) ) . '/' );

	/**
	 * Instantiate main class.
	 **/
	if ( ! class_exists( 'WC_JPlayer_Product_Sampler' ) ) {
		require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . 'includes/class-wc-jplayer.php' );
	}
	$_GLOBALS['wc_jplayer_product_sampler'] = new WC_JPlayer_Product_Sampler( __FILE__, '1.4.1' );
}
