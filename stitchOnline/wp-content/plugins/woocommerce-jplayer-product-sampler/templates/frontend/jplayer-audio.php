<div id="<?php echo esc_attr( $jplayer_id ); ?>" class="jp-jplayer"></div>
<div id="<?php echo esc_attr( $jplayer_container_id ); ?>" class="jp-audio">
	<div class="jp-type-playlist">
		<div class="jp-gui jp-interface">
			<ul class="jp-controls">
				<li><a href="javascript:;" class="jp-previous" tabindex="1"><?php esc_html_e( 'previous', 'wc_jplayer' ); ?></a></li>
				<li><a href="javascript:;" class="jp-play" tabindex="1"><?php esc_html_e( 'play', 'wc_jplayer' ); ?></a></li>
				<li><a href="javascript:;" class="jp-pause" tabindex="1"><?php esc_html_e( 'pause', 'wc_jplayer' ); ?></a></li>
				<li><a href="javascript:;" class="jp-next" tabindex="1"><?php esc_html_e( 'next', 'wc_jplayer' ); ?></a></li>
				<li><a href="javascript:;" class="jp-stop" tabindex="1"><?php esc_html_e( 'stop', 'wc_jplayer' ); ?></a></li>
				<li><a href="javascript:;" class="jp-mute" tabindex="1" title="<?php esc_attr_e( 'mute', 'wc_jplayer' ); ?>"><?php esc_html_e( 'mute', 'wc_jplayer' ); ?></a></li>
				<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="<?php esc_attr_e( 'unmute', 'wc_jplayer' ); ?>"><?php esc_html_e( 'unmute', 'wc_jplayer' ); ?></a></li>
				<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="<?php esc_attr_e( 'max volume', 'wc_jplayer' ); ?>"><?php esc_html_e( 'max volume', 'wc_jplayer' ); ?></a></li>
			</ul>
			<div class="jp-progress">
				<div class="jp-seek-bar">
					<div class="jp-play-bar"></div>
				</div>
			</div>
			<div class="jp-volume-bar">
				<div class="jp-volume-bar-value"></div>
			</div>
			<div class="jp-time-holder">
				<div class="jp-current-time"></div>
				<div class="jp-duration"></div>
			</div>
			<ul class="jp-toggles">
				<li><a href="javascript:;" class="jp-shuffle" tabindex="1" title="<?php esc_attr_e( 'shuffle', 'wc_jplayer' ); ?>"><?php esc_html_e( 'shuffle', 'wc_jplayer' ); ?></a></li>
				<li><a href="javascript:;" class="jp-shuffle-off" tabindex="1" title="<?php esc_attr_e( 'shuffle off', 'wc_jplayer' ); ?>"><?php esc_html_e( 'shuffle off', 'wc_jplayer' ); ?></a></li>
				<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="<?php esc_attr_e( 'repeat', 'wc_jplayer' ); ?>"><?php esc_html_e( 'repeat', 'wc_jplayer' ); ?></a></li>
				<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="<?php esc_attr_e( 'repeat off', 'wc_jplayer' ); ?>"><?php esc_html_e( 'repeat off', 'wc_jplayer' ); ?></a></li>
			</ul>
		</div>
		<div class="jp-playlist">
			<ul>
				<li></li>
			</ul>
		</div>
		<div class="jp-no-solution">
			<span><?php _e( 'Update Required', 'wc_jplayer' ); ?></span>
			<?php _e( 'To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.', 'wc_jplayer' ); ?>
		</div>
	</div>
</div>
