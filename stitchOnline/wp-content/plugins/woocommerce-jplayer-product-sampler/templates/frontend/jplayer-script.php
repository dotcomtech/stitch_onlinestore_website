<script type="text/javascript">
//<![CDATA[
jQuery( document ).ready( function() {
	new jPlayerPlaylist(
		<?php echo json_encode( $jplayer_args[0] ); ?>,
		<?php echo json_encode( $jplayer_args[1] ); ?>,
		<?php echo json_encode( $jplayer_args[2] ); ?>
	);
} );
//]]>
</script>
