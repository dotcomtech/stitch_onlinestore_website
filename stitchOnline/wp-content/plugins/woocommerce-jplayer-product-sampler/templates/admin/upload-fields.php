<div class="options_group">
	<?php if ( is_array( $file_paths ) && ! empty( $file_paths ) ) : ?>
		<?php foreach ( $file_paths as $id => $file_path ) : ?>
			<p class="form-field jplayer_wrap">
				<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo esc_html( $field['label'] ); ?></label>
				<input type="text" class="short media_title" name="_jplayer_sample_title[]" id="_jplayer_sample_title" value="<?php echo esc_attr( $file_path['title'] ) ?>" placeholder="<?php esc_html_e( 'Title', 'wc_jplayer' ); ?>" />
				<input type="text" class="short file_path" name="<?php echo esc_attr( $field['name'] ); ?>" id="<?php echo esc_attr( $field['id'] ); ?>" value="<?php echo esc_attr( $file_path['path'] ); ?>" placeholder="<?php esc_html_e( 'File path/URL', 'wc_jplayer' ); ?>" />
				<input type="button" class="upload_jplayer_button button" value="<?php esc_html_e( 'Upload a file', 'wc_jplayer' ); ?>" />

				<?php if ( $id <> count( $file_paths ) - 1 ) : ?>
					<input type="button" class="del_jplayer_button button" value="<?php esc_html_e( 'Remove', 'wc_jplayer' ); ?>" />
				<?php else: ?>
					<input type="button" class="add_jplayer_button button" value="<?php esc_html_e( 'Add another file', 'wc_jplayer' ); ?>" />
				<?php endif; ?>
			</p>
		<?php endforeach; ?>
	<?php else: ?>
		<p class="form-field jplayer_wrap">
			<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo esc_html( $field['label'] ); ?></label>
			<input type="text" class="short media_title" name="_jplayer_sample_title[]" id="_jplayer_sample_title" value="" placeholder="<?php esc_html_e( 'Title', 'wc_jplayer' ); ?>" />
			<input type="text" class="short file_path" name="<?php echo esc_attr( $field['name'] ); ?>" id="<?php echo esc_attr( $field['id'] ); ?>" value="<?php echo esc_attr( ! empty( $file_paths ) ? $file_paths : '' ); ?>" placeholder="<?php esc_html_e( 'File path/URL', 'wc_jplayer' ); ?>" />
			<input type="button"  class="upload_jplayer_button button" value="<?php esc_html_e( 'Upload a file', 'wc_jplayer' ); ?>" />
			<input type="button"  class="add_jplayer_button button" value="<?php esc_html_e( 'Add another file', 'wc_jplayer' ); ?>" />
		</p>
	<?php endif; ?>
</div>
