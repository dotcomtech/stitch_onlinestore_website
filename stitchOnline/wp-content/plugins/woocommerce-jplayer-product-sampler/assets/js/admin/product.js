jQuery( function($){
	// Uploading jplayer files
	var file_path_field;
    var jplayer_downloadable_file_frame;
	window.send_to_editor_default = window.send_to_editor;
	
	jQuery('.upload_jplayer_button').live('click', function(){

        event.preventDefault();

        var $el = $( this );
        file_path_field = $el.siblings('.file_path');
        var downloadable_file_states = [
            // Main states.
            new wp.media.controller.Library({
                library:   wp.media.query(),
                multiple:  true,
                title:     $el.data('choose'),
                priority:  20,
                filterable: 'uploaded'
            })
        ];

        // Create the media frame.
        jplayer_downloadable_file_frame = wp.media.frames.downloadable_file = wp.media({
            // Set the title of the modal.
            title: $el.data('choose'),
            library: {
                type: ''
            },
            button: {
                text: $el.data('update')
            },
            multiple: true,
            states: downloadable_file_states
        });

        // When an image is selected, run a callback.
        jplayer_downloadable_file_frame.on( 'select', function() {
            var file_path = '';
            var selection = jplayer_downloadable_file_frame.state().get( 'selection' );

            selection.map( function( attachment ) {
                attachment = attachment.toJSON();
                if ( attachment.url ) {
                    file_path = attachment.url;
                }
            });

            file_path_field.val( file_path ).change();
        });

        // Set post to 0 and set our custom type
        jplayer_downloadable_file_frame.on( 'ready', function() {
            jplayer_downloadable_file_frame.uploader.options.uploader.params = {
                type: 'downloadable_product'
            };
        });

        // Finally, open the modal.
        jplayer_downloadable_file_frame.open();
	});
	
	jQuery('.add_jplayer_button').live('click', function(){
		jQuery('.add_jplayer_button').hide();
		jQuery(this).parent().after(
			'<p class="form-field class="jplayer_wrap"><label for="_jplayer_sample_file">Upload a Preview File</label>\
					<input type="text" class="short media_title" name="_jplayer_sample_title[]" id="_jplayer_sample_title" value="" placeholder="Title" />\
					<input type="text" class="short file_path" name="_jplayer_sample_file[]" id="_jplayer_sample_file" value="" placeholder="File path/URL" />\
					<input type="button"  class="upload_jplayer_button button" value="Upload a file" />\
					<input type="button"  class="add_jplayer_button button" value="Add another file" />\
				</p>'
		);
	});
	
	jQuery('.del_jplayer_button').live('click', function(){
		jQuery(this).parent().remove();
	});

	window.send_to_jplayer_url = function(html) {
		file_url = jQuery(html).attr('href');
		if (file_url) {
			jQuery(file_path_field).val(file_url);
		}
		tb_remove();
		window.send_to_editor = window.send_to_editor_default;
	}
});
