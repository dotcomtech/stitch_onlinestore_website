=== WooCommerce JPlayer Product Sampler ===
Contributors: kloon
Tags: woocommerce, e commerce, ecommerce, jplayer, sample, video, audio
Requires at least: 3.3
Tested up to: 3.3
Stable tag: 1.4.0

Add video / audio samples to your digital products and enable your customers to view or listen to the samples through JPlayer on the product page.

== Installation ==

1. Upload `woocommerce-jplayer-product-sampler` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
