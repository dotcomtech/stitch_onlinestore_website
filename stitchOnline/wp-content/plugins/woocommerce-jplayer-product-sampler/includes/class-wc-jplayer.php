<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WooCommerce jPlayer Product Sampler main class.
 */
class WC_JPlayer_Product_Sampler {

	/**
	 * Plugin version.
	 *
	 * @var string
	 */
	public $version;

	/**
	 * Absolute path to this plugin.
	 *
	 * @var string
	 */
	public $plugin_path;

	/**
	 * Absolute URL path to this plugin.
	 *
	 * @var string
	 */
	public $plugin_url;

	/**
	 * Settings handler.
	 *
	 * @var WC_JPlayer_Product_Sampler_Settings
	 */
	public $settings;

	/**
	 * Product meta handler.
	 *
	 * @var WC_JPlayer_Product_Sampler_Product_Meta
	 */
	public $product_meta;

	/**
	 * Displayer.
	 *
	 * @var WC_JPlayer_Product_Sampler_Display
	 */
	public $display;

	/**
	 * Shortcode handler.
	 *
	 * @var WC_JPlayer_Product_Sampler_Shortcode
	 */
	public $shortcode;

	/**
	 * Constructor.
	 *
	 * @param string $plugin_filepath Main plugin filepath
	 * @param string $plugin_version  Plugin version
	 */
	public function __construct( $plugin_filepath, $plugin_version ) {
		$this->plugin_path = trailingslashit( plugin_dir_path( $plugin_filepath ) );
		$this->plugin_url  = trailingslashit( plugin_dir_url( $plugin_filepath ) );
		$this->version     = $plugin_version;

		$this->load_classes();

		register_activation_hook( $plugin_filepath, array( $this, 'install' ) );

		add_action( 'admin_init', array( $this, 'check_version' ), 5 );
	}

	/**
	 * Load classes required by the plugin.
	 */
	protected function load_classes() {
		require_once( $this->plugin_path . 'includes/class-wc-jplayer-display.php' );
		require_once( $this->plugin_path . 'includes/class-wc-jplayer-product-meta.php' );
		require_once( $this->plugin_path . 'includes/class-wc-jplayer-settings.php' );
		require_once( $this->plugin_path . 'includes/class-wc-jplayer-shortcode.php' );

		$this->settings     = new WC_JPlayer_Product_Sampler_Settings( $this );
		$this->product_meta = new WC_JPlayer_Product_Sampler_Product_Meta( $this );
		$this->display      = new WC_JPlayer_Product_Sampler_Display( $this );
		$this->shortcode    = new WC_JPlayer_Product_Sampler_Shortcode( $this );
	}

	/**
	 * Check version on admin page load, if different that current version
	 * performs install.
	 */
	public function check_version() {
		if ( ! defined( 'IFRAME_REQUEST' ) && ( get_option( 'woo_jplayer_version' ) !== $this->version ) ) {
			$this->install();
		}
	}

	/**
	 * Perform installation.
	 */
	public function install() {
		// Add default settings. Existing options will not be updated.
		add_option( 'woo_jplayer_solution', $this->settings->default_playback_solution );
		add_option( 'woo_jplayer_skin', $this->settings->default_skin );
		add_option( 'woo_jplayer_loop', 'no' );
		add_option( 'woo_jplayer_placement', 'no' );
		add_option( 'woo_jplayer_location', $this->settings->default_location );
		add_option( 'woo_jplayer_location_priority', $this->settings->default_location_priority );

		// Update options for older version than 1.4.0.
		$old_options = get_option( 'woo_jplayer_psampler', array() );
		if ( ! empty( $old_options ) ) {
			update_option( 'woo_jplayer_solution', ! empty( $old_options['woo_jplayer_solution'] ) ? $old_options['woo_jplayer_solution'] : $this->settings->default_playback_solution );
			update_option( 'woo_jplayer_skin', ! empty( $old_options['woo_jplayer_skin'] ) ? $old_options['woo_jplayer_skin'] : $this->settings->default_skin );
			update_option( 'woo_jplayer_loop', ! empty( $old_options['woo_jplayer_loop'] ) ? $old_options['woo_jplayer_loop'] : 'no' );
			update_option( 'woo_jplayer_placement', ! empty( $old_options['woo_jplayer_placement'] ) ? $old_options['woo_jplayer_placement'] : 'no' );
			update_option( 'woo_jplayer_location', ! empty( $old_options['woo_jplayer_location'] ) ? $old_options['woo_jplayer_location'] : $this->settings->default_location );
			update_option( 'woo_jplayer_location_priority', ! empty( $old_options['woo_jplayer_location_priority'] ) ? $old_options['woo_jplayer_location_priority'] : $this->settings->default_location_priority );

			delete_option( 'woo_jplayer_psampler' );
		}

		update_option( 'woo_jplayer_version', $this->version );
	}
}
