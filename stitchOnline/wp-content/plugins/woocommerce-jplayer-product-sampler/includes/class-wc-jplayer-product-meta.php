<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WooCommerce jPlayer Product Sampler product meta class.
 */
class WC_JPlayer_Product_Sampler_Product_Meta {

	/**
	 * Instance of main plugin class.
	 *
	 * @var WC_JPlayer_Product_Sampler
	 */
	private $plugin;

	/**
	 * Constructor.
	 *
	 * @param WC_JPlayer_Product_Sampler $plugin Instance of main plugin class
	 */
	public function __construct( WC_JPlayer_Product_Sampler $plugin ) {
		$this->plugin = $plugin;

		add_action( 'admin_enqueue_scripts', array( $this, 'load_admin_scripts' ) );

		// Add sample upload field underneath download section in product.
		add_action( 'woocommerce_product_options_general_product_data', array( $this, 'product_fields' ) );
		add_action( 'woocommerce_process_product_meta', array( $this, 'product_fields_process' ), 1 );
	}

	/**
	 * Load scripts on admin page.
	 */
	public function load_admin_scripts() {
		wp_register_script(
			'wc-jplayer-admin-product',
			$this->plugin->plugin_url . 'assets/js/admin/product.js',
			array( 'jquery' )
		);
		wp_enqueue_script( 'wc-jplayer-admin-product' );

		$screen  = get_current_screen();
		if ( in_array( $screen->id, array( 'product', 'edit-product' ) ) ) {
			global $post;

			$params = array( 'post_id' => isset( $post->ID ) ? $post->ID : '' );
			wp_localize_script( 'wc-jplayer-admin-product', 'wc_jplayer_params', $params );
		}
	}

	/**
	 * Added upload fields on General tab of Product meta box.
	 */
	public function product_fields() {
		global $post;

		$file_paths = get_post_meta( $post->ID, '_jplayer_sample_file', true );
		$field      = array(
			'id'    => '_jplayer_sample_file',
			'name'  => '_jplayer_sample_file[]',
			'label' => __( 'Upload a Preview File', 'wc_jplayer' ),
		);

		require_once( $this->plugin->plugin_path . 'templates/admin/upload-fields.php' );
	}

	/**
	 * Process upload fields into post meta.
	 *
	 * @param mixed $post_id Post ID
	 */
	public function product_fields_process( $post_id ) {
		if ( ! empty( $_POST['_jplayer_sample_file'] ) ) {
			$data = array();
			foreach ( $_POST['_jplayer_sample_file'] as $id => $url ) {
				if ( ! empty( $url ) ) {
					$title = '';
					if ( empty( $_POST['_jplayer_sample_title'][ $id ] ) ) {
						$title = sprintf( 'Track %d', $id + 1 );
					} else {
						$title = $_POST['_jplayer_sample_title'][ $id ];
					}
					$data[] = array(
						'path'  => $url,
						'title' => $title,
					);
				}
			}
			update_post_meta( $post_id, '_jplayer_sample_file', $data );
		} else {
			delete_post_meta( $post_id, '_jplayer_sample_file' );
		}
	}
}
