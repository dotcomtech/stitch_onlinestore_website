<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WooCommerce jPlayer Product Sampler settings handler class.
 */
class WC_JPlayer_Product_Sampler_Settings {

	/**
	 * Instance of main plugin class.
	 *
	 * @var WC_JPlayer_Product_Sampler
	 */
	private $plugin;

	/**
	 * Default jPlayer playback solution.
	 *
	 * @var string
	 */
	public $default_playback_solution = 'html';

	/**
	 * Default jPlayer skin.
	 *
	 * @var string
	 */
	public $default_skin = 'blue.monday';

	/**
	 * Default location of jPlayer.
	 *
	 * @var string
	 */
	public $default_location = 'woocommerce_before_main_content';

	/**
	 * Default location priority of jPlayer.
	 *
	 * @var string
	 */
	public $default_location_priority = 10;

	/**
	 * Constructor.
	 *
	 * @param WC_JPlayer_Product_Sampler $plugin Instance of main plugin class
	 */
	public function __construct( WC_JPlayer_Product_Sampler $plugin ) {
		$this->plugin = $plugin;

		// jPlayer admin settings under WooCommerce > Settings > Products.
		add_filter( 'woocommerce_get_sections_products', array( $this, 'add_jplayer_settings_section' ) );
		add_filter( 'woocommerce_get_settings_products', array( $this, 'jplayer_get_settings' ), 10, 2 );
	}

	/**
	 * Add jPlayer admin settings under Products tab.
	 *
	 * @param array $sections Sections under Products tab
	 *
	 * @return array Sections
	 */
	public function add_jplayer_settings_section( $sections ) {
		$sections['jplayer'] = __( 'JPlayer Product Sampler', 'wc_jplayer' );
		return $sections;
	}

	/**
	 * Return settings array for jPlayer.
	 *
	 * @param array  $settings        Settings
	 * @param string $current_section Current section's name
	 *
	 * @return array Settings
	 */
	public function jplayer_get_settings( $settings, $current_section ) {
		if ( 'jplayer' !== $current_section ) {
			return $settings;
		}

		return apply_filters( 'woocommerce_jplayer_get_settings', array(
			array(
				'title' => __( 'General Settings', 'wc_jplayer' ),
				'type'  => 'title',
				'id'    => 'wc_jplayer_general_settings',
			),
			array(
				'title'   => __( 'Playback Solution', 'wc_jplayer' ),
				'desc'    => __( 'Since Adobe retired Flash, this plugin only supports HTML', 'wc_jplayer' ),
				'type'    => 'select',
				'id'      => 'woo_jplayer_solution',
				'options' => array(
					'html'  => __( 'HTML 5', 'wc_jplayer' ),
				),
				'default' => $this->default_playback_solution,
			),
			array(
				'title'   => __( 'JPlayer Skin', 'wc_jplayer' ),
				'desc'    => __( 'The JPlayer skin to use, you can upload skins to the skins folder in the plugin directory.', 'wc_jplayer' ),
				'type'    => 'select',
				'id'      => 'woo_jplayer_skin',
				'options' => $this->get_skin_options(),
				'default' => $this->default_skin,
			),
			array(
				'title'   => __( 'Loop', 'wc_jplayer' ),
				'desc'    => __( 'Enable playback to loop.', 'wc_jplayer' ),
				'type'    => 'checkbox',
				'id'      => 'woo_jplayer_loop',
				'default' => 'no',
			),
			array(
				'title'   => __( 'Use Shortcode For Player Placement', 'wc_jplayer' ),
				'desc'    => __( 'Check to disable automatic placement with below options, and to use the <b>[woo_jplayer]</b> shortcode rather.', 'wc_jplayer' ),
				'type'    => 'checkbox',
				'id'      => 'woo_jplayer_placement',
				'default' => 'no',
			),
			array(
				'title'   => __( 'JPlayer Location', 'wc_jplayer' ),
				'desc'    => __( 'Location of the JPlayer on the product page.', 'wc_jplayer' ),
				'type'    => 'select',
				'id'      => 'woo_jplayer_location',
				'options' => $this->get_available_locations(),
				'default' => $this->default_location,
			),
			array(
				'title'   => __( 'Location Priority', 'wc_jplayer' ),
				'desc'    => __( 'Enter a number to position the player above or below other content in the same location. Lower numbers gets higher priority.', 'wc_jplayer' ),
				'type'    => 'number',
				'id'      => 'woo_jplayer_location_priority',
				'default' => $this->default_location_priority,
			),
			array(
				'type' 	=> 'sectionend',
				'id' 	=> 'wc_jplayer_general_settings'
			),
		) );
	}

	/**
	 * Get list of directories.
	 *
	 * @param string $base Base dir
	 *
	 * @return array List of directories
	 */
	public function list_directories( $base = '' ) {
		if ( empty( $base ) ) {
			$base = $this->plugin->plugin_path . 'skins';
		}
		$ret = array();

		// Get all files in specified directory.
		if ( $handle = opendir( $base ) ) {
			while ( false !== ( $entry = readdir( $handle ) ) ) {
				if ( '.' !== $entry && '..' !== $entry ) {
					$ret[] = $entry;
				}
			}
			closedir( $handle );
		}
		return $ret;
	}

	/**
	 * Get jPlayer skins to be passed as select's options.
	 *
	 * @todo(gedex) Refactor how skin's lookup per issue #2
	 *
	 * @return array
	 */
	protected function get_skin_options() {
		$directories = $this->list_directories();
		return array_combine( $directories, $directories );
	}

	/**
	 * Get available locations for jPlayer placement.
	 *
	 * @return array
	 */
	protected function get_available_locations() {
		return array(
			'woocommerce_before_main_content'           => __( 'Before Main Content', 'wc_jplayer' ),
			'woocommerce_before_single_product'         => __( 'Before Product', 'wc_jplayer' ),
			'woocommerce_before_single_product_summary' => __( 'Before Product Summary', 'wc_jplayer' ),
			'woocommerce_single_product_summary'        => __( 'Product Summary', 'wc_jplayer' ),
			'woocommerce_after_single_product_summary'  => __( 'After Product Summary', 'wc_jplayer' ),
			'woocommerce_after_single_product'          => __( 'After Product', 'wc_jplayer' ),
			'woocommerce_after_main_content'            => __( 'After Main Content', 'wc_jplayer' ),
			'woocommerce_sidebar'                       => __( 'Sidebar', 'wc_jplayer' ),
		);
	}
}
