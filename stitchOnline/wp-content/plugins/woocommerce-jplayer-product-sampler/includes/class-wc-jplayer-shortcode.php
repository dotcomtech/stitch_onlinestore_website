<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WooCommerce jPlayer Product Sampler shortcode handler class.
 */
class WC_JPlayer_Product_Sampler_Shortcode {

	/**
	 * Instance of main plugin class.
	 *
	 * @var WC_JPlayer_Product_Sampler
	 */
	private $plugin;

	/**
	 * Constructor.
	 *
	 * @param WC_JPlayer_Product_Sampler $plugin Instance of main plugin class
	 */
	public function __construct( WC_JPlayer_Product_Sampler $plugin ) {
		$this->plugin = $plugin;

		add_shortcode( 'woo_jplayer', array( $this, 'jplayer_shortcode' ) );
	}

	/**
	 * Get rendered jPlayer from shortcode.
	 *
	 * @param mixed $atts
	 *
	 * @return string
	 */
	public function jplayer_shortcode( $atts ) {
		extract( shortcode_atts( array(
			'url'   => '',
			'title' => '',
		), $atts ) );

		ob_start();
		$this->plugin->display->render_jplayer( true, $url, $title );
		$content = ob_get_clean();
		return $content;
	}
}
