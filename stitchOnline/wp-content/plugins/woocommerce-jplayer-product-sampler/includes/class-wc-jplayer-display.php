<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WooCommerce jPlayer Product Sampler displayer class.
 */
class WC_JPlayer_Product_Sampler_Display {

	/**
	 * Instance of main plugin class.
	 *
	 * @var WC_JPlayer_Product_Sampler
	 */
	private $plugin;

	/**
	 * Constructor.
	 *
	 * @param WC_JPlayer_Product_Sampler $plugin Instance of main plugin class
	 */
	public function __construct( WC_JPlayer_Product_Sampler $plugin ) {
		$this->plugin = $plugin;

		// Add JPlayer scripts to head.
		add_action( 'wp_enqueue_scripts', array( $this, 'load_scripts' ) );

		// Bind to location of product page.
		$location          = get_option( 'woo_jplayer_location', $this->plugin->settings->default_location );
		$location_priority = get_option( 'woo_jplayer_location_priority', $this->plugin->settings->default_location_priority );
		add_action( $location, array( $this, 'render_jplayer' ),  $location_priority );
	}

	/**
	 * Load scripts for front-end.
	 */
	public function load_scripts() {
		if ( ! $this->allow_enqueue_jplayer_assets() ) {
			return;
		}

		$js_suffix  = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '.js' : '.min.js';
		$css_suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '.css' : '.min.css';

		// Load JPlayer.
		wp_register_script(
			'wc-jplayer',
			$this->plugin->plugin_url . '/assets/js/jPlayer/jquery.jplayer' . $js_suffix,
			array( 'jquery' ),
			'2.9.2'
		);
		wp_enqueue_script( 'wc-jplayer' );

		wp_register_script(
			'wc-jplayer-playlist',
			$this->plugin->plugin_url . '/assets/js/jPlayer/add-on/jplayer.playlist' . $js_suffix,
			array( 'wc-jplayer' )
		);
		wp_enqueue_script( 'wc-jplayer-playlist' );

		// Load skin stylesheet.
		$skin     = get_option( 'woo_jplayer_skin', $this->plugin->settings->default_skin );
		$css_file = $this->get_skin_stylesheet( $this->plugin->plugin_path . 'skins/' . $skin );
		if ( ! $css_file ) {
			$css_file = sprintf( 'css/jplayer.%s%s', $this->plugin->settings->default_skin, $css_suffix );
		}

		wp_enqueue_style(
			$skin,
			$this->plugin->plugin_url . 'skins/' . $skin . '/' . $css_file
		);

		// Fix for li styling issue on some themes.
		wp_enqueue_style(
			'wc-jplayer-style',
			$this->plugin->plugin_url . '/assets/css/style.css'
		);
	}

	/**
	 * Whether jPlayer assets are allowed to be enqueud.
	 *
	 * @return bool True if allowed to render
	 */
	public function allow_enqueue_jplayer_assets() {
		$allow_enqueue = (
			is_product()
			|| is_product_taxonomy()
			|| is_product_category()
			|| is_product_tag()
			|| is_shop()
		);

		return apply_filters( 'woo_jplayer_allow_enqueue_assets', $allow_enqueue );
	}

	/**
	 * Render jPlayer.
	 *
	 * @param bool   $shortcode From shortcode
	 * @param mixed  $url
	 * @param mixed $title
	 */
	public function render_jplayer( $shortcode = false, $url = '', $title = '' ) {
		global $post;

		if ( 'yes' === get_option( 'woo_jplayer_placement' ) && ! $shortcode ) {
			return;
		}

		$unique_id = rand();
		$the_urls  = array();

		if ( $shortcode && ! empty( $url ) ) {
			$urls   = explode( ',', $url );
			$titles = explode( ',', $title );

			foreach ( $urls as $id => $url ) {
				if ( empty( $titles[ $id ] ) ) {
					$title = __( 'Track 1', 'wc_jplayer' );
				} else {
					$title = $titles[ $id ];
				}
				$the_urls[] = array(
					'title' => $title,
					'path'  => $url,
				);
			}
		} else {
			$the_urls  = get_post_meta( $post->ID, '_jplayer_sample_file', true );
		}

		$ext  = array();
		$urls = array();

		if ( ! empty( $the_urls ) ) {
			if ( is_array( $the_urls ) ) {
				foreach ( $the_urls as $id => $the_url ) {
					$url     = parse_url( $the_url['path'] );
					$the_ext = substr( $url['path'], -3 );
					if ( ! in_array( $the_ext, $ext ) ) {
						$ext[] = $the_ext;
					}
					$urls[] = array(
						'extension' => $the_ext,
						'url'       => $the_url['path'],
						'title'     => $the_url['title'],
					);
				}
			} else {
				$url     = parse_url( $the_urls );
				$the_ext = substr( $url['path'], -3 );
				if ( ! in_array( $the_ext, $ext ) ) {
					$ext[] = $the_ext;
				}
				$urls[] = array(
					'extension' => $the_ext,
					'url'       => $the_urls,
					'title'     => __( 'Track 1', 'wc_jplayer' ),
				);
			}

			$jplayer_id           = 'jquery_jplayer_' . $unique_id;
			$jplayer_container_id = 'jp_container_' . $unique_id;

			$jplayer_args = $this->get_jplayer_js_args(
				$jplayer_id,
				$jplayer_container_id,
				$urls,
				$ext
			);
			include( $this->plugin->plugin_path . 'templates/frontend/jplayer-script.php' );

			if ( $this->has_audio_extension( $ext ) ) {
				include( $this->plugin->plugin_path . 'templates/frontend/jplayer-audio.php' );
			} else if ( $this->has_video_extension( $ext ) ) {
				include( $this->plugin->plugin_path . 'templates/frontend/jplayer-video.php' );
			}
		}
	}

	/**
	 * Get JS args to be passed to jPlayer JS.
	 *
	 * @todo(gedex) Refactor this once jPlayer is updated to 2.9.2
	 *
	 * @param mixed $id           HTML element ID containing jPlayer
	 * @param mixed $container_id HTML element ID for jPlayer container
	 * @param array $files        Files to be played
	 * @param array $extensions   Extensions of files
	 *
	 * @return array Args to be passed to jPlayer
	 */
	public function get_jplayer_js_args( $id, $container_id, $files, $extensions ) {
		if ( 0 !== strpos( $id, '#' ) ) {
			$id = '#' . $id;
		}
		if ( 0 !== strpos( $container_id, '#' ) ) {
			$container_id = '#' . $container_id;
		}

		$files_arg = array();
		foreach ( $files as $file ) {
			$files_arg[] = array(
				'title'            => $file['title'],
				$file['extension'] => $file['url'],
			);
		}
		return array(
			array(
				'jPlayer'             => $id,
				'cssSelectorAncestor' => $container_id,
			),
			$files_arg,
			array(
				'noConflict' => 'jQuery',
				'swfPath'    => $this->plugin->plugin_url . 'assets/js/jQuery.jPlayer.2.4.0',
				'supplied'   => implode( ',', $extensions ),
				'solution'   => 'html',
				'loop'       => 'yes' === get_option( 'woo_jplayer_loop' ),
			),
		);
	}



	/**
	 * Check whether given extensions has audio extension.
	 *
	 * @param array $extensions List of extensions
	 *
	 * @return bool True if extensions contain audio extension
	 */
	public function has_audio_extension( $extensions ) {
		return (
			in_array( 'mp3', $extensions ) ||
			in_array( 'm4a', $extensions ) ||
			in_array( 'm4b', $extensions ) ||
			in_array( 'wav', $extensions ) ||
			in_array( 'ogg', $extensions ) ||
			in_array( 'oga', $extensions ) ||
			in_array( 'fla', $extensions )
		);
	}

	/**
	 * Check whether given extensions has video extension.
	 *
	 * @param array $extensions List of extensions
	 *
	 * @return bool True if extensions contain video extension
	 */
	public function has_video_extension( $extensions ) {
		return (
			in_array( 'mp4', $extensions ) ||
			in_array( 'm4v', $extensions ) ||
			in_array( 'ogv', $extensions ) ||
			in_array( 'flv', $extensions ) ||
			in_array( 'webm', $extensions )
		);
	}

	/**
	 * Get skin stylesheet from given directory.
	 *
	 * @param string $dir Base dir
	 *
	 * @return mixed
	 */
	public function get_skin_stylesheet( $dir ) {
		foreach ( $this->plugin->settings->list_directories( $dir ) as $file ) {
			if ( '.css' === substr( $file, -4 ) ) {
				return $file;
			}
		}

		return false;
	}
}
