/*
    Elfsight Facebook Feed
    Version: 1.8.0
    Release date: Mon Jul 30 2018

    https://elfsight.com

    Copyright (c) 2018 Elfsight, LLC. ALL RIGHTS RESERVED
*/

(function (eapps) {
    eapps.observer = function($scope, properties, $rootScope) {
        $scope.$watch('widget.data.userAccessToken', function() {
            if ($scope.widget.data.userAccessToken) {
                setVisibility('sourceType', false, properties);
                setVisibility('source', true, properties);

            } else {
                setVisibility('sourceType', false, properties);
                setVisibility('source', false, properties);
            }
        });

        $scope.$watch('widget.data.sourceType', function() {
            if($scope.widget.data.userAccessToken) {
                if ($scope.widget.data.sourceType === 'profile') {
                    setVisibility('source', false, properties);

                } else {
                    setVisibility('source', true, properties);
                }
            }
        });
    };

    let setVisibility = function(id, value, properties) {
        properties.forEach(function(property, index) {
            if (property.id === id) {
                properties[index].visible = value;
                return false;
            }
            if (property && property.properties) {
                setVisibility(id, value, property.properties);
            }
            if (property.complex && property.complex.properties) {
                setVisibility(id, value, property.complex.properties);
            }
            if (property.subgroup && property.subgroup.properties) {
                setVisibility(id, value, property.subgroup.properties);
            }
        });
    };
})(window.eapps = window.eapps || {});