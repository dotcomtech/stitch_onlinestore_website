<?php

if (!defined('ABSPATH')) exit;


function elfsight_facebook_feed_api_test() {
    $curl_support = function_exists('curl_init');

    if ($curl_support) {
        $default_api_path = plugin_dir_url(dirname(__FILE__)) . 'api/index.php';

        $curl = curl_init();
        $curl_options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => true,
            CURLOPT_URL => $default_api_path,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CONNECTTIMEOUT => 15,
            CURLOPT_TIMEOUT => 60
        );
        curl_setopt_array($curl, $curl_options);
        $response_str = curl_exec($curl);
        curl_close($curl);

        @list ($response_headers_str, $response_body_encoded) = explode("\r\n\r\n", $response_str);

        $response_headers_raw_list = explode("\r\n", $response_headers_str);
        $response_http = array_shift($response_headers_raw_list);

        preg_match('#^([^\s]+)\s(\d+)\s([^$]+)$#', $response_http, $response_http_matches);
        array_shift($response_http_matches);

        list ($response_http_protocol, $response_http_code, $response_http_message) = $response_http_matches;

        if ($response_http_code && $response_http_code === '200') {
            return true;
        }
    } else {
        return true;
    }

    return false;
}

function elfsight_facebook_feed_api_update() {
    $default_api_path = plugin_dir_path(dirname(__FILE__)) . 'api';

    if (get_option('elfsight-facebook-feed-cc_custom_api_url')) {
        $format_option = str_replace(site_url(), '', get_option('elfsight-facebook-feed-cc_custom_api_url'));
        $format_option = preg_replace('#^\/?(.*?)\/?([a-zA-Z]+\.php)?$#', '$1', $format_option);

        $custom_api_path = ABSPATH . $format_option;
    } else {
        $custom_api_path = ABSPATH . 'elfsight-facebook-feed-api';
        add_option('elfsight-facebook-feed-cc_custom_api_url', site_url() . '/elfsight-facebook-feed-api/index.php');
    }

    if (!function_exists('elfsight_rrmdir')) {
        function elfsight_rrmdir($dir) {
            if (is_dir($dir)) {
                $files = scandir($dir);
                foreach ($files as $file) {
                    if ($file != "." && $file != "..") {
                        elfsight_rrmdir("$dir/$file");
                    }
                }

                rmdir($dir);
            } else if (file_exists($dir)) {
                unlink($dir);
            }
        }
    }

    if (!function_exists('elfsight_rcopy')) {
        function elfsight_rcopy($src, $dst) {
            if (is_dir($src)) {
                if (!file_exists($dst)) {
                    mkdir($dst);
                }
                if (file_exists("$dst/storage")) {
                    elfsight_rrmdir("$dst/storage");
                }

                elfsight_rcopy("$src/index.php", "$dst/index.php");
                elfsight_rcopy("$src/config.json", "$dst/config.json");
                elfsight_rcopy("$src/app-usage.json", "$dst/app-usage.json");

            } else if (file_exists($src)) {
                if (file_exists($dst)) {
                    unlink($dst);
                }
                copy($src, $dst);
            }
        }
    }

    elfsight_rcopy($default_api_path, $custom_api_path);
}

function elfsight_facebook_feed_activation_api_check() {
    if (get_option('elfsight-facebook-feed-cc_custom_api_url') || !elfsight_facebook_feed_api_test()) {
        elfsight_facebook_feed_api_update();
    }
}