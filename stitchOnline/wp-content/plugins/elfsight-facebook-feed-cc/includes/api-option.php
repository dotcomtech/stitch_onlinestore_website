<?php

if (!defined('ABSPATH')) exit;


if (is_array($elfsight_facebook_feed_config['settings'])) {
    array_push($elfsight_facebook_feed_config['settings']['properties'], array(
        'id' => 'apiUrl',
        'name' => 'API URL',
        'tab' => 'more',
        'type' => 'hidden',
        'defaultValue' => get_option('elfsight-facebook-feed-cc_custom_api_url', plugin_dir_url(dirname(__FILE__)) . 'api/index.php')
    ));
}

function elfsight_facebook_feed_shortcode_options_filter($options) {
    $api_url = get_option('elfsight-facebook-feed-cc_custom_api_url', plugin_dir_url(dirname(__FILE__)) . 'api/index.php');

    if (is_ssl()) {
        $api_url = str_replace('http://', 'https://', $api_url);
    }

    if (is_array($options)) {
        $options['apiUrl'] = $api_url;
    }

    return $options;
}
add_filter('elfsight_facebook_feed_shortcode_options', 'elfsight_facebook_feed_shortcode_options_filter');

function elfsight_facebook_feed_widget_options_filter($options_json) {
    $options = json_decode($options_json, true);

    if (is_array($options)) {
        unset($options['apiUrl']);
    }

    return json_encode($options);
}
add_filter('elfsight_facebook_feed_widget_options', 'elfsight_facebook_feed_widget_options_filter');