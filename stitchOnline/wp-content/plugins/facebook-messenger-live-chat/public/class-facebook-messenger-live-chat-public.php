<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://makewebbetter.com/
 * @since      1.0.0
 *
 * @package    facebook-messenger-live-chat
 * @subpackage facebook-messenger-live-chat/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    facebook-messenger-live-chat
 * @subpackage facebook-messenger-live-chat/public
 * @author     makewebbetter <webmaster@makewebbetter.com>
 */
class Facebook_Messenger_Live_Chat_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	// Add Facebook Messenger script function.
	public function mwb_fmlcrt_add_fb_msgr_script(){

		// Check device settings.
		$device = $this->mwb_fmlcrt_check_device();

		if( !$device ){
			return;
		}

		$show = $this->mwb_fmlcrt_check_pages();

		if( $show ):

			wp_enqueue_script( $this->plugin_name, MWB_FMLCRT_URL . 'public/js/facebook-messenger-live-chat-public.js', array( 'jquery' ), $this->version, false );

			$lang_default = !empty( get_locale() ) ? get_locale() : 'en_US';

			$lang = get_option('mwb_fmlcrt_plug_messenger_lang', $lang_default );

			$fb_app_id = get_option('mwb_fmlcrt_plug_fb_app_id', '' );

			$fb_app_id = !empty($fb_app_id) ? '&appId='.$fb_app_id : '';

			wp_localize_script( $this->plugin_name, 'mwb_fmlcrt_messenger_script_obj', array(
		        'language' => $lang,
		        'fb_app_id' => $fb_app_id,
		        ) );
	
		endif;
	}

	// Add Facebook Messenger button function.
	public function mwb_fmlcrt_add_fb_msgr_btn(){

		// Check device settings.
		$device = $this->mwb_fmlcrt_check_device();

		if( !$device ){
			return;
		}

		$show = $this->mwb_fmlcrt_check_pages();

		if( $show ):

			$fb_page_id = get_option( 'mwb_fmlcrt_plug_fb_pd_id', '' );

			$fb_page_id = esc_attr( $fb_page_id );	

			$min_attr = get_option( 'mwb_fmlcrt_plug_min_behaviour', '' );	

			$color = get_option( 'mwb_fmlcrt_plug_color_picker', '' );

			$greeting_text = get_option( 'mwb_fmlcrt_plug_greeting_text', array() );

			$logged_in_text = !empty( $greeting_text['logged_in'] ) ? $greeting_text['logged_in'] : '';

			$logged_out_text = !empty( $greeting_text['logged_out'] ) ? $greeting_text['logged_out'] : '';

			?>

			<div class="fb-customerchat"
			page_id="<?php echo $fb_page_id; ?>"
			theme_color="<?php echo $color ?>"
			minimized="<?php echo $min_attr; ?>"
			logged_in_greeting="<?php echo $logged_in_text; ?>"
			logged_out_greeting="<?php echo $logged_out_text; ?>" ></div>

			<?php

		endif;
	}

	// Callable function for displaying Facebook Messenger according to device settings.
	private function mwb_fmlcrt_check_device() {

		$device_display = get_option('mwb_fmlcrt_plug_device_display', 'device_all' );

		if( 'device_all' == $device_display ) {

			return true;
		}

		elseif( 'device_desk' == $device_display ) {

			if( wp_is_mobile() ) {

				return false;
			}
			else{

				return true;
			}

		}

		elseif( 'device_mob' == $device_display ) {

			if( wp_is_mobile() ) {

				return true;
			}
			else{

				return false;
			}
			
		}

		// fallback : if no condition matches for device display return true.
		return true;
	}

	// Callable function for conditional displaying of Facebook Messenger.
	private function mwb_fmlcrt_check_pages() {

		$display = get_option('mwb_fmlcrt_plug_display', 'eve');
		
		if ( $display == 'eve' ){

			return true;
		}

		else if(  $display == 'eve_except' ){

			if( is_front_page() ){

				$home_for_except = get_option('mwb_fmlcrt_home_for_except', 'yes');

				if( $home_for_except == 'yes' ){

					return true;
				}
				else{

					return false;
				}
			}

			if ( mwb_fmlcrt_woocommerce_active() ) {

				if( is_shop() ){

					$except_pages = (array)get_option('mwb_fmlcrt_plug_display_except_pages', '');

					if( in_array( 'shop', $except_pages ) ){

						return false;
					}
					else{

						return true;
					}
				}
			}

			global $post;

			if( !$post || !$post->post_name || !is_singular() ){

				return true;
			}

			$post_slug = $post->post_name;

			$except_pages = (array)get_option('mwb_fmlcrt_plug_display_except_pages', '');

			if( in_array( $post_slug, $except_pages ) ){

				return false;
			}
			else{

				return true;
			}

		}

		else if(  $display == 'only_these' ){

			if( is_front_page() ){

				$home_for_except = get_option('mwb_fmlcrt_home_for_only', 'yes');

				if( $home_for_except == 'yes' ){

					return true;
				}
				else{

					return false;
				}
			}

			if ( mwb_fmlcrt_woocommerce_active() ) {

				if( is_shop() ){

					$only_pages = (array)get_option('mwb_fmlcrt_plug_display_only_pages', '');

					if( in_array( 'shop', $only_pages ) ){

						return true;
					}
					else{

						return false;
					}
				}
			}

			global $post;

			if( !$post || !$post->post_name || !is_singular() ){

				return false;
			}

			$post_slug = $post->post_name;

			$only_pages = (array)get_option('mwb_fmlcrt_plug_display_only_pages', '');

			if( in_array( $post_slug, $only_pages ) ){

				return true;
			}
			else{

				return false;
			}

		}

		else if ( $display == 'only_home' ){

			if( is_front_page() ){

				return true;
			}

			else{

				return false;
			}
		}

		// fallback : if no condition matches for display then return true.
		return true;
	} 

}
