<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://makewebbetter.com/
 * @since             1.0.0
 * @package           facebook-messenger-live-chat
 *
 * @wordpress-plugin
 * Plugin Name:       Facebook Messenger Live Chat (Real Time)
 * Plugin URI:        http://makewebbetter.com/
 * Description:       This Plugin enables you to add Facebook Messenger with Real Time Live Chat on your website. Users can have a chat conversation on your website only and there's no need to switch back to Facebook for checking messages.
 * Version:           1.0.2
 * Author:            makewebbetter
 * Author URI:        http://makewebbetter.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mwb-fb-mlcrt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Define plugin constants.
function mwb_fmlcrt_constants( $key, $value ) {

	if( !defined($key) ){

		define( $key, $value );
	}
}

// Callable function for defining plugin constants.
function define_mwb_fmlcrt_constants() {

	mwb_fmlcrt_constants( 'MWB_FMLCRT_DIRPATH', plugin_dir_path( __FILE__ ) );
	mwb_fmlcrt_constants( 'MWB_FMLCRT_URL', plugin_dir_url( __FILE__ ) );
	mwb_fmlcrt_constants( 'MWB_FMLCRT_HOME_URL', home_url() );

	mwb_fmlcrt_constants( 'MWB_FMLCRT_TXT_DOMAIN', 'mwb-fb-mlcrt' );
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-facebook-messenger-live-chat-activator.php
 */
function activate_facebook_messenger_live_chat() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-facebook-messenger-live-chat-activator.php';
	Facebook_Messenger_Live_Chat_Activator::activate();
}

register_activation_hook( __FILE__, 'activate_facebook_messenger_live_chat' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-facebook-messenger-live-chat.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_facebook_messenger_live_chat() {

	define_mwb_fmlcrt_constants();

	$plugin = new Facebook_Messenger_Live_Chat();
	$plugin->run();

}
run_facebook_messenger_live_chat();

//Add Settings link on Plugin page.
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'mwb_fmlcrt_action_links' );

// Settings link.
function mwb_fmlcrt_action_links ( $links ) {

	$mylinks = array(
		'<a href="' . admin_url( 'admin.php?page=mwb_fmlcrt_options_menu&tab=facebook_options' ) . '">' . __('Settings', MWB_FMLCRT_TXT_DOMAIN ) . '</a>',
	);
	return array_merge(  $mylinks, $links );
}

// Function for checking if Woocommerce is active.
function mwb_fmlcrt_woocommerce_active() {

	if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

		return true;
	}

	else{

		return false;
	}
}
