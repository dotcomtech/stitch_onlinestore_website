jQuery(document).ready(function($) {
	 	
   $('input#mwb_fmlcrt_color_picker').wpColorPicker();

   $('input#mwb_fmlcrt_clear_color').click( function(e) {

   		e.preventDefault();

   		$('input#mwb_fmlcrt_color_picker').val('');
   		
   		$('.mwb_fmlcrt_color_picker_div .wp-color-result').css('background-color', '');
   })
});
