<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
    // Exit if accessed directly
}

/**
 * Iconic_WooThumbs_Licence.
 *
 * @class    Iconic_WooThumbs_Licence
 * @version  1.0.0
 * @package  Iconic_WooThumbs
 * @category Class
 * @author   Iconic
 */
class Iconic_WooThumbs_Licence
{
    /**
     * Run.
     */
    public static function run()
    {
        self::configure();
        self::add_filters();
    }
    
    /**
     * Configure.
     */
    public static function configure()
    {
        global  $woothumbs_fs ;
        
        if ( !isset( $woothumbs_fs ) ) {
            // Include Freemius SDK.
            require_once ICONIC_WOOTHUMBS_INC_PATH . 'vendor/freemius/start.php';
            $woothumbs_fs = fs_dynamic_init( array(
                'id'               => '869',
                'slug'             => 'woothumbs',
                'type'             => 'plugin',
                'public_key'       => 'pk_3e970b87cd0ed00b398a760433a79',
                'is_premium'       => true,
                'is_premium_only'  => !ICONIC_WOOTHUMBS_IS_ENVATO,
                'has_paid_plans'   => !ICONIC_WOOTHUMBS_IS_ENVATO,
                'has_addons'       => false,
                'is_org_compliant' => false,
                'trial'            => array(
                'days'               => 14,
                'is_require_payment' => true,
            ),
                'menu'             => array(
                'slug'    => 'iconic-woothumbs-settings',
                'contact' => false,
                'support' => false,
                'account' => false,
                'pricing' => !ICONIC_WOOTHUMBS_IS_ENVATO,
                'parent'  => array(
                'slug' => 'woocommerce',
            ),
            ),
                'is_live'          => true,
            ) );
        }
        
        return $woothumbs_fs;
    }
    
    /**
     * Add filters.
     */
    public static function add_filters()
    {
        global  $woothumbs_fs ;
        $woothumbs_fs->add_filter( 'show_trial', '__return_false' );
        $woothumbs_fs->add_filter(
            'templates/account.php',
            array( __CLASS__, 'back_to_settings_link' ),
            10,
            1
        );
        $woothumbs_fs->add_filter(
            'plugin_icon',
            array( __CLASS__, 'plugin_icon' ),
            10,
            1
        );
        $woothumbs_fs->add_filter( 'hide_account_tabs', '__return_true' );
        add_filter( 'plugin_action_links_' . ICONIC_WOOTHUMBS_BASENAME, array( __CLASS__, 'add_action_links' ) );
        add_action( 'admin_notices', array( __CLASS__, 'output_back_to_settings_link' ), 2 );
    }
    
    /**
     * Set plugin icon.
     *
     * @return string
     */
    public static function plugin_icon( $icon )
    {
        return ICONIC_WOOTHUMBS_PATH . '/assets/img/plugin-icon.png';
    }
    
    /**
     * Account link.
     */
    public static function account_link()
    {
        return sprintf( '<a href="%s" class="button button-secondary">%s</a>', admin_url( 'admin.php?page=iconic-woothumbs-settings-account' ), __( 'Manage Licence &amp; Billing', 'iconic-woothumbs' ) );
    }
    
    /**
     * Contact link.
     */
    public static function contact_link()
    {
        global  $woothumbs_fs ;
        return sprintf( '<a href="%s" class="button button-secondary">%s</a>', $woothumbs_fs->contact_url(), __( 'Create Support Ticket', 'iconic-woothumbs' ) );
    }
    
    /**
     * Get contact URL.
     */
    public static function get_contact_url( $subject = false, $message = false )
    {
        global  $woothumbs_fs ;
        return $woothumbs_fs->contact_url( $subject, $message );
    }
    
    /**
     * Back to settings link.
     */
    public static function back_to_settings_link( $html = '' )
    {
        return $html . sprintf( '<a href="%s" class="button button-secondary">&larr; %s</a>', admin_url( 'admin.php?page=iconic-woothumbs-settings' ), __( 'Back to Settings', 'iconic-woothumbs' ) );
    }
    
    /**
     * Output back to settings link.
     */
    public static function output_back_to_settings_link()
    {
        if ( !Iconic_WooThumbs_Settings::is_settings_page( '-account' ) ) {
            return;
        }
        ?>
		<div style="margin: 20px 0;">
			<?php 
        echo  self::back_to_settings_link() ;
        ?>
		</div>
		<?php 
    }
    
    /**
     * Has valid licence.
     *
     * @return bool
     */
    public static function has_valid_licence()
    {
        global  $woothumbs_fs ;
        if ( ICONIC_WOOTHUMBS_IS_ENVATO || $woothumbs_fs->can_use_premium_code() ) {
            return true;
        }
        return false;
    }
    
    /**
     * Add changelog link.
     *
     * @param $links
     *
     * @return array
     */
    public static function add_action_links( $links )
    {
        $links[] = sprintf( '<a href="%s" target="_blank">%s</a>', 'https://iconicwp.com/products/woothumbs/changelog/?utm_source=iconic-woothumbs&utm_medium=insideplugin', __( 'Changelog', 'iconic-woothumbs' ) );
        return $links;
    }

}