<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Iconic_WooThumbs_Settings.
 *
 * @class    Iconic_WooThumbs_Settings
 * @version  1.0.0
 * @package  Iconic_WooThumbs
 * @category Class
 * @author   Iconic
 */
class Iconic_WooThumbs_Settings {
	/**
	 * Docs Paths.
	 *
	 * @var array
	 */
	private static $docs_paths = array(
		'base'            => 'https://docs.iconicwp.com',
		'collection'      => '/collection/110-woothumbs',
		'troubleshooting' => '/category/114-troubleshooting',
		'getting-started' => '/category/113-getting-started',
	);

	/**
	 * Run.
	 */
	public static function run() {
		add_action( "update_option_iconic_woothumbs_settings", array( __CLASS__, 'on_save' ), 10, 3 );
		add_action( 'admin_notices', array( __CLASS__, 'account_getting_started' ), 1 );
	}

	/**
	 * Get docs URL.
	 *
	 * @param bool $type
	 *
	 * @return mixed|string
	 */
	public static function get_docs_url( $type = false ) {
		if ( ! $type || $type === 'base' || ! isset( self::$docs_paths[ $type ] ) ) {
			return self::$docs_paths['base'];
		}

		return self::$docs_paths['base'] . self::$docs_paths[ $type ];
	}

	/**
	 * On save settings.
	 *
	 * @param mixed  $old_value
	 * @param mixed  $value
	 * @param string $option
	 */
	public static function on_save( $old_value, $value, $option ) {
		if ( class_exists( 'WC_Regenerate_Images' ) ) {
			WC_Regenerate_Images::maybe_regenerate_images();
		}
	}

	/**
	 * Get a list of image sizes for the site
	 *
	 * @return array
	 */
	public static function get_image_sizes() {
		$image_sizes = array_merge( get_intermediate_image_sizes(), array( 'full' ) );

		return array_combine( $image_sizes, $image_sizes );
	}

	/**
	 * Clear image cache link.
	 *
	 * @return string
	 */
	public static function clear_image_cache_link() {
		ob_start();

		?>
		<button name="iconic-woothumbs-delete-image-cache" class="button button-secondary"><?php _e( 'Clear Image Cache', 'iconic-woothumbs' ); ?></button>
		<?php

		return ob_get_clean();
	}

	/**
	 * Account link.
	 *
	 * @return string
	 */
	public static function support_link() {
		return sprintf( '<a href="https://iconicwp.ticksy.com" class="button button-secondary" target="_blank">%s</a>', __( 'Create Support Ticket', 'iconic-woothumbs' ) );
	}

	/**
	 * Documentation link.
	 *
	 * @return string
	 */
	public static function documentation_link() {
		return sprintf( '<a href="%s" class="button button-secondary" target="_blank">%s</a>', self::$docs_paths['base'] . self::$docs_paths['collection'], __( 'Read Documentation', 'iconic-woothumbs' ) );
	}

	/**
	 * Add ratio fields.
	 *
	 * @param $args
	 *
	 * @return string
	 */
	public static function ratio_fields( $args ) {
		$defaults = array(
			'width'  => '',
			'height' => '',
		);

		$args = wp_parse_args( $args, $defaults );

		$width_name  = sprintf( '%s_width', $args['name'] );
		$height_name = sprintf( '%s_height', $args['name'] );
		$input       = '<input id="%s" name="iconic_woothumbs_settings[%s]" type="number" style="width: 50px;" value="%s">';
		$width       = sprintf( $input, $width_name, $width_name, $args['width'] );
		$height      = sprintf( $input, $height_name, $height_name, $args['height'] );

		return sprintf( '%s : %s', $width, $height );
	}

	/**
	 * Cross sells.
	 *
	 * @return bool|string
	 */
	public static function cross_sells() {
		$instance = new Iconic_Cross_Sells( 'woothumbs', array(
			'iconic-woo-show-single-variations',
			'iconic-woo-attribute-swatches',
		) );

		return $instance->get_output();
	}

	/**
	 * Get doc links.
	 *
	 * @return array
	 */
	public static function get_doc_links() {
		$transient_name = 'iconic_woothumbs_getting_started_links';

		if ( false !== ( $return = get_transient( $transient_name ) ) ) {
			return $return;
		}

		$return = array();
		$url    = self::get_docs_url( 'getting-started' );
		$html   = file_get_contents( $url );

		if ( ! $html ) {
			set_transient( $transient_name, $return, 12 * HOUR_IN_SECONDS );

			return $return;
		}

		$dom = new DOMDocument;

		@$dom->loadHTML( $html );

		$lists = $dom->getElementsByTagName( 'ul' );

		if ( empty( $lists ) ) {
			set_transient( $transient_name, $return, 12 * HOUR_IN_SECONDS );

			return $return;
		}

		foreach ( $lists as $list ) {
			$classes = $list->getAttribute( 'class' );

			if ( strpos( $classes, 'articleList' ) === false ) {
				continue;
			}

			$links = $list->getElementsByTagName( 'a' );

			foreach ( $links as $link ) {
				$return[] = array(
					'href'  => $link->getAttribute( 'href' ),
					'title' => $link->nodeValue,
				);
			}
		}

		set_transient( $transient_name, $return, 30 * DAY_IN_SECONDS );

		return $return;
	}

	/**
	 * Output getting started links.
	 */
	public static function output_getting_started_links() {
		$links = self::get_doc_links();

		if ( empty( $links ) ) {
			return;
		} ?>
		<h3><?php _e( 'Getting Started', 'iconic-woothumbs' ); ?></h3>

		<ol>
			<?php foreach ( $links as $link ) { ?>
				<li>
					<a href="<?php echo esc_attr( self::get_docs_url() . $link['href'] ); ?>?utm_source=iconic-woothumbs&utm_medium=insideplugin" target="_blank"><?php echo $link['title']; ?></a>
				</li>
			<?php } ?>
		</ol>
	<?php }

	/**
	 * Is settings page?
	 *
	 * @param string $fs Freemius page suffix
	 *
	 * @return bool
	 */
	public static function is_settings_page( $fs = '' ) {
		if ( ! is_admin() ) {
			return false;
		}

		$path = 'iconic-woothumbs-settings' . $fs;

		if ( empty( $_GET['page'] ) || $_GET['page'] !== $path ) {
			return false;
		}

		return true;
	}

	/**
	 * Add getting started notice to settings pages.
	 */
	public static function account_getting_started() {
		if ( ! self::is_settings_page() && ! self::is_settings_page( '-account' ) ) {
			return;
		}

		$option_name = 'iconic_woothumbs_notice_dismiss_getting_started';
		$dismissed   = get_option( $option_name, false );

		if ( $dismissed ) {
			return;
		}

		$dismiss = filter_input( INPUT_POST, $option_name );

		if ( $dismiss ) {
			update_option( $option_name, true );

			return;
		}
		?>
		<style>
			.iconic-notice {
				padding: 35px 30px;
				background-color: #fff;
				margin: 20px 20px 20px 0;
				box-shadow: 0 1px 1px rgba(0, 0, 0, 0.04);
				border: 1px solid #e5e5e5;
				font-size: 15px;
				position: relative;
			}

			.iconic-notice h2 {
				margin: 0 0 1.2em;
				font-size: 23px;
				padding-left: 40px;
				position: relative;
				line-height: 1.2em;
			}

			.iconic-notice h2 img {
				position: absolute;
				left: 0;
				top: 0;
			}

			.iconic-notice h3 {
				margin: 0 0 1.5em;
			}

			.iconic-notice p,
			.iconic-notice li {
				font-size: 15px;
			}

			.iconic-notice li {
				margin: 0 0 10px;
			}

			.iconic-notice p,
			.iconic-notice ol,
			.iconic-notice ul {
				margin-bottom: 2em;
			}

			.iconic-notice :last-child {
				margin-bottom: 0;
			}

			.iconic-notice__dismiss {
				position: absolute;
				top: 20px;
				right: 20px;
			}

			.iconic-notice__dismiss button {
				background: none;
				border: none;
				padding: 0;
				margin: 0;
				cursor: pointer;
				color: #0073aa;
				outline: none;
			}

			.iconic-notice__dismiss button:hover,
			.iconic-notice__dismiss button:active {
				color: #00a0d2;
			}
		</style>
		<div class="iconic-notice iconic-notice--getting-started">
			<form action="" method="post" class="iconic-notice__dismiss">
				<input type="hidden" name="<?php echo esc_attr( $option_name ); ?>" value="1">
				<button title="<?php echo esc_attr( __( 'Dismiss', 'iconic-woothumbs' ) ); ?>">
					<span class="dashicons dashicons-dismiss"></span></button>
			</form>
			<h2>
				<img width="24" height="28" style="display: inline-block; vertical-align: text-bottom; margin: 0 12px 0 0" src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgd2lkdGg9IjMwcHgiIGhlaWdodD0iMzUuNDU1cHgiIHZpZXdCb3g9IjAgMCAzMCAzNS40NTUiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDMwIDM1LjQ1NSIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgk8Zz4NCgkJPHBvbHlnb24gcG9pbnRzPSIxMC45MSwzMy44MTggMTMuNjM2LDM1LjQ1NSAxMy42MzYsMTkuMDkxIDEwLjkxLDE3LjQ1NSAJCSIvPg0KCQk8cG9seWdvbiBwb2ludHM9IjE2LjM2MywzNS40NTUgMzAsMjcuMTY4IDMwLDIzLjk3NiAxNi4zNjMsMzIuMjYzIAkJIi8+DQoJCTxnPg0KCQkJPHBvbHlnb24gcG9pbnRzPSIxMi4zNSwxLjU5IDI1Ljk4Niw5Ljc3MiAyOC42MzcsOC4xODIgMTUsMCAJCQkiLz4NCgkJCTxwb2x5Z29uIHBvaW50cz0iNS40NTUsMzAuNTQ1IDguMTgyLDMyLjE4MiA4LjE4MiwxNS44MTggNS40NTUsMTQuMTgyIAkJCSIvPg0KCQkJPHBvbHlnb24gcG9pbnRzPSIxNi4zNjMsMjguOTIxIDMwLDIwLjYzNCAzMCwxNy40NDIgMTYuMzYzLDI1LjcyOSAJCQkiLz4NCgkJCTxwb2x5Z29uIHBvaW50cz0iNi44NzEsNC45ODQgMjAuNTA4LDEzLjE2NyAyMy4xNTgsMTEuNTc2IDkuNTIxLDMuMzk1IAkJCSIvPg0KCQkJPHBvbHlnb24gcG9pbnRzPSIyLjcyNywxMi41NDUgMCwxMC45MDkgMCwyNy4yNzMgMi43MjcsMjguOTA5IAkJCSIvPg0KCQkJPHBvbHlnb24gcG9pbnRzPSIxNi4zNjMsMjIuMzg4IDMwLDE0LjEgMzAsMTAuOTA5IDE2LjM2MywxOS4xOTYgCQkJIi8+DQoJCQk8cG9seWdvbiBwb2ludHM9IjEuMzkyLDguMTY1IDE1LjAyOCwxNi4zNDcgMTcuNjc4LDE0Ljc1NiA0LjA0Miw2LjU3NSAJCQkiLz4NCgkJPC9nPg0KCTwvZz4NCjwvZz4NCjwvc3ZnPg0K">
				<?php _e( 'Welcome to WooThumbs by Iconic', 'iconic-woothumbs' ); ?>
			</h2>
			<p><?php _e( "You're awesome! We've been looking forward to having you on board, and we're pleased to see the day has finally come.", 'iconic-woothumbs' ); ?></p>

			<?php self::output_getting_started_links(); ?>

			<p><strong><?php _e( 'Is something not quite right?', 'iconic-woothumbs' ); ?></strong> <?php printf( __( 'Take a look at our
				<a href="%s" target="_blank">troubleshooting documentation</a>', 'iconic-woothumbs' ), self::get_docs_url( 'troubleshooting' ) ); ?>.
			</p>
		</div>
		<?php
	}
}